import { Controller, Get, Query } from '@nestjs/common'
import { ApiOperation } from '@nestjs/swagger'
import dayjs from 'src/plugins/dayjs'
import { GetDayStringByDay } from './utilities/dateTime'

@Controller()
export class AppController {
  @Get()
  @ApiOperation({ summary: 'Healthz' })
  getHello() {
    return {
      date: new Date(),
      status: 'ok'
    }
  }

  @Get('time-zone')
  @ApiOperation({ summary: 'Check Time Zone' })
  async getTimeZone(@Query() query: any) {
    const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone
    const date = query.date || new Date().toISOString()
    const { tz } = query
    const dayOfWeekString = await GetDayStringByDay(dayjs().tz(tz).day())

    const now = dayjs(date).tz(tz)
    const nowUnix = now.valueOf()
    const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf()
    const endOfDay = now
      .add(1, 'day')
      .hour(3)
      .minute(59)
      .millisecond(999)
      .valueOf()
    let nowFormat = now.subtract(1, 'day').format('YYYYMMDD')

    if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
      nowFormat = now.format('YYYYMMDD')
    }

    return {
      nowFormat,
      timeZone,
      nowLocale: new Date().toLocaleString('en-US', {
        timeZone
      }),
      now: new Date().toString(),
      ISODate: new Date().toISOString(),
      dayjs: dayjs(date),
      UTCDayjs: dayjs(date).tz('UTC'),
      timeZoneDayjs: dayjs(date).tz(tz),
      dayjsFormat: dayjs(date).format('YYYYMMDD'),
      UTCDayjsFormat: dayjs(date).tz('UTC').format('YYYYMMDD'),
      timeZoneDayjsFormat: dayjs(date).tz(tz).format('YYYYMMDD'),
      timeZoneString: dayjs(date).tz(tz).toString(),
      dayOfWeekString
    }
  }
}
