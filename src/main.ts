import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import * as helmet from 'helmet'
import * as compression from 'compression'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { RedocOptions, RedocModule } from 'nestjs-redoc'
import { urlencoded, json } from 'express'
import {
  documentKitchenBuilder,
  documentOpenBuilder,
  documentRestaurantBuilder,
  documentSuperAdminBuilder,
  documentBuilderOnline,
  documentBuilderReview
} from './utilities/documentBuilder'
import { LogLogic } from './modules/log/logic/log.logic'
import { HttpExceptionFilter } from './exception-filter/http-exception.filter'
import { ErrorFilter } from './exception-filter/error.filter'

async function documentBuilder(app: any) {
  const options = new DocumentBuilder()
    .setTitle('POS service api')
    .setDescription('POS service api.')
    .setVersion('1.0')
    .addBearerAuth()
    .addBasicAuth()
    .addOAuth2()
    .addApiKey()
    .addCookieAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)
  const redocOptions: RedocOptions = {
    title: 'POS service api',
    // logo: {
    //   url: '<url>',
    //   backgroundColor: '#FAFAFA',
    //   altText: 'ava-logo'
    // },
    docName: 'POS-api',
    sortPropsAlphabetically: true,
    pathInMiddlePanel: true,
    hideDownloadButton: false,
    hideHostname: false,
    showExtensions: true,
    expandResponses: 'all',
    // favicon: '<url>',
    auth: {
      enabled: true,
      user: 'admin',
      password: 'qwerty1234@'
    }
  }
  await RedocModule.setup('/docs', app, document, redocOptions)
}

async function documentPOSBuilder(app: any) {
  const options = new DocumentBuilder()
    .setTitle('POS service api')
    .setDescription('POS service api.')
    .setVersion('1.0')
    .addBearerAuth()
    .addBasicAuth()
    .addOAuth2()
    .addApiKey()
    .addCookieAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)
  const redocOptions: RedocOptions = {
    title: 'POS service api',
    // logo: {
    //   url: '<url>',
    //   backgroundColor: '#FAFAFA',
    //   altText: 'ava-logo'
    // },
    docName: 'POS-api',
    sortPropsAlphabetically: true,
    pathInMiddlePanel: true,
    hideDownloadButton: false,
    hideHostname: false,
    showExtensions: true,
    expandResponses: 'all',
    // favicon: '<url>',
    tagGroups: [
      {
        name: 'Authentication',
        tags: ['auth/device', 'pos/staff', 'pos/auth']
      },
      {
        name: 'Restaurant',
        tags: ['pos/restaurant', 'pos/restaurant-shifts', 'pos/discount']
      },
      {
        name: 'Table',
        tags: ['pos/table']
      },
      {
        name: 'Menu',
        tags: ['pos/menu-category', 'pos/menu', 'pos/modifier']
      },
      {
        name: 'Customer',
        tags: ['pos/customer']
      },
      {
        name: 'Order / Kitchen',
        tags: ['pos/order', 'pos/kitchen', 'pos/online-order']
      },
      {
        name: 'Third Party Order',
        tags: ['pos/kitchen-hub']
      },
      {
        name: 'Payment Gateway & Transaction',
        tags: ['pos/payment', 'pos/transaction']
      },
      {
        name: 'Delivery Setting',
        tags: ['pos/delivery-setting']
      },
      {
        name: 'Report',
        tags: ['pos/report']
      },
      {
        name: 'Other Device',
        tags: ['pos/other-device']
      },
      {
        name: 'Cash Drawer',
        tags: ['pos/cash-drawer']
      }
    ],
    auth: {
      enabled: true,
      user: 'tthai',
      password: 'qwerty1234@'
    }
  }
  await RedocModule.setup('/docs/pos', app, document, redocOptions)
}

async function bootstrap() {
  const port = process.env.PORT || 3000
  const app = await NestFactory.create(AppModule)
  await documentBuilder(app)
  await documentPOSBuilder(app)
  await documentOpenBuilder(app)
  await documentSuperAdminBuilder(app)
  await documentKitchenBuilder(app)
  await documentRestaurantBuilder(app)
  await documentBuilderOnline(app)
  await documentBuilderReview(app)
  app.use(helmet())
  app.use(compression())
  app.enableCors()
  app.use(json({ limit: '100mb' }))
  app.use(urlencoded({ extended: true, limit: '100mb' }))

  const logLogic = await app.resolve<LogLogic>(LogLogic)

  app.useGlobalFilters(new ErrorFilter(logLogic))
  app.useGlobalFilters(new HttpExceptionFilter(logLogic))
  await app.listen(port)
}
bootstrap()
