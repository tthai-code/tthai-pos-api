import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type WebSettingDocument = WebSettingFields & Document

@Schema({ timestamps: true, strict: true, collection: 'webSettings' })
export class WebSettingFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ default: false })
  isOnlineOrdering: boolean

  @Prop({ default: false })
  isReservation: boolean

  @Prop({ default: null })
  bannerImageUrl: string

  @Prop({ default: null })
  domain: string

  @Prop({ default: null })
  subDomain: string

  @Prop({ default: null })
  favIconUrl: string

  @Prop({ default: null })
  logoUrl: string

  @Prop({ default: null })
  whatAppsUrl: string

  @Prop({ default: null })
  facebookUrl: string

  @Prop({ default: null })
  mainColor: string

  @Prop({ default: null })
  hoverColor: string

  @Prop({ default: null })
  contrastColor: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const WebSettingSchema = SchemaFactory.createForClass(WebSettingFields)
WebSettingSchema.plugin(authorStampCreatePlugin)
