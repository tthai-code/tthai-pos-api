import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { OnlineWebSettingController } from './controllers/web-setting-public.controller'
import { WebSettingController } from './controllers/web-setting.controller'
import { WebSettingSchema } from './schemas/web-setting.schema'
import { WebSettingService } from './services/web-setting.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([
      { name: 'webSetting', schema: WebSettingSchema }
    ])
  ],
  providers: [WebSettingService],
  controllers: [WebSettingController, OnlineWebSettingController],
  exports: [WebSettingService]
})
export class WebSettingModule {}
