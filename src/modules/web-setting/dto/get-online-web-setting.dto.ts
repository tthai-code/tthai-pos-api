import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class GetOnlineWebSettingDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly domain: string
}
