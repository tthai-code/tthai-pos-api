import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsBoolean, IsOptional, IsString } from 'class-validator'

export class UpdateWebSettingDto {
  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isOnlineOrdering: boolean

  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isReservation: boolean

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://banner-image-url.com' })
  readonly bannerImageUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://miro.com/app/board/uXjVPRryP58=/' })
  readonly domain: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://miro.com/app/board/uXjVPRryP58=/' })
  readonly subDomain: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://fav-icon-url.com' })
  readonly favIconUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://logo-url.com' })
  readonly logoUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://what-app-url.com' })
  readonly whatAppsUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://facebook-url.com' })
  readonly facebookUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '#99cc99' })
  readonly mainColor: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '#99cc99' })
  readonly hoverColor: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '#99cc99' })
  readonly contrastColor: string
}
