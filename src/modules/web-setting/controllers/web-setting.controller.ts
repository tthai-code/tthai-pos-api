import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Put,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { UpdateWebSettingDto } from '../dto/update-web-setting.dto'
import { GetWebSettingResponse } from '../entity/web-setting.entity'
import { WebSettingService } from '../services/web-setting.service'

@ApiBearerAuth()
@ApiTags('web-setting')
@UseGuards(AdminAuthGuard)
@Controller('v1/web-setting')
export class WebSettingController {
  constructor(
    private readonly webSettingService: WebSettingService,
    private readonly restaurantService: RestaurantService
  ) {}

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => GetWebSettingResponse })
  @ApiOperation({ summary: 'get web setting by restaurant' })
  async getCatering(@Param('restaurantId') id: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const catering = await this.webSettingService.findOne({
      restaurantId: id,
      status: StatusEnum.ACTIVE
    })
    if (!catering) {
      await this.webSettingService.create({
        restaurantId: id
      })
      return await this.webSettingService.findOne({
        restaurantId: id,
        status: StatusEnum.ACTIVE
      })
    }
    return catering
  }

  @Put(':restaurantId')
  @ApiOkResponse({ type: () => GetWebSettingResponse })
  @ApiOperation({ summary: 'update web setting by restaurant' })
  async updateOnlineReceipt(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateWebSettingDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return await this.webSettingService.findOneAndUpdate(
      { restaurantId: id },
      payload
    )
  }
}
