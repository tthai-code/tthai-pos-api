import {
  Body,
  Controller,
  NotFoundException,
  Post,
  // Request,
  UseGuards
} from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import {
  ApiBasicAuth,
  ApiCreatedResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { RestaurantHoursLogic } from 'src/modules/restaurant-hours/logics/restaurant-hours.logic'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { GetOnlineWebSettingDto } from '../dto/get-online-web-setting.dto'
import { PublicWebSettingResponse } from '../entity/public-web-setting.entity'
import { WebSettingService } from '../services/web-setting.service'

@ApiBasicAuth()
@ApiTags('online/web-setting')
@UseGuards(AuthGuard('basic'))
@Controller('v1/online/web-setting')
export class OnlineWebSettingController {
  constructor(
    private readonly webSettingService: WebSettingService,
    private readonly restaurantService: RestaurantService,
    private readonly restaurantHourLogic: RestaurantHoursLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Post()
  @ApiCreatedResponse({ type: () => PublicWebSettingResponse })
  @ApiOperation({
    summary: 'Get Web Setting For Web E-commerce',
    description: 'use `basic auth`'
  })
  async getWebSetting(@Body() payload: GetOnlineWebSettingDto) {
    const { domain } = payload
    const transformDomain = domain.replace(/(?:www.)?/, '')
    const webSetting = await this.webSettingService.findOne(
      {
        $or: [
          {
            domain: transformDomain
          },
          {
            subDomain: transformDomain
          }
        ],
        status: StatusEnum.ACTIVE
      },
      {
        status: 0,
        createdAt: 0,
        updatedAt: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    if (!webSetting) throw new NotFoundException()
    const restaurant = await this.restaurantService.findOne(
      { _id: webSetting.restaurantId },
      {
        taxRate: 1,
        logo_url: 1,
        address: 1,
        defaultServiceCharge: 1,
        legalBusinessName: 1,
        dba: 1,
        businessPhone: 1
      }
    )
    const { hours } = await this.restaurantHourLogic.getRestaurantHours(
      restaurant.id
    )
    return { ...webSetting.toObject(), restaurant, hours }
  }
}
