import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { WebSettingDocument } from '../schemas/web-setting.schema'

@Injectable({ scope: Scope.REQUEST })
export class WebSettingService {
  constructor(
    @InjectModel('webSetting')
    private readonly WebSettingModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<WebSettingDocument | any> {
    return this.WebSettingModel.findById({ _id: id })
  }

  async resolveByCondition(condition: any): Promise<WebSettingDocument | any> {
    return this.WebSettingModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.WebSettingModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<WebSettingDocument> {
    return this.WebSettingModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.WebSettingModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.WebSettingModel.createCollection()

    return this.WebSettingModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<WebSettingDocument> {
    const document = new this.WebSettingModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<WebSettingDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<WebSettingDocument> {
    const document = new this.WebSettingModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<WebSettingDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<WebSettingDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<WebSettingDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.WebSettingModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.WebSettingModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<WebSettingDocument> {
    return this.WebSettingModel.findOne(condition, project)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<WebSettingDocument> {
    return this.WebSettingModel.findOne(condition, options)
  }
}
