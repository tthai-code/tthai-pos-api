import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { DayEnum } from 'src/modules/restaurant-hours/common/day.enum'

class TaxRateObject {
  @ApiProperty()
  is_alcohol_tax_active: boolean
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
}

class AddressObject {
  @ApiProperty()
  zip_code: string
  @ApiProperty()
  state: string
  @ApiProperty()
  city: string
  @ApiProperty()
  address: string
}

class RestaurantObject {
  @ApiProperty({ type: TaxRateObject })
  tax_rate: TaxRateObject
  @ApiProperty()
  business_phone: string
  @ApiProperty({ type: AddressObject })
  address: AddressObject
  @ApiProperty()
  dba: string
  @ApiProperty()
  legal_business_name: string
  @ApiProperty()
  default_service_charge: number
  @ApiProperty()
  id: string
}

class PeriodObject {
  @ApiProperty()
  opens_at: string
  @ApiProperty()
  closes_at: string
}

class HoursObject {
  @ApiProperty({ enum: Object.values(DayEnum) })
  label: string
  @ApiProperty()
  is_closed: boolean
  @ApiProperty({ type: [PeriodObject] })
  period: PeriodObject[]
}

class WebSettingObject {
  @ApiProperty()
  sub_domain: string
  @ApiProperty()
  banner_image_url: string
  @ApiProperty()
  contrast_color: string
  @ApiProperty()
  hover_color: string
  @ApiProperty()
  main_color: string
  @ApiProperty()
  domain: string
  @ApiProperty()
  is_reservation: boolean
  @ApiProperty()
  is_online_ordering: boolean
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
  @ApiProperty({ type: RestaurantObject })
  restaurant: RestaurantObject
  @ApiProperty({ type: HoursObject })
  hours: HoursObject[]
}

export class PublicWebSettingResponse extends ResponseDto<WebSettingObject> {
  @ApiProperty({
    type: WebSettingObject,
    example: {
      facebook_url: null,
      what_apps_url: null,
      fav_icon_url: null,
      contrast_color: '#FF0000FF',
      hover_color: '#FF0000FF',
      main_color: '#FF0000FF',
      sub_domain: null,
      domain: 'tthai-store-staging.us-west-1.elasticbeanstalk.com',
      banner_image_url:
        'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/production/167336677425525414.png',
      is_reservation: false,
      is_online_ordering: true,
      restaurant_id: '63984d6690416c27a7415915',
      logo_url: '',
      id: '639b74eb5d5e20e31ebc7c61',
      restaurant: {
        default_service_charge: 10,
        tax_rate: {
          is_alcohol_tax_active: false,
          alcohol_tax: 0,
          tax: 10
        },
        business_phone: '1234567890',
        address: {
          zip_code: '90263',
          state: 'CA',
          city: 'Malibu',
          address: 'TEestst Address'
        },
        dba: 'Test DBA 03',
        legal_business_name: 'TeST 03',
        id: '63984d6690416c27a7415915'
      },
      hours: [
        {
          label: 'MONDAY',
          is_closed: false,
          period: [
            {
              opens_at: '01:00',
              closes_at: '05:00'
            },
            {
              opens_at: '12:00',
              closes_at: '15:00'
            }
          ]
        },
        {
          label: 'TUESDAY',
          is_closed: false,
          period: [
            {
              opens_at: '00:00',
              closes_at: '23:59'
            }
          ]
        },
        {
          label: 'WEDNESDAY',
          is_closed: false,
          period: [
            {
              opens_at: '00:01',
              closes_at: '23:59'
            }
          ]
        },
        {
          label: 'THURSDAY',
          is_closed: false,
          period: [
            {
              opens_at: '00:00',
              closes_at: '17:00'
            }
          ]
        },
        {
          label: 'FRIDAY',
          is_closed: false,
          period: [
            {
              opens_at: '10:00',
              closes_at: '13:00'
            },
            {
              opens_at: '15:00',
              closes_at: '17:00'
            }
          ]
        },
        {
          label: 'SATURDAY',
          is_closed: false,
          period: [
            {
              opens_at: '11:00',
              closes_at: '11:30'
            },
            {
              opens_at: '13:00',
              closes_at: '14:00'
            },
            {
              opens_at: '23:00',
              closes_at: '23:30'
            }
          ]
        },
        {
          label: 'SUNDAY',
          is_closed: true,
          period: []
        }
      ]
    }
  })
  data: WebSettingObject
}
