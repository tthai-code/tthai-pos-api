import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'

export class WebSettingResponseField extends TimestampResponseDto {
  @ApiProperty()
  isOnlineOrdering: boolean

  @ApiProperty()
  isReservation: boolean

  @ApiProperty()
  domain: string

  @ApiProperty()
  mainColor: string

  @ApiProperty()
  hoverColor: string

  @ApiProperty()
  contrastColor: string

  @ApiProperty()
  facebook_url: string

  @ApiProperty()
  what_apps_url: string

  @ApiProperty()
  logo_url: string

  @ApiProperty()
  fav_icon_url: string

  @ApiProperty()
  sub_domain: string

  @ApiProperty()
  banner_image_url: string
}

export class GetWebSettingResponse extends ResponseDto<WebSettingResponseField> {
  @ApiProperty({
    type: WebSettingResponseField,
    example: {
      facebook_url: null,
      what_apps_url: null,
      logo_url: null,
      fav_icon_url: null,
      sub_domain: null,
      banner_image_url: null,
      status: 'active',
      contrast_color: null,
      hover_color: null,
      main_color: null,
      domain: 'localhost:3001',
      is_reservation: false,
      is_online_ordering: false,
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-10-10T12:46:07.855Z',
      updated_at: '2022-12-01T05:05:24.359Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '6344140fce2588c0dfe2428b'
    }
  })
  data: WebSettingResponseField
}
