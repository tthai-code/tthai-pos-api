import { BasicStrategy as Strategy } from 'passport-http'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'

const usr = 'tthai_pos_api'
const pwd = '0#3hWwqS3iDVI1ou%JHrbFlY0v$'

@Injectable()
export class BasicStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      passReqToCallback: true
    })
  }

  public validate = async (
    _,
    username: string,
    password: string
  ): Promise<boolean> => {
    if (usr === username && pwd === password) {
      return true
    }
    throw new UnauthorizedException()
  }
}
