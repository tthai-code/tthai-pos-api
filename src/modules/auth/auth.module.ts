import { Module, HttpModule, Global } from '@nestjs/common'
import { JwtModule } from '@nestjs/jwt'
import { AdminStrategy } from './passport/admin-stategy'
import { PassportModule } from '@nestjs/passport'
import { UserModule } from '../user/user.module'
import { AuthLogic } from './logics/auth.logic'
import { jwtConstants } from './constants/auth'
import { MemberStrategy } from './passport/member-stategy'
import { BasicStrategy } from './passport/basic-strategy'
import { RestaurantAccountModule } from '../restaurant-account/restaurant-account.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { AccountStrategy } from './passport/account-strategy'
import { StaffModule } from '../staff/staff.module'
import { DeviceModule } from '../device/device.module'
import { DeviceStrategy } from './passport/device-strategy'
import { StaffStrategy } from './passport/staff-strategy'
import { SuperAdminStrategy } from './passport/super-admin.strategy'
import { HeaderApiKeyStrategy } from './passport/auth-header-api-key.strategy'
import { ConfigModule } from '@nestjs/config'
import { CustomerStrategy } from './passport/customer-strategy'
import { CustomerModule } from '../customer/customer.module'

@Global()
@Module({
  imports: [
    HttpModule,
    PassportModule,
    ConfigModule,
    UserModule,
    RestaurantAccountModule,
    RestaurantModule,
    DeviceModule,
    StaffModule,
    CustomerModule,
    JwtModule.register({
      secret: jwtConstants.secretAdmin,
      signOptions: {
        expiresIn: '7d'
      }
    })
  ],
  controllers: [],
  providers: [
    AdminStrategy,
    MemberStrategy,
    AuthLogic,
    BasicStrategy,
    AccountStrategy,
    DeviceStrategy,
    StaffStrategy,
    SuperAdminStrategy,
    HeaderApiKeyStrategy,
    CustomerStrategy
  ],
  exports: [AuthLogic]
})
export class AuthModule {}
