import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException
} from '@nestjs/common'
import { UserService } from 'src/modules/user/services/user.service'
import { LoginDto } from '../../user/dto/login.dto'
import { JwtService } from '@nestjs/jwt'
import * as bcrypt from 'bcryptjs'
import { jwtConstants } from '../constants/auth'
import { RestaurantAccountService } from 'src/modules/restaurant-account/services/restaurant-account.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { StatusEnum } from 'src/common/enum/status.enum'
import { StaffLoginDto } from 'src/modules/staff/dto/staff-login.dto'
import {
  CustomerLoginDto,
  CustomerSocialLoginDto
} from 'src/modules/customer/dto/customer-login.dto'

import { StaffService } from 'src/modules/staff/services/staff.service'
import { DeviceService } from 'src/modules/device/services/device.service'
import { CustomerService } from 'src/modules/customer/services/customer.service'
import { CustomerTypeEnum } from 'src/modules/customer/common/enum/customer-type.enum'

@Injectable()
export class AuthLogic {
  constructor(
    private readonly userService: UserService,
    private readonly accountService: RestaurantAccountService,
    private readonly restaurantService: RestaurantService,
    private readonly staffService: StaffService,
    private readonly deviceService: DeviceService,
    private readonly customerService: CustomerService,
    private readonly jwtService: JwtService
  ) {}

  jwtSignToken(payload: any, expiresIn: string, secret: string) {
    return this.jwtService.sign(payload, {
      expiresIn,
      secret
    })
  }

  async loginLogic(login: LoginDto) {
    const userData = await this.userService.findOneWithPassword({
      username: login.username
    })

    const now = new Date()

    if (!userData) {
      throw new UnauthorizedException()
    }

    const validateUser = await bcrypt.compare(login.password, userData.password)
    if (!validateUser) {
      throw new UnauthorizedException()
    }

    const payload = {
      username: userData.username,
      id: userData.id,
      role: userData.role
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretSuperAdmin
    })

    return {
      ...payload,
      accessToken,
      tokenExpire: now.setDate(now.getDate() + 7)
    }
  }

  async RestaurantLoginLogic(login: LoginDto) {
    const accountData = await this.accountService.findOneWithPassword({
      username: login.username
    })

    const now = new Date()

    if (!accountData) {
      throw new UnauthorizedException()
    }

    const restaurant = await this.restaurantService
      .getModel()
      .findOne({
        accountId: accountData._id,
        status: StatusEnum.ACTIVE
      })
      .select({
        _id: 0,
        id: '$_id',
        legalBusinessName: 1,
        address: 1,
        businessPhone: 1,
        email: 1,
        taxRate: 1,
        logoUrl: 1,
        printerSetting: 1,
        authorizedPersonFirstName: 1,
        authorizedPersonLastName: 1,
        defaultServiceCharge: 1,
        timeZone: 1
      })

    const validateUser = await bcrypt.compare(
      login.password,
      accountData.password
    )
    if (!validateUser) {
      throw new UnauthorizedException()
    }

    const payload = {
      username: accountData.username,
      id: accountData.id,
      ownerName:
        restaurant &&
        restaurant.authorizedPersonFirstName &&
        restaurant.authorizedPersonLastName
          ? `${restaurant.authorizedPersonFirstName} ${restaurant.authorizedPersonLastName}`
          : 'User Admin',
      restaurant: restaurant ? restaurant : null
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretAdmin
    })

    return {
      ...payload,
      accessToken,
      tokenExpire: now.setDate(now.getDate() + 7)
    }
  }

  async RestaurantLoginAfterCreated(username: string) {
    const accountData = await this.accountService.findOneWithPassword({
      username
    })

    const now = new Date()
    if (!accountData) {
      throw new UnauthorizedException()
    }

    const restaurant = await this.restaurantService
      .getModel()
      .findOne({
        accountId: accountData._id,
        status: StatusEnum.ACTIVE
      })
      .select({
        _id: 0,
        id: '$_id',
        legalBusinessName: 1,
        address: 1,
        businessPhone: 1,
        email: 1,
        taxRate: 1,
        logoUrl: 1,
        printerSetting: 1,
        authorizedPersonFirstName: 1,
        authorizedPersonLastName: 1,
        defaultServiceCharge: 1,
        timeZone: 1
      })

    const payload = {
      username: accountData.username,
      id: accountData.id,
      ownerName:
        restaurant &&
        restaurant.authorizedPersonFirstName &&
        restaurant.authorizedPersonLastName
          ? `${restaurant.authorizedPersonFirstName} ${restaurant.authorizedPersonLastName}`
          : 'User Admin',
      restaurant: restaurant ? restaurant : null
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretAdmin
    })

    return {
      ...payload,
      accessToken,
      tokenExpire: now.setDate(now.getDate() + 7)
    }
  }

  async StaffLogin(restaurantId: string, login: StaffLoginDto) {
    const staff = await this.staffService.getModel().findOne(
      {
        restaurantId,
        passCode: login.passCode,
        status: StatusEnum.ACTIVE
      },
      { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, passCode: 0 }
    )
    if (!staff) throw new UnauthorizedException()

    const now = new Date()

    const restaurant = await this.restaurantService
      .getModel()
      .findOne({
        _id: staff.restaurantId,
        status: StatusEnum.ACTIVE
      })
      .select({
        _id: 0,
        id: '$_id',
        legalBusinessName: 1,
        dba: 1,
        address: 1,
        businessPhone: 1,
        email: 1,
        taxRate: 1,
        logoUrl: 1,
        authorizedPersonFirstName: 1,
        authorizedPersonLastName: 1,
        defaultServiceCharge: 1,
        timeZone: 1
      })

    const payload = {
      username: staff.firstName,
      id: staff.id,
      role: staff.role,
      ownerName:
        restaurant &&
        restaurant.authorizedPersonFirstName &&
        restaurant.authorizedPersonLastName
          ? `${restaurant.authorizedPersonFirstName} ${restaurant.authorizedPersonLastName}`
          : 'User Admin',
      restaurant: restaurant ? restaurant : null
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretStaff
    })

    return {
      ...payload,
      accessToken,
      tokenExpire: now.setDate(now.getDate() + 7)
    }
  }

  async ActivateDeviceCode(code: string) {
    const device = await this.deviceService.findOne({
      activationCode: code,
      status: StatusEnum.ACTIVE
    })
    if (!device) throw new NotFoundException('Not found activation code.')
    if (device.isActivated) {
      throw new BadRequestException('This activation code is already used.')
    }
    const restaurant = await this.restaurantService.findOne({
      _id: device.restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new BadRequestException()

    const activated = await this.deviceService.update(device._id, {
      isActivated: true
    })
    if (!activated) throw new BadRequestException()

    const payload = {
      restaurant: {
        id: restaurant._id
      }
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretDevice
    })

    return {
      ...payload,
      accessToken
    }
  }

  async DirectAdminActivateDeviceCode(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new BadRequestException()

    const payload = {
      restaurant: {
        id: restaurant._id
      }
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretDevice
    })

    return {
      ...payload,
      accessToken
    }
  }

  async CustomerLogin(login: CustomerLoginDto) {
    const customer = await this.customerService.findOne({
      email: login.email,
      restaurantId: login.restaurantId,
      status: StatusEnum.ACTIVE,
      loginType: CustomerTypeEnum.NORMAL
    })
    if (!customer) throw new UnauthorizedException('User is not exist.')

    const validateUser = await bcrypt.compare(login.password, customer.password)

    if (!validateUser) {
      throw new UnauthorizedException('Your account or password is incorrect.')
    }
    const now = new Date()
    const payload = {
      address: customer.address,
      uidSocial: customer.uidSocial,
      loginType: customer.loginType,
      tel: customer.tel,
      email: customer.email,
      lastName: customer.lastName,
      firstName: customer.firstName,
      restaurantId: customer.restaurantId,
      id: customer.id
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretCustomer
    })

    return {
      ...payload,
      accessToken,
      tokenExpire: now.setDate(now.getDate() + 7)
    }
  }

  async CustomerSocialLogin(login: CustomerSocialLoginDto) {
    const customer = await this.customerService.findOne({
      restaurantId: login.restaurantId,
      uidSocial: login.uidSocial,
      loginType: login.loginType,
      status: StatusEnum.ACTIVE
    })

    if (!customer) {
      throw new UnauthorizedException(`Can't find your account`)
    }
    const now = new Date()
    const payload = {
      address: customer.address,
      uidSocial: customer.uidSocial,
      loginType: customer.loginType,
      tel: customer.tel,
      email: customer.email,
      lastName: customer.lastName,
      firstName: customer.firstName,
      restaurantId: customer.restaurantId,
      id: customer.id
    }
    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretCustomer
    })

    return {
      ...payload,
      accessToken,
      tokenExpire: now.setDate(now.getDate() + 7)
    }
  }

  async DirectAdminLoginLogic(restaurantId: string) {
    const now = new Date()
    const restaurant = await this.restaurantService
      .getModel()
      .findOne({
        _id: restaurantId,
        status: StatusEnum.ACTIVE
      })
      .select({
        _id: 0,
        id: '$_id',
        legalBusinessName: 1,
        address: 1,
        businessPhone: 1,
        email: 1,
        taxRate: 1,
        logoUrl: 1,
        printerSetting: 1,
        authorizedPersonFirstName: 1,
        authorizedPersonLastName: 1,
        defaultServiceCharge: 1,
        accountId: 1,
        timeZone: 1
      })

    const accountData = await this.accountService.findOneWithPassword({
      _id: restaurant.accountId
    })
    delete restaurant.accountId
    const payload = {
      username: accountData.username,
      id: accountData.id,
      ownerName:
        restaurant &&
        restaurant.authorizedPersonFirstName &&
        restaurant.authorizedPersonLastName
          ? `${restaurant.authorizedPersonFirstName} ${restaurant.authorizedPersonLastName}`
          : 'User Admin',
      restaurant: restaurant ? restaurant : null
    }

    const accessToken = this.jwtService.sign(payload, {
      expiresIn: '7d',
      secret: jwtConstants.secretAdmin
    })

    return {
      ...payload,
      accessToken,
      tokenExpire: now.setDate(now.getDate() + 7)
    }
  }
}
