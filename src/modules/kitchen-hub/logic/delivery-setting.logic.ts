import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { ProviderEnum } from '../common/provider.enum'
import { PauseProviderDto } from '../dto/update-auto.dto'
import { ISetProviderStatus } from '../interfaces/kitchen-hub.interface'
import { DeliverySettingService } from '../services/delivery-setting.service'
import { KitchenHubHttpService } from '../services/kitchen-hub-http.service'

@Injectable()
export class DeliverySettingLogic {
  constructor(
    private readonly deliverySettingService: DeliverySettingService,
    private readonly httpService: KitchenHubHttpService,
    private readonly restaurantService: RestaurantService
  ) {}

  async getDeliverySetting(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    let deliverySetting = await this.deliverySettingService.findOne(
      {
        restaurantId
      },
      { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 }
    )
    if (!deliverySetting) {
      const { data } = await this.httpService.createStore(restaurant.dba)
      if (!data?.ok) throw new BadRequestException(data?.error_code)
      await this.deliverySettingService.create({
        restaurantId,
        storeId: data.result.id
      })
      deliverySetting = await this.deliverySettingService.findOne(
        {
          restaurantId
        },
        { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 }
      )
    }
    const { data: providers } = await this.httpService.getStoreProviders(
      deliverySetting.storeId
    )
    if (!providers.ok) throw new BadRequestException(providers?.error_code)
    const activeProviders = providers.result
      .filter((item) => item.active)
      .map((item) => item.provider_id)

    const providerStatus = await Promise.all(
      activeProviders.map((item) =>
        this.httpService.getProviderStatus(item, deliverySetting.storeId)
      )
    )
    const providerResult = providerStatus.map((item) => {
      const { data } = item
      return data.result
    })
    const mapProviders = activeProviders.map((item, index) => ({
      provider: item,
      isConnected: true,
      online: providerResult[index].online,
      canPause: item !== ProviderEnum.GLORIAFOOD
    }))
    const allProviderStatus = Object.values(ProviderEnum).map((provider) => {
      const filter = mapProviders.filter((e) => e.provider === provider)
      if (filter.length > 0) return filter[0]
      return {
        provider,
        isConnected: false,
        online: false,
        canPause: provider !== ProviderEnum.GLORIAFOOD
      }
    })
    const transform = deliverySetting.toObject()
    const result = {
      ...transform,
      onlineCount: mapProviders.filter((item) => item.online).length,
      providers: allProviderStatus
    }
    delete result.storeId
    return result
  }

  async pauseProvider(restaurantId: string, pauseProvider: PauseProviderDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const deliverySetting = await this.deliverySettingService.findOne(
      {
        restaurantId
      },
      { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 }
    )
    const { storeId } = deliverySetting
    const payload: ISetProviderStatus = {
      online: pauseProvider.online,
      pause_duration_seconds: null,
      provider_id: pauseProvider.provider,
      store_id: storeId
    }
    const { data } = await this.httpService.setProviderStatus(payload)
    if (!data.ok) throw new BadRequestException(data.error_code)
    return { success: true }
  }
}
