import { Injectable } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ThirdPartyOrderPaginateDto } from 'src/modules/order/dto/get-third-party-order.dto'
import {
  OrderStateEnum,
  ThirdPartyOrderStatusEnum
} from 'src/modules/order/enum/order-state.enum'
import { OrderMethodEnum } from 'src/modules/order/enum/order-type.enum'
import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service'
import { ThirdPartyOrderService } from 'src/modules/order/services/third-party-order.service'
import { ProviderEnum } from '../common/provider.enum'

@Injectable()
export class POSKitchenHubLogic {
  constructor(
    private readonly thirdPartyOrderService: ThirdPartyOrderService,
    private readonly kitchenQueueService: KitchenQueueService
  ) {}

  async getAllOrder(restaurantId: string, query: ThirdPartyOrderPaginateDto) {
    const allOrder = await this.thirdPartyOrderService.getAll(
      query.buildQueryGetAll(restaurantId)
    )
    const orders = await this.thirdPartyOrderService.paginate(
      query.buildQuery(restaurantId),
      query,
      { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 }
    )
    const { docs } = orders
    const orderIds = docs.map((item) => item.id)
    const kitchenQueues = await this.kitchenQueueService.getAll({
      orderId: { $in: orderIds },
      status: StatusEnum.ACTIVE
    })

    const transformDocs = docs.map((item) => {
      const index = kitchenQueues.findIndex(
        (queue) => queue.orderId === item.id
      )
      if (index > -1) {
        return {
          ...item.toObject(),
          ticketNo: kitchenQueues[index]?.ticketNo || null
        }
      }
      return {
        ...item.toObject(),
        ticketNo: kitchenQueues[index]?.ticketNo || null
      }
    })

    const results = {
      ...orders,
      docs: transformDocs,
      status: {
        active: allOrder.filter(
          (item) =>
            item.orderStatus !== OrderStateEnum.COMPLETED &&
            item.orderStatus !== OrderStateEnum.CANCELED
        ).length,
        new: allOrder.filter((item) => item.orderStatus === OrderStateEnum.NEW)
          .length,
        inProgress: allOrder.filter(
          (item) => item.orderStatus === OrderStateEnum.IN_PROGRESS
        ).length,
        ready: allOrder.filter(
          (item) => item.orderStatus === OrderStateEnum.READY
        ).length,
        upcoming: allOrder.filter((item) => !item.asap).length,
        completed: allOrder.filter(
          (item) => item.orderStatus === OrderStateEnum.COMPLETED
        ).length
      },
      type: {
        delivery: allOrder.filter((item) => {
          const deliveryCondition =
            item.orderMethod === OrderMethodEnum.DELIVERY
          if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
            return (
              item.orderStatus !== OrderStateEnum.COMPLETED &&
              item.orderStatus !== OrderStateEnum.CANCELED &&
              deliveryCondition
            )
          }
          if (query.status === ThirdPartyOrderStatusEnum.NEW) {
            return item.orderStatus === OrderStateEnum.NEW && deliveryCondition
          }
          if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
            return (
              item.orderStatus === OrderStateEnum.IN_PROGRESS &&
              deliveryCondition
            )
          }
          if (query.status === ThirdPartyOrderStatusEnum.READY) {
            return (
              item.orderStatus === OrderStateEnum.READY && deliveryCondition
            )
          }
          if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
            return (
              item.orderStatus === OrderStateEnum.COMPLETED && deliveryCondition
            )
          }
        }).length,
        pickup: allOrder.filter((item) => {
          const pickUpCondition = item.orderMethod === OrderMethodEnum.PICKUP
          if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
            return (
              item.orderStatus !== OrderStateEnum.COMPLETED &&
              item.orderStatus !== OrderStateEnum.CANCELED &&
              pickUpCondition
            )
          }
          if (query.status === ThirdPartyOrderStatusEnum.NEW) {
            return item.orderStatus === OrderStateEnum.NEW && pickUpCondition
          }
          if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
            return (
              item.orderStatus === OrderStateEnum.IN_PROGRESS && pickUpCondition
            )
          }
          if (query.status === ThirdPartyOrderStatusEnum.READY) {
            return item.orderStatus === OrderStateEnum.READY && pickUpCondition
          }
          if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
            return (
              item.orderStatus === OrderStateEnum.COMPLETED && pickUpCondition
            )
          }
        }).length
      },
      source: {
        doordash: allOrder.filter((item) => {
          if (item.deliverySource !== ProviderEnum.DOORDASH) {
            return false
          }
          if (
            item.orderMethod === OrderMethodEnum.DELIVERY &&
            query.type === OrderMethodEnum.DELIVERY
          ) {
            if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
              return (
                item.orderStatus !== OrderStateEnum.COMPLETED &&
                item.orderStatus !== OrderStateEnum.CANCELED
              )
            }
            if (query.status === ThirdPartyOrderStatusEnum.NEW) {
              return item.orderStatus === OrderStateEnum.NEW
            }
            if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
              return item.orderStatus === OrderStateEnum.IN_PROGRESS
            }
            if (query.status === ThirdPartyOrderStatusEnum.READY) {
              return item.orderStatus === OrderStateEnum.READY
            }
            if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
              return item.orderStatus === OrderStateEnum.COMPLETED
            }
          }
          if (
            item.orderMethod === OrderMethodEnum.PICKUP &&
            query.type === OrderMethodEnum.PICKUP
          ) {
            if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
              return (
                item.orderStatus !== OrderStateEnum.COMPLETED &&
                item.orderStatus !== OrderStateEnum.CANCELED
              )
            }
            if (query.status === ThirdPartyOrderStatusEnum.NEW) {
              return item.orderStatus === OrderStateEnum.NEW
            }
            if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
              return item.orderStatus === OrderStateEnum.IN_PROGRESS
            }
            if (query.status === ThirdPartyOrderStatusEnum.READY) {
              return item.orderStatus === OrderStateEnum.READY
            }
            if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
              return item.orderStatus === OrderStateEnum.COMPLETED
            }
          }
        }).length,
        gloriafood: allOrder.filter((item) => {
          if (item.deliverySource !== ProviderEnum.GLORIAFOOD) {
            return false
          }
          if (
            item.orderMethod === OrderMethodEnum.DELIVERY &&
            query.type === OrderMethodEnum.DELIVERY
          ) {
            if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
              return (
                item.orderStatus !== OrderStateEnum.COMPLETED &&
                item.orderStatus !== OrderStateEnum.CANCELED
              )
            }
            if (query.status === ThirdPartyOrderStatusEnum.NEW) {
              return item.orderStatus === OrderStateEnum.NEW
            }
            if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
              return item.orderStatus === OrderStateEnum.IN_PROGRESS
            }
            if (query.status === ThirdPartyOrderStatusEnum.READY) {
              return item.orderStatus === OrderStateEnum.READY
            }
            if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
              return item.orderStatus === OrderStateEnum.COMPLETED
            }
          }
          if (
            item.orderMethod === OrderMethodEnum.PICKUP &&
            query.type === OrderMethodEnum.PICKUP
          ) {
            if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
              return (
                item.orderStatus !== OrderStateEnum.COMPLETED &&
                item.orderStatus !== OrderStateEnum.CANCELED
              )
            }
            if (query.status === ThirdPartyOrderStatusEnum.NEW) {
              return item.orderStatus === OrderStateEnum.NEW
            }
            if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
              return item.orderStatus === OrderStateEnum.IN_PROGRESS
            }
            if (query.status === ThirdPartyOrderStatusEnum.READY) {
              return item.orderStatus === OrderStateEnum.READY
            }
            if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
              return item.orderStatus === OrderStateEnum.COMPLETED
            }
          }
        }).length,
        grubhub: allOrder.filter((item) => {
          if (item.deliverySource !== ProviderEnum.GRUBHUB) {
            return false
          }
          if (
            item.orderMethod === OrderMethodEnum.DELIVERY &&
            query.type === OrderMethodEnum.DELIVERY
          ) {
            if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
              return (
                item.orderStatus !== OrderStateEnum.COMPLETED &&
                item.orderStatus !== OrderStateEnum.CANCELED
              )
            }
            if (query.status === ThirdPartyOrderStatusEnum.NEW) {
              return item.orderStatus === OrderStateEnum.NEW
            }
            if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
              return item.orderStatus === OrderStateEnum.IN_PROGRESS
            }
            if (query.status === ThirdPartyOrderStatusEnum.READY) {
              return item.orderStatus === OrderStateEnum.READY
            }
            if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
              return item.orderStatus === OrderStateEnum.COMPLETED
            }
          }
          if (
            item.orderMethod === OrderMethodEnum.PICKUP &&
            query.type === OrderMethodEnum.PICKUP
          ) {
            if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
              return (
                item.orderStatus !== OrderStateEnum.COMPLETED &&
                item.orderStatus !== OrderStateEnum.CANCELED
              )
            }
            if (query.status === ThirdPartyOrderStatusEnum.NEW) {
              return item.orderStatus === OrderStateEnum.NEW
            }
            if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
              return item.orderStatus === OrderStateEnum.IN_PROGRESS
            }
            if (query.status === ThirdPartyOrderStatusEnum.READY) {
              return item.orderStatus === OrderStateEnum.READY
            }
            if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
              return item.orderStatus === OrderStateEnum.COMPLETED
            }
          }
        }).length,
        ubereats: allOrder.filter((item) => {
          if (item.deliverySource !== ProviderEnum.UBEREATS) {
            return false
          }
          if (
            item.orderMethod === OrderMethodEnum.DELIVERY &&
            query.type === OrderMethodEnum.DELIVERY
          ) {
            if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
              return (
                item.orderStatus !== OrderStateEnum.COMPLETED &&
                item.orderStatus !== OrderStateEnum.CANCELED
              )
            }
            if (query.status === ThirdPartyOrderStatusEnum.NEW) {
              return item.orderStatus === OrderStateEnum.NEW
            }
            if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
              return item.orderStatus === OrderStateEnum.IN_PROGRESS
            }
            if (query.status === ThirdPartyOrderStatusEnum.READY) {
              return item.orderStatus === OrderStateEnum.READY
            }
            if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
              return item.orderStatus === OrderStateEnum.COMPLETED
            }
          }
          if (
            item.orderMethod === OrderMethodEnum.PICKUP &&
            query.type === OrderMethodEnum.PICKUP
          ) {
            if (query.status === ThirdPartyOrderStatusEnum.ACTIVE) {
              return (
                item.orderStatus !== OrderStateEnum.COMPLETED &&
                item.orderStatus !== OrderStateEnum.CANCELED
              )
            }
            if (query.status === ThirdPartyOrderStatusEnum.NEW) {
              return item.orderStatus === OrderStateEnum.NEW
            }
            if (query.status === ThirdPartyOrderStatusEnum.IN_PROGRESS) {
              return item.orderStatus === OrderStateEnum.IN_PROGRESS
            }
            if (query.status === ThirdPartyOrderStatusEnum.READY) {
              return item.orderStatus === OrderStateEnum.READY
            }
            if (query.status === ThirdPartyOrderStatusEnum.COMPLETED) {
              return item.orderStatus === OrderStateEnum.COMPLETED
            }
          }
        }).length
      }
    }

    return results
  }
}
