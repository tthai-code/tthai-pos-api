import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import dayjs from 'src/plugins/dayjs'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { ItemStatusEnum } from 'src/modules/order/enum/item-status.enum'
import { OrderStateEnum } from 'src/modules/order/enum/order-state.enum'
import {
  OrderMethodEnum,
  OrderTypeEnum
} from 'src/modules/order/enum/order-type.enum'
import {
  IKitchenHubToOrderPayload,
  IMenuItemsObjectPayload
} from 'src/modules/order/interfaces/third-party-order.interfaces'
import { KitchenCounterService } from 'src/modules/order/services/kitchen-counter.service'
import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service'
import { OrderItemsService } from 'src/modules/order/services/order-items.service'
import { OrderService } from 'src/modules/order/services/order.service'
import { ThirdPartyOrderService } from 'src/modules/order/services/third-party-order.service'
import { RestaurantDocument } from 'src/modules/restaurant/schemas/restaurant.schema'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { SocketGateway } from 'src/modules/socket/socket.gateway'
import { TransactionLogic } from 'src/modules/transaction/logics/transaction.logic'
import { TThaiAppLogic } from 'src/modules/tthai-app/logics/tthai-app.logic'
import { KitchenHubOrderStatusEnum } from '../common/kitchen-order-status.enum'
import { CreateKitchenHubOrderDto } from '../dto/create-kitchen-hub.dto'
import { IKitchenHubOrderNotify } from '../interfaces/notify-order.interface'
import { DeliverySettingService } from '../services/delivery-setting.service'
import { KitchenHubHttpService } from '../services/kitchen-hub-http.service'
import { KitchenHubService } from '../services/kitchen-hub.service'
import { AcceptOrderDto } from '../dto/accept-order.dto'

@Injectable()
export class KitchenHubLogic {
  constructor(
    private readonly kitchenHubService: KitchenHubService,
    private readonly kitchenHubHttpService: KitchenHubHttpService,
    private readonly restaurantService: RestaurantService,
    private readonly transactionLogic: TransactionLogic,
    private readonly socketGateway: SocketGateway,
    private readonly deliverySettingService: DeliverySettingService,
    private readonly orderService: OrderService,
    private readonly thirdPartyOrderService: ThirdPartyOrderService,
    private readonly kitchenQueueService: KitchenQueueService,
    private readonly kitchenCounterService: KitchenCounterService,
    private readonly orderItemsService: OrderItemsService,
    private readonly tthaiAppLogic: TThaiAppLogic
  ) {}

  private randomOrderId() {
    const message =
      'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789'
    let id = ''
    for (let i = 0; i < 8; i++) {
      id += message.charAt(Math.floor(Math.random() * message.length))
    }
    return id
  }

  private async genOrderId(): Promise<string> {
    let id = ''
    while (true) {
      id = this.randomOrderId()
      const isDuplicated = await this.orderService.findOne({
        _id: id
      })
      if (!isDuplicated) {
        break
      }
    }

    return id
  }

  private async queueKitchen(restaurantId: string): Promise<string> {
    const { timeZone } = await this.restaurantService.findOne({
      _id: restaurantId
    })
    const now = dayjs().tz(timeZone)
    const nowUnix = now.valueOf()
    const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf()
    const endOfDay = now
      .add(1, 'day')
      .hour(3)
      .minute(59)
      .millisecond(999)
      .valueOf()
    let nowFormat = now.subtract(1, 'day').format('YYYYMMDD')

    if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
      nowFormat = now.format('YYYYMMDD')
    }

    const countQueue = await this.kitchenCounterService.getCounter(
      nowFormat,
      restaurantId
    )
    return `${countQueue}`
  }

  private mappingOrderMethod(method: string): string {
    switch (method) {
      case 'pickup':
        return OrderMethodEnum.PICKUP
      case 'delivery':
        return OrderMethodEnum.DELIVERY
      default:
        return OrderMethodEnum.DINE_IN
    }
  }

  private async mappingKHOrderToOrder(
    restaurant: RestaurantDocument,
    payload: any
  ): Promise<IKitchenHubToOrderPayload> {
    const { order, customer, items, charges, timestamps, provider } = payload
    const orderPayload: IKitchenHubToOrderPayload = {}
    orderPayload.orderKHId = order.id
    orderPayload.restaurant = {
      id: restaurant.id,
      legalBusinessName: restaurant.legalBusinessName,
      address: restaurant.address,
      businessPhone: restaurant.businessPhone,
      email: restaurant.email
    }
    orderPayload._id = await this.genOrderId()
    const customerName = customer.name.split(' ')
    orderPayload.customer = {
      firstName: customerName[0],
      lastName: customerName[customerName.length - 1],
      tel: customer.phoneNumber
    }
    orderPayload.orderType = OrderTypeEnum.THIRD_PARTY
    orderPayload.orderMethod = this.mappingOrderMethod(order.type)
    orderPayload.summaryItems = items.map((item) => {
      const result: IMenuItemsObjectPayload = {}
      result.id = item?.id
      result.name = item?.name
      result.unit = item?.quantity
      result.price = Number(item?.basePrice)
      result.amount = Number(item?.price)
      result.modifiers = item?.options?.map((option) => ({
        id: option?.id,
        name: option?.name,
        label: option?.modifierName,
        abbreviation: option?.modifierName ? option?.modifierName[0] : null,
        nativeName: option?.name,
        price: Number(option?.price)
      }))
      result.note = item?.instructions
      return result
    })
    orderPayload.subtotal = charges?.subtotal || 0
    orderPayload.discount = charges?.discount || 0
    orderPayload.tax = charges?.tax || 0
    orderPayload.tips = charges?.tips || 0
    orderPayload.total = charges?.total - (charges?.tips || 0)
    orderPayload.asap = order.asap
    orderPayload.orderDate = timestamps.placedAt
    orderPayload.pickUpDate = timestamps?.pickupAt
    orderPayload.scheduledForDate = timestamps?.scheduledFor
    orderPayload.deliverySource = provider.id
    return orderPayload
  }

  async calculatePrepareTime(
    restaurantId: string,
    itemLength: number
  ): Promise<number> {
    const tthaiAppSetting = await this.tthaiAppLogic.getTThaiApp(restaurantId)
    const { waitTime } = tthaiAppSetting
    const queue = await this.kitchenQueueService.getAll({
      restaurantId,
      queueStatus: ItemStatusEnum.IN_PROGRESS
    })
    const length = queue.length || 0
    let prepareTime =
      waitTime.baseTime +
      length * waitTime.perOrderTime +
      itemLength * waitTime.perItemTime
    if (prepareTime > waitTime.maxTime) {
      prepareTime = waitTime.maxTime
    }
    return prepareTime
  }

  async handleNewOrderFromKH(
    payload: CreateKitchenHubOrderDto,
    isAutoAccept: boolean,
    restaurant: RestaurantDocument,
    session: ClientSession
  ) {
    const { order, items } = payload
    const orderPayload = await this.mappingKHOrderToOrder(restaurant, payload)

    if (isAutoAccept) {
      const prepareTime = await this.calculatePrepareTime(
        restaurant.id,
        items.length
      )
      orderPayload.orderStatus = OrderStateEnum.IN_PROGRESS
      orderPayload.prepareTime = prepareTime
      const created = await this.thirdPartyOrderService.transactionCreate(
        orderPayload,
        session
      )

      const { data } = await this.kitchenHubHttpService.acceptOrder(
        order.id.toString(),
        prepareTime
      )
      if (!data.ok) throw new BadRequestException(data.error_code)
      const itemPayload = created.summaryItems.map((item) => {
        const transform = {
          ...item,
          orderId: created.id
        }
        return transform
      })
      const itemsData = await this.orderItemsService.transactionCreateMany(
        itemPayload,
        session
      )
      const ticketNo = await this.queueKitchen(created.restaurant.id)
      await this.kitchenQueueService.transactionCreate(
        {
          restaurantId: created.restaurant.id,
          orderId: created.id,
          ticketNo,
          items: itemsData.map((item) => item.id)
        },
        session
      )
      await this.transactionLogic.createThirdPartyOrderTransaction(
        created,
        session
      )
      const notifyObject: IKitchenHubOrderNotify = {
        id: created.id,
        kitchenHubId: created.orderKHId,
        customer: created.customer,
        type: created.orderMethod,
        placedDate: created.orderDate,
        source: created.deliverySource,
        orderStatus: created.orderStatus
      }
      this.socketGateway.server
        .to(`pos-${restaurant.id}`)
        .emit('receiveThirdPartyOrder', notifyObject)
      return created
    }
    const created = await this.thirdPartyOrderService.transactionCreate(
      orderPayload,
      session
    )
    const notifyObject: IKitchenHubOrderNotify = {
      id: created.id,
      kitchenHubId: created.orderKHId,
      customer: created.customer,
      type: created.orderMethod,
      placedDate: created.orderDate,
      source: created.deliverySource,
      orderStatus: created.orderStatus
    }
    this.socketGateway.server
      .to(`pos-${restaurant.id}`)
      .emit('receiveThirdPartyOrder', notifyObject)
    return created
  }

  async receiveOrderFromKH(payload: CreateKitchenHubOrderDto) {
    const logic = async (session: ClientSession) => {
      const created = await this.kitchenHubService.transactionCreate(
        payload,
        session
      )
      const { order, store } = created

      const deliveryConfig = await this.deliverySettingService.findOne({
        storeId: store.id
      })
      if (!deliveryConfig?.storeId) {
        throw new BadRequestException("This store ID wasn't in database.")
      }
      const restaurant = await this.restaurantService.findOne({
        _id: deliveryConfig.restaurantId,
        status: StatusEnum.ACTIVE
      })
      if (!restaurant) {
        throw new NotFoundException('Not found restaurant.')
      }
      if (order.status === KitchenHubOrderStatusEnum.NEW) {
        await this.handleNewOrderFromKH(
          payload,
          deliveryConfig.isAutoAccept,
          restaurant,
          session
        )
      }

      return created
    }

    return runTransaction(logic)
  }

  async acceptOrder(payload: AcceptOrderDto) {
    const { orderId, preparationTime } = payload
    const order = await this.thirdPartyOrderService.findOne({
      _id: orderId,
      status: StatusEnum.ACTIVE
    })
    if (!order) throw new NotFoundException('Not found order.')
    if (order.orderStatus !== OrderStateEnum.NEW) {
      throw new BadRequestException(
        `Order status isn't new order, now order status is ${order.orderStatus}`
      )
    }
    const prepareTime = preparationTime
    const { data } = await this.kitchenHubHttpService.acceptOrder(
      order.orderKHId.toString(),
      prepareTime
    )
    if (!data.ok) {
      if (data.error_code === 'order_expired') {
        await this.thirdPartyOrderService.update(orderId, {
          orderStatus: OrderStateEnum.CANCELED
        })
      }
      throw new BadRequestException(data.error_code)
    }

    const logic = async (session: ClientSession) => {
      const updated = await this.thirdPartyOrderService.transactionUpdate(
        orderId,
        {
          orderStatus: OrderStateEnum.IN_PROGRESS,
          prepareTime
        },
        session
      )
      if (!updated) throw new BadRequestException()
      const itemPayload = updated.summaryItems.map((item) => {
        const transform = {
          ...item,
          orderId
        }
        return transform
      })
      const itemsData = await this.orderItemsService.transactionCreateMany(
        itemPayload,
        session
      )
      const ticketNo = await this.queueKitchen(updated.restaurant.id)
      await this.kitchenQueueService.transactionCreate(
        {
          restaurantId: updated.restaurant.id,
          orderId: orderId,
          ticketNo,
          items: itemsData.map((item) => item.id)
        },
        session
      )
      await this.transactionLogic.createThirdPartyOrderTransaction(
        updated,
        session
      )
    }

    await runTransaction(logic)
    return { success: true }
  }

  async rejectOrder(orderId: string) {
    const order = await this.thirdPartyOrderService.findOne({
      _id: orderId,
      status: StatusEnum.ACTIVE
    })
    if (!order) throw new NotFoundException('Not found order.')
    const { data } = await this.kitchenHubHttpService.cancelOrder(
      order.orderKHId.toString()
    )
    if (!data.ok) throw new BadRequestException(data.error_code)
    await this.thirdPartyOrderService.update(orderId, {
      orderStatus: OrderStateEnum.CANCELED
    })
    await this.kitchenQueueService.updateOne(
      { orderId },
      {
        queueStatus: ItemStatusEnum.CANCELED
      }
    )
    return { success: true }
  }

  async completeOrder(orderId: string) {
    const order = await this.thirdPartyOrderService.findOne({
      _id: orderId,
      status: StatusEnum.ACTIVE
    })
    if (!order) throw new NotFoundException('Not found order.')
    const { data } = await this.kitchenHubHttpService.completeOrder(
      order.orderKHId.toString()
    )
    if (!data.ok) throw new BadRequestException(data.error_code)
    await this.thirdPartyOrderService.update(orderId, {
      orderStatus: OrderStateEnum.COMPLETED
    })
    await this.kitchenQueueService.updateOne(
      { orderId },
      {
        queueStatus: ItemStatusEnum.COMPLETED
      }
    )
    await this.orderItemsService.updateMany(
      { orderId },
      { itemStatus: ItemStatusEnum.COMPLETED }
    )

    return { success: true }
  }

  async getOrderByIdForPOS(orderId: string) {
    const order = await this.thirdPartyOrderService.findOne(
      {
        _id: orderId,
        status: StatusEnum.ACTIVE
      },
      {
        // summaryItems: 0,
        createdAt: 0,
        updatedAt: 0,
        status: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    if (!order) throw new NotFoundException('Not found order.')
    const items = await this.orderItemsService.getAll({ orderId: order.id })
    const kitchenQueue = await this.kitchenQueueService.findOne({ orderId })
    const result = {
      ...order.toObject(),
      // items,
      items: items.length > 0 ? items : order.summaryItems,
      customer: {
        id: order.customer?.id || null,
        firstName: order.customer?.firstName,
        lastName: order.customer?.lastName,
        tel: order.customer?.tel
      },
      ticketNo: kitchenQueue?.ticketNo ?? null
    }
    delete result.summaryItems
    return result
  }
}
