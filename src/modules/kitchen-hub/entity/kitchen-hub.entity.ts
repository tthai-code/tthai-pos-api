import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { OrderStateEnum } from 'src/modules/order/enum/order-state.enum'
import {
  OrderMethodEnum,
  OrderTypeEnum
} from 'src/modules/order/enum/order-type.enum'
import { ProviderEnum } from '../common/provider.enum'

class SummaryStatusObject {
  @ApiProperty()
  active: number
  @ApiProperty()
  now: number
  @ApiProperty()
  inProgress: number
  @ApiProperty()
  ready: number
  @ApiProperty()
  upcoming: number
  @ApiProperty()
  completed: number
}

class SummaryTypeObject {
  @ApiProperty()
  delivery: number
  @ApiProperty()
  pickup: number
}

class SummarySourceObject {
  @ApiProperty()
  doordash: number
  @ApiProperty()
  gloriafood: number
  @ApiProperty()
  grubhub: number
  @ApiProperty()
  ubereats: number
}

class ModifiersObject {
  @ApiProperty()
  id: string
  @ApiProperty()
  name: string
  @ApiProperty()
  label: string
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  price: number
}

class ThirdPartyOrderItemObject {
  @ApiProperty()
  id: string
  @ApiProperty()
  name: string
  @ApiProperty()
  unit: number
  @ApiProperty()
  price: number
  @ApiProperty()
  amount: number
  @ApiProperty({ type: [ModifiersObject] })
  modifiers: ModifiersObject[]
  @ApiProperty()
  note: string
}

class StaffObject {
  @ApiProperty()
  id: string
  @ApiProperty()
  first_name: string
  @ApiProperty()
  last_name: string
}

class CustomerObject {
  @ApiProperty()
  tel: string
  @ApiProperty()
  last_name: string
  @ApiProperty()
  first_name: string
}

class AddressObject {
  @ApiProperty()
  zip_code: string
  @ApiProperty()
  state: string
  @ApiProperty()
  city: string
  @ApiProperty()
  address: string
}

class RestaurantObject {
  @ApiProperty()
  email: string
  @ApiProperty()
  business_phone: string
  @ApiProperty()
  address: AddressObject
  @ApiProperty()
  legal_business_name: string
  @ApiProperty()
  id: string
}

class KitchenHubObject {
  @ApiProperty()
  completed_date: string
  @ApiProperty()
  note: string
  @ApiProperty()
  order_k_h_id: number
  @ApiProperty({ enum: Object.values(ProviderEnum) })
  delivery_source: string
  @ApiProperty({
    enum: [
      OrderStateEnum.NEW,
      OrderStateEnum.IN_PROGRESS,
      OrderStateEnum.READY,
      OrderStateEnum.UPCOMING,
      OrderStateEnum.COMPLETED,
      OrderStateEnum.CANCELED
    ]
  })
  order_status: string
  @ApiProperty()
  total: number
  @ApiProperty()
  tips: number
  @ApiProperty()
  convenience_fee: number
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
  @ApiProperty()
  service_charge: number
  @ApiProperty()
  discount: number
  @ApiProperty()
  subtotal: number
  @ApiProperty({ type: [ThirdPartyOrderItemObject] })
  summary_items: ThirdPartyOrderItemObject[]
  @ApiProperty()
  scheduled_for_date: string
  @ApiProperty()
  asap: boolean
  @ApiProperty()
  pick_up_date: string
  @ApiProperty()
  order_date: string
  @ApiProperty()
  number_of_guest: number
  @ApiProperty({ enum: Object.values(OrderMethodEnum) })
  order_method: string
  @ApiProperty({ enum: Object.values(OrderTypeEnum) })
  order_type: string
  @ApiProperty({ type: StaffObject })
  staff: StaffObject
  @ApiProperty({ type: CustomerObject })
  customer: CustomerObject
  @ApiProperty({ type: RestaurantObject })
  restaurant: RestaurantObject
  @ApiProperty()
  id: string
  @ApiProperty()
  ticket_no: string
  @ApiProperty()
  prepare_time: number
}

class PaginateKitchenHubOrder extends PaginateResponseDto<KitchenHubObject[]> {
  @ApiProperty({
    example: {
      active: 6,
      new: 0,
      inProgress: 0,
      ready: 0,
      upcoming: 0,
      completed: 0
    }
  })
  status: SummaryStatusObject

  @ApiProperty({
    example: {
      delivery: 0,
      pickup: 6
    }
  })
  type: SummaryTypeObject

  @ApiProperty({
    example: {
      doordash: 0,
      gloriafood: 6,
      grubhub: 0,
      ubereats: 0
    }
  })
  source: SummarySourceObject

  @ApiProperty({
    type: [KitchenHubObject],
    example: {
      completed_date: null,
      prepare_time: 44,
      note: '',
      order_k_h_id: 5191520877805568,
      delivery_source: 'gloriafood',
      order_status: 'UPCOMING',
      total: 3.11,
      tips: 0,
      convenience_fee: 0,
      alcohol_tax: 0,
      tax: 0.12,
      service_charge: 0,
      discount: 0,
      subtotal: 2.99,
      summary_items: [
        {
          id: '2487615',
          name: '20 oz. Bottled Soda',
          unit: 1,
          price: 2.99,
          amount: 2.99,
          modifiers: [
            {
              id: '7241500',
              name: 'Very very long long long option name to test multiline',
              label: 'Choose a drink',
              abbreviation: 'C',
              native_name: 'Choose a drink',
              price: 0
            }
          ],
          note: null
        }
      ],
      scheduled_for_date: null,
      asap: true,
      pick_up_date: '2022-12-01T14:53:34.000Z',
      order_date: '2022-12-01T14:09:34.000Z',
      number_of_guest: 1,
      order_method: 'PICKUP',
      order_type: 'THIRD_PARTY',
      staff: null,
      customer: {
        tel: '6262145123',
        last_name: 'Test',
        first_name: 'Tthai'
      },
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      id: 'jQxRHeHt',
      ticket_no: '8'
    }
  })
  results: KitchenHubObject[]
}

export class PaginateKitchenHubResponse extends ResponseDto<PaginateKitchenHubOrder> {
  @ApiProperty()
  data: PaginateKitchenHubOrder
}

class GetOrderByIdObject extends KitchenHubObject {
  @ApiProperty()
  ticket_no: string
}

export class GetThirdPartyOrderByIdResponse extends ResponseDto<GetOrderByIdObject> {
  @ApiProperty({
    type: GetOrderByIdObject,
    example: {
      completed_date: null,
      note: '',
      prepare_time: 44,
      order_k_h_id: 5191520877805568,
      delivery_source: 'gloriafood',
      order_status: 'UPCOMING',
      total: 3.11,
      tips: 0,
      convenience_fee: 0,
      alcohol_tax: 0,
      tax: 0.12,
      service_charge: 0,
      discount: 0,
      subtotal: 2.99,
      scheduled_for_date: null,
      asap: true,
      pick_up_date: '2022-12-01T14:53:34.000Z',
      order_date: '2022-12-01T14:09:34.000Z',
      number_of_guest: 1,
      order_method: 'PICKUP',
      order_type: 'THIRD_PARTY',
      staff: null,
      customer: {
        id: null,
        first_name: 'Tthai',
        last_name: 'Test',
        tel: '6262145123'
      },
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      id: 'jQxRHeHt',
      items: [
        {
          item_status: 'COMPLETED',
          note: null,
          amount: 2.99,
          unit: 1,
          modifiers: [
            {
              id: '7241500',
              name: 'Very very long long long option name to test multiline',
              label: 'Choose a drink',
              abbreviation: 'C',
              native_name: 'Choose a drink',
              price: 0
            }
          ],
          price: 2.99,
          name: '20 oz. Bottled Soda',
          order_id: 'jQxRHeHt',
          id: '6388b5d215af8c5c0cf0721b'
        }
      ],
      ticket_no: '8'
    }
  })
  data: GetOrderByIdObject
}
