import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { ProviderEnum } from '../common/provider.enum'

class ProviderObject {
  @ApiProperty({ enum: Object.values(ProviderEnum) })
  provider: string

  @ApiProperty()
  is_connected: boolean

  @ApiProperty()
  online: boolean

  @ApiProperty()
  can_pause: boolean
}

class DeliverySettingObject {
  @ApiProperty()
  is_auto_accept: boolean

  @ApiProperty()
  restaurant_id: string

  @ApiProperty()
  online_count: number

  @ApiProperty({ type: [ProviderObject] })
  providers: ProviderObject[]
}

export class GetDeliverySettingResponse extends ResponseDto<DeliverySettingObject> {
  @ApiProperty({
    type: DeliverySettingObject,
    example: {
      is_auto_accept: true,
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      online_count: 1,
      providers: [
        {
          provider: 'doordash',
          is_connected: false,
          online: false,
          can_pause: true
        },
        {
          provider: 'grubhub',
          is_connected: false,
          online: false,
          can_pause: true
        },
        {
          provider: 'ubereats',
          is_connected: false,
          online: false,
          can_pause: true
        },
        {
          provider: 'gloriafood',
          is_connected: true,
          online: true,
          can_pause: false
        }
      ]
    }
  })
  data: DeliverySettingObject
}
