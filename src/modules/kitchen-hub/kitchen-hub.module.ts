import { HttpModule } from '@nestjs/axios'
import { forwardRef, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'
import { OrderModule } from '../order/order.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { SocketModule } from '../socket/socket.module'
import { TransactionModule } from '../transaction/transaction.module'
import { TThaiAppModule } from '../tthai-app/tthai-app.module'
import { OpenKitchenHubController } from './controllers/open-kitchen-hub.controller'
import { POSDeliverySettingController } from './controllers/pos-delivery-setting.controller'
import { POSKitchenHubController } from './controllers/pos-kitchen-hub.controller'
import { PublicQueueController } from './controllers/public-queue.controller'
import { DeliverySettingLogic } from './logic/delivery-setting.logic'
import { KitchenHubLogic } from './logic/kitchen-hub.logic'
import { POSKitchenHubLogic } from './logic/pos-kitchen-hub.logic'
import { DeliverySettingSchema } from './schemas/delivery-setting.schema'
import { KitchenHubSchema } from './schemas/kitchen-hub.schema'
import { DeliverySettingService } from './services/delivery-setting.service'
import { KitchenHubHttpService } from './services/kitchen-hub-http.service'
import { KitchenHubService } from './services/kitchen-hub.service'

@Module({
  imports: [
    HttpModule,
    ConfigModule,
    forwardRef(() => RestaurantModule),
    forwardRef(() => TransactionModule),
    forwardRef(() => SocketModule),
    forwardRef(() => OrderModule),
    forwardRef(() => TThaiAppModule),
    MongooseModule.forFeature([
      { name: 'kitchenHub', schema: KitchenHubSchema },
      { name: 'deliverySetting', schema: DeliverySettingSchema }
    ])
  ],
  providers: [
    KitchenHubService,
    DeliverySettingService,
    DeliverySettingLogic,
    KitchenHubHttpService,
    KitchenHubLogic,
    POSKitchenHubLogic
  ],
  controllers: [
    OpenKitchenHubController,
    POSDeliverySettingController,
    POSKitchenHubController,
    PublicQueueController
  ],
  exports: [KitchenHubHttpService, DeliverySettingService]
})
export class KitchenHubModule {}
