import { ApiProperty } from '@nestjs/swagger'
import { IsBoolean, IsIn, IsNotEmpty } from 'class-validator'
import { ProviderEnum } from '../common/provider.enum'

export class UpdateAutoAcceptDto {
  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty({ example: true })
  readonly isAutoAccept: boolean
}

const providerEnum = Object.values(ProviderEnum).filter(
  (item) => item !== ProviderEnum.GLORIAFOOD
)
export class PauseProviderDto {
  @IsIn(providerEnum)
  @ApiProperty({
    enum: providerEnum,
    example: ProviderEnum.DOORDASH
  })
  provider: string

  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty({ example: false })
  online: boolean
}
