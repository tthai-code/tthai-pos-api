import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsBoolean,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator'
import { KitchenHubOrderStatusEnum } from '../common/kitchen-order-status.enum'
import { KitchenHubOrderTypeEnum } from '../common/kitchen-order-type.enum'

export class OrderObjectDto {
  @IsNumber()
  @ApiProperty()
  id: number

  @IsString()
  @ApiProperty()
  externalId: string

  @IsString()
  @ApiProperty()
  number: string

  @IsNumber()
  @ApiProperty()
  dailyNumber: number

  @IsEnum(KitchenHubOrderTypeEnum)
  @ApiProperty({ enum: Object.values(KitchenHubOrderTypeEnum) })
  type: string

  @IsEnum(KitchenHubOrderStatusEnum)
  @ApiProperty({ enum: Object.values(KitchenHubOrderStatusEnum) })
  status: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  notes: string

  @IsBoolean()
  @ApiProperty()
  paid: boolean

  @IsBoolean()
  @ApiProperty()
  asap: boolean

  @IsOptional()
  @IsBoolean()
  @ApiPropertyOptional()
  addDisposableItems: boolean

  @IsNumber()
  @IsOptional()
  @ApiPropertyOptional()
  prepTimeMinutes: number
}

export class ProviderObjectDto {
  @IsString()
  @ApiProperty()
  id: string

  @IsString()
  @ApiProperty()
  name: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  merchantId: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  storeId: string
}

export class LocationObjectDto {
  @IsString()
  @ApiProperty()
  id: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  partnerLocationId: string

  @IsString()
  @ApiProperty()
  name: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  street: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  city: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  state: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  zipCode: string

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional()
  lat: number

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional()
  lon: number
}

export class StoreObjectDto {
  @IsString()
  @ApiProperty()
  id: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  partnerStoreId: string

  @IsOptional()
  @IsString()
  @ApiProperty()
  name: string
}

export class CustomerObjectDto {
  @IsString()
  @ApiProperty()
  name: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  email: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  phoneNumber: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  phoneCode: string
}

export class OptionObjectDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  id: string

  @IsString()
  @ApiProperty()
  name: string

  @IsNumber()
  @ApiProperty()
  quantity: number

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  basePrice: string

  @IsString()
  @ApiProperty()
  price: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  modifierName: string
}

export class ItemObjectDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  id: string

  @IsString()
  @ApiProperty()
  name: string

  @IsNumber()
  @ApiProperty()
  quantity: number

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  basePrice: string

  @IsString()
  @ApiProperty()
  price: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  instructions: string

  @Type(() => OptionObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: [OptionObjectDto] })
  @ApiProperty()
  options: OptionObjectDto[]
}

export class AddressObjectDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  country: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  street: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  city: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  state: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  zipCode: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  unitNumber: string

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional()
  latitude: number

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional()
  longitude: number
}

export class CourierObjectDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  name: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  photoUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  phoneNumber: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  status: string
}

export class DeliveryObjectDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  type: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  status: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  notes: string

  @Type(() => AddressObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: AddressObjectDto })
  @ApiProperty()
  address: AddressObjectDto

  @Type(() => CourierObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: CourierObjectDto })
  @ApiProperty()
  courier: CourierObjectDto
}

export class PaymentObjectDto {
  @IsString()
  @ApiProperty()
  method: string
}

export class ChargesObjectDto {
  @IsString()
  @ApiProperty()
  subtotal: string

  @IsString()
  @ApiProperty()
  tax: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  tips: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  discount: string

  @IsString()
  @ApiProperty()
  total: string
}

export class TimestampsObjectDto {
  @IsString()
  @ApiProperty()
  placedAt: string

  @IsString()
  @ApiProperty()
  createdAt: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  acceptedAt: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  cancelledAt: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  completedAt: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional()
  pickupAt: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  deliveryAt: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  scheduledFor: string
}

export class CreateKitchenHubOrderDto {
  @Type(() => OrderObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: OrderObjectDto })
  order: OrderObjectDto

  @Type(() => ProviderObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: ProviderObjectDto })
  provider: ProviderObjectDto

  @Type(() => LocationObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: LocationObjectDto })
  location: LocationObjectDto

  @Type(() => StoreObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: StoreObjectDto })
  store: any

  @Type(() => CustomerObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: CustomerObjectDto })
  customer: CustomerObjectDto

  @Type(() => ItemObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: [ItemObjectDto] })
  items: ItemObjectDto[]

  @Type(() => DeliveryObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: DeliveryObjectDto })
  delivery: DeliveryObjectDto

  @Type(() => PaymentObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: PaymentObjectDto })
  payment: PaymentObjectDto

  @Type(() => ChargesObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: ChargesObjectDto })
  charges: ChargesObjectDto

  @Type(() => TimestampsObjectDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: TimestampsObjectDto })
  timestamps: TimestampsObjectDto
}
