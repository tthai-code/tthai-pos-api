import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator'

export class AcceptOrderDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<order-id>' })
  readonly orderId: string

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({
    default: 15,
    example: 15,
    description: 'preparation time in minute'
  })
  readonly preparationTime: number = 15
}
