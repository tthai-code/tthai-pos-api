import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Patch,
  Request,
  UseGuards
} from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { errorResponse } from 'src/utilities/errorResponse'
import { PauseProviderDto, UpdateAutoAcceptDto } from '../dto/update-auto.dto'
import { GetDeliverySettingResponse } from '../entity/get-delivery-setting.entity'
import { DeliverySettingLogic } from '../logic/delivery-setting.logic'
import { DeliverySettingService } from '../services/delivery-setting.service'

@ApiBearerAuth()
@ApiTags('pos/delivery-setting')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/delivery-setting')
export class POSDeliverySettingController {
  constructor(
    private readonly deliverySettingService: DeliverySettingService,
    private readonly deliverySettingLogic: DeliverySettingLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => GetDeliverySettingResponse })
  @ApiOperation({
    summary: 'Get Delivery Setting (Auto Accept)',
    description: 'use *bearer* `staff_token`'
  })
  async getDeliverySetting(@Request() request: any) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const deliverySetting = await this.deliverySettingLogic.getDeliverySetting(
      restaurantId
    )
    await this.logLogic.createLogic(
      request,
      'Get Delivery Setting (Auto Accept)',
      deliverySetting
    )
    return deliverySetting
  }

  @Patch('/auto')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Update Delivery Setting (Auto Accept)',
    description: 'use *bearer* `staff_token`'
  })
  async updateAutoAccept(@Body() payload: UpdateAutoAcceptDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const updated = await this.deliverySettingService.updateOne(
      { restaurantId },
      payload
    )
    if (!updated) throw new BadRequestException()
    return { success: true }
  }

  @Patch('/pause')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiBadRequestResponse(
    errorResponse(400, 'provider_not_found', '2022-11-17T11:03:25.372Z')
  )
  @ApiOperation({
    summary: 'Pause Provider',
    description: 'use *bearer* `staff_token`'
  })
  async pauseProvider(
    @Body() payload: PauseProviderDto,
    @Request() request: any
  ) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const updated = await this.deliverySettingLogic.pauseProvider(
      restaurantId,
      payload
    )
    await this.logLogic.createLogic(request, 'Pause Provider', updated)
    return updated
  }
}
