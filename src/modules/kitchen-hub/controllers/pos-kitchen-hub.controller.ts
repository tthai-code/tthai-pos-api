import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Query,
  Request,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { ThirdPartyOrderPaginateDto } from 'src/modules/order/dto/get-third-party-order.dto'
import { OrderStateEnum } from 'src/modules/order/enum/order-state.enum'
import { ThirdPartyOrderService } from 'src/modules/order/services/third-party-order.service'
import { AcceptOrderDto } from '../dto/accept-order.dto'
import {
  GetThirdPartyOrderByIdResponse,
  PaginateKitchenHubResponse
} from '../entity/kitchen-hub.entity'
import { KitchenHubLogic } from '../logic/kitchen-hub.logic'
import { POSKitchenHubLogic } from '../logic/pos-kitchen-hub.logic'

@ApiBearerAuth()
@ApiTags('pos/kitchen-hub')
@UseGuards(StaffAuthGuard)
@Controller('/v1/pos/kitchen-hub')
export class POSKitchenHubController {
  constructor(
    private readonly kitchenHubLogic: KitchenHubLogic,
    private readonly thirdPartyOrderService: ThirdPartyOrderService,
    private readonly posKitchenHubLogic: POSKitchenHubLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaginateKitchenHubResponse })
  @ApiOperation({
    summary: 'Get 3rd party order list',
    description: 'use bearer `staff_token`'
  })
  async getAllOrder(
    @Query() query: ThirdPartyOrderPaginateDto,
    @Request() request: any
  ) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const results = await this.posKitchenHubLogic.getAllOrder(
      restaurantId,
      query
    )
    await this.logLogic.createLogic(
      request,
      'Get 3rd party order list',
      results
    )
    return results
  }

  @Get(':orderId')
  @ApiOkResponse({ type: () => GetThirdPartyOrderByIdResponse })
  @ApiOperation({
    summary: 'Get 3rd party Order Detail by ID',
    description: 'use bearer `staff_token`'
  })
  async getOrderById(@Param('orderId') orderId: string) {
    return await this.kitchenHubLogic.getOrderByIdForPOS(orderId)
  }

  @Patch('accept-order')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Accept 3rd party order/Mark in progress',
    description: 'use bearer `staff_token`'
  })
  async acceptOrder(@Body() payload: AcceptOrderDto) {
    return await this.kitchenHubLogic.acceptOrder(payload)
  }

  @Patch('cancel-order')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Cancel 3rd party order',
    description: 'use bearer `staff_token`'
  })
  async cancelOrder(@Body() payload: AcceptOrderDto) {
    return await this.kitchenHubLogic.rejectOrder(payload.orderId)
  }

  @Patch('ready-order')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Ready 3rd party order',
    description: 'use bearer `staff_token`'
  })
  async readyOrder(@Body() payload: AcceptOrderDto) {
    await this.thirdPartyOrderService.update(payload.orderId, {
      orderStatus: OrderStateEnum.READY
    })
    return { success: true }
  }

  @Patch('upcoming-order')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Upcoming 3rd party order',
    description: 'use bearer `staff_token`'
  })
  async upcomingOrder(@Body() payload: AcceptOrderDto) {
    await this.thirdPartyOrderService.update(payload.orderId, {
      orderStatus: OrderStateEnum.UPCOMING
    })
    return { success: true }
  }

  @Patch('complete-order')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Complete 3rd party order',
    description: 'use bearer `staff_token`'
  })
  async completeOrder(@Body() payload: AcceptOrderDto) {
    return await this.kitchenHubLogic.completeOrder(payload.orderId)
  }
}
