import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiHeaders, ApiOperation, ApiTags } from '@nestjs/swagger'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { CreateKitchenHubOrderDto } from '../dto/create-kitchen-hub.dto'
import { KitchenHubLogic } from '../logic/kitchen-hub.logic'
import { KitchenHubService } from '../services/kitchen-hub.service'

@ApiHeaders([{ name: 'x-api-key', required: true }])
@ApiTags('open/kitchen-hub')
@UseGuards(AuthGuard('api-key'))
@Controller('v1/open/kitchen-hub')
export class OpenKitchenHubController {
  constructor(
    private readonly kitchenHubService: KitchenHubService,
    private readonly kitchenHubLogic: KitchenHubLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Post()
  @ApiOperation({ summary: 'Receive Kitchen Hub Notify Order' })
  async receiveOrder(
    @Body() payload: CreateKitchenHubOrderDto,
    @Request() req: any
  ) {
    const created = await this.kitchenHubLogic.receiveOrderFromKH(payload)
    await this.logLogic.createLogic(req, 'Receive Kitchen Hub Order', {
      created
    })
    return { success: true }
  }
}
