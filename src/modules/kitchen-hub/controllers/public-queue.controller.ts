import { Controller, Get, Query } from '@nestjs/common'
import { ApiOperation, ApiProperty } from '@nestjs/swagger'
import { IsNumberString, IsString } from 'class-validator'
import { ItemStatusEnum } from 'src/modules/order/enum/item-status.enum'
import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service'
import { TThaiAppLogic } from 'src/modules/tthai-app/logics/tthai-app.logic'

class CheckQueueDto {
  @IsString()
  @ApiProperty()
  restaurantId: string

  @IsNumberString()
  @ApiProperty()
  item: number
}

@Controller('v1/public')
export class PublicQueueController {
  constructor(
    private readonly kitchenQueueService: KitchenQueueService,
    private readonly tthaiAppLogic: TThaiAppLogic
  ) {}

  @Get('queue')
  @ApiOperation({ summary: '', description: 'check queue prepare time' })
  async checkPrepareTime(@Query() query: CheckQueueDto) {
    const tthaiAppSetting = await this.tthaiAppLogic.getTThaiApp(
      query.restaurantId
    )
    const { waitTime } = tthaiAppSetting
    const queue = await this.kitchenQueueService.getAll({
      restaurantId: query.restaurantId,
      queueStatus: ItemStatusEnum.IN_PROGRESS
    })
    const length = queue.length || 0
    let prepareTime =
      waitTime.baseTime +
      length * waitTime.perOrderTime +
      +query.item * waitTime.perItemTime
    if (prepareTime > waitTime.maxTime) {
      prepareTime = waitTime.maxTime
    }
    return { prepareTime, length, waitTime }
  }
}
