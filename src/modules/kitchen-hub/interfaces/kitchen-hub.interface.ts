export interface ISetProviderStatus {
  online: boolean
  pause_duration_seconds?: number | null
  provider_id: string
  store_id: string
}

export interface IAcceptOrder {
  order_id: string
  prep_time_minutes: number
}

export interface IAuthProvider {
  login?: string
  password?: string
  provider_id: string
  protocol?: string
}

export interface IConnectProvider {
  login?: string
  password?: string
  provider_id: string
  provider_store_id?: string
  store_id: string
}

export interface IDisconnectProvider {
  provider_id: string
  store_id: string
}
