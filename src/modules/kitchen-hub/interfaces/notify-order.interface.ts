import { ICustomerObjectOrderPayload } from 'src/modules/order/interfaces/third-party-order.interfaces'

export interface IKitchenHubOrderNotify {
  id?: string
  kitchenHubId?: number | string
  customer?: ICustomerObjectOrderPayload
  type?: string
  placedDate?: Date | string
  source?: string
  orderStatus?: string
}
