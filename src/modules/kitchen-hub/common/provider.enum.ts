export enum ProviderEnum {
  DOORDASH = 'doordash',
  GRUBHUB = 'grubhub',
  UBEREATS = 'ubereats',
  GLORIAFOOD = 'gloriafood'
}
