export enum KitchenHubOrderStatusEnum {
  NEW = 'new',
  ACCEPTED = 'accepted',
  COMPLETED = 'completed',
  CANCELLED = 'cancelled'
}
