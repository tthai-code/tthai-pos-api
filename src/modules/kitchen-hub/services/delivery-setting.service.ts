import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { DeliverySettingDocument } from '../schemas/delivery-setting.schema'

@Injectable({ scope: Scope.REQUEST })
export class DeliverySettingService {
  constructor(
    @InjectModel('deliverySetting')
    private readonly DeliverySettingModel: Model<DeliverySettingDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<DeliverySettingDocument | any> {
    return this.DeliverySettingModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.DeliverySettingModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.DeliverySettingModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<DeliverySettingDocument> {
    return this.DeliverySettingModel
  }

  async getSession(): Promise<ClientSession> {
    await this.DeliverySettingModel.createCollection()

    return await this.DeliverySettingModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<DeliverySettingDocument> {
    const document = new this.DeliverySettingModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.DeliverySettingModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<DeliverySettingDocument> {
    const document = new this.DeliverySettingModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<DeliverySettingDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(
    condition: any,
    payload: any
  ): Promise<DeliverySettingDocument> {
    return this.DeliverySettingModel.updateOne(condition, { ...payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<DeliverySettingDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async delete(id: string): Promise<DeliverySettingDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.DeliverySettingModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.DeliverySettingModel.findById(id)
  }

  findOne(condition, project?: any): Promise<DeliverySettingDocument> {
    return this.DeliverySettingModel.findOne(condition, project)
  }
}
