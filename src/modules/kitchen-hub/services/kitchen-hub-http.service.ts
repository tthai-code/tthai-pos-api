import { HttpService } from '@nestjs/axios'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { REQUEST } from '@nestjs/core'
import {
  IAcceptOrder,
  IAuthProvider,
  IConnectProvider,
  IDisconnectProvider,
  ISetProviderStatus
} from '../interfaces/kitchen-hub.interface'

@Injectable({ scope: Scope.REQUEST })
export class KitchenHubHttpService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    @Inject(REQUEST) private request: any
  ) {}

  private readonly url = this.configService.get<string>('KITCHEN_HUB_API_URL')
  private readonly key = this.configService.get<string>('KITCHEN_HUB_API_KEY')
  private readonly locationId = this.configService.get<string>(
    'KITCHEN_HUB_LOCATION_ID'
  )

  private readonly options = {
    headers: {
      'x-api-key': this.key
    }
  }

  async ping(): Promise<any> {
    return this.httpService
      .post(`${this.url}/ping`, null, this.options)
      .toPromise()
  }

  async getLocation(locationId: string = this.locationId): Promise<any> {
    return this.httpService
      .post(
        `${this.url}/getLocation`,
        {
          location_id: locationId
        },
        this.options
      )
      .toPromise()
  }

  async createStore(
    storeName: string,
    locationId: string = this.locationId,
    partnerStoreId?: string
  ): Promise<any> {
    return this.httpService
      .post(
        `${this.url}/createStore`,
        {
          store_name: storeName,
          location_id: locationId,
          partner_store_id: partnerStoreId || null
        },
        this.options
      )
      .toPromise()
  }

  async authProvider(payload: IAuthProvider): Promise<any> {
    return this.httpService
      .post(`${this.url}/authProvider`, payload, this.options)
      .toPromise()
  }

  async connectProvider(payload: IConnectProvider): Promise<any> {
    return this.httpService
      .post(`${this.url}/connectProvider`, payload, this.options)
      .toPromise()
  }

  async disconnectProvider(payload: IDisconnectProvider): Promise<any> {
    return this.httpService
      .post(`${this.url}/disconnectProvider`, payload, this.options)
      .toPromise()
  }

  async getStoreProviders(storeId: string): Promise<any> {
    return this.httpService
      .post(
        `${this.url}/getStoreProviders`,
        { store_id: storeId },
        this.options
      )
      .toPromise()
  }

  async getProviderStatus(providerId: string, storeId: string): Promise<any> {
    return this.httpService
      .post(
        `${this.url}/getProviderStatus`,
        { provider_id: providerId, store_id: storeId },
        this.options
      )
      .toPromise()
  }

  async setProviderStatus(payload: ISetProviderStatus): Promise<any> {
    return this.httpService
      .post(
        `${this.url}/setProviderStatus`,
        {
          online: true,
          ...payload
        },
        this.options
      )
      .toPromise()
  }

  async acceptOrder(orderId: string, prepTime: number): Promise<any> {
    const payload: IAcceptOrder = {
      order_id: orderId,
      prep_time_minutes: prepTime
    }
    return this.httpService
      .post(`${this.url}/acceptOrder`, payload, this.options)
      .toPromise()
  }

  async cancelOrder(orderId: string): Promise<any> {
    return this.httpService
      .post(`${this.url}/cancelOrder`, { order_id: orderId }, this.options)
      .toPromise()
  }

  async completeOrder(orderId: string): Promise<any> {
    return this.httpService
      .post(`${this.url}/completeOrder`, { order_id: orderId }, this.options)
      .toPromise()
  }
}
