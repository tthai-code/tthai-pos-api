import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { KitchenHubDocument } from '../schemas/kitchen-hub.schema'

@Injectable({ scope: Scope.REQUEST })
export class KitchenHubService {
  constructor(
    @InjectModel('kitchenHub')
    private readonly KitchenHubModel: PaginateModel<KitchenHubDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<KitchenHubDocument | any> {
    return this.KitchenHubModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.KitchenHubModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.KitchenHubModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<KitchenHubDocument> {
    return this.KitchenHubModel
  }

  async getSession(): Promise<ClientSession> {
    await this.KitchenHubModel.createCollection()

    return await this.KitchenHubModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<KitchenHubDocument> {
    const document = new this.KitchenHubModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.KitchenHubModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<KitchenHubDocument> {
    const document = new this.KitchenHubModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<KitchenHubDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<KitchenHubDocument> {
    return this.KitchenHubModel.updateOne(condition, { ...payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<KitchenHubDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async delete(id: string): Promise<KitchenHubDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.KitchenHubModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.KitchenHubModel.findById(id)
  }

  findOne(condition, project?: any): Promise<KitchenHubDocument> {
    return this.KitchenHubModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: any,
    select?: any
  ): Promise<PaginateResult<KitchenHubDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.KitchenHubModel.paginate(query, options)
  }
}
