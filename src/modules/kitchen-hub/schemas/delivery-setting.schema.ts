import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type DeliverySettingDocument = DeliverySettingField & Document

@Schema({ timestamps: true, strict: true, collection: 'deliverySettings' })
export class DeliverySettingField {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ default: null })
  storeId: string

  @Prop({ default: true })
  isAutoAccept: boolean

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const DeliverySettingSchema =
  SchemaFactory.createForClass(DeliverySettingField)
DeliverySettingSchema.plugin(authorStampCreatePlugin)
