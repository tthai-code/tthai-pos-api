import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { KitchenHubOrderStatusEnum } from '../common/kitchen-order-status.enum'
import { KitchenHubOrderTypeEnum } from '../common/kitchen-order-type.enum'

export type KitchenHubDocument = KitchenHubField & Document

@Schema({ _id: false, strict: true, timestamps: false })
class OrderObjectField {
  @Prop({ default: null })
  id: number

  @Prop({ default: null })
  externalId: string

  @Prop({ default: null })
  number: string

  @Prop({ default: null })
  dailyNumber: number

  @Prop({ enum: Object.values(KitchenHubOrderTypeEnum) })
  type: string

  @Prop({ enum: Object.values(KitchenHubOrderStatusEnum) })
  status: string

  @Prop({ default: null })
  notes: string

  @Prop({ default: null })
  paid: boolean

  @Prop({ default: null })
  asap: boolean

  @Prop({ default: null })
  addDisposableItems: boolean

  @Prop({ default: null })
  prepTimeMinutes: number
}

@Schema({ _id: false, strict: true, timestamps: false })
class ProviderField {
  @Prop({ default: null })
  id: string

  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  merchantId: string

  @Prop({ default: null })
  storeId: string
}

@Schema({ _id: false, strict: true, timestamps: false })
class LocationField {
  @Prop({ default: null })
  id: string

  @Prop({ default: null })
  partnerLocationId: string

  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  street: string

  @Prop({ default: null })
  city: string

  @Prop({ default: null })
  state: string

  @Prop({ default: null })
  zipCode: string

  @Prop({ default: null })
  lat: number

  @Prop({ default: null })
  lon: number
}

@Schema({ _id: false, strict: true, timestamps: false })
class StoreField {
  @Prop({ default: null })
  id: string

  @Prop({ default: null })
  partnerStoreId: string

  @Prop({ default: null })
  name: string
}

@Schema({ _id: false, strict: true, timestamps: false })
class CustomerField {
  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  email: string

  @Prop({ default: null })
  phoneNumber: string

  @Prop({ default: null })
  phoneCode: string
}

@Schema({ _id: false, strict: true, timestamps: false })
class OptionObjectField {
  @Prop({ default: null })
  id: string

  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  basePrice: string

  @Prop({ default: null })
  quantity: number

  @Prop({ default: null })
  price: string

  @Prop({ default: null })
  modifierName: string
}

@Schema({ _id: false, strict: true, timestamps: false })
class ItemObjectField {
  @Prop({ default: null })
  id: string

  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  quantity: number

  @Prop({ default: null })
  basePrice: string

  @Prop({ default: null })
  price: string

  @Prop({ default: null })
  instructions: string

  @Prop({ default: null })
  options: OptionObjectField[]
}

@Schema({ _id: false, strict: true, timestamps: false })
class AddressObjectField {
  @Prop({ default: null })
  country: string

  @Prop({ default: null })
  street: string

  @Prop({ default: null })
  city: string

  @Prop({ default: null })
  state: string

  @Prop({ default: null })
  zipCode: string

  @Prop({ default: null })
  unitNumber: string

  @Prop({ default: null })
  latitude: number

  @Prop({ default: null })
  longitude: number
}

@Schema({ _id: false, strict: true, timestamps: false })
class CourierObjectField {
  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  photoUrl: string

  @Prop({ default: null })
  phoneNumber: string

  @Prop({ default: null })
  status: string
}

@Schema({ _id: false, strict: true, timestamps: false })
class DeliveryObjectField {
  @Prop({ default: null })
  type: string

  @Prop({ default: null })
  status: string

  @Prop({ default: null })
  notes: string

  @Prop({ default: null })
  address: AddressObjectField

  @Prop({ default: null })
  courier: CourierObjectField
}

@Schema({ _id: false, strict: true, timestamps: false })
class PaymentObjectField {
  @Prop({ default: null })
  method: string
}

@Schema({ _id: false, strict: true, timestamps: false })
class ChargesObjectField {
  @Prop({ default: null })
  subtotal: string

  @Prop({ default: null })
  tax: string

  @Prop({ default: null })
  tips: string

  @Prop({ default: null })
  discount: string

  @Prop({ default: null })
  total: string
}

@Schema({ _id: false, strict: true, timestamps: false })
class TimestampsObjectField {
  @Prop({ default: null })
  placedAt: string

  @Prop({ default: null })
  createdAt: string

  @Prop({ default: null })
  acceptedAt: string

  @Prop({ default: null })
  cancelledAt: string

  @Prop({ default: null })
  completedAt: string

  @Prop({ default: null })
  pickupAt: string

  @Prop({ default: null })
  deliveryAt: string

  @Prop({ default: null })
  scheduledFor: string
}

@Schema({ timestamps: true, strict: true, collection: 'kitchenHub' })
export class KitchenHubField {
  @Prop({ default: null })
  order: OrderObjectField

  @Prop({ default: null })
  provider: ProviderField

  @Prop({ default: null })
  location: LocationField

  @Prop({ default: null })
  store: StoreField

  @Prop({ default: null })
  customer: CustomerField

  @Prop({ default: null })
  items: ItemObjectField[]

  @Prop({ default: null })
  delivery: DeliveryObjectField

  @Prop({ default: null })
  payment: PaymentObjectField

  @Prop({ default: null })
  charges: ChargesObjectField

  @Prop({ default: null })
  timestamps: TimestampsObjectField

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string
}
export const KitchenHubSchema = SchemaFactory.createForClass(KitchenHubField)
KitchenHubSchema.plugin(mongoosePaginate)
