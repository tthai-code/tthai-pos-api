import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AuthLogic } from 'src/modules/auth/logics/auth.logic'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { BankAccountStatusEnum } from 'src/modules/payment-gateway/common/ach-bank.type.enum'
import { PaymentGatewayStatusEnum } from 'src/modules/payment-gateway/common/payment-gateway.enum'
import { BankAccountService } from 'src/modules/payment-gateway/services/bank-account.service'
import { PaymentGatewayService } from 'src/modules/payment-gateway/services/payment-gateway.service'
import { RestaurantHolidaysService } from 'src/modules/restaurant-holiday/services/restaurant-holiday.service'
import { SubscriptionStatusEnum } from 'src/modules/subscription/common/subscription-plan.enum'
import { PackageService } from 'src/modules/subscription/services/package.service'
import { SubscriptionService } from 'src/modules/subscription/services/subscription.service'
import { CreateRestaurantDto } from '../dto/create-restaurant.dto'
import { UpdateRestaurantProfile } from '../dto/update-restaurant-profile.dto'
import { RestaurantService } from '../services/restaurant.service'

@Injectable()
export class RestaurantLogic {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly restaurantHolidaysService: RestaurantHolidaysService,
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly bankAccountService: BankAccountService,
    private readonly subscriptionService: SubscriptionService,
    private readonly packageService: PackageService,
    private readonly authLogic: AuthLogic
  ) {}

  private splitName(name: string) {
    const str = name.split(' ')
    const firstName = str[0]
    str.splice(0, 1)
    const lastName = str.join(' ')
    return { firstName, lastName }
  }

  async CreateLogic(payload: CreateRestaurantDto, username: string) {
    const logic = async (session: ClientSession) => {
      if (!username) throw new NotFoundException()
      const restaurant = await this.restaurantService.findOne({
        accountId: payload.accountId,
        status: StatusEnum.ACTIVE
      })
      if (restaurant)
        throw new BadRequestException(
          'This account has restaurant profile already.'
        )
      const { firstName, lastName } = this.splitName(
        payload.authorizedPersonName
      )
      payload.authorizedPersonFirstName = firstName
      payload.authorizedPersonLastName = lastName
      delete payload.authorizedPersonName
      const created = await this.restaurantService.transactionCreate(
        payload,
        session
      )
      if (!created) throw new BadRequestException()
      const holidays = await this.restaurantHolidaysService.transactionCreate(
        { restaurantId: created._id, holidays: [] },
        session
      )
      if (!holidays) throw new BadRequestException()
      const payment = await this.paymentGatewayService.transactionCreate(
        { restaurantId: created._id },
        session
      )
      if (!payment) throw new BadRequestException()
    }
    await runTransaction(logic)
    const result = await this.authLogic.RestaurantLoginAfterCreated(username)
    return result
  }

  async updateRestaurantProfile(id: string, payload: UpdateRestaurantProfile) {
    const restaurant = await this.restaurantService.findOne({ _id: id })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const { firstName, lastName } = this.splitName(payload.authorizedPersonName)
    payload.authorizedPersonFirstName = firstName
    payload.authorizedPersonLastName = lastName
    delete payload.authorizedPersonName
    const updated = await this.restaurantService.update(id, payload)
    if (!updated) throw new BadRequestException()
    return updated
  }

  async getInfoAvailable(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const available = {
      bankAccount: false,
      paymentGateway: false,
      subscription: []
    }
    const paymentGateway = await this.paymentGatewayService.findOne({
      restaurantId
    })
    available.paymentGateway =
      paymentGateway?.paymentStatus === PaymentGatewayStatusEnum.ACTIVE
    const bankAccount = await this.bankAccountService.findOne({
      restaurantId,
      bankAccountStatus: BankAccountStatusEnum.ACTIVE
    })
    available.bankAccount = !!bankAccount
    const subscriptions = await this.subscriptionService.getAll({
      restaurantId,
      subscriptionStatus: SubscriptionStatusEnum.ACTIVE
    })
    const packageIds = []
    for (const subscription of subscriptions) {
      packageIds.push(subscription?.packageId)
    }
    const setPackageIds = [...new Set(packageIds)]
    const allPackage = await this.packageService.getAll({
      _id: { $in: setPackageIds },
      status: StatusEnum.ACTIVE
    })
    available.subscription = allPackage.map((item) => item.packageType)
    available.subscription = [...new Set(available.subscription.flat())]
    return available
  }
}
