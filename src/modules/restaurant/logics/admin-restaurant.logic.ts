import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaymentGatewayStatusEnum } from 'src/modules/payment-gateway/common/payment-gateway.enum'
import { BankAccountLogic } from 'src/modules/payment-gateway/logics/bank-account.logic'
import { PaymentGatewayService } from 'src/modules/payment-gateway/services/payment-gateway.service'
import { IRestaurantInfo } from '../interfaces/admin-restaurant.interface'
import { RestaurantService } from '../services/restaurant.service'

@Injectable()
export class AdminRestaurantLogic {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly bankAccountLogic: BankAccountLogic
  ) {}

  async getRestaurantInfo(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId
    })
    if (!restaurant) {
      throw new NotFoundException('Not found restaurant.')
    }
    const paymentGateway = await this.paymentGatewayService.findOne({
      restaurantId
    })
    const bankAccountLogic = await this.bankAccountLogic.getBankAccountForAdmin(
      restaurantId
    )
    const result: IRestaurantInfo = {
      profile: {
        id: restaurantId,
        legalBusinessName: restaurant?.legalBusinessName,
        dba: restaurant?.dba,
        ein: restaurant?.ein,
        address: restaurant?.address?.address,
        city: restaurant?.address?.city,
        state: restaurant?.address?.state,
        zipCode: restaurant?.address?.zipCode,
        phone: restaurant?.phone,
        email: restaurant?.email
      },
      paymentGateway: {
        merchantId: paymentGateway?.mid,
        status:
          paymentGateway?.paymentStatus || PaymentGatewayStatusEnum.INACTIVE,
        paymentCreditCardCharges: 'Interchange Plus'
      },
      achPaymentFile: bankAccountLogic,
      posProcessingFee: {
        creditCard: restaurant?.posProcessingFee?.creditCard,
        debitCard: restaurant?.posProcessingFee?.debitCard
      }
    }
    return result
  }

  async getBankAccount(id: string) {
    return this.bankAccountLogic.getBankAccountById(id)
  }

  async updateRestaurantStatus(id: string, status: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: { $ne: StatusEnum.DELETED }
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    if (restaurant.status === status) {
      throw new NotFoundException(`Restaurant status is already ${status}`)
    }
    await this.restaurantService.update(id, { status })
    return { success: true, id }
  }
}
