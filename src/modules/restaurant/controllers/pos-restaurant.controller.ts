import { Controller, Get, NotFoundException, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import {
  GetPOSRestaurantInfoResponse,
  TaxAndServiceChargeResponse
} from '../entities/pos-restaurant.entity'
import { RestaurantService } from '../services/restaurant.service'

@ApiBearerAuth()
@ApiTags('pos/restaurant')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/restaurant')
export class POSRestaurantController {
  constructor(private readonly restaurantService: RestaurantService) {}

  @Get()
  @ApiOkResponse({ type: () => GetPOSRestaurantInfoResponse })
  @ApiOperation({
    summary: 'Get Restaurant Info',
    description: 'use *bearer* `staff_token`'
  })
  async getRestaurantInfo() {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const restaurant = await this.restaurantService.findOne(
      {
        _id: restaurantId,
        status: StatusEnum.ACTIVE
      },
      {
        legalBusinessName: 1,
        dba: 1,
        address: 1,
        businessPhone: 1,
        email: 1,
        taxRate: 1,
        defaultServiceCharge: 1,
        logoUrl: 1,
        authorizedPersonName: {
          $concat: [
            '$authorizedPersonFirstName',
            ' ',
            '$authorizedPersonLastName'
          ]
        },
        authorizedPersonFirstName: 1,
        authorizedPersonLastName: 1,
        timeZone: 1
      }
    )
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return restaurant.toObject()
  }

  @Get('tax-service-charge')
  @ApiOkResponse({ type: () => TaxAndServiceChargeResponse })
  @ApiOperation({
    summary: 'Get Restaurant Tax and Service Charge Info',
    description: 'use *bearer* `staff_token`'
  })
  async getTaxInfo() {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const restaurant = await this.restaurantService.findOne(
      { _id: restaurantId, status: StatusEnum.ACTIVE },
      { taxRate: 1, defaultServiceCharge: 1 }
    )
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return restaurant.toObject()
  }
}
