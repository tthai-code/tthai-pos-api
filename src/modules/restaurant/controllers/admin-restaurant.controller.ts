import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import {
  SuccessObjectResponse,
  SuccessWithIdResponse
} from 'src/common/entity/success.entity'
import { StatusEnum } from 'src/common/enum/status.enum'
import { SuperAdminAuthGuard } from 'src/modules/auth/guards/super-admin-auth.guard'
import { RestaurantPaginateDto } from '../dto/get-restaurant.dto'
import { EditDebitCreditFeeDto } from '../dto/pos-fee.dto'
import {
  AdminRestaurantInfoResponse,
  AdminRestaurantPaginateResponse,
  GetACHResponse
} from '../entities/admin-restaurant.entity'
import { AdminRestaurantLogic } from '../logics/admin-restaurant.logic'
import { RestaurantService } from '../services/restaurant.service'

@ApiBearerAuth()
@ApiTags('admin/restaurant')
@UseGuards(SuperAdminAuthGuard)
@Controller('v1/admin/restaurant')
export class AdminRestaurantController {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly adminRestaurantLogic: AdminRestaurantLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => AdminRestaurantPaginateResponse })
  @ApiOperation({
    summary: 'Get Restaurant List',
    description: 'use bearer `admin_token`'
  })
  async getRestaurants(@Query() query: RestaurantPaginateDto) {
    return await this.restaurantService.paginate(query.buildQuery(), query)
  }

  @Get(':restaurantId/info')
  @ApiOkResponse({ type: () => AdminRestaurantInfoResponse })
  @ApiOperation({
    summary: 'Get Restaurant Info',
    description: 'use bearer `admin_token`'
  })
  async getRestaurantInfo(@Param('restaurantId') id: string) {
    return await this.adminRestaurantLogic.getRestaurantInfo(id)
  }

  @Get(':achId/ach')
  @ApiOkResponse({ type: () => GetACHResponse })
  @ApiOperation({
    summary: 'Get ACH by ID',
    description: 'use bearer `admin_token`'
  })
  async getACHById(@Param('achId') id: string) {
    return await this.adminRestaurantLogic.getBankAccount(id)
  }

  @Patch(':restaurantId/credit-fee')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Edit POS Processing Credit Fee',
    description: 'use bearer `admin_token`'
  })
  async editCreditFee(
    @Param('restaurantId') restaurantId: string,
    @Body() payload: EditDebitCreditFeeDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId
    })
    if (!restaurant) throw new NotFoundException('Not found Restaurant.')
    const { posProcessingFee } = restaurant
    posProcessingFee.creditCard = +payload.fee
    await this.restaurantService.update(restaurantId, {
      posProcessingFee
    })
    return { success: true }
  }

  @Patch(':restaurantId/debit-fee')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Edit POS Processing Debit Fee',
    description: 'use bearer `admin_token`'
  })
  async editDebitFee(
    @Param('restaurantId') restaurantId: string,
    @Body() payload: EditDebitCreditFeeDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId
    })
    if (!restaurant) throw new NotFoundException('Not found Restaurant.')
    const { posProcessingFee } = restaurant
    posProcessingFee.debitCard = +payload.fee
    await this.restaurantService.update(restaurantId, {
      posProcessingFee
    })
    return { success: true }
  }

  @Patch(':restaurantId/activate')
  @ApiOkResponse({ type: () => SuccessWithIdResponse })
  @ApiOperation({
    summary: 'Activate Restaurant by ID',
    description: 'use bearer `admin_token`'
  })
  async activateRestaurant(@Param('restaurantId') restaurantId: string) {
    return await this.adminRestaurantLogic.updateRestaurantStatus(
      restaurantId,
      StatusEnum.ACTIVE
    )
  }

  @Patch(':restaurantId/deactivate')
  @ApiOkResponse({ type: () => SuccessWithIdResponse })
  @ApiOperation({
    summary: 'Deactivate Restaurant by ID',
    description: 'use bearer `admin_token`'
  })
  async deactivateRestaurant(@Param('restaurantId') restaurantId: string) {
    return await this.adminRestaurantLogic.updateRestaurantStatus(
      restaurantId,
      StatusEnum.INACTIVE
    )
  }

  @Delete(':restaurantId')
  @ApiOkResponse({ type: () => SuccessWithIdResponse })
  @ApiOperation({
    summary: 'Delete Restaurant by ID',
    description: 'use bearer `admin_token`'
  })
  async deleteRestaurant(@Param('restaurantId') restaurantId: string) {
    return await this.adminRestaurantLogic.updateRestaurantStatus(
      restaurantId,
      StatusEnum.DELETED
    )
  }
}
