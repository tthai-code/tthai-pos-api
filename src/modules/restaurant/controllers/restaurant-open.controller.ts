import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import {
  ApiBasicAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantPaginateDto } from '../dto/get-restaurant.dto'
import {
  OpenGetRestaurantResponse,
  PaginateRestaurantOpenResponse
} from '../entities/restaurant-open.entity'
import { RestaurantService } from '../services/restaurant.service'

@ApiBasicAuth()
@ApiTags('open/restaurant')
@UseGuards(AuthGuard('basic'))
@Controller('v1/open/restaurant')
export class RestaurantOpenController {
  constructor(private readonly restaurantService: RestaurantService) {}

  @Get()
  @ApiOkResponse({ type: () => PaginateRestaurantOpenResponse })
  @ApiOperation({
    summary: 'Get paginate restaurant list',
    description:
      'username: `tthai_pos_api`\n\n password: `0#3hWwqS3iDVI1ou%JHrbFlY0v$`'
  })
  async getRestaurants(@Query() query: RestaurantPaginateDto) {
    return await this.restaurantService.paginate(query.buildQuery(), query, {
      legalBusinessName: 1,
      doingBusinessAs: '$dba',
      address: 1,
      businessPhone: 1,
      email: 1,
      logoUrl: 1,
      taxRate: 1,
      defaultServiceCharge: 1
    })
  }

  @Get(':id')
  @ApiOkResponse({ type: () => OpenGetRestaurantResponse })
  @ApiOperation({ summary: 'Get restaurant by ID' })
  async getRestaurant(@Param('id') id: string) {
    return await this.restaurantService.findOne(
      { _id: id, status: StatusEnum.ACTIVE },
      {
        legalBusinessName: 1,
        doingBusinessAs: '$dba',
        address: 1,
        businessPhone: 1,
        email: 1,
        logoUrl: 1,
        taxRate: 1,
        defaultServiceCharge: 1
      }
    )
  }
}
