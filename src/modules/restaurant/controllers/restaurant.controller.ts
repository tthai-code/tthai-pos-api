import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { RestaurantPaginateDto } from '../dto/get-restaurant.dto'
import { CreateRestaurantDto } from '../dto/create-restaurant.dto'
import { RestaurantService } from '../services/restaurant.service'
import { RestaurantEntity } from '../entities/restaurant.entity'
import { RestaurantLogic } from '../logics/restaurant.logic'
import { SetTaxRestaurantDto } from '../dto/set-tax-restaurant.dto'
import { getRestaurantByIdResponse } from '../response/get-restaurant-by-id.response'
import { UpdateRestaurantProfile } from '../dto/update-restaurant-profile.dto'
import { SetServiceCharge } from '../dto/set-service-charge.dto'

@ApiBearerAuth()
@ApiTags('restaurant')
@UseGuards(AdminAuthGuard)
@Controller('v1/restaurant')
export class RestaurantController {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly restaurantLogic: RestaurantLogic
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get Restaurant List' })
  @ApiOkResponse({ type: [RestaurantEntity] })
  async getRestaurants(@Query() query: RestaurantPaginateDto) {
    return await this.restaurantService.paginate(query.buildQuery(), query)
  }

  @Get(':id')
  @ApiOkResponse(getRestaurantByIdResponse)
  @ApiOperation({ summary: 'Get Restaurant By ID' })
  async getRestaurant(@Param('id') id: number) {
    const raw = await this.restaurantService.findById(id)
    const transform = raw.toObject()
    transform.authorizedPersonName = `${transform.authorizedPersonFirstName} ${transform.authorizedPersonLastName}`
    delete transform.authorizedPersonFirstName
    delete transform.authorizedPersonLastName
    return transform
  }

  @Post()
  @ApiOperation({ summary: 'Create Restaurant' })
  async createRestaurant(@Body() restaurant: CreateRestaurantDto) {
    const username = AdminAuthGuard.getAuthorizedUser()?.username
    return this.restaurantLogic.CreateLogic(restaurant, username)
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update Restaurant Profile' })
  async updateRestaurant(
    @Body() Restaurant: UpdateRestaurantProfile,
    @Param('id') id: string
  ) {
    return this.restaurantLogic.updateRestaurantProfile(id, Restaurant)
  }

  @Delete()
  @ApiOperation({ summary: 'Delete Restaurant' })
  async deleteRestaurant(@Query('id') id: string) {
    return this.restaurantService.update(id, { status: StatusEnum.DELETED })
  }

  @Patch(':id/tax')
  @ApiOperation({ summary: 'Update Tax Setting' })
  async setTaxRestaurant(
    @Param('id') id: string,
    @Body() payload: SetTaxRestaurantDto
  ) {
    return this.restaurantService.update(id, { taxRate: payload })
  }

  @Patch(':id/service-charge')
  @ApiOperation({ summary: 'Update Service Charge Setting' })
  async setServiceCharge(
    @Param('id') id: string,
    @Body() payload: SetServiceCharge
  ) {
    return this.restaurantService.update(id, payload)
  }

  @Get(':id/tax')
  @ApiOperation({ summary: 'Get Tax Setting' })
  async getTaxRestaurant(@Param('id') id: string) {
    return await this.restaurantService.findOneWithSelect(
      { _id: id },
      { taxRate: 1 }
    )
  }

  @Get('info/available')
  @ApiOperation({
    summary: 'Get Restaurant Info Available Feature',
    description: 'use bearer `restaurant_token`'
  })
  async getInfoAvailable() {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.restaurantLogic.getInfoAvailable(restaurantId)
  }
}
