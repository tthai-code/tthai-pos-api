import { forwardRef, Global, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { PaymentGatewayModule } from '../payment-gateway/payment-gateway.module'
import { RestaurantHolidaysModule } from '../restaurant-holiday/restaurant-holiday.module'
import { RestaurantHoursModule } from '../restaurant-hours/restaurant-hours.module'
import { SubscriptionModule } from '../subscription/subscription.module'
import { AdminRestaurantController } from './controllers/admin-restaurant.controller'
import { POSRestaurantController } from './controllers/pos-restaurant.controller'
import { RestaurantOpenController } from './controllers/restaurant-open.controller'
import { RestaurantController } from './controllers/restaurant.controller'
import { AdminRestaurantLogic } from './logics/admin-restaurant.logic'
import { RestaurantLogic } from './logics/restaurant.logic'
import { RestaurantSchema } from './schemas/restaurant.schema'
import { RestaurantService } from './services/restaurant.service'

@Global()
@Module({
  imports: [
    forwardRef(() => PaymentGatewayModule),
    forwardRef(() => SubscriptionModule),
    RestaurantHoursModule,
    RestaurantHolidaysModule,
    PaymentGatewayModule,
    MongooseModule.forFeature([
      { name: 'restaurant', schema: RestaurantSchema }
    ])
  ],
  providers: [RestaurantService, RestaurantLogic, AdminRestaurantLogic],
  controllers: [
    RestaurantController,
    POSRestaurantController,
    RestaurantOpenController,
    AdminRestaurantController
  ],
  exports: [RestaurantService]
})
export class RestaurantModule {}
