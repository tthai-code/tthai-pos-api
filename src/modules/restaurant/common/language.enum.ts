export enum LanguageEnum {
  ENG = 'ENG',
  THAI = 'THAI',
  ENGTHAI = 'ENG/THAI'
}
