export enum FontSizeEnum {
  SMALL = 'SMALL',
  NORMAL = 'NORMAL',
  BIG = 'BIG'
}
