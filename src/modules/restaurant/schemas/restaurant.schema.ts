import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { timeZone } from 'src/resources/timeZone'
import { FontSizeEnum } from '../common/font-size.enum'
import { LanguageEnum } from '../common/language.enum'

export type RestaurantDocument = RestaurantFields & Document

@Schema({ _id: false, strict: true, timestamps: false })
export class AddressFields {
  @Prop({ default: '' })
  address: string

  @Prop({ default: '' })
  city: string

  @Prop({ default: '' })
  state: string

  @Prop({ default: '' })
  zipCode: string
}

@Schema({ _id: false, strict: true, timestamps: false })
export class TaxRateSettingFields {
  @Prop({ default: 0 })
  tax: number

  @Prop({ default: 0 })
  alcoholTax: number

  @Prop({ default: false })
  isAlcoholTaxActive: boolean
}

@Schema({ _id: false, strict: true, timestamps: false })
export class ReceiptSettingFields {
  @Prop({ default: false })
  isDisplayLogo: boolean

  @Prop({ default: false })
  isDisplayText: boolean

  @Prop({ default: '' })
  text: string

  @Prop({ default: FontSizeEnum.NORMAL, enum: Object.values(FontSizeEnum) })
  fontSize: string

  @Prop({ default: LanguageEnum.ENG, enum: Object.values(LanguageEnum) })
  language: string
}

@Schema({ _id: false, strict: true, timestamps: false })
export class PosProcessingFeeFields {
  @Prop({ default: 0.25 })
  creditCard: number

  @Prop({ default: 0.5 })
  debitCard: number
}

@Schema({ timestamps: true, strict: true })
export class RestaurantFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurantAccount' })
  accountId: Types.ObjectId

  @Prop()
  legalBusinessName: string

  @Prop()
  dba: string

  @Prop()
  ein: string

  @Prop({
    default: new AddressFields()
  })
  address: AddressFields

  @Prop()
  businessPhone: string

  @Prop({ default: '' })
  authorizedPersonFirstName: string

  @Prop({ default: '' })
  authorizedPersonLastName: string

  @Prop()
  title: string

  @Prop()
  phone: string

  @Prop()
  email: string

  @Prop()
  referredBy: string

  @Prop({ default: null })
  einDocument: string

  @Prop({ default: null })
  salesTaxPermitDocument: string

  @Prop({ default: null })
  logoUrl: string

  @Prop({
    default: new TaxRateSettingFields()
  })
  taxRate: TaxRateSettingFields

  @Prop({ default: 10 })
  defaultServiceCharge: number

  @Prop({
    default: new ReceiptSettingFields()
  })
  receiptSetting: ReceiptSettingFields

  @Prop({
    default: new PosProcessingFeeFields()
  })
  posProcessingFee: PosProcessingFeeFields

  @Prop({ default: 'Etc/UTC', enum: timeZone })
  timeZone: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}

export const RestaurantSchema = SchemaFactory.createForClass(RestaurantFields)
RestaurantSchema.plugin(mongoosePaginate)
RestaurantSchema.plugin(authorStampCreatePlugin)
