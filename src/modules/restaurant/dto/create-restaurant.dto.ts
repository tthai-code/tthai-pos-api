import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsString,
  IsOptional,
  ValidateNested,
  IsNotEmpty
} from 'class-validator'

// This Create Dto is from situation before register RestaurantAccount success and first login
export class AddressFieldDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Some Address' })
  readonly address: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Addison' })
  readonly city: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Dallas' })
  readonly state: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: '75001' })
  readonly zipCode: string
}

export class CreateRestaurantDto {
  public authorizedPersonFirstName: string
  public authorizedPersonLastName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: true, example: '630a5dfd0e0a99f32e9f9b5a' })
  readonly accountId: string

  @IsString()
  @ApiProperty({ required: true, example: 'Tthai Restaurant' })
  readonly legalBusinessName: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'dba' })
  readonly dba: string

  @IsString()
  @ApiProperty({ required: true, example: '983242321' })
  readonly ein: string

  @Type(() => AddressFieldDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => AddressFieldDto })
  readonly address: AddressFieldDto

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '0111111111' })
  readonly businessPhone: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Yoshi Satoshi' })
  public authorizedPersonName: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Owner' })
  readonly title: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '093243221' })
  readonly phone: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test@mail.com' })
  readonly email: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'TEST01' })
  readonly referredBy: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://example.com/####/####' })
  readonly einDocument: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://example.com/####/####' })
  readonly salesTaxPermitDocument: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'America/Los_Angeles' })
  readonly timeZone: string
}
