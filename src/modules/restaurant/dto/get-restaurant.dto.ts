import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class RestaurantPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'createdAt' })
  readonly sortBy: string = 'createdAt'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  public buildQuery() {
    return {
      $or: [
        {
          legalBusinessName: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          dba: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          email: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          businessPhone: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      status: { $ne: StatusEnum.DELETED }
    }
  }
}
