import { ApiProperty } from '@nestjs/swagger'
import { IsNumber } from 'class-validator'

export class SetServiceCharge {
  @IsNumber()
  @ApiProperty({ example: 10 })
  defaultServiceCharge: number
}
