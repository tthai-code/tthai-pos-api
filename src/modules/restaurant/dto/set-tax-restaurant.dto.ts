import { ApiProperty } from '@nestjs/swagger'
import { IsBoolean, IsNumber } from 'class-validator'

export class SetTaxRestaurantDto {
  @IsNumber()
  @ApiProperty({ example: 7 })
  readonly tax: number

  @IsNumber()
  @ApiProperty({ example: 3 })
  readonly alcoholTax: number

  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isAlcoholTaxActive: boolean
}
