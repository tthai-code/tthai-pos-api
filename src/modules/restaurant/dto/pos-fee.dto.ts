import { ApiProperty } from '@nestjs/swagger'
import { IsNumber } from 'class-validator'

export class EditDebitCreditFeeDto {
  @IsNumber()
  @ApiProperty({ example: 0.25 })
  fee: number
}
