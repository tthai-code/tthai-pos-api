import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsString,
  IsOptional,
  ValidateNested,
  IsNotEmpty
} from 'class-validator'
import { AddressFieldDto } from './create-restaurant.dto'

export class UpdateRestaurantProfile {
  public authorizedPersonFirstName: string
  public authorizedPersonLastName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: true, example: 'Tthai Restaurant' })
  readonly legalBusinessName: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'dba' })
  readonly dba: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: true, example: '983242321' })
  readonly ein: string

  @Type(() => AddressFieldDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => AddressFieldDto })
  readonly address: AddressFieldDto

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '0111111111' })
  readonly businessPhone: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Yoshi Satoshi' })
  public authorizedPersonName: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Owner' })
  readonly title: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '093243221' })
  readonly phone: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test@mail.com' })
  readonly email: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'TEST01' })
  readonly referredBy: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://example.com/####/####' })
  readonly einDocument: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://example.com/####/####' })
  readonly salesTaxPermitDocument: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'America/Los_Angeles' })
  readonly timeZone: string
}
