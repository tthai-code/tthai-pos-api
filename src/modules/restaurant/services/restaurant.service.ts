import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantDocument } from '../schemas/restaurant.schema'
import { RestaurantPaginateDto } from '../dto/get-restaurant.dto'
import { AdminReportPaginateDto } from 'src/modules/report/dto/admin-report.dto'

@Injectable({ scope: Scope.REQUEST })
export class RestaurantService {
  constructor(
    @InjectModel('restaurant')
    private readonly RestaurantModel: PaginateModel<RestaurantDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<RestaurantDocument | any> {
    return this.RestaurantModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.RestaurantModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<RestaurantDocument> {
    return this.RestaurantModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.RestaurantModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.RestaurantModel.createCollection()

    return this.RestaurantModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<RestaurantDocument> {
    const document = new this.RestaurantModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async create(payload: any): Promise<RestaurantDocument> {
    const document = new this.RestaurantModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<RestaurantDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<RestaurantDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.RestaurantModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.RestaurantModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<RestaurantDocument> {
    return this.RestaurantModel.findOne(condition, project)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<RestaurantDocument> {
    return this.RestaurantModel.findOne(condition, options)
  }

  paginate(
    query: any,
    queryParam: RestaurantPaginateDto | AdminReportPaginateDto,
    select?: any
  ): Promise<PaginateResult<RestaurantDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.RestaurantModel.paginate(query, options)
  }

  findOneForOrder(id: string): Promise<RestaurantDocument> {
    return this.RestaurantModel.findOne(
      {
        _id: id,
        status: StatusEnum.ACTIVE
      },
      {
        _id: 0,
        id: '$_id',
        legalBusinessName: 1,
        address: 1,
        businessPhone: 1,
        email: 1,
        dba: 1,
        timeZone: 1
      }
    ).lean()
  }
}
