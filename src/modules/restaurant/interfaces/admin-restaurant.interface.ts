export interface IRestaurantProfile {
  id: string
  legalBusinessName: string
  dba: string
  ein: string
  address: string
  city: string
  state: string
  zipCode: string
  phone: string
  email: string
}

export interface IPaymentGatewayConnect {
  merchantId?: string
  status?: string
  paymentCreditCardCharges?: string
}

export interface IACHPaymentFile {
  id?: string
  date?: string | Date
}

export interface IPOSProcessingFee {
  creditCard?: number
  debitCard?: number
}

export interface IRestaurantInfo {
  profile: IRestaurantProfile
  paymentGateway: IPaymentGatewayConnect
  achPaymentFile: IACHPaymentFile[] | any[]
  posProcessingFee: IPOSProcessingFee
}
