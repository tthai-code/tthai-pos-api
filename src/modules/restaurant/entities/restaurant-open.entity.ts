import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'

class TaxRateFields {
  @ApiProperty()
  is_alcohol_tax_active: boolean
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
}

class AddressFields {
  @ApiProperty()
  zip_code: string
  @ApiProperty()
  state: string
  @ApiProperty()
  city: string
  @ApiProperty()
  address: string
}

class GetRestaurantFields {
  @ApiProperty()
  id: string
  @ApiProperty()
  default_service_charge: number
  @ApiProperty()
  tax_rate: TaxRateFields
  @ApiProperty()
  logo_url: string
  @ApiProperty()
  email: string
  @ApiProperty()
  business_phone: string
  @ApiProperty()
  address: AddressFields
  @ApiProperty()
  doing_business_as: string
  @ApiProperty()
  legal_business_name: string
}

class PaginateRestaurantOpenFields extends PaginateResponseDto<
  Array<GetRestaurantFields>
> {
  @ApiProperty({
    type: [GetRestaurantFields],
    example: [
      {
        tax_rate: {
          is_alcohol_tax_active: false,
          alcohol_tax: 25.2,
          tax: 14
        },
        logo_url: null,
        email: 'info@carolinaswise.com',
        business_phone: '5102638342',
        address: {
          zip_code: '94501',
          state: 'CA',
          city: 'Alameda',
          address: '1319 Park St'
        },
        legal_business_name: 'May Thai Kitchen LLC',
        default_service_charge: 10,
        doing_business_as: 'May Thai Kitchen',
        id: '630eff5751c2eac55f52662c'
      }
    ]
  })
  results: Array<GetRestaurantFields>
}

export class PaginateRestaurantOpenResponse extends ResponseDto<PaginateRestaurantOpenFields> {
  @ApiProperty({
    type: PaginateRestaurantOpenFields
  })
  data: PaginateRestaurantOpenFields
}

export class OpenGetRestaurantResponse extends ResponseDto<GetRestaurantFields> {
  @ApiProperty({
    type: GetRestaurantFields,
    example: {
      tax_rate: {
        is_alcohol_tax_active: false,
        alcohol_tax: 25.2,
        tax: 14
      },
      logo_url: null,
      email: 'info@carolinaswise.com',
      business_phone: '5102638342',
      address: {
        zip_code: '94501',
        state: 'CA',
        city: 'Alameda',
        address: '1319 Park St'
      },
      legal_business_name: 'May Thai Kitchen LLC',
      default_service_charge: 10,
      doing_business_as: 'May Thai Kitchen',
      id: '630eff5751c2eac55f52662c'
    }
  })
  data: GetRestaurantFields
}
