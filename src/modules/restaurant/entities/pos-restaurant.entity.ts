import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class TaxRateFields {
  @ApiProperty()
  is_alcohol_tax_active: boolean
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
}

class TaxAndServiceChargeFields {
  @ApiProperty()
  id: string

  @ApiProperty({ type: TaxRateFields })
  tax_rate: TaxRateFields

  @ApiProperty()
  defaultServiceCharge: number
}

class AddressFields {
  @ApiProperty()
  zip_code: string
  @ApiProperty()
  state: string
  @ApiProperty()
  city: string
  @ApiProperty()
  address: string
}

class RestaurantFields extends TaxAndServiceChargeFields {
  @ApiProperty()
  time_zone: string
  @ApiProperty()
  logo_url: string
  @ApiProperty()
  email: string
  @ApiProperty()
  authorized_person_last_name: string
  @ApiProperty()
  authorized_person_first_name: string
  @ApiProperty()
  business_phone: string
  @ApiProperty({ type: AddressFields })
  address: AddressFields
  @ApiProperty()
  dba: string
  @ApiProperty()
  legal_business_name: string
  @ApiProperty()
  authorized_person_name: string
}

export class GetPOSRestaurantInfoResponse extends ResponseDto<RestaurantFields> {
  @ApiProperty({
    type: RestaurantFields,
    example: {
      time_zone: 'Etc/UTC',
      default_service_charge: 10,
      tax_rate: {
        is_alcohol_tax_active: false,
        alcohol_tax: 0,
        tax: 10
      },
      logo_url: null,
      email: 'test03@gmail.com',
      authorized_person_last_name: 'Norton',
      authorized_person_first_name: 'Edward',
      business_phone: '1234567890',
      address: {
        zip_code: '90263',
        state: 'CA',
        city: 'Malibu',
        address: 'TEestst Address'
      },
      dba: 'Test DBA 03',
      legal_business_name: 'TeST 03',
      authorized_person_name: 'Edward Norton',
      id: '63984d6690416c27a7415915'
    }
  })
  data: RestaurantFields
}

export class TaxAndServiceChargeResponse extends ResponseDto<TaxAndServiceChargeFields> {
  @ApiProperty({
    type: TaxAndServiceChargeFields,
    example: {
      tax_rate: {
        is_alcohol_tax_active: false,
        alcohol_tax: 12.5,
        tax: 10
      },
      default_service_charge: 10,
      id: '630e55a0d9c30fd7cdcb424b'
    }
  })
  data: TaxAndServiceChargeFields
}
