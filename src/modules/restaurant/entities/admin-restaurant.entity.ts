import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import {
  ACHBankAccountTypeEnum,
  BankAccountStatusEnum
} from 'src/modules/payment-gateway/common/ach-bank.type.enum'

class AdminRestaurantPaginateObject extends PaginateResponseDto<any> {
  @ApiProperty({
    example: [
      {
        default_service_charge: 10,
        status: 'active',
        printer_setting: {
          receipt_ip_printer: '',
          kitchen_ip_printer: ''
        },
        pos_processing_fee: {
          debit_card: 0.25,
          credit_card: 0.5
        },
        receipt_setting: {
          language: 'ENG',
          font_size: 'NORMAL',
          text: '',
          is_display_text: false,
          is_display_logo: false
        },
        tax_rate: {
          is_alcohol_tax_active: false,
          alcohol_tax: 0,
          tax: 0
        },
        logo_url: null,
        sales_tax_permit_document: 'https://example.com/####/####',
        ein_document: 'https://example.com/####/####',
        referred_by: '',
        email: 'newjeans@gmail.com',
        phone: '093243221',
        title: 'Owner',
        authorized_person_last_name: 'Jeans',
        authorized_person_first_name: 'New',
        business_phone: '0222222233',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        ein: '983542323',
        dba: 'New Jeans',
        legal_business_name: 'New Jeans',
        account_id: '633d3606a9c503069fb8566f',
        created_at: '2022-10-09T21:34:57.241Z',
        updated_at: '2022-10-09T21:34:57.241Z',
        updated_by: {
          username: 'newjeans@gmail.com',
          id: '633d3606a9c503069fb8566f'
        },
        created_by: {
          username: 'newjeans@gmail.com',
          id: '633d3606a9c503069fb8566f'
        },
        id: '63433e8145728de99075847a'
      }
    ]
  })
  results: any
}

export class AdminRestaurantPaginateResponse extends ResponseDto<AdminRestaurantPaginateObject> {
  @ApiProperty({ type: AdminRestaurantPaginateObject })
  data: any
}

class ProfileObject {
  id: string
  legal_business_name: string
  dba: string
  ein: string
  address: string
  city: string
  state: string
  zip_code: string
  phone: string
  email: string
}

class PaymentGatewayObject {
  merchant_id: string
  status: string
  payment_credit_card_charges: string
}

class ACHProfileObject {
  date: string
  id: string
}

class POSProcessingFeeObject {
  credit_card: number
  debit_card: number
}

class AdminRestaurantInfo {
  profile: ProfileObject
  payment_gateway: PaymentGatewayObject
  ach_payment_file: ACHProfileObject[]
  pos_processing_fee: POSProcessingFeeObject
}

export class AdminRestaurantInfoResponse extends ResponseDto<AdminRestaurantInfo> {
  @ApiProperty({
    example: {
      profile: {
        id: '630e55a0d9c30fd7cdcb424b',
        legal_business_name: 'Holy Beef',
        dba: 'Test Doing as Business',
        ein: '983542321',
        address: 'Some Address',
        city: 'Addison',
        state: 'Dallas',
        zip_code: '75001',
        phone: '093243222',
        email: 'corywong@mail.com'
      },
      payment_gateway: {
        merchant_id: '820000003069',
        status: 'ACTIVE',
        payment_credit_card_charges: 'Interchange Plus'
      },
      ach_payment_file: [
        {
          date: '2022-11-24T10:48:15.658Z',
          id: '6384568aa13f6c6b60e231b5'
        },
        {
          date: '2022-11-24T10:48:15.658Z',
          id: '6384560fc8e1b6d47c475c86'
        },
        {
          date: '2022-11-24T10:48:15.658Z',
          id: '63844ba6438534c4ecaf0b49'
        },
        {
          date: '2022-11-24T10:48:15.658Z',
          id: '63844b25c05b5be2ac6c6657'
        },
        {
          date: '2022-11-24T10:48:15.658Z',
          id: '637f4c0cbf5a8f91bfb59f0f'
        }
      ],
      pos_processing_fee: {
        credit_card: 0.5,
        debit_card: 0.25
      }
    }
  })
  data: AdminRestaurantInfo
}

class AdminACHObject {
  @ApiProperty({ enum: Object.values(BankAccountStatusEnum) })
  bank_account_status: string
  @ApiProperty()
  token: string
  @ApiProperty()
  date: string
  @ApiProperty()
  signature: string
  @ApiProperty()
  routing_number: string
  @ApiProperty()
  account_number: string
  @ApiProperty({ enum: Object.values(ACHBankAccountTypeEnum) })
  account_type: string
  @ApiProperty()
  title: string
  @ApiProperty()
  print_name: string
  @ApiProperty()
  name: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
}

export class GetACHResponse extends ResponseDto<AdminACHObject> {
  @ApiProperty({
    type: AdminACHObject,
    example: {
      bank_account_status: 'INACTIVE',
      token: '9031395982141234',
      date: '2022-11-24T10:48:15.658Z',
      signature: 'Cory Wong 1',
      routing_number: 'XXXXXX808',
      account_number: 'XXXXXXXXXXXX1234',
      account_type: 'SAVING',
      title: 'Mr.',
      print_name: 'Cory Wong',
      name: 'Cory Wong 1',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      id: '63844b25c05b5be2ac6c6657'
    }
  })
  data: AdminACHObject
}
