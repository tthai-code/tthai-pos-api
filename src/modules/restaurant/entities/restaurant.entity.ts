import { ApiProperty } from '@nestjs/swagger'

class AddressResponse {
  @ApiProperty({ example: 'Some Address' })
  readonly address: string

  @ApiProperty({ example: 'Addison' })
  readonly city: string

  @ApiProperty({ example: 'Dallas' })
  readonly state: string

  @ApiProperty({ example: '75001' })
  readonly zip_code: string

  @ApiProperty({ example: 'test' })
  readonly note: string
}

class ResultsResponse {
  @ApiProperty({ example: '6308974b19416019c26ae15d' })
  id: string

  @ApiProperty({ example: 'Tthai Restaurant' })
  legal_business_name: string

  @ApiProperty({ example: '0111111111' })
  business_phone: string

  @ApiProperty({ example: 'test@mail.com' })
  email: string

  @ApiProperty({ type: () => AddressResponse })
  address: AddressResponse
}

class DataResponse {
  @ApiProperty({ example: 1 })
  total: number
  @ApiProperty({ example: 1 })
  limit: number
  @ApiProperty({ example: 1 })
  page: number
  @ApiProperty({ example: 1 })
  pages: number
  @ApiProperty({ type: () => [ResultsResponse] })
  results: Array<ResultsResponse>
}

export class RestaurantEntity {
  @ApiProperty({ example: 'done' })
  message: string
  @ApiProperty({ type: () => DataResponse })
  data: DataResponse
}
