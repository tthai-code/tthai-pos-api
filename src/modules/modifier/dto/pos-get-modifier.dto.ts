import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class POSModifierPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'createdAt' })
  readonly sortBy: string = 'position'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'asc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          label: {
            $regex: this.search,
            $options: 'i'
          },
          'items.name': {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      restaurantId: id,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
