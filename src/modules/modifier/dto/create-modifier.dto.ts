import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  ArrayNotEmpty,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ModifierTypeEnum } from '../common/modifier-type.enum'

export class ModifierItemsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Beef' })
  readonly name: string

  @IsString()
  @ApiProperty({ example: 'Beef' })
  readonly nativeName: string

  @IsNumber()
  @ApiProperty({ example: 2.5 })
  readonly price: number
}

export class CreateModifierDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Protein Choices' })
  readonly label: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'P' })
  readonly abbreviation: string

  @IsEnum(ModifierTypeEnum)
  @ApiProperty({ example: ModifierTypeEnum.REQUIRED, enum: ModifierTypeEnum })
  readonly type: string

  @IsNumber()
  @ApiProperty({ example: 1 })
  readonly maxSelected: number

  @Type(() => ModifierItemsDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [ModifierItemsDto] })
  readonly items: Array<ModifierItemsDto>

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({ example: 1 })
  readonly position: number

  @IsEnum(StatusEnum)
  @IsOptional()
  @ApiPropertyOptional({ example: StatusEnum.ACTIVE, enum: StatusEnum })
  readonly status: string
}

export class POSCreateModifierDto {
  public restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Protein Choices' })
  readonly label: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'P' })
  readonly abbreviation: string

  @IsEnum(ModifierTypeEnum)
  @ApiProperty({ example: ModifierTypeEnum.REQUIRED, enum: ModifierTypeEnum })
  readonly type: string

  @IsNumber()
  @ApiProperty({ example: 1 })
  readonly maxSelected: number

  @Type(() => ModifierItemsDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [ModifierItemsDto] })
  readonly items: Array<ModifierItemsDto>

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({
    example: 1,
    default: 999,
    description: 'use to sort modifier position for show'
  })
  readonly position: number
}
