import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'

export class GetAllModifierDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<restaurant-id>' })
  readonly restaurant: string

  public buildQuery(id: string) {
    const result = {
      restaurantId: { $eq: !id ? this.restaurant : id },
      status: StatusEnum.ACTIVE
    }
    if (!id && !this.restaurant) delete result.restaurantId
    return result
  }
}
