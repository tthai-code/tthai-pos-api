export enum ModifierTypeEnum {
  OPTIONAL = 'OPTIONAL',
  REQUIRED = 'REQUIRED'
}
