import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { ModifierTypeEnum } from '../common/modifier-type.enum'

export type ModifierDocument = ModifierFields & Document

@Schema({ timestamps: false, _id: false, strict: true })
export class ModifierItemsFields {
  @Prop({ required: true })
  name: string

  @Prop({ required: true })
  nativeName: string

  @Prop({ default: 0 })
  price: number
}

@Schema({ timestamps: true, strict: true, collection: 'modifiers' })
export class ModifierFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  label: string

  @Prop({ required: true })
  abbreviation: string

  @Prop({ enum: ModifierTypeEnum, default: ModifierTypeEnum.OPTIONAL })
  type: string

  @Prop({ default: 0 })
  maxSelected: number

  @Prop({ default: [] })
  items: Array<ModifierItemsFields>

  @Prop({ default: 999 })
  position: number

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}

export const ModifierSchema = SchemaFactory.createForClass(ModifierFields)
ModifierSchema.plugin(mongoosePaginate)
ModifierSchema.plugin(authorStampCreatePlugin)
