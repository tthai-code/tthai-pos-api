import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { POSCreateModifierDto } from '../dto/create-modifier.dto'
import { POSModifierPaginateDto } from '../dto/pos-get-modifier.dto'
import {
  POSCreateUpdateModifierResponse,
  POSModifierPaginateResponse,
  POSModifierResponse
} from '../entity/pos-modifier.dto'
import { POSModifierLogic } from '../logics/pos-modifier.logic'

@ApiBearerAuth()
@ApiTags('pos/modifier')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/modifier')
export class POSModifierController {
  constructor(private readonly posModifierLogic: POSModifierLogic) {}

  @Get()
  @ApiOkResponse({ type: () => POSModifierPaginateResponse })
  @ApiOperation({
    summary: 'Get Paginate Modifier List',
    description: 'use bearer `staff_token`'
  })
  async getModifierList(@Query() query: POSModifierPaginateDto) {
    const restaurantId = StaffAuthGuard?.getAuthorizedUser()?.restaurant?.id
    return await this.posModifierLogic.getListModifier(restaurantId, query)
  }

  @Get(':id')
  @ApiOkResponse({ type: () => POSModifierResponse })
  @ApiOperation({
    summary: 'Get Modifier By ID',
    description: 'use bearer `staff_token`'
  })
  async getModifier(@Param('id') id: string) {
    return await this.posModifierLogic.getModifierById(id)
  }

  @Post()
  @ApiCreatedResponse({ type: () => POSCreateUpdateModifierResponse })
  @ApiOperation({
    summary: 'Create a Modifier',
    description: 'use bearer `staff_token`'
  })
  async createModifier(@Body() payload: POSCreateModifierDto) {
    const restaurantId = StaffAuthGuard?.getAuthorizedUser()?.restaurant?.id
    return await this.posModifierLogic.createModifier(restaurantId, payload)
  }

  @Put(':id')
  @ApiOkResponse({ type: () => POSCreateUpdateModifierResponse })
  @ApiOperation({
    summary: 'Update a Modifier by ID',
    description: 'use bearer `staff_token`'
  })
  async updateModifier(
    @Param('id') id: string,
    @Body() payload: POSCreateModifierDto
  ) {
    return await this.posModifierLogic.updateModifier(id, payload)
  }

  @Delete(':id')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Delete Modifier by ID',
    description: 'use bearer `staff_token`'
  })
  async deleteModifier(@Param('id') id: string) {
    return await this.posModifierLogic.deleteModifier(id)
  }

  @Post(':modifierId/menu/:menuId')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Add Modifier to Menu by ID',
    description: 'use bearer `staff_token`'
  })
  async addModifierToMenu(
    @Param('menuId') menuId: string,
    @Param('modifierId') modifierId: string
  ) {
    return await this.posModifierLogic.createMenuModifier(menuId, modifierId)
  }

  @Delete(':menuModifierId/menu')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Remove Modifier from Menu by menuModifierId',
    description: 'use bearer `staff_token`'
  })
  async removeMenuModifier(@Param('menuModifierId') menuModifierId: string) {
    return await this.posModifierLogic.deleteMenuModifier(menuModifierId)
  }
}
