import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { GetAllMenuModifierResponse } from 'src/modules/menu-modifier/entitiy/menu-modifier.entity'
import { CreateModifierDto } from '../dto/create-modifier.dto'
import { GetAllModifierDto } from '../dto/get-all-modifier.dto'
import { ModifierPaginateDto } from '../dto/get-modifier.dto'
import { UpdateModifierDto } from '../dto/update-modifier.dto'
import { ModifierLogic } from '../logics/modifier.logic'
import { ModifierService } from '../services/modifier.service'

@ApiBearerAuth()
@ApiTags('modifier')
@Controller('v1/modifier')
@UseGuards(AdminAuthGuard)
export class ModifierController {
  constructor(
    private readonly modifierService: ModifierService,
    private readonly modifierLogic: ModifierLogic
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get Modifier List' })
  async getModifiers(@Query() query: ModifierPaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant.id
    return await this.modifierService.paginate(
      query.buildQuery(restaurantId),
      query
    )
  }

  @Get('all')
  @ApiOkResponse({ type: () => GetAllMenuModifierResponse })
  @ApiOperation({ summary: 'Get All Modifier List' })
  async getAllModifiers(@Query() query: GetAllModifierDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant.id
    return await this.modifierService.getAll(query.buildQuery(restaurantId), {
      label: 1,
      abbreviation: 1,
      items: 1,
      type: 1,
      maxSelected: 1
    })
  }

  @Get(':id/modifier')
  @ApiOperation({ summary: 'Get a Modifier' })
  async getModifier(@Param('id') id: string) {
    return await this.modifierService.findOne({ _id: id })
  }

  @Post()
  @ApiOperation({ summary: 'Create Modifier' })
  async createModifier(@Body() payload: CreateModifierDto) {
    return await this.modifierLogic.createModifier(payload)
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update Modifier' })
  async updateModifier(
    @Param('id') id: string,
    @Body() payload: UpdateModifierDto
  ) {
    // return await this.modifierService.update(id, payload)
    return await this.modifierLogic.updateModifier(id, payload)
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete Modifier' })
  async deleteModifier(@Param('id') id: string) {
    return await this.modifierService.delete(id)
  }
}
