import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { SuccessField } from 'src/common/entity/success.entity'
import { ModifierTypeEnum } from '../common/modifier-type.enum'

class POSCreateUpdateModifier extends SuccessField {
  @ApiProperty()
  id: string
}

export class POSCreateUpdateModifierResponse extends ResponseDto<POSCreateUpdateModifier> {
  @ApiProperty({
    type: POSCreateUpdateModifier,
    example: {
      success: true,
      id: '63b649273965c0c664bd2d46'
    }
  })
  data: POSCreateUpdateModifier
}

class ModifierItemsObject {
  @ApiProperty()
  name: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  price: number
}

class ModifierObject {
  @ApiProperty()
  items: ModifierItemsObject[]
  @ApiProperty()
  max_selected: number
  @ApiProperty({ enum: Object.values(ModifierTypeEnum) })
  @ApiProperty()
  type: string
  @ApiProperty()
  label: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  position: number
  @ApiProperty()
  id: string
}

class PaginateModifier extends PaginateResponseDto<ModifierObject[]> {
  @ApiProperty({
    type: [ModifierObject],
    example: [
      {
        items: [
          {
            name: 'Beef',
            native_name: 'Beef',
            price: 2.5
          },
          {
            name: 'Pork',
            native_name: 'Pork',
            price: 1
          },
          {
            name: 'Chicken',
            native_name: 'Chicken',
            price: 0
          }
        ],
        max_selected: 1,
        type: 'REQUIRED',
        label: 'Protein Choices',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        abbreviation: 'P',
        position: 999,
        id: '630f464bb75d5da2ea75a5aa'
      }
    ]
  })
  results: ModifierObject[]
}

export class POSModifierPaginateResponse extends ResponseDto<PaginateModifier> {
  @ApiProperty({ type: PaginateModifier })
  data: PaginateModifier
}

export class POSModifierResponse extends ResponseDto<ModifierObject> {
  @ApiProperty({
    type: ModifierObject,
    example: {
      items: [
        {
          name: 'Beef',
          native_name: 'Beef',
          price: 2.5
        },
        {
          name: 'Pork',
          native_name: 'Pork',
          price: 1
        },
        {
          name: 'Chicken',
          native_name: 'Chicken',
          price: 0
        }
      ],
      max_selected: 1,
      type: 'REQUIRED',
      label: 'Protein Choices',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      abbreviation: 'P',
      position: 999,
      id: '630f464bb75d5da2ea75a5aa'
    }
  })
  data: ModifierObject
}
