import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service'
import { POSCreateModifierDto } from '../dto/create-modifier.dto'
import { POSModifierPaginateDto } from '../dto/pos-get-modifier.dto'
import { ModifierService } from '../services/modifier.service'

@Injectable()
export class POSModifierLogic {
  constructor(
    private readonly modifierService: ModifierService,
    private readonly menuModifierService: MenuModifierService
  ) {}

  async getListModifier(restaurantId: string, query: POSModifierPaginateDto) {
    return await this.modifierService.paginate(
      query.buildQuery(restaurantId),
      query,
      {
        status: 0,
        createdAt: 0,
        updatedAt: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
  }

  async getModifierById(id: string) {
    const modifier = await this.modifierService.findOne(
      { _id: id, status: StatusEnum.ACTIVE },
      {
        status: 0,
        createdAt: 0,
        updatedAt: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    if (!modifier) throw new NotFoundException('Not found modifier.')
    return modifier
  }

  async createModifier(restaurantId: string, payload: POSCreateModifierDto) {
    payload.restaurantId = restaurantId
    const created = await this.modifierService.create(payload)
    return { success: true, id: created.id }
  }

  async updateModifier(id: string, payload: POSCreateModifierDto) {
    const logic = async (session: ClientSession) => {
      const modifier = await this.modifierService.findOne({
        _id: id,
        status: StatusEnum.ACTIVE
      })
      if (!modifier) throw new NotFoundException('Not found modifier.')

      const updated = await this.modifierService.transactionUpdate(
        id,
        payload,
        session
      )
      await this.menuModifierService.transactionUpdateMany(
        { modifierId: id },
        payload,
        session
      )
      return updated
    }
    const result = await runTransaction(logic)
    return { success: true, id: result.id }
  }

  async deleteModifier(id: string) {
    const modifier = await this.modifierService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!modifier) throw new NotFoundException('Not found modifier.')
    await this.modifierService.delete(id)
    await this.menuModifierService.deleteMany({ modifierId: id })
    return { success: true }
  }

  async createMenuModifier(menuId: string, modifierId: string) {
    const isExist = await this.menuModifierService.findOne({
      menuId,
      modifierId,
      status: { $ne: StatusEnum.DELETED }
    })
    if (isExist) throw new BadRequestException('Menu Modifier is exist.')
    const modifier = await this.modifierService.findOne(
      {
        _id: modifierId
      },
      {
        label: 1,
        abbreviation: 1,
        type: 1,
        items: 1,
        maxSelected: 1
      }
    )
    await this.menuModifierService.create({
      modifierId,
      menuId,
      label: modifier.label,
      abbreviation: modifier.abbreviation,
      type: modifier.type,
      items: modifier.items,
      maxSelected: modifier.maxSelected
    })
    return { success: true }
  }

  async deleteMenuModifier(menuModifierId: string) {
    const menuModifier = await this.menuModifierService.findOne({
      _id: menuModifierId
    })
    if (!menuModifier) throw new NotFoundException('Not found menu modifier.')
    await this.menuModifierService.delete(menuModifierId)
    return { success: true }
  }
}
