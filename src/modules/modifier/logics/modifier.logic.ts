import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service'
import { CreateModifierDto } from '../dto/create-modifier.dto'
import { UpdateModifierDto } from '../dto/update-modifier.dto'
import { ModifierService } from '../services/modifier.service'

@Injectable()
export class ModifierLogic {
  constructor(
    private readonly modifierService: ModifierService,
    private readonly menuModifierService: MenuModifierService
  ) {}

  async createModifier(payload: CreateModifierDto) {
    const isExist = await this.modifierService.findOne({
      restaurantId: payload.restaurantId,
      position: payload.position,
      status: StatusEnum.ACTIVE
    })
    if (isExist) {
      throw new BadRequestException(
        `Position ${payload.position} is already used.`
      )
    }
    return await this.modifierService.create(payload)
  }

  async updateModifier(id: string, payload: UpdateModifierDto) {
    const modifier = await this.modifierService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!modifier) throw new NotFoundException('Not found modifier.')
    const isExist = await this.modifierService.findOne({
      _id: { $ne: id },
      restaurantId: modifier.restaurantId,
      position: payload.position,
      status: StatusEnum.ACTIVE
    })
    if (isExist) {
      throw new BadRequestException(
        `Position ${payload.position} is already used.`
      )
    }

    const logic = async (session: ClientSession) => {
      const updated = await this.modifierService.transactionUpdate(
        id,
        payload,
        session
      )
      await this.menuModifierService.transactionUpdateMany(
        { modifierId: id },
        payload,
        session
      )
      return updated
    }
    return runTransaction(logic)
  }
}
