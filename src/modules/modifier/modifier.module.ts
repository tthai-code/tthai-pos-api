import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { MenuModifierModule } from '../menu-modifier/menu-modifier.module'
import { ModifierController } from './controllers/modifier.controller'
import { POSModifierController } from './controllers/pos-modifier.controller'
import { ModifierLogic } from './logics/modifier.logic'
import { POSModifierLogic } from './logics/pos-modifier.logic'
import { ModifierSchema } from './models/modifier.schema'
import { ModifierService } from './services/modifier.service'

@Module({
  imports: [
    forwardRef(() => MenuModifierModule),
    MongooseModule.forFeature([{ name: 'modifier', schema: ModifierSchema }])
  ],
  providers: [ModifierService, ModifierLogic, POSModifierLogic],
  controllers: [ModifierController, POSModifierController],
  exports: [ModifierService]
})
export class ModifierModule {}
