import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ModifierDocument } from '../models/modifier.schema'
import { ModifierPaginateDto } from '../dto/get-modifier.dto'
import { POSModifierPaginateDto } from '../dto/pos-get-modifier.dto'

@Injectable({ scope: Scope.REQUEST })
export class ModifierService {
  constructor(
    @InjectModel('modifier')
    private readonly ModifierModel: PaginateModel<ModifierDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<ModifierDocument | any> {
    return this.ModifierModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.ModifierModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<ModifierDocument> {
    return this.ModifierModel
  }

  async create(payload: any): Promise<ModifierDocument> {
    const document = new this.ModifierModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<ModifierDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<ModifierDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async delete(id: string): Promise<ModifierDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.ModifierModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.ModifierModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<ModifierDocument> {
    return this.ModifierModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: ModifierPaginateDto | POSModifierPaginateDto,
    select?: any
  ): Promise<PaginateResult<ModifierDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }
    return this.ModifierModel.paginate(query, options)
  }
}
