import { Injectable } from '@nestjs/common'
import { RestaurantHolidaysService } from 'src/modules/restaurant-holiday/services/restaurant-holiday.service'
import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service'
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CategoryHoursService } from '../services/category-hours.service'
import { MenuCategoriesService } from '../services/menu-category.service'
import { GetMenuCategoryListDto } from '../dto/get-menu-categories.dto'

@Injectable()
export class OpenMenuCategoryLogic {
  constructor(
    private readonly menuCategoryService: MenuCategoriesService,
    private readonly categoryHoursService: CategoryHoursService,
    private readonly restaurantService: RestaurantService,
    private readonly restaurantPeriodService: RestaurantPeriodService,
    private readonly restaurantHolidaysService: RestaurantHolidaysService,
    private readonly restaurantDayService: RestaurantDayService
  ) {}

  async getCategoryList(query: GetMenuCategoryListDto) {
    const categories = await this.menuCategoryService.getAll(
      query.buildQuery(),
      {
        position: 0,
        isShow: 0,
        isDisplayOnWebAndApp: 0,
        status: 0,
        createdAt: 0,
        updatedAt: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    return categories
  }
}
