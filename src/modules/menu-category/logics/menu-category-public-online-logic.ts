import { Injectable, NotFoundException } from '@nestjs/common'
import * as dayjs from 'dayjs'
import * as utc from 'dayjs/plugin/utc'
import * as timezone from 'dayjs/plugin/timezone'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service'
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service'
import { RestaurantHolidaysService } from 'src/modules/restaurant-holiday/services/restaurant-holiday.service'
import { MenuService } from 'src/modules/menu/services/menu.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CategoryHoursService } from '../services/category-hours.service'
import { MenuCategoriesService } from '../services/menu-category.service'
import { MenuCategoriesOnlineService } from '../services/menu-category-public-online.service'
import { GetDayStringByDay } from 'src/utilities/dateTime'

import { MenuPaginateDto } from 'src/modules/menu/dto/get-menu.dto'
import { WebSettingService } from 'src/modules/web-setting/services/web-setting.service'
import { RestaurantPeriodDocument } from 'src/modules/restaurant-hours/schemas/restaurant-period.schema'

dayjs.extend(utc)
dayjs.extend(timezone)
@Injectable()
export class MenuCategoryOnlineLogic {
  constructor(
    private readonly menuCategoryService: MenuCategoriesService,
    private readonly menuCategoriesOnlineService: MenuCategoriesOnlineService,
    private readonly categoryHoursService: CategoryHoursService,
    private readonly restaurantService: RestaurantService,
    private readonly restaurantPeriodService: RestaurantPeriodService,
    private readonly restaurantHolidaysService: RestaurantHolidaysService,
    private readonly restaurantDayService: RestaurantDayService,
    private readonly menuService: MenuService,
    private readonly webSettingService: WebSettingService
  ) {}

  private filterAvailablePeriod(
    timeZone: string,
    restaurantPeriods: RestaurantPeriodDocument[]
  ) {
    const day = dayjs().tz(timeZone)
    return restaurantPeriods.filter((item) => {
      const [openHour, openMinute] = item?.opensAt?.split(':')
      const [closeHour, closeMinute] = item?.closesAt?.split(':')
      const opensAt = day
        .hour(+(Number(openHour) - 1))
        .minute(+openMinute)
        .second(0)
        .millisecond(0)
        .valueOf()
      const closesAt = day
        .hour(+(Number(closeHour) - 1))
        .minute(+closeMinute)
        .second(0)
        .millisecond(0)
        .valueOf()
      const now = day.valueOf()
      return !(now < opensAt || now > closesAt)
    })
  }

  async getAvailableStatusForCategory(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const dayOfWeekString = await GetDayStringByDay(
      dayjs().tz(restaurant.timeZone).day()
    )
    const restaurantPeriods = await this.restaurantPeriodService.getAll({
      restaurantId: restaurantId,
      status: StatusEnum.ACTIVE,
      label: dayOfWeekString
    })

    const availablePeriod = this.filterAvailablePeriod(
      restaurant.timeZone,
      restaurantPeriods
    )

    const periodIds = availablePeriod.map((item) => item.id)
    const categoryHours = await this.categoryHoursService.getAll({
      restaurantPeriodId: { $in: periodIds }
    })
    const categoryIds = await categoryHours.map((item) => `${item.categoryId}`)
    return categoryIds
  }

  async getMenuCategoryByRestaurantId(
    restaurantId: string,
    queryParam: MenuPaginateDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const dayOfWeekString = await GetDayStringByDay(
      dayjs().tz(restaurant.timeZone).day()
    )
    const restaurantPeriods = await this.restaurantPeriodService.getAll({
      restaurantId: restaurantId,
      status: StatusEnum.ACTIVE,
      label: dayOfWeekString
    })

    const availablePeriod = this.filterAvailablePeriod(
      restaurant.timeZone,
      restaurantPeriods
    )

    const periodIds = availablePeriod.map((item) => item.id)
    const categoryHours = await this.categoryHoursService.getAll({
      restaurantPeriodId: { $in: periodIds }
    })
    const categoryIds = await categoryHours.map((item) => `${item.categoryId}`)
    const categories = await this.menuCategoriesOnlineService.getAll(
      {
        restaurantId,
        isShow: true,
        isDisplayOnWebAndApp: true,
        status: StatusEnum.ACTIVE
      },
      { createdAt: 0, updatedAt: 0, updatedBy: 0, createdBy: 0 },
      { sort: { position: 1 } }
    )
    const webSetting = await this.webSettingService.findOne({ restaurantId })
    const { isOnlineOrdering } = webSetting

    const categoriesWithMenu = []
    for (let i = 0; i < categories.length; i++) {
      const isAvailable = categoryIds.includes(`${categories[i]._id}`)

      const menus = await this.menuService.paginate(
        { category: categories[i].id, status: StatusEnum.ACTIVE },
        queryParam,
        { createdAt: 0, updatedAt: 0, updatedBy: 0, createdBy: 0 }
      )
      if (menus.docs.length > 0) {
        categoriesWithMenu.push({
          status: categories[i].status,
          isDisplayOnWebAndApp: categories[i].isDisplayOnWebAndApp,
          isContainAlcohol: categories[i].isContainAlcohol,
          isShow: categories[i].isShow,
          position: categories[i].position,
          name: categories[i].name,
          restaurantId: categories[i].restaurantId,
          description: categories[i].description,
          id: categories[i]._id,
          isAvailable: isOnlineOrdering && isAvailable,
          menus: menus.docs.map((item) => ({
            ...item.toObject(),
            isAvailable: isOnlineOrdering && isAvailable
          }))
        })
      }
    }

    return categoriesWithMenu
  }

  async getCategoryById(id: string) {
    const category = await this.menuCategoryService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!category) throw new NotFoundException('Not found menu category.')
    const restaurantDays = await this.restaurantDayService.getAll(
      { restaurantId: category.restaurantId, isClosed: false },
      { _id: 0, label: 1, isClosed: 1 }
    )
    if (restaurantDays.length <= 0) {
      return {
        name: category.name,
        position: category.position,
        description: category.description,
        isShow: category.isShow,
        isContainAlcohol: category.isContainAlcohol,
        isDisplayOnWebAndApp: category.isDisplayOnWebAndApp,
        hours: []
      }
    }
    const openDays = restaurantDays.map((item) => item.label)
    const restaurantPeriods = await this.restaurantPeriodService.getAll(
      {
        restaurantId: category.restaurantId,
        label: { $in: openDays },
        status: StatusEnum.ACTIVE
      },
      { label: 1, opensAt: 1, closesAt: 1 }
    )
    const categoryHours = await this.categoryHoursService.getAll({
      categoryId: id
    })
    const selectedPeriod = categoryHours.map(
      (selected) => `${selected.restaurantPeriodId}`
    )
    const hours = restaurantPeriods.map((item) => ({
      id: item._id,
      label: item.label,
      opensAt: item.opensAt,
      closesAt: item.closesAt,
      isSelected: selectedPeriod.includes(`${item._id}`)
    }))

    return {
      name: category.name,
      position: category.position,
      description: category.description,
      isShow: category.isShow,
      isContainAlcohol: category.isContainAlcohol,
      isDisplayOnWebAndApp: category.isDisplayOnWebAndApp,
      hours
    }
  }
}
