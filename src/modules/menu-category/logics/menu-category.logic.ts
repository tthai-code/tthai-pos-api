import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service'
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CreateMenuCategoryDto } from '../dto/create-menu-category.dto.'
import { UpdateCategoryPositionDto } from '../dto/update-category-position.dto'
import { UpdateMenuCategoryDto } from '../dto/update-menu-categories.dto'
import { CategoryHoursService } from '../services/category-hours.service'
import { MenuCategoriesService } from '../services/menu-category.service'

@Injectable()
export class MenuCategoryLogic {
  constructor(
    private readonly menuCategoryService: MenuCategoriesService,
    private readonly categoryHoursService: CategoryHoursService,
    private readonly restaurantService: RestaurantService,
    private readonly restaurantPeriodService: RestaurantPeriodService,
    private readonly restaurantDayService: RestaurantDayService
  ) {}

  async createMenuCategory(createCategory: CreateMenuCategoryDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: createCategory.restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const lastPosition = await this.menuCategoryService.findOne(
      {
        restaurantId: createCategory.restaurantId,
        // position: createCategory.position,
        status: StatusEnum.ACTIVE
      },
      {},
      { sort: { position: -1 } }
    )
    const position = lastPosition?.position || 0
    createCategory.position = position + 1
    // if (isPositionExist) {
    //   throw new BadRequestException('Duplicate category position')
    // }
    const created = await this.menuCategoryService.create(createCategory)
    if (!created) throw new BadRequestException()
    const hoursPayload = createCategory.hours
      .filter((period) => period.isSelected)
      .map((period) => ({
        restaurantId: createCategory.restaurantId,
        categoryId: created._id,
        restaurantPeriodId: period.id
      }))
    await this.categoryHoursService.createMany(hoursPayload)
    return created
  }

  async getCategoryHoursForCreate(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const restaurantDays = await this.restaurantDayService.getAll(
      { restaurantId, isClosed: false },
      { _id: 0, label: 1, isClosed: 1 }
    )
    if (restaurantDays.length <= 0) return []
    const openDays = restaurantDays.map((item) => item.label)
    const restaurantPeriods = await this.restaurantPeriodService.getAll(
      {
        restaurantId,
        label: { $in: openDays },
        status: StatusEnum.ACTIVE
      },
      { label: 1, opensAt: 1, closesAt: 1 }
    )
    const result = restaurantPeriods.map((item) => ({
      id: item._id,
      label: item.label,
      opensAt: item.opensAt,
      closesAt: item.closesAt,
      isSelected: false
    }))
    return result
  }

  async getCategoryById(id: string) {
    const category = await this.menuCategoryService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!category) throw new NotFoundException('Not found menu category.')
    const restaurantDays = await this.restaurantDayService.getAll(
      { restaurantId: category.restaurantId, isClosed: false },
      { _id: 0, label: 1, isClosed: 1 }
    )
    if (restaurantDays.length <= 0) {
      return {
        name: category.name,
        position: category.position,
        description: category.description,
        isShow: category.isShow,
        isContainAlcohol: category.isContainAlcohol,
        isDisplayOnWebAndApp: category.isDisplayOnWebAndApp,
        hours: []
      }
    }
    const openDays = restaurantDays.map((item) => item.label)
    const restaurantPeriods = await this.restaurantPeriodService.getAll(
      {
        restaurantId: category.restaurantId,
        label: { $in: openDays },
        status: StatusEnum.ACTIVE
      },
      { label: 1, opensAt: 1, closesAt: 1 }
    )
    const categoryHours = await this.categoryHoursService.getAll({
      categoryId: id
    })
    const selectedPeriod = categoryHours.map(
      (selected) => `${selected.restaurantPeriodId}`
    )
    const hours = restaurantPeriods.map((item) => ({
      id: item._id,
      label: item.label,
      opensAt: item.opensAt,
      closesAt: item.closesAt,
      isSelected: selectedPeriod.includes(`${item._id}`)
    }))

    return {
      name: category.name,
      position: category.position,
      description: category.description,
      isShow: category.isShow,
      isContainAlcohol: category.isContainAlcohol,
      isDisplayOnWebAndApp: category.isDisplayOnWebAndApp,
      hours
    }
  }

  async updateMenuCategory(id: string, payload: UpdateMenuCategoryDto) {
    const category = await this.menuCategoryService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!category) throw new NotFoundException('Not found menu category.')
    // const isPositionExist = await this.menuCategoryService.findOne({
    //   _id: { $ne: id },
    //   restaurantId: category.restaurantId,
    //   position: payload.position,
    //   status: StatusEnum.ACTIVE
    // })
    // if (isPositionExist) {
    //   throw new BadRequestException('Duplicate category position')
    // }
    const updated = await this.menuCategoryService.update(id, payload)
    if (!updated) throw new BadRequestException()
    await this.categoryHoursService.deleteMany({ categoryId: id })
    const hoursPayload = payload.hours
      .filter((period) => period.isSelected)
      .map((period) => ({
        restaurantId: category.restaurantId,
        categoryId: updated._id,
        restaurantPeriodId: period.id
      }))
    await this.categoryHoursService.createMany(hoursPayload)
    return updated
  }

  async updateCategoryPosition(payload: UpdateCategoryPositionDto) {
    const { items } = payload
    const bulkPayload = items.map((item) => ({
      updateOne: {
        filter: { _id: item.id },
        update: { position: item.position }
      }
    }))
    await this.menuCategoryService.bulkWrite(bulkPayload, { ordered: true })
    return { success: true }
  }
}
