import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsBoolean,
  IsEnum,
  ValidateNested,
  IsOptional
} from 'class-validator'
import { DayEnum } from 'src/modules/restaurant-hours/common/day.enum'

class CategoryHoursFields {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '634cfb9f984f966064984acd' })
  id: string

  @IsEnum(DayEnum)
  @IsNotEmpty()
  @ApiProperty({ enum: Object.values(DayEnum), example: DayEnum.MONDAY })
  label: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '09:00AM' })
  opensAt: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '12:00AM' })
  closesAt: string

  @IsBoolean()
  @ApiProperty({ example: true })
  isSelected: boolean
}
export class CreateMenuCategoryDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '630e55a0d9c30fd7cdcb424b' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Best Seller' })
  readonly name: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'description test' })
  readonly description: string

  @IsNumber()
  @ApiProperty({ example: 7 })
  public position: number

  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isShow: boolean

  @IsBoolean()
  @ApiProperty({ example: false })
  readonly isContainAlcohol: boolean

  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isDisplayOnWebAndApp: boolean

  @Type(() => CategoryHoursFields)
  @ValidateNested({ each: true })
  @ApiProperty({
    type: () => [CategoryHoursFields]
  })
  readonly hours: Array<CategoryHoursFields>
}
