import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import { ArrayNotEmpty, ValidateNested } from 'class-validator'
import { PositionObject } from 'src/modules/menu/dto/update-menu-position.dto'

export class UpdateCategoryPositionDto {
  @Type(() => PositionObject)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: [PositionObject] })
  items: PositionObject[]
}
