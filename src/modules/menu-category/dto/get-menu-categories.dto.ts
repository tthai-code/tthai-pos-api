import { IsString, IsOptional, IsNotEmpty } from 'class-validator'
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class GetMenuCategoryListDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  public buildQuery() {
    const result = {
      restaurantId: this.restaurantId,
      isDisplayOnWebAndApp: true,
      isShow: true,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
export class MenuCategoriesPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'position' })
  readonly sortBy: string = 'position'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'asc' })
  readonly sortOrder: string = 'asc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'category name' })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<restaurant-id>' })
  readonly restaurant: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          name: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      restaurantId: { $eq: !id ? this.restaurant : id },
      status: this.status || StatusEnum.ACTIVE
    }
    if (!id && !this.restaurant) delete result.restaurantId
    return result
  }

  public buildQueryPOS(id: string) {
    const result = {
      restaurantId: id,
      isShow: true,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
