import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'

export type CategoryHoursDocument = CategoryHoursFields & Document

@Schema({ timestamps: true, strict: true, collection: 'categoryHours' })
export class CategoryHoursFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'menuCategories' })
  categoryId: Types.ObjectId

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurantPeriod' })
  restaurantPeriodId: Types.ObjectId
}
export const CategoryHoursSchema =
  SchemaFactory.createForClass(CategoryHoursFields)
