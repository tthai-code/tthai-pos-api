import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type MenuCategoriesDocument = MenuCategoriesFields & Document

@Schema({ timestamps: true, collection: 'menuCategories' })
export class MenuCategoriesFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  name: string

  @Prop()
  description: string

  @Prop()
  position: number

  @Prop({ default: true })
  isShow: boolean

  @Prop({ default: false })
  isContainAlcohol: boolean

  @Prop({ default: false })
  isDisplayOnWebAndApp: boolean

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}

export const MenuCategoriesSchema =
  SchemaFactory.createForClass(MenuCategoriesFields)
MenuCategoriesSchema.plugin(mongoosePaginate)
MenuCategoriesSchema.plugin(authorStampCreatePlugin)
