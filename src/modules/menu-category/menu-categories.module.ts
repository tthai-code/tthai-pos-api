import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantHoursModule } from '../restaurant-hours/restaurant-hours.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { MenuModule } from '../menu/menu.module'
import { RestaurantHolidaysModule } from '../restaurant-holiday/restaurant-holiday.module'
import { MenuCategoriesController } from './controllers/menu-category.controller'
import { POSMenuCategoryController } from './controllers/pos-menu-category.controller'
import { MenuCategoriesOnlineController } from './controllers/menu-category-public-online.controller'

import { MenuCategoryLogic } from './logics/menu-category.logic'
import { MenuCategoryOnlineLogic } from './logics/menu-category-public-online-logic'
import { CategoryHoursSchema } from './schemas/category-hours.schema'
import { MenuCategoriesSchema } from './schemas/menu-category.schema'
import { CategoryHoursService } from './services/category-hours.service'
import { MenuCategoriesService } from './services/menu-category.service'
import { MenuCategoriesOnlineService } from './services/menu-category-public-online.service'
import { OpenMenuCategoryLogic } from './logics/menu-category-open.controller'
import { OpenCategoryController } from './controllers/open-category.controller'
import { WebSettingModule } from '../web-setting/web-setting.module'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    forwardRef(() => RestaurantHoursModule),
    forwardRef(() => RestaurantHolidaysModule),
    forwardRef(() => MenuModule),
    forwardRef(() => WebSettingModule),
    MongooseModule.forFeature([
      { name: 'menuCategories', schema: MenuCategoriesSchema },
      { name: 'categoryHours', schema: CategoryHoursSchema }
    ])
  ],
  providers: [
    MenuCategoriesService,
    MenuCategoriesOnlineService,
    MenuCategoryLogic,
    MenuCategoryOnlineLogic,
    CategoryHoursService,
    OpenMenuCategoryLogic
  ],
  controllers: [
    POSMenuCategoryController,
    MenuCategoriesController,
    MenuCategoriesOnlineController,
    OpenCategoryController
  ],
  exports: [
    MenuCategoriesService,
    CategoryHoursService,
    MenuCategoryOnlineLogic
  ]
})
export class MenuCategoriesModule {}
