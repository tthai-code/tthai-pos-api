import { Controller, Get, Query, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import {
  ApiBasicAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { GetMenuCategoryListDto } from '../dto/get-menu-categories.dto'
import { OpenCategoryListResponse } from '../entity/open-category.entity'
import { OpenMenuCategoryLogic } from '../logics/menu-category-open.controller'

@ApiBasicAuth()
@ApiTags('open/category')
@UseGuards(AuthGuard('basic'))
@Controller('v1/open/menu')
export class OpenCategoryController {
  constructor(private readonly openMenuCategoryLogic: OpenMenuCategoryLogic) {}

  @Get('category')
  @ApiOkResponse({ type: OpenCategoryListResponse })
  @ApiOperation({
    summary: 'Get all menu category is display on app.',
    description: 'use basic auth'
  })
  async getAllCategory(@Query() query: GetMenuCategoryListDto) {
    return await this.openMenuCategoryLogic.getCategoryList(query)
  }
}
