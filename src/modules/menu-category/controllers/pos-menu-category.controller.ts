import { Controller, Get, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse
} from '@nestjs/swagger'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { errorResponse } from 'src/utilities/errorResponse'
import { MenuCategoriesPaginateDto } from '../dto/get-menu-categories.dto'
import { POSMenuCategoryResponse } from '../entity/menu-category.entity'
import { MenuCategoriesService } from '../services/menu-category.service'

@ApiBearerAuth()
@ApiTags('pos/menu-category')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/menu-category')
export class POSMenuCategoryController {
  constructor(private readonly menuCategoryService: MenuCategoriesService) {}

  private readonly menuCategoryPaginateDto = new MenuCategoriesPaginateDto()

  @Get()
  @ApiOkResponse({ type: () => POSMenuCategoryResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', '/v1/pos/menu-category')
  )
  @ApiOperation({
    summary: 'Get all category for display on POS',
    description: 'use *bearer* `staff_token`'
  })
  async getAllCategory() {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.menuCategoryService.getAll(
      this.menuCategoryPaginateDto.buildQueryPOS(restaurantId),
      {
        name: 1,
        isContainAlcohol: 1,
        position: 1
      },
      { sort: { position: 1 } }
    )
  }
}
