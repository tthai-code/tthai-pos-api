import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { MenuCategoriesPaginateDto } from '../dto/get-menu-categories.dto'
import { CreateMenuCategoryDto } from '../dto/create-menu-category.dto.'
import { MenuCategoriesService } from '../services/menu-category.service'
import { UpdateMenuCategoryDto } from '../dto/update-menu-categories.dto'
import { MenuCategoryLogic } from '../logics/menu-category.logic'
import { CategoryHoursService } from '../services/category-hours.service'
import {
  CategoryHoursResponse,
  CreatedMenuCategoryResponse,
  DeletedMenuCategoryResponse,
  GetMenuCategoryResponse,
  PaginateMenuCategoryResponse
} from '../entity/menu-category.entity'
import { UpdateCategoryPositionDto } from '../dto/update-category-position.dto'

@ApiBearerAuth()
@ApiTags('menu-category')
@Controller('v1/menu-category')
@UseGuards(AdminAuthGuard)
export class MenuCategoriesController {
  constructor(
    private readonly menuCategoriesService: MenuCategoriesService,
    private readonly menuCategoryLogic: MenuCategoryLogic,
    private readonly categoryHoursService: CategoryHoursService
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaginateMenuCategoryResponse })
  @ApiOperation({ summary: 'Get Menu Categories List' })
  async getMenuCategories(@Query() query: MenuCategoriesPaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.menuCategoriesService.paginate(
      query.buildQuery(restaurantId),
      query
    )
  }

  @Get(':id/category')
  @ApiOkResponse({ type: () => GetMenuCategoryResponse })
  @ApiOperation({ summary: 'Get Menu Category By ID' })
  async getMenuCategory(@Param('id') id: string) {
    return await this.menuCategoryLogic.getCategoryById(id)
  }

  @Post()
  @ApiCreatedResponse({ type: () => CreatedMenuCategoryResponse })
  @ApiOperation({ summary: 'Create Menu Category' })
  async createMenuCategory(@Body() menuCategory: CreateMenuCategoryDto) {
    return this.menuCategoryLogic.createMenuCategory(menuCategory)
  }

  @Put(':id')
  @ApiOkResponse({ type: () => CreatedMenuCategoryResponse })
  @ApiOperation({ summary: 'Update Menu Category' })
  async updateMenuCategory(
    @Body() category: UpdateMenuCategoryDto,
    @Param('id') id: string
  ) {
    return this.menuCategoryLogic.updateMenuCategory(id, category)
  }

  @Delete(':id')
  @ApiOkResponse({ type: () => DeletedMenuCategoryResponse })
  @ApiOperation({ summary: 'Delete Menu Category' })
  async deleteMenuCategory(@Param('id') id: string) {
    await this.categoryHoursService.deleteMany({ categoryId: id })
    return this.menuCategoriesService.update(id, { status: StatusEnum.DELETED })
  }

  @Get('/hours')
  @ApiOkResponse({ type: () => CategoryHoursResponse })
  @ApiOperation({ summary: 'Get all category hours for create category' })
  async getCategoryHoursForCreate() {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.menuCategoryLogic.getCategoryHoursForCreate(restaurantId)
  }

  @Put('position/all')
  @ApiOperation({ summary: 'Update Category Position' })
  async updateCategoryPosition(@Body() payload: UpdateCategoryPositionDto) {
    return await this.menuCategoryLogic.updateCategoryPosition(payload)
  }
}
