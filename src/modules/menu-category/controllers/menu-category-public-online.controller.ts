import { Controller, Get, Param, Query } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'

import { MenuCategoriesOnlineService } from '../services/menu-category-public-online.service'
import { MenuCategoryOnlineLogic } from '../logics/menu-category-public-online-logic'
import { CategoryHoursService } from '../services/category-hours.service'
import { MenuPaginateDto } from 'src/modules/menu/dto/get-menu.dto'
import {
  GetMenuCategoryResponse,
  GetOnlineMenuCategoryListResponse
} from '../entity/menu-category.entity'
import { GetMenuCategoryListDto } from '../dto/get-menu-categories.dto'
import { GetOnlineCategoryWithMenuResponse } from '../entity/online-category.entity'

@ApiTags('menu-category-public-online')
@Controller('v1/public/online/menu-category')
export class MenuCategoriesOnlineController {
  constructor(
    private readonly menuCategoriesOnlineService: MenuCategoriesOnlineService,
    private readonly menuCategoryOnlineLogic: MenuCategoryOnlineLogic,
    private readonly categoryHoursService: CategoryHoursService
  ) {}

  @Get()
  @ApiOkResponse({ type: () => GetOnlineCategoryWithMenuResponse })
  @ApiOperation({
    summary: 'Get Menu Categories List Online',
    description: 'use query limit=9'
  })
  async getMenuCategories(@Query() query: MenuPaginateDto) {
    return await this.menuCategoryOnlineLogic.getMenuCategoryByRestaurantId(
      query.restaurantId,
      query
    )
  }

  @Get('list')
  @ApiOkResponse({ type: () => GetOnlineMenuCategoryListResponse })
  @ApiOperation({ summary: 'Get Menu Category List' })
  async getMenuCategoryList(@Query() query: GetMenuCategoryListDto) {
    return await this.menuCategoriesOnlineService.getAll(
      query.buildQuery(),
      {
        status: 0,
        createdAt: 0,
        updatedAt: 0,
        createdBy: 0,
        updatedBy: 0
      },
      { sort: { position: 1 } }
    )
  }

  @Get(':id/category')
  @ApiOkResponse({ type: () => GetMenuCategoryResponse })
  @ApiOperation({ summary: 'Get Menu Category By ID' })
  async getMenuCategory(@Param('id') id: string) {
    return await this.menuCategoryOnlineLogic.getCategoryById(id)
  }
}
