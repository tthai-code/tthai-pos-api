import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'
import { PaginateMenuResponse } from 'src/modules/menu/entity/menu.entity'

class CategoryHoursFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  label: string

  @ApiProperty()
  opens_at: string

  @ApiProperty()
  closes_at: string

  @ApiProperty()
  is_selected: boolean
}

class POSMenuCategoryFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  position: number

  @ApiProperty()
  is_contain_alcohol: boolean
}
class BaseMenuCategoryFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  restaurant_id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  position: number

  @ApiProperty()
  is_show: boolean

  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty()
  is_display_on_web_and_app: boolean
}

class CreatedMenuCategoryFields extends TimestampResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  restaurant_id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  position: number

  @ApiProperty()
  is_show: boolean

  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty()
  is_display_on_web_and_app: boolean
}

class MenuCategoryWithHoursFields extends BaseMenuCategoryFields {
  @ApiProperty({ type: [CategoryHoursFields] })
  hours: Array<CategoryHoursFields>
}

class MenuCategoryWithMenu extends BaseMenuCategoryFields {
  @ApiProperty({ type: [CategoryHoursFields] })
  menus: Array<PaginateMenuResponse>
}

class PaginateMenuCategoryFields extends TimestampResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  restaurant_id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  position: number

  @ApiProperty()
  is_show: boolean

  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty()
  is_display_on_web_and_app: boolean
}

export class PaginateMenuCategoryResponse extends PaginateResponseDto<
  Array<PaginateMenuCategoryFields>
> {
  @ApiProperty({
    type: [PaginateMenuCategoryFields],
    example: [
      {
        status: 'active',
        is_display_on_web_and_app: true,
        is_contain_alcohol: false,
        is_show: true,
        description: 'test desc',
        position: 2,
        name: 'Best Seller',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        created_at: '2022-10-17T07:38:35.687Z',
        updated_at: '2022-10-17T08:02:32.237Z',
        updated_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        created_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        id: '634d067bcc64f608779162f5'
      }
    ]
  })
  results: Array<PaginateMenuCategoryFields>
}

export class GetMenuCategoryResponse extends ResponseDto<MenuCategoryWithHoursFields> {
  @ApiProperty({
    type: MenuCategoryWithHoursFields,
    example: {
      name: 'Best Seller',
      position: 2,
      description: 'test desc',
      is_show: true,
      is_contain_alcohol: false,
      is_display_on_web_and_app: true,
      hours: [
        {
          id: '634d03669a40151f6e4243a0',
          label: 'MONDAY',
          opens_at: '09:00AM',
          closes_at: '12:00AM',
          is_selected: true
        },
        {
          id: '634d03669a40151f6e4243a1',
          label: 'MONDAY',
          opens_at: '03:00PM',
          closes_at: '09:00PM',
          is_selected: true
        },
        {
          id: '634d03669a40151f6e4243a2',
          label: 'TUESDAY',
          opens_at: '08:00AM',
          closes_at: '05:00PM',
          is_selected: false
        },
        {
          id: '634d03669a40151f6e4243a3',
          label: 'TUESDAY',
          opens_at: '11:00PM',
          closes_at: '00:30AM',
          is_selected: false
        }
      ]
    }
  })
  data: MenuCategoryWithHoursFields
}

export class GetMenuCategoryWithMenuResponse extends ResponseDto<CreatedMenuCategoryFields> {
  @ApiProperty({
    type: CreatedMenuCategoryFields,
    example: {
      status: 'active',
      is_display_on_web_and_app: true,
      is_contain_alcohol: false,
      is_show: true,
      description: 'test desc',
      position: 2,
      name: 'Best Seller',
      menus: [
        {
          id: '6310667a062dfd651b6e5023',
          status: 'active',
          note: '',
          cover_image: null,
          image_urls: [
            'https://storage.googleapis.com/tthai-pos-staging/upload/166456465419581603.webp'
          ],
          price: 12,
          category: ['6336bb21c64b8b0998892950', '634d067bcc64f608779162f5'],
          name: 'Mushroom Dumplings',
          restaurant_id: '630eff5751c2eac55f52662c',
          __v: 3,
          description:
            'Pan grilled dumplings stuffed with shiitake mushroom and chive. Served with ginger sauce.',
          native_name: 'เกี๊ยวเห็ดหอม',
          position: 999
        }
      ],
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-10-17T07:34:07.985Z',
      updated_at: '2022-10-17T07:34:07.985Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '634d056f24810adce81a5620'
    }
  })
  data: CreatedMenuCategoryFields
}

export class CreatedMenuCategoryResponse extends ResponseDto<CreatedMenuCategoryFields> {
  @ApiProperty({
    type: CreatedMenuCategoryFields,
    example: {
      status: 'active',
      is_display_on_web_and_app: true,
      is_contain_alcohol: false,
      is_show: true,
      description: 'test desc',
      position: 2,
      name: 'Best Seller',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-10-17T07:34:07.985Z',
      updated_at: '2022-10-17T07:34:07.985Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '634d056f24810adce81a5620'
    }
  })
  data: CreatedMenuCategoryFields
}

export class DeletedMenuCategoryResponse extends ResponseDto<CreatedMenuCategoryFields> {
  @ApiProperty({
    type: CreatedMenuCategoryFields,
    example: {
      status: 'deleted',
      is_display_on_web_and_app: true,
      is_contain_alcohol: false,
      is_show: true,
      description: 'test desc',
      position: 2,
      name: 'Best Seller',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-10-17T07:34:07.985Z',
      updated_at: '2022-10-17T07:34:07.985Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '634d056f24810adce81a5620'
    }
  })
  data: CreatedMenuCategoryFields
}

export class CategoryHoursResponse extends ResponseDto<
  Array<CategoryHoursFields>
> {
  @ApiProperty({
    type: [CategoryHoursFields],
    example: [
      {
        id: '634d0c62eb8b609259fb3c72',
        label: 'MONDAY',
        opens_at: '09:00AM',
        closes_at: '12:00AM',
        is_selected: false
      },
      {
        id: '634d0c62eb8b609259fb3c73',
        label: 'MONDAY',
        opens_at: '03:00PM',
        closes_at: '09:00PM',
        is_selected: false
      },
      {
        id: '634d0c62eb8b609259fb3c74',
        label: 'TUESDAY',
        opens_at: '08:00AM',
        closes_at: '05:00PM',
        is_selected: false
      },
      {
        id: '634d0c62eb8b609259fb3c75',
        label: 'TUESDAY',
        opens_at: '11:00PM',
        closes_at: '00:30AM',
        is_selected: false
      }
    ]
  })
  data: Array<CategoryHoursFields>
}

export class POSMenuCategoryResponse extends ResponseDto<
  Array<POSMenuCategoryFields>
> {
  @ApiProperty({
    type: [POSMenuCategoryFields],
    example: [
      {
        is_contain_alcohol: false,
        position: 2,
        name: 'Best Seller',
        id: '634d067bcc64f608779162f5'
      }
    ]
  })
  data: Array<POSMenuCategoryFields>
}

class OnlineMenuCategoryFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  restaurant_id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  position: number

  @ApiProperty()
  is_show: boolean

  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty()
  is_display_on_web_and_app: boolean
}

export class GetOnlineMenuCategoryListResponse extends ResponseDto<
  OnlineMenuCategoryFields[]
> {
  @ApiProperty({
    type: [OnlineMenuCategoryFields],
    example: [
      {
        name: 'Appetizers',
        restaurant_id: '630eff5751c2eac55f52662c',
        is_contain_alcohol: false,
        is_display_on_web_and_app: true,
        is_show: true,
        position: 1,
        description: '',
        id: '6336bb21c64b8b0998892950'
      },
      {
        name: 'Soup',
        restaurant_id: '630eff5751c2eac55f52662c',
        is_contain_alcohol: false,
        is_display_on_web_and_app: true,
        is_show: true,
        position: 2,
        description: '',
        id: '6337412cc64b8b0998892c62'
      }
    ]
  })
  data: OnlineMenuCategoryFields[]
}
