import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class MenuObject {
  @ApiProperty()
  status: string
  @ApiProperty()
  note: string
  @ApiProperty()
  cover_image: string
  @ApiProperty()
  image_urls: string[]
  @ApiProperty()
  price: number
  @ApiProperty()
  category: string[]
  @ApiProperty()
  name: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  description: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  position: number
  @ApiProperty()
  id: string
  @ApiProperty()
  is_available: boolean
}

class CategoryObjectWithMenu {
  @ApiProperty()
  status: string
  @ApiProperty()
  is_display_on_web_and_app: boolean
  @ApiProperty()
  is_contain_alcohol: boolean
  @ApiProperty()
  is_show: boolean
  @ApiProperty()
  position: number
  @ApiProperty()
  name: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  description: string
  @ApiProperty()
  id: string
  @ApiProperty()
  is_available: boolean
  @ApiProperty({ type: [MenuObject] })
  menus: MenuObject[]
}

export class GetOnlineCategoryWithMenuResponse extends ResponseDto<
  CategoryObjectWithMenu[]
> {
  @ApiProperty({
    type: [CategoryObjectWithMenu],
    example: [
      {
        status: 'active',
        is_display_on_web_and_app: true,
        is_contain_alcohol: false,
        is_show: true,
        position: 1,
        name: 'Appetizers',
        restaurant_id: '630eff5751c2eac55f52662c',
        description: '',
        id: '6336bb21c64b8b0998892950',
        is_available: true,
        menus: [
          {
            status: 'active',
            note: '',
            cover_image: null,
            image_urls: [],
            price: 13,
            category: ['6336bb21c64b8b0998892950'],
            name: 'Chicken Satay',
            restaurant_id: '630eff5751c2eac55f52662c',
            description: 'test desc',
            native_name: 'test nativeName',
            position: 999,
            id: '63373f60c64b8b0998892bd7',
            is_available: true
          }
        ]
      }
    ]
  })
  data: CategoryObjectWithMenu[]
}
