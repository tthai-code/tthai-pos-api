import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class CategoryObject {
  name: string
  restaurant_id: string
  is_contain_alcohol: boolean
  description: string
  id: string
}

export class OpenCategoryListResponse extends ResponseDto<CategoryObject[]> {
  @ApiProperty({
    type: CategoryObject,
    example: [
      {
        name: 'Appetizers',
        restaurant_id: '630eff5751c2eac55f52662c',
        is_contain_alcohol: false,
        description: '',
        id: '6336bb21c64b8b0998892950'
      },
      {
        name: 'Soup',
        restaurant_id: '630eff5751c2eac55f52662c',
        is_contain_alcohol: false,
        description: '',
        id: '6337412cc64b8b0998892c62'
      }
    ]
  })
  data: CategoryObject[]
}
