import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { MenuCategoriesDocument } from '../schemas/menu-category.schema'
import { MenuCategoriesPaginateDto } from '../dto/get-menu-categories.dto'

@Injectable({ scope: Scope.REQUEST })
export class MenuCategoriesOnlineService {
  constructor(
    @InjectModel('menuCategories')
    private readonly MenuCategoriesModel:
      | PaginateModel<MenuCategoriesDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<MenuCategoriesDocument | any> {
    return this.MenuCategoriesModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.MenuCategoriesModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<MenuCategoriesDocument> {
    return this.MenuCategoriesModel
  }

  async getSession(): Promise<ClientSession> {
    await this.MenuCategoriesModel.createCollection()

    return this.MenuCategoriesModel.startSession()
  }

  async create(payload: any): Promise<MenuCategoriesDocument> {
    const document = new this.MenuCategoriesModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(
    id: string,
    MenuCategories: any
  ): Promise<MenuCategoriesDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...MenuCategories }).save()
  }

  async delete(id: string): Promise<MenuCategoriesDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any, options?: any): Promise<any[]> {
    return this.MenuCategoriesModel.find(condition, project, options)
  }

  getAllWithSorted(
    condition: any,
    project?: any,
    sort = { position: 1 }
  ): Promise<any[]> {
    return this.MenuCategoriesModel.find(condition, project).sort(sort)
  }

  findById(id: number): Promise<any> {
    return this.MenuCategoriesModel.findById(id)
  }

  findOne(condition: any): Promise<MenuCategoriesDocument> {
    return this.MenuCategoriesModel.findOne(condition)
  }

  paginate(
    query: any,
    queryParam: MenuCategoriesPaginateDto,
    select?: any
  ): Promise<PaginateResult<MenuCategoriesDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }
    return this.MenuCategoriesModel.paginate(query, options)
  }
}
