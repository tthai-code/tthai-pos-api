import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CategoryHoursDocument } from '../schemas/category-hours.schema'

@Injectable({ scope: Scope.REQUEST })
export class CategoryHoursService {
  constructor(
    @InjectModel('categoryHours')
    private readonly CategoryHoursModel: Model<CategoryHoursDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<CategoryHoursDocument | any> {
    return this.CategoryHoursModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any,
    project?: any
  ): Promise<CategoryHoursDocument | any> {
    return this.CategoryHoursModel.find(condition, project)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.CategoryHoursModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<CategoryHoursDocument> {
    return this.CategoryHoursModel
  }

  async getSession(): Promise<ClientSession> {
    await this.CategoryHoursModel.createCollection()

    return this.CategoryHoursModel.startSession()
  }

  async create(payload: any): Promise<CategoryHoursDocument> {
    const document = new this.CategoryHoursModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(
    id: string,
    MenuCategories: any
  ): Promise<CategoryHoursDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...MenuCategories }).save()
  }

  async delete(id: string): Promise<CategoryHoursDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  async createMany(payload: any): Promise<CategoryHoursDocument> {
    return this.CategoryHoursModel.insertMany(payload)
  }

  async deleteMany(condition: any): Promise<void> {
    return this.CategoryHoursModel.deleteMany(condition)
  }

  async transactionDeleteAll(condition, session): Promise<void> {
    return this.CategoryHoursModel.deleteMany(condition, { session })
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.CategoryHoursModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.CategoryHoursModel.findById(id)
  }

  findOne(condition: any): Promise<CategoryHoursDocument> {
    return this.CategoryHoursModel.findOne(condition)
  }
}
