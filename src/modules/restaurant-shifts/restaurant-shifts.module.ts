import { forwardRef, Global, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { POSRestaurantShiftsController } from './controllers/pos-restaurant-shifts.controller'
import { RestaurantShiftsController } from './controllers/restaurant-shifts.controller'
import { RestaurantShiftsSchema } from './schemas/restaurant-shifts.schema'
import { RestaurantShiftsService } from './services/restaurant-shifts.service'

@Global()
@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([
      { name: 'shifts', schema: RestaurantShiftsSchema }
    ])
  ],
  providers: [RestaurantShiftsService],
  controllers: [RestaurantShiftsController, POSRestaurantShiftsController],
  exports: [RestaurantShiftsService]
})
export class RestaurantShiftsModule {}
