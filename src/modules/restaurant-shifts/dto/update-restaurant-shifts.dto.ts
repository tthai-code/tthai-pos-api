import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'

class ShiftFieldsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, example: '12:00AM' })
  startAt: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, example: '12:00AM' })
  endAt: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string
}

export class UpdateShiftsDto {
  @Type(() => ShiftFieldsDto)
  @ValidateNested({ each: true })
  @IsNotEmpty()
  @ApiProperty({ type: () => [ShiftFieldsDto] })
  readonly shifts: Array<ShiftFieldsDto>
}
