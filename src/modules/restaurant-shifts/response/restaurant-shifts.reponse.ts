export const getShiftsResponse = {
  schema: {
    type: 'object',
    example: {
      message: 'done',
      data: {
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        shifts: [
          {
            start_at: '09:00AM',
            end_at: '12:00AM',
            status: 'active'
          },
          {
            start_at: '01:PM',
            end_at: '06:00PM',
            status: 'active'
          }
        ],
        updated_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        updated_at: '2022-10-05T07:01:26.119Z',
        id: '633d2b491ef7a2c53f0c885b'
      }
    }
  }
}
