import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type RestaurantShiftsDocument = RestaurantShiftsField & Document

@Schema({ _id: false, timestamps: false, strict: true })
class ShiftPeriodFieldsSchema {
  @Prop({ required: true })
  startAt: string

  @Prop({ required: true })
  endAt: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string
}

@Schema({ timestamps: true, strict: true, collection: 'shifts' })
export class RestaurantShiftsField {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop()
  shifts: Array<ShiftPeriodFieldsSchema>

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const RestaurantShiftsSchema = SchemaFactory.createForClass(
  RestaurantShiftsField
)
RestaurantShiftsSchema.plugin(mongoosePaginate)
RestaurantShiftsSchema.plugin(authorStampCreatePlugin)
