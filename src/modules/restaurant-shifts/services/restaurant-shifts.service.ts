import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantShiftsDocument } from '../schemas/restaurant-shifts.schema'

@Injectable({ scope: Scope.REQUEST })
export class RestaurantShiftsService {
  constructor(
    @InjectModel('shifts')
    private readonly RestaurantShiftsModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<RestaurantShiftsDocument | any> {
    return this.RestaurantShiftsModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any
  ): Promise<RestaurantShiftsDocument | any> {
    return this.RestaurantShiftsModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.RestaurantShiftsModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<RestaurantShiftsDocument> {
    return this.RestaurantShiftsModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.RestaurantShiftsModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.RestaurantShiftsModel.createCollection()

    return this.RestaurantShiftsModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<RestaurantShiftsDocument> {
    const document = new this.RestaurantShiftsModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<RestaurantShiftsDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<RestaurantShiftsDocument> {
    const document = new this.RestaurantShiftsModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<RestaurantShiftsDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<RestaurantShiftsDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<RestaurantShiftsDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.RestaurantShiftsModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.RestaurantShiftsModel.findById(id)
  }

  findOne(condition: any): Promise<RestaurantShiftsDocument> {
    return this.RestaurantShiftsModel.findOne(condition)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<RestaurantShiftsDocument> {
    return this.RestaurantShiftsModel.findOne(condition, options)
  }
}
