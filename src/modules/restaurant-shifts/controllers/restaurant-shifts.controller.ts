import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { UpdateShiftsDto } from '../dto/update-restaurant-shifts.dto'
import { getShiftsResponse } from '../response/restaurant-shifts.reponse'
import { RestaurantShiftsService } from '../services/restaurant-shifts.service'

@ApiBearerAuth()
@ApiTags('restaurant-shifts')
@UseGuards(AdminAuthGuard)
@Controller('v1/restaurant')
export class RestaurantShiftsController {
  constructor(
    private readonly restaurantShiftsService: RestaurantShiftsService,
    private readonly restaurantService: RestaurantService
  ) {}

  @Get(':restaurantId/shift')
  @ApiOkResponse(getShiftsResponse)
  @ApiOperation({ summary: 'Get Restaurant Shifts by Restaurant ID' })
  async getShiftsByRestaurantID(@Param('restaurantId') id: string) {
    const restaurantId = await this.restaurantService.findOne({ _id: id })
    if (!restaurantId) throw new NotFoundException('Not Found Restaurant.')
    const restaurantShifts = await this.restaurantShiftsService.findOne({
      restaurantId: id
    })
    if (restaurantShifts) {
      return restaurantShifts
    }
    return {
      restaurant_id: id,
      shifts: []
    }
  }

  @Patch(':restaurantId/shift')
  @ApiOperation({ summary: 'Update Restaurant Shifts by Restaurant ID' })
  async updateShiftsByRestaurantID(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateShiftsDto
  ) {
    const restaurantId = await this.restaurantService.findOne({ _id: id })
    if (!restaurantId) throw new NotFoundException('Not Found Restaurant.')
    const isShiftExisted = await this.restaurantShiftsService.findOne({
      restaurantId: id
    })
    if (isShiftExisted) {
      return await this.restaurantShiftsService.findOneAndUpdate(
        { restaurantId: id },
        payload
      )
    } else {
      return await this.restaurantShiftsService.create({
        restaurantId: id,
        shifts: payload.shifts
      })
    }
  }
}
