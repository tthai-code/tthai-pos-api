import {
  Controller,
  Get,
  NotFoundException,
  Param,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { getShiftsResponse } from '../response/restaurant-shifts.reponse'
import { RestaurantShiftsService } from '../services/restaurant-shifts.service'

@ApiBearerAuth()
@ApiTags('pos/restaurant-shifts')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/restaurant')
export class POSRestaurantShiftsController {
  constructor(
    private readonly restaurantShiftsService: RestaurantShiftsService,
    private readonly restaurantService: RestaurantService
  ) {}

  @Get(':restaurantId/shift')
  @ApiOkResponse(getShiftsResponse)
  @ApiOperation({ summary: 'Get Restaurant Shifts by Restaurant ID' })
  async getShiftsByRestaurantID(@Param('restaurantId') id: string) {
    const restaurantId = await this.restaurantService.findOne({ _id: id })
    if (!restaurantId) throw new NotFoundException('Not Found Restaurant.')
    return await this.restaurantShiftsService.findOne({ restaurantId: id })
  }

  // @Patch(':restaurantId/shift')
  // @ApiOperation({ summary: 'Update Restaurant Shifts by Restaurant ID' })
  // async updateShiftsByRestaurantID(
  //   @Param('restaurantId') id: string,
  //   @Body() payload: UpdateShiftsDto
  // ) {
  //   const restaurantId = await this.restaurantService.findOne({ _id: id })
  //   if (!restaurantId) throw new NotFoundException('Not Found Restaurant.')
  //   return await this.restaurantShiftsService.findOneAndUpdate(
  //     { restaurantId: id },
  //     payload
  //   )
  // }
}
