import { forwardRef, Global, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { MenuCategoriesModule } from '../menu-category/menu-categories.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { RestaurantHoursController } from './controllers/restaurant-hours.controller'
import { RestaurantHoursLogic } from './logics/restaurant-hours.logic'
import { RestaurantDaySchema } from './schemas/restaurant-day.schema'
import { RestaurantPeriodSchema } from './schemas/restaurant-period.schema'
import { RestaurantDayService } from './services/restaurant-day.service'
import { RestaurantPeriodService } from './services/restaurant-period.service'

@Global()
@Module({
  imports: [
    forwardRef(() => MenuCategoriesModule),
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([
      { name: 'restaurantDay', schema: RestaurantDaySchema },
      { name: 'restaurantPeriod', schema: RestaurantPeriodSchema }
    ])
  ],
  providers: [
    RestaurantPeriodService,
    RestaurantDayService,
    RestaurantHoursLogic
  ],
  controllers: [RestaurantHoursController],
  exports: [RestaurantDayService, RestaurantPeriodService, RestaurantHoursLogic]
})
export class RestaurantHoursModule {}
