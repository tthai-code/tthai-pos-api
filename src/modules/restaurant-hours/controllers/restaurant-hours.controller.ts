import { Body, Controller, Get, Param, Patch, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { UpdateHoursDto } from '../dto/update-hours'
import { RestaurantHoursResponse } from '../entity/restaurant-hours.entity'
import { RestaurantHoursLogic } from '../logics/restaurant-hours.logic'

@ApiBearerAuth()
@ApiTags('restaurant-hours')
@UseGuards(AdminAuthGuard)
@Controller('v1/restaurant')
export class RestaurantHoursController {
  constructor(private readonly restaurantHoursLogic: RestaurantHoursLogic) {}

  @Get(':restaurantId/hours')
  @ApiOkResponse({ type: () => RestaurantHoursResponse })
  @ApiOperation({ summary: 'Get Restaurant Hours by Restaurant ID' })
  async getAllRestaurantHours(@Param('restaurantId') id: string) {
    return await this.restaurantHoursLogic.getRestaurantHours(id)
  }

  @Patch(':restaurantId/hours')
  @ApiOkResponse({ type: () => RestaurantHoursResponse })
  @ApiOperation({ summary: 'Update Restaurant Hours by Restaurant ID' })
  async updateHours(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateHoursDto
  ) {
    return await this.restaurantHoursLogic.updateHours(id, payload)
  }
}
