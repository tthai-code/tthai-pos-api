import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  ArrayNotEmpty,
  IsBoolean,
  IsEnum,
  IsNotEmpty,
  IsString,
  ValidateNested
} from 'class-validator'
import { DayEnum } from '../common/day.enum'

class PeriodFieldsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, example: '09:00AM' })
  opensAt: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, example: '12:00AM' })
  closesAt: string
}

class HoursFieldsDto {
  @IsEnum(DayEnum)
  @ApiProperty({ enum: Object.values(DayEnum), example: DayEnum.MONDAY })
  readonly label: string

  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty({ example: false })
  readonly isClosed: boolean

  @Type(() => PeriodFieldsDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [PeriodFieldsDto] })
  readonly period: Array<PeriodFieldsDto>
}

export class UpdateHoursDto {
  @Type(() => HoursFieldsDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({
    type: () => [HoursFieldsDto],
    example: {
      hours: [
        {
          label: 'MONDAY',
          isClosed: false,
          period: [
            {
              opensAt: '09:00AM',
              closesAt: '12:00AM'
            }
          ]
        },
        {
          label: 'TUESDAY',
          isClosed: true,
          period: []
        },
        {
          label: 'WEDNESDAY',
          isClosed: true,
          period: []
        },
        {
          label: 'THURSDAY',
          isClosed: true,
          period: []
        },
        {
          label: 'FRIDAY',
          isClosed: true,
          period: []
        },
        {
          label: 'SATURDAY',
          isClosed: true,
          period: []
        },
        {
          label: 'SUNDAY',
          isClosed: true,
          period: []
        }
      ]
    }
  })
  readonly hours: Array<HoursFieldsDto>
}
