import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { RestaurantPeriodDocument } from '../schemas/restaurant-period.schema'

@Injectable({ scope: Scope.REQUEST })
export class RestaurantPeriodService {
  constructor(
    @InjectModel('restaurantPeriod')
    private readonly RestaurantPeriodModel:
      | any
      | Model<RestaurantPeriodDocument>,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<RestaurantPeriodDocument | any> {
    return this.RestaurantPeriodModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any
  ): Promise<RestaurantPeriodDocument | any> {
    return this.RestaurantPeriodModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.RestaurantPeriodModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<RestaurantPeriodDocument> {
    return this.RestaurantPeriodModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.RestaurantPeriodModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.RestaurantPeriodModel.createCollection()

    return this.RestaurantPeriodModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<RestaurantPeriodDocument> {
    const document = new this.RestaurantPeriodModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<RestaurantPeriodDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<RestaurantPeriodDocument> {
    const document = new this.RestaurantPeriodModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<RestaurantPeriodDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<RestaurantPeriodDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  getAll(condition: any, project?: any, options?: any): Promise<any[]> {
    return this.RestaurantPeriodModel.find(condition, project, options)
  }

  findById(id: number): Promise<any> {
    return this.RestaurantPeriodModel.findById(id)
  }

  findOne(condition: any): Promise<RestaurantPeriodDocument> {
    return this.RestaurantPeriodModel.findOne(condition)
  }

  deleteAll(condition): Promise<void> {
    return this.RestaurantPeriodModel.deleteMany(condition)
  }

  async transactionDeleteAll(condition, session): Promise<void> {
    return this.RestaurantPeriodModel.deleteMany(condition, { session })
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<RestaurantPeriodDocument> {
    return this.RestaurantPeriodModel.findOne(condition, options)
  }
}
