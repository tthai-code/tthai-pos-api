import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { RestaurantDayDocument } from '../schemas/restaurant-day.schema'

@Injectable({ scope: Scope.REQUEST })
export class RestaurantDayService {
  constructor(
    @InjectModel('restaurantDay')
    private readonly RestaurantDayModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<RestaurantDayDocument | any> {
    return this.RestaurantDayModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any
  ): Promise<RestaurantDayDocument | any> {
    return this.RestaurantDayModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.RestaurantDayModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<RestaurantDayDocument> {
    return this.RestaurantDayModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.RestaurantDayModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.RestaurantDayModel.createCollection()

    return this.RestaurantDayModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<RestaurantDayDocument> {
    const document = new this.RestaurantDayModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<RestaurantDayDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateCondition(
    condition: any,
    product: any,
    session: ClientSession
  ): Promise<RestaurantDayDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<RestaurantDayDocument> {
    const document = new this.RestaurantDayModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async createMany(payload: any): Promise<RestaurantDayDocument> {
    return this.RestaurantDayModel.insertMany(payload)
  }

  async update(id: string, payload: any): Promise<RestaurantDayDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<RestaurantDayDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  getAll(condition: any, project?: any, options?: any): Promise<any[]> {
    return this.RestaurantDayModel.find(condition, project, options)
  }

  findById(id: number): Promise<any> {
    return this.RestaurantDayModel.findById(id)
  }

  findOne(condition: any): Promise<RestaurantDayDocument> {
    return this.RestaurantDayModel.findOne(condition)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<RestaurantDayDocument> {
    return this.RestaurantDayModel.findOne(condition, options)
  }
}
