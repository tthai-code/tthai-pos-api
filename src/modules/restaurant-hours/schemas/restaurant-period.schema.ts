import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { DayEnum } from '../common/day.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { StatusEnum } from 'src/common/enum/status.enum'

export type RestaurantPeriodDocument = RestaurantPeriodFields & Document

@Schema({ timestamps: true, strict: true, collection: 'restaurantPeriods' })
export class RestaurantPeriodFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ enum: Object.values(DayEnum) })
  label: string

  @Prop({ required: true })
  opensAt: string

  @Prop({ required: true })
  closesAt: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const RestaurantPeriodSchema = SchemaFactory.createForClass(
  RestaurantPeriodFields
)
RestaurantPeriodSchema.plugin(authorStampCreatePlugin)
