import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { DayEnum } from '../common/day.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type RestaurantDayDocument = RestaurantDayFields & Document

@Schema({ timestamps: true, strict: true, collection: 'restaurantDays' })
export class RestaurantDayFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ enum: Object.values(DayEnum) })
  label: string

  @Prop({ default: true })
  isClosed: boolean

  @Prop()
  position: number

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const RestaurantDaySchema =
  SchemaFactory.createForClass(RestaurantDayFields)
RestaurantDaySchema.plugin(authorStampCreatePlugin)
