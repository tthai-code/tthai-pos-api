import { Injectable, NotFoundException } from '@nestjs/common'
import { classToPlain } from 'class-transformer'
import * as dayjs from 'dayjs'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { CategoryHoursService } from 'src/modules/menu-category/services/category-hours.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { DayEnum } from '../common/day.enum'
import { UpdateHoursDto } from '../dto/update-hours'
import { RestaurantDayService } from '../services/restaurant-day.service'
import { RestaurantPeriodService } from '../services/restaurant-period.service'

@Injectable()
export class RestaurantHoursLogic {
  constructor(
    private readonly restaurantDayService: RestaurantDayService,
    private readonly restaurantPeriodService: RestaurantPeriodService,
    private readonly restaurantService: RestaurantService,
    private readonly categoryHoursService: CategoryHoursService
  ) {}

  private dayScoreQuery() {
    return {
      $switch: {
        branches: [
          {
            case: { $eq: ['$label', DayEnum.MONDAY] },
            then: 0
          },
          {
            case: { $eq: ['$label', DayEnum.TUESDAY] },
            then: 1
          },
          {
            case: { $eq: ['$label', DayEnum.WEDNESDAY] },
            then: 2
          },
          {
            case: { $eq: ['$label', DayEnum.THURSDAY] },
            then: 3
          },
          {
            case: { $eq: ['$label', DayEnum.FRIDAY] },
            then: 4
          },
          {
            case: { $eq: ['$label', DayEnum.SATURDAY] },
            then: 5
          }
        ],
        default: 6
      }
    }
  }

  private initDay(id: string) {
    const days = Object.values(DayEnum)
    return days.map((day, index) => ({
      restaurantId: id,
      label: day,
      isClosed: true,
      position: index + 1
    }))
  }

  async getRestaurantHours(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')

    let restaurantDays = await this.restaurantDayService.getAll(
      { restaurantId },
      {
        _id: 0,
        label: 1,
        isClosed: 1,
        score: this.dayScoreQuery()
      }
    )
    if (restaurantDays.length <= 0) {
      const days = this.initDay(restaurantId)
      await this.restaurantDayService.createMany(days)
      restaurantDays = await this.restaurantDayService.getAll(
        { restaurantId },
        {
          _id: 0,
          label: 1,
          isClosed: 1,
          score: this.dayScoreQuery()
        }
      )
    }
    restaurantDays.sort((a, b) => a?.toObject()?.score - b?.toObject()?.score)
    const restaurantPeriods = await this.restaurantPeriodService.getAll(
      {
        restaurantId,
        status: StatusEnum.ACTIVE
      },
      { label: 1, opensAt: 1, closesAt: 1 }
    )
    restaurantPeriods.sort((a, b) => {
      const [hourA, minuteA] = a.opensAt.split(':')
      const [hourB, minuteB] = b.opensAt.split(':')
      const dateA = dayjs().hour(hourA).minute(minuteA).toDate()
      const dateB = dayjs().hour(hourB).minute(minuteB).toDate()
      if (dateA < dateB) return -1
      return 1
    })
    const result = []
    restaurantDays.forEach((day) => {
      const period = restaurantPeriods
        .filter((item) => item.label === day.label)
        .map((transform) => ({
          opensAt: transform.opensAt,
          closesAt: transform.closesAt
        }))
      result.push({
        label: day.label,
        isClosed: day.isClosed,
        period: period
      })
    })
    return { hours: result }
  }

  async updateHours(restaurantId: string, payload: UpdateHoursDto) {
    const logic = async (session: ClientSession) => {
      const restaurant = await this.restaurantService.findOne({
        _id: restaurantId,
        status: StatusEnum.ACTIVE
      })
      if (!restaurant) throw new NotFoundException('Not found restaurant.')
      const dayPromise = []
      const createPeriodPromise = []
      const hours = classToPlain(payload.hours)
      hours.forEach((day) => {
        dayPromise.push(
          this.restaurantDayService.transactionUpdateCondition(
            { restaurantId, label: day.label },
            { isClosed: day.isClosed },
            session
          )
        )
        day.period.forEach((item) => {
          createPeriodPromise.push(
            this.restaurantPeriodService.transactionCreate(
              {
                restaurantId,
                label: day.label,
                opensAt: item.opensAt,
                closesAt: item.closesAt
              },
              session
            )
          )
        })
      })
      await this.restaurantPeriodService.transactionDeleteAll(
        { restaurantId },
        session
      )
      await this.categoryHoursService.transactionDeleteAll(
        { restaurantId },
        session
      )
      await Promise.all(dayPromise)
      await Promise.all(createPeriodPromise)
      return
    }
    await runTransaction(logic)
    return this.getRestaurantHours(restaurantId)
  }
}
