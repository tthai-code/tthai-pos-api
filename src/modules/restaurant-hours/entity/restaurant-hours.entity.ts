import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class PeriodFields {
  @ApiProperty()
  opens_at: string

  @ApiProperty()
  closes_at: string
}

class HoursFields {
  @ApiProperty()
  label: string

  @ApiProperty()
  is_closed: boolean

  @ApiProperty({ type: [PeriodFields] })
  period: Array<PeriodFields>
}

class RestaurantHoursFieldsResponse {
  @ApiProperty({ type: [HoursFields] })
  hours: Array<HoursFields>
}

export class RestaurantHoursResponse extends ResponseDto<RestaurantHoursFieldsResponse> {
  @ApiProperty({
    type: RestaurantHoursFieldsResponse,
    example: {
      hours: [
        {
          label: 'MONDAY',
          is_closed: false,
          period: [
            {
              opens_at: '09:00AM',
              closes_at: '12:00AM'
            },
            {
              opens_at: '03:00PM',
              closes_at: '09:00PM'
            }
          ]
        },
        {
          label: 'TUESDAY',
          is_closed: false,
          period: [
            {
              opens_at: '08:00AM',
              closes_at: '05:00PM'
            },
            {
              opens_at: '11:00PM',
              closes_at: '00:30AM'
            }
          ]
        },
        {
          label: 'WEDNESDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'THURSDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'FRIDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'SATURDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'SUNDAY',
          is_closed: true,
          period: []
        }
      ]
    }
  })
  data: RestaurantHoursFieldsResponse
}
