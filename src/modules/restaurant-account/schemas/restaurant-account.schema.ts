import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type RestaurantAccountDocument = RestaurantAccountFields & Document

@Schema({ timestamps: true, strict: true, collection: 'restaurantAccounts' })
export class RestaurantAccountFields {
  @Prop({ required: true, index: true })
  username: string

  @Prop()
  password: string

  @Prop({ required: true })
  email: string

  @Prop()
  locale: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}

export const RestaurantAccountSchema = SchemaFactory.createForClass(
  RestaurantAccountFields
)
RestaurantAccountSchema.plugin(mongoosePaginate)
RestaurantAccountSchema.plugin(authorStampCreatePlugin)
