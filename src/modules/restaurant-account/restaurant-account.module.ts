import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantAccountController } from './controllers/restaurant-account.controller'
import { RestaurantAccountService } from './services/restaurant-account.service'
import { RestaurantAccountSchema } from './schemas/restaurant-account.schema'
import { AccountPublicController } from './controllers/public-account.controller'
import { AccountLogic } from './logics/restaurant-account.logic'
import { POSAccountLogic } from './logics/pos-restaurant-account.logic'
import { POSAccountController } from './controllers/pos-restaurant-account.controller'

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'restaurantAccount', schema: RestaurantAccountSchema }
    ])
  ],
  providers: [RestaurantAccountService, AccountLogic, POSAccountLogic],
  controllers: [
    RestaurantAccountController,
    AccountPublicController,
    POSAccountController
  ],
  exports: [RestaurantAccountService]
})
export class RestaurantAccountModule {}
