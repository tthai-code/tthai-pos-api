import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'

export class CreateAccountDto {
  @IsString()
  @ApiProperty()
  readonly username: string

  @IsString()
  @ApiProperty()
  public password: string

  @IsString()
  @ApiProperty()
  readonly email: string

  @IsString()
  @ApiProperty()
  readonly locale: string
}
