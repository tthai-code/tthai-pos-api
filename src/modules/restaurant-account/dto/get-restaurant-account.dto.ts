import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class RestaurantAccountPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'createdAt' })
  readonly sortBy: string = 'createdAt'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: StatusEnum.ACTIVE })
  readonly status: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  public buildQuery() {
    const result = {
      $or: [
        {
          username: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          email: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      status: this.status || { $ne: StatusEnum.DELETED }
    }
    return result
  }
}
