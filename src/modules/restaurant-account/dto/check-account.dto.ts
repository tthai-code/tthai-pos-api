import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class CheckAccountDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'test02@gmail.com' })
  readonly username: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<password>' })
  public password: string
}
