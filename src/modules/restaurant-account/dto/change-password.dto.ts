import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class ChangePasswordRestaurantDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'qwerty1243@' })
  public password: string
}
