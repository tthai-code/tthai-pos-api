import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantAccountDocument } from '../schemas/restaurant-account.schema'
import { RestaurantAccountPaginateDto } from '../dto/get-restaurant-account.dto'

@Injectable({ scope: Scope.REQUEST })
export class RestaurantAccountService {
  constructor(
    @InjectModel('restaurantAccount')
    private readonly AccountModel:
      | PaginateModel<RestaurantAccountDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<RestaurantAccountDocument | any> {
    return this.AccountModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.AccountModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<RestaurantAccountDocument> {
    return this.AccountModel
  }

  async create(payload: any): Promise<RestaurantAccountDocument> {
    const document = new this.AccountModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, User: any): Promise<RestaurantAccountDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...User }).save()
  }

  async delete(id: string): Promise<RestaurantAccountDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.AccountModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.AccountModel.findById(id)
  }

  findOne(condition): Promise<RestaurantAccountDocument> {
    return this.AccountModel.findOne(condition, { password: 0 })
  }

  findOneWithPassword(condition): Promise<RestaurantAccountDocument> {
    return this.AccountModel.findOne(condition)
  }

  paginate(
    query: any,
    queryParam: RestaurantAccountPaginateDto
  ): Promise<PaginateResult<RestaurantAccountDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder }
    }

    return this.AccountModel.paginate(query, options)
  }
}
