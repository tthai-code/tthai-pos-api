import {
  Injectable,
  NotFoundException,
  UnauthorizedException
} from '@nestjs/common'
import * as bcrypt from 'bcryptjs'
import { RestaurantAccountService } from '../services/restaurant-account.service'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CheckAccountDto } from '../dto/check-account.dto'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'

@Injectable()
export class POSAccountLogic {
  constructor(
    private readonly accountService: RestaurantAccountService,
    private readonly restaurantService: RestaurantService
  ) {}

  async checkUsernamePassword(restaurantId: string, payload: CheckAccountDto) {
    const accountData = await this.accountService.findOneWithPassword({
      status: StatusEnum.ACTIVE,
      username: payload.username
    })

    if (!accountData) throw new NotFoundException('Not found account.')

    const restaurant = await this.restaurantService.findOne({
      accountId: accountData.id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) {
      throw new NotFoundException('Not found restaurant.')
    }

    if (`${restaurant.id}` !== restaurantId) {
      throw new UnauthorizedException()
    }

    const isCorrect = await bcrypt.compare(
      payload.password,
      accountData.password
    )

    return { isCorrect }
  }
}
