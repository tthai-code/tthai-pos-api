import { BadRequestException, Injectable } from '@nestjs/common'
import * as bcrypt from 'bcryptjs'
import { CreateAccountDto } from '../dto/create-account.dto'
import { RestaurantAccountService } from '../services/restaurant-account.service'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ChangePasswordRestaurantDto } from '../dto/change-password.dto'

@Injectable()
export class AccountLogic {
  constructor(private readonly accountService: RestaurantAccountService) {}

  async createUserLogic(payload: CreateAccountDto) {
    const userData = await this.accountService.findOne({
      status: StatusEnum.ACTIVE,
      username: payload.username
    })

    if (userData) {
      throw new BadRequestException(
        `Username ${payload.username} already used.`
      )
    }

    const salt = bcrypt.genSaltSync(10)
    payload.password = bcrypt.hashSync(payload.password, salt)

    return await this.accountService.create(payload)
  }

  // async updateUserLogic(id: string, payload: UpdateUserDto) {
  //   const userData = await this.accountService.findOne({
  //     status: StatusEnum.ACTIVE,
  //     username: payload.username,
  //     _id: { $ne: id }
  //   })

  //   if (userData) {
  //     throw new BadRequestException(
  //       `Username ${payload.username} already used.`
  //     )
  //   }

  //   return await this.accountService.update(id, payload)
  // }

  async changeUserPasswordLogic(
    id: string,
    payload: ChangePasswordRestaurantDto
  ) {
    const salt = bcrypt.genSaltSync(10)
    payload.password = bcrypt.hashSync(payload.password, salt)
    return await this.accountService.update(id, payload)
  }
}
