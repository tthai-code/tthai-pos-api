import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class CheckAccountObject {
  @ApiProperty()
  is_correct: boolean
}

export class CheckAccountResponse extends ResponseDto<CheckAccountObject> {
  @ApiProperty({
    type: CheckAccountObject,
    example: {
      is_correct: true
    }
  })
  data: CheckAccountObject
}
