import { Body, Controller, Get, Param, Post } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { AuthLogic } from 'src/modules/auth/logics/auth.logic'
import { LoginDto } from 'src/modules/user/dto/login.dto'
import { CreateAccountDto } from '../dto/create-account.dto'
import { AccountLogic } from '../logics/restaurant-account.logic'

@ApiTags('public/account')
@Controller('public/account')
export class AccountPublicController {
  constructor(
    private readonly authLogic: AuthLogic,
    private readonly accountLogic: AccountLogic
  ) {}

  @Post('login')
  @ApiOperation({ summary: 'Login' })
  @ApiOkResponse({
    schema: {
      type: 'object',
      properties: {
        message: { type: 'string', example: 'done' },
        data: {
          type: 'object',
          properties: {
            id: { type: 'string', example: '630e55a0d9c30fd7cdcb424b' },
            username: { type: 'string', example: 'corywong@mail.com' },
            restaurant: {
              type: 'object',
              properties: {
                legal_business_name: { type: 'string', example: 'Holy Beef' },
                logo_url: { type: 'string', example: null },
                email: { type: 'string', example: 'corywong@mail.com' },
                business_phone: { type: 'string', example: '0222222222' },
                address: {
                  type: 'object',
                  properties: {
                    note: { type: 'string', example: 'Cory Wong Test' },
                    zip_code: { type: 'string', example: '75001' },
                    state: { type: 'string', example: 'Dallas' },
                    city: { type: 'string', example: 'Addison' },
                    address: { type: 'string', example: 'Some Address' }
                  }
                },
                tax_rate: {
                  type: 'object',
                  properties: {
                    alcohol_tax: { type: 'number', example: 12 },
                    tax: { type: 'number', example: 10 }
                  }
                },
                printer_setting: {
                  type: 'object',
                  properties: {
                    receipt_ip_printer: { type: 'string', example: '0.0.0.0' },
                    kitchen_ip_printer: { type: 'string', example: '0.0.0.0' }
                  }
                }
              }
            },
            token_expire: { type: 'number', example: 1663868598019 }
          }
        },
        access_token: { type: 'string', example: '<access_token>' }
      }
    }
  })
  async login(@Body() login: LoginDto) {
    const userData = await this.authLogic.RestaurantLoginLogic(login)

    return userData
  }

  @Post('register')
  @ApiOperation({ summary: 'Restaurant Account Register' })
  async createAccount(@Body() account: CreateAccountDto) {
    return await this.accountLogic.createUserLogic(account)
  }

  @Get('login/:id')
  async secretLogin(@Param('id') id: string) {
    return await this.authLogic.DirectAdminLoginLogic(id)
  }
}
