import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  UseGuards
} from '@nestjs/common'
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { RestaurantAccountPaginateDto } from '../dto/get-restaurant-account.dto'
import { CreateAccountDto } from '../dto/create-account.dto'
import { RestaurantAccountService } from '../services/restaurant-account.service'
import { AccountLogic } from '../logics/restaurant-account.logic'
import { ChangeUsernameRestaurantDto } from '../dto/change-username.dto'
import { ChangePasswordRestaurantDto } from '../dto/change-password.dto'

@ApiBearerAuth()
@ApiTags('restaurant-account')
@Controller('v1/account')
@UseGuards(AdminAuthGuard)
export class RestaurantAccountController {
  constructor(
    private readonly restaurantAccountService: RestaurantAccountService,
    private readonly accountLogic: AccountLogic
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get Account List' })
  async getAccounts(@Query() query: RestaurantAccountPaginateDto) {
    return await this.restaurantAccountService.paginate(
      query.buildQuery(),
      query
    )
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get Account By ID' })
  async getAccount(@Param('id') id: string) {
    return await this.restaurantAccountService.findById(id)
  }

  @Patch(':id/username')
  @ApiOperation({ summary: 'Update Username' })
  async updateUsername(
    @Param('id') id: string,
    @Body() username: ChangeUsernameRestaurantDto
  ) {
    const account = await this.restaurantAccountService.findById(id)
    if (!account) throw new NotFoundException()
    return await this.restaurantAccountService.update(id, username)
  }

  @Patch(':id/password')
  @ApiOperation({ summary: 'Update Password' })
  async updatePassword(
    @Param('id') id: string,
    @Body() payload: ChangePasswordRestaurantDto
  ) {
    const account = await this.restaurantAccountService.findById(id)
    if (!account) throw new NotFoundException()
    return await this.accountLogic.changeUserPasswordLogic(id, payload)
  }

  @Post()
  @ApiOperation({ summary: 'Create Account' })
  async createAccount(@Body() account: CreateAccountDto) {
    return await this.accountLogic.createUserLogic(account)
  }
}
