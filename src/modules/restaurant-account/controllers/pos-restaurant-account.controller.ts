import { Body, Controller, Post, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { DeviceAuthGuard } from 'src/modules/auth/guards/device-auth.guard'
import { CheckAccountDto } from '../dto/check-account.dto'
import { CheckAccountResponse } from '../entity/pos-account.entity'
import { POSAccountLogic } from '../logics/pos-restaurant-account.logic'

@ApiBearerAuth()
@ApiTags('pos/auth')
@UseGuards(DeviceAuthGuard)
@Controller('v1/pos/auth')
export class POSAccountController {
  constructor(private readonly posAccountLogic: POSAccountLogic) {}

  @Post('check')
  @ApiOkResponse({ type: () => CheckAccountResponse })
  @ApiOperation({
    summary: 'Check Username and Password',
    description: 'use bearer `device_token`'
  })
  async checkUsernamePassword(@Body() payload: CheckAccountDto) {
    const restaurantId = DeviceAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.posAccountLogic.checkUsernamePassword(
      restaurantId,
      payload
    )
  }
}
