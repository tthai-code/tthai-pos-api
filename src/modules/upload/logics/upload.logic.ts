import { Injectable } from '@nestjs/common'
import { S3 } from 'aws-sdk'
import { Storage } from '@google-cloud/storage'
// import * as path from 'path'
import * as dayjs from 'dayjs'
import * as advancedFormat from 'dayjs/plugin/advancedFormat'
dayjs.extend(advancedFormat)

interface UploadResultInterface {
  publicUrl: string
  prefix: string
  name: string
}
@Injectable()
export class UploadLogic {
  // GCS_PROJECT_ID = process.env.GCS_PROJECT_ID
  // GCS_BUCKET_NAME = process.env.GCS_BUCKET_NAME
  // GCS_SERVICE_ACCOUNT = JSON.parse(process.env.GCS_SERVICE_ACCOUNT)
  private AWS_S3_BUCKET_NAME = process.env.AWS_S3_BUCKET_NAME
  private s3!: any
  private accessKeyId = process.env.AWS_S3_ACCESS_KEY_ID
  private secretAccessKey = process.env.AWS_S3_SECRET_ACCESS_KEY
  private AWS_S3_REGION = process.env.AWS_S3_REGION

  storage!: any

  constructor() {
    // this.storage = new Storage({
    //   projectId: this.GCS_PROJECT_ID,
    //   credentials: {
    //     client_email: this.GCS_SERVICE_ACCOUNT.client_email,
    //     private_key: this.GCS_SERVICE_ACCOUNT.private_key
    //   }
    // })

    this.s3 = new S3({
      accessKeyId: this.accessKeyId,
      secretAccessKey: this.secretAccessKey
    })
  }

  private randomFileName() {
    const randomNumber = Math.floor(Math.random() * 100000) + 1
    return `${dayjs().format('x')}${randomNumber}`
  }

  // uploadLogic(file): Promise<UploadResultInterface> {
  //   return new Promise((resolve, reject) => {
  //     const { originalname, buffer } = file
  //     // eslint-disable-next-line @typescript-eslint/ban-types
  //     const temps: object = buffer
  //     const fileName = originalname.split('.')
  //     const extension = fileName[fileName.length - 1]

  //     const name = `upload/${this.randomFileName()}.${extension}`
  //     const bucket = this.storage.bucket(this.GCS_BUCKET_NAME)
  //     const blob = bucket.file(name)

  //     const blobStream = blob.createWriteStream({
  //       resumable: false
  //     })

  //     blobStream
  //       .on('finish', () => {
  //         const publicUrl = `https://storage.googleapis.com/${this.GCS_BUCKET_NAME}/${blob.name}`
  //         resolve({
  //           publicUrl,
  //           prefix: `https://storage.googleapis.com/${this.GCS_BUCKET_NAME}/`,
  //           name: `${blob.name}`
  //         })
  //       })
  //       .on('error', (err) => {
  //         console.log(err)
  //         reject(`Unable to upload file, something went wrong`)
  //       })
  //       .end(Buffer.from(Object.values(temps)))
  //   })
  // }

  // uploadLogicFromBuffer(
  //   file: Buffer,
  //   extension: string,
  //   folder = 'slip'
  // ): Promise<UploadResultInterface> {
  //   return new Promise((resolve, reject) => {
  //     const name = `${folder}/${this.randomFileName()}.${extension}`
  //     const bucket = this.storage.bucket(this.GCS_BUCKET_NAME)
  //     const blob = bucket.file(name)

  //     const blobStream = blob.createWriteStream({
  //       resumable: false
  //     })

  //     blobStream
  //       .on('finish', () => {
  //         const publicUrl = `https://storage.googleapis.com/${this.GCS_BUCKET_NAME}/${blob.name}`
  //         resolve({
  //           publicUrl,
  //           prefix: `https://storage.googleapis.com/${this.GCS_BUCKET_NAME}/`,
  //           name: `${blob.name}`
  //         })
  //       })
  //       .on('error', (err) => {
  //         console.log(err)
  //         reject(`Unable to upload file, something went wrong`)
  //       })
  //       .end(Buffer.from(file))
  //   })
  // }

  uploadLogicS3(file, path?: string): Promise<UploadResultInterface | string> {
    return new Promise(async (resolve, reject) => {
      const { originalname, buffer } = file
      // eslint-disable-next-line @typescript-eslint/ban-types
      const temps: object = buffer

      const fileName = originalname.split('.')
      const extention = fileName[fileName.length - 1]
      let contentType = 'application/octet-stream'
      if (
        extention === 'png' ||
        extention === 'jpg' ||
        extention === 'jpeg' ||
        extention === 'gif'
      ) {
        contentType = 'image/' + extention
      } else if (extention === 'pdf') {
        contentType = 'application/' + extention
      }

      const name = `${this.randomFileName()}.${extention}`
      const folder =
        process.env.NODE_ENV === 'production' ? 'production' : 'staging'

      const pathFile = path ? `${path.replace(/^\/|\/$/g, '')}/` : ''
      try {
        const uploadedImage = await this.s3
          .upload({
            Bucket: this.AWS_S3_BUCKET_NAME,
            Key: `upload/${folder}/${pathFile}${name}`,
            Body: Buffer.from(Object.values(temps)),
            ACL: 'public-read',
            ContentType: contentType
          })
          .promise()

        resolve({
          publicUrl: `${uploadedImage.Location}`,
          prefix: `https://${this.AWS_S3_BUCKET_NAME}.s3.${
            this.AWS_S3_REGION
          }.amazonaws.com/upload/${folder}/${pathFile.replace(/\/$/, '')}`,
          name
        })
      } catch (err) {
        reject(err)
      }
    })
  }
}
