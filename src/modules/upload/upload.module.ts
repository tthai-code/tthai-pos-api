import { Module } from '@nestjs/common'
import { UploadController } from './controllers/upload.controller'
import { UploadLogic } from './logics/upload.logic'

@Module({
  imports: [],
  controllers: [UploadController],
  providers: [UploadLogic],
  exports: [UploadLogic]
})
export class UploadModule {}
