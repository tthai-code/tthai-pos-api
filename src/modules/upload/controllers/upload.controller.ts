import {
  Controller,
  Post,
  Query,
  UploadedFile,
  UseInterceptors
} from '@nestjs/common'
import { FileInterceptor } from '@nestjs/platform-express'
import { ApiBody, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger'
import { UploadFileDto } from '../dto/upload.dto'
import { UploadLogic } from '../logics/upload.logic'

@ApiTags('uploads')
@Controller('uploads')
export class UploadController {
  constructor(private readonly uploadLogic: UploadLogic) {}

  @Post()
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          format: 'binary'
        }
      }
    }
  })
  @ApiOperation({ summary: 'Upload file' })
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Query() query: UploadFileDto
  ): Promise<any> {
    return await this.uploadLogic.uploadLogicS3(file, query.path)
  }
}
