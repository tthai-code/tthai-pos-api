import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'

export class UploadFileDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'images/jpeg' })
  readonly path: string
}
