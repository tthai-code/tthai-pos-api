import { ApiProperty } from '@nestjs/swagger'
import { IsBoolean, IsNumber } from 'class-validator'

export class UpdateCateringDto {
  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isActive: boolean

  @IsNumber()
  @ApiProperty({ example: 1 })
  readonly deposit: number

  @IsNumber()
  @ApiProperty({ example: 15 })
  readonly minimumDeliveryTime: number
}
