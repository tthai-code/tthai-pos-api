import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CateringDocument } from '../schemas/catering.schema'

@Injectable({ scope: Scope.REQUEST })
export class CateringService {
  constructor(
    @InjectModel('catering')
    private readonly CateringModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<CateringDocument | any> {
    return this.CateringModel.findById({ _id: id })
  }

  async resolveByCondition(condition: any): Promise<CateringDocument | any> {
    return this.CateringModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.CateringModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<CateringDocument> {
    return this.CateringModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.CateringModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.CateringModel.createCollection()

    return this.CateringModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<CateringDocument> {
    const document = new this.CateringModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<CateringDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<CateringDocument> {
    const document = new this.CateringModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<CateringDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<CateringDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<CateringDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.CateringModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.CateringModel.findById(id)
  }

  findOne(condition: any): Promise<CateringDocument> {
    return this.CateringModel.findOne(condition)
  }

  findOneWithSelect(condition: any, options?: any): Promise<CateringDocument> {
    return this.CateringModel.findOne(condition, options)
  }
}
