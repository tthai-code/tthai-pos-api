import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Put,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { UpdateCateringDto } from '../dto/update-catering.dto'
import { GetCateringResponse } from '../enitity/catering.entity'
import { CateringService } from '../services/catering.service'

@ApiBearerAuth()
@ApiTags('catering-setting')
@UseGuards(AdminAuthGuard)
@Controller('v1/catering')
export class CateringController {
  constructor(
    private readonly cateringService: CateringService,
    private readonly restaurantService: RestaurantService
  ) {}

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => GetCateringResponse })
  @ApiOperation({ summary: 'get catering setting by restaurant' })
  async getCatering(@Param('restaurantId') id: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const catering = await this.cateringService.findOne({
      restaurantId: id,
      status: StatusEnum.ACTIVE
    })
    if (!catering) {
      await this.cateringService.create({
        restaurantId: id
      })
      return await this.cateringService.findOne({
        restaurantId: id,
        status: StatusEnum.ACTIVE
      })
    }
    return catering
  }

  @Put(':restaurantId')
  @ApiOkResponse({ type: () => GetCateringResponse })
  @ApiOperation({ summary: 'update catering setting by restaurant' })
  async updateOnlineReceipt(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateCateringDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return await this.cateringService.findOneAndUpdate(
      { restaurantId: id },
      payload
    )
  }
}
