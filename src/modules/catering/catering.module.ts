import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { CateringController } from './controllers/catering.controller'
import { CateringSchema } from './schemas/catering.schema'
import { CateringService } from './services/catering.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([{ name: 'catering', schema: CateringSchema }])
  ],
  providers: [CateringService],
  controllers: [CateringController],
  exports: [CateringService]
})
export class CateringModule {}
