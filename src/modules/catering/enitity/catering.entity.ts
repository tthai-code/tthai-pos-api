import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'

export class CateringResponseField extends TimestampResponseDto {
  @ApiProperty()
  minimum_delivery_time: number

  @ApiProperty()
  deposit: number

  @ApiProperty()
  is_active: boolean

  @ApiProperty()
  restaurant_id: string
}

export class GetCateringResponse extends ResponseDto<CateringResponseField> {
  @ApiProperty({
    type: CateringResponseField,
    example: {
      status: 'active',
      minimum_delivery_time: 15,
      deposit: 1,
      is_active: true,
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-10-10T12:35:20.193Z',
      updated_at: '2022-10-10T12:35:58.048Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '634411882921cfce8d7d54dc'
    }
  })
  data: CateringResponseField
}
