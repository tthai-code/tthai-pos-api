import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, Types, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type MenuDocument = MenuFields & Document

@Schema({ timestamps: true, strict: true, collection: 'menus' })
export class MenuFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  name: string

  @Prop({ default: null })
  nativeName: string

  @Prop({ default: [] })
  category: Array<string>

  @Prop({ default: 999 })
  position: number

  @Prop({ default: null })
  description: string

  @Prop({ required: true })
  price: number

  @Prop({ default: [] })
  imageUrls: Array<string>

  @Prop({ default: null })
  coverImage: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const MenuSchema = SchemaFactory.createForClass(MenuFields)
MenuSchema.plugin(mongoosePaginate)
MenuSchema.plugin(authorStampCreatePlugin)
