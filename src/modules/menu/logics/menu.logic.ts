import { Injectable, NotFoundException } from '@nestjs/common'
import { MenuCategoriesService } from 'src/modules/menu-category/services/menu-category.service'
import { MenuService } from '../services/menu.service'
import { CreateMenuDto } from '../dto/create-menu.dto'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ModifierService } from 'src/modules/modifier/services/modifier.service'
import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service'
import { UpdateMenuDto } from '../dto/update-menu.dto'
import { POSMenuPaginateDto } from '../dto/get-pos-menu.dto'
import { MenuPaginateDto } from '../dto/get-menu.dto'
import { MenuCategoryOnlineLogic } from 'src/modules/menu-category/logics/menu-category-public-online-logic'
import { WebSettingService } from 'src/modules/web-setting/services/web-setting.service'
import { UpdateMenuPositionDto } from '../dto/update-menu-position.dto'

@Injectable()
export class MenuLogic {
  constructor(
    private readonly menuCategoriesService: MenuCategoriesService,
    private readonly menuService: MenuService,
    private readonly modifierService: ModifierService,
    private readonly menuModifierService: MenuModifierService,
    private readonly menuCategoryOnlineLogic: MenuCategoryOnlineLogic,
    private readonly webSettingService: WebSettingService
  ) {}

  async createLogic(payload: CreateMenuDto) {
    const lastPosition = await this.menuService.findOne(
      {
        restaurantId: payload.restaurantId,
        // position: payload.position,
        status: StatusEnum.ACTIVE
      },
      {},
      { sort: { position: -1 } }
    )
    const position = lastPosition?.position | 0
    payload.position = position + 1
    // if (isExist) {
    //   throw new BadRequestException(
    //     `Position ${payload.position} is already used.`
    //   )
    // }

    const menu = await this.menuService.create(payload)
    const modifier = await this.modifierService.getAll(
      {
        _id: { $in: payload.modifiers },
        status: StatusEnum.ACTIVE
      },
      {
        label: 1,
        abbreviation: 1,
        type: 1,
        items: 1,
        maxSelected: 1,
        position: 1
      }
    )
    const menuModifierPayload = modifier.map((item) => {
      const payload = {
        ...item._doc,
        modifierId: item._doc._id,
        menuId: menu.id
      }
      delete payload._id
      return this.menuModifierService.create(payload)
    })
    await Promise.all(menuModifierPayload)

    return menu
  }

  async updateLogic(id: string, payload: UpdateMenuDto) {
    const menuData = await this.menuService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!menuData) {
      throw new NotFoundException(`Menu id ${id} not found.`)
    }
    // const isExist = await this.menuService.findOne({
    //   _id: { $ne: id },
    //   restaurantId: menuData.restaurantId,
    //   position: payload.position,
    //   status: StatusEnum.ACTIVE
    // })
    // if (isExist) {
    //   throw new BadRequestException(
    //     `Position ${payload.position} is already used.`
    //   )
    // }

    return this.menuService.update(id, payload)
  }

  async getMenuById(id: string) {
    const menu = await this.menuService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!menu) throw new NotFoundException('Not found menu.')
    const category = await this.menuCategoriesService.getAll(
      { _id: { $in: menu.category }, isShow: true, status: StatusEnum.ACTIVE },
      { name: 1 }
    )
    return {
      id: menu._id,
      name: menu.name,
      nativeName: menu.nativeName,
      description: menu.description,
      position: menu.position,
      price: menu.price,
      coverImage: menu.coverImage,
      imageUrls: menu.imageUrls,
      category
    }
  }

  async getMenuForPOSById(id: string) {
    const menu = await this.menuService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!menu) throw new NotFoundException('Not found menu.')
    const category = await this.menuCategoriesService.getAll(
      { _id: { $in: menu.category }, isShow: true, status: StatusEnum.ACTIVE },
      { name: 1, isContainAlcohol: 1 }
    )
    const modifiers = await this.menuModifierService.getAll(
      {
        menuId: id,
        status: StatusEnum.ACTIVE
      },
      {
        label: 1,
        abbreviation: 1,
        items: 1,
        type: 1,
        maxSelected: 1
      },
      {
        sort: {
          type: -1,
          position: 1
        }
      }
    )
    return {
      id: menu._id,
      name: menu.name,
      nativeName: menu.nativeName,
      description: menu.description,
      position: menu.position,
      price: menu.price,
      coverImage: menu.coverImage,
      imageUrls: menu.imageUrls,
      category,
      isContainAlcohol:
        category.length > 0
          ? category.some((item) => item.isContainAlcohol)
          : false,
      modifiers
    }
  }

  async getPaginateMenuPOS(restaurantId: string, query: POSMenuPaginateDto) {
    const menuPaginate = await this.menuService.paginate(
      query.buildQuery(restaurantId),
      query
    )
    if (!menuPaginate.docs) return menuPaginate
    const { docs } = menuPaginate
    const results = []
    const category = await this.menuCategoriesService.getAll(
      { restaurantId, isShow: true, status: StatusEnum.ACTIVE },
      { name: 1, isContainAlcohol: 1 }
    )
    const containAlcohol = category.filter((item) => item.isContainAlcohol)
    for (const menu of docs) {
      const modifiers = await this.menuModifierService.getAll(
        {
          menuId: menu.id,
          status: StatusEnum.ACTIVE
        },
        {
          _id: 0,
          id: '$_id',
          label: 1,
          abbreviation: 1,
          items: 1,
          type: 1,
          maxSelected: 1,
          position: 1
        },
        {
          sort: { type: -1, position: 1 }
        }
      )

      const menuCategory = []
      menu.category.forEach((e) => {
        const index = category.findIndex((item) => `${item._id}` === e)
        if (index > -1) {
          menuCategory.push({
            id: e,
            name: category[index].name,
            isContainAlcohol: category[index].isContainAlcohol
          })
        }
      })
      results.push({
        ...menu.toObject(),
        modifiers,
        category: menuCategory,
        isContainAlcohol:
          category.length > 0
            ? containAlcohol.some((item) => menu.category.includes(item.id))
            : false
      })
    }
    return { ...menuPaginate, docs: results }
  }

  async getPaginateMenuWeb(query: MenuPaginateDto) {
    const menuPaginate = await this.menuService.paginate(
      query.buildQuery(query.restaurantId),
      query,
      { status: 0, createdAt: 0, createdBy: 0, updatedBy: 0, updatedAt: 0 }
    )
    if (!menuPaginate.docs) return menuPaginate
    const webSetting = await this.webSettingService.findOne({
      restaurantId: query.restaurantId
    })
    let availableCategory = []
    if (webSetting.isOnlineOrdering) {
      availableCategory =
        await this.menuCategoryOnlineLogic.getAvailableStatusForCategory(
          query.restaurantId
        )
    }
    const { docs } = menuPaginate
    const results = []
    for (const menu of docs) {
      const isAvailable =
        availableCategory.findIndex(
          (item) => menu.category.findIndex((e) => e === item) > -1
        ) > -1
      const modifiers = await this.menuModifierService.getAll(
        {
          menuId: menu.id,
          status: StatusEnum.ACTIVE
        },
        {
          _id: 0,
          id: '$_id',
          label: 1,
          abbreviation: 1,
          items: 1,
          type: 1,
          maxSelected: 1
        },
        {
          sort: {
            type: -1,
            position: 1
          }
        }
      )
      results.push({
        ...menu.toObject(),
        isAvailable,
        modifiers
      })
    }
    return { ...menuPaginate, docs: results }
  }

  async getMenuForWebById(id: string) {
    const menu = await this.menuService.findOne(
      {
        _id: id,
        status: StatusEnum.ACTIVE
      },
      { status: 0, createdAt: 0, createdBy: 0, updatedBy: 0, updatedAt: 0 }
    )
    if (!menu) throw new NotFoundException('Not found menu.')
    const category = await this.menuCategoriesService.getAll(
      { _id: { $in: menu.category }, isShow: true, status: StatusEnum.ACTIVE },
      { name: 1, isContainAlcohol: 1 }
    )
    const webSetting = await this.webSettingService.findOne({
      restaurantId: menu.restaurantId
    })
    let availableCategory = []
    if (webSetting.isOnlineOrdering) {
      availableCategory =
        await this.menuCategoryOnlineLogic.getAvailableStatusForCategory(
          `${menu.restaurantId}`
        )
    }
    const isAvailable =
      availableCategory.findIndex(
        (item) => menu.category.findIndex((e) => e === item) > -1
      ) > -1
    const modifiers = await this.menuModifierService.getAll(
      {
        menuId: id,
        status: StatusEnum.ACTIVE
      },
      {
        label: 1,
        abbreviation: 1,
        items: 1,
        type: 1,
        maxSelected: 1
      },
      {
        sort: {
          type: -1,
          position: 1
        }
      }
    )
    return {
      id: menu._id,
      name: menu.name,
      nativeName: menu.nativeName,
      description: menu.description,
      position: menu.position,
      price: menu.price,
      coverImage: menu.coverImage,
      imageUrls: menu.imageUrls,
      category,
      isContainAlcohol:
        category.length > 0
          ? category.some((item) => item.isContainAlcohol)
          : false,
      modifiers,
      isAvailable
    }
  }

  async updateMenuPosition(payload: UpdateMenuPositionDto) {
    const { items } = payload
    const bulkPayload = items.map((item) => ({
      updateOne: {
        filter: { _id: item.id },
        update: { position: item.position }
      }
    }))
    await this.menuService.bulkWrite(bulkPayload, { ordered: true })
    return { success: true }
  }

  async getAllMenuPOS(restaurantId: string) {
    const menuPaginate = await this.menuService.getAll({
      restaurantId,
      status: StatusEnum.ACTIVE
    })
    const results = []
    const category = await this.menuCategoriesService.getAll(
      { restaurantId, isShow: true, status: StatusEnum.ACTIVE },
      { name: 1, isContainAlcohol: 1 }
    )
    const containAlcohol = category.filter((item) => item.isContainAlcohol)
    for (const menu of menuPaginate) {
      const modifiers = await this.menuModifierService.getAll(
        {
          menuId: menu.id,
          status: StatusEnum.ACTIVE
        },
        {
          _id: 0,
          id: '$_id',
          label: 1,
          abbreviation: 1,
          items: 1,
          type: 1,
          maxSelected: 1,
          position: 1
        },
        {
          sort: { type: -1, position: 1 }
        }
      )

      const menuCategory = []
      menu.category.forEach((e) => {
        const index = category.findIndex((item) => `${item._id}` === e)
        if (index > -1) {
          menuCategory.push({
            id: e,
            name: category[index].name,
            isContainAlcohol: category[index].isContainAlcohol
          })
        }
      })
      results.push({
        ...menu.toObject(),
        modifiers,
        category: menuCategory,
        isContainAlcohol:
          category.length > 0
            ? containAlcohol.some((item) => menu.category.includes(item.id))
            : false
      })
    }
    return results
  }
}
