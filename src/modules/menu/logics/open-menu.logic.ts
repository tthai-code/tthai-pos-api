import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { MenuCategoryOnlineLogic } from 'src/modules/menu-category/logics/menu-category-public-online-logic'
import { MenuCategoriesService } from 'src/modules/menu-category/services/menu-category.service'
import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service'
import { ModifierService } from 'src/modules/modifier/services/modifier.service'
import { OpenMenuPaginateDto } from '../dto/get-open-menu.dto'
import { MenuService } from '../services/menu.service'

@Injectable()
export class OpenMenuLogic {
  constructor(
    private readonly menuCategoriesService: MenuCategoriesService,
    private readonly menuService: MenuService,
    private readonly modifierService: ModifierService,
    private readonly menuModifierService: MenuModifierService,
    private readonly menuCategoryOnlineLogic: MenuCategoryOnlineLogic
  ) {}

  async getPaginateMenuApp(query: OpenMenuPaginateDto) {
    const menuPaginate = await this.menuService.paginate(
      query.buildQuery(query.restaurantId),
      query,
      { status: 0, createdAt: 0, createdBy: 0, updatedBy: 0, updatedAt: 0 }
    )
    if (!menuPaginate.docs) return menuPaginate
    const availableCategory =
      await this.menuCategoryOnlineLogic.getAvailableStatusForCategory(
        query.restaurantId
      )
    const { docs } = menuPaginate
    const results = []
    for (const menu of docs) {
      const isAvailable =
        availableCategory.findIndex(
          (item) => menu.category.findIndex((e) => e === item) > -1
        ) > -1
      const modifiers = await this.menuModifierService.getAll(
        {
          menuId: menu.id,
          status: StatusEnum.ACTIVE
        },
        {
          _id: 0,
          id: '$_id',
          label: 1,
          abbreviation: 1,
          items: 1,
          type: 1,
          maxSelected: 1
        }
      )
      results.push({
        ...menu.toObject(),
        isAvailable,
        modifiers
      })
    }
    return { ...menuPaginate, docs: results }
  }

  async getMenuForAppById(id: string) {
    const menu = await this.menuService.findOne(
      {
        _id: id,
        status: StatusEnum.ACTIVE
      },
      { status: 0, createdAt: 0, createdBy: 0, updatedBy: 0, updatedAt: 0 }
    )
    if (!menu) throw new NotFoundException('Not found menu.')
    const category = await this.menuCategoriesService.getAll(
      { _id: { $in: menu.category }, isShow: true, status: StatusEnum.ACTIVE },
      { name: 1, isContainAlcohol: 1 }
    )
    const availableCategory =
      await this.menuCategoryOnlineLogic.getAvailableStatusForCategory(
        `${menu.restaurantId}`
      )
    const isAvailable =
      availableCategory.findIndex(
        (item) => menu.category.findIndex((e) => e === item) > -1
      ) > -1
    const modifiers = await this.menuModifierService.getAll(
      {
        menuId: id,
        status: StatusEnum.ACTIVE
      },
      {
        label: 1,
        abbreviation: 1,
        items: 1,
        type: 1,
        maxSelected: 1
      }
    )
    return {
      id: menu._id,
      name: menu.name,
      nativeName: menu.nativeName,
      description: menu.description,
      position: menu.position,
      price: menu.price,
      coverImage: menu.coverImage,
      imageUrls: menu.imageUrls,
      category,
      isContainAlcohol:
        category.length > 0
          ? category.some((item) => item.isContainAlcohol)
          : false,
      modifiers,
      isAvailable
    }
  }
}
