import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { MenuCategoriesModule } from '../menu-category/menu-categories.module'
import { MenuModifierModule } from '../menu-modifier/menu-modifier.module'
import { ModifierModule } from '../modifier/modifier.module'
import { POSMenuController } from './controllers/pos-menu.controller'
import { MenuController } from './controllers/menu.controller'
import { MenuOnlineController } from './controllers/menu-public-online.controller'
import { MenuLogic } from './logics/menu.logic'
import { MenuSchema } from './schemas/menu.schema'
import { MenuService } from './services/menu.service'
import { OpenMenuController } from './controllers/open-menu.controller'
import { OpenMenuLogic } from './logics/open-menu.logic'
import { WebSettingModule } from '../web-setting/web-setting.module'

@Module({
  imports: [
    forwardRef(() => MenuCategoriesModule),
    forwardRef(() => ModifierModule),
    forwardRef(() => MenuModifierModule),
    forwardRef(() => WebSettingModule),
    MongooseModule.forFeature([{ name: 'menu', schema: MenuSchema }])
  ],
  providers: [MenuService, MenuLogic, OpenMenuLogic],
  controllers: [
    MenuController,
    POSMenuController,
    MenuOnlineController,
    OpenMenuController
  ],
  exports: [MenuService, MenuLogic]
})
export class MenuModule {}
