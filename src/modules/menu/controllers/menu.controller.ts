import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service'
import { CreateMenuDto } from '../dto/create-menu.dto'
import { MenuPaginateDto } from '../dto/get-menu.dto'
import { UpdateMenuPositionDto } from '../dto/update-menu-position.dto'
import { UpdateMenuDto } from '../dto/update-menu.dto'
import {
  CreatedMenuResponse,
  DeletedMenuResponse,
  GetMenuResponse,
  PaginateMenuResponse
} from '../entity/menu.entity'
import { MenuLogic } from '../logics/menu.logic'
import { MenuService } from '../services/menu.service'

@ApiBearerAuth()
@ApiTags('menu')
@Controller('v1/menu')
@UseGuards(AdminAuthGuard)
export class MenuController {
  constructor(
    private readonly menuService: MenuService,
    private readonly menuLogic: MenuLogic,
    private readonly menuModifierService: MenuModifierService
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get Menu List' })
  @ApiOkResponse({ type: () => PaginateMenuResponse })
  async getMenus(@Query() query: MenuPaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.menuService.paginate(
      query.buildQuery(restaurantId),
      query
    )
  }

  @Get(':id')
  @ApiOkResponse({ type: () => GetMenuResponse })
  @ApiOperation({ summary: 'Get Menu By ID' })
  async getMenu(@Param('id') id: string) {
    return await this.menuLogic.getMenuById(id)
  }

  @Post()
  @ApiCreatedResponse({ type: () => CreatedMenuResponse })
  @ApiOperation({ summary: 'Create Menu' })
  async createMenu(@Body() menu: CreateMenuDto) {
    return this.menuLogic.createLogic(menu)
  }

  @Put(':id')
  @ApiOkResponse({ type: () => CreatedMenuResponse })
  @ApiOperation({ summary: 'Update Menu' })
  async updateMenu(@Body() menu: UpdateMenuDto, @Param('id') id: string) {
    return this.menuLogic.updateLogic(id, menu)
  }

  @Delete(':id')
  @ApiOkResponse({ type: () => DeletedMenuResponse })
  @ApiOperation({ summary: 'Delete Menu' })
  async deleteMenu(@Param('id') id: string) {
    await this.menuModifierService.deleteMany({ menuId: id })
    return this.menuService.update(id, { status: StatusEnum.DELETED })
  }

  @Put('position/all')
  @ApiOperation({ summary: 'Update Position Menu' })
  async updatePosition(@Body() payload: UpdateMenuPositionDto) {
    return await this.menuLogic.updateMenuPosition(payload)
  }
}
