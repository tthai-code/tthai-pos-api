import { Controller, Get, Param, Query } from '@nestjs/common'
import {
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse
} from '@nestjs/swagger'
import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service'
import { errorResponse } from 'src/utilities/errorResponse'
import { MenuPaginateDto } from '../dto/get-menu.dto'
import { OnlineMenuPaginateResponse } from '../entity/menu.entity'
import { OnlineMenuResponse } from '../entity/pos-menu.entity'
import { MenuLogic } from '../logics/menu.logic'
import { MenuService } from '../services/menu.service'

@ApiTags('menu-public-online')
@Controller('v1/public/online/menu')
export class MenuOnlineController {
  constructor(
    private readonly menuService: MenuService,
    private readonly menuLogic: MenuLogic,
    private readonly menuModifierService: MenuModifierService
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get Menu List Online' })
  @ApiOkResponse({ type: () => OnlineMenuPaginateResponse })
  async getMenus(@Query() query: MenuPaginateDto) {
    return await this.menuLogic.getPaginateMenuWeb(query)
  }

  @Get(':id')
  @ApiOkResponse({ type: () => OnlineMenuResponse })
  @ApiUnauthorizedResponse(
    errorResponse(404, 'Not found menu.', '/v1/public/online/menu/:id')
  )
  @ApiOperation({ summary: 'Get Menu By ID for Online' })
  async getMenu(@Param('id') id: string) {
    return await this.menuLogic.getMenuForWebById(id)
  }
}
