import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse
} from '@nestjs/swagger'
import { MenuService } from '../services/menu.service'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { MenuLogic } from '../logics/menu.logic'
import { POSMenuPaginateDto } from '../dto/get-pos-menu.dto'
import {
  PaginatePOSMenuResponse,
  POSAllMenuResponse,
  POSMenuResponse
} from '../entity/pos-menu.entity'
import { errorResponse } from 'src/utilities/errorResponse'

@ApiBearerAuth()
@ApiTags('pos/menu')
@Controller('v1/pos/menu')
@UseGuards(StaffAuthGuard)
export class POSMenuController {
  constructor(
    private readonly menuService: MenuService,
    private readonly menuLogic: MenuLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaginatePOSMenuResponse })
  @ApiUnauthorizedResponse(errorResponse(401, 'Unauthorized', '/v1/pos/menu'))
  @ApiOperation({
    summary: 'Get Paginate Menu List for POS',
    description: 'use *bearer* `staff_token`'
  })
  async getMenus(@Query() query: POSMenuPaginateDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.menuLogic.getPaginateMenuPOS(restaurantId, query)
  }

  @Get('all-menu')
  @ApiOkResponse({ type: () => POSAllMenuResponse })
  @ApiOperation({
    summary: 'Get All Menu for POS',
    description:
      'use *bearer* `staff_token`\n\nuse this route after login with staff.'
  })
  async getAllMenu() {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.menuLogic.getAllMenuPOS(restaurantId)
  }

  @Get(':id')
  @ApiOkResponse({ type: () => POSMenuResponse })
  @ApiUnauthorizedResponse(
    errorResponse(404, 'Not found menu.', '/v1/pos/menu/:id')
  )
  @ApiOperation({
    summary: 'Get Menu By ID for POS',
    description: 'use *bearer* `staff_token`'
  })
  async getMenu(@Param('id') id: string) {
    return await this.menuLogic.getMenuForPOSById(id)
  }
}
