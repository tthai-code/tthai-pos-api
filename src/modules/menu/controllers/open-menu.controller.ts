import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import {
  ApiBasicAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { OpenMenuPaginateDto } from '../dto/get-open-menu.dto'
import {
  GetOpenMenuResponse,
  PaginateOpenMenuResponse
} from '../entity/open-menu.entity'
import { OpenMenuLogic } from '../logics/open-menu.logic'

@ApiBasicAuth()
@ApiTags('open/menu')
@UseGuards(AuthGuard('basic'))
@Controller('v1/open/menu')
export class OpenMenuController {
  constructor(private readonly openMenuLogic: OpenMenuLogic) {}

  @Get()
  @ApiOkResponse({ type: PaginateOpenMenuResponse })
  @ApiOperation({
    summary: 'Get Paginate Menu for app.',
    description: 'use basic auth'
  })
  async getMenuPaginate(@Query() query: OpenMenuPaginateDto) {
    return await this.openMenuLogic.getPaginateMenuApp(query)
  }

  @Get(':menuId')
  @ApiOkResponse({ type: GetOpenMenuResponse })
  @ApiOperation({
    summary: 'Get Menu By ID',
    description: 'use basic auth'
  })
  async getMenuById(@Param('menuId') id: string) {
    return await this.openMenuLogic.getMenuForAppById(id)
  }
}
