import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { MenuDocument } from '../schemas/menu.schema'
import { MenuPaginateDto } from '../dto/get-menu.dto'
import { POSMenuPaginateDto } from '../dto/get-pos-menu.dto'
import { OpenMenuPaginateDto } from '../dto/get-open-menu.dto'

@Injectable({ scope: Scope.REQUEST })
export class MenuService {
  constructor(
    @InjectModel('menu')
    private readonly MenuModel: PaginateModel<MenuDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<MenuDocument | any> {
    return this.MenuModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.MenuModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<MenuDocument> {
    return this.MenuModel
  }

  async create(payload: any): Promise<MenuDocument> {
    const document = new this.MenuModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, menu: any): Promise<MenuDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...menu }).save()
  }

  async delete(id: string): Promise<MenuDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.MenuModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.MenuModel.findById(id)
  }

  findOne(condition, project?: any, options?: any): Promise<MenuDocument> {
    return this.MenuModel.findOne(condition, project, options)
  }

  paginate(
    query: any,
    queryParam: MenuPaginateDto | POSMenuPaginateDto | OpenMenuPaginateDto,
    select?: any
  ): Promise<PaginateResult<MenuDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.MenuModel.paginate(query, options)
  }

  bulkWrite(payload: any[], options?: any) {
    return this.MenuModel.bulkWrite(payload, options)
  }
}
