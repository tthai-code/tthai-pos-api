import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'
import { ModifierTypeEnum } from 'src/modules/modifier/common/modifier-type.enum'

class ModifierItemFields {
  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  price: number
}

class ModifierFields {
  @ApiProperty()
  max_selected: number

  @ApiProperty({ enum: Object.values(ModifierTypeEnum) })
  type: string

  @ApiProperty({ type: [ModifierItemFields] })
  items: Array<ModifierItemFields>

  @ApiProperty()
  abbreviation: string

  @ApiProperty()
  label: string

  @ApiProperty()
  id: string
}

class CategoryFields {
  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty()
  name: string

  @ApiProperty()
  id: string
}

class MenuFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  description: string

  @ApiProperty({ type: [CategoryFields] })
  category: Array<CategoryFields>

  @ApiProperty()
  position: number

  @ApiProperty()
  price: number

  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty()
  cover_image: string

  @ApiProperty()
  image_urls: Array<string>

  @ApiProperty({ type: [ModifierFields] })
  modifiers: Array<ModifierFields>
}

class OnlineMenuField extends MenuFields {
  @ApiProperty()
  is_available: boolean
}

export class MenuFieldsWithTimestamps extends TimestampResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  category: Array<string>

  @ApiProperty()
  position: number

  @ApiProperty()
  price: number

  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty()
  cover_image: string

  @ApiProperty()
  image_urls: Array<string>

  @ApiProperty({ type: [ModifierFields] })
  modifiers: Array<ModifierFields>
}

export class POSMenuResponse extends ResponseDto<MenuFields> {
  @ApiProperty({
    type: MenuFields,
    example: {
      id: '6310685f062dfd651b6e5071',
      name: 'Spicy Wings of Heaven',
      native_name: 'ปีกไก่ทอด',
      description:
        'Chicken wings tossed with house sauce topped with crispy basil.',
      position: 999,
      price: 12,
      cover_image: null,
      image_urls: [
        'https://storage.googleapis.com/tthai-pos-staging/upload/166456399070133572.jpg'
      ],
      category: [
        {
          name: 'Appetizers',
          is_contain_alcohol: false,
          id: '6336bb21c64b8b0998892950'
        }
      ],
      is_contain_alcohol: false,
      modifiers: [
        {
          max_selected: 1,
          type: 'REQUIRED',
          items: [
            {
              name: 'No Spicy',
              price: 0,
              native_name: 'Native Name Test'
            },
            {
              name: 'Mild',
              price: 0,
              native_name: 'Native Name Test'
            },
            {
              name: 'Medium',
              price: 0,
              native_name: 'Native Name Test'
            },
            {
              name: 'Hot',
              price: 0,
              native_name: 'Native Name Test'
            },
            {
              name: 'Thai Hot',
              price: 0,
              native_name: 'Native Name Test'
            }
          ],
          abbreviation: 'P',
          label: 'Spicy Level',
          id: '63525eb44f459775fd5126ce'
        }
      ]
    }
  })
  data: MenuFields
}

export class POSAllMenuResponse extends ResponseDto<MenuFields[]> {
  @ApiProperty({
    type: [MenuFields],
    example: [
      {
        status: 'active',
        cover_image:
          'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341660144217278.jpeg',
        image_urls: [
          'https://tthai-pos-storage.s3.amazonaws.com/upload/staging/16733630954401957.jpeg'
        ],
        price: 10,
        description: 'Test Position',
        position: 4,
        category: [
          {
            id: '63bef3df75f22af7cdd40f8f',
            name: 'Recommended',
            is_contain_alcohol: false
          }
        ],
        native_name: 'Error',
        name: 'Test Error',
        restaurant_id: '63984d6690416c27a7415915',
        created_at: '2022-12-17T04:04:40.657Z',
        updated_at: '2023-02-06T07:13:44.930Z',
        updated_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        created_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        id: '639d3fd8d248e2ea7245147c',
        modifiers: [
          {
            position: 999,
            max_selected: 1,
            type: 'REQUIRED',
            items: [
              {
                name: 'Pork',
                native_name: 'Pork',
                price: 0
              },
              {
                name: 'Chicken',
                native_name: 'Chicken',
                price: 0
              }
            ],
            abbreviation: 'P',
            label: 'Protein Choice',
            id: '63a8035744e33943c3e37de4'
          }
        ],
        is_contain_alcohol: false
      },
      {
        status: 'active',
        cover_image:
          'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341812368128171.png',
        image_urls: [],
        price: 12,
        description: 'Error',
        position: 3,
        category: [
          {
            id: '63bef3df75f22af7cdd40f8f',
            name: 'Recommended',
            is_contain_alcohol: false
          },
          {
            id: '63bff8382402d33518f0d428',
            name: 'Test Cate',
            is_contain_alcohol: false
          }
        ],
        native_name: 'Test 3',
        name: 'TEst 3',
        restaurant_id: '63984d6690416c27a7415915',
        created_at: '2022-12-17T04:06:45.203Z',
        updated_at: '2023-02-06T07:13:44.930Z',
        updated_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        created_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        id: '639d4055f643c9055745b36f',
        modifiers: [
          {
            position: 1,
            max_selected: 3,
            type: 'REQUIRED',
            items: [
              {
                name: 'Test1',
                native_name: 'Test1',
                price: 0
              },
              {
                name: 'Test3',
                native_name: 'Test3',
                price: 0
              },
              {
                name: 'Test2',
                native_name: 'Test2',
                price: 0
              }
            ],
            abbreviation: 'Test',
            label: 'Test Modifiers',
            id: '63cecffb6c3735b0588bbc5c'
          },
          {
            position: 999,
            max_selected: 1,
            type: 'REQUIRED',
            items: [
              {
                name: 'Pork',
                native_name: 'Pork',
                price: 0
              },
              {
                name: 'Chicken',
                native_name: 'Chicken',
                price: 0
              }
            ],
            abbreviation: 'P',
            label: 'Protein Choice',
            id: '63ced04a6c3735b0588bbcb1'
          },
          {
            position: 2,
            max_selected: 1,
            type: 'OPTIONAL',
            items: [
              {
                name: 'Test',
                native_name: 'Position',
                price: 0
              }
            ],
            abbreviation: 'Test',
            label: 'Test Position',
            id: '63cecfdb6c3735b0588bbc45'
          },
          {
            position: 3,
            max_selected: 5,
            type: 'OPTIONAL',
            items: [
              {
                name: 'POS 01',
                native_name: 'POS 01',
                price: 0
              },
              {
                name: 'POS 02',
                native_name: 'POS 02',
                price: 0
              }
            ],
            abbreviation: 'EX',
            label: 'Extra',
            id: '63cecfdb6c3735b0588bbc46'
          },
          {
            position: 998,
            max_selected: 1,
            type: 'OPTIONAL',
            items: [
              {
                name: 'Low',
                native_name: 'Low',
                price: 0
              },
              {
                name: 'Med',
                native_name: 'Med',
                price: 0
              },
              {
                name: 'High',
                native_name: 'High',
                price: 0
              }
            ],
            abbreviation: 'S',
            label: 'Spicy Level',
            id: '63ced0246c3735b0588bbc74'
          }
        ],
        is_contain_alcohol: false
      },
      {
        status: 'active',
        cover_image: null,
        image_urls: [],
        price: 10,
        description: 'TEst',
        position: 2,
        category: [
          {
            id: '63bef3df75f22af7cdd40f8f',
            name: 'Recommended',
            is_contain_alcohol: false
          }
        ],
        native_name: 'Test',
        name: 'Test Position',
        restaurant_id: '63984d6690416c27a7415915',
        created_at: '2022-12-23T10:47:20.716Z',
        updated_at: '2023-02-06T07:13:44.930Z',
        updated_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        created_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        id: '63a587381ad2246f4c2dfc07',
        modifiers: [],
        is_contain_alcohol: false
      },
      {
        status: 'active',
        cover_image:
          'https://tthai-pos-storage.s3.amazonaws.com/upload/staging/167484345986176682.png',
        image_urls: [],
        price: 0.01,
        description: '123213',
        position: 5,
        category: [
          {
            id: '63bef3df75f22af7cdd40f8f',
            name: 'Recommended',
            is_contain_alcohol: false
          }
        ],
        native_name: 'Test',
        name: 'Test New Positon',
        restaurant_id: '63984d6690416c27a7415915',
        created_at: '2023-01-27T18:17:57.150Z',
        updated_at: '2023-02-06T07:13:44.930Z',
        updated_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        created_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        id: '63d415557cd459af1c03af61',
        modifiers: [
          {
            position: 2,
            max_selected: 1,
            type: 'OPTIONAL',
            items: [
              {
                name: 'Test',
                native_name: 'Position',
                price: 0
              }
            ],
            abbreviation: 'Test',
            label: 'Test Position',
            id: '63d415557cd459af1c03af64'
          }
        ],
        is_contain_alcohol: false
      },
      {
        status: 'active',
        cover_image: null,
        image_urls: [],
        price: 0,
        description: null,
        position: 6,
        category: [],
        native_name: 'pos 5',
        name: 'Test pos 5',
        restaurant_id: '63984d6690416c27a7415915',
        created_at: '2023-01-27T18:18:35.848Z',
        updated_at: '2023-02-06T07:13:44.930Z',
        updated_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        created_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        id: '63d4157b72edd9b398138859',
        modifiers: [],
        is_contain_alcohol: false
      },
      {
        status: 'active',
        cover_image:
          'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/production/167566694197439914.avif',
        image_urls: [],
        price: 1,
        description: null,
        position: 1,
        category: [
          {
            id: '63bef3df75f22af7cdd40f8f',
            name: 'Recommended',
            is_contain_alcohol: false
          }
        ],
        native_name: 'เทส avif',
        name: 'avif test 01',
        restaurant_id: '63984d6690416c27a7415915',
        created_at: '2023-02-06T07:02:31.758Z',
        updated_at: '2023-02-06T07:13:44.930Z',
        updated_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        created_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        id: '63e0a6077205023612e25feb',
        modifiers: [
          {
            position: 3,
            max_selected: 5,
            type: 'OPTIONAL',
            items: [
              {
                name: 'POS 01',
                native_name: 'POS 01',
                price: 0
              },
              {
                name: 'POS 02',
                native_name: 'POS 02',
                price: 0
              }
            ],
            abbreviation: 'EX',
            label: 'Extra',
            id: '63e0a6077205023612e25fee'
          }
        ],
        is_contain_alcohol: false
      }
    ]
  })
  data: MenuFields[]
}
export class PaginatePOSMenuFields extends PaginateResponseDto<
  Array<MenuFieldsWithTimestamps>
> {
  @ApiProperty({
    type: [MenuFieldsWithTimestamps],
    example: [
      {
        status: 'active',
        cover_image:
          'https://tthai-pos-storage.s3.amazonaws.com/upload/staging/167484345986176682.png',
        image_urls: [],
        price: 0.01,
        description: '123213',
        position: 4,
        category: [
          {
            id: '63bef3df75f22af7cdd40f8f',
            name: 'Recommended',
            is_contain_alcohol: false
          }
        ],
        native_name: 'Test',
        name: 'Test New Positon',
        restaurant_id: '63984d6690416c27a7415915',
        created_at: '2023-01-27T18:17:57.150Z',
        updated_at: '2023-01-27T18:33:55.223Z',
        updated_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        created_by: {
          username: 'test03@gmail.com',
          id: '6398079c8f56875d327ef2f6'
        },
        id: '63d415557cd459af1c03af61',
        modifiers: [
          {
            position: 2,
            max_selected: 1,
            type: 'OPTIONAL',
            items: [
              {
                name: 'Test',
                native_name: 'Position',
                price: 0
              }
            ],
            abbreviation: 'Test',
            label: 'Test Position',
            id: '63d415557cd459af1c03af64'
          }
        ],
        is_contain_alcohol: false
      }
    ]
  })
  results: Array<MenuFieldsWithTimestamps>
}

export class PaginatePOSMenuResponse extends ResponseDto<PaginatePOSMenuFields> {
  @ApiProperty({
    type: [PaginatePOSMenuFields]
  })
  data: PaginatePOSMenuFields
}

export class OnlineMenuResponse extends ResponseDto<OnlineMenuField> {
  @ApiProperty({
    type: OnlineMenuField,
    example: {
      id: '633744a3c64b8b0998892cf1',
      name: 'Som Tum Thai',
      native_name: 'test nativeName',
      description: 'test desc',
      position: 999,
      price: 13,
      cover_image: null,
      image_urls: [],
      category: [
        {
          name: 'Salads',
          is_contain_alcohol: false,
          id: '63374377c64b8b0998892cb4'
        }
      ],
      is_contain_alcohol: false,
      modifiers: [
        {
          max_selected: 1,
          type: 'REQUIRED',
          items: [
            {
              name: 'Tofu',
              price: 0,
              native_name: 'Native Name Test'
            },
            {
              name: 'Veggie',
              price: 0,
              native_name: 'Native Name Test'
            },
            {
              name: 'Beef',
              price: 3,
              native_name: 'Native Name Test'
            },
            {
              name: 'Shrimp',
              price: 4,
              native_name: 'Native Name Test'
            },
            {
              name: 'Seafood',
              price: 6,
              native_name: 'Native Name Test'
            },
            {
              name: 'Chicken',
              price: 2,
              native_name: 'Native Name Test'
            },
            {
              name: 'Pork',
              price: 2,
              native_name: 'Native Name Test'
            },
            {
              name: 'No Protein',
              price: 0,
              native_name: 'Native Name Test'
            }
          ],
          label: 'Protein Choice',
          abbreviation: 'P',
          id: '633744a3c64b8b0998892cf4'
        },
        {
          max_selected: 1,
          type: 'REQUIRED',
          items: [
            {
              name: 'No Spicy',
              price: 0,
              native_name: 'ไม่เผ็ด'
            },
            {
              name: 'Mild',
              price: 0,
              native_name: 'เผ็ดน้อย'
            },
            {
              name: 'Medium',
              price: 0,
              native_name: 'เผ็ดปานกลาง'
            },
            {
              name: 'Hot',
              price: 0,
              native_name: 'เผ็ด'
            },
            {
              name: 'Thai Hot',
              price: 0,
              native_name: 'เผ็ดมาก'
            }
          ],
          label: 'Spicy Level',
          abbreviation: 'S',
          id: '633744a3c64b8b0998892cf5'
        }
      ],
      is_available: false
    }
  })
  data: OnlineMenuField
}
