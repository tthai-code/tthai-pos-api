import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { ModifierTypeEnum } from 'src/modules/modifier/common/modifier-type.enum'

class ModifierItemObject {
  @ApiProperty()
  name: string
  @ApiProperty()
  price: number
  @ApiProperty()
  native_name: string
}

class ModifierObject {
  @ApiProperty()
  max_selected: number
  @ApiProperty({ enum: Object.values(ModifierTypeEnum) })
  type: string
  @ApiProperty({ type: [ModifierItemObject] })
  items: ModifierItemObject[]
  @ApiProperty()
  label: string
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  id: string
}

class OpenMenuPaginateObject {
  @ApiProperty()
  note: string
  @ApiProperty()
  cover_image: string
  @ApiProperty()
  image_urls: string[]
  @ApiProperty()
  price: number
  @ApiProperty({
    type: 'array',
    items: {
      type: 'string'
    }
  })
  category: string[] | CategoryObject[]
  @ApiProperty()
  name: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  description: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  position: number
  @ApiProperty()
  id: string
  @ApiProperty()
  is_available: boolean
  @ApiProperty()
  modifiers: ModifierObject[]
}

class CategoryObject {
  @ApiProperty()
  name: string
  @ApiProperty()
  is_contain_alcohol: boolean
  @ApiProperty()
  id: string
}

class OpenMenuObject extends OpenMenuPaginateObject {
  @ApiProperty({ type: [CategoryObject] })
  category: CategoryObject[]
}

class OpenMenuPaginate extends PaginateResponseDto<OpenMenuPaginateObject[]> {
  @ApiProperty({
    type: [OpenMenuPaginateObject],
    example: [
      {
        note: '',
        cover_image: null,
        image_urls: [],
        price: 13,
        category: ['63374377c64b8b0998892cb4'],
        name: 'Som Tum Thai',
        restaurant_id: '630eff5751c2eac55f52662c',
        description: 'test desc',
        native_name: 'test nativeName',
        position: 999,
        id: '633744a3c64b8b0998892cf1',
        is_available: false,
        modifiers: [
          {
            max_selected: 1,
            type: 'REQUIRED',
            items: [
              {
                name: 'No Spicy',
                price: 0,
                native_name: 'ไม่เผ็ด'
              },
              {
                name: 'Mild',
                price: 0,
                native_name: 'เผ็ดน้อย'
              },
              {
                name: 'Medium',
                price: 0,
                native_name: 'เผ็ดปานกลาง'
              },
              {
                name: 'Hot',
                price: 0,
                native_name: 'เผ็ด'
              },
              {
                name: 'Thai Hot',
                price: 0,
                native_name: 'เผ็ดมาก'
              }
            ],
            label: 'Spicy Level',
            abbreviation: 'S',
            id: '633744a3c64b8b0998892cf5'
          },
          {
            max_selected: 1,
            type: 'REQUIRED',
            items: [
              {
                name: 'Tofu',
                price: 0,
                native_name: 'Native Name Test'
              },
              {
                name: 'Veggie',
                price: 0,
                native_name: 'Native Name Test'
              },
              {
                name: 'Beef',
                price: 3,
                native_name: 'Native Name Test'
              },
              {
                name: 'Shrimp',
                price: 4,
                native_name: 'Native Name Test'
              },
              {
                name: 'Seafood',
                price: 6,
                native_name: 'Native Name Test'
              },
              {
                name: 'Chicken',
                price: 2,
                native_name: 'Native Name Test'
              },
              {
                name: 'Pork',
                price: 2,
                native_name: 'Native Name Test'
              },
              {
                name: 'No Protein',
                price: 0,
                native_name: 'Native Name Test'
              }
            ],
            label: 'Protein Choice',
            abbreviation: 'P',
            id: '633744a3c64b8b0998892cf4'
          }
        ]
      }
    ]
  })
  results: OpenMenuPaginateObject[]
}

export class PaginateOpenMenuResponse extends ResponseDto<OpenMenuPaginate> {
  @ApiProperty({ type: OpenMenuPaginate })
  data: OpenMenuPaginate
}

export class GetOpenMenuResponse extends ResponseDto<OpenMenuObject> {
  @ApiProperty({
    type: OpenMenuObject,
    example: {
      id: '633742e5c64b8b0998892c83',
      name: 'Coconut Lemongrass Soup',
      native_name: 'test nativeName',
      description: 'test desc',
      position: 999,
      price: 15,
      cover_image: null,
      image_urls: [],
      category: [
        {
          name: 'Soup',
          is_contain_alcohol: false,
          id: '6337412cc64b8b0998892c62'
        }
      ],
      is_contain_alcohol: false,
      modifiers: [],
      is_available: true
    }
  })
  data: OpenMenuObject
}
