import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'
import { ModifierTypeEnum } from 'src/modules/modifier/common/modifier-type.enum'

class MenuFields extends TimestampResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  restaurant_id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  description: string

  @ApiProperty()
  category: Array<string>

  @ApiProperty()
  position: number

  @ApiProperty()
  price: number

  @ApiProperty()
  cover_image: string

  @ApiProperty()
  image_urls: Array<string>
}

class ModifierItemObject {
  @ApiProperty()
  name: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  price: number
}
class ModifierObject {
  @ApiProperty()
  max_selected: number
  @ApiProperty({ enum: Object.values(ModifierTypeEnum) })
  type: string
  @ApiProperty({ type: [ModifierItemObject] })
  items: ModifierItemObject[]
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  label: string
  @ApiProperty()
  id: string
}
class MenuFieldsWithModifier extends MenuFields {
  @ApiProperty({ type: [ModifierObject] })
  modifiers: ModifierObject[]
}

class OnlineMenuFieldsWithModifier extends MenuFieldsWithModifier {
  @ApiProperty()
  is_available: boolean
}

class CategoryFields {
  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty()
  name: string

  @ApiProperty()
  id: string
}
class GetMenuFields extends TimestampResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  description: string

  @ApiProperty({ type: [CategoryFields] })
  category: Array<CategoryFields>

  @ApiProperty()
  position: number

  @ApiProperty()
  price: number

  @ApiProperty()
  cover_image: string

  @ApiProperty()
  image_urls: Array<string>
}

export class PaginateMenuResponse extends PaginateResponseDto<
  Array<MenuFieldsWithModifier>
> {
  @ApiProperty({
    type: [MenuFieldsWithModifier],
    example: [
      {
        status: 'active',
        cover_image: '',
        image_urls: [],
        modifiers: [
          {
            max_selected: 1,
            type: 'REQUIRED',
            items: [
              {
                name: 'Shrimp',
                native_name: 'Native Name Test',
                price: 2
              },
              {
                name: 'No Protein',
                native_name: 'Native Name Test',
                price: 0
              }
            ],
            abbreviation: 'P',
            label: 'Choice of Protein',
            id: '634d2a6c57a023457c8e4990'
          }
        ],
        price: 40,
        description: 'Test Desc',
        position: 1,
        category: ['634d067bcc64f608779162f5'],
        native_name: 'เบียร์ช้าง',
        name: 'Chang Beer',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        created_at: '2022-10-17T10:11:56.416Z',
        updated_at: '2022-10-17T10:47:34.775Z',
        updated_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        created_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        id: '634d2a6c57a023457c8e498d'
      }
    ]
  })
  results: Array<MenuFieldsWithModifier>
}

export class CreatedMenuResponse extends ResponseDto<MenuFields> {
  @ApiProperty({
    type: MenuFields,
    example: {
      status: 'active',
      cover_image: '',
      image_urls: [],
      price: 40,
      description: 'Test Desc',
      position: 1,
      category: ['634d067bcc64f608779162f5'],
      native_name: 'เบียร์ช้าง',
      name: 'Chang Beer',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-10-17T10:11:56.416Z',
      updated_at: '2022-10-17T10:11:56.416Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '634d2a6c57a023457c8e498d'
    }
  })
  data: MenuFields
}

export class DeletedMenuResponse extends ResponseDto<MenuFields> {
  @ApiProperty({
    type: MenuFields,
    example: {
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      status: 'deleted',
      cover_image: null,
      image_urls: [],
      price: 15,
      description: null,
      position: null,
      category: [
        {
          name: 'A La Carte',
          id: '630f2b664c861c248e7758f6'
        }
      ],
      native_name: null,
      name: 'Pad Krapow',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      updated_at: '2022-10-17T11:07:49.684Z',
      created_at: '2022-09-21T12:34:25.125Z',
      id: '632b04d1322209df557131ad'
    }
  })
  data: MenuFields
}

export class GetMenuResponse extends ResponseDto<GetMenuFields> {
  @ApiProperty({
    type: GetMenuFields,
    example: {
      id: '634d2a6c57a023457c8e498d',
      name: 'Chang Beer',
      native_name: 'เบียร์ช้าง',
      description: 'Test Desc',
      position: 1,
      price: 40,
      cover_image: '',
      image_urls: [],
      category: [
        {
          name: 'Best Seller',
          id: '634d067bcc64f608779162f5'
        }
      ]
    }
  })
  data: GetMenuFields
}

class OnlineMenuPaginateField extends PaginateResponseDto<
  OnlineMenuFieldsWithModifier[]
> {
  @ApiProperty({
    type: [OnlineMenuFieldsWithModifier],
    example: [
      {
        note: '',
        cover_image: null,
        image_urls: [],
        price: 15,
        category: ['6337412cc64b8b0998892c62'],
        name: 'Coconut Lemongrass Soup',
        restaurant_id: '630eff5751c2eac55f52662c',
        description: 'test desc',
        native_name: 'test nativeName',
        position: 999,
        id: '633742e5c64b8b0998892c83',
        is_available: true,
        modifiers: []
      }
    ]
  })
  results: OnlineMenuFieldsWithModifier[]
}

export class OnlineMenuPaginateResponse extends ResponseDto<OnlineMenuPaginateField> {
  @ApiProperty({ type: OnlineMenuPaginateField })
  data: OnlineMenuPaginateField
}
