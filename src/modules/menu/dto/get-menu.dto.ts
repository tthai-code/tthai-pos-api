import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class MenuPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'position' })
  readonly sortBy: string = 'position'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'asc' })
  readonly sortOrder: string = 'asc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<category-id>' })
  readonly category: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<restaurant-id>' })
  readonly restaurantId: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          name: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          nativeName: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      category: this.category,
      restaurantId: !id ? this.restaurantId : id,
      status: this.status || { $ne: StatusEnum.DELETED }
    }
    if (!id && !this.restaurantId) delete result.restaurantId
    if (!this.category) delete result.category
    return result
  }
}
