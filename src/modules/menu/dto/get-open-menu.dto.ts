import { IsString, IsOptional } from 'class-validator'
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class OpenMenuPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'position' })
  readonly sortBy: string = 'position'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'asc' })
  readonly sortOrder: string = 'asc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<category-id>' })
  readonly categoryId: string

  @IsString()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          name: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          nativeName: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      category: this.categoryId,
      restaurantId: !id ? this.restaurantId : id,
      status: StatusEnum.ACTIVE
    }
    if (!id && !this.restaurantId) delete result.restaurantId
    if (!this.categoryId) delete result.category
    return result
  }
}
