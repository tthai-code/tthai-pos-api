import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  ArrayNotEmpty,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested
} from 'class-validator'

export class PositionObject {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<menu-id>' })
  id: string

  @IsNumber()
  @ApiProperty({ example: 1 })
  position: number
}

export class UpdateMenuPositionDto {
  @Type(() => PositionObject)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: [PositionObject] })
  items: PositionObject[]
}
