import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class POSMenuPaginateDto extends PaginateDto {
  @IsString()
  readonly sortBy: string = 'position'

  @IsString()
  readonly sortOrder: string = 'asc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: 'กะเพรา',
    description: 'Search by name or native name of menu'
  })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<category-id>' })
  readonly category: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          name: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          nativeName: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      category: this.category,
      restaurantId: id,
      status: StatusEnum.ACTIVE
    }
    if (!this.category) delete result.category
    return result
  }
}
