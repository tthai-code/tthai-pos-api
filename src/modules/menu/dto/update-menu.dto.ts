import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString
} from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'
export class UpdateMenuDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Ka Prao' })
  readonly name: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'กะเพรา' })
  readonly nativeName: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  readonly description: string

  @IsNumber()
  @ApiProperty()
  public position: number

  @IsNumber()
  @ApiProperty()
  public price: number

  @IsOptional()
  @IsString({ each: true })
  @ApiPropertyOptional({
    example: ['6308974b19416019c26ae15d', '6308974b19416019c26ae151']
  })
  readonly category: Array<string>

  @IsOptional()
  @IsString({ each: true })
  @ApiPropertyOptional({ example: ['<image-urls>'] })
  readonly imageUrls: Array<string>

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<coverImage-url>' })
  readonly coverImage: string

  @IsString()
  @IsOptional()
  @IsEnum(StatusEnum)
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string
}
