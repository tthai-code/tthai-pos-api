import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { CreateDiscountDto } from '../dto/create-discount.dto'
import { POSPaginateDiscountDto } from '../dto/pos-get-discount.dto'
import {
  CreateUpdateDiscountResponse,
  DiscountResponse,
  PaginateDiscountResponse
} from '../entity/pos-discount.entity'
import { POSDiscountLogic } from '../logic/pos-discount.logic'

@ApiBearerAuth()
@ApiTags('pos/discount')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/discount')
export class POSDiscountController {
  constructor(private readonly posDiscountLogic: POSDiscountLogic) {}

  @Get()
  @ApiOkResponse({ type: () => PaginateDiscountResponse })
  @ApiOperation({
    summary: 'Get Discount List',
    description: 'use bearer `staff_token`'
  })
  async getDiscountList(@Query() query: POSPaginateDiscountDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.posDiscountLogic.getDiscountList(restaurantId, query)
  }

  @Get(':id')
  @ApiOkResponse({ type: () => DiscountResponse })
  @ApiOperation({
    summary: 'Get Discount by ID',
    description: 'use bearer `staff_token`'
  })
  async getDiscount(@Param('id') id: string) {
    return await this.posDiscountLogic.getDiscount(id)
  }

  @Post()
  @ApiCreatedResponse({ type: () => CreateUpdateDiscountResponse })
  @ApiOperation({
    summary: 'Create a Discount',
    description: 'use bearer `staff_token`'
  })
  async createDiscount(@Body() payload: CreateDiscountDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.posDiscountLogic.createDiscount(restaurantId, payload)
  }

  @Put(':id')
  @ApiOkResponse({ type: () => CreateUpdateDiscountResponse })
  @ApiOperation({
    summary: 'Update Discount by ID',
    description: 'use bearer `staff_token`'
  })
  async updateDiscount(
    @Param('id') id: string,
    @Body() payload: CreateDiscountDto
  ) {
    return await this.posDiscountLogic.updateDiscount(id, payload)
  }

  @Delete(':id')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Delete Discount by ID',
    description: 'use bearer `staff_token`'
  })
  async deleteDiscount(@Param('id') id: string) {
    return await this.posDiscountLogic.deleteDiscount(id)
  }
}
