import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class CreateDiscountDto {
  public restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Military' })
  readonly name: string

  @IsNumber()
  @ApiProperty({ example: 1 })
  readonly discount: number
}
