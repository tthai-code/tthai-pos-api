import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'

export class POSPaginateDiscountDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    description: 'can sort by `createdAt`, `name`, `discount`'
  })
  readonly sortBy: string = 'createdAt'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ enum: ['desc', 'asc'] })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  public buildQuery(restaurantId: string) {
    const result = {
      $or: [
        {
          name: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      restaurantId,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
