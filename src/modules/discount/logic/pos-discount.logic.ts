import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CreateDiscountDto } from '../dto/create-discount.dto'
import { POSPaginateDiscountDto } from '../dto/pos-get-discount.dto'
import { DiscountService } from '../services/discount.service'

@Injectable()
export class POSDiscountLogic {
  constructor(
    private readonly discountService: DiscountService,
    private readonly restaurantService: RestaurantService
  ) {}

  async getDiscount(id: string) {
    const discount = await this.discountService.findOne(
      {
        _id: id,
        status: StatusEnum.ACTIVE
      },
      {
        status: 0,
        createdAt: 0,
        updatedAt: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    if (!discount) throw new NotFoundException('Not found discount.')
    return discount
  }

  async getDiscountList(restaurantId: string, query: POSPaginateDiscountDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return await this.discountService.paginate(
      query.buildQuery(restaurantId),
      query,
      {
        status: 0,
        createdAt: 0,
        updatedAt: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
  }

  async createDiscount(restaurantId: string, payload: CreateDiscountDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    payload.restaurantId = restaurantId
    const created = await this.discountService.create(payload)
    return {
      success: true,
      id: created.id
    }
  }

  async updateDiscount(id: string, payload: CreateDiscountDto) {
    const discount = await this.discountService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!discount) throw new NotFoundException('Not found discount.')
    payload.restaurantId = `${discount.restaurantId}`
    const updated = await this.discountService.update(id, payload)
    return {
      success: true,
      id: updated.id
    }
  }

  async deleteDiscount(id: string) {
    const discount = await this.discountService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!discount) throw new NotFoundException('Not found discount.')
    await this.discountService.delete(id)
    return { success: true }
  }
}
