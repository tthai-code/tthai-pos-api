import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { POSPaginateDiscountDto } from '../dto/pos-get-discount.dto'
import { DiscountDocument } from '../schemas/discount.schema'

@Injectable({ scope: Scope.REQUEST })
export class DiscountService {
  constructor(
    @InjectModel('discount')
    private readonly DiscountModel: PaginateModel<DiscountDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<DiscountDocument | any> {
    return this.DiscountModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.DiscountModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<DiscountDocument> {
    return this.DiscountModel
  }

  async create(payload: any): Promise<DiscountDocument> {
    const document = new this.DiscountModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<DiscountDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<DiscountDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async delete(id: string): Promise<DiscountDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.DiscountModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.DiscountModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<DiscountDocument> {
    return this.DiscountModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: POSPaginateDiscountDto,
    select?: any
  ): Promise<PaginateResult<DiscountDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }
    return this.DiscountModel.paginate(query, options)
  }
}
