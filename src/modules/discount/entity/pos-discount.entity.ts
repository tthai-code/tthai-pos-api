import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { SuccessField } from 'src/common/entity/success.entity'

class CreateUpdateDiscountObject extends SuccessField {
  @ApiProperty()
  id: string
}

export class CreateUpdateDiscountResponse extends ResponseDto<CreateUpdateDiscountObject> {
  @ApiProperty({
    type: CreateUpdateDiscountObject,
    example: {
      success: true,
      id: '63b69a7dc2f8dde0dffce9b9'
    }
  })
  data: CreateUpdateDiscountObject
}

class DiscountObject {
  @ApiProperty()
  discount: number
  @ApiProperty()
  name: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
}

class PaginateDiscountObject extends PaginateResponseDto<DiscountObject[]> {
  @ApiProperty({
    type: [DiscountObject],
    example: [
      {
        discount: 4,
        name: 'Special',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        id: '63b69a7dc2f8dde0dffce9b9'
      }
    ]
  })
  results: DiscountObject[]
}

export class PaginateDiscountResponse extends ResponseDto<PaginateDiscountObject> {
  @ApiProperty({ type: PaginateDiscountObject })
  data: PaginateDiscountObject
}

export class DiscountResponse extends ResponseDto<DiscountObject> {
  @ApiProperty({
    type: DiscountObject,
    example: {
      discount: 4,
      name: 'Special',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      id: '63b69a7dc2f8dde0dffce9b9'
    }
  })
  data: DiscountObject
}
