import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type DiscountDocument = DiscountFields & Document

@Schema({ timestamps: true, strict: true, collection: 'discounts' })
export class DiscountFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  name: string

  @Prop({ required: true })
  discount: number

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const DiscountSchema = SchemaFactory.createForClass(DiscountFields)
DiscountSchema.plugin(authorStampCreatePlugin)
DiscountSchema.plugin(mongoosePaginate)
