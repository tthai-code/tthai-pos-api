import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { POSDiscountController } from './controllers/pos-discount.controller'
import { POSDiscountLogic } from './logic/pos-discount.logic'
import { DiscountSchema } from './schemas/discount.schema'
import { DiscountService } from './services/discount.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([{ name: 'discount', schema: DiscountSchema }])
  ],
  providers: [DiscountService, POSDiscountLogic],
  controllers: [POSDiscountController],
  exports: []
})
export class DiscountModule {}
