export enum BillStatusEnum {
  PAID = 'PAID',
  UNPAID = 'UNPAID'
}
