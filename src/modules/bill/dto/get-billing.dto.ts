import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsNotEmpty, IsOptional, IsString } from 'class-validator'
import dayjs from 'src/plugins/dayjs'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'

export class AllBillingPaginateDto extends PaginateDto {
  @IsString()
  readonly sortBy: string = 'createdAt'

  @IsString()
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'iPad' })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: new Date().toISOString() })
  readonly startDate: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: new Date().toISOString() })
  readonly endDate: string

  public buildQuery(restaurantIds: string[]) {
    const result = {
      // $or: [
      //   {
      //     // _id: {
      //     //   $regex: this.search,
      //     //   $options: 'i'
      //     // }
      //   }
      // ],
      invoiceDate: {
        $gte: dayjs(this.startDate).tz('Etc/GMT+5').startOf('day').toDate(),
        $lte: dayjs(this.endDate)
          .add(1, 'day')
          .tz('Etc/GMT+5')
          .startOf('day')
          .toDate()
      },
      restaurantId: { $in: restaurantIds },
      status: StatusEnum.ACTIVE
    }
    console.log(result)
    if (!this.startDate || !this.endDate) delete result.invoiceDate
    if (restaurantIds.length <= 0) delete result.restaurantId
    return result
  }
}

export class BillingPaginateDto extends PaginateDto {
  @IsString()
  readonly sortBy: string = 'invoiceDate'

  @IsString()
  readonly sortOrder: string = 'desc'

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  public buildQuery() {
    const result = {
      restaurantId: this.restaurantId,
      status: StatusEnum.ACTIVE
    }

    return result
  }
}
