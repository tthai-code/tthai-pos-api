import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class PayMonthlyBillDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly invoiceDate: string
}
