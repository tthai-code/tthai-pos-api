import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class CreateBillDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: 'string', example: new Date().toISOString() })
  readonly invoiceDate: string | Date

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: 'string', example: new Date().toISOString() })
  readonly startedPeriod: string | Date

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: 'string', example: new Date().toISOString() })
  readonly endedPeriod: string | Date
}
