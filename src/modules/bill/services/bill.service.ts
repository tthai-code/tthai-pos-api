import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession, Model } from 'mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import {
  AllBillingPaginateDto,
  BillingPaginateDto
} from '../dto/get-billing.dto'
import { BillDocument } from '../schemas/bill.schema'

@Injectable({ scope: Scope.REQUEST })
export class BillService {
  constructor(
    @InjectModel('bill')
    private readonly BillModel: PaginateModel<BillDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<BillDocument | any> {
    return this.BillModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.BillModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.BillModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<BillDocument> {
    return this.BillModel
  }

  async getSession(): Promise<ClientSession> {
    await this.BillModel.createCollection()

    return await this.BillModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<BillDocument> {
    const document = new this.BillModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async createMany(payload: any): Promise<any> {
    return this.BillModel.insertMany(payload, { ordered: true })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.BillModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<BillDocument> {
    const document = new this.BillModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<BillDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<BillDocument> {
    return this.BillModel.updateOne(condition, { ...payload })
  }

  async updateMany(condition: any, payload: any): Promise<Array<any>> {
    return this.BillModel.updateMany(condition, { $set: payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<BillDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.BillModel.updateMany(condition, { $set: payload }, { session })
  }

  async delete(id: string): Promise<BillDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.BillModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.BillModel.findById(id)
  }

  findOne(condition, project?: any): Promise<BillDocument> {
    return this.BillModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: AllBillingPaginateDto | BillingPaginateDto,
    select?: any
  ): PaginateResult<BillDocument> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.BillModel.paginate(query, options)
  }
}
