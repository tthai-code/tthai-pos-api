import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession, Model } from 'mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { BillTransactionDocument } from '../schemas/bill-transaction.schem'

@Injectable({ scope: Scope.REQUEST })
export class BillTransactionService {
  constructor(
    @InjectModel('billTransactions')
    private readonly BillTransactionModel:
      | PaginateModel<BillTransactionDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<BillTransactionDocument | any> {
    return this.BillTransactionModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.BillTransactionModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.BillTransactionModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<BillTransactionDocument> {
    return this.BillTransactionModel
  }

  async getSession(): Promise<ClientSession> {
    await this.BillTransactionModel.createCollection()

    return await this.BillTransactionModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<BillTransactionDocument> {
    const document = new this.BillTransactionModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionCreateMany(
    payload: any,
    session: ClientSession
  ): Promise<BillTransactionDocument> {
    return this.BillTransactionModel.insertMany(payload, { session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.BillTransactionModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<BillTransactionDocument> {
    const document = new this.BillTransactionModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<BillTransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(
    condition: any,
    payload: any
  ): Promise<BillTransactionDocument> {
    return this.BillTransactionModel.updateOne(condition, { ...payload })
  }

  async updateMany(condition: any, payload: any): Promise<Array<any>> {
    return this.BillTransactionModel.updateMany(condition, { $set: payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<BillTransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.BillTransactionModel.updateMany(
      condition,
      { $set: payload },
      { session }
    )
  }

  async delete(id: string): Promise<BillTransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.BillTransactionModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.BillTransactionModel.findById(id)
  }

  findOne(condition, project?: any): Promise<BillTransactionDocument> {
    return this.BillTransactionModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: any,
    select?: any
  ): Promise<PaginateResult<BillTransactionDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.BillTransactionModel.paginate(query, options)
  }
}
