import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession, Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { BillItemsDocument } from '../schemas/bill-item.schema'

@Injectable({ scope: Scope.REQUEST })
export class BillItemsService {
  constructor(
    @InjectModel('billItems')
    private readonly BillItemsModel: Model<BillItemsDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<BillItemsDocument | any> {
    return this.BillItemsModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.BillItemsModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.BillItemsModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<BillItemsDocument> {
    return this.BillItemsModel
  }

  async getSession(): Promise<ClientSession> {
    await this.BillItemsModel.createCollection()

    return await this.BillItemsModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<BillItemsDocument> {
    const document = new this.BillItemsModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.BillItemsModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<BillItemsDocument> {
    const document = new this.BillItemsModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<BillItemsDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<BillItemsDocument> {
    return this.BillItemsModel.updateOne(condition, { ...payload })
  }

  async updateMany(condition: any, payload: any): Promise<Array<any>> {
    return this.BillItemsModel.updateMany(condition, { $set: payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<BillItemsDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.BillItemsModel.updateMany(
      condition,
      { $set: payload },
      { session }
    )
  }

  async delete(id: string): Promise<BillItemsDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.BillItemsModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.BillItemsModel.findById(id)
  }

  findOne(condition, project?: any): Promise<BillItemsDocument> {
    return this.BillItemsModel.findOne(condition, project)
  }
}
