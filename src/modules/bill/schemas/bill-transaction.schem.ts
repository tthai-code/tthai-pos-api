import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Schema as MongooseSchema, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { SubscriptionPaymentStatus } from 'src/modules/subscription/common/subscription-payment-status.enum'

export type BillTransactionDocument = BillTransactionField & Document

@Schema({ timestamps: true, strict: true, collection: 'billTransactions' })
export class BillTransactionField {
  @Prop({ required: true })
  restaurantId: string

  @Prop({ required: true })
  billId: string

  @Prop({ default: null })
  response: MongooseSchema.Types.Mixed

  @Prop({
    enum: Object.values(SubscriptionPaymentStatus),
    default: SubscriptionPaymentStatus.PENDING
  })
  paymentStatus: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const BillTransactionsSchema =
  SchemaFactory.createForClass(BillTransactionField)
BillTransactionsSchema.plugin(authorStampCreatePlugin)
BillTransactionsSchema.plugin(mongoosePaginate)
