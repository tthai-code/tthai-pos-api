import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { BillStatusEnum } from '../common/bill-status.enum'

export type BillDocument = BillFields & Document

@Schema({ _id: false, timestamps: false, strict: true })
export class BillItemsObjectField {
  @Prop({ required: true })
  description: string

  @Prop({ default: 1 })
  quantity: number

  @Prop({ default: 0 })
  amount: number
}

@Schema({ timestamps: true, strict: true, collection: 'bills' })
export class BillFields {
  @Prop({ required: true })
  restaurantId: string

  @Prop({ required: true })
  startedPeriod: Date

  @Prop({ required: true })
  endedPeriod: Date

  @Prop({ required: true })
  invoiceDate: Date

  @Prop({ default: null })
  dueDate: Date

  @Prop({ default: 0 })
  amountDue: number

  @Prop({ default: [] })
  items: BillItemsObjectField[]

  @Prop({ default: BillStatusEnum.UNPAID, enum: Object.values(BillStatusEnum) })
  billStatus: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const BillSchema = SchemaFactory.createForClass(BillFields)
BillSchema.plugin(authorStampCreatePlugin)
BillSchema.plugin(mongoosePaginate)
