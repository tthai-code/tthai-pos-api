import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'

export type BillItemsDocument = BillItemsFields & Document

@Schema({ timestamps: false, strict: true, collection: 'billItems' })
export class BillItemsFields {
  @Prop({ required: true })
  restaurantId: string

  @Prop({ required: true })
  billId: string

  @Prop({ required: true })
  description: string

  @Prop({ default: 1 })
  quantity: number

  @Prop({ default: 0 })
  amount: number
}
export const BillItemsSchema = SchemaFactory.createForClass(BillItemsFields)
