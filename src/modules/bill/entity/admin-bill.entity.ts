import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { BillStatusEnum } from '../common/bill-status.enum'

class BillItemsObject {
  @ApiProperty()
  description: string
  @ApiProperty()
  quantity: number
  @ApiProperty()
  amount: number
}

class BillingObject {
  @ApiProperty({ enum: Object.values(BillStatusEnum) })
  bill_status: string
  @ApiProperty({ type: [BillItemsObject] })
  items: BillItemsObject[]
  @ApiProperty()
  amount_due: number
  @ApiProperty({ description: 'Same as Transferred Date' })
  due_date: string
  @ApiProperty()
  invoice_date: string
  @ApiProperty()
  ended_period: string
  @ApiProperty()
  started_period: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
}

class AdminBillingObject extends BillingObject {
  @ApiProperty()
  restaurant_name: string
}

class PaginateBillObject extends PaginateResponseDto<BillingObject[]> {
  @ApiProperty({
    type: [BillingObject],
    example: [
      {
        bill_status: 'UNPAID',
        items: [
          {
            description: 'Credit Card Sales',
            quantity: 0,
            amount: 0
          },
          {
            description: 'Debit Card Sales',
            quantity: 0,
            amount: 0
          },
          {
            description: 'More iPad',
            quantity: 2,
            amount: 40
          },
          {
            description: 'test package 2.1',
            quantity: 1,
            amount: 0
          },
          {
            description: 'iPad',
            quantity: 1,
            amount: 0
          }
        ],
        amount_due: 40,
        due_date: null,
        invoice_date: '2022-11-30T17:00:00.000Z',
        ended_period: '2022-11-30T16:59:59.999Z',
        started_period: '2022-10-31T17:00:00.000Z',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        id: '6392f2cf9aef71b576bcb5bb'
      }
    ]
  })
  results: BillingObject[]
}

class PaginateAdminBillObject extends PaginateResponseDto<
  AdminBillingObject[]
> {
  @ApiProperty({
    type: [BillingObject],
    example: [
      {
        bill_status: 'UNPAID',
        items: [
          {
            description: 'Credit Card Sales',
            quantity: 0,
            amount: 0
          },
          {
            description: 'Debit Card Sales',
            quantity: 0,
            amount: 0
          },
          {
            description: 'More iPad',
            quantity: 2,
            amount: 40
          },
          {
            description: 'test package 2.1',
            quantity: 1,
            amount: 0
          },
          {
            description: 'iPad',
            quantity: 1,
            amount: 0
          }
        ],
        amount_due: 40,
        due_date: null,
        invoice_date: '2022-11-30T17:00:00.000Z',
        ended_period: '2022-11-30T16:59:59.999Z',
        started_period: '2022-10-31T17:00:00.000Z',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        restaurant_name: 'Holy Beef',
        id: '6392f2cf9aef71b576bcb5bb'
      }
    ]
  })
  results: AdminBillingObject[]
}

export class PaginateBillResponse extends ResponseDto<PaginateBillObject> {
  @ApiProperty({ type: PaginateBillObject })
  data: PaginateBillObject
}

export class PaginateAdminBillResponse extends ResponseDto<PaginateAdminBillObject> {
  @ApiProperty({ type: PaginateAdminBillObject })
  data: PaginateAdminBillObject
}

export class BillResponse extends ResponseDto<BillingObject> {
  @ApiProperty({
    type: BillingObject,
    example: {
      bill_status: 'UNPAID',
      items: [
        {
          description: 'Credit Card Sales',
          quantity: 0,
          amount: 0
        },
        {
          description: 'Debit Card Sales',
          quantity: 0,
          amount: 0
        },
        {
          description: 'More iPad',
          quantity: 2,
          amount: 40
        },
        {
          description: 'test package 2.1',
          quantity: 1,
          amount: 0
        },
        {
          description: 'iPad',
          quantity: 1,
          amount: 0
        }
      ],
      amount_due: 40,
      due_date: null,
      invoice_date: '2022-11-30T17:00:00.000Z',
      ended_period: '2022-11-30T16:59:59.999Z',
      started_period: '2022-10-31T17:00:00.000Z',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      id: '6392f2cf9aef71b576bcb5bb'
    }
  })
  data: BillingObject
}
