import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { OrderModule } from '../order/order.module'
import { PaymentGatewayModule } from '../payment-gateway/payment-gateway.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { SubscriptionModule } from '../subscription/subscription.module'
import { TransactionModule } from '../transaction/transaction.module'
import { AdminBillController } from './controllers/admin-bill.controller'
import { MonthlyBillController } from './controllers/monthly-bill.controller'
import { AdminBillLogic } from './logic/admin-bill.logic'
import { BillLogic } from './logic/bill.logic'
import { BillItemsSchema } from './schemas/bill-item.schema'
import { BillTransactionsSchema } from './schemas/bill-transaction.schem'
import { BillSchema } from './schemas/bill.schema'
import { BillItemsService } from './services/bill-item.service'
import { BillTransactionService } from './services/bill-transaction.service'
import { BillService } from './services/bill.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    forwardRef(() => PaymentGatewayModule),
    forwardRef(() => TransactionModule),
    forwardRef(() => SubscriptionModule),
    forwardRef(() => OrderModule),
    MongooseModule.forFeature([
      { name: 'bill', schema: BillSchema },
      { name: 'billItems', schema: BillItemsSchema },
      { name: 'billTransactions', schema: BillTransactionsSchema }
    ])
  ],
  providers: [
    BillService,
    BillItemsService,
    BillTransactionService,
    BillLogic,
    AdminBillLogic
  ],
  controllers: [MonthlyBillController, AdminBillController],
  exports: []
})
export class BillModule {}
