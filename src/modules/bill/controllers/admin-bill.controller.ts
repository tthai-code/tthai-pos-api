import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuperAdminAuthGuard } from 'src/modules/auth/guards/super-admin-auth.guard'
import {
  AllBillingPaginateDto,
  BillingPaginateDto
} from '../dto/get-billing.dto'
import {
  BillResponse,
  PaginateAdminBillResponse,
  PaginateBillResponse
} from '../entity/admin-bill.entity'
import { AdminBillLogic } from '../logic/admin-bill.logic'
import { BillService } from '../services/bill.service'

@Controller('v1/admin/bill')
export class AdminBillController {
  constructor(
    private readonly billService: BillService,
    private readonly adminBillLogic: AdminBillLogic
  ) {}

  @ApiBearerAuth()
  @UseGuards(SuperAdminAuthGuard)
  @ApiTags('admin/billing')
  @Get()
  @ApiOkResponse({ type: () => PaginateAdminBillResponse })
  @ApiOperation({
    summary: 'Get All Billing for Super Admin',
    description: 'use bearer `admin_token`'
  })
  async getAllBilling(@Query() query: AllBillingPaginateDto) {
    return await this.adminBillLogic.getAllBilling(query)
  }

  @ApiBearerAuth()
  @UseGuards(SuperAdminAuthGuard)
  @ApiTags('admin/restaurant')
  @Get('restaurant')
  @ApiOkResponse({ type: () => PaginateBillResponse })
  @ApiOperation({
    summary: 'Get Billing for Admin by Restaurant',
    description: 'use bearer `admin_token`'
  })
  async getBillRestaurant(@Query() query: BillingPaginateDto) {
    return await this.billService.paginate(query.buildQuery(), query, {
      createdAt: 0,
      updatedAt: 0,
      createdBy: 0,
      updatedBy: 0,
      status: 0
    })
  }

  @ApiTags('admin/billing')
  @Get('/:billId/billing')
  @ApiOkResponse({ type: () => BillResponse })
  @ApiOperation({
    summary: 'Get Billing By ID'
  })
  async getBill(@Param('billId') billId: string) {
    return await this.adminBillLogic.getBillingById(billId)
  }
}
