import { Body, Controller, Post, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { ApiBasicAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { CreateBillDto } from '../dto/create-bill.dto'
import { PayMonthlyBillDto } from '../dto/pay-bill.dto'
import { BillLogic } from '../logic/bill.logic'

@ApiBasicAuth()
@ApiTags('cron/bill')
@UseGuards(AuthGuard('basic'))
@Controller('v1/cron/bill')
export class MonthlyBillController {
  constructor(private readonly billLogic: BillLogic) {}

  @Post()
  @ApiOperation({
    summary: 'Create Monthly Bill',
    description: 'use basic auth'
  })
  async createMonthlyBill(@Body() payload: CreateBillDto) {
    return await this.billLogic.createMonthlyBill(payload)
  }

  @Post('/pay')
  @ApiOperation({ summary: 'Pay Monthly Bill', description: 'use basic auth' })
  async payMonthlyBill(@Body() payload: PayMonthlyBillDto) {
    return await this.billLogic.payMonthlyBill(payload.invoiceDate)
  }
}
