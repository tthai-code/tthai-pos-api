import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { SubscriptionPaymentStatus } from 'src/modules/subscription/common/subscription-payment-status.enum'
import { AllBillingPaginateDto } from '../dto/get-billing.dto'
import { BillTransactionService } from '../services/bill-transaction.service'
import { BillService } from '../services/bill.service'

@Injectable()
export class AdminBillLogic {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly billService: BillService,
    private readonly billTransactionService: BillTransactionService
  ) {}

  async getAllBilling(query: AllBillingPaginateDto) {
    const restaurant = await this.restaurantService.getAll({
      $or: [
        {
          legalBusinessName: { $regex: query.search, $options: 'i' }
        },
        {
          dba: { $regex: query.search, $options: 'i' }
        }
      ],
      status: StatusEnum.ACTIVE
    })
    const restaurantIds = restaurant.map((item) => item.id)
    const billing = await this.billService.paginate(
      query.buildQuery(restaurantIds),
      query,
      { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 }
    )
    const { docs } = billing
    const results = []
    for (const bill of docs) {
      const index = restaurant.findIndex(
        (item) => `${item.id}` === `${bill.restaurantId}`
      )
      const restaurantName = restaurant[index].legalBusinessName
      results.push({
        ...bill.toObject(),
        restaurantName
      })
    }
    return {
      ...billing,
      docs: results
    }
  }

  async getBillingById(id: string) {
    const billing = await this.billService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!billing) {
      throw new NotFoundException('Not found billing from id.')
    }
    const restaurant = await this.restaurantService.findOne({
      _id: billing.restaurantId
    })
    const transaction = await this.billTransactionService.findOne({
      billId: id,
      paymentStatus: { $ne: SubscriptionPaymentStatus.FAILURE }
    })
    const result = {
      ...billing.toObject(),
      restaurant: restaurant?.toObject() || null,
      transaction: transaction?.toObject() || null
    }
    return result
  }
}
