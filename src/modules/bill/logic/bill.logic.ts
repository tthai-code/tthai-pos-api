import { Injectable } from '@nestjs/common'
import * as dayjs from 'dayjs'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { OrderService } from 'src/modules/order/services/order.service'
import {
  ACHBankAccountTypeEnum,
  BankAccountStatusEnum
} from 'src/modules/payment-gateway/common/ach-bank.type.enum'
import { IRecurring } from 'src/modules/payment-gateway/interfaces/card-pointe.interface'
import { BankAccountService } from 'src/modules/payment-gateway/services/bank-account.service'
import { PaymentGatewayHttpService } from 'src/modules/payment-gateway/services/payment-gateway-http.service'
import { PaymentGatewayService } from 'src/modules/payment-gateway/services/payment-gateway.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { SubscriptionPaymentStatus } from 'src/modules/subscription/common/subscription-payment-status.enum'
import {
  SubscriptionPlanEnum,
  SubscriptionStatusEnum
} from 'src/modules/subscription/common/subscription-plan.enum'
import { SubscriptionService } from 'src/modules/subscription/services/subscription.service'
import { PaymentMethodEnum } from 'src/modules/transaction/common/payment-method.enum'
import { PaymentStatusEnum } from 'src/modules/transaction/common/payment-status.enum'
// import { TransactionStatusEnum } from 'src/modules/transaction/common/transaction-status.enum'
import { TransactionService } from 'src/modules/transaction/services/transaction.service'
import { BillStatusEnum } from '../common/bill-status.enum'
import { CreateBillDto } from '../dto/create-bill.dto'
import { BillItemsService } from '../services/bill-item.service'
import { BillTransactionService } from '../services/bill-transaction.service'
import { BillService } from '../services/bill.service'

@Injectable()
export class BillLogic {
  constructor(
    private readonly billService: BillService,
    private readonly billItemsService: BillItemsService,
    private readonly billTransactionService: BillTransactionService,
    private readonly restaurantService: RestaurantService,
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly bankAccountService: BankAccountService,
    private readonly transactionService: TransactionService,
    private readonly subscriptionService: SubscriptionService,
    private readonly orderService: OrderService,
    private readonly paymentGatewayHttpService: PaymentGatewayHttpService
  ) {}

  private numberToFixed(number: number, digit: number): number {
    return Number(number.toFixed(digit))
  }

  private summaryTransactionItem(items: any[], key: string): number {
    return (
      items
        .map((item) => item[key])
        .reduce((sum, current) => sum + current, 0) || 0
    )
  }

  private groupBy(array: any[], key: string): any[] {
    const groupByObject = array.reduce(
      (r, v, i, a, k = v[key]) => ((r[k] || (r[k] = [])).push(v), r),
      {}
    )
    const transform = Object.entries(groupByObject).map((e) => ({
      restaurantId: e[0],
      data: e[1]
    }))
    return transform
  }

  private async getAllRestaurantHavingBankAccount(): Promise<any[]> {
    const restaurants = await this.restaurantService.getAll({
      status: StatusEnum.ACTIVE
    })
    const activeRestaurantId = restaurants.map((item) => item.id)
    const bankAccounts = await this.bankAccountService.getAll({
      restaurantId: { $in: activeRestaurantId },
      status: StatusEnum.ACTIVE,
      bankAccountStatus: BankAccountStatusEnum.ACTIVE
    })
    if (bankAccounts.length <= 0) return []
    const restaurantIds = bankAccounts.map((item) => item.restaurantId)
    return restaurantIds
  }

  async createMonthlyBill(createBill: CreateBillDto) {
    const restaurantIds = await this.getAllRestaurantHavingBankAccount()
    const subscription = await this.subscriptionService.getAll({
      restaurantId: { $in: restaurantIds },
      autoRenewAt: dayjs(createBill.invoiceDate).toDate(),
      status: StatusEnum.ACTIVE
    })
    const transaction = await this.transactionService.getAll({
      restaurantId: { $in: restaurantIds },
      paymentMethod: {
        $in: [PaymentMethodEnum.CREDIT, PaymentMethodEnum.DEBIT]
      },
      openDate: {
        $gte: dayjs(createBill.startedPeriod).toDate(),
        $lte: dayjs(createBill.endedPeriod).toDate()
      },
      // transactionStatus: TransactionStatusEnum.CLOSED,
      paymentStatus: PaymentStatusEnum.PAID,
      status: StatusEnum.ACTIVE
    })
    const posProcessingFee = await this.restaurantService.getAll(
      {
        _id: { $in: restaurantIds },
        status: StatusEnum.ACTIVE
      },
      { posProcessingFee: 1 }
    )
    const restaurantBillPayload = []
    for (const restaurantId of restaurantIds) {
      const restaurantSubscription = subscription.filter(
        (item) => `${item.restaurantId}` === `${restaurantId}`
      )
      const restaurantTransaction = transaction.filter(
        (item) => `${item.restaurantId}` === `${restaurantId}`
      )
      const posProcessingFeeIndex = posProcessingFee.findIndex(
        (item) => `${item._id}` === `${restaurantId}`
      )
      const { debitCard, creditCard } =
        posProcessingFee[posProcessingFeeIndex].posProcessingFee
      const debitTransaction = restaurantTransaction.filter(
        (transaction) => transaction.paymentMethod === PaymentMethodEnum.DEBIT
      )
      const creditTransaction = restaurantTransaction.filter(
        (transaction) => transaction.paymentMethod === PaymentMethodEnum.CREDIT
      )

      const debitSales = this.summaryTransactionItem(
        debitTransaction,
        'totalPaid'
      )
      const creditSales = this.summaryTransactionItem(
        creditTransaction,
        'totalPaid'
      )
      const calculatedDebitSales = this.numberToFixed(
        (debitSales * debitCard) / 100,
        2
      )

      const calculatedCreditSales = this.numberToFixed(
        (creditSales * creditCard) / 100,
        2
      )
      const calculatedSubscription = this.summaryTransactionItem(
        restaurantSubscription,
        'amount'
      )
      const amountDue =
        calculatedCreditSales + calculatedDebitSales + calculatedSubscription
      const subscriptionItems = restaurantSubscription.map((item) => ({
        description: item.name,
        quantity: item.quantity,
        amount: item.amount
      }))
      const items = [
        {
          description: 'Credit Card Sales',
          quantity: creditSales,
          amount: calculatedCreditSales
        },
        {
          description: 'Debit Card Sales',
          quantity: debitSales,
          amount: calculatedDebitSales
        },
        ...subscriptionItems
      ]

      restaurantBillPayload.push({
        restaurantId,
        startedPeriod: dayjs(createBill.startedPeriod).toDate(),
        endedPeriod: dayjs(createBill.endedPeriod).toDate(),
        invoiceDate: dayjs(createBill.invoiceDate).toDate(),
        dueDate: dayjs(createBill.invoiceDate).add(2, 'day').toDate(),
        amountDue,
        items
      })
    }
    return await this.billService.createMany(restaurantBillPayload)
  }

  async payMonthlyBill(invoiceDate: string) {
    const bills = await this.billService.getAll({
      invoiceDate: dayjs(invoiceDate).toDate(),
      billStatus: BillStatusEnum.UNPAID,
      status: StatusEnum.ACTIVE
    })
    const restaurantIds = bills.map((item) => item.restaurantId)
    const bankAccounts = await this.bankAccountService.getAll({
      restaurantId: { $in: restaurantIds },
      bankAccountStatus: BankAccountStatusEnum.ACTIVE,
      status: StatusEnum.ACTIVE
    })
    const recurringBills = []
    for (const bill of bills) {
      const index = bankAccounts.findIndex(
        (item) => `${item.restaurantId}` === bill.restaurantId
      )
      if (index !== -1) {
        const bankAccount = bankAccounts[index]
        const recurringObject: IRecurring = {
          account: bankAccount.token,
          accttype:
            bankAccount.accountType === ACHBankAccountTypeEnum.SAVING
              ? 'ESAV'
              : 'ECHK',
          amount: bill.amountDue.toFixed(2),
          name: bankAccount.name
        }
        recurringBills.push(
          this.paymentGatewayHttpService.recurring(recurringObject)
        )
      }
    }
    const paidBills = await Promise.all(recurringBills)
    const resultBillTransactions = []
    for (let i = 0; i < paidBills.length; i++) {
      const { data } = paidBills[i]
      const bill = bills[i]
      const transactionObject = {
        restaurantId: bill.restaurantId,
        billId: bill.id,
        response: data,
        paymentStatus: SubscriptionPaymentStatus.PENDING
      }
      if (!['00', '000'].includes(data.respcode)) {
        transactionObject.paymentStatus = SubscriptionPaymentStatus.FAILURE
      }
      resultBillTransactions.push(transactionObject)
    }

    const successBillIds = resultBillTransactions
      .filter(
        (transaction) =>
          transaction.paymentStatus === SubscriptionPaymentStatus.PENDING
      )
      .map((item) => item.billId)
    const successRestaurantIds = resultBillTransactions
      .filter(
        (transaction) =>
          transaction.paymentStatus === SubscriptionPaymentStatus.PENDING
      )
      .map((item) => item.restaurantId)

    const logic = async (session: ClientSession) => {
      const billTransactions =
        await this.billTransactionService.transactionCreateMany(
          resultBillTransactions,
          session
        )
      await this.billService.transactionUpdateMany(
        { _id: { $in: successBillIds } },
        { billStatus: BillStatusEnum.PAID },
        session
      )
      await this.subscriptionService.transactionUpdateMany(
        {
          restaurantId: { $in: successRestaurantIds },
          paymentPlan: { $ne: SubscriptionPlanEnum.YEARLY },
          subscriptionStatus: SubscriptionStatusEnum.ACTIVE
        },
        {
          lastRenewAt: dayjs().toDate(),
          autoRenewAt: dayjs(invoiceDate).add(1, 'month').toDate()
        },
        session
      )
      await this.subscriptionService.transactionUpdateMany(
        {
          restaurantId: { $in: successRestaurantIds },
          paymentPlan: SubscriptionPlanEnum.YEARLY,
          subscriptionStatus: SubscriptionStatusEnum.ACTIVE
        },
        {
          lastRenewAt: dayjs().toDate(),
          autoRenewAt: dayjs(invoiceDate).add(1, 'year').toDate()
        },
        session
      )
      return billTransactions
    }
    return runTransaction(logic)
  }
}
