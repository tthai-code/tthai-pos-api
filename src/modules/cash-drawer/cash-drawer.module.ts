import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { CashDrawerController } from './controllers/cash-drawer.controller'
import { POSCashDrawerController } from './controllers/pos-cash-drawer.controller'
import { CashDrawerLogic } from './logic/cash-drawer.logic'
import { POSCashDrawerLogic } from './logic/pos-cash-drawer.logic'
import { CashActivitySchema } from './schemas/cash-activity.schema'
import { CashDrawerSchema } from './schemas/cash-drawer.schema'
import { CashActivityService } from './services/cash-activity.service'
import { CashDrawerService } from './services/cash-drawer.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([
      { name: 'cashDrawers', schema: CashDrawerSchema },
      { name: 'cashActivity', schema: CashActivitySchema }
    ])
  ],
  providers: [
    CashDrawerService,
    CashActivityService,
    POSCashDrawerLogic,
    CashDrawerLogic
  ],
  controllers: [CashDrawerController, POSCashDrawerController],
  exports: [POSCashDrawerLogic]
})
export class CashDrawerModule {}
