import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'
import * as dayjs from 'dayjs'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'

export class PaginateCashDrawerDto extends PaginateDto {
  @IsString()
  readonly sortBy: string = 'createdAt'

  @IsString()
  readonly sortOrder: string = 'desc'

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly startDate: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly endDate: string

  public buildQuery(restaurantId: string) {
    const result = {
      startTime: {
        $gte: dayjs(this.startDate).toDate(),
        $lte: dayjs(this.endDate).toDate()
      },
      restaurantId,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
