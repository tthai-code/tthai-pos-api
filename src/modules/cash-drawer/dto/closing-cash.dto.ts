import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class ClosingCashDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: 'string', example: new Date().toISOString() })
  public date: string | Date

  @IsNumber()
  @ApiProperty()
  readonly actualAmount: number
}
