import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import {
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString
} from 'class-validator'
import { CashDrawerActionEnum } from '../common/cash-drawer.enum'

export class CashActivityDto {
  public restaurantId: string
  public cashDrawerId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: 'string', example: new Date().toISOString() })
  public date: string | Date

  @IsNumber()
  @ApiProperty({ example: 10.2 })
  readonly amount: number

  @IsString()
  @IsIn(Object.values(CashDrawerActionEnum))
  @ApiProperty({
    enum: Object.values(CashDrawerActionEnum).filter(
      (item) => item !== CashDrawerActionEnum.CASH_SALES
    ),
    example: CashDrawerActionEnum.STARTING_CASH
  })
  readonly action: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Kevin Start' })
  readonly user: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Starting evening shift' })
  readonly comment: string
}
