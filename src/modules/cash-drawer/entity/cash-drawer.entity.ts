import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import {
  CashDrawerActionEnum,
  CashDrawerStatusEnum
} from '../common/cash-drawer.enum'

class CashDrawerObject {
  @ApiProperty()
  id: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  start_time: string
  @ApiProperty()
  end_time: string
  @ApiProperty({ enum: Object.values(CashDrawerStatusEnum) })
  cash_drawer_status: string
  @ApiProperty()
  total_cash_sales: number
  @ApiProperty()
  cash_in: number
  @ApiProperty()
  expected_amount: number
  @ApiProperty()
  actual_amount: number
  @ApiProperty()
  difference: number
}

class PaginateCashDrawerObject extends PaginateResponseDto<CashDrawerObject[]> {
  @ApiProperty({
    type: [CashDrawerObject],
    example: {
      id: '639b30a9a595361d1b13b926',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      start_time: '2022-12-15T14:35:03.772Z',
      end_time: '2022-12-15T15:02:05.148Z',
      cash_drawer_status: 'CLOSED',
      total_cash_sales: 0,
      cash_in: 110.4,
      expected_amount: 110.4,
      actual_amount: 120.55,
      difference: 10.15
    }
  })
  results: CashDrawerObject[]
}

export class PaginateCashDrawerResponse extends ResponseDto<PaginateCashDrawerObject> {
  @ApiProperty({ type: PaginateCashDrawerObject })
  data: PaginateCashDrawerObject
}

class CashActivityObject {
  @ApiProperty({
    enum: Object.values(CashDrawerActionEnum).filter(
      (item) => item !== CashDrawerActionEnum.CASH_SALES
    )
  })
  action: string
  @ApiProperty()
  comment: string
  @ApiProperty()
  user: string
  @ApiProperty()
  amount: number
  @ApiProperty()
  date: string
  @ApiProperty()
  id: string
}

class CashDrawerWithActivityObject {
  @ApiProperty()
  id: string
  @ApiProperty()
  start_time: string
  @ApiProperty()
  end_time: string
  @ApiProperty({ type: [CashActivityObject] })
  activity: CashActivityObject[]
  @ApiProperty()
  expected_amount: number
  @ApiProperty()
  actual_amount: number
  @ApiProperty()
  difference: number
  @ApiProperty()
  cash_drawer_status: string
}

export class CashDrawerWithActivityResponse extends ResponseDto<CashDrawerWithActivityObject> {
  @ApiProperty({
    type: CashDrawerWithActivityObject,
    example: {
      id: '639b30a9a595361d1b13b926',
      start_time: '2022-12-15T14:35:03.772Z',
      end_time: '2022-12-15T15:02:05.148Z',
      activity: [
        {
          action: 'STARTING_CASH',
          comment: 'Starting night shift',
          user: 'Kevin Start',
          amount: 10.2,
          date: '2022-12-15T08:35:03.772Z',
          id: '639b30a9a595361d1b13b928'
        },
        {
          action: 'CASH_IN',
          comment: 'Starting night shift',
          user: 'Kevin Start',
          amount: 10.2,
          date: '2022-12-15T14:35:03.772Z',
          id: '639b323ed414156964aecf08'
        },
        {
          action: 'CASH_OUT',
          comment: 'Starting night shift',
          user: 'Kevin Start',
          amount: 10.2,
          date: '2022-12-15T14:35:03.772Z',
          id: '639b3242d414156964aecf0d'
        },
        {
          action: 'CASH_IN',
          comment: 'Starting night shift',
          user: 'Kevin Start',
          amount: 100.2,
          date: '2022-12-15T14:35:03.772Z',
          id: '639b3256d414156964aecf13'
        },
        {
          action: 'CASH_COUNT',
          comment: 'Starting night shift',
          user: 'Kevin Start',
          amount: 0,
          date: '2022-12-15T14:35:03.772Z',
          id: '639b32d29eca4b55f4b55756'
        }
      ],
      expected_amount: 110.4,
      actual_amount: 120.55,
      difference: 10.15,
      cash_drawer_status: 'CLOSED'
    }
  })
  data: CashDrawerWithActivityObject
}
