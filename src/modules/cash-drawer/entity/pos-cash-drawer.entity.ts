import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class CashDrawerStatusObject {
  @ApiProperty()
  is_open_cash_drawer_status: boolean
}

class ExpectedCashObject {
  @ApiProperty({ example: 128.5 })
  expected_amount: number
}

export class CashDrawerStatusResponse extends ResponseDto<CashDrawerStatusObject> {
  @ApiProperty({
    type: CashDrawerStatusObject,
    example: {
      is_open_cash_drawer_status: false
    }
  })
  data: CashDrawerStatusObject
}

export class SummaryClosingCashResponse extends ResponseDto<ExpectedCashObject> {
  @ApiProperty({
    type: ExpectedCashObject
  })
  data: ExpectedCashObject
}
