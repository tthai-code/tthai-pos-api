import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession, Model } from 'mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CashActivityDocument } from '../schemas/cash-activity.schema'

@Injectable({ scope: Scope.REQUEST })
export class CashActivityService {
  constructor(
    @InjectModel('cashActivity')
    private readonly CashActivityModel:
      | PaginateModel<CashActivityDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<CashActivityDocument | any> {
    return this.CashActivityModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.CashActivityModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.CashActivityModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<CashActivityDocument> {
    return this.CashActivityModel
  }

  async getSession(): Promise<ClientSession> {
    await this.CashActivityModel.createCollection()

    return await this.CashActivityModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<CashActivityDocument> {
    const document = new this.CashActivityModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async createMany(payload: any): Promise<any> {
    return this.CashActivityModel.insertMany(payload, { ordered: true })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.CashActivityModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<CashActivityDocument> {
    const document = new this.CashActivityModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<CashActivityDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<CashActivityDocument> {
    return this.CashActivityModel.updateOne(condition, { ...payload })
  }

  async updateMany(condition: any, payload: any): Promise<Array<any>> {
    return this.CashActivityModel.updateMany(condition, { $set: payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<CashActivityDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.CashActivityModel.updateMany(
      condition,
      { $set: payload },
      { session }
    )
  }

  async delete(id: string): Promise<CashActivityDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any, options?: any): Promise<any[]> {
    return this.CashActivityModel.find(condition, project, options)
  }

  findById(id): Promise<any> {
    return this.CashActivityModel.findById(id)
  }

  findOne(condition, project?: any): Promise<CashActivityDocument> {
    return this.CashActivityModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: any,
    select?: any
  ): PaginateResult<CashActivityDocument> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.CashActivityModel.paginate(query, options)
  }
}
