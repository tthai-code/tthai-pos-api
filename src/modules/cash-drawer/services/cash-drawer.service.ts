import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession, Model } from 'mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CashDrawerDocument } from '../schemas/cash-drawer.schema'

@Injectable({ scope: Scope.REQUEST })
export class CashDrawerService {
  constructor(
    @InjectModel('cashDrawers')
    private readonly CashDrawerModel: PaginateModel<CashDrawerDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<CashDrawerDocument | any> {
    return this.CashDrawerModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.CashDrawerModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.CashDrawerModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<CashDrawerDocument> {
    return this.CashDrawerModel
  }

  async getSession(): Promise<ClientSession> {
    await this.CashDrawerModel.createCollection()

    return await this.CashDrawerModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<CashDrawerDocument> {
    const document = new this.CashDrawerModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async createMany(payload: any): Promise<any> {
    return this.CashDrawerModel.insertMany(payload, { ordered: true })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.CashDrawerModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<CashDrawerDocument> {
    const document = new this.CashDrawerModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<CashDrawerDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<CashDrawerDocument> {
    return this.CashDrawerModel.updateOne(condition, { ...payload })
  }

  async updateMany(condition: any, payload: any): Promise<Array<any>> {
    return this.CashDrawerModel.updateMany(condition, { $set: payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<CashDrawerDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateOne(
    condition: any,
    payload: any,
    session: ClientSession
  ) {
    return this.CashDrawerModel.findOneAndUpdate(condition, payload, session)
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.CashDrawerModel.updateMany(
      condition,
      { $set: payload },
      { session }
    )
  }

  async delete(id: string): Promise<CashDrawerDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.CashDrawerModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.CashDrawerModel.findById(id)
  }

  findOne(condition, project?: any): Promise<CashDrawerDocument> {
    return this.CashDrawerModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: any,
    select?: any
  ): PaginateResult<CashDrawerDocument> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.CashDrawerModel.paginate(query, options)
  }
}
