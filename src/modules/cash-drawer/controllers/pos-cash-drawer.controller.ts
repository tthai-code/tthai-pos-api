import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { ClosingCashDto } from '../dto/closing-cash.dto'
import { CashActivityDto } from '../dto/handle-cash-action.dto'
import {
  CashDrawerStatusResponse,
  SummaryClosingCashResponse
} from '../entity/pos-cash-drawer.entity'
import { POSCashDrawerLogic } from '../logic/pos-cash-drawer.logic'
import { CashDrawerService } from '../services/cash-drawer.service'

@ApiBearerAuth()
@ApiTags('pos/cash-drawer')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/cash-drawer')
export class POSCashDrawerController {
  constructor(
    private readonly cashDrawerService: CashDrawerService,
    private readonly posCashDrawerLogic: POSCashDrawerLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Get('status')
  @ApiOkResponse({ type: () => CashDrawerStatusResponse })
  @ApiOperation({
    summary: 'Get Action Before Create Action',
    description: 'use bearer `staff_token`'
  })
  async getStatus() {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.posCashDrawerLogic.getStatus(restaurantId)
  }

  @Post()
  @ApiCreatedResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Create Cash Drawer Action',
    description: 'use bearer `staff_token`'
  })
  async handleCashAction(
    @Body() payload: CashActivityDto,
    @Request() request: any
  ) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const cashDrawer = await this.posCashDrawerLogic.handleCashActivity(
      restaurantId,
      payload
    )
    await this.logLogic.createLogic(
      request,
      'Create Cash Drawer Action',
      cashDrawer
    )
    return cashDrawer
  }

  @Get('summary')
  @ApiOkResponse({ type: () => SummaryClosingCashResponse })
  @ApiOperation({
    summary: 'Get Expected Amount Before Closing Cash',
    description: 'use bearer `staff_token`'
  })
  async getSummaryCash(@Request() request: any) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const summaryCash = await this.posCashDrawerLogic.getSummaryCash(
      restaurantId
    )
    await this.logLogic.createLogic(
      request,
      'Get Expected Amount Before Closing Cash',
      summaryCash
    )
    return summaryCash
  }

  @Post('closing')
  @ApiCreatedResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Closing Cash Drawer Action',
    description: 'use bearer `staff_token`'
  })
  async closingCash(@Body() payload: ClosingCashDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.posCashDrawerLogic.closingCash(restaurantId, payload)
  }
}
