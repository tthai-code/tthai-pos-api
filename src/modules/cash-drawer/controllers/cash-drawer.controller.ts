import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { PaginateCashDrawerDto } from '../dto/get-cash-drawer.dto'
import {
  CashDrawerWithActivityResponse,
  PaginateCashDrawerResponse
} from '../entity/cash-drawer.entity'
import { CashDrawerLogic } from '../logic/cash-drawer.logic'
import { CashDrawerService } from '../services/cash-drawer.service'

@ApiBearerAuth()
@ApiTags('cash-drawer')
@UseGuards(AdminAuthGuard)
@Controller('v1/cash-drawer')
export class CashDrawerController {
  constructor(
    private readonly cashDrawerService: CashDrawerService,
    private readonly cashDrawerLogic: CashDrawerLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaginateCashDrawerResponse })
  @ApiOperation({ summary: 'get cash drawer' })
  async getCashDrawers(@Query() query: PaginateCashDrawerDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.cashDrawerLogic.getCashDrawer(restaurantId, query)
  }

  @Get(':cashDrawerId')
  @ApiOkResponse({ type: () => CashDrawerWithActivityResponse })
  @ApiOperation({ summary: 'Get Cash Activity by Cash Drawer ID' })
  async getCashActivity(@Param('cashDrawerId') id: string) {
    return await this.cashDrawerLogic.getCashDrawerActivities(id)
  }
}
