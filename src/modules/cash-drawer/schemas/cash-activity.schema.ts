import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { CashDrawerActionEnum } from '../common/cash-drawer.enum'

export type CashActivityDocument = CashActivityFields & Document

@Schema({ timestamps: true, strict: true, collection: 'cashActivities' })
export class CashActivityFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'cashDrawers' })
  cashDrawerId: Types.ObjectId

  @Prop({ required: true })
  date: Date

  @Prop({ default: 0 })
  amount: number

  @Prop({ required: true })
  user: string

  @Prop({ default: '' })
  comment: string

  @Prop({ enum: Object.values(CashDrawerActionEnum) })
  action: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const CashActivitySchema =
  SchemaFactory.createForClass(CashActivityFields)
CashActivitySchema.plugin(mongoosePaginate)
CashActivitySchema.plugin(authorStampCreatePlugin)
