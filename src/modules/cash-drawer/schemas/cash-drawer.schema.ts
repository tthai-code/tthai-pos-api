import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { CashDrawerStatusEnum } from '../common/cash-drawer.enum'

export type CashDrawerDocument = CashDrawerFields & Document

@Schema({
  timestamps: true,
  strict: true,
  collection: 'cashDrawers',
  toJSON: { virtuals: true },
  toObject: { virtuals: true }
})
export class CashDrawerFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  startTime: Date

  @Prop({ default: null })
  endTime: Date

  @Prop({
    enum: Object.values(CashDrawerStatusEnum),
    default: CashDrawerStatusEnum.OPEN
  })
  cashDrawerStatus: string

  @Prop({ default: 0 })
  startingCash: number

  @Prop({ default: 0 })
  totalCashSales: number

  @Prop({ default: 0 })
  cashIn: number

  @Prop({ default: 0 })
  cashOut: number

  @Prop({ default: 0 })
  actualAmount: number

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const CashDrawerSchema = SchemaFactory.createForClass(CashDrawerFields)
CashDrawerSchema.plugin(mongoosePaginate)
CashDrawerSchema.plugin(authorStampCreatePlugin)
CashDrawerSchema.virtual('expectedAmount').get(function () {
  const { startingCash, totalCashSales, cashIn, cashOut } = this
  return startingCash + totalCashSales + cashIn - cashOut
})
