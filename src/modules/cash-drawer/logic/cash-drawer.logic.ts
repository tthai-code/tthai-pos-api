import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { PaginateCashDrawerDto } from '../dto/get-cash-drawer.dto'
import { ICashDrawerResponse } from '../interfaces/cash-drawer.interfaces'
import { CashActivityService } from '../services/cash-activity.service'
import { CashDrawerService } from '../services/cash-drawer.service'

@Injectable()
export class CashDrawerLogic {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly cashDrawerService: CashDrawerService,
    private readonly cashActivityService: CashActivityService
  ) {}

  async getCashDrawer(restaurantId: string, query: PaginateCashDrawerDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found Restaurant.')
    const cashDrawers = await this.cashDrawerService.paginate(
      query.buildQuery(restaurantId),
      query
    )
    const { docs } = cashDrawers
    const transform = docs.map((item) => {
      const expectedAmount = Number(
        (
          item.startingCash +
          item.totalCashSales +
          item.cashIn -
          item.cashOut
        ).toFixed(2)
      )
      const difference = Number((item.actualAmount - expectedAmount).toFixed(2))
      const result: ICashDrawerResponse = {
        id: item.id,
        restaurantId,
        startTime: item.startTime,
        endTime: item.endTime,
        cashDrawerStatus: item.cashDrawerStatus,
        totalCashSales: item.totalCashSales,
        cashIn: item.cashIn,
        expectedAmount,
        actualAmount: item.actualAmount,
        difference
      }
      return result
    })
    return { ...cashDrawers, docs: transform }
  }

  async getCashDrawerActivities(id: string) {
    const cashDrawer = await this.cashDrawerService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!cashDrawer) {
      throw new NotFoundException('Not found cash drawer activity.')
    }
    const cashActivity = await this.cashActivityService.getAll(
      {
        cashDrawerId: id,
        status: StatusEnum.ACTIVE
      },
      {
        status: 0,
        createdAt: 0,
        updatedAt: 0,
        createdBy: 0,
        updatedBy: 0,
        cashDrawerId: 0
      },
      {
        sort: {
          date: 1
        }
      }
    )
    const expectedAmount = Number(
      (
        cashDrawer.startingCash +
        cashDrawer.totalCashSales +
        cashDrawer.cashIn -
        cashDrawer.cashOut
      ).toFixed(2)
    )
    const difference = Number(
      (cashDrawer.actualAmount - expectedAmount).toFixed(2)
    )
    const result = {
      id,
      startTime: cashDrawer.startTime,
      endTime: cashDrawer.endTime,
      activity: cashActivity,
      expectedAmount,
      actualAmount: cashDrawer.actualAmount,
      difference,
      cashDrawerStatus: cashDrawer.cashDrawerStatus
    }
    return result
  }
}
