import {
  BadGatewayException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import {
  CashDrawerActionEnum,
  CashDrawerStatusEnum
} from '../common/cash-drawer.enum'
import { ClosingCashDto } from '../dto/closing-cash.dto'
import { CashActivityDto } from '../dto/handle-cash-action.dto'
import {
  ICashDrawer,
  ICashSales,
  ISummaryCashForClosing
} from '../interfaces/cash-drawer.interfaces'
import { CashActivityService } from '../services/cash-activity.service'
import { CashDrawerService } from '../services/cash-drawer.service'

@Injectable()
export class POSCashDrawerLogic {
  constructor(
    private readonly cashDrawerService: CashDrawerService,
    private readonly cashActivityService: CashActivityService,
    private readonly restaurantService: RestaurantService
  ) {}

  async getStatus(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found Restaurant.')
    const openCashDrawer = await this.cashDrawerService.findOne({
      restaurantId,
      cashDrawerStatus: CashDrawerStatusEnum.OPEN
    })
    if (!openCashDrawer) {
      return { isOpenCashDrawerStatus: false }
    }
    return { isOpenCashDrawerStatus: true }
  }

  private async startingCash(restaurantId: string, payload: CashActivityDto) {
    let openCashDrawer = await this.cashDrawerService.findOne({
      restaurantId,
      cashDrawerStatus: CashDrawerStatusEnum.OPEN
    })
    payload.restaurantId = restaurantId
    const logic = async (session: ClientSession) => {
      if (!openCashDrawer) {
        const cashDrawerPayload: ICashDrawer = {
          restaurantId,
          startTime: payload.date,
          startingCash: payload.amount
        }
        openCashDrawer = await this.cashDrawerService.transactionCreate(
          cashDrawerPayload,
          session
        )
      } else {
        await this.cashDrawerService.transactionUpdateOne(
          { _id: openCashDrawer.id },
          {
            $inc: { startingCash: payload.amount }
          },
          session
        )
      }
      payload.cashDrawerId = openCashDrawer.id
      await this.cashActivityService.transactionCreate(payload, session)
      return
    }
    await runTransaction(logic)
    return { success: true }
  }

  private async cashInOut(restaurantId: string, payload: CashActivityDto) {
    const openCashDrawer = await this.cashDrawerService.findOne({
      restaurantId,
      cashDrawerStatus: CashDrawerStatusEnum.OPEN
    })
    if (!openCashDrawer) {
      throw new BadGatewayException(
        'Please Starting Cash Before do another action.'
      )
    }
    payload.restaurantId = restaurantId
    payload.cashDrawerId = openCashDrawer.id
    let inc
    if (payload.action === CashDrawerActionEnum.CASH_IN) {
      inc = { $inc: { cashIn: payload.amount } }
    } else if (payload.action === CashDrawerActionEnum.CASH_OUT) {
      inc = { $inc: { cashOut: payload.amount } }
    }
    const logic = async (session: ClientSession) => {
      await this.cashActivityService.transactionCreate(payload, session)
      await this.cashDrawerService.transactionUpdateOne(
        { _id: openCashDrawer.id },
        inc,
        session
      )
      return
    }
    await runTransaction(logic)
    return { success: true }
  }

  private async cashCount(restaurantId: string, payload: CashActivityDto) {
    const openCashDrawer = await this.cashDrawerService.findOne({
      restaurantId,
      cashDrawerStatus: CashDrawerStatusEnum.OPEN
    })
    if (!openCashDrawer) {
      throw new BadGatewayException(
        'Please Starting Cash Before do another action.'
      )
    }
    payload.restaurantId = restaurantId
    payload.cashDrawerId = openCashDrawer.id
    await this.cashActivityService.create(payload)

    return { success: true }
  }

  async handleCashActivity(restaurantId: string, payload: CashActivityDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found Restaurant.')
    const { action } = payload
    if (action === CashDrawerActionEnum.STARTING_CASH) {
      return this.startingCash(restaurantId, payload)
    } else if (action === CashDrawerActionEnum.CASH_IN) {
      return this.cashInOut(restaurantId, payload)
    } else if (action === CashDrawerActionEnum.CASH_OUT) {
      return this.cashInOut(restaurantId, payload)
    } else if (action === CashDrawerActionEnum.CASH_COUNT) {
      return this.cashCount(restaurantId, payload)
    }
    return { success: false }
  }

  async getSummaryCash(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found Restaurant.')
    const openCashDrawer = await this.cashDrawerService.findOne({
      restaurantId,
      cashDrawerStatus: CashDrawerStatusEnum.OPEN
    })
    if (!openCashDrawer) {
      throw new BadGatewayException(
        'Please Starting Cash Before do another action.'
      )
    }
    const { startingCash, totalCashSales, cashIn, cashOut } = openCashDrawer
    const payload: ISummaryCashForClosing = {
      expectedAmount: startingCash + totalCashSales + cashIn - cashOut
    }
    return payload
  }

  async closingCash(restaurantId: string, payload: ClosingCashDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found Restaurant.')
    const openCashDrawer = await this.cashDrawerService.findOne({
      restaurantId,
      cashDrawerStatus: CashDrawerStatusEnum.OPEN
    })
    if (!openCashDrawer) {
      throw new BadGatewayException(
        'Please Starting Cash Before do another action.'
      )
    }
    await this.cashDrawerService.update(openCashDrawer.id, {
      endTime: payload.date,
      actualAmount: payload.actualAmount,
      cashDrawerStatus: CashDrawerStatusEnum.CLOSED
    })
    return { success: true }
  }

  public async cashSales(restaurantId: string, payload: ICashSales) {
    const openCashDrawer = await this.cashDrawerService.findOne({
      restaurantId,
      cashDrawerStatus: CashDrawerStatusEnum.OPEN
    })
    if (!openCashDrawer) {
      throw new BadGatewayException(
        'Please Starting Cash Before do another action.'
      )
    }
    payload.restaurantId = restaurantId
    payload.cashDrawerId = openCashDrawer.id
    const logic = async (session: ClientSession) => {
      await this.cashActivityService.transactionCreate(payload, session)
      await this.cashDrawerService.transactionUpdateOne(
        { _id: openCashDrawer.id },
        { $inc: { totalCashSales: payload.amount } },
        session
      )
      return
    }
    await runTransaction(logic)
    return { success: true }
  }
}
