export enum SubscriptionPaymentStatus {
  SUCCESSFUL = 'SUCCESSFUL',
  PENDING = 'PENDING',
  FAILURE = 'FAILURE'
}
