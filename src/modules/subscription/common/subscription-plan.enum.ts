export enum SubscriptionPlanEnum {
  YEARLY = 'YEARLY',
  MONTHLY = 'MONTHLY',
  FREE = 'FREE'
}

export enum SubscriptionStatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}
