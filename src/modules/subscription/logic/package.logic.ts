import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import dayjs from 'src/plugins/dayjs'
import { PackagePaginate } from '../dto/get-package.dto'
import { PackageService } from '../services/package.service'

@Injectable()
export class PackageLogic {
  constructor(private readonly packageService: PackageService) {}

  async getPackageList(query: PackagePaginate) {
    const packageList = await this.packageService.paginate(
      query.buildQuery(),
      query
    )

    const { docs } = packageList
    const dayInMonth = dayjs().daysInMonth()
    const resultDocs = docs.map((item) => ({
      ...item.toObject(),
      dailyPrice: item.monthlyPrice / dayInMonth
    }))
    return { ...packageList, docs: resultDocs }
  }

  async getPackageByID(id: string) {
    const packageData = await this.packageService.findOne(
      {
        _id: id,
        status: StatusEnum.ACTIVE
      },
      { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 }
    )
    if (!packageData) {
      throw new NotFoundException('Not found package.')
    }
    const dayInMonth = dayjs().daysInMonth()
    return {
      ...packageData.toObject(),
      dailyPrice: packageData.monthlyPrice / dayInMonth
    }
  }
}
