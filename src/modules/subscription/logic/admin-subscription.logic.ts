import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import {
  SubscriptionPlanEnum,
  SubscriptionStatusEnum
} from '../common/subscription-plan.enum'
import { EditSubscriptionDto } from '../dto/edit-subscription.dto'
import { SubscriptionPaginateDto } from '../dto/get-subscription.dto'
import { PackageService } from '../services/package.service'
import { SubscriptionService } from '../services/subscription.service'

@Injectable()
export class AdminSubscriptionLogic {
  constructor(
    private readonly subscriptionService: SubscriptionService,
    private readonly restaurantService: RestaurantService,
    private readonly packageService: PackageService
  ) {}

  async getSubscriptionByRestaurant(
    restaurantId: string,
    query: SubscriptionPaginateDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: { $ne: StatusEnum.DELETED }
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return this.subscriptionService.paginate(
      query.buildQuery(restaurantId),
      query
    )
  }

  async unsubscribe(subscriptionId: string) {
    const subscription = await this.subscriptionService.findOne({
      _id: subscriptionId,
      status: StatusEnum.ACTIVE
    })
    if (!subscription) {
      throw new NotFoundException('Not found subscription by this id.')
    }
    if (subscription.subscriptionStatus === SubscriptionStatusEnum.INACTIVE) {
      throw new BadRequestException('Subscription is already inactive.')
    }
    return this.subscriptionService.updateOne(
      { _id: subscriptionId },
      { subscriptionStatus: SubscriptionStatusEnum.INACTIVE }
    )
  }

  async subscribe(subscriptionId: string) {
    const subscription = await this.subscriptionService.findOne({
      _id: subscriptionId,
      status: StatusEnum.ACTIVE
    })
    if (!subscription) {
      throw new NotFoundException('Not found subscription by this id.')
    }
    if (subscription.subscriptionStatus === SubscriptionStatusEnum.ACTIVE) {
      throw new BadRequestException('Subscription is already active.')
    }
    return this.subscriptionService.updateOne(
      { _id: subscriptionId },
      { subscriptionStatus: SubscriptionStatusEnum.ACTIVE }
    )
  }

  async updateSubscription(
    subscriptionId: string,
    payload: EditSubscriptionDto
  ) {
    const subscription = await this.subscriptionService.findOne({
      _id: subscriptionId,
      status: StatusEnum.ACTIVE
    })
    if (!subscription) {
      throw new NotFoundException('Not found subscription by this id.')
    }
    const packageData = await this.packageService.findOne({
      _id: subscription.packageId,
      status: StatusEnum.ACTIVE
    })
    if (!packageData) throw new NotFoundException('Not found package.')
    const paymentPlan = payload.paymentPlan
    if (paymentPlan === SubscriptionPlanEnum.MONTHLY) {
      payload.price = packageData.monthlyPrice
    } else if (paymentPlan === SubscriptionPlanEnum.YEARLY) {
      payload.price = packageData.yearlyPrice
    } else {
      payload.price = 0
      payload.paymentPlan = SubscriptionPlanEnum.FREE
    }
    payload.amount = payload.price * payload.quantity
    return this.subscriptionService.updateOne({ _id: subscriptionId }, payload)
  }
}
