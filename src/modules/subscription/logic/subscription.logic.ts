import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import dayjs from 'src/plugins/dayjs'
import { StatusEnum } from 'src/common/enum/status.enum'
import { BankAccountStatusEnum } from 'src/modules/payment-gateway/common/ach-bank.type.enum'
import { BankAccountService } from 'src/modules/payment-gateway/services/bank-account.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import {
  SubscriptionPlanEnum,
  SubscriptionStatusEnum
} from '../common/subscription-plan.enum'
import { AddSubscriptionDto } from '../dto/add-subscription.dto'
import { ISubscriptionPayload } from '../interfaces/subscription.interfaces'
import { PackageService } from '../services/package.service'
import { SubscriptionService } from '../services/subscription.service'

@Injectable()
export class SubscriptionLogic {
  constructor(
    private readonly subscriptionService: SubscriptionService,
    private readonly packageService: PackageService,
    private readonly restaurantService: RestaurantService,
    private readonly bankAccountService: BankAccountService
  ) {}

  async initDefaultPackage(restaurantId: string) {
    const defaultPackages = await this.packageService.getAll({
      isActiveDefault: true,
      isFree: true,
      status: StatusEnum.ACTIVE
    })
    const subscription = await this.subscriptionService.getAll({
      restaurantId,
      status: StatusEnum.ACTIVE,
      subscriptionStatus: SubscriptionStatusEnum.ACTIVE
    })
    const defaultPackageIds = defaultPackages.map((item) => `${item._id}`)
    const subscriptionIds = subscription.map((item) => item.packageId)
    const isAlreadyInit = subscriptionIds.some((item) =>
      defaultPackageIds.includes(item)
    )
    if (!isAlreadyInit) {
      const subscriptionsPayload = []
      const now = dayjs().tz('Etc/GMT+5')
      for (const item of defaultPackages) {
        const subscribe: ISubscriptionPayload = {
          restaurantId,
          packageId: item.id,
          name: item.name,
          paymentPlan: SubscriptionPlanEnum.FREE,
          quantity: 1,
          price: 0,
          amount: 0,
          subscribedAt: now.toDate(),
          autoRenewAt: now.add(1, 'month').date(1).startOf('day').toDate()
        }
        subscriptionsPayload.push(subscribe)
      }
      return this.subscriptionService.createMany(subscriptionsPayload)
    }
    return null
  }

  async addSubscription(restaurantId: string, payload: AddSubscriptionDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const bankAccount = await this.bankAccountService.findOne({
      restaurantId,
      token: { $ne: null },
      bankAccountStatus: BankAccountStatusEnum.ACTIVE,
      status: StatusEnum.ACTIVE
    })
    if (!bankAccount) {
      throw new BadRequestException(
        'Must be active ACH Bank Account before subscribe.'
      )
    }
    const packageData = await this.packageService.findOne({
      _id: payload.packageId,
      status: StatusEnum.ACTIVE
    })
    if (!packageData) throw new NotFoundException('Not found package.')
    let price: number
    let renewType: string
    const paymentPlan = payload.paymentPlan
    if (paymentPlan === SubscriptionPlanEnum.MONTHLY) {
      price = packageData.monthlyPrice
      renewType = 'month'
    } else if (paymentPlan === SubscriptionPlanEnum.YEARLY) {
      price = packageData.yearlyPrice
      renewType = 'year'
    } else {
      price = 0
      payload.paymentPlan = SubscriptionPlanEnum.FREE
      renewType = 'month'
    }
    const now = dayjs().tz('Etc/GMT+5')
    const transform: ISubscriptionPayload = {
      restaurantId,
      packageId: payload.packageId,
      name: packageData.name,
      paymentPlan,
      quantity: payload.quantity,
      price,
      amount: payload.quantity * price,
      subscribedAt: now.toDate(),
      autoRenewAt: now.add(1, renewType).date(1).startOf('day').toDate()
    }
    return this.subscriptionService.create(transform)
  }
}
