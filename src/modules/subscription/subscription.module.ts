import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { PaymentGatewayModule } from '../payment-gateway/payment-gateway.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { AdminPackageController } from './controllers/admin-package.controller'
import { AdminSubscriptionController } from './controllers/admin-subscription.controller'
import { PackageController } from './controllers/package.controller'
import { SubscriptionController } from './controllers/subscription.controller'
import { AdminSubscriptionLogic } from './logic/admin-subscription.logic'
import { PackageLogic } from './logic/package.logic'
import { SubscriptionLogic } from './logic/subscription.logic'
import { PackageSchema } from './schemas/package.schema'
import { SubscriptionTransactionSchema } from './schemas/subscription-transaction.schema'
import { SubscriptionSchema } from './schemas/subscription.schema'
import { PackageService } from './services/package.service'
import { SubscriptionTransactionService } from './services/subscription-transaction.service'
import { SubscriptionService } from './services/subscription.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    forwardRef(() => PaymentGatewayModule),
    MongooseModule.forFeature([
      { name: 'package', schema: PackageSchema },
      { name: 'subscription', schema: SubscriptionSchema },
      { name: 'subscriptionTransaction', schema: SubscriptionTransactionSchema }
    ])
  ],
  providers: [
    PackageService,
    SubscriptionService,
    SubscriptionLogic,
    AdminSubscriptionLogic,
    SubscriptionTransactionService,
    PackageLogic
  ],
  controllers: [
    AdminPackageController,
    SubscriptionController,
    AdminSubscriptionController,
    PackageController
  ],
  exports: [SubscriptionService, PackageService, SubscriptionLogic]
})
export class SubscriptionModule {}
