import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import {
  SubscriptionPlanEnum,
  SubscriptionStatusEnum
} from '../common/subscription-plan.enum'

export type SubscriptionDocument = SubscriptionFields & Document

@Schema({ timestamps: true, strict: true, collection: 'subscription' })
export class SubscriptionFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  packageId: string

  @Prop()
  name: string

  @Prop({ enum: Object.values(SubscriptionPlanEnum), required: true })
  paymentPlan: string

  @Prop({ default: 1 })
  quantity: number

  @Prop({ default: 0 })
  price: number

  @Prop({ default: 0 })
  amount: number

  @Prop({
    enum: Object.values(SubscriptionStatusEnum),
    default: SubscriptionStatusEnum.ACTIVE
  })
  subscriptionStatus: string

  @Prop({ default: null })
  subscribedAt: Date

  @Prop({ default: null })
  lastRenewAt: Date

  @Prop({ default: null })
  autoRenewAt: Date

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const SubscriptionSchema =
  SchemaFactory.createForClass(SubscriptionFields)
SubscriptionSchema.plugin(authorStampCreatePlugin)
SubscriptionSchema.plugin(mongoosePaginate)
