import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, Schema as MongooseSchema, Types } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { SubscriptionPaymentStatus } from '../common/subscription-payment-status.enum'

export type SubscriptionTransactionDocument = SubscriptionTransactionField &
  Document

@Schema({
  timestamps: true,
  strict: true,
  collection: 'subscriptionTransactions'
})
export class SubscriptionTransactionField {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'subscription' })
  subscriptionId: Types.ObjectId

  @Prop({ type: MongooseSchema.Types.Mixed, default: null })
  response: any

  @Prop({ default: null })
  subscribedAt: Date

  @Prop({ default: null })
  autoRenewAt: Date

  @Prop({
    enum: Object.values(SubscriptionPaymentStatus),
    default: SubscriptionPaymentStatus.PENDING
  })
  paymentStatus: string
}
export const SubscriptionTransactionSchema = SchemaFactory.createForClass(
  SubscriptionTransactionField
)
SubscriptionTransactionSchema.plugin(mongoosePaginate)
