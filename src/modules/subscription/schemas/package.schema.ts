import * as mongoosePaginate from 'mongoose-paginate'
import { Document } from 'mongoose'
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type PackageDocument = PackageFields & Document

@Schema({ timestamps: true, strict: true, collection: 'packages' })
export class PackageFields {
  @Prop({ required: true })
  name: string

  @Prop({ default: '' })
  description: string

  @Prop({ default: 0 })
  monthlyPrice: number

  @Prop({ default: 0 })
  yearlyPrice: number

  @Prop({ default: false })
  isFree: boolean

  @Prop({ default: false })
  isActiveDefault: boolean

  @Prop({ default: 0 })
  iPadCapacity: number

  @Prop({ default: [] })
  packageType: string[]

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const PackageSchema = SchemaFactory.createForClass(PackageFields)
PackageSchema.plugin(mongoosePaginate)
PackageSchema.plugin(authorStampCreatePlugin)
