import { Controller, Get, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { GetAllPackageResponse } from '../entity/package.entity'
import { PackageService } from '../services/package.service'

@ApiBearerAuth()
@ApiTags('package')
@UseGuards(AdminAuthGuard)
@Controller('v1/package')
export class PackageController {
  constructor(private readonly packageService: PackageService) {}

  @Get()
  @ApiOkResponse({ type: () => GetAllPackageResponse })
  @ApiOperation({
    summary: 'Get All Package list for subscribe.',
    description: 'use bearer `restaurant_token`'
  })
  async getAllPackage() {
    return await this.packageService.getAll(
      { status: StatusEnum.ACTIVE, isActiveDefault: false },
      { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 }
    )
  }
}
