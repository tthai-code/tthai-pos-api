import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { SuperAdminAuthGuard } from 'src/modules/auth/guards/super-admin-auth.guard'
import { AddSubscriptionDto } from '../dto/add-subscription.dto'
import { EditSubscriptionDto } from '../dto/edit-subscription.dto'
import { SubscriptionPaginateDto } from '../dto/get-subscription.dto'
import {
  AdminSubscriptionPaginateResponse,
  AdminSubscriptionResponse
} from '../entity/admin-subscription.entity'
import { AdminSubscriptionLogic } from '../logic/admin-subscription.logic'
import { SubscriptionLogic } from '../logic/subscription.logic'
import { SubscriptionService } from '../services/subscription.service'

@ApiBearerAuth()
@ApiTags('admin/subscription')
@UseGuards(SuperAdminAuthGuard)
@Controller('/v1/admin/subscription')
export class AdminSubscriptionController {
  constructor(
    private readonly subscriptionService: SubscriptionService,
    private readonly adminSubscriptionLogic: AdminSubscriptionLogic,
    private readonly subscriptionLogic: SubscriptionLogic
  ) {}

  @Get(':restaurantId/restaurant')
  @ApiOkResponse({ type: () => AdminSubscriptionPaginateResponse })
  @ApiOperation({
    summary: 'Get Subscription by Restaurant',
    description: 'use bearer `admin_token`'
  })
  async getSubscription(
    @Param('restaurantId') restaurantId: string,
    @Query() query: SubscriptionPaginateDto
  ) {
    return await this.adminSubscriptionLogic.getSubscriptionByRestaurant(
      restaurantId,
      query
    )
  }

  @Get(':subscriptionId')
  @ApiOkResponse({ type: () => AdminSubscriptionResponse })
  @ApiOperation({
    summary: 'Get a subscription by id',
    description: 'use bearer `admin_token`'
  })
  async getSubscriptionById(@Param('subscriptionId') subscriptionId: string) {
    const subscription = await this.subscriptionService.findOne({
      _id: subscriptionId,
      status: StatusEnum.ACTIVE
    })
    if (!subscription) throw new NotFoundException('Not found subscription.')
    return subscription
  }

  @Patch(':subscriptionId/unsubscribe')
  @ApiOkResponse({ type: () => AdminSubscriptionResponse })
  @ApiOperation({
    summary: 'Unsubscribe by id',
    description: 'user bearer `admin_token`'
  })
  async unsubscribe(@Param('subscriptionId') subscriptionId: string) {
    return await this.adminSubscriptionLogic.unsubscribe(subscriptionId)
  }

  @Patch(':subscriptionId/subscribe')
  @ApiOkResponse({ type: () => AdminSubscriptionResponse })
  @ApiOperation({
    summary: 'Subscribe by id',
    description: 'user bearer `admin_token`'
  })
  async subscribe(@Param('subscriptionId') subscriptionId: string) {
    return await this.adminSubscriptionLogic.subscribe(subscriptionId)
  }

  @Put(':subscriptionId')
  @ApiOkResponse({ type: () => AdminSubscriptionResponse })
  @ApiOperation({
    summary: 'Edit Subscription by id',
    description: 'use bearer `admin_token`'
  })
  async updateSubscription(
    @Param('subscriptionId') subscriptionId: string,
    @Body() payload: EditSubscriptionDto
  ) {
    return await this.adminSubscriptionLogic.updateSubscription(
      subscriptionId,
      payload
    )
  }

  @Post(':restaurantId')
  @ApiCreatedResponse({ type: () => AdminSubscriptionResponse })
  @ApiOperation({
    summary: 'Add Subscription for restaurant.',
    description: 'use bearer `admin_token`'
  })
  async addSubscription(
    @Param('restaurantId') restaurantId: string,
    @Body() payload: AddSubscriptionDto
  ) {
    return await this.subscriptionLogic.addSubscription(restaurantId, payload)
  }
}
