import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuperAdminAuthGuard } from 'src/modules/auth/guards/super-admin-auth.guard'
import { CreatePackageDto } from '../dto/create-package.dto'
import { PackagePaginate } from '../dto/get-package.dto'
import {
  PackagePaginateResponse,
  PackageResponse
} from '../entity/package.entity'
import { PackageLogic } from '../logic/package.logic'
import { PackageService } from '../services/package.service'

@ApiBearerAuth()
@ApiTags('admin/package')
@UseGuards(SuperAdminAuthGuard)
@Controller('v1/admin/package')
export class AdminPackageController {
  constructor(
    private readonly packageService: PackageService,
    private readonly packageLogic: PackageLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PackagePaginateResponse })
  @ApiOperation({
    summary: 'Get Package List',
    description: 'use barer `admin_token`'
  })
  async getPackages(@Query() query: PackagePaginate) {
    return await this.packageLogic.getPackageList(query)
  }

  @Post()
  @ApiCreatedResponse({ type: () => PackageResponse })
  @ApiOperation({
    summary: 'Create a  Package',
    description: 'use barer `admin_token`'
  })
  async createPackage(@Body() payload: CreatePackageDto) {
    payload.isFree = payload.monthlyPrice + payload.yearlyPrice === 0
    return await this.packageService.create(payload)
  }

  @Get(':id')
  @ApiOkResponse({ type: () => PackageResponse })
  @ApiOperation({
    summary: 'Get a Package by ID',
    description: 'use barer `admin_token`'
  })
  async getPackage(@Param('id') id: string) {
    return await this.packageLogic.getPackageByID(id)
  }

  @Put(':id')
  @ApiOkResponse({ type: () => PackageResponse })
  @ApiOperation({
    summary: 'Update Package by ID',
    description: 'use barer `admin_token`'
  })
  async updatePackage(
    @Param('id') id: string,
    @Body() payload: CreatePackageDto
  ) {
    payload.isFree = payload.monthlyPrice + payload.yearlyPrice === 0
    return await this.packageService.update(id, payload)
  }

  @Delete(':id')
  @ApiOkResponse({ type: () => PackageResponse })
  @ApiOperation({
    summary: 'Delete Package by ID',
    description: 'use barer `admin_token`'
  })
  async deletePackage(@Param('id') id: string) {
    return await this.packageService.delete(id)
  }
}
