import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { AddSubscriptionDto } from '../dto/add-subscription.dto'
import { SubscriptionPaginateDto } from '../dto/get-subscription.dto'
import {
  GetSubscriptionListResponse,
  SubscriptionResponse
} from '../entity/subscription.entity'
import { SubscriptionLogic } from '../logic/subscription.logic'
import { SubscriptionService } from '../services/subscription.service'

@ApiBearerAuth()
@ApiTags('subscription')
@UseGuards(AdminAuthGuard)
@Controller('v1/subscription')
export class SubscriptionController {
  constructor(
    private readonly subscriptionService: SubscriptionService,
    private readonly subscriptionLogic: SubscriptionLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => GetSubscriptionListResponse })
  @ApiOperation({
    summary: 'Get Subscription List',
    description: 'use bearer `restaurant_token`'
  })
  async getSubscriptionList(@Query() query: SubscriptionPaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.subscriptionService.paginate(
      query.buildQuery(restaurantId),
      query
    )
  }

  @Post()
  @ApiCreatedResponse({ type: () => SubscriptionResponse })
  @ApiOperation({
    summary: 'Add New Subscription',
    description: 'use bearer `restaurant_token`'
  })
  async addSubscription(@Body() payload: AddSubscriptionDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.subscriptionLogic.addSubscription(restaurantId, payload)
  }
}
