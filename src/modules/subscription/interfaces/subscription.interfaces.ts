export interface ISubscriptionPayload {
  restaurantId: string
  packageId: string
  name: string
  paymentPlan: string
  quantity: number
  price: number
  amount: number
  subscriptionStatus?: string
  subscribedAt?: Date
  lastRenewAt?: Date
  autoRenewAt?: Date
}
