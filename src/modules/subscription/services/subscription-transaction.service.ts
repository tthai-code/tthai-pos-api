import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { SubscriptionTransactionDocument } from '../schemas/subscription-transaction.schema'

@Injectable({ scope: Scope.REQUEST })
export class SubscriptionTransactionService {
  constructor(
    @InjectModel('subscriptionTransaction')
    private readonly SubscriptionTransactionModel:
      | PaginateModel<SubscriptionTransactionDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<SubscriptionTransactionDocument | any> {
    return this.SubscriptionTransactionModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.SubscriptionTransactionModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.SubscriptionTransactionModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<SubscriptionTransactionDocument> {
    return this.SubscriptionTransactionModel
  }

  async getSession(): Promise<ClientSession> {
    await this.SubscriptionTransactionModel.createCollection()

    return await this.SubscriptionTransactionModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<SubscriptionTransactionDocument> {
    const document = new this.SubscriptionTransactionModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.SubscriptionTransactionModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<SubscriptionTransactionDocument> {
    const document = new this.SubscriptionTransactionModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async createMany(payload: any): Promise<SubscriptionTransactionDocument> {
    return this.SubscriptionTransactionModel.insertMany(payload)
  }

  async update(
    id: string,
    product: any
  ): Promise<SubscriptionTransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(
    condition: any,
    payload: any
  ): Promise<SubscriptionTransactionDocument> {
    return this.SubscriptionTransactionModel.findOneAndUpdate(
      condition,
      { $set: { ...payload } },
      { new: true }
    )
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<SubscriptionTransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.SubscriptionTransactionModel.updateMany(
      condition,
      { $set: payload },
      { session }
    )
  }

  async delete(id: string): Promise<SubscriptionTransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any, options?: any): Promise<any[]> {
    return this.SubscriptionTransactionModel.find(condition, project, options)
  }

  findById(id): Promise<any> {
    return this.SubscriptionTransactionModel.findById(id)
  }

  findOne(condition, project?: any): Promise<SubscriptionTransactionDocument> {
    return this.SubscriptionTransactionModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: any,
    select?: any
  ): Promise<PaginateResult<SubscriptionTransactionDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.SubscriptionTransactionModel.paginate(query, options)
  }
}
