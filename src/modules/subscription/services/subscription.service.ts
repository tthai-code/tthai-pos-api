import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { SubscriptionDocument } from '../schemas/subscription.schema'

@Injectable({ scope: Scope.REQUEST })
export class SubscriptionService {
  constructor(
    @InjectModel('subscription')
    private readonly SubscriptionModel:
      | PaginateModel<SubscriptionDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<SubscriptionDocument | any> {
    return this.SubscriptionModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.SubscriptionModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.SubscriptionModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<SubscriptionDocument> {
    return this.SubscriptionModel
  }

  async getSession(): Promise<ClientSession> {
    await this.SubscriptionModel.createCollection()

    return await this.SubscriptionModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<SubscriptionDocument> {
    const document = new this.SubscriptionModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.SubscriptionModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<SubscriptionDocument> {
    const document = new this.SubscriptionModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async createMany(payload: any): Promise<SubscriptionDocument> {
    return this.SubscriptionModel.insertMany(payload)
  }

  async update(id: string, product: any): Promise<SubscriptionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<SubscriptionDocument> {
    return this.SubscriptionModel.findOneAndUpdate(
      condition,
      { $set: { ...payload } },
      { new: true }
    )
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<SubscriptionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.SubscriptionModel.updateMany(
      condition,
      { $set: payload },
      { session }
    )
  }

  async delete(id: string): Promise<SubscriptionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.SubscriptionModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.SubscriptionModel.findById(id)
  }

  findOne(condition, project?: any): Promise<SubscriptionDocument> {
    return this.SubscriptionModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: any,
    select?: any
  ): Promise<PaginateResult<SubscriptionDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.SubscriptionModel.paginate(query, options)
  }
}
