import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PackageDocument } from '../schemas/package.schema'
import { PackagePaginate } from '../dto/get-package.dto'

@Injectable({ scope: Scope.REQUEST })
export class PackageService {
  constructor(
    @InjectModel('package')
    private readonly PackageModel: PaginateModel<PackageDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<PackageDocument | any> {
    return this.PackageModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.PackageModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.PackageModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<PackageDocument> {
    return this.PackageModel
  }

  async getSession(): Promise<ClientSession> {
    await this.PackageModel.createCollection()

    return await this.PackageModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<PackageDocument> {
    const document = new this.PackageModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.PackageModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<PackageDocument> {
    const document = new this.PackageModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<PackageDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<PackageDocument> {
    return this.PackageModel.updateOne(condition, { ...payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<PackageDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async delete(id: string): Promise<PackageDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.PackageModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.PackageModel.findById(id)
  }

  findOne(condition, project?: any): Promise<PackageDocument> {
    return this.PackageModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: PackagePaginate,
    select?: any
  ): Promise<PaginateResult<PackageDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.PackageModel.paginate(query, options)
  }
}
