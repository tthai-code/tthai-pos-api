import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'
import {
  SubscriptionPlanEnum,
  SubscriptionStatusEnum
} from '../common/subscription-plan.enum'

class SubscriptionObject extends TimestampResponseDto {
  @ApiProperty()
  auto_renew_at: Date | string
  @ApiProperty()
  last_renew_at: Date | string
  @ApiProperty()
  subscribed_at: Date | string
  @ApiProperty({ enum: Object.values(SubscriptionStatusEnum) })
  subscription_status: string
  @ApiProperty()
  price: number
  @ApiProperty()
  quantity: number
  @ApiProperty({ enum: Object.values(SubscriptionPlanEnum) })
  payment_plan: string
  @ApiProperty()
  name: string
  @ApiProperty()
  package_id: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
}

class SubscriptionPaginateObject extends PaginateResponseDto<
  SubscriptionObject[]
> {
  @ApiProperty({
    type: [SubscriptionObject],
    example: {
      total: 1,
      limit: 25,
      page: 1,
      pages: 1,
      results: [
        {
          status: 'active',
          auto_renew_at: '2022-12-31T17:00:00.000Z',
          last_renew_at: null,
          subscribed_at: '2022-12-02T13:16:42.926Z',
          subscription_status: 'ACTIVE',
          price: 20,
          quantity: 1,
          payment_plan: 'MONTHLY',
          name: 'More iPad',
          package_id: '6389d131200bb5f8fcd11cf9',
          restaurant_id: '630e55a0d9c30fd7cdcb424b',
          created_at: '2022-12-02T13:16:42.938Z',
          updated_at: '2022-12-02T13:16:42.938Z',
          updated_by: {
            username: 'corywong@mail.com',
            id: '630e53ec9e21d871a49fb4f5'
          },
          created_by: {
            username: 'corywong@mail.com',
            id: '630e53ec9e21d871a49fb4f5'
          },
          id: '6389fabad4ee57a0d96a0cdc'
        }
      ]
    }
  })
  results: SubscriptionObject[]
}

export class AdminSubscriptionPaginateResponse extends ResponseDto<SubscriptionPaginateObject> {
  @ApiProperty({ type: SubscriptionPaginateObject })
  data: SubscriptionPaginateObject
}

export class AdminSubscriptionResponse extends ResponseDto<SubscriptionObject> {
  @ApiProperty({
    type: SubscriptionObject,
    example: {
      status: 'active',
      auto_renew_at: '2022-12-31T17:00:00.000Z',
      last_renew_at: null,
      subscribed_at: '2022-12-02T13:16:42.926Z',
      subscription_status: 'INACTIVE',
      price: 20,
      quantity: 1,
      payment_plan: 'MONTHLY',
      name: 'More iPad',
      package_id: '6389d131200bb5f8fcd11cf9',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-12-02T13:16:42.938Z',
      updated_at: '2022-12-07T09:49:27.138Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '6389fabad4ee57a0d96a0cdc'
    }
  })
  data: SubscriptionObject
}
