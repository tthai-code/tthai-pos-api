import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'

class SubscriptionObject extends PaginateResponseDto<any> {
  @ApiProperty({
    example: [
      {
        status: 'active',
        auto_renew_at: '2022-12-31T17:00:00.000Z',
        last_renew_at: null,
        subscribed_at: '2022-12-02T13:16:42.926Z',
        subscription_status: 'ACTIVE',
        price: 20,
        quantity: 1,
        payment_plan: 'MONTHLY',
        name: 'More iPad',
        package_id: '6389d131200bb5f8fcd11cf9',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        created_at: '2022-12-02T13:16:42.938Z',
        updated_at: '2022-12-02T13:16:42.938Z',
        updated_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        created_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        id: '6389fabad4ee57a0d96a0cdc'
      }
    ]
  })
  results: any
}

export class GetSubscriptionListResponse extends ResponseDto<SubscriptionObject> {
  @ApiProperty()
  data: SubscriptionObject
}

export class SubscriptionResponse extends ResponseDto<any> {
  @ApiProperty({
    example: {
      status: 'active',
      auto_renew_at: '2022-12-31T17:00:00.000Z',
      last_renew_at: null,
      subscribed_at: '2022-12-02T13:16:42.926Z',
      subscription_status: 'ACTIVE',
      price: 20,
      quantity: 1,
      payment_plan: 'MONTHLY',
      name: 'More iPad',
      package_id: '6389d131200bb5f8fcd11cf9',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-12-02T13:16:42.938Z',
      updated_at: '2022-12-02T13:16:42.938Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '6389fabad4ee57a0d96a0cdc'
    }
  })
  data: any
}
