import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'

export class PackageResponse extends ResponseDto<any> {
  @ApiProperty({
    example: {
      created_by: {
        username: 'superadmin',
        id: '6318d06a1fa78236d380db83'
      },
      updated_by: {
        username: 'superadmin',
        id: '6318d06a1fa78236d380db83'
      },
      status: 'active',
      package_type: ['IPAD'],
      i_pad_capacity: 1,
      is_active_default: true,
      is_free: true,
      daily_price: 0,
      yearly_price: 0,
      monthly_price: 0,
      description:
        'ฟรีค่าบริการรายเดือนสำหรับ iPad เครื่องแรก ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)',
      name: 'iPad',
      updated_at: '2022-12-20T07:56:47.996Z',
      created_at: '2022-12-02T10:20:25.976Z',
      id: '6389d169200bb5f8fcd11cfb'
    }
  })
  data: any
}

class PackagePaginate extends PaginateResponseDto<any> {
  @ApiProperty({
    example: [
      {
        created_by: {
          username: 'superadmin',
          id: '6318d06a1fa78236d380db83'
        },
        updated_by: {
          username: 'superadmin',
          id: '6318d06a1fa78236d380db83'
        },
        status: 'active',
        package_type: ['IPAD'],
        i_pad_capacity: 1,
        is_active_default: true,
        is_free: true,
        daily_price: 0,
        yearly_price: 0,
        monthly_price: 0,
        description:
          'ฟรีค่าบริการรายเดือนสำหรับ iPad เครื่องแรก ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)',
        name: 'iPad',
        updated_at: '2022-12-20T07:56:47.996Z',
        created_at: '2022-12-02T10:20:25.976Z',
        id: '6389d169200bb5f8fcd11cfb'
      }
    ]
  })
  results: any
}

export class PackagePaginateResponse extends ResponseDto<PackagePaginate> {
  @ApiProperty()
  data: PackagePaginate
}

export class GetAllPackageResponse extends ResponseDto<any> {
  @ApiProperty({
    example: [
      {
        daily_price: 0.7,
        yearly_price: 200,
        monthly_price: 20,
        description:
          'เพิ่ม iPad 1 เครื่อง ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)',
        name: 'More iPad',
        i_pad_capacity: 1,
        is_active_default: false,
        is_free: false,
        package_type: ['IPAD'],
        id: '6389d131200bb5f8fcd11cf9'
      },
      {
        daily_price: 0,
        yearly_price: 0,
        monthly_price: 0,
        description:
          'ฟรีค่าบริการรายเดือนสำหรับ iPad เครื่องแรก ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)',
        name: 'iPad',
        is_free: true,
        i_pad_capacity: 1,
        is_active_default: true,
        package_type: ['IPAD'],
        id: '6389d169200bb5f8fcd11cfb'
      }
    ]
  })
  data: any
}
