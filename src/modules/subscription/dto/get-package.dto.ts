import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'

export class PackagePaginate extends PaginateDto {
  @IsString()
  readonly sortBy: string = 'createdAt'

  @IsString()
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Tim' })
  readonly search: string = ''

  public buildQuery() {
    const result = {
      $or: [
        {
          name: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
