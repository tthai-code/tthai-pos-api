import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsNumber, IsString } from 'class-validator'
import { SubscriptionPlanEnum } from '../common/subscription-plan.enum'

export class EditSubscriptionDto {
  public price: number
  public amount: number

  @IsNumber()
  @ApiProperty({ example: 1 })
  quantity: number

  @IsString()
  @IsEnum(SubscriptionPlanEnum)
  @ApiProperty({
    example: SubscriptionPlanEnum.MONTHLY,
    enum: Object.values(SubscriptionPlanEnum)
  })
  public paymentPlan: string
}
