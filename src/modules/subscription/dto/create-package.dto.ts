import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import {
  IsArray,
  IsBoolean,
  IsIn,
  IsNumber,
  IsOptional,
  IsString
} from 'class-validator'
import { PackageTypeEnum } from '../common/package-type.enum'

export class CreatePackageDto {
  public isFree: boolean

  @IsString()
  @ApiProperty({ example: 'More iPad' })
  readonly name: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example:
      'เพิ่ม iPad 1 เครื่อง ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)'
  })
  readonly description: string

  @IsNumber()
  @ApiProperty({ example: 20 })
  readonly monthlyPrice: number

  @IsNumber()
  @ApiProperty({ example: 200 })
  readonly yearlyPrice: number

  @IsBoolean()
  @ApiProperty({ example: false })
  readonly isActiveDefault: boolean

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({
    example: 1,
    description: 'required when packageType has `IPAD`'
  })
  readonly iPadCapacity: number

  @IsArray()
  @IsIn(Object.values(PackageTypeEnum), { each: true })
  @IsString({ each: true })
  @ApiProperty({
    example: [PackageTypeEnum.IPAD],
    description: `each value must be one in \`${Object.values(
      PackageTypeEnum
    )}\``
  })
  readonly packageType: string[]
}
