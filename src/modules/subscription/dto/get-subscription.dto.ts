import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'

export class SubscriptionPaginateDto extends PaginateDto {
  @IsString()
  readonly sortBy: string = 'createdAt'

  @IsString()
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'iPad' })
  readonly search: string = ''

  public buildQuery(restaurantId: string) {
    const result = {
      $or: [
        {
          name: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      restaurantId,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
