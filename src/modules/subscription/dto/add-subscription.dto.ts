import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsNumber, IsString } from 'class-validator'
import { SubscriptionPlanEnum } from '../common/subscription-plan.enum'

export class AddSubscriptionDto {
  @IsString()
  @ApiProperty({ example: '<package-id>' })
  packageId: string

  @IsNumber()
  @ApiProperty({ example: 1 })
  quantity: number

  @IsString()
  @IsEnum(SubscriptionPlanEnum)
  @ApiProperty({
    example: SubscriptionPlanEnum.MONTHLY,
    enum: Object.values(SubscriptionPlanEnum)
  })
  public paymentPlan: string
}
