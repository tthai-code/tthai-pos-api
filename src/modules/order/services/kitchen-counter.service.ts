import { InjectModel } from '@nestjs/mongoose'
import { Injectable } from '@nestjs/common'
import { Model } from 'mongoose'
import { KitchenCounterDocument } from '../schemas/kitchen-counter.shcema'

@Injectable()
export class KitchenCounterService {
  constructor(
    @InjectModel('kitchen-counter')
    private readonly kitchenCounterModel: Model<KitchenCounterDocument>
  ) {}

  async getCounter(prefix: string, restaurantId: string) {
    const result = await this.kitchenCounterModel.findOneAndUpdate(
      { prefix, restaurantId },
      { $inc: { counter: 1 } },
      { new: true, upsert: true }
    )

    return result.counter
  }
}
