import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ThirdPartyOrderDocument } from '../schemas/third-party-order.schema'
import { ThirdPartyOrderPaginateDto } from '../dto/get-third-party-order.dto'

@Injectable({ scope: Scope.REQUEST })
export class ThirdPartyOrderService {
  constructor(
    @InjectModel('thirdPartyOrder')
    private readonly OrderModel: PaginateModel<ThirdPartyOrderDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<ThirdPartyOrderDocument | any> {
    return this.OrderModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.OrderModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.OrderModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<ThirdPartyOrderDocument> {
    return this.OrderModel
  }

  async getSession(): Promise<ClientSession> {
    await this.OrderModel.createCollection()

    return await this.OrderModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<ThirdPartyOrderDocument> {
    const document = new this.OrderModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.OrderModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<ThirdPartyOrderDocument> {
    const document = new this.OrderModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<ThirdPartyOrderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(
    condition: any,
    payload: any
  ): Promise<ThirdPartyOrderDocument> {
    return this.OrderModel.updateOne(condition, { ...payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<ThirdPartyOrderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async delete(id: string): Promise<ThirdPartyOrderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.OrderModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.OrderModel.findById(id)
  }

  findOne(condition, project?: any): Promise<ThirdPartyOrderDocument> {
    return this.OrderModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: ThirdPartyOrderPaginateDto,
    select?: any
  ): Promise<PaginateResult<ThirdPartyOrderDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.OrderModel.paginate(query, options)
  }
}
