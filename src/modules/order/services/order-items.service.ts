import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { OrderItemsDocument } from '../schemas/order-items.schema'

@Injectable({ scope: Scope.REQUEST })
export class OrderItemsService {
  constructor(
    @InjectModel('orderItems')
    private readonly OrderItemsModel: PaginateModel<OrderItemsDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<OrderItemsDocument | any> {
    return this.OrderItemsModel.findById({ _id: id })
  }

  async resolveByCondition(condition: any): Promise<OrderItemsDocument | any> {
    return this.OrderItemsModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.OrderItemsModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<OrderItemsDocument> {
    return this.OrderItemsModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.OrderItemsModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.OrderItemsModel.createCollection()

    return this.OrderItemsModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<OrderItemsDocument> {
    const document = new this.OrderItemsModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionCreateMany(
    payload: any,
    session: ClientSession
  ): Promise<Array<OrderItemsDocument> | any> {
    return this.OrderItemsModel.insertMany(payload, {
      session,
      ordered: true,
      lean: true
    })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<OrderItemsDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateCondition(
    condition: any,
    product: any,
    session: ClientSession
  ): Promise<OrderItemsDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<OrderItemsDocument> {
    return this.OrderItemsModel.updateMany(
      condition,
      { $set: payload },
      { session, new: true }
    )
  }

  async create(payload: any): Promise<OrderItemsDocument> {
    const document = new this.OrderItemsModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async createMany(payload: any): Promise<OrderItemsDocument> {
    return this.OrderItemsModel.insertMany(payload)
  }

  async update(id: string, payload: any): Promise<OrderItemsDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async updateMany(condition: any, payload: any): Promise<OrderItemsDocument> {
    return this.OrderItemsModel.updateMany(condition, { $set: payload })
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<OrderItemsDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.OrderItemsModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.OrderItemsModel.findById(id)
  }

  findOne(condition: any): Promise<OrderItemsDocument> {
    return this.OrderItemsModel.findOne(condition)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<OrderItemsDocument> {
    return this.OrderItemsModel.findOne(condition, options)
  }

  paginate(
    query: any,
    queryParam: any,
    select?: any
  ): Promise<PaginateResult<OrderItemsDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.OrderItemsModel.paginate(query, options)
  }
}
