import { InjectModel } from '@nestjs/mongoose'
import { Injectable } from '@nestjs/common'
import { Model } from 'mongoose'
import { OrderCounterDocument } from '../schemas/order-counter.schema'

@Injectable()
export class OrderCounterService {
  constructor(
    @InjectModel('order-counter')
    private readonly orderCounterModel: Model<OrderCounterDocument>
  ) {}

  async getCounter(prefix: string, restaurantId: string) {
    const result = await this.orderCounterModel.findOneAndUpdate(
      { prefix, restaurantId },
      { $inc: { counter: 1 } },
      { new: true, upsert: true }
    )

    return result.counter
  }
}
