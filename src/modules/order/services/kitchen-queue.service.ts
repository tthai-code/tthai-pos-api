import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { KitchenQueueDocument } from '../schemas/kitchen-queue.schema'
import { PaginateKitchenQueue } from '../dto/get-kitchen.dto'

@Injectable({ scope: Scope.REQUEST })
export class KitchenQueueService {
  constructor(
    @InjectModel('kitchenQueue')
    private readonly KitchenQueueModel:
      | PaginateModel<KitchenQueueDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<KitchenQueueDocument | any> {
    return this.KitchenQueueModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.KitchenQueueModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.KitchenQueueModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<KitchenQueueDocument> {
    return this.KitchenQueueModel
  }

  async getSession(): Promise<ClientSession> {
    await this.KitchenQueueModel.createCollection()

    return await this.KitchenQueueModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<KitchenQueueDocument> {
    const document = new this.KitchenQueueModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.KitchenQueueModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<KitchenQueueDocument> {
    const document = new this.KitchenQueueModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<KitchenQueueDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<KitchenQueueDocument> {
    return this.KitchenQueueModel.updateOne(
      condition,
      { $set: payload },
      { new: true }
    )
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<KitchenQueueDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<KitchenQueueDocument> {
    return this.KitchenQueueModel.updateMany(
      condition,
      { $set: payload },
      { session, new: true }
    )
  }

  async delete(id: string): Promise<KitchenQueueDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.KitchenQueueModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.KitchenQueueModel.findById(id)
  }

  findOne(
    condition,
    project?: any,
    options?: any
  ): Promise<KitchenQueueDocument> {
    return this.KitchenQueueModel.findOne(condition, project, options)
  }

  paginate(
    query: any,
    queryParam: PaginateKitchenQueue,
    select?: any
  ): Promise<PaginateResult<KitchenQueueDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.KitchenQueueModel.paginate(query, options)
  }
}
