import { InjectModel } from '@nestjs/mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'

import { Inject, Injectable, Scope } from '@nestjs/common'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { OrderOnlineDocument } from '../schemas/order-online.schema'
import {
  OrderOnlineCustomerPaginateDto,
  OrderOnlinePaginateDto
} from '../dto/get-order-online.dto'

@Injectable({ scope: Scope.REQUEST })
export class OrderOnlineService {
  constructor(
    @InjectModel('orderOnline')
    private readonly OrderOnlineModel: PaginateModel<OrderOnlineDocument> | any,

    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<OrderOnlineDocument | any> {
    return this.OrderOnlineModel.findById({ _id: id })
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<OrderOnlineDocument> {
    const document = new this.OrderOnlineModel(payload)
    document?.setAuthor(this.request)
    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<OrderOnlineDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)
    return document.set({ ...product }).save({ session })
  }

  async update(id: string, product: any): Promise<OrderOnlineDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.OrderOnlineModel.find(condition, project)
  }

  findOne(condition, project?: any): Promise<OrderOnlineDocument> {
    return this.OrderOnlineModel.findOne(condition, project)
  }
  paginate(
    query: any,
    queryParam: OrderOnlinePaginateDto | OrderOnlineCustomerPaginateDto,
    select?: any
  ): Promise<PaginateResult<OrderOnlineDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.OrderOnlineModel.paginate(query, options)
  }
}
