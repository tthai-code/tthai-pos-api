import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { OrderDocument } from '../schemas/order.schema'
import { ToGoOrderPaginateDto } from '../dto/get-order.dto'

@Injectable({ scope: Scope.REQUEST })
export class OrderService {
  constructor(
    @InjectModel('order')
    private readonly OrderModel: PaginateModel<OrderDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<OrderDocument | any> {
    return this.OrderModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.OrderModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.OrderModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<OrderDocument> {
    return this.OrderModel
  }

  async getSession(): Promise<ClientSession> {
    await this.OrderModel.createCollection()

    return await this.OrderModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<OrderDocument> {
    const document = new this.OrderModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.OrderModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<OrderDocument> {
    const document = new this.OrderModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<OrderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<OrderDocument> {
    return this.OrderModel.updateOne(condition, { ...payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<OrderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async delete(id: string): Promise<OrderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.OrderModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.OrderModel.findById(id)
  }

  findOne(condition, project?: any): Promise<OrderDocument> {
    return this.OrderModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: ToGoOrderPaginateDto
  ): Promise<PaginateResult<OrderDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder }
    }

    return this.OrderModel.paginate(query, options)
  }
}
