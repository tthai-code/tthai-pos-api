export interface IRestaurantAddressObjectPayload {
  address?: string
  city?: string
  state?: string
  zipCode?: string
}

export interface IStaffObjectOrderPayload {
  id?: string
  firstName?: string
  lastName?: string
}

export interface IRestaurantObjectOrderPayload {
  id?: string
  legalBusinessName?: string
  address?: IRestaurantAddressObjectPayload
  businessPhone?: string
  email?: string
}

export interface ICustomerObjectOrderPayload {
  id?: string
  firstName?: string
  lastName?: string
  tel?: string
}

export interface IModifierObjectOrderPayload {
  id?: string
  label?: string
  abbreviation?: string
  name?: string
  nativeName?: string
  price?: number
}

export interface IMenuItemsObjectPayload {
  id?: string
  name?: string
  nativeName?: string
  price?: number
  modifiers?: IModifierObjectOrderPayload[]
  isContainAlcohol?: boolean
  coverImage?: string
  unit?: number
  amount?: number
  note?: string
}

export interface IKitchenHubToOrderPayload {
  _id?: string
  restaurant?: IRestaurantObjectOrderPayload
  staff?: IStaffObjectOrderPayload
  customer?: ICustomerObjectOrderPayload
  orderType?: string
  orderMethod?: string
  orderDate?: Date
  pickUpDate?: Date
  asap?: boolean
  scheduledForDate?: Date
  summaryItems?: IMenuItemsObjectPayload[]
  subtotal?: number
  discount?: number
  serviceCharge?: number
  tax?: number
  alcoholTax?: number
  convenienceFee?: number
  tips?: number
  total?: number
  orderStatus?: string
  deliverySource?: string
  orderKHId?: number
  note?: string
  prepareTime?: number
}
