export enum OrderTypeEnum {
  DINE_IN = 'DINE_IN',
  TO_GO = 'TO_GO',
  ONLINE = 'ONLINE',
  TTHAI_APP = 'TTHAI_APP',
  THIRD_PARTY = 'THIRD_PARTY'
}

export enum OrderMethodEnum {
  DINE_IN = 'DINE_IN',
  DELIVERY = 'DELIVERY',
  PICKUP = 'PICKUP',
  CURBSIDE = 'CURBSIDE'
}
