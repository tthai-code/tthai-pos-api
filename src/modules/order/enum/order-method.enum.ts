export enum OrderMethod {
  DELIVERY = 'DELIVERY',
  PICKUP = 'PICKUP'
}
