export enum OnlinePaymentStatusEnum {
  PAID = 'PAID',
  UNPAID = 'UNPAID'
}
