import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import dayjs from 'src/plugins/dayjs'

import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CustomerService } from 'src/modules/customer/services/customer.service'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { StaffService } from 'src/modules/staff/services/staff.service'
import { CreateOrderOnline } from '../dto/create-order-pos.dto'

import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderTypeEnum } from '../enum/order-type.enum'
import { OnlinePaymentStatusEnum } from '../enum/payment-status.enum'

import { KitchenCounterService } from '../services/kitchen-counter.service'
import { KitchenQueueService } from '../services/kitchen-queue.service'
import { OrderCounterService } from '../services/order-counter.service'
import { OrderItemsService } from '../services/order-items.service'
import { OrderService } from '../services/order.service'
import { OrderOnlineService } from '../services/order-online.service'
import { SocketGateway } from 'src/modules/socket/socket.gateway'

import { ItemStatusEnum } from '../enum/item-status.enum'
import { OrderOnlineCustomerPaginateDto } from '../dto/get-order-online.dto'
import { TransactionLogic } from 'src/modules/transaction/logics/transaction.logic'
import { CardPointeLogic } from 'src/modules/payment-gateway/logics/card-pointe.logic'
import { IOnlinePayment } from 'src/modules/payment-gateway/interfaces/card-pointe.interface'
import { PayOnlineOrderDto } from '../dto/pay-online-order.dto'
import { IPaymentMethodCardType } from 'src/modules/transaction/interfaces/online-transaction'
import { POSMailerService } from 'src/modules/mailer/services/mailer.service'
import { OrderOnlineDocument } from '../schemas/order-online.schema'
import { TransactionDocument } from 'src/modules/transaction/schemas/transaction.schema'
import { WebSettingService } from 'src/modules/web-setting/services/web-setting.service'
import { TransactionService } from 'src/modules/transaction/services/transaction.service'
// import { CashDrawerActionEnum } from 'src/modules/cash-drawer/common/cash-drawer.enum'
import { POSCashDrawerLogic } from 'src/modules/cash-drawer/logic/pos-cash-drawer.logic'
import { OrderHourLogic } from './order-hour.logic'
import { PaymentGatewayService } from 'src/modules/payment-gateway/services/payment-gateway.service'

@Injectable()
export class OrderOnlineLogic {
  constructor(
    private readonly orderService: OrderService,
    private readonly orderOnlineService: OrderOnlineService,
    private readonly orderCounterService: OrderCounterService,
    private readonly restaurantService: RestaurantService,
    private readonly staffService: StaffService,
    private readonly kitchenQueueService: KitchenQueueService,
    private readonly kitchenCounterService: KitchenCounterService,
    private readonly orderItemsService: OrderItemsService,
    private readonly customerService: CustomerService,
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly socketGateway: SocketGateway,
    private readonly cardPointeLogic: CardPointeLogic,
    private readonly transactionLogic: TransactionLogic,
    private readonly transactionService: TransactionService,
    private readonly mailerService: POSMailerService,
    private readonly webSettingService: WebSettingService,
    private readonly posCashDrawerLogic: POSCashDrawerLogic,
    private readonly orderHourLogic: OrderHourLogic
  ) {}

  private randomOrderId() {
    const message =
      'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789'
    let id = ''
    for (let i = 0; i < 8; i++) {
      id += message.charAt(Math.floor(Math.random() * message.length))
    }
    return id
  }

  private async genOrderId(): Promise<string> {
    let id = ''
    while (true) {
      id = this.randomOrderId()
      const isDuplicated = await this.orderService.findOne({
        _id: id
      })
      if (!isDuplicated) {
        break
      }
    }

    return id
  }

  private async queueKitchen(restaurantId: string): Promise<string> {
    const { timeZone } = await this.restaurantService.findOne({
      _id: restaurantId
    })
    const now = dayjs().tz(timeZone)
    const nowUnix = now.valueOf()
    const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf()
    const endOfDay = now
      .add(1, 'day')
      .hour(3)
      .minute(59)
      .millisecond(999)
      .valueOf()
    let nowFormat = now.subtract(1, 'day').format('YYYYMMDD')

    if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
      nowFormat = now.format('YYYYMMDD')
    }

    const countQueue = await this.kitchenCounterService.getCounter(
      nowFormat,
      restaurantId
    )
    return `${countQueue}`
  }

  async createOrderOnline(customerId: string, createOrder: CreateOrderOnline) {
    const logic = async (session: ClientSession) => {
      const [restaurant] = await Promise.all([
        this.restaurantService.findOneForOrder(createOrder.restaurantId)
      ])

      if (!restaurant) throw new NotFoundException('Restaurant not found.')

      const checkOpenHour = await this.orderHourLogic.checkAvailableOrder(
        restaurant
      )

      if (!checkOpenHour) {
        throw new BadRequestException(
          'Now, We are closed. Apologize for any inconvenience.'
        )
      }

      let customer
      const customerData = await this.customerService.findOne(
        {
          _id: customerId || createOrder.customerId,
          status: StatusEnum.ACTIVE
        },
        { firstName: 1, lastName: 1, tel: 1, email: 1 }
      )
      if (!customerData && !createOrder.customer) {
        customer = {
          id: null,
          firstName: 'Guest',
          lastName: '',
          tel: '',
          email: ''
        }
      }
      if (!!createOrder.customer) {
        customer = createOrder.customer
      } else {
        customer = customerData.toObject()
      }

      const paymentGatewayData = await this.paymentGatewayService.findOne({
        restaurantId: createOrder.restaurantId
      })
      createOrder._id = await this.genOrderId()
      createOrder.orderType = OrderTypeEnum.ONLINE
      createOrder.orderStatus = OrderStateEnum.NEW
      createOrder.orderDate = new Date()
      createOrder.restaurant = restaurant
      createOrder.customer = customer

      if (!!paymentGatewayData && paymentGatewayData.isConvenienceFee) {
        createOrder.convenienceFee = paymentGatewayData.convenienceFee
      }

      if (!!paymentGatewayData && paymentGatewayData.isServiceCharge) {
        createOrder.serviceCharge =
          (Number(createOrder.subtotal) || 0) *
          ((Number(paymentGatewayData.serviceCharge) || 0) / 100)
      } else {
        createOrder.serviceCharge = 0
      }

      createOrder.total =
        (Number(createOrder.subtotal) || 0) +
        (Number(createOrder.tax) || 0) +
        (Number(createOrder.convenienceFee) || 0) +
        (Number(createOrder.serviceCharge) || 0)

      const rawResult = await this.orderOnlineService.transactionCreate(
        {
          ...createOrder,
          summaryItems: createOrder.items
        },
        session
      )
      const result = rawResult.toObject()

      return {
        id: result.id,
        orderStatus: result.orderStatus,
        paymentStatus: result.paymentStatus
      }
    }
    return runTransaction(logic)
  }

  async updatePaidOrderOnlineAndCreateOrderById(
    orderId: string,
    payload: PayOnlineOrderDto
  ) {
    const logic = async (session: ClientSession) => {
      const order = await this.orderOnlineService.findOne({
        _id: orderId,
        status: StatusEnum.ACTIVE
      })

      if (!order) {
        throw new NotFoundException('Not found order.')
      }
      if (order.paymentStatus === OnlinePaymentStatusEnum.PAID) {
        throw new BadRequestException('This order is already paid.')
      }
      const {
        restaurant: { id: restaurantId }
      } = order

      const restaurant = await this.restaurantService.findOne({
        _id: restaurantId,
        status: StatusEnum.ACTIVE
      })
      const checkOpenHour = await this.orderHourLogic.checkAvailableOrder(
        restaurant
      )

      if (!checkOpenHour) {
        throw new BadRequestException(
          'Now, We are closed. Apologize for any inconvenience.'
        )
      }

      const amount = Number(order.total) + Number(payload.tips)
      const paymentPayload: IOnlinePayment = {
        account: payload.account,
        expiry: `${payload.expirationMonth}${payload.expirationYear}`,
        amount: amount.toFixed(2),
        orderid: order.id,
        name: payload.cardholderName
      }

      const paymentTransaction =
        await this.cardPointeLogic.payOnlineTransaction(
          restaurantId,
          paymentPayload
        )
      const { binInfo } = paymentTransaction
      const { cardType, paymentMethod } =
        this.cardPointeLogic.identifyPaymentMethod(
          binInfo.product,
          binInfo.cardusestring
        )
      // update table ordersOnline
      const updated = await this.orderOnlineService.transactionUpdate(
        orderId,
        {
          total: amount,
          tips: payload.tips,
          paymentStatus: OnlinePaymentStatusEnum.PAID
        },
        session
      )
      const paymentObject: IPaymentMethodCardType = {
        retRef: paymentTransaction.retref,
        batchId: paymentTransaction.batchid,
        cardType,
        paymentMethod,
        cardLastFour: payload.account.slice(-4),
        paidBy: payload.cardholderName,
        response: paymentTransaction
      }
      const createdTransaction =
        await this.transactionLogic.createOnlineTransaction(
          updated,
          paymentObject,
          session
        )

      const orderObj = order.toObject()
      const itemPayload = orderObj.summaryItems.map((item) => {
        const transform = {
          ...item,
          orderId: orderId
        }
        return transform
      })

      // insert orderItem
      const itemsData = await this.orderItemsService.transactionCreateMany(
        itemPayload,
        session
      )

      const ticketNo = await this.queueKitchen(orderObj.restaurant.id)
      await this.kitchenQueueService.transactionCreate(
        {
          restaurantId: orderObj.restaurant.id,
          orderId: orderId,
          ticketNo,
          items: itemsData.map((item) => item.id)
        },
        session
      )

      // sent socket
      const notifyObject = {
        id: updated.id,
        customer: updated.customer,
        type: updated.orderMethod,
        placedDate: updated.orderDate,
        source: updated.orderType,
        orderStatus: updated.orderStatus
      }
      this.socketGateway.server
        .to(`pos-${order.restaurant.id}`)
        .emit('receiveOnlineOrder', notifyObject)

      await this.sendOnlineOrderReceipt(
        order,
        ticketNo,
        createdTransaction,
        payload.timeZone,
        payload.offsetTimeZone
      )

      return { updated, transaction: createdTransaction }
    }
    await runTransaction(logic)
    // const { updated, transaction } = await runTransaction(logic)
    // await this.posCashDrawerLogic.cashSales(updated.restaurant.id, {
    //   date: transaction.openDate,
    //   amount: transaction.totalPaid,
    //   action: CashDrawerActionEnum.CASH_SALES,
    //   user:
    //     transaction?.createdBy?.username === 'system'
    //       ? 'Online'
    //       : transaction?.createdBy?.username,
    //   comment: `Order# ${orderId}`
    // })
    return this.orderOnlineService.findOne({ _id: orderId })
  }

  async getOrderOnlineById(orderId: string) {
    const order = await this.orderOnlineService.findOne(
      {
        _id: orderId,
        status: StatusEnum.ACTIVE
      },
      {
        // summaryItems: 0,
        table: 0,
        createdAt: 0,
        updatedAt: 0,
        status: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    if (!order) throw new NotFoundException('Not found order.')
    const items = await this.orderItemsService.getAll({ orderId: order.id })
    const result = {
      ...order.toObject(),
      // items,
      items: items.length > 0 ? items : order.summaryItems,
      customer: {
        id: order.customer?.id || null,
        firstName: order.customer?.firstName,
        lastName: order.customer?.lastName,
        tel: order.customer?.tel
      }
    }
    delete result.summaryItems
    return result
  }

  async getOrderOnlineAll(
    customerId: string,
    query: OrderOnlineCustomerPaginateDto
  ) {
    const order = await this.orderOnlineService.paginate(
      query.buildQuery(customerId),
      query,
      {
        table: 0,
        createdAt: 0,
        updatedAt: 0,
        status: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )

    const { docs } = order
    const transformPayload = []
    for (const doc of docs) {
      const kitchenQueue = await this.kitchenQueueService.findOne({
        orderId: doc.id
      })
      transformPayload.push({
        ...doc?.toObject(),
        ticketNo: kitchenQueue?.ticketNo || null
      })
    }

    return { ...order, docs: transformPayload }
  }

  async acceptOrder(orderId: string) {
    const order = await this.orderOnlineService.findOne({
      _id: orderId,
      status: StatusEnum.ACTIVE
    })
    if (!order) throw new NotFoundException('Not found order.')
    if (order.orderStatus !== OrderStateEnum.NEW) {
      throw new BadRequestException(
        `Order status isn't new order, now order status is ${order.orderStatus}`
      )
    }

    const logic = async (session: ClientSession) => {
      const updated = await this.orderOnlineService.transactionUpdate(
        orderId,
        {
          orderStatus: OrderStateEnum.IN_PROGRESS
        },
        session
      )
      if (!updated) throw new BadRequestException()

      const orderObj = updated.toObject()

      // sent socket
      const notifyObject = {
        id: orderObj.id,
        customer: orderObj.customer,
        type: orderObj.orderMethod,
        placedDate: orderObj.orderDate,
        source: orderObj.orderType,
        orderStatus: orderObj.orderStatus
      }
      this.socketGateway.server
        .to(`web-${orderObj.customer.id}`)
        .emit('receiveOrderWeb', notifyObject)

      return await orderObj
    }

    await runTransaction(logic)
    return { success: true }
  }

  async rejectOrder(orderId: string) {
    const order = await this.orderOnlineService.findOne({
      _id: orderId,
      status: StatusEnum.ACTIVE
    })
    if (!order) throw new NotFoundException('Not found order.')

    await this.orderOnlineService.update(orderId, {
      orderStatus: OrderStateEnum.VOID
    })
    await this.kitchenQueueService.updateOne(
      { orderId },
      {
        queueStatus: ItemStatusEnum.CANCELED
      }
    )
    await this.orderItemsService.updateMany(
      { orderId },
      { itemStatus: ItemStatusEnum.CANCELED }
    )
    return { success: true }
  }

  async completeOrder(orderId: string) {
    const order = await this.orderOnlineService.findOne({
      _id: orderId,
      status: StatusEnum.ACTIVE
    })
    if (!order) throw new NotFoundException('Not found order.')

    await this.orderOnlineService.update(orderId, {
      orderStatus: OrderStateEnum.COMPLETED
    })
    await this.kitchenQueueService.updateOne(
      { orderId },
      {
        queueStatus: ItemStatusEnum.COMPLETED
      }
    )
    await this.orderItemsService.updateMany(
      { orderId },
      { itemStatus: ItemStatusEnum.COMPLETED }
    )
    return { success: true }
  }

  async getOrderOnlineByIdForPOS(orderId: string) {
    const order = await this.orderOnlineService.findOne(
      {
        _id: orderId,
        status: StatusEnum.ACTIVE
      },
      {
        // summaryItems: 0,
        table: 0,
        createdAt: 0,
        updatedAt: 0,
        status: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    if (!order) throw new NotFoundException('Not found order.')
    const items = await this.orderItemsService.getAll({ orderId: order.id })
    const kitchenQueue = await this.kitchenQueueService.findOne({ orderId })
    const result = {
      ...order.toObject(),
      // items,
      items: items.length > 0 ? items : order.summaryItems,
      customer: {
        id: order.customer?.id || null,
        firstName: order.customer?.firstName,
        lastName: order.customer?.lastName,
        tel: order.customer?.tel,
        email: order.customer?.email
      },
      ticketNo: kitchenQueue?.ticketNo ?? null
    }
    delete result.summaryItems
    return result
  }

  private formatPhoneNumber(phoneNumberString) {
    const cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
    if (match) {
      return '(' + match[1] + ') ' + match[2] + '-' + match[3]
    }
    return null
  }

  private currencyFormat(number: number) {
    return number.toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD'
    })
  }

  async sendOnlineOrderReceipt(
    order: OrderOnlineDocument,
    ticketNo: string,
    transaction: TransactionDocument,
    timeZone: string,
    offsetTimeZone: number
  ) {
    const webSetting = await this.webSettingService.findOne({
      restaurantId: order?.restaurant?.id
    })
    const { logoUrl } = webSetting
    const {
      restaurant: {
        dba,
        legalBusinessName,
        address: { address, city, state, zipCode },
        businessPhone
      },
      orderDate,
      orderType,
      id,
      summaryItems
    } = order
    const itemsQuantity = summaryItems.reduce(
      (prev, item) => item.unit + prev,
      0
    )
    const itemList = summaryItems.map((item) => ({
      label: `${item.unit} x ${item.name}`,
      amount: this.currencyFormat(item.amount)
    }))
    const summaryKey = [
      { key: 'discount', label: 'Discount' },
      { key: 'subtotal', label: 'Subtotal' },
      { key: 'tax', label: 'Sales Tax' },
      { key: 'alcoholTax', label: 'Alcohol Tax' },
      { key: 'serviceCharge', label: 'Service Charge' }
    ]
    const summaryList = summaryKey.map((item) => ({
      label: item.label,
      amount: this.currencyFormat(order[item.key])
    }))
    const orderDateTimeZone = dayjs(orderDate).tz(timeZone)
    const transactionOpenDateTimeZone = dayjs(transaction.openDate).tz(timeZone)
    const utc = this.getUTC(offsetTimeZone)

    const mailBody = {
      logoUrl: logoUrl || process.env.TTHAI_LOGO_URL,
      dba: dba || legalBusinessName,
      total: this.currencyFormat(order.total + transaction.tips),
      orderDate: orderDateTimeZone.format('MMMM D, YYYY'),
      orderTime: `${orderDateTimeZone.format('hh:mmA')} ${utc}`,
      orderType: `Type: ${orderType}`,
      orderId: `Order#${id}`,
      transactionId: `TXN ${transaction.txn}`,
      itemsQuantity: `${itemsQuantity} ${
        itemsQuantity > 1 ? 'Items' : 'Item'
      }:`,
      ticketNo: `Ticket#${ticketNo}`,
      itemList,
      summaryList,
      tips: {
        label: 'Tips',
        amount: this.currencyFormat(transaction.tips)
      },
      addressDetail: [
        dba || legalBusinessName,
        address,
        `${city}, ${state} ${zipCode}`,
        this.formatPhoneNumber(businessPhone)
      ],
      paymentDetail: {
        paidBy: transaction.paidBy,
        cardType: transaction.cardType,
        cardLastFour: transaction.cardLastFour,
        date: `${transactionOpenDateTimeZone.format('M/DD/YY, h:mm A')} ${utc}`,
        authCode: `#${transaction?.response['authcode']}`
      }
    }
    const payloadHTML = this.mailerService.transformToHTML(mailBody)
    return await this.mailerService.postMail({
      to: order.customer.email,
      from: `${dba} <${process.env.EMAIL_USER}>`,
      subject: `"Your Food Order Ticket Number ${ticketNo} from ${dba}"`,
      text: payloadHTML,
      html: payloadHTML
    })
  }

  private getUTC(offset: number) {
    const utc = (offset / 60) * -1
    if (utc >= 0) {
      return `(UTC +${utc.toString().padStart(2, '0')}:00)`
    }
    return `(UTC -${utc.toString().replace('-', '').padStart(2, '0')}:00)`
  }
}
