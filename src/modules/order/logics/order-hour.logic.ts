import { Injectable } from '@nestjs/common'
import * as dayjs from 'dayjs'
import * as utc from 'dayjs/plugin/utc'
import * as timezone from 'dayjs/plugin/timezone'
import { RestaurantHolidaysService } from 'src/modules/restaurant-holiday/services/restaurant-holiday.service'
import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service'
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service'
import { RestaurantDocument } from 'src/modules/restaurant/schemas/restaurant.schema'
import { GetDayStringByDay } from 'src/utilities/dateTime'
import { StatusEnum } from 'src/common/enum/status.enum'

dayjs.extend(utc)
dayjs.extend(timezone)

@Injectable()
export class OrderHourLogic {
  constructor(
    private readonly restaurantPeriodService: RestaurantPeriodService,
    private readonly restaurantDayService: RestaurantDayService,
    private readonly restaurantHolidaysService: RestaurantHolidaysService
  ) {}

  async checkAvailableOrder(restaurant: RestaurantDocument) {
    const { id: restaurantId, timeZone } = restaurant
    const currentDay = dayjs().tz(timeZone)
    const dayOfWeekString = await GetDayStringByDay(currentDay.day())

    const restaurantDay = await this.restaurantDayService.findOne({
      restaurantId,
      status: StatusEnum.ACTIVE,
      label: dayOfWeekString
    })

    if (restaurantDay.isClosed) {
      return false
    }

    const restaurantPeriods = await this.restaurantPeriodService.getAll({
      restaurantId,
      status: StatusEnum.ACTIVE,
      label: dayOfWeekString
    })

    for (const item of restaurantPeriods) {
      const [openHour, openMinute] = item?.opensAt?.split(':')
      const [closeHour, closeMinute] = item?.closesAt?.split(':')
      const opensAt = currentDay
        .hour(+(openHour - 1))
        .minute(+openMinute)
        .second(0)
        .millisecond(0)
        .valueOf()
      const closesAt = currentDay
        .hour(+(closeHour - 1))
        .minute(+closeMinute)
        .second(0)
        .millisecond(0)
        .valueOf()
      const now = currentDay.valueOf()
      if (now > opensAt && now < closesAt) {
        return true
      }
    }
    return false
  }
}
