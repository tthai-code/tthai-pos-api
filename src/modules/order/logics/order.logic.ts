import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import dayjs from 'src/plugins/dayjs'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CustomerService } from 'src/modules/customer/services/customer.service'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { StaffService } from 'src/modules/staff/services/staff.service'
import { TableStatusEnum } from 'src/modules/table/common/table-status.enum'
import { TableService } from 'src/modules/table/services/table.service'
import { VoidTransactionLogic } from 'src/modules/transaction/logics/void-transaction.logic'
import {
  CreateOrderDineInPOSDto,
  CreateOrderToGoPOSDto
} from '../dto/create-order-pos.dto'
import { ToGoOrderPaginateDto } from '../dto/get-order.dto'
import { MoveOrderTableDto } from '../dto/move-order-table.dto'
import { UpdateOrderItemsDto } from '../dto/update-items.dto'
import { ItemStatusEnum } from '../enum/item-status.enum'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderTypeEnum } from '../enum/order-type.enum'
import { KitchenCounterService } from '../services/kitchen-counter.service'
import { KitchenQueueService } from '../services/kitchen-queue.service'
import { OrderCounterService } from '../services/order-counter.service'
import { OrderItemsService } from '../services/order-items.service'
import { OrderService } from '../services/order.service'
import { OrderItemsLogic } from './order-items.logic'

@Injectable()
export class OrderLogic {
  constructor(
    private readonly orderService: OrderService,
    private readonly orderCounterService: OrderCounterService,
    private readonly restaurantService: RestaurantService,
    private readonly staffService: StaffService,
    private readonly tableService: TableService,
    private readonly kitchenQueueService: KitchenQueueService,
    private readonly kitchenCounterService: KitchenCounterService,
    private readonly orderItemsService: OrderItemsService,
    private readonly customerService: CustomerService,
    private readonly voidTransactionLogic: VoidTransactionLogic,
    private readonly orderItemsLogic: OrderItemsLogic
  ) {}

  private randomOrderId() {
    const message =
      'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789'
    let id = ''
    for (let i = 0; i < 8; i++) {
      id += message.charAt(Math.floor(Math.random() * message.length))
    }
    return id
  }

  private async genOrderId(): Promise<string> {
    let id = ''
    while (true) {
      id = this.randomOrderId()
      const isDuplicated = await this.orderService.findOne({
        _id: id
      })
      if (!isDuplicated) {
        break
      }
    }

    return id
  }

  // private async createOrderId(restaurantId: string): Promise<string> {
  //   const now = dayjs().format('YYYYMMDD')
  //   const countBill = await this.orderCounterService.getCounter(
  //     now,
  //     restaurantId
  //   )
  //   const id = `${now}${countBill.toString().padStart(5, '0')}`
  //   return id
  // }

  private async queueKitchen(restaurantId: string): Promise<string> {
    const { timeZone } = await this.restaurantService.findOne({
      _id: restaurantId
    })
    const now = dayjs().tz(timeZone)
    const nowUnix = now.valueOf()
    const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf()
    const endOfDay = now
      .add(1, 'day')
      .hour(3)
      .minute(59)
      .millisecond(999)
      .valueOf()
    let nowFormat = now.subtract(1, 'day').format('YYYYMMDD')

    if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
      nowFormat = now.format('YYYYMMDD')
    }

    const countQueue = await this.kitchenCounterService.getCounter(
      nowFormat,
      restaurantId
    )
    return `${countQueue}`
  }

  async createOrderDineInPOS(
    staffId: string,
    createOrder: CreateOrderDineInPOSDto
  ) {
    const logic = async (session: ClientSession) => {
      const [restaurant, staff, table] = await Promise.all([
        this.restaurantService.findOneForOrder(createOrder.restaurantId),
        this.staffService.findOneForOrder(staffId),
        this.tableService.findOneForOrder(createOrder.tableId)
      ])

      if (!restaurant) throw new NotFoundException('Restaurant not found.')
      if (!staff) throw new NotFoundException('Staff not found.')
      if (!table) throw new BadRequestException('This table is unavailable.')
      let customer
      const customerData = await this.customerService.findOne(
        {
          _id: createOrder?.customerId,
          status: StatusEnum.ACTIVE
        },
        { firstName: 1, lastName: 1 }
      )
      if (!customerData) {
        customer = {
          id: null,
          firstName: 'Guest',
          lastName: ''
        }
      } else {
        customer = customerData.toObject()
      }

      createOrder._id = await this.genOrderId()
      createOrder.orderType = OrderTypeEnum.DINE_IN
      createOrder.orderDate = new Date()
      createOrder.restaurant = restaurant
      createOrder.staff = staff
      createOrder.table = table
      createOrder.customer = customer
      const rawResult = await this.orderService.transactionCreate(
        createOrder,
        session
      )
      const result = rawResult.toObject()
      const itemPayload = createOrder.items.map((item) => {
        const transform = {
          ...item,
          orderId: result.id
        }
        return transform
      })

      const itemsData = await this.orderItemsService.transactionCreateMany(
        itemPayload,
        session
      )

      if (table && createOrder.orderType === OrderTypeEnum.DINE_IN) {
        await this.tableService.transactionUpdate(
          createOrder.tableId,
          {
            tableStatus: TableStatusEnum.SEATED
          },
          session
        )
      }
      const ticketNo = await this.queueKitchen(createOrder.restaurantId)
      const kitchenReceipt = await this.kitchenQueueService.transactionCreate(
        {
          restaurantId: createOrder.restaurantId,
          orderId: result.id,
          ticketNo,
          items: itemsData.map((item) => item.id)
        },
        session
      )

      return {
        id: result.id,
        restaurant: result.restaurant,
        customer: {
          id: result.customer?.id || null,
          firstName: result.customer?.firstName,
          lastName: result.customer?.lastName,
          tel: result.customer?.tel
        },
        table: result.table,
        staff: result.staff,
        orderType: result.orderType,
        numberOfGuest: result.numberOfGuest,
        orderDate: result.orderDate,
        ticketNo: kitchenReceipt.ticketNo,
        items: itemsData,
        subtotal: result.total,
        discount: result.discount,
        serviceCharge: result.serviceCharge,
        tax: result.tax,
        alcoholTax: result.alcoholTax,
        total: result.total,
        orderStatus: result.orderStatus
      }
    }
    return runTransaction(logic)
  }

  async createOrderToGoPOS(
    staffId: string,
    createOrder: CreateOrderToGoPOSDto
  ) {
    const logic = async (session: ClientSession) => {
      const [restaurant, staff] = await Promise.all([
        this.restaurantService.findOneForOrder(createOrder.restaurantId),
        this.staffService.findOneForOrder(staffId)
      ])

      if (!restaurant) throw new NotFoundException('Restaurant not found.')
      if (!staff) throw new NotFoundException('Staff not found.')
      let customer
      const customerData = await this.customerService.findOne(
        {
          _id: createOrder?.customerId,
          status: StatusEnum.ACTIVE
        },
        { firstName: 1, lastName: 1 }
      )
      if (!customerData) {
        customer = {
          id: null,
          firstName: 'Guest',
          lastName: ''
        }
      } else {
        customer = customerData.toObject()
      }

      createOrder._id = await this.genOrderId()
      createOrder.orderType = OrderTypeEnum.TO_GO
      createOrder.orderDate = new Date()
      createOrder.restaurant = restaurant
      createOrder.staff = staff
      createOrder.customer = customer
      const rawResult = await this.orderService.transactionCreate(
        createOrder,
        session
      )
      const result = rawResult.toObject()
      const itemPayload = createOrder.items.map((item) => {
        const transform = {
          ...item,
          orderId: result.id
        }
        return transform
      })

      const itemsData = await this.orderItemsService.transactionCreateMany(
        itemPayload,
        session
      )

      const ticketNo = await this.queueKitchen(createOrder.restaurantId)
      const kitchenReceipt = await this.kitchenQueueService.transactionCreate(
        {
          restaurantId: createOrder.restaurantId,
          orderId: result.id,
          ticketNo,
          items: itemsData.map((item) => item.id)
        },
        session
      )

      return {
        id: result.id,
        restaurant: result.restaurant,
        customer: {
          id: result.customer?.id || null,
          firstName: result.customer?.firstName,
          lastName: result.customer?.lastName,
          tel: result.customer?.tel
        },
        staff: result.staff,
        orderType: result.orderType,
        numberOfGuest: result.numberOfGuest,
        orderDate: result.orderDate,
        pickUpDate: result.pickUpDate,
        ticketNo: kitchenReceipt.ticketNo,
        items: itemsData,
        subtotal: result.total,
        discount: result.discount,
        serviceCharge: result.serviceCharge,
        tax: result.tax,
        alcoholTax: result.alcoholTax,
        total: result.total,
        orderStatus: result.orderStatus
      }
    }
    return runTransaction(logic)
  }

  async getOrderByTable(tableId: string) {
    const table = await this.tableService.findOne({
      _id: tableId,
      status: StatusEnum.ACTIVE
    })
    if (!table) throw new NotFoundException('Not found table.')
    const result = await this.orderService.findOne(
      {
        'table.id': tableId,
        status: StatusEnum.ACTIVE,
        orderStatus: OrderStateEnum.PENDING
      },
      {
        summaryItems: 0,
        createdAt: 0,
        updatedAt: 0,
        pickUpDate: 0,
        status: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    if (!result) throw new NotFoundException('Not found order.')
    const items = await this.orderItemsService.getAll({ orderId: result.id })
    return {
      ...result.toObject(),
      items,
      customer: {
        id: result.customer?.id || null,
        firstName: result.customer?.firstName,
        lastName: result.customer?.lastName,
        tel: result.customer?.tel
      }
    }
  }

  async getOrderToGoById(orderId: string) {
    const order = await this.orderService.findOne(
      {
        _id: orderId,
        orderType: OrderTypeEnum.TO_GO,
        status: StatusEnum.ACTIVE
      },
      {
        summaryItems: 0,
        table: 0,
        createdAt: 0,
        updatedAt: 0,
        status: 0,
        createdBy: 0,
        updatedBy: 0
      }
    )
    if (!order) throw new NotFoundException('Not found order.')
    const items = await this.orderItemsService.getAll({ orderId: order.id })
    const kitchenQueue = await this.kitchenQueueService.findOne({ orderId })
    return {
      ...order.toObject(),
      items,
      customer: {
        id: order.customer?.id || null,
        firstName: order.customer?.firstName,
        lastName: order.customer?.lastName,
        tel: order.customer?.tel
      },
      ticketNo: kitchenQueue?.ticketNo ?? null
    }
  }

  async voidOrderById(orderId: string) {
    const logic = async (session: ClientSession) => {
      const order = await this.orderService.findOne({
        _id: orderId,
        status: StatusEnum.ACTIVE
      })
      if (!order) throw new NotFoundException('Not found order.')
      if (order.orderStatus === OrderStateEnum.VOID)
        throw new BadRequestException('This order is already void.')
      if (order.orderType === OrderTypeEnum.DINE_IN) {
        await this.tableService.transactionUpdate(
          order.table.id,
          {
            tableStatus: TableStatusEnum.AVAILABLE
          },
          session
        )
      }
      const summaryItems = await this.orderItemsLogic.summaryOrderForVoid(order)
      const updated = await this.orderService.transactionUpdate(
        orderId,
        {
          summaryItems,
          orderStatus: OrderStateEnum.VOID
        },
        session
      )
      await this.kitchenQueueService.transactionUpdateMany(
        { orderId },
        { queueStatus: ItemStatusEnum.CANCELED },
        session
      )
      await this.orderItemsService.transactionUpdateMany(
        { orderId },
        { itemStatus: ItemStatusEnum.CANCELED },
        session
      )
      await this.voidTransactionLogic.createVoidTransaction(updated, session)
      return updated
    }
    await runTransaction(logic)
    return this.orderService.findOne({ _id: orderId }, { orderStatus: 1 })
  }

  async moveOrderTable(orderId: string, payload: MoveOrderTableDto) {
    const logic = async (session: ClientSession) => {
      const order = await this.orderService.findOne({
        _id: orderId,
        orderStatus: OrderStateEnum.PENDING,
        status: StatusEnum.ACTIVE
      })
      if (!order) throw new NotFoundException('Not found order.')
      const orderData = order.toObject()
      if (orderData.orderType !== OrderTypeEnum.DINE_IN)
        throw new BadRequestException('This order type is not dine-in.')
      if (orderData.table.id === payload.tableId) return
      const table = await this.tableService.findOne(
        {
          _id: payload.tableId,
          status: StatusEnum.ACTIVE
        },
        { _id: 0, id: '$_id', tableNo: 1, tableStatus: 1 }
      )
      if (!table) throw new NotFoundException('Not found table.')
      if (table.tableStatus !== TableStatusEnum.AVAILABLE)
        throw new BadRequestException('This table is not available.')

      await this.orderService.transactionUpdate(
        orderId,
        {
          table
        },
        session
      )
      await this.tableService.transactionUpdate(
        order.table.id,
        {
          tableStatus: TableStatusEnum.AVAILABLE
        },
        session
      )
      await this.tableService.transactionUpdate(
        payload.tableId,
        {
          tableStatus: TableStatusEnum.SEATED
        },
        session
      )
      return
    }
    await runTransaction(logic)
    return this.orderService.findOne({ _id: orderId }, { table: 1 })
  }

  async updateNewOrderItems(orderId: string, payload: UpdateOrderItemsDto) {
    const logic = async (session: ClientSession) => {
      const order = await this.orderService.findOne({
        _id: orderId,
        orderStatus: OrderStateEnum.PENDING,
        status: StatusEnum.ACTIVE
      })
      if (!order) throw new NotFoundException('Not found order.')
      const newItems = payload.items.map((item) => {
        const transform = {
          ...item,
          orderId
        }
        return transform
      })
      await this.orderService.transactionUpdate(
        orderId,
        {
          subtotal: payload.subtotal,
          discount: payload.discount,
          serviceCharge: payload.serviceCharge,
          tax: payload.tax,
          alcoholTax: payload.alcoholTax,
          total: payload.total
        },
        session
      )
      const itemsData = await this.orderItemsService.transactionCreateMany(
        newItems,
        session
      )

      const ticketNo = await this.queueKitchen(order.restaurant.id)
      const kitchenReceipt = await this.kitchenQueueService.transactionCreate(
        {
          restaurantId: order.restaurant.id,
          orderId,
          ticketNo,
          items: itemsData.map((item) => item.id)
        },
        session
      )

      return { kitchenReceipt, itemsData }
    }
    const results = await runTransaction(logic)
    const orderRaw = await this.orderService.findOne(
      { _id: orderId },
      {
        restaurant: 1,
        customer: 1,
        table: 1,
        staff: 1,
        orderType: 1,
        pickUpDate: 1,
        numberOfGuest: 1,
        orderDate: 1,
        subtotal: 1,
        discount: 1,
        serviceCharge: 1,
        tax: 1,
        alcoholTax: 1,
        total: 1,
        orderStatus: 1
      }
    )
    const order = orderRaw.toObject()
    const kitchen = results.kitchenReceipt.toObject()
    return { ...order, ticketNo: kitchen.ticketNo, items: results.itemsData }
  }

  async getPaginateOrderToGo(query: any, queryParam: ToGoOrderPaginateDto) {
    const orders = await this.orderService.paginate(query, queryParam)
    const orderIds = orders?.docs?.map((item) => item._id)
    const items = await this.orderItemsService.getAll({
      orderId: { $in: orderIds }
    })
    const docs = []
    for (const doc of orders?.docs) {
      const kitchenQueue = await this.kitchenQueueService.findOne({
        orderId: doc.id
      })
      docs.push({
        ...doc.toObject(),
        items: items
          .filter((e) => e.orderId === doc.id)
          .map((item) => {
            const result = item.toObject()
            return result
          }),
        ticketNo: kitchenQueue?.ticketNo || null
      })
    }
    return { ...orders, docs }
  }
}
