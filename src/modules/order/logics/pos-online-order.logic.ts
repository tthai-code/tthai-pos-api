import { Injectable } from '@nestjs/common'
import { OrderOnlinePaginateDto } from '../dto/get-order-online.dto'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderMethodEnum } from '../enum/order-type.enum'
import { KitchenQueueService } from '../services/kitchen-queue.service'
import { OrderOnlineService } from '../services/order-online.service'
import { OrderOnlineLogic } from './order-online.logic'

@Injectable()
export class POSOnlineOrderLogic {
  constructor(
    private readonly orderOnlineLogic: OrderOnlineLogic,
    private readonly orderOnlineService: OrderOnlineService,
    private readonly kitchenQueueService: KitchenQueueService
  ) {}

  async getAllOrder(restaurantId: string, query: OrderOnlinePaginateDto) {
    const allOrder = await this.orderOnlineService.getAll(
      query.buildQueryGetAll(restaurantId)
    )

    const result = await this.orderOnlineService.paginate(
      query.buildQuery(restaurantId),
      query,
      { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 }
    )
    const { docs } = result
    const transformPayload = []
    for (const doc of docs) {
      const kitchenQueue = await this.kitchenQueueService.findOne({
        orderId: doc.id
      })
      transformPayload.push({
        ...doc?.toObject(),
        ticketNo: kitchenQueue?.ticketNo || null
      })
    }

    return {
      ...result,
      docs: transformPayload,
      status: {
        active: allOrder.filter(
          (item) =>
            item.orderStatus !== OrderStateEnum.COMPLETED &&
            item.orderStatus !== OrderStateEnum.VOID
        ).length,
        new: allOrder.filter((item) => item.orderStatus === OrderStateEnum.NEW)
          .length,
        inProgress: allOrder.filter(
          (item) => item.orderStatus === OrderStateEnum.IN_PROGRESS
        ).length,
        ready: allOrder.filter(
          (item) => item.orderStatus === OrderStateEnum.READY
        ).length,
        upcoming: allOrder.filter(
          (item) => item.orderStatus === OrderStateEnum.UPCOMING
        ).length,
        completed: allOrder.filter(
          (item) => item.orderStatus === OrderStateEnum.COMPLETED
        ).length
      },
      type: {
        delivery: allOrder.filter(
          (item) =>
            item.orderMethod === OrderMethodEnum.DELIVERY &&
            item.orderStatus !== OrderStateEnum.VOID
        ).length,
        pickup: allOrder.filter(
          (item) =>
            item.orderMethod === OrderMethodEnum.PICKUP &&
            item.orderStatus !== OrderStateEnum.VOID
        ).length
      }
    }
  }
}
