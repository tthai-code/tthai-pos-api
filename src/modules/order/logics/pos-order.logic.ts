import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { OrderTypeEnum } from '../enum/order-type.enum'
import { KitchenQueueService } from '../services/kitchen-queue.service'
import { OrderOnlineService } from '../services/order-online.service'
import { OrderService } from '../services/order.service'
import { ThirdPartyOrderService } from '../services/third-party-order.service'

@Injectable()
export class POSOrderLogic {
  constructor(
    private readonly orderService: OrderService,
    private readonly onlineOrderService: OrderOnlineService,
    private readonly thirdPartyOrderService: ThirdPartyOrderService,
    private readonly kitchenQueueService: KitchenQueueService
  ) {}

  async getDineInToGoOrder(id: string, ticketNo: string) {
    const order = await this.orderService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!order)
      throw new NotFoundException(
        `Order #${id} not found from order type dine in and to go`
      )
    return { ...order.toObject(), ticketNo }
  }

  async getOnlineOrder(id: string, ticketNo: string) {
    const order = await this.onlineOrderService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!order)
      throw new NotFoundException(
        `Order #${id} not found from order type online`
      )
    return { ...order.toObject(), ticketNo }
  }

  async getThirdPartyOrder(id: string, ticketNo: string) {
    const order = await this.thirdPartyOrderService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!order)
      throw new NotFoundException(
        `Order #${id} not found from order type3rd party`
      )
    return { ...order.toObject(), ticketNo }
  }

  async getOrderInfo(id: string, orderType: string) {
    const kitchenQueue = await this.kitchenQueueService.findOne(
      {
        orderId: id
      },
      {},
      { sort: { createdAt: -1 } }
    )
    const ticketNo = kitchenQueue?.ticketNo || null
    if (
      orderType === OrderTypeEnum.DINE_IN ||
      orderType === OrderTypeEnum.TO_GO
    ) {
      return this.getDineInToGoOrder(id, ticketNo)
    }

    if (orderType === OrderTypeEnum.ONLINE) {
      return this.getOnlineOrder(id, ticketNo)
    }

    if (orderType === OrderTypeEnum.THIRD_PARTY) {
      return this.getThirdPartyOrder(id, ticketNo)
    }
    return null
  }
}
