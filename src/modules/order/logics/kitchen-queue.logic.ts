import { Injectable } from '@nestjs/common'
import { PaginateKitchenQueue } from '../dto/get-kitchen.dto'
import { ItemStatusEnum } from '../enum/item-status.enum'
import { KitchenQueueService } from '../services/kitchen-queue.service'
import { OrderItemsService } from '../services/order-items.service'

@Injectable()
export class KitchenQueueLogic {
  constructor(
    private readonly kitchenQueueService: KitchenQueueService,
    private readonly orderItemsService: OrderItemsService
  ) {}

  private queueStatusCondition(items: Array<any>) {
    if (items.every((item) => item.itemStatus === ItemStatusEnum.READY)) {
      return ItemStatusEnum.READY
    } else if (
      items.every((item) => item.itemStatus === ItemStatusEnum.COMPLETED)
    ) {
      return ItemStatusEnum.COMPLETED
    } else if (
      items.some((item) => item.itemStatus === ItemStatusEnum.IN_PROGRESS)
    ) {
      return ItemStatusEnum.IN_PROGRESS
    } else {
      return ItemStatusEnum.CANCELED
    }
  }

  async getPaginateKitchenQueue(query: any, queryParam: PaginateKitchenQueue) {
    const kitchenQueues = await this.kitchenQueueService.paginate(
      query,
      queryParam,
      { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 }
    )
    const orderItemsIds = kitchenQueues?.docs?.map((item) => item.items)
    const items = await this.orderItemsService.getAll({
      _id: { $in: orderItemsIds.flat() }
    })
    const result = kitchenQueues?.docs?.map((queue) => {
      const itemsResult = items
        .filter((item) => queue.items.includes(item.id))
        .map((item) => {
          const result = item.toObject()
          return result
        })
      return {
        ...queue.toObject(),
        items: itemsResult
        // queueStatus: this.queueStatusCondition(itemsResult)
      }
    })
    return { ...kitchenQueues, docs: result }
  }
}
