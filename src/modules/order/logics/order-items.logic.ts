import { Injectable, NotFoundException } from '@nestjs/common'
import { classToPlain } from 'class-transformer'
import { StatusEnum } from 'src/common/enum/status.enum'
import { SummaryOrderDto } from '../dto/summary-order.dto'
import { UpdateExistOrderItemsDto } from '../dto/update-exist-items.dto'
import { ItemStatusEnum } from '../enum/item-status.enum'
import { OrderDocument } from '../schemas/order.schema'
import { KitchenQueueService } from '../services/kitchen-queue.service'
import { OrderItemsService } from '../services/order-items.service'
import { OrderService } from '../services/order.service'

@Injectable()
export class OrderItemsLogic {
  constructor(
    private readonly orderService: OrderService,
    private readonly orderItemsService: OrderItemsService,
    private readonly kitchenQueueService: KitchenQueueService
  ) {}

  async updateExistItems(
    orderItemId: string,
    payload: UpdateExistOrderItemsDto
  ) {
    const item = await this.orderItemsService.findOne({
      _id: orderItemId
    })
    if (!item) throw new NotFoundException('Not found order item.')

    const transform = classToPlain(payload)

    const updated = await this.orderItemsService.update(orderItemId, {
      ...transform.items
    })
    const order = await this.orderService.update(item.orderId, transform)
    return { orderId: order.id, items: updated }
  }

  async removeItem(orderItemId: string) {
    const item = await this.orderItemsService.findOne({
      _id: orderItemId,
      itemStatus: { $ne: ItemStatusEnum.CANCELED }
    })
    if (!item) throw new NotFoundException('Not found order item.')

    const updated = await this.orderItemsService.update(orderItemId, {
      itemStatus: ItemStatusEnum.CANCELED
    })
    const summary = updated.amount
    await this.orderService.updateOne(
      { _id: item.orderId },
      {
        $inc: { subtotal: -summary, total: -summary }
      }
    )
    return { orderId: item.orderId, items: updated }
  }

  async summaryOrder(orderId: string, payload: SummaryOrderDto) {
    const order = await this.orderService.findOne({
      _id: orderId,
      status: StatusEnum.ACTIVE
    })
    if (!order) throw new NotFoundException('Not found order.')
    const items = await this.orderItemsService.getAll({ orderId })
    const results = []
    for (const item of items) {
      const index = results.findIndex((result) => {
        if (result.menuId !== item.menuId) return false
        if (item.modifiers.length !== result.modifiers.length) return false
        const sortedResult = JSON.parse(JSON.stringify(result.modifiers)).sort(
          (a, b) => a.id - b.id || a.name.localeCompare(b.name)
        )
        const sortedItem = JSON.parse(JSON.stringify(item.modifiers)).sort(
          (a, b) => a.id - b.id || a.name.localeCompare(b.name)
        )
        for (let i = 0; i < sortedResult.length; i++) {
          if (
            sortedResult[i].id !== sortedItem[i].id ||
            sortedResult[i].name !== sortedItem[i].name
          ) {
            return false
          }
        }
        return true
      })
      if (index === -1) {
        results.push(item)
      } else {
        results[index].unit += item.unit
        results[index].amount += item.amount
      }
    }
    const updated = await this.orderService.update(orderId, {
      subtotal: payload.subtotal,
      discount: payload.discount,
      serviceCharge: payload.serviceCharge,
      tax: payload.tax,
      alcoholTax: payload.alcoholTax,
      total: payload.total,
      summaryItems: results.map((item) => ({
        id: item.menuId,
        name: item.name,
        nativeName: item.nativeName,
        price: item.price,
        modifiers: item.modifiers,
        isContainAlcohol: item.isContainAlcohol,
        coverImage: item.coverImage,
        unit: item.unit,
        amount: item.amount,
        note: item.note,
        category: item.category
      }))
    })
    const result = updated.toObject()
    result.customer = {
      ...result.customer,
      id: result.customer?.id || null
    }
    return result
  }

  async summaryOrderForVoid(order: OrderDocument) {
    const items = await this.orderItemsService.getAll({ orderId: order.id })
    const results = []
    for (const item of items) {
      const index = results.findIndex((result) => {
        if (result.menuId !== item.menuId) return false
        if (item.modifiers.length !== result.modifiers.length) return false
        const sortedResult = JSON.parse(JSON.stringify(result.modifiers)).sort(
          (a, b) => a.id - b.id || a.name.localeCompare(b.name)
        )
        const sortedItem = JSON.parse(JSON.stringify(item.modifiers)).sort(
          (a, b) => a.id - b.id || a.name.localeCompare(b.name)
        )
        for (let i = 0; i < sortedResult.length; i++) {
          if (
            sortedResult[i].id !== sortedItem[i].id ||
            sortedResult[i].name !== sortedItem[i].name
          ) {
            return false
          }
        }
        return true
      })
      if (index === -1) {
        results.push(item)
      } else {
        results[index].unit += item.unit
        results[index].amount += item.amount
      }
    }

    const summaryItems = results.map((item) => ({
      id: item.menuId,
      name: item.name,
      nativeName: item.nativeName,
      price: item.price,
      modifiers: item.modifiers,
      isContainAlcohol: item.isContainAlcohol,
      coverImage: item.coverImage,
      unit: item.unit,
      amount: item.amount,
      note: item.note,
      category: item.category
    }))
    return summaryItems
  }
}
