import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoose from 'mongoose'

export type KitchenCounterDocument = KitchenCounter & mongoose.Document

@Schema({ timestamps: true, collection: '_kitchenCounter' })
export class KitchenCounter {
  @Prop({ required: true })
  prefix: string

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: mongoose.Types.ObjectId

  @Prop({ default: 0 })
  counter: number
}

export const KitchenCounterSchema = SchemaFactory.createForClass(KitchenCounter)
