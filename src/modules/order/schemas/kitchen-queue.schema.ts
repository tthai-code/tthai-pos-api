import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { ItemStatusEnum } from '../enum/item-status.enum'

export type KitchenQueueDocument = KitchenQueueFields & Document

@Schema({ timestamps: true, strict: true, collection: 'kitchenQueue' })
export class KitchenQueueFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  orderId: string

  @Prop({ required: true })
  ticketNo: string

  @Prop({ default: [] })
  items: Array<string>

  @Prop({ default: false })
  isReprint: boolean

  @Prop({
    enum: Object.values(ItemStatusEnum),
    default: ItemStatusEnum.IN_PROGRESS
  })
  queueStatus: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop({ default: new Date() })
  printDate: Date

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const KitchenQueueSchema =
  SchemaFactory.createForClass(KitchenQueueFields)
KitchenQueueSchema.plugin(authorStampCreatePlugin)
KitchenQueueSchema.plugin(mongoosePaginate)
