/* eslint-disable @typescript-eslint/ban-types */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoose from 'mongoose'

export type OrderCounterDocument = OrderCounter & mongoose.Document

@Schema({ timestamps: true, collection: '_orderCounter' })
export class OrderCounter {
  @Prop({ required: true })
  prefix: string

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: mongoose.Types.ObjectId

  @Prop({ default: 0 })
  counter: number
}

export const OrderCounterSchema = SchemaFactory.createForClass(OrderCounter)
