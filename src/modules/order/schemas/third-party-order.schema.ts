import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoose from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderMethodEnum, OrderTypeEnum } from '../enum/order-type.enum'
import { CustomerFields, RestaurantFields } from './order.schema'

export type ThirdPartyOrderDocument = ThirdPartyOrderFields & mongoose.Document

@Schema({ _id: false, strict: true, timestamps: false })
export class StaffThirdPartyFields {
  @Prop({ default: null })
  id: string

  @Prop({ default: null })
  firstName: string

  @Prop({ default: null })
  lastName: string
}

@Schema({ _id: false, strict: true, timestamps: false })
export class ModifierThirdPartyFields {
  @Prop({ default: null })
  id: string

  @Prop({ default: null })
  label: string

  @Prop({ default: null })
  abbreviation: string

  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  nativeName: string

  @Prop({ default: null })
  price: number
}

@Schema({ _id: false, strict: true, timestamps: false })
export class ItemThirdPartyFields {
  @Prop({ default: null })
  id: number

  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  nativeName: string

  @Prop({ default: null })
  price: number

  @Prop({ default: [] })
  modifiers: Array<ModifierThirdPartyFields>

  @Prop({ default: null })
  isContainAlcohol: boolean

  @Prop({ required: null })
  coverImage: string

  @Prop({ default: null })
  unit: number

  @Prop({ default: null })
  amount: number

  @Prop({ default: '' })
  note: string
}

@Schema({ _id: false, timestamps: true, collection: 'thirdPartyOrders' })
export class ThirdPartyOrderFields {
  @Prop({ required: true })
  _id: string

  @Prop()
  restaurant: RestaurantFields

  @Prop({ default: new CustomerFields() })
  customer: CustomerFields

  @Prop({ default: null })
  staff: StaffThirdPartyFields

  @Prop({
    default: OrderTypeEnum.THIRD_PARTY,
    enum: Object.values(OrderTypeEnum)
  })
  orderType: string

  @Prop({
    default: OrderMethodEnum.PICKUP,
    enum: Object.values(OrderMethodEnum)
  })
  orderMethod: string

  @Prop({ min: 1, default: 1 })
  numberOfGuest: number

  @Prop()
  orderDate: Date

  @Prop({ default: null })
  completedDate: Date

  @Prop({ default: null })
  pickUpDate: Date

  @Prop({ default: true })
  asap: boolean

  @Prop({ default: null })
  scheduledForDate: Date

  @Prop({ default: [] })
  summaryItems: Array<ItemThirdPartyFields>

  @Prop({ default: 0 })
  subtotal: number

  @Prop({ default: 0 })
  discount: number

  @Prop({ default: 0 })
  serviceCharge: number

  @Prop({ default: 0 })
  tax: number

  @Prop({ default: 0 })
  alcoholTax: number

  @Prop({ default: 0 })
  convenienceFee: number

  @Prop({ default: 0 })
  tips: number

  @Prop({ default: 0 })
  total: number

  @Prop({
    enum: Object.values(OrderStateEnum),
    default: OrderStateEnum.NEW
  })
  orderStatus: string

  @Prop({ default: null })
  deliverySource: string

  @Prop({ default: null })
  orderKHId: number

  @Prop({ default: 0 })
  prepareTime: number

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop({ default: '' })
  note: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}

export const ThirdPartyOrderSchema = SchemaFactory.createForClass(
  ThirdPartyOrderFields
)
ThirdPartyOrderSchema.plugin(authorStampCreatePlugin)
ThirdPartyOrderSchema.plugin(mongoosePaginate)
