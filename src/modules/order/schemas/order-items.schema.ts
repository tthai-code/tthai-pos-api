import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { ItemStatusEnum } from '../enum/item-status.enum'
import { MenuCategoryOrderItemFields } from './order.schema'

export type OrderItemsDocument = OrderItemsFields & Document

@Schema({ _id: false, strict: true, timestamps: false })
class ModifierFields {
  @Prop({ required: true })
  id: string

  @Prop({ required: true })
  label: string

  @Prop({ required: true })
  abbreviation: string

  @Prop({ required: true })
  name: string

  @Prop({ required: true })
  nativeName: string

  @Prop({ required: true })
  price: number
}

@Schema({ timestamps: false, strict: true, collection: 'orderItems' })
export class OrderItemsFields {
  @Prop({ required: true })
  orderId: string

  @Prop({ required: true })
  menuId: string

  @Prop({ required: true })
  name: string

  @Prop({ required: true })
  nativeName: string

  @Prop({ required: true })
  price: number

  @Prop({ default: [] })
  modifiers: Array<ModifierFields>

  @Prop({ required: true })
  isContainAlcohol: boolean

  @Prop({ required: null })
  coverImage: string

  @Prop({ required: true })
  unit: number

  @Prop({ required: true })
  amount: number

  @Prop({ default: '' })
  note: string

  @Prop({ default: [] })
  category: MenuCategoryOrderItemFields[]

  @Prop({
    enum: Object.values(ItemStatusEnum),
    default: ItemStatusEnum.IN_PROGRESS
  })
  itemStatus: string
}
export const OrderItemsSchema = SchemaFactory.createForClass(OrderItemsFields)
OrderItemsSchema.plugin(mongoosePaginate)
