import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoose from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderTypeEnum } from '../enum/order-type.enum'
import { OrderMethod } from '../enum/order-method.enum'
import { OnlinePaymentStatusEnum } from '../enum/payment-status.enum'

import { AddressFields } from 'src/modules/restaurant/schemas/restaurant.schema'
import { MenuCategoryOrderItemFields } from './order.schema'

export type OrderOnlineDocument = OrderOnlineFields & mongoose.Document

@Schema({ _id: false, strict: true, timestamps: false })
export class TableFields {
  @Prop({ required: true })
  id: string

  @Prop({ required: true })
  tableNo: string
}

@Schema({ _id: false, strict: true, timestamps: false })
export class StaffFields {
  @Prop({ required: true })
  id: string

  @Prop({ default: null })
  firstName: string

  @Prop({ default: null })
  lastName: string
}

@Schema({ _id: false, strict: true, timestamps: false })
class ModifierFields {
  @Prop({ required: true })
  id: string

  @Prop({ required: true })
  label: string

  @Prop({ required: true })
  abbreviation: string

  @Prop({ required: true })
  name: string

  @Prop({ required: true })
  nativeName: string

  @Prop({ required: true })
  price: number
}

@Schema({ _id: false, strict: true, timestamps: false })
export class ItemFields {
  @Prop({ required: true })
  id: number

  @Prop({ required: true })
  name: string

  @Prop({ required: true })
  nativeName: string

  @Prop({ required: true })
  price: number

  @Prop({ default: [] })
  modifiers: Array<ModifierFields>

  @Prop({ required: true })
  isContainAlcohol: boolean

  @Prop({ required: null })
  coverImage: string

  @Prop({ required: true })
  unit: number

  @Prop({ required: true })
  amount: number

  @Prop({ default: '' })
  note: string

  @Prop({ default: [] })
  category: MenuCategoryOrderItemFields[]
}

@Schema({ _id: false, strict: true, timestamps: false })
export class RestaurantFields {
  @Prop({ required: true })
  id: string

  @Prop()
  legalBusinessName: string

  @Prop()
  address: AddressFields

  @Prop()
  dba: string

  @Prop()
  businessPhone: string

  @Prop()
  email: string
}

@Schema({ _id: false, strict: true, timestamps: false })
export class CustomerFields {
  @Prop({ default: null })
  id: string

  @Prop({ default: 'Guest' })
  firstName: string

  @Prop({ default: '' })
  lastName: string

  @Prop({ default: '' })
  tel: string

  @Prop({ default: '' })
  email: string
}

@Schema({ _id: false, timestamps: true, collection: 'ordersOnline' })
export class OrderOnlineFields {
  @Prop({ required: true })
  _id: string

  @Prop()
  restaurant: RestaurantFields

  @Prop({ default: new CustomerFields() })
  customer: CustomerFields

  @Prop({ default: null })
  table: TableFields

  @Prop({ default: null })
  staff: StaffFields

  @Prop({ required: true, enum: Object.values(OrderTypeEnum) })
  orderType: string

  @Prop({ min: 1, default: 1 })
  numberOfGuest: number

  @Prop()
  orderDate: Date

  @Prop({ default: null })
  pickUpDate: Date

  @Prop({ required: true, enum: Object.values(OrderMethod) })
  orderMethod: string

  @Prop({
    default: OnlinePaymentStatusEnum.UNPAID,
    enum: Object.values(OnlinePaymentStatusEnum)
  })
  paymentStatus: string

  @Prop({ default: [] })
  summaryItems: Array<ItemFields>

  @Prop({ default: 0 })
  subtotal: number

  @Prop({ default: 0 })
  discount: number

  @Prop({ default: 0 })
  serviceCharge: number

  @Prop({ default: 0 })
  tax: number

  @Prop({ default: 0 })
  alcoholTax: number

  @Prop({ default: 0 })
  convenienceFee: number

  @Prop({ default: 0 })
  total: number

  @Prop({ default: 0 })
  tips: number

  @Prop({
    enum: Object.values(OrderStateEnum),
    default: OrderStateEnum.PENDING
  })
  orderStatus: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}

export const OrderOnlineSchema = SchemaFactory.createForClass(OrderOnlineFields)
OrderOnlineSchema.plugin(authorStampCreatePlugin)
OrderOnlineSchema.plugin(mongoosePaginate)
