import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'
import * as dayjs from 'dayjs'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'

export class PaginateKitchenQueue extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'ticketNo' })
  readonly sortBy: string = 'ticketNo'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<order-id>' })
  readonly orderId: string = ''

  public buildQuery(id: string) {
    const result = {
      restaurantId: id,
      orderId: this.orderId,
      createdAt: {
        $gte: dayjs().startOf('day').toDate(),
        $lte: dayjs().endOf('day').toDate()
      },
      status: StatusEnum.ACTIVE
    }
    if (!this.orderId) delete result.orderId
    return result
  }
}
