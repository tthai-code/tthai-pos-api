import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsBoolean,
  IsNotEmptyObject,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator'
import { MenuCategoryOrderItemDto } from './create-order-pos.dto'

class ModifierFieldDto {
  @IsString()
  @ApiProperty({ example: '<modifier-id>' })
  readonly id: string

  @IsString()
  @ApiProperty({ example: 'Protein Choices' })
  readonly label: string

  @IsString()
  @ApiProperty({ example: 'P' })
  readonly abbreviation: string

  @IsString()
  @ApiProperty({ example: 'Chicken' })
  readonly name: string

  @IsString()
  @ApiProperty({ example: 'ไก่' })
  readonly nativeName: string

  @IsNumber()
  @ApiProperty({ example: 0 })
  readonly price
}

class ItemsDto {
  @IsString()
  @ApiProperty({ example: '<menu-id>' })
  readonly menuId: string

  @IsString()
  @ApiProperty({ example: 'Ka Prao' })
  readonly name: string

  @IsString()
  @ApiProperty({ example: 'กะเพรา' })
  readonly nativeName: string

  @IsNumber()
  @ApiProperty({ example: 50 })
  readonly price: number

  @IsBoolean()
  @ApiProperty({ example: false })
  readonly isContainAlcohol: boolean

  @Type(() => ModifierFieldDto)
  @IsOptional()
  @ValidateNested({ each: true })
  @ApiPropertyOptional({ type: () => [ModifierFieldDto] })
  readonly modifiers: Array<ModifierFieldDto>

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'https://example.com/' })
  readonly coverImage: string

  @IsNumber()
  @ApiProperty({ example: 2 })
  readonly unit: number

  @IsNumber()
  @ApiProperty({ example: 100 })
  readonly amount: number

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test note' })
  readonly note: string

  @Type(() => MenuCategoryOrderItemDto)
  @ValidateNested({ each: true })
  @IsOptional()
  @ApiPropertyOptional({ type: [MenuCategoryOrderItemDto] })
  readonly category: MenuCategoryOrderItemDto[]
}

export class UpdateExistOrderItemsDto {
  @Type(() => ItemsDto)
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => ItemsDto })
  items: ItemsDto

  @IsNumber()
  @ApiProperty({ example: 100 })
  readonly subtotal: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  readonly discount: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  readonly serviceCharge: number

  @IsNumber()
  @ApiProperty({ example: 7 })
  readonly tax: number

  @IsNumber()
  @ApiProperty({ example: 0 })
  readonly alcoholTax: number

  @IsNumber()
  @ApiProperty({ example: 107 })
  readonly total: number
}
