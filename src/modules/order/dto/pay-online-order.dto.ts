import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class PayOnlineOrderDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<token-from-iframe>' })
  readonly account: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'John Snow' })
  readonly cardholderName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '10' })
  readonly expirationMonth: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '25' })
  readonly expirationYear: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<cvv-number>' })
  readonly cvv: string

  @IsNumber()
  @ApiProperty({ example: 10 })
  readonly tips: number

  @IsString()
  @ApiProperty({ example: 'Asia/Bangkok' })
  readonly timeZone: string

  @IsNumber()
  @ApiProperty({ example: -420 })
  readonly offsetTimeZone: number
}
