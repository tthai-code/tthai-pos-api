import { ApiProperty } from '@nestjs/swagger'
import { IsNumber } from 'class-validator'

export class SummaryOrderDto {
  @IsNumber()
  @ApiProperty({ example: 93.6 })
  readonly subtotal: number

  @IsNumber()
  @ApiProperty({ example: 0 })
  readonly discount: number

  @IsNumber()
  @ApiProperty({ example: 5.6 })
  readonly serviceCharge: number

  @IsNumber()
  @ApiProperty({ example: 5.6 })
  readonly tax: number

  @IsNumber()
  @ApiProperty({ example: 8 })
  readonly alcoholTax: number

  @IsNumber()
  @ApiProperty({ example: 112.8 })
  readonly total: number
}
