import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  ArrayNotEmpty,
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator'
import { OrderMethod } from '../enum/order-method.enum'

export class ModifierFieldDto {
  @IsString()
  @ApiProperty({ example: '<modifier-id>' })
  readonly id: string

  @IsString()
  @ApiProperty({ example: 'Protein Choices' })
  readonly label: string

  @IsString()
  @ApiProperty({ example: 'P' })
  readonly abbreviation: string

  @IsString()
  @ApiProperty({ example: 'Chicken' })
  readonly name: string

  @IsString()
  @ApiProperty({ example: 'ไก่' })
  readonly nativeName: string

  @IsNumber()
  @ApiProperty({ example: 0 })
  readonly price
}

export class MenuCategoryOrderItemDto {
  @IsString()
  @ApiProperty({ example: '<category-id>' })
  id: string

  @IsString()
  @ApiProperty({ example: 'Appetizer' })
  name: string
}

export class ItemsDto {
  @IsString()
  @ApiProperty({ example: '<menu-id>' })
  readonly menuId: string

  @IsString()
  @ApiProperty({ example: 'Ka Prao' })
  readonly name: string

  @IsString()
  @ApiProperty({ example: 'กะเพรา' })
  readonly nativeName: string

  @IsNumber()
  @ApiProperty({ example: 50 })
  readonly price: number

  @IsBoolean()
  @ApiProperty({ example: false })
  readonly isContainAlcohol: boolean

  @Type(() => ModifierFieldDto)
  @IsOptional()
  @ValidateNested({ each: true })
  @ApiPropertyOptional({ type: () => [ModifierFieldDto] })
  readonly modifiers: Array<ModifierFieldDto>

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'https://example.com/' })
  readonly coverImage: string

  @IsNumber()
  @ApiProperty({ example: 2 })
  readonly unit: number

  @IsNumber()
  @ApiProperty({ example: 100 })
  readonly amount: number

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test note' })
  readonly note: string

  @Type(() => MenuCategoryOrderItemDto)
  @ValidateNested({ each: true })
  @IsOptional()
  @ApiPropertyOptional({ type: [MenuCategoryOrderItemDto] })
  readonly category: MenuCategoryOrderItemDto[]
}

export class CreateOrderDineInPOSDto {
  public _id: string
  public restaurant: any
  public table: any
  public staff: any
  public customer: any
  public orderDate: Date
  public orderType: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<table-id>' })
  readonly tableId: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<customer-id>' })
  readonly customerId: string

  @IsNumber()
  @ApiProperty({ example: 2 })
  readonly numberOfGuest: number

  @Type(() => ItemsDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [ItemsDto] })
  readonly items: Array<ItemsDto>

  @IsNumber()
  @ApiProperty({ example: 100 })
  readonly subtotal: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  readonly discount: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  readonly serviceCharge: number

  @IsNumber()
  @ApiProperty({ example: 7 })
  readonly tax: number

  @IsNumber()
  @ApiProperty({ example: 0 })
  readonly alcoholTax: number

  @IsNumber()
  @ApiProperty({ example: 107 })
  readonly total: number
}

export class CreateOrderToGoPOSDto {
  public _id: string
  public restaurant: any
  public staff: any
  public customer: any
  public orderDate: Date
  public orderType: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<customer-id>' })
  readonly customerId: string

  @IsNumber()
  @ApiProperty({ example: 2 })
  readonly numberOfGuest: number

  @Type(() => ItemsDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [ItemsDto] })
  readonly items: Array<ItemsDto>

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '2022-10-22T15:27:47.287Z' })
  readonly pickUpDate: string

  @IsNumber()
  @ApiProperty({ example: 100 })
  readonly subtotal: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  readonly discount: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  readonly serviceCharge: number

  @IsNumber()
  @ApiProperty({ example: 7 })
  readonly tax: number

  @IsNumber()
  @ApiProperty({ example: 0 })
  readonly alcoholTax: number

  @IsNumber()
  @ApiProperty({ example: 107 })
  readonly total: number
}

export class CreateOrderOnline {
  public _id: string
  public restaurant: any
  public table: any
  public staff: any
  public customer: any
  public orderDate: Date
  public orderType: string
  public orderStatus: string
  public convenienceFee: number
  public serviceCharge: number
  public total: number

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'customer-id' })
  readonly customerId: string

  @Type(() => ItemsDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [ItemsDto] })
  readonly items: Array<ItemsDto>

  @IsString()
  @IsNotEmpty()
  @ApiPropertyOptional({
    enum: Object.values(OrderMethod)
  })
  readonly orderMethod: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    default: ''
  })
  readonly pickUpDate: Date

  @IsNumber()
  @ApiProperty({ example: 100 })
  readonly subtotal: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  readonly discount: number

  @IsNumber()
  @ApiProperty({ example: 7 })
  readonly tax: number

  @IsNumber()
  @ApiProperty({ example: 0 })
  readonly alcoholTax: number
}
