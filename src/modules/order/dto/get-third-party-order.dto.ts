import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsIn, IsNotEmpty, IsOptional, IsString } from 'class-validator'
import * as dayjs from 'dayjs'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ProviderEnum } from 'src/modules/kitchen-hub/common/provider.enum'
import {
  OrderStateEnum,
  ThirdPartyOrderStatusEnum
} from '../enum/order-state.enum'
import { OrderMethodEnum } from '../enum/order-type.enum'

export class ThirdPartyOrderPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'createdAt' })
  readonly sortBy: string = 'createdAt'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'sT3JdMlf' })
  readonly search: string = ''

  @IsString()
  @IsIn(Object.values(ThirdPartyOrderStatusEnum))
  @ApiProperty({
    enum: Object.values(ThirdPartyOrderStatusEnum),
    example: ThirdPartyOrderStatusEnum.ACTIVE
  })
  readonly status: string = ThirdPartyOrderStatusEnum.ACTIVE

  @IsString()
  @IsIn([OrderMethodEnum.PICKUP, OrderMethodEnum.DELIVERY])
  @ApiProperty({
    enum: [OrderMethodEnum.PICKUP, OrderMethodEnum.DELIVERY],
    example: OrderMethodEnum.PICKUP
  })
  readonly type: string

  @IsString()
  @IsIn([
    ProviderEnum.DOORDASH,
    ProviderEnum.GLORIAFOOD,
    ProviderEnum.GRUBHUB,
    ProviderEnum.UBEREATS,
    'ALL'
  ])
  @ApiProperty({
    enum: Object.values(ProviderEnum),
    example: ProviderEnum.GLORIAFOOD
  })
  readonly source: string = 'ALL'

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly startDate: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly endDate: string

  public buildQuery(restaurantId: string) {
    let orderStatus
    if (this.status === ThirdPartyOrderStatusEnum.ACTIVE) {
      orderStatus = {
        $nin: [ThirdPartyOrderStatusEnum.COMPLETED, OrderStateEnum.CANCELED]
      }
    } else if (this.status === ThirdPartyOrderStatusEnum.UPCOMING) {
      orderStatus = ThirdPartyOrderStatusEnum.UPCOMING
    } else {
      orderStatus = this.status
    }
    const result = {
      $or: [
        {
          _id: {
            $regex: this.search,
            $options: 'i'
          },
          'customer.firstName': {
            $regex: this.search,
            $options: 'i'
          },
          'customer.lastName': {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      orderDate: {
        $gte: dayjs(this.startDate).toDate(),
        $lte: dayjs(this.endDate).toDate()
      },
      orderMethod: this.type,
      orderStatus,
      deliverySource: this.source,
      'restaurant.id': restaurantId,
      status: StatusEnum.ACTIVE
    }
    if (this.source === 'ALL') delete result.deliverySource

    return result
  }

  public buildQueryGetAll(id: string) {
    const result = {
      $or: [
        {
          _id: { $eq: this.search }
        },
        {
          'customer.firstName': {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          'customer.lastName': {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      'restaurant.id': id,
      orderDate: {
        $gte: dayjs(this.startDate).toDate(),
        $lte: dayjs(this.endDate).toDate()
      },
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
