import { ApiProperty } from '@nestjs/swagger'
import { IsEnum } from 'class-validator'
import { ItemStatusEnum } from '../enum/item-status.enum'

export class UpdateQueueStatusDto {
  @IsEnum(ItemStatusEnum)
  @ApiProperty({
    enum: Object.values(ItemStatusEnum),
    example: ItemStatusEnum.READY
  })
  readonly queueStatus: string
}
