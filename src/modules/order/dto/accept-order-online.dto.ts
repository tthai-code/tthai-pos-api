import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class AcceptOrderOnlineDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<order-id>' })
  readonly orderId: string
}
