import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class MoveOrderTableDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<table-id>' })
  readonly tableId: string
}
