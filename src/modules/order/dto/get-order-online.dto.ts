import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsIn, IsNotEmpty, IsOptional, IsString } from 'class-validator'
import * as dayjs from 'dayjs'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ThirdPartyOrderStatusEnum } from '../enum/order-state.enum'
import { OrderTypeEnum } from '../enum/order-type.enum'
import { OnlinePaymentStatusEnum } from '../enum/payment-status.enum'
import { OrderStateEnum } from '../enum/order-state.enum'

export class OrderOnlinePaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'orderDate' })
  readonly sortBy: string = 'orderDate'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  @IsString()
  @IsIn(Object.values(ThirdPartyOrderStatusEnum))
  @ApiProperty({
    enum: Object.values(ThirdPartyOrderStatusEnum),
    example: ThirdPartyOrderStatusEnum.ACTIVE
  })
  readonly status: string = ThirdPartyOrderStatusEnum.ACTIVE

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly startDate: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly endDate: string

  public buildQuery(id: string) {
    let orderStatus
    if (this.status === ThirdPartyOrderStatusEnum.ACTIVE) {
      orderStatus = {
        $nin: [ThirdPartyOrderStatusEnum.COMPLETED, OrderStateEnum.VOID]
      }
    } else if (this.status === ThirdPartyOrderStatusEnum.UPCOMING) {
      orderStatus = ThirdPartyOrderStatusEnum.UPCOMING
    } else {
      orderStatus = this.status
    }
    const result = {
      $or: [
        {
          _id: { $eq: this.search }
        },
        {
          'customer.firstName': {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          'customer.lastName': {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      'restaurant.id': id,
      orderType: OrderTypeEnum.ONLINE,
      orderStatus,
      orderDate: {
        $gte: dayjs(this.startDate).toDate(),
        $lte: dayjs(this.endDate).toDate()
      },
      paymentStatus: OnlinePaymentStatusEnum.PAID,
      status: StatusEnum.ACTIVE
    }
    return result
  }

  public buildQueryGetAll(id: string) {
    const result = {
      $or: [
        {
          _id: { $eq: this.search }
        },
        {
          'customer.firstName': {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          'customer.lastName': {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      'restaurant.id': id,
      orderType: OrderTypeEnum.ONLINE,
      orderDate: {
        $gte: dayjs(this.startDate).toDate(),
        $lte: dayjs(this.endDate).toDate()
      },
      paymentStatus: OnlinePaymentStatusEnum.PAID,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}

export class OrderOnlineCustomerPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'orderDate' })
  readonly sortBy: string = 'orderDate'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  @IsString()
  @IsIn(Object.values(ThirdPartyOrderStatusEnum))
  @ApiProperty({
    enum: Object.values(ThirdPartyOrderStatusEnum),
    example: ThirdPartyOrderStatusEnum.ACTIVE
  })
  readonly status: string = ThirdPartyOrderStatusEnum.ACTIVE

  public buildQuery(id: string) {
    let orderStatus
    if (this.status === ThirdPartyOrderStatusEnum.ACTIVE) {
      orderStatus = { $ne: ThirdPartyOrderStatusEnum.COMPLETED }
    } else if (this.status === ThirdPartyOrderStatusEnum.UPCOMING) {
      orderStatus = ThirdPartyOrderStatusEnum.UPCOMING
    } else {
      orderStatus = this.status
    }
    const result = {
      $or: [
        {
          _id: { $eq: this.search }
        },
        {
          'customer.firstName': {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          'customer.lastName': {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      'customer.id': id,
      orderType: OrderTypeEnum.ONLINE,
      orderStatus,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
