import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsString } from 'class-validator'
import { ItemStatusEnum } from '../enum/item-status.enum'

export class ChangeItemStatusDto {
  @IsString()
  @IsEnum(ItemStatusEnum)
  @ApiProperty({
    enum: Object.values(ItemStatusEnum),
    example: ItemStatusEnum.READY
  })
  itemStatus: string
}
