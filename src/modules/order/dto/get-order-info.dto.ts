import { ApiProperty } from '@nestjs/swagger'
import { IsEnum } from 'class-validator'
import { OrderTypeEnum } from '../enum/order-type.enum'

export class GetOrderInfoDto {
  @IsEnum(OrderTypeEnum)
  @ApiProperty({ enum: Object.values(OrderTypeEnum) })
  readonly orderType: string
}
