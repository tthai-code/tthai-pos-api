import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderTypeEnum } from '../enum/order-type.enum'

export class ToGoOrderPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'orderDate' })
  readonly sortBy: string = 'orderDate'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: OrderStateEnum.PENDING,
    enum: Object.values(OrderStateEnum)
  })
  readonly orderStatus: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          _id: { $eq: this.search }
        },
        {
          'staff.firstName': {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          'staff.lastName': {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      'restaurant.id': id,
      orderType: OrderTypeEnum.TO_GO,
      orderStatus: this.orderStatus,
      status: StatusEnum.ACTIVE
    }
    if (!this.orderStatus) delete result.orderStatus
    return result
  }
}
