import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class AddressObject {
  @ApiProperty()
  zip_code: string
  @ApiProperty()
  state: string
  @ApiProperty()
  city: string
  @ApiProperty()
  address: string
}

class RestaurantObject {
  @ApiProperty()
  email: string
  @ApiProperty()
  business_phone: string
  @ApiProperty()
  address: AddressObject
  @ApiProperty()
  legal_business_name: string
  @ApiProperty()
  id: string
}

class ModifierItemObject {
  @ApiProperty()
  id: string
  @ApiProperty()
  label: string
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  name: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  price: number
}

class CustomerObject {
  @ApiProperty()
  id: string
  @ApiProperty()
  first_name: string
  @ApiProperty()
  last_name: string
  @ApiProperty()
  tel: string
}

class StaffObject {
  @ApiProperty()
  id: string

  @ApiProperty()
  first_name: string

  @ApiProperty()
  last_name: string
}

class OnlineOrderObject {
  @ApiProperty()
  tips: number
  @ApiProperty()
  order_status: string
  @ApiProperty()
  total: number
  @ApiProperty()
  convenience_fee: number
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
  @ApiProperty()
  service_charge: number
  @ApiProperty()
  discount: number
  @ApiProperty()
  subtotal: number
  @ApiProperty()
  payment_status: string
  @ApiProperty()
  order_method: string
  @ApiProperty()
  pick_up_date: string
  @ApiProperty()
  order_date: string
  @ApiProperty()
  number_of_guest: number
  @ApiProperty()
  order_type: string
  @ApiProperty()
  staff: StaffObject
  @ApiProperty()
  customer: CustomerObject
  @ApiProperty()
  restaurant: RestaurantObject
  @ApiProperty()
  id: string
  @ApiProperty()
  items: ModifierItemObject[]
  @ApiProperty()
  ticket_no: string
}

export class GetOnlineOrderByIdForPOSResponse extends ResponseDto<OnlineOrderObject> {
  @ApiProperty({
    type: OnlineOrderObject,
    example: {
      tips: 0,
      order_status: 'NEW',
      total: 107,
      convenience_fee: 0,
      alcohol_tax: 0,
      tax: 7,
      service_charge: 10,
      discount: 10,
      subtotal: 100,
      payment_status: 'UNPAID',
      order_method: 'PICKUP',
      pick_up_date: null,
      order_date: '2022-12-07T08:04:37.051Z',
      number_of_guest: 1,
      order_type: 'ONLINE',
      staff: {
        id: '6390437f6f06b6b4ca69f6d0',
        first_name: 'Cory',
        last_name: 'Wong'
      },
      customer: {
        id: '6390437f6f06b6b4ca69f6d0',
        first_name: 'Tim',
        last_name: 'Henson',
        tel: '5102638229'
      },
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      id: 'Ko9wfPR6',
      items: [
        {
          menu_id: '630fc086af525d6ed9fc0d5d',
          name: 'Ka Prao',
          native_name: 'กะเพรา',
          price: 50,
          is_contain_alcohol: false,
          modifiers: [
            {
              id: '630f464bb75d5da2ea75a5aa',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Chicken',
              native_name: 'ไก่',
              price: 0
            }
          ],
          cover_image: 'https://example.com/',
          unit: 2,
          amount: 100,
          note: 'test note'
        }
      ],
      ticket_no: '1'
    }
  })
  data: OnlineOrderObject
}
