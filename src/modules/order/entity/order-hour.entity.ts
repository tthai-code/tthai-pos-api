import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class CheckOrderHourObject {
  @ApiProperty()
  is_available: boolean
}

export class CheckOrderHourResponse extends ResponseDto<CheckOrderHourObject> {
  @ApiProperty({
    type: CheckOrderHourObject,
    example: {
      is_available: false
    }
  })
  data: CheckOrderHourObject
}
