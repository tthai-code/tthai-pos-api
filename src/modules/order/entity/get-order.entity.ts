import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { ItemStatusEnum } from '../enum/item-status.enum'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderTypeEnum } from '../enum/order-type.enum'
import { MenuCategoryOrderItemObject } from './create-order.entity'

class AddressFields {
  @ApiProperty()
  address: string

  @ApiProperty()
  city: string

  @ApiProperty()
  state: string

  @ApiProperty()
  zip_code: string
}

class RestaurantFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  legal_business_name: string

  @ApiProperty()
  email: string

  @ApiProperty()
  business_phone: string

  @ApiProperty({ type: AddressFields })
  address: AddressFields
}

class CustomerFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  first_name: string

  @ApiProperty()
  last_name: string

  @ApiProperty()
  tel: string
}

class TableFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  table_no: string
}

class StaffFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  first_name: string

  @ApiProperty()
  last_name: string
}

class ModifierFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  label: string

  @ApiProperty()
  abbreviation: string

  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  price: number
}

class ItemFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  order_id: string

  @ApiProperty()
  menu_id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  price: number

  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty({ type: [ModifierFields] })
  modifiers: Array<ModifierFields>

  @ApiProperty()
  unit: number

  @ApiProperty()
  amount: number

  @ApiProperty()
  note: string

  @ApiProperty({ enum: Object.values(ItemStatusEnum) })
  item_status: string

  @ApiProperty({ type: [MenuCategoryOrderItemObject] })
  category: MenuCategoryOrderItemObject[]
}

class OrderBaseFields {
  @ApiProperty()
  id: string

  @ApiProperty({ type: RestaurantFields })
  restaurant: RestaurantFields

  @ApiProperty({ type: CustomerFields })
  customer: CustomerFields

  @ApiProperty({ type: StaffFields })
  staff: StaffFields

  @ApiProperty({ enum: Object.values(OrderTypeEnum) })
  order_type: string

  @ApiProperty()
  number_of_guest: number

  @ApiProperty()
  order_date: string

  @ApiProperty()
  ticket_no: string

  @ApiProperty()
  items: Array<ItemFields>

  @ApiProperty()
  subtotal: number

  @ApiProperty()
  discount: number

  @ApiProperty()
  service_charge: number

  @ApiProperty()
  tax: number

  @ApiProperty()
  alcohol_tax: number

  @ApiProperty()
  total: number

  @ApiProperty({ enum: Object.values(OrderStateEnum) })
  order_status: string
}

class GetOrderByTableIdFields extends OrderBaseFields {
  @ApiProperty({ type: TableFields })
  table: TableFields
}

class GetToGoOrderByIdFields extends OrderBaseFields {
  @ApiProperty()
  pick_up_date: string

  @ApiProperty()
  ticket_no: string
}

export class GetOrderByTableIdResponse extends ResponseDto<GetOrderByTableIdFields> {
  @ApiProperty({
    type: GetOrderByTableIdFields,
    example: {
      order_status: 'PENDING',
      total: 40,
      convenience_fee: 0,
      alcohol_tax: 0,
      tax: 0,
      service_charge: 0,
      discount: 0,
      subtotal: 40,
      order_date: '2022-10-30T20:48:42.091Z',
      number_of_guest: 1,
      order_type: 'DINE_IN',
      staff: {
        last_name: 'Plini',
        first_name: 'Plini',
        id: '630f1c078653105bf1478b82'
      },
      table: {
        table_no: 1,
        id: '631280e1d8aafae7cae2452b'
      },
      customer: {
        id: null,
        first_name: 'Guest',
        last_name: '',
        tel: ''
      },
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      id: 'CPbcayEE',
      items: [
        {
          item_status: 'IN_PROGRESS',
          category: [
            {
              id: '635ee32a19664a40ea51a10c',
              name: 'Drink'
            }
          ],
          note: '',
          amount: 40,
          unit: 1,
          is_contain_alcohol: true,
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'No Protein',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Chang Beer',
          menu_id: '634d2a6c57a023457c8e498d',
          order_id: 'CPbcayEE',
          id: '635ee32a19664a40ea51a10c'
        }
      ]
    }
  })
  data: GetOrderByTableIdFields
}

export class GetToGoOrderByIdResponse extends ResponseDto<GetToGoOrderByIdFields> {
  @ApiProperty({
    type: GetToGoOrderByIdFields,
    example: {
      order_status: 'PENDING',
      total: 80,
      convenience_fee: 0,
      alcohol_tax: 0,
      tax: 0,
      service_charge: 0,
      discount: 0,
      subtotal: 80,
      pick_up_date: '2022-10-22T15:40:35.829Z',
      order_date: '2022-10-30T20:54:48.453Z',
      number_of_guest: 1,
      order_type: 'TO_GO',
      staff: {
        last_name: 'Plini',
        first_name: 'Plini',
        id: '630f1c078653105bf1478b82'
      },
      customer: {
        id: null,
        first_name: 'Guest',
        last_name: '',
        tel: ''
      },
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      id: 'ufgV2657',
      items: [
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 40,
          unit: 1,
          is_contain_alcohol: true,
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'No Protein',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Chang Beer',
          menu_id: '634d2a6c57a023457c8e498d',
          order_id: 'ufgV2657',
          id: '635ee498d72a2d86521286a1'
        },
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 40,
          unit: 1,
          is_contain_alcohol: true,
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'Chicken',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Chang Beer',
          menu_id: '634d2a6c57a023457c8e498d',
          order_id: 'ufgV2657',
          id: '635ee498d72a2d86521286a2'
        }
      ],
      ticket_no: 1
    }
  })
  data: GetToGoOrderByIdFields
}

class ToGoOrderFields {
  @ApiProperty()
  id: string

  @ApiProperty({ type: RestaurantFields })
  restaurant: RestaurantFields

  @ApiProperty({ type: CustomerFields })
  customer: CustomerFields

  @ApiProperty({ type: StaffFields })
  staff: StaffFields

  @ApiProperty({ enum: Object.values(OrderTypeEnum) })
  order_type: string

  @ApiProperty()
  pick_up_date: string

  @ApiProperty()
  number_of_guest: number

  @ApiProperty()
  order_date: string

  @ApiProperty()
  ticket_no: string

  @ApiProperty()
  items: Array<ItemFields>

  @ApiProperty()
  subtotal: number

  @ApiProperty()
  discount: number

  @ApiProperty()
  service_charge: number

  @ApiProperty()
  tax: number

  @ApiProperty()
  alcohol_tax: number

  @ApiProperty()
  total: number

  @ApiProperty({ enum: Object.values(OrderStateEnum) })
  order_status: string
}

class PaginateToGoOrder extends PaginateResponseDto<Array<ToGoOrderFields>> {
  @ApiProperty({
    type: [ToGoOrderFields],
    example: [
      {
        status: 'active',
        order_status: 'PENDING',
        total: 80,
        convenience_fee: 0,
        alcohol_tax: 0,
        tax: 0,
        service_charge: 0,
        discount: 0,
        subtotal: 80,
        summary_items: [],
        pick_up_date: '2022-10-22T15:40:35.829Z',
        order_date: '2022-10-30T21:52:24.789Z',
        number_of_guest: 1,
        order_type: 'TO_GO',
        staff: {
          last_name: 'Plini',
          first_name: 'Plini',
          id: '630f1c078653105bf1478b82'
        },
        table: null,
        customer: {
          tel: '',
          last_name: '',
          first_name: 'Guest'
        },
        restaurant: {
          email: 'corywong@mail.com',
          business_phone: '0222222222',
          address: {
            zip_code: '75001',
            state: 'Dallas',
            city: 'Addison',
            address: 'Some Address'
          },
          legal_business_name: 'Holy Beef',
          id: '630e55a0d9c30fd7cdcb424b'
        },
        created_at: '2022-10-30T21:52:24.820Z',
        updated_at: '2022-10-30T21:52:24.820Z',
        updated_by: {
          username: 'Plini',
          id: '630f1c078653105bf1478b82'
        },
        created_by: {
          username: 'Plini',
          id: '630f1c078653105bf1478b82'
        },
        id: 'vH0o0Uwj',
        items: [
          {
            id: '635ef218376e7921d44f5065',
            item_status: 'IN_PROGRESS',
            note: '',
            amount: 40,
            unit: 1,
            is_contain_alcohol: true,
            modifiers: [
              {
                id: '634d2a6c57a023457c8e4990',
                label: 'Choice of Protein',
                abbreviation: 'P',
                name: 'No Protein',
                native_name: 'Native Name Test',
                price: 0
              }
            ],
            price: 40,
            native_name: 'test nativeName',
            name: 'Chang Beer',
            menu_id: '634d2a6c57a023457c8e498d',
            order_id: 'vH0o0Uwj'
          },
          {
            id: '635ef218376e7921d44f5066',
            item_status: 'IN_PROGRESS',
            note: '',
            amount: 40,
            unit: 1,
            is_contain_alcohol: true,
            modifiers: [
              {
                id: '634d2a6c57a023457c8e4990',
                label: 'Choice of Protein',
                abbreviation: 'P',
                name: 'Chicken',
                native_name: 'Native Name Test',
                price: 0
              }
            ],
            price: 40,
            native_name: 'test nativeName',
            name: 'Chang Beer',
            menu_id: '634d2a6c57a023457c8e498d',
            order_id: 'vH0o0Uwj'
          }
        ],
        ticket_no: '1'
      }
    ]
  })
  results: Array<ToGoOrderFields>
}

export class PaginateToGoOrderResponse extends ResponseDto<PaginateToGoOrder> {
  @ApiProperty({ type: PaginateToGoOrder })
  data: PaginateToGoOrder
}
