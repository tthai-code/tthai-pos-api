import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { ItemStatusEnum } from '../enum/item-status.enum'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderTypeEnum } from '../enum/order-type.enum'

class AddressFields {
  @ApiProperty()
  address: string

  @ApiProperty()
  city: string

  @ApiProperty()
  state: string

  @ApiProperty()
  zip_code: string
}

class RestaurantFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  legal_business_name: string

  @ApiProperty()
  email: string

  @ApiProperty()
  business_phone: string

  @ApiProperty({ type: AddressFields })
  address: AddressFields
}

class CustomerFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  first_name: string

  @ApiProperty()
  last_name: string

  @ApiProperty()
  tel: string
}

class TableFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  table_no: string
}

class StaffFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  first_name: string

  @ApiProperty()
  last_name: string
}

class ModifierFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  label: string

  @ApiProperty()
  abbreviation: string

  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  price: number
}

export class MenuCategoryOrderItemObject {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string
}

class ItemFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  order_id: string

  @ApiProperty()
  menu_id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  price: number

  @ApiProperty()
  is_contain_alcohol: boolean

  @ApiProperty({ type: [ModifierFields] })
  modifiers: Array<ModifierFields>

  @ApiProperty()
  unit: number

  @ApiProperty()
  amount: number

  @ApiProperty()
  note: string

  @ApiProperty({ enum: Object.values(ItemStatusEnum) })
  item_status: string

  @ApiProperty({ type: MenuCategoryOrderItemObject })
  category: MenuCategoryOrderItemObject[]
}

class OrderBaseFields {
  @ApiProperty()
  id: string

  @ApiProperty({ type: RestaurantFields })
  restaurant: RestaurantFields

  @ApiProperty({ type: CustomerFields })
  customer: CustomerFields

  @ApiProperty({ type: StaffFields })
  staff: StaffFields

  @ApiProperty({ enum: Object.values(OrderTypeEnum) })
  order_type: string

  @ApiProperty()
  number_of_guest: number

  @ApiProperty()
  order_date: string

  @ApiProperty()
  ticket_no: string

  @ApiProperty()
  items: Array<ItemFields>

  @ApiProperty()
  subtotal: number

  @ApiProperty()
  discount: number

  @ApiProperty()
  service_charge: number

  @ApiProperty()
  tax: number

  @ApiProperty()
  alcohol_tax: number

  @ApiProperty()
  total: number

  @ApiProperty({ enum: Object.values(OrderStateEnum) })
  order_status: string
}

class CreateOrderDineInFields extends OrderBaseFields {
  @ApiProperty({ type: TableFields })
  table: TableFields
}

class CreateOrderToGoFields extends OrderBaseFields {
  @ApiProperty()
  pickUpDate: string
}

export class CreateOrderCheckoutInResponse extends ResponseDto<OrderBaseFields> {
  @ApiProperty({
    type: OrderBaseFields,
    example: {
      id: 'qMePYBBz',
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      customer: {
        id: null,
        first_name: 'Guest',
        last_name: '',
        tel: ''
      },
      table: {
        table_no: 1,
        id: '631280e1d8aafae7cae2452b'
      },
      staff: {
        last_name: 'Plini',
        first_name: 'Plini',
        id: '630f1c078653105bf1478b82'
      },
      order_type: 'DINE_IN',
      number_of_guest: 1,
      order_date: '2022-10-30T21:24:00.627Z',
      ticket_no: '1',
      items: [
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 40,
          unit: 1,
          is_contain_alcohol: true,
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'No Protein',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Chang Beer',
          menu_id: '634d2a6c57a023457c8e498d',
          order_id: 'qMePYBBz',
          id: '635eeb704ac76ccbae5ebe8e'
        }
      ],
      subtotal: 40,
      discount: 0,
      service_charge: 0,
      tax: 0,
      alcohol_tax: 0,
      total: 40,
      order_status: 'PENDING'
    }
  })
  data: OrderBaseFields
}

export class CreateOrderDineInResponse extends ResponseDto<CreateOrderDineInFields> {
  @ApiProperty({
    type: CreateOrderDineInFields,
    example: {
      id: 'qMePYBBz',
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      customer: {
        id: null,
        first_name: 'Guest',
        last_name: '',
        tel: ''
      },
      table: {
        table_no: 1,
        id: '631280e1d8aafae7cae2452b'
      },
      staff: {
        last_name: 'Plini',
        first_name: 'Plini',
        id: '630f1c078653105bf1478b82'
      },
      order_type: 'DINE_IN',
      number_of_guest: 1,
      order_date: '2022-10-30T21:24:00.627Z',
      ticket_no: '1',
      items: [
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 40,
          unit: 1,
          is_contain_alcohol: true,
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'No Protein',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Chang Beer',
          menu_id: '634d2a6c57a023457c8e498d',
          order_id: 'qMePYBBz',
          id: '635eeb704ac76ccbae5ebe8e'
        }
      ],
      subtotal: 40,
      discount: 0,
      service_charge: 0,
      tax: 0,
      alcohol_tax: 0,
      total: 40,
      order_status: 'PENDING'
    }
  })
  data: CreateOrderDineInFields
}

export class CreateOrderToGoResponse extends ResponseDto<CreateOrderToGoFields> {
  @ApiProperty({
    type: CreateOrderToGoFields,
    example: {
      id: 'ASNN9MNe',
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      customer: {
        first_name: 'Guest',
        last_name: '',
        tel: ''
      },
      staff: {
        last_name: 'Plini',
        first_name: 'Plini',
        id: '630f1c078653105bf1478b82'
      },
      order_type: 'TO_GO',
      number_of_guest: 1,
      order_date: '2022-10-30T21:21:55.059Z',
      pick_up_date: '2022-10-22T15:40:35.829Z',
      ticket_no: '8',
      items: [
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 40,
          unit: 1,
          is_contain_alcohol: true,
          category: [
            {
              id: '635ee32a19664a40ea51a10c',
              name: 'Drink'
            }
          ],
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'No Protein',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Chang Beer',
          menu_id: '634d2a6c57a023457c8e498d',
          order_id: 'ASNN9MNe',
          id: '635eeaf326af700a6402eade'
        },
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 40,
          unit: 1,
          is_contain_alcohol: true,
          category: [
            {
              id: '635ee32a19664a40ea51a10c',
              name: 'Drink'
            }
          ],
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'Chicken',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Chang Beer',
          menu_id: '634d2a6c57a023457c8e498d',
          order_id: 'ASNN9MNe',
          id: '635eeaf326af700a6402eadf'
        }
      ],
      subtotal: 80,
      discount: 0,
      service_charge: 0,
      tax: 0,
      alcohol_tax: 0,
      total: 80,
      order_status: 'PENDING'
    }
  })
  data: CreateOrderToGoFields
}
