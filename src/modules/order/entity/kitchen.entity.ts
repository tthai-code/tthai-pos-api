import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { ItemStatusEnum } from '../enum/item-status.enum'

class ModifiersFields {
  @ApiProperty()
  id: string
  @ApiProperty()
  label: string
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  name: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  price: number
}

class ItemsFields {
  @ApiProperty({ enum: Object.values(ItemStatusEnum) })
  item_status: string
  @ApiProperty()
  note: string
  @ApiProperty()
  amount: number
  @ApiProperty()
  unit: number
  @ApiProperty()
  cover_image: string
  @ApiProperty()
  is_contain_alcohol: boolean
  @ApiProperty()
  modifiers: Array<ModifiersFields>
  @ApiProperty()
  price: number
  @ApiProperty()
  native_name: string
  @ApiProperty()
  name: string
  @ApiProperty()
  menu_id: string
  @ApiProperty()
  order_id: string
  @ApiProperty()
  id: string
}

class KitchenQueueFields {
  @ApiProperty()
  print_date: string
  @ApiProperty({ enum: Object.values(ItemStatusEnum) })
  queue_status: string
  @ApiProperty()
  is_reprint: boolean
  @ApiProperty()
  items: Array<ItemsFields>
  @ApiProperty()
  ticket_no: string
  @ApiProperty()
  order_id: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
}

class KitchenFieldsPaginate extends PaginateResponseDto<
  Array<KitchenQueueFields>
> {
  @ApiProperty({
    type: [KitchenQueueFields],
    example: [
      {
        print_date: '2022-10-31T19:15:54.960Z',
        queue_status: 'READY',
        is_reprint: false,
        items: [
          {
            item_status: 'READY',
            note: '',
            amount: 80,
            unit: 2,
            cover_image: '',
            is_contain_alcohol: false,
            modifiers: [
              {
                id: '630fcd08b6fb5ee3bab46885',
                label: 'Protein Choices',
                abbreviation: 'P',
                name: 'Chicken',
                native_name: 'Chicken',
                price: 0
              }
            ],
            price: 40,
            native_name: 'test nativeName',
            name: 'Pad Thai',
            menu_id: '630fc086af525d6ed9fc0d5d',
            order_id: 'HS9SqQB2',
            id: '63601f35ee84c9491202f524'
          }
        ],
        ticket_no: '1',
        order_id: 'HS9SqQB2',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        id: '63601f35ee84c9491202f527'
      }
    ]
  })
  results: Array<KitchenQueueFields>
}

export class PaginateKitchenQueueResponse extends ResponseDto<KitchenFieldsPaginate> {
  @ApiProperty({ type: () => KitchenFieldsPaginate })
  data: KitchenFieldsPaginate
}

class SuccessFields {
  @ApiProperty()
  success: boolean
}

export class UpdateItemStatusResponse extends ResponseDto<SuccessFields> {
  @ApiProperty({
    type: () => SuccessFields,
    example: {
      success: true
    }
  })
  data: SuccessFields
}
