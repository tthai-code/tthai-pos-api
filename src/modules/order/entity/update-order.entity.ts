import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'

class ModifierFields {
  @ApiProperty()
  name: string
  @ApiProperty()
  price: number
  @ApiProperty()
  native_name: string
  @ApiProperty()
  label: string
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  id: string
}

class ItemFields {
  @ApiProperty()
  item_status: string
  @ApiProperty()
  note: string
  @ApiProperty()
  amount: number
  @ApiProperty()
  unit: number
  @ApiProperty()
  cover_image: string
  @ApiProperty()
  is_contain_alcohol: boolean
  @ApiProperty({ type: () => [ModifierFields] })
  modifiers: Array<ModifierFields>
  @ApiProperty()
  price: number
  @ApiProperty()
  native_name: string
  @ApiProperty()
  name: string
  @ApiProperty()
  menu_id: string
  @ApiProperty()
  order_id: string
  @ApiProperty()
  id: string
}

class AddressFields {
  @ApiProperty()
  zip_code: string
  @ApiProperty()
  state: string
  @ApiProperty()
  city: string
  @ApiProperty()
  address: string
}

class StaffFields {
  @ApiProperty()
  last_name: string
  @ApiProperty()
  first_name: string
  @ApiProperty()
  id: string
}

class TableFields {
  @ApiProperty()
  table_no: string
  @ApiProperty()
  id: string
}

class CustomerFields {
  @ApiProperty()
  tel: string
  @ApiProperty()
  last_name: string
  @ApiProperty()
  first_name: string
  @ApiProperty()
  id: string
}

class RestaurantFields {
  @ApiProperty()
  email: string
  @ApiProperty()
  business_phone: string
  @ApiProperty({ type: () => AddressFields })
  address: AddressFields
  @ApiProperty()
  legal_business_name: string
  @ApiProperty()
  id: string
}

class BaseOrderFields {
  @ApiProperty()
  order_status: string
  @ApiProperty()
  total: number
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
  @ApiProperty()
  service_charge: number
  @ApiProperty()
  discount: number
  @ApiProperty()
  subtotal: number
  @ApiProperty()
  pick_up_date: string
  @ApiProperty()
  order_date: string
  @ApiProperty()
  number_of_guest: number
  @ApiProperty()
  order_type: string
  @ApiProperty({ type: () => StaffFields })
  staff: StaffFields
  @ApiProperty({ type: () => TableFields })
  table: TableFields
  @ApiProperty({ type: () => CustomerFields })
  customer: CustomerFields
  @ApiProperty({ type: () => RestaurantFields })
  restaurant: RestaurantFields
  @ApiProperty()
  id: string
  @ApiProperty()
  ticket_no: string
  @ApiProperty({ type: () => [ItemFields] })
  items: Array<ItemFields>
}

class OrderWithTimestampFields extends TimestampResponseDto {
  @ApiProperty()
  order_status: string
  @ApiProperty()
  total: number
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
  @ApiProperty()
  service_charge: number
  @ApiProperty()
  discount: number
  @ApiProperty()
  subtotal: number
  @ApiProperty()
  pick_up_date: string
  @ApiProperty()
  order_date: string
  @ApiProperty()
  number_of_guest: number
  @ApiProperty()
  order_type: string
  @ApiProperty({ type: () => StaffFields })
  staff: StaffFields
  @ApiProperty({ type: () => TableFields })
  table: TableFields
  @ApiProperty({ type: () => CustomerFields })
  customer: CustomerFields
  @ApiProperty({ type: () => RestaurantFields })
  restaurant: RestaurantFields
  @ApiProperty()
  id: string
  @ApiProperty()
  ticket_no: string
  @ApiProperty({ type: () => [ItemFields] })
  items: Array<ItemFields>
}

export class AddNewOrderItemsResponse extends ResponseDto<BaseOrderFields> {
  @ApiProperty({
    type: BaseOrderFields,
    example: {
      order_status: 'PENDING',
      total: 48,
      alcohol_tax: 0,
      tax: 0,
      service_charge: 0,
      discount: 0,
      subtotal: 48,
      pick_up_date: null,
      order_date: '2022-10-31T19:48:37.705Z',
      number_of_guest: 1,
      order_type: 'DINE_IN',
      staff: {
        last_name: 'NP',
        first_name: 'Pavarich',
        id: '6316cc72b889593b028928f1'
      },
      table: {
        table_no: '5',
        id: '633699d658fe7dc03f44920e'
      },
      customer: {
        tel: '',
        last_name: 'Henson',
        first_name: 'Tim',
        id: '635a421765e365cb4f0bb378'
      },
      restaurant: {
        email: 'info@carolinaswise.com',
        business_phone: '5102638229',
        address: {
          zip_code: '94501',
          state: 'CA',
          city: 'Alameda',
          address: '1319 Park St'
        },
        legal_business_name: 'May Thai Kitchen LLC',
        id: '630eff5751c2eac55f52662c'
      },
      id: 'tyLRbra1',
      ticket_no: '2',
      items: [
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 32,
          unit: 2,
          cover_image: null,
          is_contain_alcohol: false,
          modifiers: [
            {
              name: 'Pork',
              price: 2,
              native_name: 'Native Name Test',
              label: 'Protein Choice',
              abbreviation: 'P',
              id: '6337443cc64b8b0998892cd2'
            }
          ],
          price: 14,
          native_name: 'test nativeName',
          name: 'Larb',
          menu_id: '6337443cc64b8b0998892ccf',
          order_id: 'tyLRbra1',
          id: '636026fcf3b802dec079707f'
        }
      ]
    }
  })
  data: BaseOrderFields
}

class UpdateExistOrderItemsField {
  @ApiProperty()
  order_id: string

  @ApiProperty({ type: () => ItemFields })
  items: ItemFields
}

export class UpdateExistOrderItemsResponse extends ResponseDto<UpdateExistOrderItemsField> {
  @ApiProperty({
    type: () => UpdateExistOrderItemsField,
    example: {
      order_id: 'tyLRbra1',
      items: {
        item_status: 'IN_PROGRESS',
        note: 'mild spicy',
        amount: 32,
        unit: 2,
        cover_image: null,
        is_contain_alcohol: false,
        modifiers: [
          {
            name: 'Pork',
            price: 2,
            native_name: 'Native Name Test',
            label: 'Protein Choice',
            abbreviation: 'P',
            id: '6337443cc64b8b0998892cd2'
          }
        ],
        price: 14,
        native_name: 'test nativeName',
        name: 'Larb',
        menu_id: '6337443cc64b8b0998892ccf',
        order_id: 'tyLRbra1',
        id: '63602695f3b802dec0797075'
      }
    }
  })
  data: UpdateExistOrderItemsField
}

export class DeleteOrderItemResponse extends ResponseDto<UpdateExistOrderItemsField> {
  @ApiProperty({
    type: () => UpdateExistOrderItemsField,
    example: {
      order_id: 'tyLRbra1',
      items: {
        item_status: 'CANCELED',
        note: '',
        amount: 32,
        unit: 2,
        cover_image: null,
        is_contain_alcohol: false,
        modifiers: [
          {
            name: 'Pork',
            price: 2,
            native_name: 'Native Name Test',
            label: 'Protein Choice',
            abbreviation: 'P',
            id: '6337443cc64b8b0998892cd2'
          }
        ],
        price: 14,
        native_name: 'test nativeName',
        name: 'Larb',
        menu_id: '6337443cc64b8b0998892ccf',
        order_id: 'tyLRbra1',
        id: '636026fcf3b802dec079707f'
      }
    }
  })
  data: UpdateExistOrderItemsField
}

export class SummaryOrderItemsResponse extends ResponseDto<OrderWithTimestampFields> {
  @ApiProperty({
    type: () => OrderWithTimestampFields,
    example: {
      created_by: {
        username: 'Test',
        id: '63a03f43014de42db475bba9'
      },
      updated_by: {
        username: 'Test',
        id: '63a03f43014de42db475bba9'
      },
      status: 'active',
      order_status: 'PENDING',
      total: 112.8,
      convenience_fee: 0,
      alcohol_tax: 8,
      tax: 5.6,
      service_charge: 5.6,
      discount: 0,
      subtotal: 93.6,
      summary_items: [
        {
          id: '634d2a6c57a023457c8e498d',
          name: 'Chang Beer',
          native_name: 'test nativeName',
          price: 40,
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'No Protein',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          is_contain_alcohol: true,
          unit: 2,
          amount: 40,
          note: ''
        }
      ],
      pick_up_date: '2022-12-05T15:40:35.829Z',
      order_date: '2023-01-23T05:04:49.772Z',
      number_of_guest: 1,
      order_type: 'TO_GO',
      staff: {
        last_name: 'Staff',
        first_name: 'Test',
        id: '63a03f43014de42db475bba9'
      },
      table: null,
      customer: {
        tel: '',
        last_name: '',
        first_name: 'Guest',
        id: null
      },
      restaurant: {
        email: 'corywong@mail.com',
        business_phone: '0222222222',
        address: {
          zip_code: '75001',
          state: 'Dallas',
          city: 'Addison',
          address: 'Some Address'
        },
        dba: 'Test Doing as Business',
        legal_business_name: 'Holy Beef',
        id: '630e55a0d9c30fd7cdcb424b'
      },
      updated_at: '2023-01-23T05:08:22.149Z',
      created_at: '2023-01-23T05:04:49.796Z',
      id: 'irn2gMwJ'
    }
  })
  data: OrderWithTimestampFields
}
