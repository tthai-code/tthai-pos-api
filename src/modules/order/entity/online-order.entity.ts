import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class CreateOnlineOrderObject {
  @ApiProperty()
  id: string
  @ApiProperty()
  order_status: string
  @ApiProperty()
  payment_status: string
}

export class CreateOnlineOrderResponse extends ResponseDto<CreateOnlineOrderObject> {
  @ApiProperty({
    type: CreateOnlineOrderObject,
    example: { id: 'NuxG1klo', order_status: 'NEW', payment_status: 'UNPAID' }
  })
  data: CreateOnlineOrderObject
}
