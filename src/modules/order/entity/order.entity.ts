import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class VoidOrderFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  orderStatus: string
}

class TableFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  table_no: string
}

class MoveTableFields {
  @ApiProperty()
  id: string

  @ApiProperty({ type: TableFields })
  table: TableFields
}

export class VoidOrderResponse extends ResponseDto<VoidOrderFields> {
  @ApiProperty({
    type: VoidOrderFields,
    example: {
      order_status: 'VOID',
      id: 'qMePYBBz'
    }
  })
  data: VoidOrderFields
}

export class MoveTableResponse extends ResponseDto<MoveTableFields> {
  @ApiProperty({
    type: MoveTableFields,
    example: {
      table: {
        table_no: 1,
        id: '631280e1d8aafae7cae2452b'
      },
      id: 'HS9SqQB2'
    }
  })
  data: MoveTableFields
}
