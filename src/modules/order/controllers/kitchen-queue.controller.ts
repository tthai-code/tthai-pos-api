import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { ChangeItemStatusDto } from '../dto/change-item-status.dto'
import { PaginateKitchenQueue } from '../dto/get-kitchen.dto'
import { UpdateQueueStatusDto } from '../dto/update-queue-status.dto'
import {
  PaginateKitchenQueueResponse,
  UpdateItemStatusResponse
} from '../entity/kitchen.entity'
import { KitchenQueueLogic } from '../logics/kitchen-queue.logic'
import { KitchenQueueService } from '../services/kitchen-queue.service'
import { OrderItemsService } from '../services/order-items.service'

@ApiBearerAuth()
@ApiTags('pos/kitchen')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/kitchen')
export class KitchenQueueController {
  constructor(
    private readonly kitchenQueueService: KitchenQueueService,
    private readonly kitchenQueueLogic: KitchenQueueLogic,
    private readonly orderItemService: OrderItemsService
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaginateKitchenQueueResponse })
  @ApiOperation({
    summary: 'Get Paginate Kitchen Queue',
    description: 'use *bearer* `staff_token`'
  })
  async getKitchenQueues(@Query() query: PaginateKitchenQueue) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.kitchenQueueLogic.getPaginateKitchenQueue(
      query.buildQuery(restaurantId),
      query
    )
  }

  @Patch(':orderItemId/item')
  @ApiOkResponse({ type: () => UpdateItemStatusResponse })
  @ApiOperation({
    summary: 'Change Item Status',
    description:
      'use *bearer* `staff_token`\n\nchange order item status by order item id'
  })
  async updateItemStatus(
    @Param('orderItemId') orderItemId: string,
    @Body() payload: ChangeItemStatusDto
  ) {
    const updated = await this.orderItemService.update(orderItemId, {
      itemStatus: payload.itemStatus
    })
    if (!updated) throw new BadRequestException()
    return { success: true }
  }

  @Patch(':kitchenQueueId')
  @ApiOperation({
    summary: 'Update Kitchen Queue Status',
    description:
      'use *bearer* `staff_token`\n\nchange queue status by kitchen queue id'
  })
  async updateQueueStatus(
    @Param('kitchenQueueId') kitchenQueueId: string,
    @Body() payload: UpdateQueueStatusDto
  ) {
    return await this.kitchenQueueService.update(kitchenQueueId, payload)
  }
}
