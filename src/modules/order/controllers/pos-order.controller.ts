import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Request,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { Roles } from 'src/decorators/role.decorator'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { StaffRoleEnum } from 'src/modules/staff/common/staff-role.enum'
import {
  CreateOrderDineInPOSDto,
  CreateOrderToGoPOSDto
} from '../dto/create-order-pos.dto'
import { GetOrderInfoDto } from '../dto/get-order-info.dto'
import { ToGoOrderPaginateDto } from '../dto/get-order.dto'
import { MoveOrderTableDto } from '../dto/move-order-table.dto'
import { SummaryOrderDto } from '../dto/summary-order.dto'
import { UpdateExistOrderItemsDto } from '../dto/update-exist-items.dto'
import { UpdateOrderItemsDto } from '../dto/update-items.dto'
import {
  CreateOrderDineInResponse,
  CreateOrderToGoResponse
} from '../entity/create-order.entity'
import {
  GetOrderByTableIdResponse,
  GetToGoOrderByIdResponse,
  PaginateToGoOrderResponse
} from '../entity/get-order.entity'
import { MoveTableResponse, VoidOrderResponse } from '../entity/order.entity'
import {
  AddNewOrderItemsResponse,
  DeleteOrderItemResponse,
  SummaryOrderItemsResponse,
  UpdateExistOrderItemsResponse
} from '../entity/update-order.entity'
import { OrderItemsLogic } from '../logics/order-items.logic'
import { OrderLogic } from '../logics/order.logic'
import { POSOrderLogic } from '../logics/pos-order.logic'
import { OrderService } from '../services/order.service'

@ApiBearerAuth()
@ApiTags('pos/order')
@UseGuards(StaffAuthGuard)
@Controller('/v1/pos/order')
export class POSOrderController {
  constructor(
    private readonly orderService: OrderService,
    private readonly orderLogic: OrderLogic,
    private readonly orderItemLogic: OrderItemsLogic,
    private readonly posOrderLogic: POSOrderLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Post('dine-in')
  @ApiCreatedResponse({ type: () => CreateOrderDineInResponse })
  @ApiOperation({
    summary: 'Create Dine In Order POS',
    description: 'use *bearer* `staff_token`\n\nDine in Order'
  })
  async createOrderDineInPOS(
    @Body() payload: CreateOrderDineInPOSDto,
    @Request() request: any
  ) {
    const staffId = StaffAuthGuard.getAuthorizedUser()?.id
    const created = await this.orderLogic.createOrderDineInPOS(staffId, payload)
    await this.logLogic.createLogic(
      request,
      'Create Dine In Order POS',
      created
    )
    return created
  }

  @Post('to-go')
  @ApiCreatedResponse({ type: () => CreateOrderToGoResponse })
  @ApiOperation({
    summary: 'Create To Go Order POS',
    description: 'use *bearer* `staff_token`\n\nTO Go Order'
  })
  async createOrderToGoPOS(
    @Body() payload: CreateOrderToGoPOSDto,
    @Request() request: any
  ) {
    const staffId = StaffAuthGuard.getAuthorizedUser()?.id
    const created = await this.orderLogic.createOrderToGoPOS(staffId, payload)
    await this.logLogic.createLogic(request, 'Create To Go Order POS', created)
    return created
  }

  @Get(':tableId/table')
  @ApiOkResponse({ type: () => GetOrderByTableIdResponse })
  @ApiOperation({
    summary: 'Get Dine In Order By Table ID',
    description:
      'use *bearer* `staff_token`\n\nGet dine-in order by table id while table is seating'
  })
  async getOrderByTable(@Param('tableId') tableId: string) {
    return await this.orderLogic.getOrderByTable(tableId)
  }

  @Get(':orderId/to-go')
  @ApiOkResponse({ type: () => GetToGoOrderByIdResponse })
  @ApiOperation({
    summary: 'Get To Go Order By ID',
    description: 'use *bearer* `staff_token`\n\nGet to-go order by order id'
  })
  async getOrderToGoById(@Param('orderId') orderId: string) {
    return await this.orderLogic.getOrderToGoById(orderId)
  }

  @Get('to-go')
  @ApiOkResponse({ type: () => PaginateToGoOrderResponse })
  @ApiOperation({
    summary: 'Get Paginate To Go Order',
    description: 'use *bearer* `staff_token`\n\nGet paginate list to-go order'
  })
  async getPaginateToGoOrder(@Query() query: ToGoOrderPaginateDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.orderLogic.getPaginateOrderToGo(
      query.buildQuery(restaurantId),
      query
    )
  }

  @Patch(':orderId/void')
  @Roles(StaffRoleEnum.MANAGER)
  @ApiOkResponse({ type: () => VoidOrderResponse })
  @ApiOperation({
    summary: 'Void Order by ID',
    description:
      'use *bearer* `staff_token`\n\nrole manager can void order by id'
  })
  async voidOrderById(
    @Param('orderId') orderId: string,
    @Request() request: any
  ) {
    const voidOrder = await this.orderLogic.voidOrderById(orderId)
    await this.logLogic.createLogic(request, 'Void Order By ID', voidOrder)
    return voidOrder
  }

  @Patch(':orderId/move-table')
  @ApiOkResponse({ type: () => MoveTableResponse })
  @ApiOperation({
    summary: 'Move Table Order By ID',
    description: 'use *bearer* `staff_token`\n\nmove table by order id'
  })
  async moveOrderTableById(
    @Param('orderId') orderId: string,
    @Body() payload: MoveOrderTableDto,
    @Request() request: any
  ) {
    const moved = await this.orderLogic.moveOrderTable(orderId, payload)
    await this.logLogic.createLogic(request, 'Move Table Order By ID', moved)
    return moved
  }

  @Patch(':orderId/items')
  @ApiOkResponse({ type: () => AddNewOrderItemsResponse })
  @ApiOperation({
    summary: 'Add new order items by order id',
    description: 'use *bearer* `staff_token`\n\nadd new order items by order id'
  })
  async addNewOrderItemsById(
    @Param('orderId') orderId: string,
    @Body() payload: UpdateOrderItemsDto,
    @Request() request: any
  ) {
    const updated = await this.orderLogic.updateNewOrderItems(orderId, payload)
    await this.logLogic.createLogic(
      request,
      'Add new order items by order id',
      updated
    )
    return updated
  }

  @Patch(':orderItemId/order-item')
  @ApiOkResponse({ type: () => UpdateExistOrderItemsResponse })
  @ApiOperation({
    summary: 'update exist order item',
    description:
      'use *bearer* `staff_token`\n\nupdate exist order items by order item id'
  })
  async updateExistOrderItem(
    @Param('orderItemId') orderItemId: string,
    @Body() payload: UpdateExistOrderItemsDto,
    @Request() request: any
  ) {
    const updated = await this.orderItemLogic.updateExistItems(
      orderItemId,
      payload
    )
    await this.logLogic.createLogic(request, 'update exist order item', updated)
    return updated
  }

  @Delete(':orderItemId/order-item')
  @Roles(StaffRoleEnum.MANAGER)
  @ApiOkResponse({ type: () => DeleteOrderItemResponse })
  @ApiOperation({
    summary: 'delete/cancel order item',
    description:
      'use *bearer* `staff_token`\n\ndelete/cancel order items by order item id'
  })
  async deleteOrderItem(
    @Param('orderItemId') orderItemId: string,
    @Request() request: any
  ) {
    const deleted = await this.orderItemLogic.removeItem(orderItemId)
    await this.logLogic.createLogic(
      request,
      'delete/cancel order item',
      deleted
    )
    return deleted
  }

  @Post(':orderId/summary')
  @ApiOkResponse({ type: () => SummaryOrderItemsResponse })
  @ApiOperation({
    summary: 'summary order items',
    description: 'use *bearer* `staff_token`\n\nsummary order items by order id'
  })
  async summaryOrderItems(
    @Param('orderId') orderId: string,
    @Body() payload: SummaryOrderDto,
    @Request() request: any
  ) {
    const summary = await this.orderItemLogic.summaryOrder(orderId, payload)
    await this.logLogic.createLogic(request, 'summary order items', summary)
    return summary
  }

  @Get(':orderId/info')
  @ApiOkResponse({ type: () => GetOrderByTableIdResponse })
  @ApiOperation({
    summary: 'get order info by order id',
    description: 'use *bearer* `staff_token`'
  })
  async getOrderInfoByID(
    @Param('orderId') id: string,
    @Query() query: GetOrderInfoDto
  ) {
    const order = await this.posOrderLogic.getOrderInfo(id, query.orderType)
    return order
  }
}
