import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Request,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'

import { CustomerAuthGuard } from 'src/modules/auth/guards/customer-auth.guard'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { CreateOrderOnline } from '../dto/create-order-pos.dto'
import { OrderOnlineCustomerPaginateDto } from '../dto/get-order-online.dto'
import { PayOnlineOrderDto } from '../dto/pay-online-order.dto'

import { CreateOrderCheckoutInResponse } from '../entity/create-order.entity'
import { GetToGoOrderByIdResponse } from '../entity/get-order.entity'
import { CreateOnlineOrderResponse } from '../entity/online-order.entity'

import { OrderOnlineLogic } from '../logics/order-online.logic'

@ApiBearerAuth()
@ApiTags('order-online')
@UseGuards(CustomerAuthGuard)
@Controller('v1/public/online-order')
export class OrderOnlineController {
  constructor(
    private readonly orderOnlineLogic: OrderOnlineLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Post('checkout')
  @ApiCreatedResponse({ type: () => CreateOnlineOrderResponse })
  @ApiOperation({
    summary: 'Create Order-Online status NEW',
    description: 'use *bearer* `customer_token`\n\n NEW Order Online'
  })
  async createOrderOnline(
    @Body() payload: CreateOrderOnline,
    @Request() request: any
  ) {
    const customerId = CustomerAuthGuard.getAuthorizedUser()?.id
    const orders = await this.orderOnlineLogic.createOrderOnline(
      customerId,
      payload
    )
    await this.logLogic.createLogic(
      request,
      'Create Order-Online status NEW',
      orders
    )
    return orders
  }

  @Patch(':orderId/paid')
  @ApiCreatedResponse({ type: () => CreateOrderCheckoutInResponse })
  @ApiOperation({
    summary: 'Update Order-Online Pay status Pay and Create Orders',
    description: 'use *bearer* `customer_token`\n\n Pay Order Online'
  })
  async updatePaidOrderAndCreateOrderById(
    @Param('orderId') orderId: string,
    @Body() payload: PayOnlineOrderDto,
    @Request() request: any
  ) {
    const order =
      await this.orderOnlineLogic.updatePaidOrderOnlineAndCreateOrderById(
        orderId,
        payload
      )
    await this.logLogic.createLogic(
      request,
      'Update Order-Online Pay status Pay and Create Orders',
      order
    )
    return order
  }

  @Get('history')
  @ApiOkResponse({ type: () => GetToGoOrderByIdResponse })
  @ApiOperation({
    summary: 'Get Order All',
    description: 'use *bearer* `customer_token`\n\nGet order All'
  })
  async getOrderAll(@Query() query: OrderOnlineCustomerPaginateDto) {
    const customerId = CustomerAuthGuard.getAuthorizedUser()?.id
    return await this.orderOnlineLogic.getOrderOnlineAll(customerId, query)
  }

  @Get(':orderId')
  @ApiOkResponse({ type: () => GetToGoOrderByIdResponse })
  @ApiOperation({
    summary: 'Get Order By ID',
    description: 'use *bearer* `customer_token`\n\nGet order by order id'
  })
  async getOrderToGoById(@Param('orderId') orderId: string) {
    return await this.orderOnlineLogic.getOrderOnlineById(orderId)
  }
}
