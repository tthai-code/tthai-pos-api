import {
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  UseGuards
} from '@nestjs/common'
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderLogic } from '../logics/order.logic'
import { OrderService } from '../services/order.service'

@ApiBearerAuth()
@ApiTags('orders')
@Controller('v1/orders')
@UseGuards(AdminAuthGuard)
export class OrderController {
  constructor(
    private readonly orderService: OrderService,
    private readonly orderLogic: OrderLogic
  ) {}

  @Get(':tableId/table')
  @ApiOperation({ summary: 'Get Order By Seated Table ID' })
  async getOrderByTable(@Param('tableId') tableId: string) {
    const order = await this.orderService.findOne({
      'table.id': tableId,
      status: StatusEnum.ACTIVE,
      orderStatus: OrderStateEnum.PENDING
    })
    if (!order) throw new NotFoundException()
    return order
  }

  @Patch(':id/kitchen')
  @ApiOperation({ summary: 'Print Receipt Kitchen' })
  async printKitchen(@Param('id') id: string) {
    const order = await this.orderService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE,
      orderStatus: OrderStateEnum.PENDING
    })
    if (!order) throw new NotFoundException('Order not found.')
    return await this.orderService.update(id, { printKitchenDate: new Date() })
  }

  @Patch(':id/invoice')
  @ApiOperation({ summary: 'Print Invoice' })
  async printInvoice(@Param('id') id: string) {
    const order = await this.orderService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE,
      orderStatus: OrderStateEnum.PENDING
    })
    if (!order) throw new NotFoundException('Order not found.')
    return await this.orderService.update(id, { printInvoiceDate: new Date() })
  }
}
