import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Query,
  Request,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { AcceptOrderOnlineDto } from '../dto/accept-order-online.dto'
import { OrderOnlinePaginateDto } from '../dto/get-order-online.dto'
import { GetOnlineOrderByIdForPOSResponse } from '../entity/pos-get-order.entity'
import { OrderStateEnum } from '../enum/order-state.enum'
import { OrderOnlineLogic } from '../logics/order-online.logic'
import { POSOnlineOrderLogic } from '../logics/pos-online-order.logic'
import { OrderOnlineService } from '../services/order-online.service'

@ApiBearerAuth()
@ApiTags('pos/online-order')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/online-order')
export class POSOnlineOrderController {
  constructor(
    private readonly orderOnlineLogic: OrderOnlineLogic,
    private readonly orderOnlineService: OrderOnlineService,
    private readonly posOnlineOrderLogic: POSOnlineOrderLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Get()
  @ApiOperation({
    summary: 'Get order online list',
    description: 'use bearer `staff_token`'
  })
  async getAllOrder(
    @Query() query: OrderOnlinePaginateDto,
    @Request() request: any
  ) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const orders = await this.posOnlineOrderLogic.getAllOrder(
      restaurantId,
      query
    )
    await this.logLogic.createLogic(request, 'Get order online list', orders)
    return orders
  }

  @Get(':orderId')
  @ApiOkResponse({ type: () => GetOnlineOrderByIdForPOSResponse })
  @ApiOperation({
    summary: 'Get Online Order Detail by ID',
    description: 'use bearer `staff_token`'
  })
  async getOrderDetail(@Param('orderId') orderId: string) {
    return await this.orderOnlineLogic.getOrderOnlineByIdForPOS(orderId)
  }

  @Patch('in-progress')
  @ApiOperation({
    summary: 'Update online order status to In progress',
    description: 'use bearer `staff_token`'
  })
  async acceptOrder(@Body() payload: AcceptOrderOnlineDto) {
    return await this.orderOnlineLogic.acceptOrder(payload.orderId)
  }

  @Patch('cancel')
  @ApiOperation({
    summary: 'Cancel online order',
    description: 'use bearer `staff_token`'
  })
  async cancelOrder(@Body() payload: AcceptOrderOnlineDto) {
    return await this.orderOnlineLogic.rejectOrder(payload.orderId)
  }

  @Patch('ready')
  @ApiOperation({
    summary: 'Ready online order',
    description: 'use bearer `staff_token`'
  })
  async readyOrder(@Body() payload: AcceptOrderOnlineDto) {
    await this.orderOnlineService.update(payload.orderId, {
      orderStatus: OrderStateEnum.READY
    })
    return { success: true }
  }

  @Patch('upcoming')
  @ApiOperation({
    summary: 'Upcoming online order',
    description: 'use bearer `staff_token`'
  })
  async upcomingOrder(@Body() payload: AcceptOrderOnlineDto) {
    await this.orderOnlineService.update(payload.orderId, {
      orderStatus: OrderStateEnum.UPCOMING
    })
    return { success: true }
  }

  @Patch('complete')
  @ApiOperation({
    summary: 'Complete online order',
    description: 'use bearer `staff_token`'
  })
  async completeOrder(@Body() payload: AcceptOrderOnlineDto) {
    return await this.orderOnlineLogic.completeOrder(payload.orderId)
  }
}
