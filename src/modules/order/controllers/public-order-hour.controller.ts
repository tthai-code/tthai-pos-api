import { Controller, Get, NotFoundException, Param } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CheckOrderHourResponse } from '../entity/order-hour.entity'
import { OrderHourLogic } from '../logics/order-hour.logic'

@ApiTags('public/order-hour')
@Controller('v1/public/order-hour')
export class PublicOrderHourController {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly orderHourLogic: OrderHourLogic
  ) {}

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => CheckOrderHourResponse })
  @ApiOperation({ summary: 'Check Available Restaurant' })
  async checkHour(@Param('restaurantId') restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const checkOpenHour = await this.orderHourLogic.checkAvailableOrder(
      restaurant
    )
    return { isAvailable: checkOpenHour }
  }
}
