import { Module, forwardRef } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { OrderController } from './controllers/order.controller'
import { OrderOnlineController } from './controllers/order-public-online.controller'

import { OrderLogic } from './logics/order.logic'
import { OrderOnlineLogic } from './logics/order-online.logic'

import { OrderCounterSchema } from './schemas/order-counter.schema'
import { OrderSchema } from './schemas/order.schema'
import { OrderOnlineSchema } from './schemas/order-online.schema'

import { OrderCounterService } from './services/order-counter.service'
import { OrderService } from './services/order.service'
import { OrderOnlineService } from './services/order-online.service'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { StaffModule } from '../staff/staff.module'
import { TableModule } from '../table/table.module'
import { KitchenQueueService } from './services/kitchen-queue.service'
import { POSOrderController } from './controllers/pos-order.controller'
import { KitchenQueueSchema } from './schemas/kitchen-queue.schema'
import { KitchenCounterService } from './services/kitchen-counter.service'
import { KitchenCounterSchema } from './schemas/kitchen-counter.shcema'
import { CustomerModule } from '../customer/customer.module'
import { OrderItemsService } from './services/order-items.service'
import { OrderItemsSchema } from './schemas/order-items.schema'
import { OrderItemsLogic } from './logics/order-items.logic'
import { KitchenQueueController } from './controllers/kitchen-queue.controller'
import { KitchenQueueLogic } from './logics/kitchen-queue.logic'
import { ThirdPartyOrderService } from './services/third-party-order.service'
import { ThirdPartyOrderSchema } from './schemas/third-party-order.schema'
import { SocketModule } from '../socket/socket.module'
import { POSOnlineOrderController } from './controllers/pos-online-order.controller'
import { PaymentGatewayModule } from '../payment-gateway/payment-gateway.module'
import { TransactionModule } from '../transaction/transaction.module'
import { POSOnlineOrderLogic } from './logics/pos-online-order.logic'
import { WebSettingModule } from '../web-setting/web-setting.module'
import { CashDrawerModule } from '../cash-drawer/cash-drawer.module'
import { OrderHourLogic } from './logics/order-hour.logic'
import { PublicOrderHourController } from './controllers/public-order-hour.controller'
import { POSOrderLogic } from './logics/pos-order.logic'

@Module({
  imports: [
    CustomerModule,
    StaffModule,
    TableModule,
    RestaurantModule,
    PaymentGatewayModule,
    MongooseModule.forFeature([
      { name: 'order', schema: OrderSchema },
      { name: 'orderOnline', schema: OrderOnlineSchema },
      { name: 'order-counter', schema: OrderCounterSchema },
      { name: 'kitchenQueue', schema: KitchenQueueSchema },
      { name: 'kitchen-counter', schema: KitchenCounterSchema },
      { name: 'orderItems', schema: OrderItemsSchema },
      { name: 'thirdPartyOrder', schema: ThirdPartyOrderSchema }
    ]),
    forwardRef(() => SocketModule),
    forwardRef(() => PaymentGatewayModule),
    forwardRef(() => TransactionModule),
    forwardRef(() => WebSettingModule),
    forwardRef(() => CashDrawerModule)
  ],
  providers: [
    OrderService,
    OrderOnlineService,
    OrderLogic,
    OrderOnlineLogic,
    OrderCounterService,
    OrderItemsService,
    KitchenQueueService,
    KitchenCounterService,
    KitchenQueueLogic,
    OrderItemsLogic,
    ThirdPartyOrderService,
    POSOnlineOrderLogic,
    OrderHourLogic,
    POSOrderLogic
  ],
  controllers: [
    OrderController,
    OrderOnlineController,
    POSOrderController,
    KitchenQueueController,
    POSOnlineOrderController,
    PublicOrderHourController
  ],
  exports: [
    OrderService,
    ThirdPartyOrderService,
    KitchenQueueService,
    KitchenCounterService,
    OrderItemsService,
    OrderOnlineService,
    OrderItemsLogic
  ]
})
export class OrderModule {}
