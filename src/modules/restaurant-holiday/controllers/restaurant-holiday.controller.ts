import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { UpdateHolidaysDto } from '../dto/update-holiday.dto'
import { getHolidaysResponse } from '../response/get-holidays.reponse'
import { RestaurantHolidaysService } from '../services/restaurant-holiday.service'

@ApiBearerAuth()
@ApiTags('restaurant-holidays')
@UseGuards(AdminAuthGuard)
@Controller('v1/restaurant')
export class RestaurantHolidaysController {
  constructor(
    private readonly restaurantHolidaysService: RestaurantHolidaysService,
    private readonly restaurantService: RestaurantService
  ) {}

  @Get(':restaurantId/holiday')
  @ApiOkResponse(getHolidaysResponse)
  @ApiOperation({ summary: 'Get Restaurant Holidays by Restaurant ID' })
  async getHolidaysByRestaurantID(@Param('restaurantId') id: string) {
    const restaurantId = await this.restaurantService.findOne({ _id: id })
    if (!restaurantId) throw new NotFoundException('Not Found Restaurant.')
    return await this.restaurantHolidaysService.findOne({ restaurantId: id })
  }

  @Patch(':restaurantId/holiday')
  @ApiOperation({ summary: 'Update Restaurant Holidays by Restaurant ID' })
  async updateHolidaysByRestaurantID(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateHolidaysDto
  ) {
    const restaurantId = await this.restaurantService.findOne({ _id: id })
    if (!restaurantId) throw new NotFoundException('Not Found Restaurant.')
    return await this.restaurantHolidaysService.findOneAndUpdate(
      { restaurantId: id },
      payload
    )
  }
}
