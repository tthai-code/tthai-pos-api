import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantHolidaysDocument } from '../schemas/restaurant-holiday.schema'

@Injectable({ scope: Scope.REQUEST })
export class RestaurantHolidaysService {
  constructor(
    @InjectModel('restaurantHolidays')
    private readonly RestaurantHolidaysModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<RestaurantHolidaysDocument | any> {
    return this.RestaurantHolidaysModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any
  ): Promise<RestaurantHolidaysDocument | any> {
    return this.RestaurantHolidaysModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.RestaurantHolidaysModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<RestaurantHolidaysDocument> {
    return this.RestaurantHolidaysModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.RestaurantHolidaysModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.RestaurantHolidaysModel.createCollection()

    return this.RestaurantHolidaysModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<RestaurantHolidaysDocument> {
    const document = new this.RestaurantHolidaysModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<RestaurantHolidaysDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<RestaurantHolidaysDocument> {
    const document = new this.RestaurantHolidaysModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<RestaurantHolidaysDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<RestaurantHolidaysDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<RestaurantHolidaysDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.RestaurantHolidaysModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.RestaurantHolidaysModel.findById(id)
  }

  findOne(condition: any): Promise<RestaurantHolidaysDocument> {
    return this.RestaurantHolidaysModel.findOne(condition)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<RestaurantHolidaysDocument> {
    return this.RestaurantHolidaysModel.findOne(condition, options)
  }
}
