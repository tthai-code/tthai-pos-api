import { forwardRef, Global, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { RestaurantHolidaysController } from './controllers/restaurant-holiday.controller'
import { RestaurantHolidaysSchema } from './schemas/restaurant-holiday.schema'
import { RestaurantHolidaysService } from './services/restaurant-holiday.service'

@Global()
@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([
      { name: 'restaurantHolidays', schema: RestaurantHolidaysSchema }
    ])
  ],
  providers: [RestaurantHolidaysService],
  controllers: [RestaurantHolidaysController],
  exports: [RestaurantHolidaysService]
})
export class RestaurantHolidaysModule {}
