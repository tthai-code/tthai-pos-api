import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type RestaurantHolidaysDocument = RestaurantHolidaysField & Document

@Schema({ _id: false, timestamps: false, strict: true })
class PeriodFieldsSchema {
  @Prop()
  opensAt: string

  @Prop()
  closesAt: string
}

@Schema({ _id: false, timestamps: false, strict: true })
class HolidaysFieldsSchema {
  @Prop()
  date: string

  @Prop()
  period: Array<PeriodFieldsSchema>

  @Prop()
  isClosed: boolean
}

@Schema({ timestamps: true, strict: true, collection: 'restaurantHolidays' })
export class RestaurantHolidaysField {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop()
  holidays: Array<HolidaysFieldsSchema>

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const RestaurantHolidaysSchema = SchemaFactory.createForClass(
  RestaurantHolidaysField
)
RestaurantHolidaysSchema.plugin(mongoosePaginate)
RestaurantHolidaysSchema.plugin(authorStampCreatePlugin)
