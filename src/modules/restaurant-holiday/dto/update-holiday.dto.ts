import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsBoolean,
  IsNotEmpty,
  IsString,
  ValidateNested
} from 'class-validator'

class PeriodFieldsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '09:00AM' })
  readonly opensAt: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '02:00PM' })
  readonly closesAt: string
}

class HolidayFieldsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '31-10-2022' })
  readonly date: string

  @Type(() => PeriodFieldsDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [PeriodFieldsDto] })
  readonly period: Array<PeriodFieldsDto>

  @IsBoolean()
  @ApiProperty({ example: false })
  readonly isClosed: boolean
}

export class UpdateHolidaysDto {
  @Type(() => HolidayFieldsDto)
  @ValidateNested({ each: true })
  @IsNotEmpty()
  @ApiProperty({ type: () => [HolidayFieldsDto] })
  readonly holidays: Array<HolidayFieldsDto>
}
