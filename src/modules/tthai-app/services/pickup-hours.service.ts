import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PickupHoursDocument } from '../schemas/pickup-hours.schema'

@Injectable({ scope: Scope.REQUEST })
export class PickupHoursService {
  constructor(
    @InjectModel('pickupHours')
    private readonly PickupHoursModel: Model<PickupHoursDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<PickupHoursDocument | any> {
    return this.PickupHoursModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any,
    project?: any
  ): Promise<PickupHoursDocument | any> {
    return this.PickupHoursModel.find(condition, project)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.PickupHoursModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<PickupHoursDocument> {
    return this.PickupHoursModel
  }

  async getSession(): Promise<ClientSession> {
    await this.PickupHoursModel.createCollection()

    return this.PickupHoursModel.startSession()
  }

  async create(payload: any): Promise<PickupHoursDocument> {
    const document = new this.PickupHoursModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<PickupHoursDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<PickupHoursDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  async createMany(payload: any): Promise<PickupHoursDocument> {
    return this.PickupHoursModel.insertMany(payload)
  }

  async deleteMany(condition: any): Promise<void> {
    return this.PickupHoursModel.deleteMany(condition)
  }

  async transactionDeleteAll(condition, session): Promise<void> {
    return this.PickupHoursModel.deleteMany(condition, { session })
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.PickupHoursModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.PickupHoursModel.findById(id)
  }

  findOne(condition: any): Promise<PickupHoursDocument> {
    return this.PickupHoursModel.findOne(condition)
  }
}
