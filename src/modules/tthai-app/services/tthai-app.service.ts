import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { TThaiAppDocument } from '../schemas/tthai-app.schema'

@Injectable({ scope: Scope.REQUEST })
export class TThaiAppService {
  constructor(
    @InjectModel('tthaiApp')
    private readonly TThaiAppModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<TThaiAppDocument | any> {
    return this.TThaiAppModel.findById({ _id: id })
  }

  async resolveByCondition(condition: any): Promise<TThaiAppDocument | any> {
    return this.TThaiAppModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.TThaiAppModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<TThaiAppDocument> {
    return this.TThaiAppModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.TThaiAppModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.TThaiAppModel.createCollection()

    return this.TThaiAppModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<TThaiAppDocument> {
    const document = new this.TThaiAppModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<TThaiAppDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<TThaiAppDocument> {
    const document = new this.TThaiAppModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<TThaiAppDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<TThaiAppDocument> {
    const document = await this.resolveByCondition(condition)

    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<TThaiAppDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.TThaiAppModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.TThaiAppModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<TThaiAppDocument> {
    return this.TThaiAppModel.findOne(condition, project)
  }

  findOneWithSelect(condition: any, options?: any): Promise<TThaiAppDocument> {
    return this.TThaiAppModel.findOne(condition, options)
  }
}
