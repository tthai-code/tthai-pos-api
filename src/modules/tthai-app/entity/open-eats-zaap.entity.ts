import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class PeriodItemsObject {
  @ApiProperty()
  opens_at: string
  @ApiProperty()
  closes_at: string
}

class PickupHoursObject {
  @ApiProperty()
  label: string
  @ApiProperty()
  is_closed: boolean
  @ApiProperty({ type: [PeriodItemsObject] })
  period: PeriodItemsObject[]
}

class CustomizeInAppTextObject {
  @ApiProperty()
  item_instructions_desc: string
  @ApiProperty()
  item_instructions_label: string
  @ApiProperty()
  delivery_order_instructions_label: string
  @ApiProperty()
  order_instructions_desc: string
  @ApiProperty()
  pick_up_order_instructions_label: string
}

class WaitTimeObject {
  @ApiProperty()
  per_item_time: number
  @ApiProperty()
  per_order_time: number
  @ApiProperty()
  base_time: number
}

class EatsZaabObject {
  @ApiProperty({ type: CustomizeInAppTextObject })
  customize_in_app_text: CustomizeInAppTextObject
  @ApiProperty()
  is_item_special_instructions: boolean
  @ApiProperty({ type: WaitTimeObject })
  wait_time: WaitTimeObject
  @ApiProperty({ type: [PickupHoursObject] })
  pickup_hours: PickupHoursObject[]
  @ApiProperty()
  is_mobile_ordering: boolean
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
}

export class GetEatsZaabResponse extends ResponseDto<EatsZaabObject> {
  @ApiProperty({
    type: EatsZaabObject,
    example: {
      customize_in_app_text: {
        item_instructions_desc: 'TEST',
        item_instructions_label: 'Special Instructions',
        delivery_order_instructions_label:
          'All deliveries provided by in-house and third-party drivers.',
        order_instructions_desc:
          'Please inform us if you have any dietary restrictions.',
        pick_up_order_instructions_label:
          'Please come pick up order inside the restuarant.'
      },
      is_item_special_instructions: false,
      wait_time: {
        per_item_time: 0,
        per_order_time: 10,
        base_time: 10
      },
      pickup_hours: [
        {
          label: 'MONDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'TUESDAY',
          is_closed: false,
          period: [
            {
              opens_at: '11:00',
              closes_at: '23:30'
            }
          ]
        },
        {
          label: 'WEDNESDAY',
          is_closed: false,
          period: [
            {
              opens_at: '15:00',
              closes_at: '12:03'
            }
          ]
        },
        {
          label: 'THURSDAY',
          is_closed: false,
          period: [
            {
              opens_at: '12:00',
              closes_at: '23:00'
            }
          ]
        },
        {
          label: 'FRIDAY',
          is_closed: false,
          period: [
            {
              opens_at: '00:00',
              closes_at: '23:59'
            }
          ]
        },
        {
          label: 'SATURDAY',
          is_closed: false,
          period: [
            {
              opens_at: '05:23',
              closes_at: '12:03'
            },
            {
              opens_at: '22:23',
              closes_at: '23:00'
            }
          ]
        },
        {
          label: 'SUNDAY',
          is_closed: true,
          period: []
        }
      ],
      is_mobile_ordering: false,
      restaurant_id: '630eff5751c2eac55f52662c',
      id: '634cbcc025606c127758c48e'
    }
  })
  data: EatsZaabObject
}
