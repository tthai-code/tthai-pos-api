import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'
import { DayEnum } from 'src/modules/restaurant-hours/common/day.enum'

class PeriodFieldsResponse {
  @ApiProperty({ type: String })
  opensAt: string

  @ApiProperty({ type: String })
  closesAt: string
}

class PickUpHoursFieldsResponse {
  @ApiProperty({ enum: Object.values(DayEnum) })
  readonly label: string

  @ApiProperty({ example: false })
  readonly isClosed: boolean

  @ApiProperty({ type: () => [PeriodFieldsResponse] })
  readonly period: Array<PeriodFieldsResponse>
}

class WaitTimeResponse {
  @ApiProperty()
  baseTime: number

  @ApiProperty()
  perOrderTime: number

  @ApiProperty()
  perItemTime: number
}

class CustomizeInAppTextResponse {
  @ApiProperty()
  pickUpOrderInstructionsLabel: string

  @ApiProperty()
  orderInstructionsDesc: string

  @ApiProperty()
  deliveryOrderInstructionsLabel: string

  @ApiProperty()
  itemInstructionsLabel: string

  @ApiProperty()
  itemInstructionsDesc: string
}

export class TThaiAppResponseFields extends TimestampResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  restaurantId: string

  @ApiProperty()
  isMobileOrdering: boolean

  @ApiProperty()
  isItemSpecialInstructions: boolean

  @ApiProperty({ type: PickUpHoursFieldsResponse })
  pickupHours: PickUpHoursFieldsResponse

  @ApiProperty({ type: WaitTimeResponse })
  waitTime: WaitTimeResponse

  @ApiProperty({ type: CustomizeInAppTextResponse })
  customizeInAppText: CustomizeInAppTextResponse
}

export class TThaiAppResponse extends ResponseDto<TThaiAppResponseFields> {
  @ApiProperty({
    type: TThaiAppResponseFields,
    example: {
      status: 'active',
      customize_in_app_text: {
        item_instructions_desc: 'Item Desc Test',
        item_instructions_label: 'Item Label Test',
        delivery_order_instructions_label: 'Test Label',
        order_instructions_desc: 'Test Desc',
        pick_up_order_instructions_label: 'pickUpOrderInstructionsLabel Test'
      },
      is_item_special_instructions: false,
      wait_time: {
        per_item_time: 15,
        per_order_time: 15,
        base_time: 15
      },
      pickup_hours: [
        {
          label: 'MONDAY',
          is_closed: false,
          period: [
            {
              opens_at: '09:00AM',
              closes_at: '12:00AM'
            }
          ]
        },
        {
          label: 'TUESDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'WEDNESDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'THURSDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'FRIDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'SATURDAY',
          is_closed: true,
          period: []
        },
        {
          label: 'SUNDAY',
          is_closed: true,
          period: []
        }
      ],
      is_mobile_ordering: true,
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-10-11T09:56:47.331Z',
      updated_at: '2022-10-11T10:42:14.119Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '63453ddf7fd721e55848b038'
    }
  })
  data: TThaiAppResponseFields
}
