import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantHoursModule } from '../restaurant-hours/restaurant-hours.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { POSEatsZaabController } from './controllers/pos-eats-zaap.controller'
import { TThaiAppController } from './controllers/tthai-app.controller'
import { PickupHoursLogic } from './logics/pickup-hours.logic'
import { POSEatsZaabLogic } from './logics/pos-eats-zaap.logic'
import { TThaiAppLogic } from './logics/tthai-app.logic'
import { PickupHoursSchema } from './schemas/pickup-hours.schema'
import { TThaiAppSchema } from './schemas/tthai-app.schema'
import { PickupHoursService } from './services/pickup-hours.service'
import { TThaiAppService } from './services/tthai-app.service'

@Module({
  imports: [
    forwardRef(() => RestaurantHoursModule),
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([
      { name: 'tthaiApp', schema: TThaiAppSchema },
      { name: 'pickupHours', schema: PickupHoursSchema }
    ])
  ],
  providers: [
    TThaiAppService,
    TThaiAppLogic,
    PickupHoursService,
    PickupHoursLogic,
    POSEatsZaabLogic
  ],
  controllers: [TThaiAppController, POSEatsZaabController],
  exports: [TThaiAppService, TThaiAppLogic]
})
export class TThaiAppModule {}
