import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { DayEnum } from 'src/modules/restaurant-hours/common/day.enum'

export type TThaiAppDocument = TThaiAppFields & Document

@Schema({ _id: false, timestamps: false, strict: true })
export class PeriodFields {
  @Prop()
  opensAt: string

  @Prop()
  closesAt: string
}

@Schema({ _id: false, timestamps: false, strict: true })
export class PickHoursFields {
  @Prop({ enum: Object.values(DayEnum) })
  label: string

  @Prop({ default: true })
  isClosed: boolean

  @Prop({ default: [] })
  period: Array<PeriodFields>
}

@Schema({ _id: false, timestamps: false, strict: true })
export class WaitTimeFields {
  @Prop({ default: 0 })
  baseTime: number

  @Prop({ default: 0 })
  perOrderTime: number

  @Prop({ default: 0 })
  perItemTime: number

  @Prop({ default: 60 })
  maxTime: number
}

@Schema({ _id: false, timestamps: false, strict: true })
export class CustomizeInAppTextFields {
  @Prop({ default: 'Please come pick up order inside the restuarant.' })
  pickUpOrderInstructionsLabel: string

  @Prop({ default: 'Please inform us if you have any dietary restrictions.' })
  orderInstructionsDesc: string

  @Prop({
    default: 'All deliveries provided by in-house and third-party drivers.'
  })
  deliveryOrderInstructionsLabel: string

  @Prop({ default: 'Special Instructions' })
  itemInstructionsLabel: string

  @Prop({ default: null })
  itemInstructionsDesc: string
}

@Schema({ timestamps: true, strict: true, collection: 'tthaiApps' })
export class TThaiAppFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ default: false })
  isMobileOrdering: boolean

  @Prop()
  pickupHours: Array<PickHoursFields>

  @Prop({ default: new WaitTimeFields() })
  waitTime: WaitTimeFields

  @Prop({ default: false })
  isItemSpecialInstructions: boolean

  @Prop({ default: new CustomizeInAppTextFields() })
  customizeInAppText: CustomizeInAppTextFields

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const TThaiAppSchema = SchemaFactory.createForClass(TThaiAppFields)
TThaiAppSchema.plugin(authorStampCreatePlugin)
