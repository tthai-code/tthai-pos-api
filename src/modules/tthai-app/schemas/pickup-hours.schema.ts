import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'

export type PickupHoursDocument = PickupHoursFields & Document

@Schema({ timestamps: true, strict: true, collection: 'pickupHours' })
export class PickupHoursFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurantPeriod' })
  restaurantPeriodId: Types.ObjectId
}
export const PickupHoursSchema = SchemaFactory.createForClass(PickupHoursFields)
