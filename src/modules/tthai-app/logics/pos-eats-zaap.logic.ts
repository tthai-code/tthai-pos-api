import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { TThaiAppService } from '../services/tthai-app.service'
import { PickupHoursLogic } from './pickup-hours.logic'

@Injectable()
export class POSEatsZaabLogic {
  constructor(
    private readonly tthaiAppService: TThaiAppService,
    private readonly restaurantService: RestaurantService,
    private readonly pickupHoursLogic: PickupHoursLogic
  ) {}

  private async fetchRestaurant(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return restaurant
  }

  async getTThaiApp(restaurantId: string) {
    await this.fetchRestaurant(restaurantId)
    const tthaiApp = await this.tthaiAppService.findOne(
      {
        restaurantId,
        status: StatusEnum.ACTIVE
      },
      { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 }
    )
    if (!tthaiApp) {
      throw new BadRequestException(
        'Please Setup Eats Zaap Setting before get info.'
      )
    }
    const pickupHours = await this.pickupHoursLogic.getPickupHoursForEatsZaab(
      restaurantId
    )
    const result = { ...tthaiApp.toObject(), pickupHours }
    return result
  }
}
