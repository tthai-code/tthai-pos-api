import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import {
  UpdateCustomizeInAppTextDto,
  UpdateItemSpecialInstructionsDto,
  UpdateMobileOrderingDto,
  UpdatePickUpHoursDto,
  UpdateWaitTimeDto
} from '../dto/update-tthai-app.dto'
import { TThaiAppService } from '../services/tthai-app.service'
import { PickupHoursLogic } from './pickup-hours.logic'

@Injectable()
export class TThaiAppLogic {
  constructor(
    private readonly tthaiAppService: TThaiAppService,
    private readonly restaurantService: RestaurantService,
    private readonly pickupHoursLogic: PickupHoursLogic
  ) {}

  private initialHours = [
    {
      label: 'MONDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'TUESDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'WEDNESDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'THURSDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'FRIDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'SATURDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'SUNDAY',
      isClosed: true,
      period: []
    }
  ]

  private initTThaiAppSetting(restaurantId: string) {
    return {
      restaurantId,
      pickupHours: this.initialHours
    }
  }

  private async fetchRestaurant(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return restaurant
  }

  async getTThaiApp(restaurantId: string) {
    await this.fetchRestaurant(restaurantId)
    const tthaiApp = await this.tthaiAppService.findOne(
      {
        restaurantId,
        status: StatusEnum.ACTIVE
      },
      { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 }
    )
    if (!tthaiApp) {
      const payload = this.initTThaiAppSetting(restaurantId)
      const created = await this.tthaiAppService.create(payload)
      if (!created) throw new BadRequestException()
      const docs = await this.tthaiAppService.findOne(
        {
          restaurantId,
          status: StatusEnum.ACTIVE
        },
        { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 }
      )
      const pickupHours = await this.pickupHoursLogic.getPickupHours(
        restaurantId
      )
      const result = { ...docs.toObject(), pickupHours }
      return result
    }
    const pickupHours = await this.pickupHoursLogic.getPickupHours(restaurantId)
    const result = { ...tthaiApp.toObject(), pickupHours }
    return result
  }

  async updateMobileOrdering(
    restaurantId: string,
    payload: UpdateMobileOrderingDto
  ) {
    await this.fetchRestaurant(restaurantId)
    return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload)
  }

  async updateItemSpecialInstructions(
    restaurantId: string,
    payload: UpdateItemSpecialInstructionsDto
  ) {
    await this.fetchRestaurant(restaurantId)
    return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload)
  }

  async updatePickUpHoursDto(
    restaurantId: string,
    payload: UpdatePickUpHoursDto
  ) {
    await this.fetchRestaurant(restaurantId)
    return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload)
  }

  async updateWaitTimeDto(restaurantId: string, payload: UpdateWaitTimeDto) {
    await this.fetchRestaurant(restaurantId)
    return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload)
  }

  async updateCustomizeInAppTextDto(
    restaurantId: string,
    payload: UpdateCustomizeInAppTextDto
  ) {
    await this.fetchRestaurant(restaurantId)
    return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload)
  }
}
