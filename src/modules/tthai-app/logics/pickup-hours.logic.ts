import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service'
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { UpdatePickupHoursDto } from '../dto/update-pickup-hours.dto'
import { PickupHoursService } from '../services/pickup-hours.service'

@Injectable()
export class PickupHoursLogic {
  constructor(
    private readonly pickupHoursService: PickupHoursService,
    private readonly restaurantService: RestaurantService,
    private readonly restaurantPeriodService: RestaurantPeriodService,
    private readonly restaurantDayService: RestaurantDayService
  ) {}

  async getPickupHours(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const restaurantDays = await this.restaurantDayService.getAll(
      { restaurantId },
      { _id: 0, label: 1, isClosed: 1 },
      { sort: { position: 1 } }
    )
    const openDays = restaurantDays
      .filter((day) => !day.isClosed)
      .map((item) => item.label)
    const restaurantPeriods = await this.restaurantPeriodService.getAll(
      {
        restaurantId,
        label: { $in: openDays },
        status: StatusEnum.ACTIVE
      },
      { label: 1, opensAt: 1, closesAt: 1 }
    )
    const pickupHours = await this.pickupHoursService.getAll({ restaurantId })
    const selectedPeriod = pickupHours.map(
      (selected) => `${selected.restaurantPeriodId}`
    )
    const result = []
    for (const day of restaurantDays) {
      const item = {
        label: day.label,
        isClosed: day.isClosed,
        period: []
      }
      if (day.isClosed) {
        result.push(item)
      } else {
        const dayPeriod = restaurantPeriods.filter(
          (period) => period.label === day.label
        )
        item.period = dayPeriod.map((e) => ({
          id: e._id,
          label: e.label,
          opensAt: e.opensAt,
          closesAt: e.closesAt,
          isSelected: selectedPeriod.includes(`${e._id}`)
        }))
        result.push(item)
      }
    }
    return result
  }

  async updatePickupHours(restaurantId: string, payload: UpdatePickupHoursDto) {
    const { period } = payload
    const pickupHoursPayload = period.map((item) => ({
      restaurantPeriodId: item,
      restaurantId
    }))
    await this.pickupHoursService.deleteMany({ restaurantId })
    await this.pickupHoursService.createMany(pickupHoursPayload)
    return { success: true }
  }

  async getPickupHoursForEatsZaab(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const restaurantDays = await this.restaurantDayService.getAll(
      { restaurantId },
      { _id: 0, label: 1, isClosed: 1 },
      { sort: { position: 1 } }
    )
    const openDays = restaurantDays
      .filter((day) => !day.isClosed)
      .map((item) => item.label)
    const restaurantPeriods = await this.restaurantPeriodService.getAll(
      {
        restaurantId,
        label: { $in: openDays },
        status: StatusEnum.ACTIVE
      },
      { label: 1, opensAt: 1, closesAt: 1 }
    )
    const pickupHours = await this.pickupHoursService.getAll({ restaurantId })
    const selectedPeriod = pickupHours.map(
      (selected) => `${selected.restaurantPeriodId}`
    )
    const result = []
    for (const day of restaurantDays) {
      const item = {
        label: day.label,
        isClosed: day.isClosed,
        period: []
      }
      if (day.isClosed) {
        result.push(item)
      } else {
        const dayPeriod = restaurantPeriods.filter(
          (period) =>
            period.label === day.label &&
            selectedPeriod.includes(`${period._id}`)
        )
        item.period = dayPeriod.map((e) => ({
          opensAt: e.opensAt,
          closesAt: e.closesAt
        }))
        result.push(item)
      }
    }
    return result
  }
}
