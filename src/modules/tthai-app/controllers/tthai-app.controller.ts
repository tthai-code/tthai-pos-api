import { Body, Controller, Get, Param, Patch, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { UpdatePickupHoursDto } from '../dto/update-pickup-hours.dto'
import {
  UpdateCustomizeInAppTextDto,
  UpdateItemSpecialInstructionsDto,
  UpdateMobileOrderingDto,
  UpdateWaitTimeDto
} from '../dto/update-tthai-app.dto'
import { TThaiAppResponse } from '../entity/tthai-app.entity'
import { PickupHoursLogic } from '../logics/pickup-hours.logic'
import { TThaiAppLogic } from '../logics/tthai-app.logic'

@ApiBearerAuth()
@ApiTags('tthai-app-setting')
@UseGuards(AdminAuthGuard)
@Controller('v1/tthai-app')
export class TThaiAppController {
  constructor(
    private readonly tthaiAppLogic: TThaiAppLogic,
    private readonly pickupHoursLogic: PickupHoursLogic
  ) {}

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => TThaiAppResponse })
  @ApiOperation({ summary: 'get TThai app setting by restaurant' })
  async getTThaiApp(@Param('restaurantId') id: string) {
    return await this.tthaiAppLogic.getTThaiApp(id)
  }

  @Patch(':restaurantId/mobile-ordering')
  @ApiOkResponse({ type: () => TThaiAppResponse })
  @ApiOperation({ summary: 'update mobile ordering by restaurant' })
  async updateMobileOrdering(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateMobileOrderingDto
  ) {
    return await this.tthaiAppLogic.updateMobileOrdering(id, payload)
  }

  @Patch(':restaurantId/item-special')
  @ApiOkResponse({ type: () => TThaiAppResponse })
  @ApiOperation({ summary: 'update item special instructions by restaurant' })
  async updateIsItemSpecialInstructions(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateItemSpecialInstructionsDto
  ) {
    return await this.tthaiAppLogic.updateItemSpecialInstructions(id, payload)
  }

  @Patch(':restaurantId/pickup-hours')
  @ApiOkResponse({ type: () => TThaiAppResponse })
  @ApiOperation({ summary: 'update pick up hours by restaurant' })
  async updatePickUpHoursDto(
    @Param('restaurantId') id: string,
    @Body() payload: UpdatePickupHoursDto
  ) {
    return await this.pickupHoursLogic.updatePickupHours(id, payload)
  }

  @Patch(':restaurantId/wait-time')
  @ApiOkResponse({ type: () => TThaiAppResponse })
  @ApiOperation({ summary: 'update wait time by restaurant' })
  async updateWaitTimeDto(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateWaitTimeDto
  ) {
    return await this.tthaiAppLogic.updateWaitTimeDto(id, payload)
  }

  @Patch(':restaurantId/in-app-text')
  @ApiOkResponse({ type: () => TThaiAppResponse })
  @ApiOperation({ summary: 'update customize in app text by restaurant' })
  async updateCustomizeInAppTextDto(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateCustomizeInAppTextDto
  ) {
    return await this.tthaiAppLogic.updateCustomizeInAppTextDto(id, payload)
  }
}
