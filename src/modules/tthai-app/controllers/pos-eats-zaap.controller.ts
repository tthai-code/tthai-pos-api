import { Controller, Get, Param, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import {
  ApiBasicAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { GetEatsZaabResponse } from '../entity/open-eats-zaap.entity'
import { POSEatsZaabLogic } from '../logics/pos-eats-zaap.logic'

@ApiBasicAuth()
@ApiTags('open/eats-zaap')
@UseGuards(AuthGuard('basic'))
@Controller('v1/open/eats-zaap')
export class POSEatsZaabController {
  constructor(private readonly posEatsZaabLogic: POSEatsZaabLogic) {}

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => GetEatsZaabResponse })
  @ApiOperation({
    summary: 'Get EatsZaab Setting by Restaurant ID',
    description: 'use basic auth'
  })
  async getEatsZaab(@Param('restaurantId') id: string) {
    return await this.posEatsZaabLogic.getTThaiApp(id)
  }
}
