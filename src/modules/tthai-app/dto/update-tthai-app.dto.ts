import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  ArrayNotEmpty,
  IsBoolean,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested
} from 'class-validator'
import { DayEnum } from 'src/modules/restaurant-hours/common/day.enum'

export class UpdateMobileOrderingDto {
  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isMobileOrdering: boolean
}

export class UpdateItemSpecialInstructionsDto {
  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isItemSpecialInstructions: boolean
}

class PeriodFieldsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, example: '09:00AM' })
  opensAt: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, example: '12:00AM' })
  closesAt: string
}

class PickUpHoursFieldsDto {
  @IsEnum(DayEnum)
  @ApiProperty({ enum: Object.values(DayEnum), example: DayEnum.MONDAY })
  readonly label: string

  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty({ example: false })
  readonly isClosed: boolean

  @Type(() => PeriodFieldsDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [PeriodFieldsDto] })
  readonly period: Array<PeriodFieldsDto>
}

export class UpdatePickUpHoursDto {
  @Type(() => PickUpHoursFieldsDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [PickUpHoursFieldsDto] })
  readonly pickupHours: Array<PickUpHoursFieldsDto>
}

class UpdateWaitTimeFieldsDto {
  @IsNumber()
  @ApiProperty({ example: 15 })
  readonly baseTime: number

  @IsNumber()
  @ApiProperty({ example: 15 })
  readonly perOrderTime: number

  @IsNumber()
  @ApiProperty({ example: 15 })
  readonly perItemTime: number

  @IsNumber()
  @ApiProperty({ example: 60 })
  readonly maxTime: number
}

export class UpdateWaitTimeDto {
  @Type(() => UpdateWaitTimeFieldsDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => UpdateWaitTimeFieldsDto })
  readonly waitTime: UpdateWaitTimeFieldsDto
}

class UpdateCustomizeInAppTextFieldsDto {
  @IsString()
  @ApiProperty({ example: 'pickUpOrderInstructionsLabel Test' })
  readonly pickUpOrderInstructionsLabel: string

  @IsString()
  @ApiProperty({ example: 'Test Desc' })
  readonly orderInstructionsDesc: string

  @IsString()
  @ApiProperty({ example: 'Test Label' })
  readonly deliveryOrderInstructionsLabel: string

  @IsString()
  @ApiProperty({ example: 'Item Label Test' })
  readonly itemInstructionsLabel: string

  @IsString()
  @ApiProperty({ example: 'Item Desc Test' })
  readonly itemInstructionsDesc: string
}

export class UpdateCustomizeInAppTextDto {
  @Type(() => UpdateCustomizeInAppTextFieldsDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => UpdateCustomizeInAppTextFieldsDto })
  readonly customizeInAppText: UpdateCustomizeInAppTextFieldsDto
}
