import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'

export class UpdatePickupHoursDto {
  @IsString({ each: true })
  @ApiProperty({ example: ['<period-id>'] })
  readonly period: string[]
}
