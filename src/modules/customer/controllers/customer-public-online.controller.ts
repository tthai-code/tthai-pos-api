import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse
} from '@nestjs/swagger'
import { errorResponse } from 'src/utilities/errorResponse'
import { POSCustomerPaginateDto } from '../dto/paginate-customer.dto'
import {
  CreatedCustomerResponse,
  CustomerResponse,
  PaginateCustomerResponse
} from '../entity/customer.entity'
import { CustomerLogic } from '../logics/customer.logic'
import { CustomerService } from '../services/customer.service'
import { AuthLogic } from 'src/modules/auth/logics/auth.logic'

import {
  CustomerLoginDto,
  CustomerSocialLoginDto
} from '../dto/customer-login.dto'
import { CreateCustomerOnlineDto } from '../dto/create-customer.dto'
import { CustomerAuthGuard } from 'src/modules/auth/guards/customer-auth.guard'
import { UpdateCustomerOnlineDto } from '../dto/update-online-customer.dto'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { CustomerGuestOnlineDto } from '../dto/continue-guest.dto'
import { CustomerOnlineLogic } from '../logics/customer-online.logic'
import { CustomerGuestResponse } from '../entity/customer-online.entity'

@ApiTags('customer-online')
@Controller('v1/public/online/customer')
export class CustomerOnlineController {
  constructor(
    private readonly customerService: CustomerService,
    private readonly customerLogic: CustomerLogic,
    private readonly authLogic: AuthLogic,
    private readonly customerOnlineLogic: CustomerOnlineLogic
  ) {}

  @Post('login')
  @ApiOperation({
    summary: 'Normal Login'
  })
  @ApiOkResponse({ type: () => CustomerResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Not found staff.', '/v1/staff/login')
  )
  async customerLogin(@Body() payload: CustomerLoginDto) {
    return this.authLogic.CustomerLogin(payload)
  }

  @Post('socialLogin')
  @ApiOperation({
    summary: 'Social Login'
  })
  @ApiOkResponse({ type: () => CustomerResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Not found staff.', '/v1/staff/login')
  )
  async customerSocialLogin(@Body() payload: CustomerSocialLoginDto) {
    return this.authLogic.CustomerSocialLogin(payload)
  }

  @Post()
  @ApiCreatedResponse({ type: () => CreatedCustomerResponse })
  @ApiBadRequestResponse(
    errorResponse(
      400,
      'This phone number is already exist.',
      'v1/public/online/customer'
    )
  )
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', 'v1/public/online/customer')
  )
  @ApiOperation({
    summary: 'Create a new customer',
    description: ''
  })
  async createCustomer(@Body() payload: CreateCustomerOnlineDto) {
    return await this.customerLogic.createCustomerOnline(payload)
  }

  @Get('all')
  @ApiOkResponse({ type: () => PaginateCustomerResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', 'v1/pos/customer')
  )
  @ApiOperation({
    summary: 'Get paginate customer list',
    description: ''
  })
  async getCustomers(@Query() query: POSCustomerPaginateDto) {
    return await this.customerService.paginate({}, query, {
      password: 0
    })
  }

  @Get(':customerId')
  @ApiOkResponse({ type: () => CustomerResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', '/v1/pos/customer/:customerId')
  )
  @ApiNotFoundResponse(
    errorResponse(404, 'Not found customer.', '/v1/pos/customer/:customerId')
  )
  @ApiOperation({
    summary: 'Get a customer by id',
    description: ''
  })
  async getCustomer(@Param('customerId') id: string) {
    return await this.customerLogic.getCustomer(id)
  }

  @ApiBearerAuth()
  @UseGuards(CustomerAuthGuard)
  @Put('info')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Update Customer Information',
    description: 'use bearer `customer token`'
  })
  async updateCustomerInfo(@Body() payload: UpdateCustomerOnlineDto) {
    const customerId = CustomerAuthGuard.getAuthorizedUser()?.id
    return await this.customerLogic.updateCustomerOnline(customerId, payload)
  }

  @Post('guest')
  @ApiCreatedResponse({ type: () => CustomerGuestResponse })
  @ApiOperation({
    summary: 'Continue With Guest',
    description: 'use to create customer and get `customer_token`'
  })
  async continueAsGuest(@Body() payload: CustomerGuestOnlineDto) {
    return await this.customerOnlineLogic.continueWithGuest(payload)
  }
}
