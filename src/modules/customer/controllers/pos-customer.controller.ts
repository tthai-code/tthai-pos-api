import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { errorResponse } from 'src/utilities/errorResponse'
import { CreateCustomerDto } from '../dto/create-customer.dto'
import { POSCustomerPaginateDto } from '../dto/paginate-customer.dto'
import { UpdateCustomerDto } from '../dto/update-customer.dto'
import {
  CreatedCustomerResponse,
  CustomerResponse,
  DeletedCustomerResponse,
  PaginateCustomerResponse
} from '../entity/customer.entity'
import { CustomerLogic } from '../logics/customer.logic'
import { CustomerService } from '../services/customer.service'

@ApiBearerAuth()
@ApiTags('pos/customer')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/customer')
export class POSCustomerController {
  constructor(
    private readonly customerService: CustomerService,
    private readonly customerLogic: CustomerLogic
  ) {}

  @Post()
  @ApiCreatedResponse({ type: () => CreatedCustomerResponse })
  @ApiBadRequestResponse(
    errorResponse(
      400,
      'This phone number is already exist.',
      '/v1/pos/customer'
    )
  )
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', '/v1/pos/customer')
  )
  @ApiOperation({
    summary: 'Create a new customer',
    description: 'use *bearer* `staff_token`'
  })
  async createCustomer(@Body() payload: CreateCustomerDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.customerLogic.createCustomer(restaurantId, payload)
  }

  @Get()
  @ApiOkResponse({ type: () => PaginateCustomerResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', 'v1/pos/customer')
  )
  @ApiOperation({
    summary: 'Get paginate customer list',
    description: 'use *bearer* `staff_token`'
  })
  async getCustomers(@Query() query: POSCustomerPaginateDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.customerService.paginate(
      query.buildQuery(restaurantId),
      query,
      {
        firstName: 1,
        lastName: 1,
        tel: 1
      }
    )
  }

  @Get(':customerId')
  @ApiOkResponse({ type: () => CustomerResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', '/v1/pos/customer/:customerId')
  )
  @ApiNotFoundResponse(
    errorResponse(404, 'Not found customer.', '/v1/pos/customer/:customerId')
  )
  @ApiOperation({
    summary: 'Get a customer by id',
    description: 'use *bearer* `staff_token`'
  })
  async getCustomer(@Param('customerId') id: string) {
    return await this.customerLogic.getCustomer(id)
  }

  @Put(':customerId')
  @ApiOkResponse({ type: () => CreatedCustomerResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', '/v1/pos/customer/:customerId')
  )
  @ApiNotFoundResponse(
    errorResponse(404, 'Not found customer.', '/v1/pos/customer/:customerId')
  )
  @ApiOperation({
    summary: 'Update a customer by id',
    description: 'use *bearer* `staff_token`'
  })
  async updateCustomer(
    @Param('customerId') id: string,
    @Body() payload: UpdateCustomerDto
  ) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.customerLogic.updateCustomer(id, restaurantId, payload)
  }

  @Delete(':customerId')
  @ApiOkResponse({ type: () => DeletedCustomerResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Unauthorized', '/v1/pos/customer/:customerId')
  )
  @ApiNotFoundResponse(
    errorResponse(404, 'Not found customer.', '/v1/pos/customer/:customerId')
  )
  @ApiOperation({
    summary: 'Delete a customer by id',
    description: 'use *bearer* `staff_token`'
  })
  async deleteCustomer(@Param('customerId') id: string) {
    const customer = await this.customerService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!customer) throw new NotFoundException('Not found customer.')
    return await this.customerService.delete(id)
  }
}
