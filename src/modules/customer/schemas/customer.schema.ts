import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CustomerTypeEnum } from 'src/modules/customer/common/enum/customer-type.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type CustomerDocument = CustomerFields & Document

@Schema({ _id: false, strict: true, timestamps: false })
export class AddressFields {
  @Prop({ default: '' })
  address: string

  @Prop({ default: '' })
  city: string

  @Prop({ default: '' })
  state: string

  @Prop({ default: '' })
  zipCode: string
}

@Schema({ timestamps: true, strict: true, collection: 'customers' })
export class CustomerFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  firstName: string

  @Prop({ default: '' })
  lastName: string

  @Prop({ default: '' })
  email: string

  @Prop({ default: '' })
  password: string

  @Prop({ default: '' })
  tel: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop({
    default: CustomerTypeEnum.NORMAL,
    enum: Object.values(CustomerTypeEnum)
  })
  loginType: string

  @Prop({ default: '' })
  uidSocial: string

  @Prop({
    default: new AddressFields()
  })
  address: AddressFields

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const CustomerSchema = SchemaFactory.createForClass(CustomerFields)
CustomerSchema.plugin(mongoosePaginate)
CustomerSchema.plugin(authorStampCreatePlugin)
