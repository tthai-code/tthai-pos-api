import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { jwtConstants } from 'src/modules/auth/constants/auth'
import { AuthLogic } from 'src/modules/auth/logics/auth.logic'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CustomerTypeEnum } from '../common/enum/customer-type.enum'
import { CustomerGuestOnlineDto } from '../dto/continue-guest.dto'
import { CustomerService } from '../services/customer.service'

@Injectable()
export class CustomerOnlineLogic {
  constructor(
    private readonly customerService: CustomerService,
    private readonly restaurantService: RestaurantService,
    private readonly authLogic: AuthLogic
  ) {}

  async continueWithGuest(payload: CustomerGuestOnlineDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: payload.restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    payload.loginType = CustomerTypeEnum.GUEST
    const customer = await this.customerService.create(payload)
    const transform = {
      address: customer?.address,
      uidSocial: customer?.uidSocial,
      loginType: customer?.loginType,
      tel: customer?.tel,
      email: customer?.email,
      lastName: customer?.lastName,
      firstName: customer?.firstName,
      restaurantId: customer?.restaurantId,
      id: customer?.id
    }
    const accessToken = this.authLogic.jwtSignToken(
      transform,
      '7d',
      jwtConstants.secretCustomer
    )
    const now = new Date()
    return {
      ...transform,
      accessToken,
      tokenExpire: now.setDate(now.getDate() + 7)
    }
  }
}
