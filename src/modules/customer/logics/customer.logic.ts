import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { classToPlain } from 'class-transformer'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CustomerTypeEnum } from 'src/modules/customer/common/enum/customer-type.enum'

import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import {
  CreateCustomerDto,
  CreateCustomerOnlineDto
} from '../dto/create-customer.dto'
import { UpdateCustomerDto } from '../dto/update-customer.dto'
import { CustomerService } from '../services/customer.service'
import * as bcrypt from 'bcryptjs'
import { UpdateCustomerOnlineDto } from '../dto/update-online-customer.dto'

@Injectable()
export class CustomerLogic {
  constructor(
    private customerService: CustomerService,
    private restaurantService: RestaurantService
  ) {}

  async getCustomer(customerId: string) {
    const customer = await this.customerService.findOne(
      {
        _id: customerId,
        status: StatusEnum.ACTIVE
      },
      { password: 0, createdAt: 0, updatedAt: 0, updatedBy: 0, createdBy: 0 }
    )
    if (!customer) throw new NotFoundException('Not found customer.')
    return customer
  }

  async createCustomer(
    restaurantId: string,
    createCustomer: CreateCustomerDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    createCustomer.loginType = CustomerTypeEnum.GUEST
    // const query = {
    //   restaurantId: restaurantId,
    //   tel: createCustomer?.tel,
    //   status: StatusEnum.ACTIVE
    // }
    // if (!createCustomer.tel) delete query.tel
    // const isExist = await this.customerService.findOne({
    //   restaurantId: restaurantId,
    //   tel: createCustomer.tel,
    //   status: StatusEnum.ACTIVE
    // })
    // if (isExist) {
    //   throw new BadRequestException('This phone number is already exist.')
    // }
    const payload = classToPlain(createCustomer)
    return this.customerService.create({ ...payload, restaurantId })
  }

  async createCustomerOnline(createCustomer: CreateCustomerOnlineDto) {
    const restaurantId = createCustomer.restaurantId
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    if (createCustomer.loginType !== CustomerTypeEnum.GUEST) {
      const isExist = await this.customerService.findOne({
        restaurantId: restaurantId,
        $or: [
          {
            tel: createCustomer.tel
          },
          {
            email: createCustomer.email
          }
        ],
        status: StatusEnum.ACTIVE,
        loginType: { $ne: CustomerTypeEnum.GUEST }
      })
      if (isExist) {
        throw new BadRequestException(
          'This phone number or email is already exist.'
        )
      }
    }

    const salt = bcrypt.genSaltSync(10)
    const payload = classToPlain(createCustomer)
    if (payload.password !== '') {
      payload.password = bcrypt.hashSync(payload.password, salt)
    }

    return this.customerService.create({ ...payload, restaurantId })
  }

  async updateCustomer(
    customerId: string,
    restaurantId: string,
    updateCustomer: UpdateCustomerDto
  ) {
    const customer = await this.customerService.findOne({
      _id: customerId,
      status: StatusEnum.ACTIVE
    })
    if (!customer) throw new NotFoundException('Not found customer.')
    const isExist = await this.customerService.findOne({
      _id: { $ne: customerId },
      restaurantId,
      tel: updateCustomer.tel,
      status: StatusEnum.ACTIVE
    })
    if (isExist) {
      throw new BadRequestException('This phone number is already exist.')
    }
    return this.customerService.update(customerId, updateCustomer)
  }

  async updateCustomerOnline(
    customerId: string,
    payload: UpdateCustomerOnlineDto
  ) {
    const customer = await this.customerService.findOne({
      _id: customerId,
      status: StatusEnum.ACTIVE
    })
    if (!customer) throw new NotFoundException('Not found customer.')
    const isExist = await this.customerService.findOne({
      _id: { $ne: customerId },
      restaurantId: customer.restaurantId,
      $or: [
        {
          tel: payload.tel
        },
        {
          email: payload.email
        }
      ],
      loginType: { $ne: CustomerTypeEnum.GUEST },
      status: StatusEnum.ACTIVE
    })
    if (isExist) {
      throw new BadRequestException(
        'This phone number or email is already exist.'
      )
    }
    await this.customerService.update(customerId, payload)
    return { success: true }
  }
}
