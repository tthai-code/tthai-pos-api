export enum CustomerTypeEnum {
  NORMAL = 'NORMAL',
  GUEST = 'GUEST',
  GOOGLE = 'GOOGLE',
  FACEBOOK = 'FACEBOOK',
  APPLE = 'APPLE'
}
