import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class CustomerGuestOnlineDto {
  public loginType: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '630e55a0d9c30fd7cdcb424b' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Tim' })
  readonly firstName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Henson' })
  readonly lastName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '5102638229' })
  readonly tel: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'tim@gmail.com' })
  readonly email: string
}
