import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class POSCustomerPaginateDto extends PaginateDto {
  @IsString()
  readonly sortBy: string = 'createdAt'

  @IsString()
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: 'Tim',
    description: 'Search by firstName, lastName or tel'
  })
  readonly search: string = ''

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          firstName: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          lastName: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          tel: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      restaurantId: id,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
