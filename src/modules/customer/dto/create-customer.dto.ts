import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import {
  IsNotEmpty,
  IsString,
  IsOptional,
  ValidateNested
} from 'class-validator'
import { CustomerTypeEnum } from 'src/modules/customer/common/enum/customer-type.enum'
import { Type } from 'class-transformer'

export class AddressFieldDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Some Address' })
  readonly address: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Addison' })
  readonly city: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Dallas' })
  readonly state: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: '75001' })
  readonly zipCode: string
}

export class CreateCustomerDto {
  public loginType: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Tim' })
  firstName: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Henson' })
  lastName: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: '5102638229' })
  tel: string

  @IsOptional()
  @Type(() => AddressFieldDto)
  @ValidateNested({ each: true })
  @ApiPropertyOptional({ type: () => AddressFieldDto })
  readonly address: AddressFieldDto

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'admin@gmail.com' })
  email: string
}

export class CreateCustomerOnlineDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '630e55a0d9c30fd7cdcb424b' })
  restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Tim' })
  firstName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'admin@gmail.com' })
  email: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'admin1234' })
  password: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Henson' })
  lastName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '5102638229' })
  tel: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    enum: Object.values(CustomerTypeEnum)
  })
  loginType: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: '<uid-social>' })
  uidSocial: string

  @Type(() => AddressFieldDto)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => AddressFieldDto })
  readonly address: AddressFieldDto
}
