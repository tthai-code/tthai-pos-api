import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'
import { CustomerTypeEnum } from '../common/enum/customer-type.enum'

export class CustomerLoginDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'test@gmail.com' })
  readonly email: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '123456' })
  readonly password: string
}

export class CustomerSocialLoginDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '123456' })
  readonly uidSocial: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    enum: Object.values(CustomerTypeEnum).filter(
      (item) =>
        item !== CustomerTypeEnum.GUEST && item !== CustomerTypeEnum.NORMAL
    )
  })
  readonly loginType: string
}
