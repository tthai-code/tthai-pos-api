import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsNotEmpty,
  IsNotEmptyObject,
  IsString,
  ValidateNested
} from 'class-validator'

class AddressFieldDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Some Address' })
  readonly address: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Addison' })
  readonly city: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Dallas' })
  readonly state: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '75001' })
  readonly zipCode: string
}

export class UpdateCustomerOnlineDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Tim' })
  readonly firstName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Henson' })
  readonly lastName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '5102638229' })
  readonly tel: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'admin@gmail.com' })
  readonly email: string

  @Type(() => AddressFieldDto)
  @IsNotEmpty()
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => AddressFieldDto })
  readonly address: AddressFieldDto
}
