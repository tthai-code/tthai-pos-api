import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class UpdateCustomerDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Tim' })
  firstName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Henson' })
  lastName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '5102638229' })
  tel: string
}
