import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { POSCustomerController } from './controllers/pos-customer.controller'
import { CustomerOnlineController } from './controllers/customer-public-online.controller'
import { CustomerLogic } from './logics/customer.logic'
import { CustomerSchema } from './schemas/customer.schema'
import { CustomerService } from './services/customer.service'
import { CustomerOnlineLogic } from './logics/customer-online.logic'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([{ name: 'customer', schema: CustomerSchema }])
  ],
  providers: [CustomerService, CustomerLogic, CustomerOnlineLogic],
  controllers: [POSCustomerController, CustomerOnlineController],
  exports: [CustomerService]
})
export class CustomerModule {}
