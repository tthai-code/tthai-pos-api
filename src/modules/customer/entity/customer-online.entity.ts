import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { CustomerTypeEnum } from '../common/enum/customer-type.enum'

class AddressObject {
  @ApiProperty()
  zip_code: string
  @ApiProperty()
  state: string
  @ApiProperty()
  city: string
  @ApiProperty()
  address: string
}

class CustomerGuestObject {
  @ApiProperty()
  address: AddressObject
  @ApiProperty()
  uid_social: string
  @ApiProperty({ enum: Object.values(CustomerTypeEnum) })
  login_type: string
  @ApiProperty()
  tel: string
  @ApiProperty()
  email: string
  @ApiProperty()
  last_name: string
  @ApiProperty()
  first_name: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
  @ApiProperty()
  token_expire: number
}

export class CustomerGuestResponse extends ResponseDto<CustomerGuestObject> {
  @ApiProperty({
    type: CustomerGuestObject,
    example: {
      address: {
        zip_code: '',
        state: '',
        city: '',
        address: ''
      },
      uid_social: '',
      login_type: 'GUEST',
      tel: '7523123453',
      email: '',
      last_name: 'Henson',
      first_name: 'Tim',
      restaurant_id: '63984d6690416c27a7415915',
      id: '63bfd33151f3530d7650ea1d',
      token_expire: 1674120625297
    }
  })
  data: CustomerGuestObject

  @ApiProperty({ example: '<customer_token>' })
  access_token: string
}
