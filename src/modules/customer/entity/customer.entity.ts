import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'

class CreatedCustomerFieldsWithTimestampResponse extends TimestampResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  restaurant_id: string

  @ApiProperty()
  first_name: string

  @ApiProperty()
  last_name: string

  @ApiProperty()
  tel: string

  @ApiProperty()
  status: string
}

class CustomerFieldResponse {
  @ApiProperty()
  id: string

  @ApiProperty()
  first_name: string

  @ApiProperty()
  last_name: string

  @ApiProperty()
  tel: string
}

class PaginateCustomerFields extends PaginateResponseDto<
  Array<CustomerFieldResponse>
> {
  @ApiProperty({
    example: [
      {
        tel: '5102638229',
        last_name: 'Henson',
        first_name: 'Tim',
        id: '635a421765e365cb4f0bb378'
      }
    ]
  })
  results: Array<CustomerFieldResponse>
}

export class PaginateCustomerResponse extends ResponseDto<PaginateCustomerFields> {
  @ApiProperty({ type: PaginateCustomerFields })
  data: PaginateCustomerFields
}

export class CreatedCustomerResponse extends ResponseDto<CreatedCustomerFieldsWithTimestampResponse> {
  @ApiProperty({
    type: CreatedCustomerFieldsWithTimestampResponse,
    example: {
      status: 'active',
      tel: '5102638229',
      last_name: 'Henson',
      first_name: 'Tim',
      restaurant_id: '630eff5751c2eac55f52662c',
      created_at: '2022-10-27T08:32:23.856Z',
      updated_at: '2022-10-27T08:32:23.856Z',
      updated_by: {
        username: 'Pavarich',
        id: '6316cc72b889593b028928f1'
      },
      created_by: {
        username: 'Pavarich',
        id: '6316cc72b889593b028928f1'
      },
      id: '635a421765e365cb4f0bb378'
    }
  })
  data: CreatedCustomerFieldsWithTimestampResponse
}

export class CustomerResponse extends ResponseDto<CustomerFieldResponse> {
  @ApiProperty({
    type: CustomerFieldResponse,
    example: {
      tel: '5102638229',
      last_name: 'Henson',
      first_name: 'Tim',
      id: '635a421765e365cb4f0bb378'
    }
  })
  data: CustomerFieldResponse
}

export class DeletedCustomerResponse extends ResponseDto<CreatedCustomerFieldsWithTimestampResponse> {
  @ApiProperty({
    type: CreatedCustomerFieldsWithTimestampResponse,
    example: {
      created_by: {
        username: 'Pavarich',
        id: '6316cc72b889593b028928f1'
      },
      updated_by: {
        username: 'Pavarich',
        id: '6316cc72b889593b028928f1'
      },
      status: 'deleted',
      tel: '5102638229',
      last_name: 'Henson',
      first_name: 'Tim',
      restaurant_id: '630eff5751c2eac55f52662c',
      updated_at: '2022-10-27T08:41:23.531Z',
      created_at: '2022-10-27T08:32:23.856Z',
      id: '635a421765e365cb4f0bb378'
    }
  })
  data: CreatedCustomerFieldsWithTimestampResponse
}
