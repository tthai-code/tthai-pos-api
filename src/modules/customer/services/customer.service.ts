import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { CustomerDocument } from '../schemas/customer.schema'
import { POSCustomerPaginateDto } from '../dto/paginate-customer.dto'

@Injectable({ scope: Scope.REQUEST })
export class CustomerService {
  constructor(
    @InjectModel('customer')
    private readonly CustomerModel: PaginateModel<CustomerDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<CustomerDocument | any> {
    return this.CustomerModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.CustomerModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<CustomerDocument> {
    return this.CustomerModel
  }

  async create(payload: any): Promise<CustomerDocument> {
    const document = new this.CustomerModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, User: any): Promise<CustomerDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...User }).save()
  }

  async delete(id: string): Promise<CustomerDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.CustomerModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.CustomerModel.findById(id)
  }

  findOne(
    condition: any,
    project?: any,
    options?: any
  ): Promise<CustomerDocument> {
    return this.CustomerModel.findOne(condition, project, options)
  }

  paginate(
    query: any,
    queryParam: POSCustomerPaginateDto,
    select?: any
  ): Promise<PaginateResult<CustomerDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.CustomerModel.paginate(query, options)
  }
}
