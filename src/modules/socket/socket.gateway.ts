import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer
} from '@nestjs/websockets'
import { Server, Socket } from 'socket.io'

@WebSocketGateway({
  namespace: 'v1/notify',
  cors: {
    origin: '*'
  }
})
export class SocketGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer()
  server: Server

  afterInit() {
    console.log('Websocket Server Started, Listening on Port:3001')
  }

  handleDisconnect(client: Socket) {
    console.log({
      message: `Client disconnected: ${client.id}`,
      timestamps: new Date()
    })
  }

  handleConnection(client: Socket) {
    client.emit('connected', {
      message: `Client connected: ${client.id}`,
      timestamps: new Date()
    })
    console.log({
      message: `Client connected: ${client.id}`,
      timestamps: new Date()
    })
  }

  @SubscribeMessage('joinRoomPOS')
  handleJoinRoomPOS(
    @ConnectedSocket() client: Socket,
    @MessageBody('restaurantId') restaurantId: string
  ) {
    client.join(`pos-${restaurantId}`)
    client.emit('joinedRoomPOS', {
      message: `Client joined room: pos-${restaurantId}`,
      timestamps: new Date()
    })
  }

  @SubscribeMessage('leaveRoomPOS')
  handleLeaveRoomPOS(
    @ConnectedSocket() client: Socket,
    @MessageBody('restaurantId') restaurantId: string
  ) {
    client.leave(`pos-${restaurantId}`)
    client.emit('leftRoomPOS', {
      message: `Client left room: pos-${restaurantId}`,
      timestamps: new Date()
    })
  }

  @SubscribeMessage('joinRoomWeb')
  handleJoinRoomWeb(
    @ConnectedSocket() client: Socket,
    @MessageBody('customerId') customerId: string
  ) {
    client.join(`web-${customerId}`)
    client.emit('joinedRoomWeb', {
      message: `Client joined room: web-${customerId}`,
      timestamps: new Date()
    })
  }

  @SubscribeMessage('leaveRoomWeb')
  handleLeaveRoomWeb(
    @ConnectedSocket() client: Socket,
    @MessageBody('customerId') customerId: string
  ) {
    client.leave(`web-${customerId}`)
    client.emit('leftRoomWeb', {
      message: `Client left room: web-${customerId}`,
      timestamps: new Date()
    })
  }
}
