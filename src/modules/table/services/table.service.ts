import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { TableDocument } from '../models/table.schema'
import { TablePaginateDto } from '../dto/get-table.dto'
import { TableStatusEnum } from '../common/table-status.enum'

@Injectable({ scope: Scope.REQUEST })
export class TableService {
  constructor(
    @InjectModel('table')
    private readonly TableModel: PaginateModel<TableDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<TableDocument | any> {
    return this.TableModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.TableModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<TableDocument> {
    return this.TableModel
  }

  async getSession(): Promise<ClientSession> {
    await this.TableModel.createCollection()

    return this.TableModel.startSession()
  }

  async create(payload: any): Promise<TableDocument> {
    const document = new this.TableModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<TableDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async transactionUpdate(
    id: string,
    payload: any,
    session: ClientSession
  ): Promise<TableDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save({ session })
  }

  async delete(id: string): Promise<TableDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any, options?: any): Promise<any[]> {
    return this.TableModel.find(condition, project, options)
  }

  findById(id: number): Promise<any> {
    return this.TableModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<TableDocument> {
    return this.TableModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: TablePaginateDto
  ): Promise<PaginateResult<TableDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder }
    }

    return this.TableModel.paginate(query, options)
  }

  async customPaginate(
    query: any,
    queryParam: TablePaginateDto,
    select?: any
  ): Promise<PaginateResult<TableDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }
    const allDocs = await this.TableModel.find(query, select)
    const docs = await this.TableModel.find(query, select, {
      sort: options.sort,
      collation: { locale: 'en_US', numericOrdering: true },
      limit: options.limit,
      skip: (options.page - 1) * options.limit
    })
    return {
      docs,
      total: allDocs.length,
      limit: options.limit,
      page: options.page,
      pages: Math.ceil(allDocs.length / options.page)
    }
  }

  findOneForOrder(id: string): Promise<TableDocument> {
    return this.TableModel.findOne(
      {
        _id: id,
        tableStatus: TableStatusEnum.AVAILABLE,
        status: StatusEnum.ACTIVE
      },
      { _id: 0, id: '$_id', tableNo: 1 }
    ).lean()
  }
}
