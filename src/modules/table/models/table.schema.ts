import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { TableStatusEnum } from '../common/table-status.enum'

export type TableDocument = TableFields & Document

@Schema({ _id: false, timestamps: false, strict: true })
export class TableZoneInTableFields {
  @Prop()
  id: string

  @Prop()
  name: string
}

@Schema({
  timestamps: true,
  strict: true,
  collection: 'tables',
  versionKey: false
})
export class TableFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop()
  tableNo: string

  @Prop()
  tableZone: TableZoneInTableFields

  @Prop()
  minSize: number

  @Prop()
  maxSize: number

  @Prop({
    enum: Object.values(TableStatusEnum),
    default: TableStatusEnum.AVAILABLE
  })
  tableStatus: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const TableSchema = SchemaFactory.createForClass(TableFields)
TableSchema.plugin(mongoosePaginate)
TableSchema.plugin(authorStampCreatePlugin)
