import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { TableZoneService } from 'src/modules/table-zone/services/table-zone.service'
import { CreateTableDto } from '../dto/create-table.dto'
import { UpdateTableDto } from '../dto/update-table.dto'
import { TableService } from '../services/table.service'

@Injectable()
export class TableLogic {
  constructor(
    private readonly tableService: TableService,
    private readonly tableZoneService: TableZoneService
  ) {}

  async CreateTable(table: CreateTableDto) {
    const tableZone = await this.tableZoneService.findOne(
      {
        _id: table.tableZoneId,
        restaurantId: table.restaurantId,
        status: StatusEnum.ACTIVE
      },
      { name: 1 }
    )
    if (!tableZone) throw new NotFoundException('Table Zone not found.')
    const existTable = await this.tableService.findOne({
      restaurantId: table.restaurantId,
      tableNo: table.tableNo,
      status: StatusEnum.ACTIVE
    })
    if (existTable) {
      throw new BadRequestException(
        `Table No. ${table.tableNo}  is already used.`
      )
    }
    const payload = {
      ...table,
      tableZone: {
        id: tableZone.id,
        name: tableZone.name
      }
    }
    delete payload.tableZoneId
    return this.tableService.create(payload)
  }

  async UpdateTable(id: string, table: UpdateTableDto) {
    const oldTable = await this.tableService.findOne({
      _id: id
    })
    if (!oldTable) throw new NotFoundException('Table not found.')
    const tableZone = await this.tableZoneService.findOne(
      {
        _id: table.tableZoneId,
        status: StatusEnum.ACTIVE
      },
      { name: 1 }
    )
    if (!tableZone) throw new NotFoundException('Table Zone not found.')
    const existTable = await this.tableService.findOne({
      _id: { $ne: id },
      restaurantId: oldTable.restaurantId,
      tableNo: table.tableNo,
      status: StatusEnum.ACTIVE
    })
    if (existTable) {
      throw new BadRequestException(
        `Table No. ${table.tableNo}  is already used.`
      )
    }
    const payload = {
      ...table,
      tableZone: {
        id: tableZone.id,
        name: tableZone.name
      }
    }
    delete payload.tableZoneId
    return this.tableService.update(id, payload)
  }

  async GetAllTable(query: any, restaurantId: string) {
    const tableZones = await this.tableZoneService.getAll(
      {
        restaurantId,
        status: StatusEnum.ACTIVE
      },
      { _id: 1, name: 1 }
    )
    const tables = await this.tableService.getAll(
      query,
      {
        tableNo: 1,
        maxSize: 1,
        minSize: 1,
        tableStatus: 1,
        tableZone: 1
      },
      {
        sort: { tableNo: 1 },
        collation: { locale: 'en_US', numericOrdering: true }
      }
    )
    const payload = tableZones.map((zone) => ({
      tableZone: zone.name,
      tables: tables
        .filter((table) => table['tableZone']['id'] === zone.id)
        .map((item) => ({
          id: item.id,
          tableNo: item.tableNo,
          maxSize: item.maxSize,
          minSize: item.minSize,
          tableStatus: item.tableStatus
        }))
    }))
    return payload
  }
}
