import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString
} from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'
import { TableStatusEnum } from '../common/table-status.enum'

export class CreateTableDto {
  public tableZone: any

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '1' })
  readonly tableNo: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<table-zone-id>' })
  readonly tableZoneId: string

  @IsNumber()
  @ApiProperty({ example: 1 })
  readonly minSize: number

  @IsNumber()
  @ApiProperty({ example: 3 })
  readonly maxSize: number

  @IsOptional()
  @IsEnum(TableStatusEnum)
  @ApiPropertyOptional({
    example: TableStatusEnum.AVAILABLE,
    enum: Object.values(TableStatusEnum)
  })
  readonly tableStatus: string

  @IsOptional()
  @IsEnum(StatusEnum)
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string
}
