import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class TablePaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'tableNo' })
  readonly sortBy: string = 'tableNo'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'asc' })
  readonly sortOrder: string = 'asc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<table-zone-id>' })
  readonly zone: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<restaurant-id>' })
  readonly restaurant: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          tableNo: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      'tableZone.id': {
        $eq: this.zone
      },
      restaurantId: { $eq: !id ? this.restaurant : id },
      status: this.status || { $ne: StatusEnum.DELETED }
    }
    if (!id && !this.restaurant) delete result.restaurantId
    if (!this.zone) delete result['tableZone.id']
    return result
  }
}
