import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { TableStatusEnum } from '../common/table-status.enum'

export class AllTableDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: TableStatusEnum.AVAILABLE,
    enum: Object.values(TableStatusEnum)
  })
  readonly tableStatus: string

  public buildQuery(id: string) {
    const result = {
      restaurantId: id,
      status: StatusEnum.ACTIVE,
      tableStatus: this.tableStatus
    }
    if (!this.tableStatus) delete result.tableStatus
    return result
  }
}
