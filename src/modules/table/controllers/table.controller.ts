import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { CreateTableDto } from '../dto/create-table.dto'
import { TablePaginateDto } from '../dto/get-table.dto'
import { UpdateTableDto } from '../dto/update-table.dto'
import { TableLogic } from '../logics/table.logic'
import { TableService } from '../services/table.service'

@ApiBearerAuth()
@ApiTags('table')
@Controller('v1/table')
@UseGuards(AdminAuthGuard)
export class TableController {
  constructor(
    private readonly tableService: TableService,
    private readonly tableLogic: TableLogic
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get List Paginate Table' })
  async getTables(@Query() query: TablePaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    const table = await this.tableService.customPaginate(
      query.buildQuery(restaurantId),
      query
    )
    return table
  }

  @Get(':id/table')
  @ApiOperation({ summary: 'Get a Table By ID' })
  async getTable(@Param('id') id: string) {
    return await this.tableService.findOne({ _id: id })
  }

  @Post()
  @ApiOperation({ summary: 'Create a Table' })
  async createTable(@Body() payload: CreateTableDto) {
    return await this.tableLogic.CreateTable(payload)
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update a Table' })
  async updateTable(@Param('id') id: string, @Body() payload: UpdateTableDto) {
    return await this.tableLogic.UpdateTable(id, payload)
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a Table By ID' })
  async deleteTable(@Param('id') id: string) {
    return await this.tableService.delete(id)
  }
}
