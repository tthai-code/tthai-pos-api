import { Controller, Get, Query, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse
} from '@nestjs/swagger'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { errorResponse } from 'src/utilities/errorResponse'
import { AllTableDto } from '../dto/get-all-table.dto'
import { POSTableResponse } from '../entity/table.entity'
import { TableLogic } from '../logics/table.logic'

@ApiBearerAuth()
@ApiTags('pos/table')
@Controller('v1/pos/table')
@UseGuards(StaffAuthGuard)
export class POSTableController {
  constructor(private readonly tableLogic: TableLogic) {}

  @Get()
  @ApiOperation({
    summary: 'Get All Table for Display on POS',
    description: 'use *bearer* `staff_token`'
  })
  @ApiOkResponse({ type: () => POSTableResponse })
  @ApiUnauthorizedResponse(errorResponse(401, 'Unauthorized', 'v1/pos/table'))
  async getAllTables(@Query() query: AllTableDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.tableLogic.GetAllTable(
      query.buildQuery(restaurantId),
      restaurantId
    )
  }
}
