import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { TableZoneModule } from '../table-zone/table-zone.module'
import { POSTableController } from './controllers/pos-table.controller'
import { TableController } from './controllers/table.controller'
import { TableLogic } from './logics/table.logic'
import { TableSchema } from './models/table.schema'
import { TableService } from './services/table.service'

@Module({
  imports: [
    TableZoneModule,
    MongooseModule.forFeature([{ name: 'table', schema: TableSchema }])
  ],
  providers: [TableService, TableLogic],
  controllers: [TableController, POSTableController],
  exports: [TableService]
})
export class TableModule {}
