import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TableStatusEnum } from '../common/table-status.enum'

class TableFieldsResponse {
  @ApiProperty()
  id: string

  @ApiProperty()
  table_no: string

  @ApiProperty()
  max_size: number

  @ApiProperty()
  min_size: number

  @ApiProperty({ enum: Object.values(TableStatusEnum) })
  table_status: string
}

class TableZoneFieldsResponse {
  @ApiProperty()
  table_zone: string

  @ApiProperty({ type: [TableFieldsResponse] })
  tables: Array<TableFieldsResponse>
}

export class POSTableResponse extends ResponseDto<
  Array<TableZoneFieldsResponse>
> {
  @ApiProperty({
    type: [TableZoneFieldsResponse],
    example: [
      {
        table_zone: 'Patio',
        tables: [
          {
            id: '633699d658fe7dc03f44920e',
            table_no: '5',
            max_size: 6,
            min_size: 1,
            table_status: 'AVAILABLE'
          },
          {
            id: '633699e358fe7dc03f449216',
            table_no: '6',
            max_size: 6,
            min_size: 1,
            table_status: 'AVAILABLE'
          },
          {
            id: '6336b32ec64b8b0998892917',
            table_no: '7',
            max_size: 4,
            min_size: 1,
            table_status: 'AVAILABLE'
          }
        ]
      },
      {
        table_zone: 'Indoor',
        tables: [
          {
            id: '63157c99b66b8e14a6c00f8e',
            table_no: '1',
            max_size: 5,
            min_size: 1,
            table_status: 'AVAILABLE'
          },
          {
            id: '63157f97b66b8e14a6c0103a',
            table_no: '2',
            max_size: 5,
            min_size: 1,
            table_status: 'AVAILABLE'
          },
          {
            id: '633699b458fe7dc03f4491fe',
            table_no: '3',
            max_size: 5,
            min_size: 1,
            table_status: 'AVAILABLE'
          },
          {
            id: '633699c458fe7dc03f449206',
            table_no: '4',
            max_size: 3,
            min_size: 1,
            table_status: 'AVAILABLE'
          }
        ]
      },
      {
        table_zone: 'Bar',
        tables: [
          {
            id: '6336b370c64b8b0998892924',
            table_no: '8',
            max_size: 2,
            min_size: 1,
            table_status: 'AVAILABLE'
          },
          {
            id: '6336b379c64b8b099889292c',
            table_no: '9',
            max_size: 2,
            min_size: 1,
            table_status: 'AVAILABLE'
          }
        ]
      }
    ]
  })
  data: Array<TableZoneFieldsResponse>
}
