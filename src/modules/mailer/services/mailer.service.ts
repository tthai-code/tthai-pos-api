import { Injectable } from '@nestjs/common'
import { MailerService } from '@nestjs-modules/mailer'
import { MailerDto } from '../dto/mailer.dto'

@Injectable()
export class POSMailerService {
  constructor(private readonly mailerService: MailerService) {}

  transformToHTML(payload: any) {
    const logoUrl = `<img style="width: 72px; height: auto;" src="${payload.logoUrl}" alt="logo"/>`
    const sectionHead = `<tr><td align="center">${logoUrl}</td></tr><tr><td align="center" style="font-size: 1.225rem; font-weight: 600;">${payload.dba}</td></tr>`
    const sectionTotal = `<tr><td style="font-size: 3rem; font-weight: 700;">${payload.total}</td></tr>`
    const sectionOrderInfo = `
      <tr>
        <td align="left">${payload.orderDate}</td>
        <td align="right">${payload.orderTime}</td>
      </tr>
      <tr>
        <td align="left">${payload.orderType}</td>
      </tr>
      <tr>
        <td align="left">${payload.orderId}</td>
        <td align="right">${payload.transactionId}</td>
      </tr>
      <tr>
        <td align="left">${payload.ticketNo}</td>
      </tr>
    `
    const sectionItemsQuantity = `
    <tr>
      <td align="left">${payload.itemsQuantity}</td>
    </tr>
    `
    const sectionItemList = payload.itemList
      .map(
        (item) => `
    <tr>
      <td align="left">${item.label}</td>
      <td align="right">${item.amount}</td>
    </tr>
    `
      )
      .join('')
    const sectionSummary = payload.summaryList
      .map(
        (item) => `
    <tr>
      <td align="left">${item.label}</td>
      <td align="right">${item.amount}</td>
    </tr>
  `
      )
      .join('')
    const sectionTips = `
      <tr>
        <td align="left">${payload.tips.label}</td>
        <td align="right">${payload.tips.amount}</td>
      </tr>
      `
    const sectionAddress = payload.addressDetail
      .map(
        (item) => `
      <tr>
        <td align="center">${item}</td>
      </tr>
    `
      )
      .join('')
    const sectionPayment = `
    <tr style="font-size: 0.725rem;">
      <td align="left">${payload.paymentDetail.paidBy} ${payload.paymentDetail.cardType} ${payload.paymentDetail.cardLastFour}</td>
      <td align="right">${payload.paymentDetail.date}</td>
    </tr>
    <tr style="font-size: 0.725rem;">
      <td align="left">${payload.paymentDetail.cardType}</td>
      <td align="right">${payload.paymentDetail.authCode}</td>
    </tr>
    `
    const sectionPolicy = `
    <tr style="font-size: 0.725rem;">
      <td style="display: flex; flex-wrap: wrap;">
      Return Policy: We stand behind everything we sell. If\n you aren't satisfied with your purchase, you can\n return it for a replacement or full refund
      </td>
    </tr>
    `
    const footer = `
    <tr style="font-size: 1.225rem; font-weight: 500">
      <td style="display: flex; flex-wrap: wrap;">
      <a href="https://www.tthai.co/th/" style="text-decoration: none;">Powered by TThai.co</a>
      </td>
    </tr>
    `
    return `
    <head>
    <style>
    </style>
    </head>
    <body>
      <table style="margin-left: auto; margin-right: auto;" align="center">
      ${sectionHead}
      ${sectionTotal}
      </table>
      <table style="margin-left: auto; margin-right: auto;" align="center">
      ${sectionOrderInfo}
      <br>
      ${sectionItemsQuantity}
      ${sectionItemList}
      <br>
      ${sectionSummary}
      <br>
      ${sectionTips}
      </table>
      <br>
      <table style="margin-left: auto; margin-right: auto;" align="center">
      ${sectionAddress}
      </table>
      <br>
      <table style="margin-left: auto; margin-right: auto;" align="center">
      ${sectionPayment}
      </table>
      <br>
      <br>
      ${sectionPolicy}
      ${footer}
    </body>
    `
  }

  async postMail(mailDto: MailerDto) {
    const { to, from, subject, text, html } = mailDto

    const send = await this.mailerService.sendMail({
      to,
      from,
      subject,
      text,
      html
    })
    return send
  }
}
