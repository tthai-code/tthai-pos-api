import { Global, Module } from '@nestjs/common'
import { MailerModule } from '@nestjs-modules/mailer'
import { POSMailerService } from './services/mailer.service'
import { ConfigModule } from '@nestjs/config'
import { MailerController } from './controllers/mailer.controller'
import { HttpModule } from '@nestjs/axios'

@Global()
@Module({
  imports: [
    HttpModule,
    ConfigModule.forRoot(),
    MailerModule.forRoot({
      transport: {
        service: 'gmail',
        host: 'smtp.gmail.com',
        auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_PASSWORD
        }
      }
    })
  ],
  providers: [POSMailerService],
  controllers: [MailerController],
  exports: [POSMailerService]
})
export class POSMailerModule {}
