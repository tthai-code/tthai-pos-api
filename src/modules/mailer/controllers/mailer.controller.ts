import { Body, Controller, Post } from '@nestjs/common'
import { MailerDto } from '../dto/mailer.dto'
import { POSMailerService } from '../services/mailer.service'

@Controller('mail')
export class MailerController {
  constructor(private readonly mailerService: POSMailerService) {}

  @Post()
  async postEmail(@Body() payload: MailerDto) {
    return await this.mailerService.postMail(payload)
  }
}
