import { IsNotEmpty, IsOptional, IsString } from 'class-validator'

export class MailerDto {
  @IsString()
  @IsNotEmpty()
  readonly to: string

  @IsString()
  @IsNotEmpty()
  readonly from: string

  @IsString()
  @IsNotEmpty()
  readonly subject: string

  @IsOptional()
  @IsString()
  readonly text: string

  @IsOptional()
  @IsString()
  readonly html: string
}
