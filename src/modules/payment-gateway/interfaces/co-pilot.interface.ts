export interface ICreateMerchant {
  templateId?: string
  merchant: {
    salesCode?: string
    akaBusinessName: string
    dbaName: string
    legalBusinessName: string
    demographic: {
      businessAddress: {
        address1: string
        city: string
        zip: string
        stateCd: string
      }
    }
    bankDetail: {
      depositBank: {
        bankAcctNum: string
        bankRoutingNum: string
        bankAcctTypeCd: string
        bankName: string
      }
      withdrawalBank: {
        bankAcctNum: string
        bankRoutingNum: string
        bankAcctTypeCd: string
        bankName: string
      }
    }
    ownership: {
      owner: {
        ownerEmail: string
        ownerName: string
        ownerTitle: string
      }
    }
  }
}
