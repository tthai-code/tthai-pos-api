import { Body, Controller, Get, Put, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { UpdateBankAccountDto } from '../dto/bank-account.dto'
import {
  GetActiveBankAccountResponse,
  GetBankAccountResponse,
  UpdateBankAccountResponse
} from '../entity/bank-account.entity'
import { BankAccountLogic } from '../logics/bank-account.logic'
import { BankAccountService } from '../services/bank-account.service'

@ApiBearerAuth()
@ApiTags('bank-account')
@UseGuards(AdminAuthGuard)
@Controller('/v1/bank-account')
export class BankAccountController {
  constructor(
    private readonly bankAccountLogic: BankAccountLogic,
    private readonly bankAccountService: BankAccountService
  ) {}

  @Get()
  @ApiOkResponse({ type: () => GetActiveBankAccountResponse })
  @ApiOperation({
    summary: 'Get Bank Account by Restaurant ID',
    description: 'use bearer token `restaurant_token`'
  })
  async getBankAccount() {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.bankAccountLogic.getBankAccount(restaurantId)
  }

  @Get('all')
  @ApiOkResponse({ type: () => GetBankAccountResponse })
  @ApiOperation({
    summary: 'Get Bank Account List by Restaurant ID',
    description: 'use bearer token `restaurant_token`'
  })
  async getBankAccounts() {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.bankAccountLogic.getBankAccounts(restaurantId)
  }

  @Put()
  @ApiOkResponse({ type: () => UpdateBankAccountResponse })
  @ApiOperation({
    summary: 'Create New Bank Account Information (ACH)',
    description:
      'use bearer token `restaurant_token`\n\nInit or Change New Bank Account'
  })
  async updateBankAccount(@Body() payload: UpdateBankAccountDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.bankAccountLogic.updateBankAccount(restaurantId, payload)
  }
}
