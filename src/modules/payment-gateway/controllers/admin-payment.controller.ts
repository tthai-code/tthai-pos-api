import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { SuperAdminAuthGuard } from 'src/modules/auth/guards/super-admin-auth.guard'
import {
  GetAdminPaymentStatusDto,
  UpdateBankAccountStatusDto
} from '../dto/admin-update-payment.dto'
import { UpdatePaymentDto } from '../dto/update-payment.dto'
import {
  GetAdminPaymentStatusResponse,
  GetCoPilotPaymentStatusResponse,
  GetPaymentGatewayResponse
} from '../entity/admin-payment.entity'
import { AdminPaymentLogic } from '../logics/admin-payment.logic'
import { PaymentGatewayLogic } from '../logics/payment-gateway.logic'

@ApiBearerAuth()
@ApiTags('admin/payment-gateway')
@UseGuards(SuperAdminAuthGuard)
@Controller('v1/admin/payment-gateway')
export class AdminPaymentGatewayController {
  constructor(
    private readonly adminPaymentLogic: AdminPaymentLogic,
    private readonly paymentGatewayLogic: PaymentGatewayLogic
  ) {}

  @Get('status')
  @ApiOkResponse({ type: () => GetAdminPaymentStatusResponse })
  @ApiOperation({ summary: 'Payment Gateway Approved Status' })
  async getPaymentStatus(@Query() query: GetAdminPaymentStatusDto) {
    return await this.adminPaymentLogic.getPaymentStatus(query.restaurantId)
  }

  @Patch('bank-account')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({ summary: 'Payment Gateway Approved Status' })
  async updateBankAccountApproved(
    @Query() query: GetAdminPaymentStatusDto,
    @Body() payload: UpdateBankAccountStatusDto
  ) {
    return await this.adminPaymentLogic.updateBankAccountApproved(
      query.restaurantId,
      payload
    )
  }

  @Get('co-pilot')
  @ApiOkResponse({ type: () => GetCoPilotPaymentStatusResponse })
  @ApiOperation({
    summary: 'Get CoPilot Application Status',
    description:
      'จะ get ก็ต่อเมื่อหลังจาก get `/v1/admin/payment-gateway/status` แล้ว `midCoPilot` ไม่ใช่ `null`'
  })
  async getCoPilotStatus(@Query() query: GetAdminPaymentStatusDto) {
    return await this.adminPaymentLogic.getCoPilotStatus(query.restaurantId)
  }

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => GetPaymentGatewayResponse })
  @ApiOperation({
    summary: 'Get Payment Gateway Setting Detail',
    description: 'ใช้ตอนกด Edit เพื่อจะดูหรือแก้ไขข้อมูลในหน้า Super Admin'
  })
  async getPaymentGatewaySetting(@Param('restaurantId') id: string) {
    return await this.paymentGatewayLogic.getPaymentGatewayWithDecode(id)
  }

  @Put(':restaurantId')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({ summary: 'Update Payment Gateway Setting' })
  async updateMIDPayment(
    @Param('restaurantId') id: string,
    @Body() payload: UpdatePaymentDto
  ) {
    await this.paymentGatewayLogic.updatePaymentGateway(id, payload)
    return { success: true }
  }
}
