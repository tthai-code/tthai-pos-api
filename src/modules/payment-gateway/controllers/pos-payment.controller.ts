import { Controller, Get, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { PaymentGatewayResponse } from '../entity/get-pos-payment.response'
import { PaymentGatewayLogic } from '../logics/payment-gateway.logic'
import { PaymentGatewayService } from '../services/payment-gateway.service'

@ApiBearerAuth()
@ApiTags('pos/payment')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/payment')
export class POSPaymentGatewayController {
  constructor(
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly paymentGatewayLogic: PaymentGatewayLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaymentGatewayResponse })
  @ApiOperation({
    summary: 'Get Payment Gateway API Config',
    description: 'use *bearer* `staff_token`'
  })
  async getPaymentGateway() {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.paymentGatewayLogic.getPaymentGatewayWithDecode(
      restaurantId
    )
  }
}
