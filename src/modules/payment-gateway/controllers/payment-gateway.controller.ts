import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { stateCodes } from '../common/state-code.enum'
import { CreateMerchantDto } from '../dto/create-merchant.dto'
import { UpdatePaymentDto } from '../dto/update-payment.dto'
import { CreateMerchantResponse } from '../entity/create-merchant.entity'
import { SignatureStatusResponse } from '../entity/get-merchant-status.entity'
import { GetPaymentResponse } from '../entity/get-payment.response'
import { UpdatePaymentResponseDto } from '../entity/update-payment.reponse'
import { CoPilotLogic } from '../logics/co-pilot.logic'
import {
  IPaymentGateway,
  PaymentGatewayLogic
} from '../logics/payment-gateway.logic'

@ApiBearerAuth()
@ApiTags('payment-gateway-setting')
@UseGuards(AdminAuthGuard)
@Controller('/v1/payment-gateway')
export class PaymentGatewayController {
  constructor(
    private readonly paymentGatewayLogic: PaymentGatewayLogic,
    private readonly coPilotLogic: CoPilotLogic
  ) {}

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => GetPaymentResponse })
  @ApiOperation({ summary: 'Get Payment Gateway Setting' })
  async getPayment(
    @Param('restaurantId') id: string
  ): Promise<IPaymentGateway> {
    return await this.paymentGatewayLogic.getPaymentGateway(id)
  }

  @Put(':restaurantId')
  @ApiOkResponse({ type: () => UpdatePaymentResponseDto })
  @ApiOperation({ summary: 'Update Payment Gateway Setting' })
  async updatePayment(
    @Param('restaurantId') id: string,
    @Body() payload: UpdatePaymentDto
  ) {
    return await this.paymentGatewayLogic.updatePaymentGateway(id, payload)
  }

  @Get('merchant/state')
  @ApiOperation({ summary: 'Get State Code List' })
  async getStateCodes() {
    return stateCodes
  }

  @Post('merchant')
  @ApiOkResponse({ type: () => CreateMerchantResponse })
  @ApiOperation({ summary: 'Create Merchant from CoPilot' })
  async createMerchant(@Body() merchant: CreateMerchantDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.coPilotLogic.createMerchant(merchant, restaurantId)
  }

  @Get(':restaurantId/merchant')
  @ApiOkResponse({ type: () => SignatureStatusResponse })
  @ApiOperation({ summary: 'Get Merchant Status' })
  async getMerchantStatus() {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.coPilotLogic.getMerchantSignatureStatus(restaurantId)
  }
}
