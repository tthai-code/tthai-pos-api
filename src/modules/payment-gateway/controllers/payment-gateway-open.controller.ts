import { Controller, Get, Param, UseGuards } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import {
  ApiBasicAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { PaymentGatewayLogic } from '../logics/payment-gateway.logic'
import { GetPaymentResponse } from '../entity/get-payment.response'

@ApiBasicAuth()
@ApiTags('open/payment')
@UseGuards(AuthGuard('basic'))
@Controller('v1/open/payment')
export class PaymentGatewayOpenController {
  constructor(private readonly paymentGatewayLogic: PaymentGatewayLogic) {}

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => GetPaymentResponse })
  @ApiOperation({
    summary: 'Get Payment Gateway setting',
    description: 'use basic auth'
  })
  async getPaymentSetting(@Param('restaurantId') id: string) {
    return await this.paymentGatewayLogic.getPaymentGatewayWithDecode(id)
  }
}
