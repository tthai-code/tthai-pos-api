import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { BankAccountDocument } from '../schemas/bank-account.schema'

@Injectable({ scope: Scope.REQUEST })
export class BankAccountService {
  constructor(
    @InjectModel('bankAccount')
    private readonly BankAccountModel: Model<BankAccountDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<BankAccountDocument | any> {
    return this.BankAccountModel.findById({ _id: id })
  }

  async resolveByCondition(condition: any): Promise<BankAccountDocument | any> {
    return this.BankAccountModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.BankAccountModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<BankAccountDocument> {
    return this.BankAccountModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.BankAccountModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.BankAccountModel.createCollection()

    return this.BankAccountModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<BankAccountDocument> {
    const document = new this.BankAccountModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<BankAccountDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<BankAccountDocument> {
    const document = new this.BankAccountModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<BankAccountDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<BankAccountDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<BankAccountDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.BankAccountModel.find(condition, project).sort({
      createdAt: -1
    })
  }

  findById(id: number): Promise<any> {
    return this.BankAccountModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<BankAccountDocument> {
    return this.BankAccountModel.findOne(condition, project)
  }

  updateMany(condition: any, payload: any): Promise<any> {
    return this.BankAccountModel.updateMany(condition, { $set: payload })
  }

  transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.BankAccountModel.updateMany(
      condition,
      { $set: payload },
      { session }
    )
  }
}
