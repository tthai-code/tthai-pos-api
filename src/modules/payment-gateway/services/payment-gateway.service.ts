import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaymentGatewayDocument } from '../schemas/payment-gateway.schema'

@Injectable({ scope: Scope.REQUEST })
export class PaymentGatewayService {
  constructor(
    @InjectModel('paymentGateway')
    private readonly PaymentGatewayModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<PaymentGatewayDocument | any> {
    return this.PaymentGatewayModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any
  ): Promise<PaymentGatewayDocument | any> {
    return this.PaymentGatewayModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.PaymentGatewayModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<PaymentGatewayDocument> {
    return this.PaymentGatewayModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.PaymentGatewayModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.PaymentGatewayModel.createCollection()

    return this.PaymentGatewayModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<PaymentGatewayDocument> {
    const document = new this.PaymentGatewayModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<PaymentGatewayDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<PaymentGatewayDocument> {
    const document = new this.PaymentGatewayModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<PaymentGatewayDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<PaymentGatewayDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<PaymentGatewayDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.PaymentGatewayModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.PaymentGatewayModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<PaymentGatewayDocument> {
    return this.PaymentGatewayModel.findOne(condition, project)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<PaymentGatewayDocument> {
    return this.PaymentGatewayModel.findOne(condition, options)
  }
}
