import { HttpService } from '@nestjs/axios'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import {
  ICapturePartialAmount,
  IRecurring,
  IRefundWithRetRef,
  IVoidByOrderId,
  IVoidRetRef
} from '../interfaces/card-pointe.interface'

export interface IAuthBasic {
  username: string
  password: string
}

export interface IValidateMerchant {
  site: string
  acctupdater: string
  cvv: string
  cardproc: string
  fee_type: string
  enabled: boolean
  echeck: string
  merchid: string
  avs: string
}

export interface IGetProfile {
  profileId: string
  accountId: string
  merchantId: string
}

@Injectable({ scope: Scope.REQUEST })
export class PaymentGatewayHttpService {
  constructor(
    private readonly httpService: HttpService,
    @Inject(REQUEST) private request: any
  ) {}

  private readonly url = process.env.PAYMENT_API_URL
  private readonly csurl = process.env.PAYMENT_API_CSURL
  private readonly ACHMerchantID = process.env.MERCHANT_ACH_ID
  private readonly username = process.env.PAYMENT_USERNAME
  private readonly password = process.env.PAYMENT_PASSWORD

  getTThaiMerchant(): any {
    return {
      mid: this.ACHMerchantID,
      username: this.username,
      password: this.password
    }
  }

  async healthCheckPaymentGateway(auth: IAuthBasic): Promise<any> {
    return this.httpService.get(this.url, { auth }).toPromise()
  }

  async validateMerchantId(merchantId: string, auth: IAuthBasic): Promise<any> {
    return this.httpService
      .get(`${this.url}/inquireMerchant/${merchantId}`, { auth })
      .toPromise()
  }

  async tokenizeCard(account: string, auth: IAuthBasic): Promise<any> {
    return this.httpService.post(this.csurl, { account }, { auth }).toPromise()
  }

  async binToken(
    merchantId: string,
    token: string,
    auth: IAuthBasic
  ): Promise<any> {
    return this.httpService
      .get(`${this.url}/bin/${merchantId}/${token}`, { auth })
      .toPromise()
  }

  async authorization(payload: any, auth: IAuthBasic): Promise<any> {
    return this.httpService
      .put(`${this.url}/auth`, payload, { auth })
      .toPromise()
  }

  async capture(
    payload: ICapturePartialAmount,
    auth: IAuthBasic
  ): Promise<any> {
    return this.httpService
      .put(`${this.url}/capture`, payload, { auth })
      .toPromise()
  }

  async authACH(payload: IRecurring): Promise<any> {
    payload.merchid = this.ACHMerchantID
    payload.capture = 'y'
    payload.currency = 'USD'
    payload.ecomind = 'E'
    payload.bin = 'y'

    return this.httpService
      .put(`${this.url}/auth`, payload, {
        auth: { username: this.username, password: this.password }
      })
      .toPromise()
  }

  async recurring(payload: IRecurring): Promise<any> {
    payload.merchid = this.ACHMerchantID
    payload.capture = 'y'
    payload.currency = 'USD'
    payload.ecomind = 'R'
    payload.cof = 'C'
    payload.cofscheduled = 'Y'
    payload.bin = 'y'

    return this.httpService
      .put(`${this.url}/auth`, payload, {
        auth: { username: this.username, password: this.password }
      })
      .toPromise()
  }

  async voidRetRef(payload: IVoidRetRef, auth: IAuthBasic): Promise<any> {
    return this.httpService
      .put(`${this.url}/void`, payload, { auth })
      .toPromise()
  }

  async voidByOrderId(payload: IVoidByOrderId, auth: IAuthBasic): Promise<any> {
    return this.httpService
      .put(`${this.url}/voidByOrderId`, payload, { auth })
      .toPromise()
  }

  async refundRetRef(
    payload: IRefundWithRetRef,
    auth: IAuthBasic
  ): Promise<any> {
    return this.httpService
      .put(`${this.url}/refund`, payload, { auth })
      .toPromise()
  }

  async createProfile(payload: any, auth: IAuthBasic): Promise<any> {
    return this.httpService
      .put(`${this.url}/profile`, payload, { auth })
      .toPromise()
  }

  async getProfile(payload: IGetProfile, auth: IAuthBasic): Promise<any> {
    return this.httpService
      .get(
        `${this.url}/profile/${payload.profileId}/${payload.accountId}/${payload.merchantId}`,
        { auth }
      )
      .toPromise()
  }

  async deleteProfile(payload: IGetProfile, auth: IAuthBasic): Promise<any> {
    return this.httpService
      .delete(
        `${this.url}/profile/${payload.profileId}/${payload.accountId}/${payload.merchantId}`,
        { auth }
      )
      .toPromise()
  }

  async inquireRetRef(
    retRef: string,
    merchantId: string,
    auth: IAuthBasic
  ): Promise<any> {
    return this.httpService
      .get(`${this.url}/inquire/${retRef}/${merchantId}`, { auth })
      .toPromise()
  }

  async inquireOrderId(
    orderId: string,
    merchantId: string,
    auth: IAuthBasic
  ): Promise<any> {
    return this.httpService
      .get(`${this.url}/inquireByOrderid/${orderId}/${merchantId}`, { auth })
      .toPromise()
  }

  async openBatch(
    merchantId: string,
    batchSource: string,
    auth: IAuthBasic
  ): Promise<any> {
    return this.httpService
      .get(`${this.url}/openbatch/${merchantId}/${batchSource}`, { auth })
      .toPromise()
  }

  async closeBatch(
    merchantId: string,
    auth: IAuthBasic,
    batchId?: string | number
  ): Promise<any> {
    const url = !!batchId
      ? `${this.url}/closebatch/${merchantId}/${batchId}`
      : `${this.url}/closebatch/${merchantId}`
    return this.httpService.get(url, { auth }).toPromise()
  }
}
