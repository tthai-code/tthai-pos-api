import { HttpService } from '@nestjs/axios'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { REQUEST } from '@nestjs/core'
import { ICreateMerchant } from '../interfaces/co-pilot.interface'

@Injectable({ scope: Scope.REQUEST })
export class CoPilotService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    @Inject(REQUEST) private request: any
  ) {}

  private readonly url = this.configService.get<string>('CO_PILOT_API_URL')
  private readonly tokenUrl =
    this.configService.get<string>('CO_PILOT_TOKEN_URL')
  private readonly username =
    this.configService.get<string>('CO_PILOT_USERNAME')
  private readonly password =
    this.configService.get<string>('CO_PILOT_PASSWORD')
  private readonly clientSecret = this.configService.get<string>(
    'CO_PILOT_CLIENT_SECRET'
  )
  private readonly templateId = this.configService.get<string>(
    'CO_PILOT_TEMPLATE_ID'
  )
  private readonly salesCode = this.configService.get<string>(
    'CO_PILOT_SALES_CODE'
  )
  private readonly clientId =
    this.configService.get<string>('CO_PILOT_CLIENT_ID')

  async tokenize(): Promise<any> {
    const params = new URLSearchParams()
    params.append('username', this.username)
    params.append('password', this.password)
    params.append('grant_type', 'password')
    params.append('client_id', this.clientId)
    params.append('client_secret', this.clientSecret)

    return this.httpService
      .post(this.tokenUrl, params, {
        headers: { 'content-type': 'application/x-www-form-urlencoded' }
      })
      .toPromise()
  }

  async createMerchantMinimum(
    payload: ICreateMerchant,
    token: string
  ): Promise<any> {
    payload.templateId = this.templateId
    payload.merchant.salesCode = this.salesCode
    return this.httpService
      .post(`${this.url}/merchant`, payload, {
        headers: {
          Authorization: `Bearer ${token}`,
          'X-CopilotAPI-Version': '1.0'
        }
      })
      .toPromise()
  }

  async signatureMerchant(merchantId: string, token: string): Promise<any> {
    return this.httpService
      .put(
        `${this.url}/merchant/${merchantId}/signature`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
            'X-CopilotAPI-Version': '1.0'
          }
        }
      )
      .toPromise()
  }

  async getMerchantSignatureStatus(
    merchantId: string,
    token: string
  ): Promise<any> {
    return this.httpService
      .get(`${this.url}/merchant/${merchantId}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          'X-CopilotAPI-Version': '1.0'
        }
      })
      .toPromise()
  }
}
