import { HttpModule } from '@nestjs/axios'
import { forwardRef, Global, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { SubscriptionModule } from '../subscription/subscription.module'
import { AdminPaymentGatewayController } from './controllers/admin-payment.controller'
import { BankAccountController } from './controllers/bank-account.controller'
import { PaymentGatewayOpenController } from './controllers/payment-gateway-open.controller'
import { PaymentGatewayController } from './controllers/payment-gateway.controller'
import { POSPaymentGatewayController } from './controllers/pos-payment.controller'
import { AdminPaymentLogic } from './logics/admin-payment.logic'
import { BankAccountLogic } from './logics/bank-account.logic'
import { CardPointeLogic } from './logics/card-pointe.logic'
import { CoPilotLogic } from './logics/co-pilot.logic'
import { PaymentGatewayLogic } from './logics/payment-gateway.logic'
import { BankAccountSchema } from './schemas/bank-account.schema'
import { PaymentGatewaySchema } from './schemas/payment-gateway.schema'
import { BankAccountService } from './services/bank-account.service'
import { CoPilotService } from './services/co-pilot.service'
import { PaymentGatewayHttpService } from './services/payment-gateway-http.service'
import { PaymentGatewayService } from './services/payment-gateway.service'

@Global()
@Module({
  imports: [
    ConfigModule,
    HttpModule,
    forwardRef(() => SubscriptionModule),
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([
      { name: 'paymentGateway', schema: PaymentGatewaySchema },
      { name: 'bankAccount', schema: BankAccountSchema }
    ])
  ],
  providers: [
    PaymentGatewayService,
    PaymentGatewayLogic,
    PaymentGatewayHttpService,
    CardPointeLogic,
    CoPilotService,
    CoPilotLogic,
    BankAccountService,
    BankAccountLogic,
    AdminPaymentLogic
  ],
  controllers: [
    PaymentGatewayController,
    PaymentGatewayOpenController,
    POSPaymentGatewayController,
    BankAccountController,
    AdminPaymentGatewayController
  ],
  exports: [
    PaymentGatewayService,
    PaymentGatewayLogic,
    CardPointeLogic,
    BankAccountService,
    BankAccountLogic,
    PaymentGatewayHttpService
  ]
})
export class PaymentGatewayModule {}
