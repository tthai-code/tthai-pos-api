export enum ACHBankAccountTypeEnum {
  CHECKING = 'CHECKING',
  SAVING = 'SAVING'
}

export enum BankAccountStatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}
