export const bankAcctTypes = [
  {
    code: 'BIZ',
    name: 'Business Checking'
  },
  {
    code: 'GL',
    name: 'General Ledger'
  },
  {
    code: 'SAVINGS',
    name: 'Savings'
  }
]
