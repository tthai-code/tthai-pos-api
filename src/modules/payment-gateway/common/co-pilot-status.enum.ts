export enum CoPilotStatusEnum {
  INPROG = 'Not Submitted.',
  OFS = 'Pending Signature.',
  QUALIFY = 'Qualifying.',
  UNDER = 'Underwriting.',
  DECLINED = 'Declined.',
  BOARDING = 'Boarding',
  BOARDED = 'Boarded',
  LIVE = 'Live',
  CANCELLED = 'Cancelled',
  DEFAULT = '-'
}
