import { ApiProperty } from '@nestjs/swagger'
import { IsBoolean, IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class UpdateAdminPaymentDto {
  public restaurantId: string
  public paymentStatus: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<merchant-id>' })
  readonly mid: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'testing' })
  readonly username: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'testing123' })
  public password: string

  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isConvenienceFee: boolean

  @IsNumber()
  @ApiProperty({ example: 0.99 })
  readonly convenienceFee: number
}
