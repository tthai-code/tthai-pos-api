import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

interface IAuthorization {
  merchid: string
  account: string
  expiry: string
  amount: string
  currency: string
  name: string
}

export class AuthNoCaptureDto implements IAuthorization {
  public merchid: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  public restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<card-number>' })
  public account: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<MMYY>' })
  readonly expiry: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '100', description: '"100" === 1.00' })
  readonly amount: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'USD' })
  readonly currency: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'CC TEST' })
  readonly name: string

  @IsString()
  @ApiProperty({ example: '<order-id>' })
  readonly orderid: string
}

export class AuthWithCaptureDto extends AuthNoCaptureDto {
  public ecomind: string
  public capture: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly cvv2: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly postal: string
}
