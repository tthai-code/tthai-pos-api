import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class TokenizeCardDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<account-number>' })
  readonly account: string
}
