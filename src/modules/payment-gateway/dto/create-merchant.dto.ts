import { ApiProperty } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsIn,
  IsNotEmpty,
  IsNotEmptyObject,
  IsString,
  ValidateNested
} from 'class-validator'
import { bankAcctTypes } from '../common/bank-account-type.enum'
import { stateCodes } from '../common/state-code.enum'

export class BusinessAddressObjectDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '1000 Continental Dr' })
  readonly address1: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'King of Prussia' })
  readonly city: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '19406' })
  readonly zip: string

  @IsString()
  @IsIn(stateCodes.map((item) => item.code))
  @IsNotEmpty()
  @ApiProperty({ example: 'PA', enum: stateCodes.map((item) => item.code) })
  readonly stateCd: string
}

export class DemographicObjectDto {
  @Type(() => BusinessAddressObjectDto)
  @ValidateNested({ each: true })
  @IsNotEmptyObject()
  @ApiProperty({ type: BusinessAddressObjectDto })
  readonly businessAddress: BusinessAddressObjectDto
}

export class BankObjectDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly bankAcctNum: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly bankRoutingNum: string

  @IsString()
  @IsIn(bankAcctTypes.map((item) => item.code))
  @IsNotEmpty()
  @ApiProperty({ enum: bankAcctTypes.map((item) => item.code) })
  readonly bankAcctTypeCd: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly bankName: string
}

export class BankDetailObjectDto {
  @Type(() => BankObjectDto)
  @ValidateNested({ each: true })
  @IsNotEmptyObject()
  @ApiProperty({
    type: BankObjectDto,
    example: {
      bankAcctNum: '12345678',
      bankRoutingNum: '325272212',
      bankAcctTypeCd: 'SAVINGS',
      bankName: 'Dep Bank'
    }
  })
  readonly depositBank: BankObjectDto

  @Type(() => BankObjectDto)
  @ValidateNested({ each: true })
  @IsNotEmptyObject()
  @ApiProperty({
    type: BankObjectDto,
    example: {
      bankAcctNum: '12345678',
      bankRoutingNum: '325272212',
      bankAcctTypeCd: 'SAVINGS',
      bankName: 'With Bank'
    }
  })
  readonly withdrawalBank: BankObjectDto
}

export class OwnerObjectDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'testing@test.com' })
  readonly ownerEmail: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'test testing' })
  readonly ownerName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'OWNER' })
  readonly ownerTitle: string
}

export class OwnerShipObjectDto {
  @Type(() => OwnerObjectDto)
  @ValidateNested({ each: true })
  @IsNotEmptyObject()
  @ApiProperty({ type: OwnerObjectDto })
  readonly owner: OwnerObjectDto
}

export class CreateMerchantDto {
  public salesCode: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'TestakaBusinessName' })
  readonly akaBusinessName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'TestdbaName' })
  readonly dbaName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'TestLegalBusinessName' })
  readonly legalBusinessName: string

  @Type(() => DemographicObjectDto)
  @ValidateNested({ each: true })
  @IsNotEmptyObject()
  @ApiProperty({ type: DemographicObjectDto })
  readonly demographic: DemographicObjectDto

  @Type(() => BankDetailObjectDto)
  @ValidateNested({ each: true })
  @IsNotEmptyObject()
  @ApiProperty({ type: BankDetailObjectDto })
  readonly bankDetail: BankDetailObjectDto

  @Type(() => OwnerShipObjectDto)
  @ValidateNested({ each: true })
  @IsNotEmptyObject()
  @ApiProperty({ type: OwnerShipObjectDto })
  readonly ownership: OwnerShipObjectDto
}
