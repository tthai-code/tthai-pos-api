import { ApiProperty } from '@nestjs/swagger'
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator'

export class GetAdminPaymentStatusDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string
}

export class UpdateBankAccountStatusDto {
  @IsBoolean()
  @IsNotEmpty()
  @ApiProperty({ example: true })
  readonly isBankAccountApproved: boolean
}
