import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class GetPaymentGatewayDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string
}

export class BinTokenDto extends GetPaymentGatewayDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<token>' })
  readonly token: string
}
