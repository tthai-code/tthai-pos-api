import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsNotEmpty, IsString, Length } from 'class-validator'
import { ACHBankAccountTypeEnum } from '../common/ach-bank.type.enum'
import * as dayjs from 'dayjs'

export class UpdateBankAccountDto {
  public restaurantId: string
  public token: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Some Account Name' })
  readonly name: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Authorized person name' })
  readonly printName: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Title' })
  readonly title: string

  @IsEnum(ACHBankAccountTypeEnum)
  @ApiProperty({
    example: ACHBankAccountTypeEnum.SAVING,
    enum: Object.values(ACHBankAccountTypeEnum)
  })
  readonly accountType: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '1234123412341234' })
  public accountNumber: string

  @IsString()
  @Length(9, 9)
  @IsNotEmpty()
  @ApiProperty({ example: '011401533' })
  public routingNumber: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Some Name' })
  readonly signature: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    example: dayjs().toISOString(),
    description: 'Format `ISO string`'
  })
  public date: string
}
