import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { PaymentGatewayStatusEnum } from '../common/payment-gateway.enum'

export type PaymentGatewayDocument = PaymentGatewayFields & Document

@Schema({ timestamps: true, strict: true, collection: 'payment-gateways' })
export class PaymentGatewayFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ default: null })
  mid: string

  @Prop({ default: null })
  username: string

  @Prop({ default: null })
  password: string

  @Prop({ default: false })
  isConvenienceFee: boolean

  @Prop({ default: 0 })
  convenienceFee: number

  @Prop({ default: false })
  isServiceCharge: boolean

  @Prop({ default: 10 })
  serviceCharge: number

  @Prop({ default: null })
  midCoPilot: string

  @Prop({
    enum: Object.values(PaymentGatewayStatusEnum),
    default: PaymentGatewayStatusEnum.INACTIVE
  })
  paymentStatus: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const PaymentGatewaySchema =
  SchemaFactory.createForClass(PaymentGatewayFields)
PaymentGatewaySchema.plugin(authorStampCreatePlugin)
