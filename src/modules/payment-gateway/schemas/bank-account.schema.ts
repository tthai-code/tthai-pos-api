import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import {
  ACHBankAccountTypeEnum,
  BankAccountStatusEnum
} from '../common/ach-bank.type.enum'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type BankAccountDocument = BankAccountFields & Document

@Schema({ timestamps: true, strict: true, collection: 'bankAccounts' })
export class BankAccountFields {
  @Prop({ type: MongooseSchema.Types.ObjectId })
  restaurantId: Types.ObjectId

  @Prop({ default: null })
  name: string

  @Prop({ default: null })
  printName: string

  @Prop({ default: null })
  title: string

  @Prop({
    enum: Object.values(ACHBankAccountTypeEnum),
    default: ACHBankAccountTypeEnum.SAVING
  })
  accountType: string

  @Prop({ default: null })
  accountNumber: string

  @Prop({ default: null })
  routingNumber: string

  @Prop({ default: null })
  signature: string

  @Prop({ default: null })
  date: Date

  @Prop({ default: null })
  token: string

  @Prop({
    default: BankAccountStatusEnum.ACTIVE,
    enum: Object.values(BankAccountStatusEnum)
  })
  bankAccountStatus: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const BankAccountSchema = SchemaFactory.createForClass(BankAccountFields)
BankAccountSchema.plugin(authorStampCreatePlugin)
