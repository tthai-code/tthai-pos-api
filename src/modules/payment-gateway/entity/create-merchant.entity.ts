import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class CreateMerchantField {
  @ApiProperty({
    example:
      'https://apply-uat.cardconnect.com/register?registrationKey=dc8d9cee-4eed-42b6-8f5e-b69c01c97b53&merchantId=103371000'
  })
  signature_url: string
}

export class CreateMerchantResponse extends ResponseDto<CreateMerchantField> {
  @ApiProperty({ type: CreateMerchantField })
  data: CreateMerchantField
}
