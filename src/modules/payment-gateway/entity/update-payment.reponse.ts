import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

export class UpdatePaymentResponse {
  @ApiProperty()
  restaurant_id: string

  @ApiProperty()
  mid: string

  @ApiProperty()
  username: string

  @ApiProperty()
  password: string

  @ApiProperty()
  is_convenience_fee: boolean

  @ApiProperty()
  convenience_fee: number

  @ApiProperty()
  is_service_charge: boolean

  @ApiProperty()
  service_charge: number
}

export class UpdatePaymentResponseDto extends ResponseDto<UpdatePaymentResponse> {
  @ApiProperty({
    type: UpdatePaymentResponse,
    example: {
      created_by: {
        username: 'system',
        id: '0'
      },
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      status: 'active',
      convenience_fee: 0.99,
      is_convenience_fee: true,
      service_charge: 0.99,
      is_service_charge: true,
      password: 'dGVzdDEyMzQ=',
      username: 'testing',
      mid: '820000003069',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      updated_at: '2022-10-09T20:00:19.328Z',
      created_at: '2022-10-09T19:56:13.672Z',
      id: '6343275d77f67acb062e51cc'
    }
  })
  data: UpdatePaymentResponse
}
