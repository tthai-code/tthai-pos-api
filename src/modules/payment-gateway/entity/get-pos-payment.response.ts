import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class PaymentGatewayFields {
  @ApiProperty()
  mid: string
  @ApiProperty()
  username: string
  @ApiProperty()
  password: string
  @ApiProperty()
  is_convenience_fee: boolean
  @ApiProperty()
  convenience_fee: number
}

export class PaymentGatewayResponse extends ResponseDto<PaymentGatewayFields> {
  @ApiProperty({
    type: PaymentGatewayFields,
    example: {
      mid: '820000003069',
      username: 'testing',
      password: 'testing123',
      is_convenience_fee: false,
      convenience_fee: 0
    }
  })
  data: PaymentGatewayFields
}
