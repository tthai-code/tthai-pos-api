import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { CoPilotStatusEnum } from '../common/co-pilot-status.enum'

class GetAdminPaymentStatusObject {
  @ApiProperty()
  mid_co_pilot: string
  @ApiProperty()
  is_bank_account_approved: boolean
  @ApiProperty()
  id: string
}

class GetCoPilotPaymentStatusObject {
  @ApiProperty()
  co_pilot_merchant_id: number
  @ApiProperty({ enum: Object.values(CoPilotStatusEnum) })
  boarding_process_status: string
}

export class GetAdminPaymentStatusResponse extends ResponseDto<GetAdminPaymentStatusObject> {
  @ApiProperty({
    type: GetAdminPaymentStatusObject,
    example: {
      mid_co_pilot: '103527650',
      is_bank_account_approved: true,
      id: '63984d6690416c27a741592a'
    }
  })
  data: GetAdminPaymentStatusObject
}

export class GetCoPilotPaymentStatusResponse extends ResponseDto<GetCoPilotPaymentStatusObject> {
  @ApiProperty({
    type: GetCoPilotPaymentStatusObject,
    example: {
      co_pilot_merchant_id: '103527650',
      boarding_process_status: 'Pending Signature.'
    }
  })
  data: GetCoPilotPaymentStatusObject
}

class PaymentGatewaySettingObject {
  mid: string
  username: string
  password: string
  is_convenience_fee: boolean
  convenience_fee: number
}

export class GetPaymentGatewayResponse extends ResponseDto<PaymentGatewaySettingObject> {
  @ApiProperty({
    type: PaymentGatewaySettingObject,
    example: {
      mid: '820000003069',
      username: 'testing',
      password: 'testing123',
      is_convenience_fee: true,
      convenience_fee: 0.99
    }
  })
  data: PaymentGatewaySettingObject
}
