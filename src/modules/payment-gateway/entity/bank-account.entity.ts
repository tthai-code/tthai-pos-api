import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import {
  ACHBankAccountTypeEnum,
  BankAccountStatusEnum
} from '../common/ach-bank.type.enum'

class BankAccountField {
  @ApiProperty({ enum: Object.values(BankAccountStatusEnum) })
  bank_account_status: string
  @ApiProperty()
  token: string
  @ApiProperty({ description: 'Format ISO Date' })
  date: string
  @ApiProperty()
  signature: string
  @ApiProperty()
  routing_number: string
  @ApiProperty()
  account_number: string
  @ApiProperty({ enum: Object.values(ACHBankAccountTypeEnum) })
  account_type: string
  @ApiProperty()
  title: string
  @ApiProperty()
  print_name: string
  @ApiProperty()
  name: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
}

export class GetActiveBankAccountResponse extends ResponseDto<BankAccountField> {
  @ApiProperty({
    type: BankAccountField,
    example: {
      bank_account_status: 'ACTIVE',
      token: '9031395982141234',
      date: '2022-11-24T10:48:15.658Z',
      signature: 'Cory Wong 1',
      routing_number: 'XXXXXX808',
      account_number: 'XXXXXXXXXXXX1234',
      account_type: 'SAVING',
      title: 'Mr.',
      print_name: 'Cory Wong',
      name: 'Cory Wong 1',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      id: '63844b25c05b5be2ac6c6657'
    }
  })
  data: BankAccountField
}

export class GetBankAccountResponse extends ResponseDto<BankAccountField[]> {
  @ApiProperty({
    type: [BankAccountField],
    example: [
      {
        bank_account_status: 'ACTIVE',
        token: '9031395982141234',
        date: '2022-11-24T10:48:15.658Z',
        signature: 'Cory Wong',
        routing_number: 'XXXXXX808',
        account_number: 'XXXXXXXXXXXX1234',
        account_type: 'SAVING',
        title: 'Mr.',
        print_name: 'Cory Wong',
        name: 'Cory Wong',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        id: '63844ba6438534c4ecaf0b49'
      },
      {
        bank_account_status: 'INACTIVE',
        token: '9031395982141234',
        date: '2022-11-24T10:48:15.658Z',
        signature: 'Cory Wong 1',
        routing_number: 'XXXXXX808',
        account_number: 'XXXXXXXXXXXX1234',
        account_type: 'SAVING',
        title: 'Mr.',
        print_name: 'Cory Wong',
        name: 'Cory Wong 1',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        id: '63844b25c05b5be2ac6c6657'
      }
    ]
  })
  data: BankAccountField[]
}

export class UpdateBankAccountResponse extends ResponseDto<BankAccountField> {
  @ApiProperty({
    type: BankAccountField,
    example: {
      bank_account_status: 'ACTIVE',
      token: '9031395982141234',
      date: '2022-11-24T10:48:15.658Z',
      signature: 'Cory Wong',
      routing_number: 'XXXXXX808',
      account_number: 'XXXXXXXXXXXX1234',
      account_type: 'SAVING',
      title: 'Mr.',
      print_name: 'Cory Wong',
      name: 'Cory Wong',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      id: '63844ba6438534c4ecaf0b49'
    }
  })
  data: BankAccountField
}
