import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class SignerField {
  @ApiProperty()
  email: string
  @ApiProperty()
  firstName: string
  @ApiProperty()
  lastName: string
  @ApiProperty()
  ipAddress: string
  @ApiProperty()
  personalGuaranteeFlg: boolean
}

class SignatureStatusField {
  @ApiProperty()
  signedSiteUserId: number
  @ApiProperty()
  signedDateTime: string
  @ApiProperty()
  signer: SignerField
  @ApiProperty()
  signatureUrl: string
  @ApiProperty()
  signatureStatusCd: string
}

class MerchantStatusField {
  @ApiProperty()
  errors: string
  @ApiProperty()
  signatureStatus: SignatureStatusField
}

export class SignatureStatusResponse extends ResponseDto<MerchantStatusField> {
  @ApiProperty({
    type: MerchantStatusField,
    example: {
      errors: null,
      signatureStatus: {
        signedSiteUserId: 237322,
        signedDateTime: '07/19/2017 04:44:28 PM',
        signer: {
          email: 'name@email.com',
          firstName: 'Forename',
          lastName: 'Surname',
          ipAddress: '192.168.125.126',
          personalGuaranteeFlg: true
        },
        signatureUrl:
          'https://cardpointe-uat.cardconnect.com/account/login#/password/02d34909-93b3-4b50-adf4-1ff91e92b33c/c08709da-8af1-4f35-8e0f-f52eca60ef81?newuser',
        signatureStatusCd: 'SIGNED'
      }
    }
  })
  data: MerchantStatusField
}
