import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

export class PaymentResponse {
  @ApiProperty()
  mid: string

  @ApiProperty()
  username: string

  @ApiProperty()
  password: string

  @ApiProperty()
  is_convenience_fee: boolean

  @ApiProperty()
  convenience_fee: number

  @ApiProperty()
  is_service_charge: boolean

  @ApiProperty()
  service_charge: number
}

export class GetPaymentResponse extends ResponseDto<PaymentResponse> {
  @ApiProperty({
    type: PaymentResponse,
    example: {
      mid: '820000003069',
      username: 'testing',
      password: 'dGVzdDEyMzQ=',
      is_convenience_fee: true,
      convenience_fee: 0.99,
      is_service_charge: true,
      service_charge: 0.99
    }
  })
  data: PaymentResponse
}
