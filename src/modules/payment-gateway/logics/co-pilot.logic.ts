import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CreateMerchantDto } from '../dto/create-merchant.dto'
import { ICreateMerchant } from '../interfaces/co-pilot.interface'
import { CoPilotService } from '../services/co-pilot.service'
import { PaymentGatewayService } from '../services/payment-gateway.service'

@Injectable()
export class CoPilotLogic {
  constructor(
    private readonly coPilotService: CoPilotService,
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly restaurantService: RestaurantService
  ) {}

  async createMerchant(merchant: CreateMerchantDto, restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const payment = await this.paymentGatewayService.findOne({ restaurantId })
    if (payment.midCoPilot) {
      throw new BadRequestException('This merchant already submit.')
    }
    const payload: ICreateMerchant = {
      merchant
    }
    const { data: token } = await this.coPilotService.tokenize()
    const accessToken = token['access_token']
    const { data } = await this.coPilotService.createMerchantMinimum(
      payload,
      accessToken
    )
    if (!data.merchantId) throw new BadRequestException()
    const { data: signature } = await this.coPilotService.signatureMerchant(
      data.merchantId,
      accessToken
    )
    if (!signature.signatureUrl) throw new BadRequestException()
    await this.paymentGatewayService.findOneAndUpdate(
      { restaurantId },
      { midCoPilot: data.merchantId }
    )
    return { signatureUrl: signature.signatureUrl }
  }

  async getMerchantSignatureStatus(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const payment = await this.paymentGatewayService.findOne({ restaurantId })
    if (!payment.midCoPilot) {
      throw new BadRequestException('This merchant is not submit yet.')
    }
    const { data: token } = await this.coPilotService.tokenize()
    const accessToken = token['access_token']
    const { data } = await this.coPilotService.getMerchantSignatureStatus(
      payment.midCoPilot,
      accessToken
    )
    if (!data) throw new BadRequestException()
    return data
  }
}
