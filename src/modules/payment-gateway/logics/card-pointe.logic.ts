import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CardTypeEnum } from 'src/modules/transaction/common/card-type.enum'
import { PaymentMethodEnum } from 'src/modules/transaction/common/payment-method.enum'
import { AuthNoCaptureDto, AuthWithCaptureDto } from '../dto/auth.dto'
import { TokenizeCardDto } from '../dto/tokenize.dto'
import {
  ICapturePartialAmount,
  IOnlinePayment
} from '../interfaces/card-pointe.interface'
import { PaymentGatewayHttpService } from '../services/payment-gateway-http.service'
import { PaymentGatewayService } from '../services/payment-gateway.service'
import { IPaymentGateway, PaymentGatewayLogic } from './payment-gateway.logic'

@Injectable()
export class CardPointeLogic {
  constructor(
    private readonly paymentService: PaymentGatewayService,
    private readonly paymentLogic: PaymentGatewayLogic,
    private readonly httpService: PaymentGatewayHttpService,
    private readonly restaurantService: RestaurantService
  ) {}

  public identifyPaymentMethod(product: string, paymentMethod: string) {
    let cardType
    if (product === 'A') {
      cardType = CardTypeEnum.AMEX
    } else if (product === 'D') {
      cardType = CardTypeEnum.DISCOVER
    } else if (product === 'M') {
      cardType = CardTypeEnum.MASTERCARD
    } else if (product === 'V') {
      cardType = CardTypeEnum.VISA
    } else {
      cardType = CardTypeEnum.NON_CO_BRAND
    }

    let method
    const regex = /credit/i.test(paymentMethod)
    if (regex) {
      method = PaymentMethodEnum.CREDIT
    } else {
      method = PaymentMethodEnum.DEBIT
    }
    return { cardType, paymentMethod: method }
  }

  public async findOneRestaurantPayment(
    restaurantId
  ): Promise<IPaymentGateway> {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return this.paymentLogic.getPaymentGatewayWithDecode(restaurantId)
  }

  async tokenizeAndBinToken(payload: TokenizeCardDto) {
    const payment = await this.findOneRestaurantPayment(payload.restaurantId)
    const { username, password, mid } = payment
    const auth = { username, password }
    const { data: token } = await this.httpService.tokenizeCard(
      payload.account,
      auth
    )
    const { data: binToken } = await this.httpService.binToken(
      mid,
      token.token,
      auth
    )
    return {
      ending: payload.account.slice(-4),
      token: token.token,
      binToken
    }
  }

  async authorizeNoCapture(payload: AuthNoCaptureDto) {
    return payload
  }

  async authorizeWithCapture(payload: AuthWithCaptureDto) {
    const payment = await this.findOneRestaurantPayment(payload.restaurantId)
    const tokenized = await this.tokenizeAndBinToken({
      restaurantId: payload.restaurantId,
      account: payload.account
    })
    delete payload.restaurantId
    payload.merchid = payment.mid
    payload.account = tokenized.token
    payload.ecomind = 'E'
    payload.capture = 'Y'

    const auth = { username: payment.username, password: payment.password }
    return this.httpService.authorization(payload, auth)
  }

  async voidTransaction(retRef: string, restaurantId: string) {
    const payment = await this.findOneRestaurantPayment(restaurantId)
    const { username, password, mid } = payment
    return this.httpService.voidRetRef(
      {
        merchid: mid,
        retref: retRef
      },
      { username, password }
    )
  }

  async closeBatch(restaurantId: string, batchId?: number) {
    const payment = await this.findOneRestaurantPayment(restaurantId)
    const { username, password, mid } = payment
    return this.httpService.closeBatch(mid, { username, password }, batchId)
  }

  async refundTransaction(
    retRef: string,
    restaurantId: string,
    amount?: number
  ) {
    const payment = await this.findOneRestaurantPayment(restaurantId)
    const { username, password, mid } = payment
    const payload = { merchid: mid, retref: retRef, amount: amount.toFixed(2) }
    if (!amount) delete payload.amount
    return this.httpService.refundRetRef(payload, { username, password })
  }

  async captureTransaction(
    retRef: string,
    restaurantId: string,
    amount?: number
  ) {
    const payment = await this.findOneRestaurantPayment(restaurantId)
    const { username, password, mid } = payment
    const payload: ICapturePartialAmount = {
      retref: retRef,
      merchid: mid,
      amount
    }
    return this.httpService.capture(payload, { username, password })
  }

  async payOnlineTransaction(restaurantId: string, payload: IOnlinePayment) {
    const payment = await this.findOneRestaurantPayment(restaurantId)
    const { username, password, mid } = payment
    payload.merchid = mid
    payload.ecomind = 'E'
    payload.capture = 'Y'
    payload.bin = 'Y'
    const auth = { username, password }
    const { data } = await this.httpService.authorization(payload, auth)
    if (!['00', '000'].includes(data.respcode)) {
      throw new BadRequestException(data.resptext)
    }
    return data
  }
}
