import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { PaymentGatewayStatusEnum } from '../common/payment-gateway.enum'
import { UpdatePaymentDto } from '../dto/update-payment.dto'
import { PaymentGatewayDocument } from '../schemas/payment-gateway.schema'
import { PaymentGatewayHttpService } from '../services/payment-gateway-http.service'
import { PaymentGatewayService } from '../services/payment-gateway.service'

export interface IPaymentGateway {
  mid: string
  username: string
  password: string
  isConvenienceFee: boolean
  convenienceFee: number
  isServiceCharge: boolean
  serviceCharge: number
  midCoPilot?: string
  paymentStatus?: string
}

@Injectable()
export class PaymentGatewayLogic {
  constructor(
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly restaurantService: RestaurantService,
    private readonly httpService: PaymentGatewayHttpService
  ) {}

  private stringToBase64(text): string {
    const encode = Buffer.from(text).toString('base64')
    return encode
  }

  private decodeBase64toString(text): string {
    const decode = Buffer.from(text, 'base64').toString('utf-8')
    return decode
  }

  async getPaymentGateway(id: string): Promise<IPaymentGateway> {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    let payment = await this.paymentGatewayService.findOne({
      restaurantId: id
    })
    if (!payment) {
      await this.paymentGatewayService.create({ restaurantId: id })
      payment = await this.paymentGatewayService.findOne({
        restaurantId: id
      })
    }
    return {
      mid: payment.mid,
      username: payment.username,
      password: payment.password,
      isConvenienceFee: payment.isConvenienceFee,
      convenienceFee: payment.convenienceFee,
      isServiceCharge: payment.isServiceCharge,
      serviceCharge: payment.serviceCharge,
      midCoPilot: payment.midCoPilot,
      paymentStatus: payment.paymentStatus
    }
  }

  async updatePaymentGateway(
    id: string,
    payload: UpdatePaymentDto
  ): Promise<PaymentGatewayDocument> {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const { data } = await this.httpService.validateMerchantId(payload.mid, {
      username: payload.username,
      password: payload.password
    })
    if (data.merchid) payload.paymentStatus = PaymentGatewayStatusEnum.ACTIVE
    payload.password = this.stringToBase64(payload.password)
    payload.restaurantId = id
    return this.paymentGatewayService.findOneAndUpdate(
      { restaurantId: id },
      payload
    )
  }

  async getPaymentGatewayWithDecode(
    restaurantId: string
  ): Promise<IPaymentGateway> {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: { $ne: StatusEnum.DELETED }
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    let payment = await this.paymentGatewayService.findOne({
      restaurantId: restaurantId
    })
    if (!payment) {
      await this.paymentGatewayService.create({ restaurantId })
      payment = await this.paymentGatewayService.findOne({
        restaurantId: restaurantId
      })
    }
    return {
      mid: payment.mid,
      username: payment.username,
      password: payment.password
        ? this.decodeBase64toString(payment.password)
        : '',
      isConvenienceFee: payment.isConvenienceFee,
      convenienceFee: payment.convenienceFee,
      isServiceCharge: payment.isServiceCharge,
      serviceCharge: payment.serviceCharge
    }
  }
}
