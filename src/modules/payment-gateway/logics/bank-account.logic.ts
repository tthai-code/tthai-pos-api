import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { ClientSession } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { SubscriptionLogic } from 'src/modules/subscription/logic/subscription.logic'
import { BankAccountStatusEnum } from '../common/ach-bank.type.enum'
import { UpdateBankAccountDto } from '../dto/bank-account.dto'
import { BankAccountService } from '../services/bank-account.service'
import { PaymentGatewayHttpService } from '../services/payment-gateway-http.service'
import { PaymentGatewayLogic } from './payment-gateway.logic'

@Injectable()
export class BankAccountLogic {
  constructor(
    private readonly bankAccountService: BankAccountService,
    private readonly restaurantService: RestaurantService,
    private readonly cardPointeService: PaymentGatewayHttpService,
    private readonly paymentLogic: PaymentGatewayLogic,
    private readonly subscriptionLogic: SubscriptionLogic
  ) {}

  private censorText(text: string, last: number) {
    const length = text.length
    return text.slice(-last).padStart(length, 'X')
  }

  async getBankAccount(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const bankAccount = await this.bankAccountService.findOne(
      { restaurantId, bankAccountStatus: BankAccountStatusEnum.ACTIVE },
      { status: 0, updatedBy: 0, updatedAt: 0, createdBy: 0, createdAt: 0 }
    )
    return bankAccount
  }

  async getBankAccounts(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const bankAccount = await this.bankAccountService.getAll(
      { restaurantId },
      { status: 0, updatedBy: 0, updatedAt: 0, createdBy: 0, createdAt: 0 }
    )
    return bankAccount
  }

  async getBankAccountForAdmin(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: { $ne: StatusEnum.DELETED }
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const bankAccount = await this.bankAccountService.getAll(
      { restaurantId },
      { date: 1 }
    )
    return bankAccount
  }

  async getBankAccountById(id: string) {
    return this.bankAccountService.findOne(
      { _id: id },
      { status: 0, updatedBy: 0, updatedAt: 0, createdBy: 0, createdAt: 0 }
    )
  }

  async updateBankAccount(restaurantId: string, payload: UpdateBankAccountDto) {
    const logic = async (session: ClientSession) => {
      const payment = this.cardPointeService.getTThaiMerchant()
      const account = `${payload.routingNumber}/${payload.accountNumber}`
      const { username, password } = payment
      const auth = {
        username,
        password
      }
      const { data } = await this.cardPointeService.tokenizeCard(account, auth)
      if (!data.token) throw new BadRequestException(data)
      const { token } = data
      payload.accountNumber = this.censorText(payload.accountNumber, 4)
      payload.routingNumber = this.censorText(payload.routingNumber, 3)
      payload.token = token
      payload.restaurantId = restaurantId
      const updated = await this.bankAccountService.transactionCreate(
        payload,
        session
      )
      if (!updated) throw new BadRequestException()
      await this.bankAccountService.transactionUpdateMany(
        { _id: { $ne: updated._id }, restaurantId },
        { bankAccountStatus: BankAccountStatusEnum.INACTIVE },
        session
      )
      return updated
    }
    const updated = await runTransaction(logic)
    await this.subscriptionLogic.initDefaultPackage(restaurantId)
    return this.bankAccountService.findOne(
      { _id: updated._id },
      { status: 0, updatedBy: 0, updatedAt: 0, createdBy: 0, createdAt: 0 }
    )
  }
}
