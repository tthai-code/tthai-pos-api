import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { UpdateBankAccountStatusDto } from '../dto/admin-update-payment.dto'
import { PaymentGatewayService } from '../services/payment-gateway.service'
import { CoPilotLogic } from './co-pilot.logic'
import { PaymentGatewayLogic } from './payment-gateway.logic'

@Injectable()
export class AdminPaymentLogic {
  constructor(
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly paymentGatewayLogic: PaymentGatewayLogic,
    private readonly restaurantService: RestaurantService,
    private readonly coPilotLogic: CoPilotLogic
  ) {}

  private displayCoPilotStatus(status = '') {
    switch (status) {
      case 'INPROG':
        return 'Not Submitted.'
      case 'OFS':
        return 'Pending Signature.'
      case 'QUALIFY':
        return 'Qualifying.'
      case 'UNDER':
        return 'Underwriting.'
      case 'DECLINED':
        return 'Declined.'
      case 'BOARDING':
        return 'Boarding'
      case 'BOARDED':
        return 'Boarded'
      case 'LIVE':
        return 'Live'
      case 'CANCELLED':
        return 'Cancelled'
      default:
        return '-'
    }
  }

  async getPaymentStatus(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: { $ne: StatusEnum.DELETED }
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const payment = await this.paymentGatewayService.findOne(
      { restaurantId },
      { isBankAccountApproved: 1, midCoPilot: 1 }
    )
    if (!payment) {
      return {
        isBankAccountApproved: false,
        midCoPilot: null
      }
    }
    return payment
  }

  async updateBankAccountApproved(
    id: string,
    payload: UpdateBankAccountStatusDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    await this.paymentGatewayService.findOneAndUpdate(
      { restaurantId: id },
      payload
    )
    return { success: true }
  }

  async getCoPilotStatus(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const data = await this.coPilotLogic.getMerchantSignatureStatus(
      restaurantId
    )
    const { merchant } = data
    const result = {
      coPilotMerchantId: merchant?.merchantId,
      boardingProcessStatus: this.displayCoPilotStatus(
        merchant?.merchantStatus?.boardingProcessStatusCd
      )
    }
    return result
  }

  async updatePaymentMerchantId(restaurantId: string, payload: any) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    await this.paymentGatewayService.findOneAndUpdate({ restaurantId }, payload)
    return { success: true }
  }
}
