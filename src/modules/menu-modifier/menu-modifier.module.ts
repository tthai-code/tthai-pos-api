import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { ModifierModule } from '../modifier/modifier.module'
import { MenuModifierController } from './controllers/menu-modifier.controller'
import { MenuModifierOnlineController } from './controllers/menu-modifier-public-online.controller'
import { MenuModifierLogic } from './logics/menu-modifier.logic'
import { MenuModifierSchema } from './schemas/menu-modifier.schema'
import { MenuModifierService } from './services/menu-modifier.service'
import { MenuModule } from '../menu/menu.module'

@Module({
  imports: [
    forwardRef(() => MenuModule),
    forwardRef(() => ModifierModule),
    MongooseModule.forFeature([
      { name: 'menuModifier', schema: MenuModifierSchema }
    ])
  ],
  providers: [MenuModifierService, MenuModifierLogic],
  controllers: [MenuModifierController, MenuModifierOnlineController],
  exports: [MenuModifierService]
})
export class MenuModifierModule {}
