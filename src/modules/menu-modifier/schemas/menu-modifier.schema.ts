import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { ModifierTypeEnum } from 'src/modules/modifier/common/modifier-type.enum'

export type MenuModifierDocument = MenuModifierFields & Document

@Schema({ _id: false, timestamps: false, strict: true })
class ModifierOptions {
  @Prop()
  name: string

  @Prop()
  nativeName: string

  @Prop({ default: 0 })
  price: number
}

@Schema({ timestamps: true, strict: true, collection: 'menuModifiers' })
export class MenuModifierFields {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: 'menu'
  })
  menuId: Types.ObjectId

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    ref: 'modifier'
  })
  modifierId: Types.ObjectId

  @Prop()
  label: string

  @Prop()
  abbreviation: string

  @Prop()
  items: Array<ModifierOptions>

  @Prop({ enum: Object.values(ModifierTypeEnum) })
  type: string

  @Prop({ default: null })
  maxSelected: number

  @Prop({ default: 999 })
  position: number

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const MenuModifierSchema =
  SchemaFactory.createForClass(MenuModifierFields)
MenuModifierSchema.plugin(mongoosePaginate)
MenuModifierSchema.plugin(authorStampCreatePlugin)
