import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { MenuService } from 'src/modules/menu/services/menu.service'
import { ModifierService } from 'src/modules/modifier/services/modifier.service'
import {
  AddBulkMenuModifiersDto,
  CreateMenuModifierDto
} from '../dto/create-menu-modifier.dto'
import { DeleteManyMenuModifiersDto } from '../dto/delete-menu-modifier.dto'
import { MenuModifierService } from '../services/menu-modifier.service'

@Injectable()
export class MenuModifierLogic {
  constructor(
    private readonly modifierService: ModifierService,
    private readonly menuModifierService: MenuModifierService,
    private readonly menuService: MenuService
  ) {}

  async CreateMenuModifier(createMenuModifier: CreateMenuModifierDto) {
    const isExist = await this.menuModifierService.findOne({
      menuId: createMenuModifier.menuId,
      modifierId: createMenuModifier.modifierId,
      status: { $ne: StatusEnum.DELETED }
    })
    if (isExist) throw new BadRequestException('Menu Modifier is exist.')
    const modifier = await this.modifierService.getModel().findOne(
      {
        _id: createMenuModifier.modifierId
      },
      {
        label: 1,
        abbreviation: 1,
        type: 1,
        items: 1,
        maxSelected: 1
      }
    )
    const created = await this.menuModifierService.create({
      modifierId: modifier._id,
      menuId: createMenuModifier.menuId,
      label: modifier.label,
      abbreviation: modifier.abbreviation,
      type: modifier.type,
      items: modifier.items,
      maxSelected: modifier.maxSelected,
      status: createMenuModifier.status ? createMenuModifier.status : 'active'
    })
    return created
  }

  async addBulkMenuModifiers(menuId: string, payload: AddBulkMenuModifiersDto) {
    const menu = await this.menuService.findOne({
      _id: menuId,
      status: StatusEnum.ACTIVE
    })
    if (!menu) throw new NotFoundException('Menu not found.')
    const { items } = payload
    const modifierIds = items.map((item) => item.modifierId)
    const menuModifiers = await this.menuModifierService.getAll({
      _id: { $in: modifierIds },
      status: StatusEnum.ACTIVE
    })
    if (menuModifiers.length > 0) {
      const modifierNames = menuModifiers.map((item) => item.label)
      throw new BadRequestException(
        `Modifiers Label (${modifierNames.join(',')}) are already exist.`
      )
    }
    const modifiers = await this.modifierService.getAll({
      _id: { $in: modifierIds },
      status: StatusEnum.ACTIVE
    })
    const existModifiers = modifiers.filter((item) =>
      modifierIds.includes(item.id)
    )
    if (existModifiers.length > 0) {
      const createPayload = existModifiers.map((item) => ({
        menuId,
        modifierId: item.id,
        label: item.label,
        abbreviation: item.abbreviation,
        type: item.type,
        items: item.items,
        maxSelected: item.maxSelected,
        position: item.position
      }))
      await this.menuModifierService.createMany(createPayload)
    }

    return { success: true }
  }

  async deleteBulkMenuModifier(
    menuId: string,
    payload: DeleteManyMenuModifiersDto
  ) {
    const menu = await this.menuService.findOne({
      _id: menuId,
      status: StatusEnum.ACTIVE
    })
    if (!menu) throw new NotFoundException('Menu not found.')
    const { items } = payload
    const menuModifiers = await this.menuModifierService.getAll({
      _id: { $in: items },
      status: StatusEnum.ACTIVE
    })
    if (menuModifiers.length > 0) {
      const menuModifierIds = menuModifiers.map((item) => item.id)
      await this.menuModifierService.deleteMany({
        _id: { $in: menuModifierIds },
        status: StatusEnum.ACTIVE
      })
    }

    return { success: true }
  }
}
