import { Controller, Get, Param } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { GetAllMenuModifierResponse } from '../entitiy/menu-modifier.entity'
import { MenuModifierLogic } from '../logics/menu-modifier.logic'
import { MenuModifierService } from '../services/menu-modifier.service'

@ApiBearerAuth()
@ApiTags('menu-modifier-online')
@Controller('v1/public/online/menu-modifier')
export class MenuModifierOnlineController {
  constructor(
    private readonly menuModifierService: MenuModifierService,
    private readonly menuModifierLogic: MenuModifierLogic
  ) {}

  @Get(':menuId')
  @ApiOperation({ summary: 'Get Menu Modifier List By Menu ID' })
  @ApiOkResponse({ type: () => GetAllMenuModifierResponse })
  async getMenus(@Param('menuId') menuId: string) {
    return await this.menuModifierService.getAll(
      {
        menuId,
        status: StatusEnum.ACTIVE
      },
      {
        items: 1,
        maxSelected: 1,
        type: 1,
        label: 1,
        abbreviation: 1,
        id: 1
      },
      {
        sort: {
          type: -1,
          position: 1
        }
      }
    )
  }
}
