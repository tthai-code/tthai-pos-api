import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import {
  AddBulkMenuModifiersDto,
  CreateMenuModifierDto
} from '../dto/create-menu-modifier.dto'
import { DeleteManyMenuModifiersDto } from '../dto/delete-menu-modifier.dto'
import { GetAllMenuModifierResponse } from '../entitiy/menu-modifier.entity'
import { MenuModifierLogic } from '../logics/menu-modifier.logic'
import { MenuModifierService } from '../services/menu-modifier.service'

@ApiBearerAuth()
@ApiTags('menu-modifier')
@Controller('v1/menu-modifier')
@UseGuards(AdminAuthGuard)
export class MenuModifierController {
  constructor(
    private readonly menuModifierService: MenuModifierService,
    private readonly menuModifierLogic: MenuModifierLogic
  ) {}

  @Get(':menuId')
  @ApiOperation({ summary: 'Get Menu Modifier List By Menu ID' })
  @ApiOkResponse({ type: () => GetAllMenuModifierResponse })
  async getMenus(@Param('menuId') menuId: string) {
    return await this.menuModifierService.getAll(
      {
        menuId,
        status: StatusEnum.ACTIVE
      },
      {
        items: 1,
        maxSelected: 1,
        type: 1,
        label: 1,
        abbreviation: 1,
        modifierId: 1,
        id: 1,
        position: 1
      },
      {
        sort: {
          type: -1,
          position: 1
        }
      }
    )
  }

  @Post()
  @ApiOperation({ summary: 'Create Menu Modifier' })
  async createMenu(@Body() modifier: CreateMenuModifierDto) {
    return this.menuModifierLogic.CreateMenuModifier(modifier)
  }

  @Patch(':id/active')
  @ApiOperation({ summary: 'Active Menu Modifier By Id' })
  async activeMenuModifier(@Param('id') id: string) {
    return await this.menuModifierService.update(id, {
      status: StatusEnum.ACTIVE
    })
  }

  @Patch(':id/inactive')
  @ApiOperation({ summary: 'Active Menu Modifier By Id' })
  async inactiveMenuModifier(@Param('id') id: string) {
    return await this.menuModifierService.update(id, {
      status: StatusEnum.INACTIVE
    })
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete Menu Modifier' })
  async deleteMenu(@Param('id') id: string) {
    return this.menuModifierService.delete(id)
  }

  @Post(':menuId/add-bulk')
  @ApiOperation({
    summary: 'Add Bulk Menu Modifiers',
    description: 'use bearer `restaurant_token`'
  })
  async addBulkMenuModifiers(
    @Param('menuId') menuId: string,
    @Body() payload: AddBulkMenuModifiersDto
  ) {
    return await this.menuModifierLogic.addBulkMenuModifiers(menuId, payload)
  }

  @Put(':menuId/delete-bulk')
  @ApiOperation({
    summary: 'Delete Many Menu Modifier',
    description: 'use bearer `restaurant_token`'
  })
  async deleteManyModifier(
    @Param('menuId') menuId: string,
    @Body() payload: DeleteManyMenuModifiersDto
  ) {
    return await this.menuModifierLogic.deleteBulkMenuModifier(menuId, payload)
  }
}
