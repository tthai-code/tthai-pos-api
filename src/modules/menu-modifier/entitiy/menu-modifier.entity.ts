import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { ModifierTypeEnum } from 'src/modules/modifier/common/modifier-type.enum'

class ItemsResponseFields {
  @ApiProperty()
  name: string

  @ApiProperty()
  native_name: string

  @ApiProperty()
  price: number
}

export class MenuModifierResponseFields {
  @ApiProperty({ enum: Object.values(ModifierTypeEnum) })
  type: string

  @ApiProperty({ type: ItemsResponseFields })
  items: Array<ItemsResponseFields>

  @ApiProperty()
  label: string

  @ApiProperty()
  abbreviation: string

  @ApiProperty()
  id: string
}

export class GetAllMenuModifierResponse extends ResponseDto<
  Array<MenuModifierResponseFields>
> {
  @ApiProperty({
    type: MenuModifierResponseFields,
    example: {
      message: 'done',
      data: [
        {
          type: 'REQUIRED',
          items: [
            {
              name: 'Not Spicy',
              native_name: 'Not Spicy',
              price: 0
            },
            {
              name: 'Quite Spicy',
              native_name: 'Quite Spicy',
              price: 0
            },
            {
              name: 'Spicy',
              native_name: 'Spicy',
              price: 0
            }
          ],
          label: 'Spicy Choices',
          id: '630fc086af525d6ed9fc0d61'
        },
        {
          type: 'REQUIRED',
          items: [
            {
              name: 'Beef',
              native_name: 'Beef',
              price: 2.5
            },
            {
              name: 'Pork',
              native_name: 'Pork',
              price: 1
            },
            {
              name: 'Chicken',
              native_name: 'Chicken',
              price: 0
            }
          ],
          label: 'Protein Choices',
          abbreviation: 'P',
          id: '630fcd08b6fb5ee3bab46885'
        }
      ]
    }
  })
  data: Array<MenuModifierResponseFields>
}
