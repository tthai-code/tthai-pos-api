import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { MenuModifierDocument } from '../schemas/menu-modifier.schema'
import { Model } from 'mongoose'
import { ClientSession } from 'mongodb'
import { MenuModifierPaginateDto } from '../dto/get-menu-modifier.dto'

@Injectable({ scope: Scope.REQUEST })
export class MenuModifierService {
  constructor(
    @InjectModel('menuModifier')
    private readonly MenuModifierModel:
      | PaginateModel<MenuModifierDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<MenuModifierDocument | any> {
    return this.MenuModifierModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.MenuModifierModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<MenuModifierDocument> {
    return this.MenuModifierModel
  }

  async create(payload: any): Promise<MenuModifierDocument> {
    const document = new this.MenuModifierModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, menu: any): Promise<MenuModifierDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...menu }).save()
  }

  async transactionCreateMany(
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.MenuModifierModel.insertMany(payload, { session })
  }

  async createMany(payload: any[]): Promise<any> {
    return this.MenuModifierModel.insertMany(payload)
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<MenuModifierDocument> {
    return this.MenuModifierModel.updateMany(
      condition,
      { $set: { ...payload } },
      {
        session,
        upsert: true,
        new: true
      }
    )
  }

  async delete(id: string): Promise<MenuModifierDocument> {
    return this.MenuModifierModel.deleteOne({ _id: id })
  }

  async deleteMany(condition): Promise<any> {
    return this.MenuModifierModel.deleteMany(condition)
  }

  getAll(condition, project?: any, options?: any): Promise<any[]> {
    return this.MenuModifierModel.find(condition, project, options)
  }

  findById(id): Promise<any> {
    return this.MenuModifierModel.findById(id)
  }

  findOne(condition): Promise<MenuModifierDocument> {
    return this.MenuModifierModel.findOne(condition)
  }

  paginate(
    query: any,
    queryParam: MenuModifierPaginateDto
  ): Promise<PaginateResult<MenuModifierDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder }
    }

    return this.MenuModifierModel.paginate(query, options)
  }
}
