import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  ArrayNotEmpty,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'

export class CreateMenuModifierDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '630a6824a411620066d39161' })
  readonly menuId: string

  @IsString()
  @ApiProperty({ example: '6308974b19416019c26ae15d' })
  readonly modifierId: string

  @IsString()
  @IsOptional()
  @IsEnum(StatusEnum)
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string
}

export class AddMenuModifierObjectDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '6308974b19416019c26ae15d' })
  readonly modifierId: string
}

export class AddBulkMenuModifiersDto {
  @Type(() => AddMenuModifierObjectDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: [AddMenuModifierObjectDto] })
  readonly items: AddMenuModifierObjectDto[]
}
