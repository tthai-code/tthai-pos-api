import { ApiProperty } from '@nestjs/swagger'
import { ArrayNotEmpty, IsString } from 'class-validator'

export class DeleteManyMenuModifiersDto {
  @IsString({ each: true })
  @ArrayNotEmpty()
  @ApiProperty({ example: ['<menu-modifier-id>', '<menu-modifier-id1>'] })
  readonly items: string[]
}
