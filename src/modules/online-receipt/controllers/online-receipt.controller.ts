import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Put,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { UpdateOnlineReceiptDto } from '../dto/update-online-receipt.dto'
import { GetOnlineReceiptResponse } from '../entity/online-receipt.entity'
import { OnlineReceiptService } from '../services/online-receipt.service'

@ApiBearerAuth()
@ApiTags('online-receipt-setting')
@UseGuards(AdminAuthGuard)
@Controller('v1/online-receipt')
export class OnlineReceiptController {
  constructor(
    private readonly onlineReceiptService: OnlineReceiptService,
    private readonly restaurantService: RestaurantService
  ) {}

  @Get(':restaurantId')
  @ApiOkResponse({ type: () => GetOnlineReceiptResponse })
  @ApiOperation({ summary: 'get online receipt setting by restaurant' })
  async getOnlineReceipt(@Param('restaurantId') id: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const onlineReceipt = await this.onlineReceiptService.findOne({
      restaurantId: id,
      status: StatusEnum.ACTIVE
    })
    if (!onlineReceipt) {
      await this.onlineReceiptService.create({
        restaurantId: id
      })
      return await this.onlineReceiptService.findOne({
        restaurantId: id,
        status: StatusEnum.ACTIVE
      })
    }
    return onlineReceipt
  }

  @Put(':restaurantId')
  @ApiOkResponse({ type: () => GetOnlineReceiptResponse })
  @ApiOperation({ summary: 'update online receipt setting by restaurant' })
  async updateOnlineReceipt(
    @Param('restaurantId') id: string,
    @Body() payload: UpdateOnlineReceiptDto
  ) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    return await this.onlineReceiptService.findOneAndUpdate(
      { restaurantId: id },
      payload
    )
  }
}
