import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { OnlineReceiptDocument } from '../schemas/online-receipt.schema'

@Injectable({ scope: Scope.REQUEST })
export class OnlineReceiptService {
  constructor(
    @InjectModel('onlineReceipt')
    private readonly OnlineReceiptModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<OnlineReceiptDocument | any> {
    return this.OnlineReceiptModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any
  ): Promise<OnlineReceiptDocument | any> {
    return this.OnlineReceiptModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.OnlineReceiptModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<OnlineReceiptDocument> {
    return this.OnlineReceiptModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.OnlineReceiptModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.OnlineReceiptModel.createCollection()

    return this.OnlineReceiptModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<OnlineReceiptDocument> {
    const document = new this.OnlineReceiptModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<OnlineReceiptDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<OnlineReceiptDocument> {
    const document = new this.OnlineReceiptModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<OnlineReceiptDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<OnlineReceiptDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<OnlineReceiptDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.OnlineReceiptModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.OnlineReceiptModel.findById(id)
  }

  findOne(condition: any): Promise<OnlineReceiptDocument> {
    return this.OnlineReceiptModel.findOne(condition)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<OnlineReceiptDocument> {
    return this.OnlineReceiptModel.findOne(condition, options)
  }
}
