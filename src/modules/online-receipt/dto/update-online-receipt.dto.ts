import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'

export class UpdateOnlineReceiptDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://miro.com/app/board/uXjVPfXXyV0=/' })
  readonly restaurantLogoUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://miro.com/app/board/uXjVPfXXyV0=/' })
  readonly facebookUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://miro.com/app/board/uXjVPfXXyV0=/' })
  readonly instagramUrl: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'https://miro.com/app/board/uXjVPfXXyV0=/' })
  readonly twitterUrl: string
}
