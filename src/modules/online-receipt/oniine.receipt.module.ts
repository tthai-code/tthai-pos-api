import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { OnlineReceiptController } from './controllers/online-receipt.controller'
import { OnlineReceiptSchema } from './schemas/online-receipt.schema'
import { OnlineReceiptService } from './services/online-receipt.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    MongooseModule.forFeature([
      { name: 'onlineReceipt', schema: OnlineReceiptSchema }
    ])
  ],
  providers: [OnlineReceiptService],
  controllers: [OnlineReceiptController],
  exports: [OnlineReceiptService]
})
export class OnlineReceiptModule {}
