import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'

export class OnlineReceiptResponseField extends TimestampResponseDto {
  @ApiProperty()
  twitterUrl: string

  @ApiProperty()
  instagramUrl: string

  @ApiProperty()
  facebookUrl: string

  @ApiProperty()
  restaurantLogoUrl: string

  @ApiProperty()
  restaurantId: string
}

export class GetOnlineReceiptResponse extends ResponseDto<OnlineReceiptResponseField> {
  @ApiProperty({
    type: OnlineReceiptResponseField,
    example: {
      status: 'active',
      twitter_url: null,
      instagram_url: null,
      facebook_url: null,
      restaurant_logo_url:
        'https://storage.googleapis.com/tthai-pos-staging/upload/166538232509038624.png',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-10-10T12:21:36.160Z',
      updated_at: '2022-10-10T12:23:29.612Z',
      updated_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      created_by: {
        username: 'corywong@mail.com',
        id: '630e53ec9e21d871a49fb4f5'
      },
      id: '63440e5016effaa8e241f2e1'
    }
  })
  data: OnlineReceiptResponseField
}
