import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type OnlineReceiptDocument = OnlineReceiptFields & Document

@Schema({ timestamps: true, strict: true, collection: 'onlineReceipts' })
export class OnlineReceiptFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ default: null })
  restaurantLogoUrl: string

  @Prop({ default: null })
  facebookUrl: string

  @Prop({ default: null })
  instagramUrl: string

  @Prop({ default: null })
  twitterUrl: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const OnlineReceiptSchema =
  SchemaFactory.createForClass(OnlineReceiptFields)
OnlineReceiptSchema.plugin(authorStampCreatePlugin)
