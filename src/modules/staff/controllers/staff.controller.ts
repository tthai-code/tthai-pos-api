import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { AuthLogic } from 'src/modules/auth/logics/auth.logic'
import { CreateStaffDto } from '../dto/create-staff.dto'
import { StaffPaginateDto } from '../dto/get-staff.dto'
import { UpdateStaffDto } from '../dto/update-staff.dto'
import { StaffLogic } from '../logics/staff.logic'
import { StaffService } from '../services/staff.service'

@ApiBearerAuth()
@ApiTags('staff')
@Controller('v1/staff')
@UseGuards(AdminAuthGuard)
export class StaffController {
  constructor(
    private readonly staffService: StaffService,
    private readonly staffLogic: StaffLogic,
    private readonly authLogic: AuthLogic
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get Staff List' })
  async getStaffs(@Query() query: StaffPaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.staffService.paginate(
      query.buildQuery(restaurantId),
      query
    )
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get a Staff' })
  async getStaff(@Param('id') id: string) {
    return await this.staffService.findOne({ _id: id })
  }

  @Post()
  @ApiOperation({ summary: 'Create a Staff' })
  async createStaff(@Body() payload: CreateStaffDto) {
    return await this.staffLogic.CreateStaff(payload)
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update a Staff' })
  async updateStaff(@Param('id') id: string, @Body() payload: UpdateStaffDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.staffLogic.UpdateStaff(id, restaurantId, payload)
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a Staff' })
  async deleteStaff(@Param('id') id: string) {
    return await this.staffService.delete(id)
  }
}
