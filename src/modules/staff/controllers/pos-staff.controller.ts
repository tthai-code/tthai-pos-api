import { Body, Controller, Post, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse
} from '@nestjs/swagger'
import { DeviceAuthGuard } from 'src/modules/auth/guards/device-auth.guard'
import { AuthLogic } from 'src/modules/auth/logics/auth.logic'
import { errorResponse } from 'src/utilities/errorResponse'
import { StaffLoginDto } from '../dto/staff-login.dto'
import { StaffLoginResponse } from '../enitity/staff.entity'

@ApiBearerAuth()
@ApiTags('pos/staff')
@Controller('v1/staff')
@UseGuards(DeviceAuthGuard)
export class POSStaffController {
  constructor(private readonly authLogic: AuthLogic) {}

  @Post('login')
  @ApiOperation({
    summary: 'Staff Login',
    description: 'use *bearer* `device_token`'
  })
  @ApiOkResponse({ type: () => StaffLoginResponse })
  @ApiUnauthorizedResponse(
    errorResponse(401, 'Not found staff.', '/v1/staff/login')
  )
  async staffLogin(@Body() payload: StaffLoginDto) {
    const restaurantId = DeviceAuthGuard.getAuthorizedUser()?.restaurant?.id
    return this.authLogic.StaffLogin(restaurantId, payload)
  }
}
