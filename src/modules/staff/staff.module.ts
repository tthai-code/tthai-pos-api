import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { POSStaffController } from './controllers/pos-staff.controller'
import { StaffController } from './controllers/staff.controller'
import { StaffLogic } from './logics/staff.logic'
import { StaffSchema } from './models/staff.schema'
import { StaffService } from './services/staff.service'

@Module({
  imports: [
    RestaurantModule,
    MongooseModule.forFeature([{ name: 'staff', schema: StaffSchema }])
  ],
  providers: [StaffService, StaffLogic],
  controllers: [StaffController, POSStaffController],
  exports: [StaffService]
})
export class StaffModule {}
