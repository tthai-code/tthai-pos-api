import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { StaffDocument } from '../models/staff.schema'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { StaffPaginateDto } from '../dto/get-staff.dto'

Injectable({ scope: Scope.REQUEST })
export class StaffService {
  constructor(
    @InjectModel('staff')
    private readonly StaffModel: PaginateModel<StaffDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<StaffDocument | any> {
    return this.StaffModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.StaffModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<StaffDocument> {
    return this.StaffModel
  }

  async create(payload: any): Promise<StaffDocument> {
    const document = new this.StaffModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, Member: any): Promise<StaffDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...Member }).save()
  }

  async delete(id: string): Promise<StaffDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<any[]> {
    return this.StaffModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.StaffModel.findById(id)
  }

  findOne(condition): Promise<StaffDocument> {
    return this.StaffModel.findOne(condition)
  }

  findOneWithPassCode(condition): Promise<StaffDocument> {
    return this.StaffModel.findOne(condition)
  }

  paginate(
    query: any,
    queryParam: StaffPaginateDto
  ): Promise<PaginateResult<StaffDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder }
    }

    return this.StaffModel.paginate(query, options)
  }

  findOneForOrder(id: string): Promise<StaffDocument> {
    return this.StaffModel.findOne(
      {
        _id: id,
        status: StatusEnum.ACTIVE
      },
      {
        _id: 0,
        id: '$_id',
        firstName: 1,
        lastName: 1
      }
    ).lean()
  }
}
