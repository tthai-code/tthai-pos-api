import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class StaffPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'createdAt' })
  readonly sortBy: string = 'createdAt'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<restaurant-id>' })
  readonly restaurant: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          firstName: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          lastName: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          email: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          tel: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      restaurantId: !id ? this.restaurant : id,
      status: this.status || { $ne: StatusEnum.DELETED }
    }
    if (!id && !this.restaurant) delete result.restaurantId
    return result
  }
}
