import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class StaffLoginDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '123456' })
  readonly passCode: string
}
