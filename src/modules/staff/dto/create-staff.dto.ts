import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsNotEmpty, IsString } from 'class-validator'
import { StaffRoleEnum } from '../common/staff-role.enum'

export class CreateStaffDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '123456' })
  readonly passCode: string

  @IsString()
  @ApiProperty({ example: 'John' })
  readonly firstName: string

  @IsString()
  @ApiProperty({ example: 'Petrucci' })
  readonly lastName: string

  @IsString()
  @ApiProperty({ example: 'johnpetrucci@mail.com' })
  readonly email: string

  @IsString()
  @ApiProperty({ example: '0632634321' })
  readonly tel: string

  @IsEnum(StaffRoleEnum)
  @ApiProperty({ example: StaffRoleEnum.STAFF, enum: StaffRoleEnum })
  readonly role: string
}
