import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsEnum, IsOptional, IsString } from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'
import { StaffRoleEnum } from '../common/staff-role.enum'

export class UpdateStaffDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: '123456' })
  readonly passCode: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'John' })
  readonly firstName: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Petrucci' })
  readonly lastName: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'johnpetrucci@mail.com' })
  readonly email: string

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: '0632634321' })
  readonly tel: string

  @IsEnum(StaffRoleEnum)
  @IsOptional()
  @ApiPropertyOptional({ example: StaffRoleEnum.STAFF, enum: StaffRoleEnum })
  readonly role: string

  @IsEnum(StatusEnum)
  @IsOptional()
  @ApiPropertyOptional({ example: StatusEnum.ACTIVE, enum: StatusEnum })
  readonly status: string
}
