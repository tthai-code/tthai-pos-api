import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CreateStaffDto } from '../dto/create-staff.dto'
import { UpdateStaffDto } from '../dto/update-staff.dto'
import { StaffService } from '../services/staff.service'

@Injectable()
export class StaffLogic {
  constructor(
    private readonly staffService: StaffService,
    private readonly restaurantService: RestaurantService
  ) {}

  async CreateStaff(payload: CreateStaffDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: payload.restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Restaurant not found.')
    const staff = await this.staffService.findOne({
      restaurantId: payload.restaurantId,
      passCode: payload.passCode,
      status: StatusEnum.ACTIVE
    })
    if (staff) throw new BadRequestException(`Pass code is exist.`)
    const created = await this.staffService.create(payload)
    if (!created) throw new BadRequestException('Can not create staff.')
    return created
  }

  async UpdateStaff(id: string, restaurantId: string, payload: UpdateStaffDto) {
    const staff = await this.staffService.findOne({
      passCode: payload.passCode,
      status: StatusEnum.ACTIVE,
      restaurantId,
      _id: { $ne: id }
    })
    if (staff) throw new BadRequestException(`Pass code is exist.`)
    const updated = await this.staffService.update(id, payload)
    if (!updated) throw new BadRequestException('Can not update staff.')
    return updated
  }
}
