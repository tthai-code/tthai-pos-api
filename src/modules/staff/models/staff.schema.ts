import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { StaffRoleEnum } from '../common/staff-role.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from '../../database/author-stamp.plugin'

export type StaffDocument = StaffFields & Document

@Schema({ timestamps: true, strict: true, collection: 'staffs' })
export class StaffFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  passCode: string

  @Prop()
  firstName: string

  @Prop()
  lastName: string

  @Prop()
  email: string

  @Prop()
  tel: string

  @Prop({ default: StaffRoleEnum.STAFF, enum: Object.values(StaffRoleEnum) })
  role: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}

export const StaffSchema = SchemaFactory.createForClass(StaffFields)
StaffSchema.plugin(mongoosePaginate)
StaffSchema.plugin(authorStampCreatePlugin)
