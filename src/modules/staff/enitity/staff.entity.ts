import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { StaffRoleEnum } from '../common/staff-role.enum'

class AddressResponseFieldResponse {
  @ApiProperty()
  address: string

  @ApiProperty()
  city: string

  @ApiProperty()
  state: string

  @ApiProperty()
  zip_code: string
}

class TagRateFieldsResponse {
  @ApiProperty()
  is_alcohol_tax_active: boolean

  @ApiProperty()
  alcohol_tax: number

  @ApiProperty()
  tax: number
}

class RestaurantFieldsResponse {
  @ApiProperty()
  legal_business_name: string

  @ApiProperty()
  dba: string

  @ApiProperty()
  authorized_person_first_name: string

  @ApiProperty()
  authorized_person_last_name: string

  @ApiProperty()
  email: string

  @ApiProperty()
  business_phone: string

  @ApiProperty({ type: AddressResponseFieldResponse })
  address: AddressResponseFieldResponse

  @ApiProperty({ type: TagRateFieldsResponse })
  taxRate: TagRateFieldsResponse

  @ApiProperty()
  logo_url: string

  @ApiProperty()
  timeZone: string
}

class StaffLoginFieldsResponse {
  @ApiProperty()
  username: string

  @ApiProperty()
  id: string

  @ApiProperty({ enum: Object.values(StaffRoleEnum) })
  role: string

  @ApiProperty()
  owner_name: string

  @ApiProperty({ type: RestaurantFieldsResponse })
  restaurant: RestaurantFieldsResponse

  @ApiProperty()
  token_expire: number
}

export class StaffLoginResponse extends ResponseDto<StaffLoginFieldsResponse> {
  @ApiProperty({
    type: StaffLoginFieldsResponse,
    example: {
      username: 'Pavarich',
      id: '6316cc72b889593b028928f1',
      role: 'MANAGER',
      owner_name: 'Paweena Naka',
      restaurant: {
        time_zone: 'Etc/UTC',
        default_service_charge: 10,
        tax_rate: {
          is_alcohol_tax_active: false,
          alcohol_tax: 25.2,
          tax: 14
        },
        logo_url: null,
        email: 'info@carolinaswise.com',
        authorized_person_last_name: 'Naka',
        authorized_person_first_name: 'Paweena',
        business_phone: '5102638229',
        address: {
          zip_code: '94501',
          state: 'CA',
          city: 'Alameda',
          address: '1319 Park St'
        },
        dba: 'Test Doing as Business',
        legal_business_name: 'May Thai Kitchen LLC',
        id: '630eff5751c2eac55f52662c'
      },
      token_expire: 1667447220531
    }
  })
  data: StaffLoginFieldsResponse

  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlBhdmFyaWNoIiwiaWQiOiI2MzE2Y2M3MmI4ODk1OTNiMDI4OTI4ZjEiLCJyb2xlIjoiTUFOQUdFUiIsIm93bmVyTmFtZSI6IlBhd2VlbmEgTmFrYSIsInJlc3RhdXJhbnQiOnsidGF4UmF0ZSI6eyJpc0FsY29ob2xUYXhBY3RpdmUiOmZhbHNlLCJhbGNvaG9sVGF4IjoyNS4yLCJ0YXgiOjE0fSwibG9nb1VybCI6bnVsbCwiZW1haWwiOiJpbmZvQGNhcm9saW5hc3dpc2UuY29tIiwiYXV0aG9yaXplZFBlcnNvbkxhc3ROYW1lIjoiTmFrYSIsImF1dGhvcml6ZWRQZXJzb25GaXJzdE5hbWUiOiJQYXdlZW5hIiwiYnVzaW5lc3NQaG9uZSI6IjUxMDI2MzgyMjkiLCJhZGRyZXNzIjp7InppcENvZGUiOiI5NDUwMSIsInN0YXRlIjoiQ0EiLCJjaXR5IjoiQWxhbWVkYSIsImFkZHJlc3MiOiIxMzE5IFBhcmsgU3QifSwibGVnYWxCdXNpbmVzc05hbWUiOiJNYXkgVGhhaSBLaXRjaGVuIExMQyIsImlkIjoiNjMwZWZmNTc1MWMyZWFjNTVmNTI2NjJjIn0sImlhdCI6MTY2Njg0MjQyMCwiZXhwIjoxNjY3NDQ3MjIwfQ.Ad3BUtkeCoOBgJFiD1bfvLPNOdILkNOhvy56_WnorNo'
  })
  access_token: string
}
