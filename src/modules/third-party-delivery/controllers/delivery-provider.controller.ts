import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { SuperAdminAuthGuard } from 'src/modules/auth/guards/super-admin-auth.guard'
import { CreateDeliveryProviderDto } from '../dto/create-provider.dto'
import { DeliverProviderPaginateDto } from '../dto/get-paginate-delivery.dto'
import {
  CreateDeliveryProviderResponseDto,
  DeleteDeliveryProviderResponse
} from '../entity/create-delivery-provider.entity'
import { PaginateDeliveryProviderResponse } from '../entity/paginate-deilivery-provider.entity'
import { DeliveryProviderService } from '../services/delivery-provider.service'

@ApiBearerAuth()
@ApiTags('delivery-provider')
@UseGuards(SuperAdminAuthGuard)
@Controller('/v1/delivery-provider')
export class DeliveryProviderController {
  constructor(
    private readonly deliveryProviderService: DeliveryProviderService
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaginateDeliveryProviderResponse })
  @ApiOperation({ summary: 'Get Paginate Delivery Providers' })
  async getDeliveryProviders(@Query() query: DeliverProviderPaginateDto) {
    return await this.deliveryProviderService.paginate(
      query.buildQuery(),
      query
    )
  }

  @Post()
  @ApiCreatedResponse({ type: () => CreateDeliveryProviderResponseDto })
  @ApiOperation({ summary: 'Create a Delivery Provider' })
  async createDeliveryProvider(@Body() payload: CreateDeliveryProviderDto) {
    return await this.deliveryProviderService.create(payload)
  }

  @Get(':id')
  @ApiOkResponse({ type: () => CreateDeliveryProviderResponseDto })
  @ApiOperation({ summary: 'Get a Delivery Provider by ID' })
  async getDeliveryProvider(@Param('id') id: string) {
    const delivery = await this.deliveryProviderService.findOne({
      _id: id,
      status: { $ne: StatusEnum.DELETED }
    })
    if (!delivery) throw new NotFoundException('Not found delivery provider.')
    return delivery
  }

  @Put(':id')
  @ApiOkResponse({ type: () => CreateDeliveryProviderResponseDto })
  @ApiOperation({ summary: 'Update a delivery provider by ID' })
  async updateDeliveryProvider(
    @Param('id') id: string,
    @Body() payload: CreateDeliveryProviderDto
  ) {
    const delivery = await this.deliveryProviderService.findOne({
      _id: id,
      status: { $ne: StatusEnum.DELETED }
    })
    if (!delivery) throw new NotFoundException('Not found delivery provider.')
    return await this.deliveryProviderService.update(id, payload)
  }

  @Delete(':id')
  @ApiOkResponse({ type: () => DeleteDeliveryProviderResponse })
  @ApiOperation({ summary: 'Delete a delivery provider by ID' })
  async deleteDeliveryProvider(@Param('id') id: string) {
    const delivery = await this.deliveryProviderService.findOne({
      _id: id,
      status: { $ne: StatusEnum.DELETED }
    })
    if (!delivery) throw new NotFoundException('Not found delivery provider.')
    return await this.deliveryProviderService.delete(id)
  }
}
