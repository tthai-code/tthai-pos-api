import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Put,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import {
  ConnectProviderDto,
  UpdateDeliveryDto
} from '../dto/update-delivery.dto'
import {
  GetAllDeliveryResponse,
  GetDeliveryResponse,
  UpdateDeliveryResponse
} from '../entity/delivery.entity'
import { DeliveryLogic } from '../logics/delivery.logic'
import { DeliveryService } from '../services/delivery.service'

@ApiBearerAuth()
@ApiTags('third-party-delivery')
@UseGuards(AdminAuthGuard)
@Controller('v1/delivery')
export class DeliveryController {
  constructor(
    private readonly deliveryService: DeliveryService,
    private readonly deliveryLogic: DeliveryLogic
  ) {}

  @Get(':restaurantId/all')
  @ApiOkResponse({ type: () => GetAllDeliveryResponse })
  @ApiOperation({ summary: 'Get all 3rd party delivery by restaurantId' })
  async getDeliveries(@Param('restaurantId') id: string) {
    return await this.deliveryLogic.getDeliverByRestaurantId(id)
  }

  @Get(':id')
  @ApiOkResponse({ type: () => GetDeliveryResponse })
  @ApiOperation({ summary: 'Get a 3rd party delivery setting by id' })
  async getDelivery(@Param('id') id: string) {
    return await this.deliveryLogic.getDelivery(id)
  }

  @Put(':id')
  @ApiOkResponse({ type: () => UpdateDeliveryResponse })
  @ApiOperation({ summary: 'Update a 3rd delivery setting by id' })
  async updateDelivery(
    @Param('id') id: string,
    @Body() payload: UpdateDeliveryDto
  ) {
    await this.deliveryLogic.updateDelivery(id, payload)
    return { success: true }
  }

  @Patch(':id/connect-provider')
  @ApiOperation({ summary: 'Connect/Disconnect Provider' })
  async providerConnection(
    @Param('id') id: string,
    @Body() payload: ConnectProviderDto
  ) {
    return await this.deliveryLogic.handleConnection(id, payload)
  }

  @Get('ubereats/auth')
  @ApiOperation({ summary: 'Get UberEats Auth URL' })
  async getUberEatsAuthUrl() {
    return await this.deliveryLogic.getUberEatsAuthUrl()
  }
}
