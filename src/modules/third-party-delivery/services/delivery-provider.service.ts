import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { REQUEST } from '@nestjs/core'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { DeliveryProviderDocument } from '../schemas/delivery-provider.schema'
import { DeliverProviderPaginateDto } from '../dto/get-paginate-delivery.dto'

@Injectable({ scope: Scope.REQUEST })
export class DeliveryProviderService {
  constructor(
    @InjectModel('deliveryProvider')
    private readonly DeliveryProviderModel:
      | PaginateModel<DeliveryProviderDocument>
      | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<DeliveryProviderDocument | any> {
    return this.DeliveryProviderModel.findById({ _id: id })
  }

  async resolveByCondition(
    condition: any
  ): Promise<DeliveryProviderDocument | any> {
    return this.DeliveryProviderModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.DeliveryProviderModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<DeliveryProviderDocument> {
    return this.DeliveryProviderModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.DeliveryProviderModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.DeliveryProviderModel.createCollection()

    return this.DeliveryProviderModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<DeliveryProviderDocument> {
    const document = new this.DeliveryProviderModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<DeliveryProviderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<DeliveryProviderDocument> {
    const document = new this.DeliveryProviderModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<DeliveryProviderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<DeliveryProviderDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<DeliveryProviderDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.DeliveryProviderModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.DeliveryProviderModel.findById(id)
  }

  findOne(condition: any): Promise<DeliveryProviderDocument> {
    return this.DeliveryProviderModel.findOne(condition)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<DeliveryProviderDocument> {
    return this.DeliveryProviderModel.findOne(condition, options)
  }

  paginate(
    query: any,
    queryParam: DeliverProviderPaginateDto,
    selectFields: any = ''
  ): Promise<PaginateResult<DeliveryProviderDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select: selectFields
    }

    return this.DeliveryProviderModel.paginate(query, options)
  }
}
