import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { ClientSession } from 'mongodb'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { DeliveryDocument } from '../schemas/delivery.schema'

@Injectable({ scope: Scope.REQUEST })
export class DeliveryService {
  constructor(
    @InjectModel('delivery')
    private readonly DeliveryModel: any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<DeliveryDocument | any> {
    return this.DeliveryModel.findById({ _id: id })
  }

  async resolveByCondition(condition: any): Promise<DeliveryDocument | any> {
    return this.DeliveryModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.DeliveryModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<DeliveryDocument> {
    return this.DeliveryModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.DeliveryModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.DeliveryModel.createCollection()

    return this.DeliveryModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<DeliveryDocument> {
    const document = new this.DeliveryModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<DeliveryDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<DeliveryDocument> {
    const document = new this.DeliveryModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<DeliveryDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<DeliveryDocument> {
    const document = await this.resolveByCondition(condition)

    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<DeliveryDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.DeliveryModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.DeliveryModel.findById(id)
  }

  findOne(condition: any): Promise<DeliveryDocument> {
    return this.DeliveryModel.findOne(condition)
  }

  findOneWithSelect(condition: any, options?: any): Promise<DeliveryDocument> {
    return this.DeliveryModel.findOne(condition, options)
  }
}
