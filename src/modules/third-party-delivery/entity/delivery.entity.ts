import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

export class GetDeliveryResponseField {
  @ApiProperty()
  id: string

  @ApiProperty()
  delivery_id: string

  @ApiProperty()
  delivery_name: string

  @ApiProperty()
  delivery_image_url: string

  @ApiProperty()
  username: string

  @ApiProperty()
  password: string

  @ApiProperty()
  is_active: boolean

  @ApiProperty()
  note: string
}

class UpdateResponseField {
  @ApiProperty()
  success: boolean
}

export class GetDeliveryResponse extends ResponseDto<GetDeliveryResponseField> {
  @ApiProperty({
    type: GetDeliveryResponseField,
    example: {
      id: '634401ae397a09daeae8781f',
      delivery_id: '6343ff580792826b8e690f8d',
      delivery_name: 'Uber Eats',
      delivery_image_url:
        'https://storage.googleapis.com/tthai-pos-staging/upload/166540063611168653.png',
      username: null,
      password: null,
      is_active: false,
      note: null
    }
  })
  data: GetDeliveryResponseField
}

export class GetAllDeliveryResponse extends ResponseDto<
  Array<GetDeliveryResponseField>
> {
  @ApiProperty({
    type: [GetDeliveryResponseField],
    example: [
      {
        id: '634400b5df92a1e04f885644',
        delivery_id: '6343b8728702273dc9187eac',
        delivery_name: 'DoorDash',
        delivery_image_url:
          'https://storage.googleapis.com/tthai-pos-staging/upload/166538232509038624.png',
        username: null,
        password: null,
        is_active: false,
        note: null
      },
      {
        id: '634401ae397a09daeae8781e',
        delivery_id: '6343fd82a650c90357a350de',
        delivery_name: 'GrabHub',
        delivery_image_url:
          'https://storage.googleapis.com/tthai-pos-staging/upload/166540016857811015.png',
        username: null,
        password: null,
        is_active: false,
        note: null
      }
    ]
  })
  data: Array<GetDeliveryResponseField>
}

export class UpdateDeliveryResponse extends ResponseDto<UpdateResponseField> {
  @ApiProperty({
    type: UpdateResponseField,
    example: {
      success: true
    }
  })
  data: UpdateResponseField
}
