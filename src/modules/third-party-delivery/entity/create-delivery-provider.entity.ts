import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'

export class CreateDeliveryProviderFieldsDto extends TimestampResponseDto {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string

  @ApiProperty()
  imageUrl: string
}

export class CreateDeliveryProviderResponseDto extends ResponseDto<CreateDeliveryProviderFieldsDto> {
  @ApiProperty({
    type: CreateDeliveryProviderFieldsDto,
    example: {
      status: 'active',
      image_url:
        'https://storage.googleapis.com/tthai-pos-staging/upload/166546057662955493.png',
      name: 'Postmates',
      created_at: '2022-10-11T03:56:46.826Z',
      updated_at: '2022-10-11T03:56:46.826Z',
      updated_by: {
        username: 'superadmin',
        id: '6318d06a1fa78236d380db83'
      },
      created_by: {
        username: 'superadmin',
        id: '6318d06a1fa78236d380db83'
      },
      id: '6344e97e291d3456a6d5acab'
    }
  })
  data: CreateDeliveryProviderFieldsDto
}

export class DeleteDeliveryProviderResponse extends ResponseDto<CreateDeliveryProviderFieldsDto> {
  @ApiProperty({
    type: CreateDeliveryProviderFieldsDto,
    example: {
      message: 'done',
      data: {
        created_by: {
          username: 'superadmin',
          id: '6318d06a1fa78236d380db83'
        },
        updated_by: {
          username: 'superadmin',
          id: '6318d06a1fa78236d380db83'
        },
        status: 'deleted',
        image_url:
          'https://storage.googleapis.com/tthai-pos-staging/upload/166538232509038624.png',
        name: 'DoorDash',
        updated_at: '2022-10-11T04:48:36.936Z',
        created_at: '2022-10-10T06:15:14.721Z',
        id: '6343b8728702273dc9187eac'
      }
    }
  })
  data: CreateDeliveryProviderFieldsDto
}
