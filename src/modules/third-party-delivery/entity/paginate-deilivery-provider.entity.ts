import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { CreateDeliveryProviderFieldsDto } from './create-delivery-provider.entity'

export class PaginateDeliveryProviderResult extends PaginateResponseDto<
  Array<CreateDeliveryProviderFieldsDto>
> {
  @ApiProperty({
    type: [CreateDeliveryProviderFieldsDto]
  })
  results: Array<CreateDeliveryProviderFieldsDto>
}

export class PaginateDeliveryProviderResponse extends ResponseDto<PaginateDeliveryProviderResult> {
  @ApiProperty({
    type: PaginateDeliveryProviderResult,
    example: {
      message: 'done',
      data: {
        total: 4,
        limit: 25,
        page: 1,
        pages: 1,
        results: [
          {
            status: 'active',
            image_url:
              'https://storage.googleapis.com/tthai-pos-staging/upload/166546057662955493.png',
            name: 'Postmates',
            created_at: '2022-10-11T03:56:46.826Z',
            updated_at: '2022-10-11T03:56:46.826Z',
            updated_by: {
              username: 'superadmin',
              id: '6318d06a1fa78236d380db83'
            },
            created_by: {
              username: 'superadmin',
              id: '6318d06a1fa78236d380db83'
            },
            id: '6344e97e291d3456a6d5acab'
          },
          {
            status: 'active',
            image_url:
              'https://storage.googleapis.com/tthai-pos-staging/upload/166540063611168653.png',
            name: 'Uber Eats',
            created_at: '2022-10-10T11:17:44.780Z',
            updated_at: '2022-10-10T11:17:44.780Z',
            updated_by: {
              username: 'superadmin',
              id: '6318d06a1fa78236d380db83'
            },
            created_by: {
              username: 'superadmin',
              id: '6318d06a1fa78236d380db83'
            },
            id: '6343ff580792826b8e690f8d'
          },
          {
            status: 'active',
            image_url:
              'https://storage.googleapis.com/tthai-pos-staging/upload/166540016857811015.png',
            name: 'GrabHub',
            created_at: '2022-10-10T11:09:54.569Z',
            updated_at: '2022-10-10T11:09:54.569Z',
            updated_by: {
              username: 'superadmin',
              id: '6318d06a1fa78236d380db83'
            },
            created_by: {
              username: 'superadmin',
              id: '6318d06a1fa78236d380db83'
            },
            id: '6343fd82a650c90357a350de'
          },
          {
            status: 'active',
            image_url:
              'https://storage.googleapis.com/tthai-pos-staging/upload/166538232509038624.png',
            name: 'DoorDash',
            created_at: '2022-10-10T06:15:14.721Z',
            updated_at: '2022-10-10T06:22:43.862Z',
            updated_by: {
              username: 'superadmin',
              id: '6318d06a1fa78236d380db83'
            },
            created_by: {
              username: 'superadmin',
              id: '6318d06a1fa78236d380db83'
            },
            id: '6343b8728702273dc9187eac'
          }
        ]
      }
    }
  })
  data: PaginateDeliveryProviderResult
}
