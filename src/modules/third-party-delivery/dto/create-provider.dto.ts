import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'

export class CreateDeliveryProviderDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'DoorDash' })
  readonly name: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'https://example.com/123123' })
  readonly imageUrl: string

  @IsOptional()
  @IsEnum(StatusEnum)
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string
}
