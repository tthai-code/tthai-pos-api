import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsBoolean, IsOptional, IsString } from 'class-validator'

export class UpdateDeliveryDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<username>' })
  readonly username: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<password>' })
  public password: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<provider-store-id>' })
  readonly providerId: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<provider-store-id>' })
  readonly providerStoreId: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Test Note' })
  readonly note: string
}

export class ConnectProviderDto {
  @IsBoolean()
  @ApiProperty({ example: true })
  readonly isActive: boolean
}
