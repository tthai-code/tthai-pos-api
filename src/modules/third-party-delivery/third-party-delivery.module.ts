import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { KitchenHubModule } from '../kitchen-hub/kitchen-hub.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { DeliveryProviderController } from './controllers/delivery-provider.controller'
import { DeliveryController } from './controllers/delivery.controller'
import { DeliveryLogic } from './logics/delivery.logic'
import { DeliveryProviderSchema } from './schemas/delivery-provider.schema'
import { DeliverySchema } from './schemas/delivery.schema'
import { DeliveryProviderService } from './services/delivery-provider.service'
import { DeliveryService } from './services/delivery.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    forwardRef(() => KitchenHubModule),
    MongooseModule.forFeature([
      { name: 'deliveryProvider', schema: DeliveryProviderSchema },
      { name: 'delivery', schema: DeliverySchema }
    ])
  ],
  providers: [DeliveryProviderService, DeliveryService, DeliveryLogic],
  controllers: [DeliveryProviderController, DeliveryController],
  exports: [DeliveryProviderService, DeliveryService]
})
export class ThirdPartyDeliveryModule {}
