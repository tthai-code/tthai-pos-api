import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type DeliveryProviderDocument = DeliveryProviderFields & Document

@Schema({
  timestamps: true,
  strict: true,
  collection: 'deliveryProviders'
})
export class DeliveryProviderFields {
  @Prop()
  name: string

  @Prop()
  providerId: string

  @Prop()
  imageUrl: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const DeliveryProviderSchema = SchemaFactory.createForClass(
  DeliveryProviderFields
)
DeliveryProviderSchema.plugin(mongoosePaginate)
DeliveryProviderSchema.plugin(authorStampCreatePlugin)
