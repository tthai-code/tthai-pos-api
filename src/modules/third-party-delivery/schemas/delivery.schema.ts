import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type DeliveryDocument = DeliveryFields & Document
@Schema({
  timestamps: true,
  strict: true,
  collection: 'deliveries',
  toJSON: { virtuals: true },
  toObject: { virtuals: true }
})
export class DeliveryFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'deliveryProvider' })
  deliveryId: Types.ObjectId

  @Prop()
  providerId: string

  @Prop({ default: false })
  isActive: boolean

  @Prop({ default: null })
  username: string

  @Prop({ default: null })
  password: string

  @Prop({ default: null })
  note: string

  @Prop({ default: null })
  providerStoreId: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const DeliverySchema = SchemaFactory.createForClass(DeliveryFields)
DeliverySchema.plugin(authorStampCreatePlugin)
DeliverySchema.virtual('deliveryProvider', {
  ref: 'deliveryProvider',
  localField: 'deliveryId',
  foreignField: '_id'
})
