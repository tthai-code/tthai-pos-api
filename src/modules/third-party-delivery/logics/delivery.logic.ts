import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import {
  IAuthProvider,
  IConnectProvider,
  IDisconnectProvider
} from 'src/modules/kitchen-hub/interfaces/kitchen-hub.interface'
import { DeliverySettingService } from 'src/modules/kitchen-hub/services/delivery-setting.service'
import { KitchenHubHttpService } from 'src/modules/kitchen-hub/services/kitchen-hub-http.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { stringToBase64, decodeBase64toString } from 'src/utilities/base64'
import {
  ConnectProviderDto,
  UpdateDeliveryDto
} from '../dto/update-delivery.dto'
import { DeliveryDocument } from '../schemas/delivery.schema'
import { DeliveryProviderService } from '../services/delivery-provider.service'
import { DeliveryService } from '../services/delivery.service'

@Injectable()
export class DeliveryLogic {
  constructor(
    private readonly deliveryService: DeliveryService,
    private readonly restaurantService: RestaurantService,
    private readonly deliveryProviderService: DeliveryProviderService,
    private readonly deliverySettingService: DeliverySettingService,
    private readonly httpService: KitchenHubHttpService
  ) {}

  private transformGetDeliveries(
    delivery: Array<DeliveryDocument>,
    deliveryProvider: Array<any>
  ) {
    const result = []
    delivery.forEach((item) => {
      const index = deliveryProvider.findIndex(
        (elm) => JSON.stringify(elm._id) === JSON.stringify(item.deliveryId)
      )
      if (deliveryProvider[index]?.status === StatusEnum.ACTIVE) {
        result.push({
          id: item._id,
          deliveryId: item.deliveryId,
          deliveryName: deliveryProvider[index]?.name,
          deliveryImageUrl: deliveryProvider[index]?.imageUrl,
          providerId: deliveryProvider[index]?.providerId,
          username: item.username,
          password: item.password,
          isActive: item.isActive,
          providerStoreId: item.providerStoreId,
          note: item.note
        })
      }
    })
    return result
  }

  async getDeliverByRestaurantId(id: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const deliveryProvider = await this.deliveryProviderService.getAll({
      status: StatusEnum.ACTIVE
    })
    const deliverySetting = await this.deliverySettingService.findOne(
      {
        restaurantId: id
      },
      { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 }
    )
    if (!deliverySetting) {
      const { data } = await this.httpService.createStore(restaurant.dba)
      if (!data?.ok) throw new BadRequestException(data?.error_code)
      await this.deliverySettingService.create({
        restaurantId: id,
        storeId: data.result.id
      })
    }
    if (!deliveryProvider) throw new BadRequestException()
    const delivery = await this.deliveryService.getAll({
      restaurantId: id,
      status: StatusEnum.ACTIVE
    })
    if (deliveryProvider.length !== delivery.length) {
      const deliveryIds = deliveryProvider.map((item) => `${item._id}`)
      const existDeliveryIds = delivery.map((item) => `${item.deliveryId}`)
      const unCreatedDelivery = []
      deliveryIds.forEach((item, index) => {
        if (!existDeliveryIds.includes(item)) {
          unCreatedDelivery.push({
            restaurantId: id,
            deliveryId: item,
            providerId: deliveryProvider[index].providerId
          })
        }
      })
      const created = await this.deliveryService
        .getModel()
        .insertMany(unCreatedDelivery)
      if (!created) throw new BadRequestException()
      const newDelivery = await this.deliveryService.getAll({
        restaurantId: id,
        status: StatusEnum.ACTIVE
      })
      return this.transformGetDeliveries(newDelivery, deliveryProvider)
    }
    return this.transformGetDeliveries(delivery, deliveryProvider)
  }

  async getDelivery(id: string) {
    const delivery = await this.deliveryService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!delivery) throw new NotFoundException('Not found delivery setting.')
    const deliveryProvider = await this.deliveryProviderService.findOne({
      _id: delivery.deliveryId
    })
    if (!deliveryProvider) throw new NotFoundException('Not found 3rd delivery')
    return {
      id: delivery._id,
      deliveryId: delivery.deliveryId,
      deliveryName: deliveryProvider.name,
      deliveryImageUrl: deliveryProvider.imageUrl,
      providerId: deliveryProvider?.providerId,
      username: delivery.username,
      password: delivery.password,
      isActive: delivery.isActive,
      providerStoreId: delivery.providerStoreId,
      note: delivery.note
    }
  }

  async getUberEatsAuthUrl() {
    const payload: IAuthProvider = {
      provider_id: 'ubereats'
    }
    const { data } = await this.httpService.authProvider(payload)
    if (!data.ok) throw new BadRequestException(data?.error_code)

    return { authUrl: data?.result?.auth_url || '/' }
  }

  async updateDelivery(id: string, payload: UpdateDeliveryDto) {
    const delivery = await this.deliveryService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!delivery) {
      throw new NotFoundException('Not found 3rd delivery setting.')
    }

    const authPayload: IAuthProvider = {
      login: payload.username,
      password: payload.password,
      provider_id: payload.providerId
    }

    if (payload.providerId !== 'ubereats') {
      const { data } = await this.httpService.authProvider(authPayload)
      if (!data?.ok) throw new BadRequestException(data?.error_code)

      const { result } = data
      if (!result?.authenticated) {
        throw new BadRequestException('Invalid credentials')
      }
    }

    payload.password = stringToBase64(payload.password)
    await this.deliveryService.update(id, payload)
    return { success: true }
  }

  async handleConnection(id: string, payload: ConnectProviderDto) {
    const delivery = await this.deliveryService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!delivery) {
      throw new NotFoundException('Not found 3rd delivery setting.')
    }
    if (delivery.isActive === payload.isActive) {
      const message = delivery.isActive
        ? `${delivery.providerId} is already connected`
        : `${delivery.providerId} is already disconnected`
      throw new BadRequestException(message)
    }

    const deliverySetting = await this.deliverySettingService.findOne(
      {
        restaurantId: delivery.restaurantId
      },
      { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 }
    )

    if (payload.isActive) {
      const connectPayload: IConnectProvider = {
        login: delivery.username,
        password: decodeBase64toString(delivery.password),
        provider_id: delivery.providerId,
        provider_store_id: delivery.providerStoreId,
        store_id: deliverySetting.storeId
      }
      const { data: connected } = await this.httpService.connectProvider(
        connectPayload
      )
      if (!connected?.ok) throw new BadRequestException(connected?.error_code)
    } else {
      const disconnectPayload: IDisconnectProvider = {
        provider_id: delivery.providerId,
        store_id: deliverySetting.storeId
      }
      const { data: disconnected } = await this.httpService.disconnectProvider(
        disconnectPayload
      )
      if (!disconnected?.ok)
        throw new BadRequestException(disconnected?.error_code)
    }

    await this.deliveryService.update(id, { isActive: payload.isActive })
    return
  }
}
