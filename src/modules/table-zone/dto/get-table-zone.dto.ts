import { IsString, IsOptional } from 'class-validator'
import { ApiPropertyOptional } from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaginateDto } from 'src/common/dto/paginate.dto'

export class TableZonePaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'name' })
  readonly sortBy: string = 'name'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'asc' })
  readonly sortOrder: string = 'asc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'test' })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '<restaurant-id>' })
  readonly restaurant: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          name: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      restaurantId: { $eq: !id ? this.restaurant : id },
      status: this.status || { $ne: StatusEnum.DELETED }
    }
    if (!id && !this.restaurant) delete result.restaurantId
    return result
  }
}
