import { ApiPropertyOptional } from '@nestjs/swagger'
import { IsEnum, IsOptional, IsString } from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'

export class UpdateTableZoneDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Table' })
  readonly name: string

  @IsOptional()
  @IsEnum(StatusEnum)
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string
}
