import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator'
import { StatusEnum } from 'src/common/enum/status.enum'

export class CreateTableZoneDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Table' })
  readonly name: string

  @IsOptional()
  @IsEnum(StatusEnum)
  @ApiPropertyOptional({
    example: StatusEnum.ACTIVE,
    enum: Object.values(StatusEnum)
  })
  readonly status: string
}
