import { StatusEnum } from 'src/common/enum/status.enum'

export class AllTableZonePaginateDto {
  public buildQuery(id: string) {
    const result = {
      restaurantId: { $eq: id },
      status: StatusEnum.ACTIVE
    }

    return result
  }
}
