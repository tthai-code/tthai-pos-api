import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { CreateTableZoneDto } from '../dto/create-table-zone.dto'
import { AllTableZonePaginateDto } from '../dto/get-all-table-zone.dto'
import { TableZonePaginateDto } from '../dto/get-table-zone.dto'
import { UpdateTableZoneDto } from '../dto/update-table-zone.dto'
import { GetAllTableZoneResponse } from '../entitiy/table-zone.entity'
import { TableZoneService } from '../services/table-zone.service'

@ApiBearerAuth()
@ApiTags('table-zone')
@Controller('v1/table-zone')
@UseGuards(AdminAuthGuard)
export class TableZoneController {
  constructor(private readonly tableZoneService: TableZoneService) {}

  @Get()
  @ApiOperation({ summary: 'Get List Paginate Table Zone for Management' })
  async getTableZones(@Query() query: TableZonePaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.tableZoneService.paginate(
      query.buildQuery(restaurantId),
      query,
      { path: 'amount', match: { status: StatusEnum.ACTIVE } }
    )
  }

  @Get('all')
  @ApiOperation({ summary: 'Get all table zone for POS' })
  @ApiOkResponse({ type: () => GetAllTableZoneResponse })
  async getAllTableZone(@Query() query: AllTableZonePaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.tableZoneService.getAll(query.buildQuery(restaurantId), {
      id: 1,
      name: 1
    })
  }

  @Post()
  @ApiOperation({ summary: 'Create a Table Zone' })
  async createTableZone(@Body() payload: CreateTableZoneDto) {
    return await this.tableZoneService.create(payload)
  }

  @Get(':id/table-zone')
  @ApiOperation({ summary: 'Get Table Zone By ID' })
  async getTableZone(@Param('id') id: string) {
    return await this.tableZoneService.findOne({ _id: id })
  }

  @Put(':id')
  @ApiOperation({ summary: 'Update Table Zone' })
  async updateTableZone(
    @Param('id') id: string,
    @Body() payload: UpdateTableZoneDto
  ) {
    return await this.tableZoneService.update(id, payload)
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete Table Zone (Soft)' })
  async deleteTableZone(@Param('id') id: string) {
    return await this.tableZoneService.delete(id)
  }
}
