import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

export class TableZoneResponseFields {
  @ApiProperty()
  id: string

  @ApiProperty()
  name: string
}

export class GetAllTableZoneResponse extends ResponseDto<
  Array<TableZoneResponseFields>
> {
  @ApiProperty({
    type: TableZoneResponseFields,
    example: [
      {
        name: 'Table',
        id: '6312697243656a16831e950c'
      },
      {
        name: 'Bar',
        id: '63128eda000e63764c3b59fc'
      }
    ]
  })
  data: Array<TableZoneResponseFields>
}
