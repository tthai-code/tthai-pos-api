import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type TableZoneDocument = TableZoneFields & Document

@Schema({
  timestamps: true,
  strict: true,
  collection: 'tableZones',
  toJSON: { virtuals: true, getters: true },
  toObject: { virtuals: true, getters: true }
})
export class TableZoneFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop()
  name: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const TableZoneSchema = SchemaFactory.createForClass(TableZoneFields)
TableZoneSchema.plugin(mongoosePaginate)
TableZoneSchema.plugin(authorStampCreatePlugin)
TableZoneSchema.virtual('amount', {
  ref: 'table',
  localField: '_id',
  foreignField: 'tableZone.id',
  count: true
})
