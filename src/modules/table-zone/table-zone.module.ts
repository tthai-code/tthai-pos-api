import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { TableZoneController } from './controllers/table-zone.controller'
import { TableZoneSchema } from './models/table-zone.schema'
import { TableZoneService } from './services/table-zone.service'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'tableZone', schema: TableZoneSchema }])
  ],
  providers: [TableZoneService],
  exports: [TableZoneService],
  controllers: [TableZoneController]
})
export class TableZoneModule {}
