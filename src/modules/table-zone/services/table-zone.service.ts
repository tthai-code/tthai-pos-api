import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { TableZoneDocument } from '../models/table-zone.schema'
import { TableZonePaginateDto } from '../dto/get-table-zone.dto'

@Injectable({ scope: Scope.REQUEST })
export class TableZoneService {
  constructor(
    @InjectModel('tableZone')
    private readonly TableZoneModel: PaginateModel<TableZoneDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<TableZoneDocument | any> {
    return this.TableZoneModel.findById({ _id: id })
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.TableZoneModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<TableZoneDocument> {
    return this.TableZoneModel
  }

  async getSession(): Promise<ClientSession> {
    await this.TableZoneModel.createCollection()

    return this.TableZoneModel.startSession()
  }

  async create(payload: any): Promise<TableZoneDocument> {
    const document = new this.TableZoneModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, SeatType: any): Promise<TableZoneDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...SeatType }).save()
  }

  async delete(id: string): Promise<TableZoneDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.TableZoneModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.TableZoneModel.findById(id)
  }

  findOne(
    condition: any,
    project?: any,
    options?: any
  ): Promise<TableZoneDocument> {
    return this.TableZoneModel.findOne(condition, project, options)
  }

  paginate(
    query: any,
    queryParam: TableZonePaginateDto,
    populate?: any
  ): Promise<PaginateResult<TableZoneDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      populate
    }
    if (!populate) delete options.populate

    return this.TableZoneModel.paginate(query, options)
  }
}
