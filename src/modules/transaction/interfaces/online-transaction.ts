import {
  ICustomerObjectOrderPayload,
  IMenuItemsObjectPayload
} from 'src/modules/order/interfaces/third-party-order.interfaces'

export interface IOnlineTransaction {
  restaurantId?: any
  txn?: string
  orderId?: string
  orderType?: string
  customer?: ICustomerObjectOrderPayload
  summaryItems?: IMenuItemsObjectPayload[] | any
  subtotal?: number
  discount?: number
  serviceCharge?: number
  tax?: number
  alcoholTax?: number
  convenienceFee?: number
  total?: number
  splitMethod?: string
  splitBy?: number
  cardType?: string
  paymentMethod?: string
  cardLastFour?: string
  paidBy?: string
  retRef?: string
  batchId?: number
  openDate?: Date
  closeDate?: Date
  paymentStatus?: string
  paidAmount?: number
  tips?: number
  totalPaid?: number
  transactionStatus?: string
  response?: any
  refundedResponse?: any
  refundedItems?: any
  refundedAmount?: number
  refundedReason?: string
  deliverySource?: string
  note?: string
}

export interface IPaymentMethodCardType {
  retRef: string
  batchId: string
  cardType: string
  paymentMethod: string
  cardLastFour: string
  paidBy: string
  response: any
}
