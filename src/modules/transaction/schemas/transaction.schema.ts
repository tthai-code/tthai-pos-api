import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { OrderItemsFields } from 'src/modules/order/schemas/order-items.schema'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'
import { PaymentMethodEnum } from '../common/payment-method.enum'
import { PaymentStatusEnum } from '../common/payment-status.enum'
import { SplitMethodEnum } from '../common/split-method.enum'
import { OrderTypeEnum } from 'src/modules/order/enum/order-type.enum'
import { CustomerFields } from 'src/modules/order/schemas/order.schema'
import { TransactionStatusEnum } from '../common/transaction-status.enum'

export type TransactionDocument = TransactionFields & Document

@Schema({ timestamps: true, strict: true, collection: 'transactions' })
export class TransactionFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  txn: string

  @Prop({ required: true })
  orderId: string

  @Prop({ default: '' })
  porderId: string

  @Prop({ required: true, enum: Object.values(OrderTypeEnum) })
  orderType: string

  @Prop({ default: new CustomerFields() })
  customer: CustomerFields

  @Prop({ required: true })
  summaryItems: Array<OrderItemsFields>

  @Prop({ default: 0 })
  subtotal: number

  @Prop({ default: 0 })
  discount: number

  @Prop({ default: 0 })
  serviceCharge: number

  @Prop({ default: 0 })
  tax: number

  @Prop({ default: 0 })
  alcoholTax: number

  @Prop({ default: 0 })
  convenienceFee: number

  @Prop({ default: 0 })
  total: number

  @Prop({
    enum: Object.values(SplitMethodEnum),
    default: SplitMethodEnum.NO_SPLIT
  })
  splitMethod: string

  @Prop({ default: 1 })
  splitBy: number

  @Prop({ default: [] })
  splitItems: Array<OrderItemsFields>

  @Prop({ default: 0 })
  totalSplitItems: number

  @Prop({ default: null })
  cardType: string

  @Prop({ enum: Object.values(PaymentMethodEnum), required: true })
  paymentMethod: string

  @Prop({ default: null })
  cardLastFour: string

  @Prop({ default: 'Guest' })
  paidBy: string

  @Prop({ default: null })
  retRef: string

  @Prop({ default: null })
  batchId: number

  @Prop({ required: true })
  openDate: Date

  @Prop({ default: null })
  closeDate: Date

  @Prop({
    enum: Object.values(PaymentStatusEnum),
    default: PaymentStatusEnum.PAID
  })
  paymentStatus: string

  @Prop({ default: false })
  isVoidBeforeTransaction: boolean

  @Prop({ default: 0 })
  paidAmount: number

  @Prop({ default: 0 })
  tips: number

  @Prop({ default: 0 })
  totalPaid: number

  @Prop({
    enum: Object.values(TransactionStatusEnum),
    default: TransactionStatusEnum.OPEN
  })
  transactionStatus: string

  @Prop({ default: null })
  response: MongooseSchema.Types.Mixed

  @Prop({ default: null })
  refundedResponse: MongooseSchema.Types.Mixed

  @Prop({ default: null })
  voidResponse: MongooseSchema.Types.Mixed

  @Prop({ default: [] })
  refundedItems: Array<OrderItemsFields>

  @Prop({ default: 0 })
  refundedAmount: number

  @Prop({ default: '' })
  refundedReason: string

  @Prop({ default: null })
  deliverySource: string

  @Prop({ default: null })
  signature: string

  @Prop({ default: '' })
  note: string

  @Prop({ default: null })
  voidReason: string

  @Prop({ default: [] })
  voidItems: Array<OrderItemsFields>

  @Prop({ default: 0 })
  voidAmount: number

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const TransactionSchema = SchemaFactory.createForClass(TransactionFields)
TransactionSchema.plugin(authorStampCreatePlugin)
TransactionSchema.plugin(mongoosePaginate)
