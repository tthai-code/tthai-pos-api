import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoose from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type TransactionLogDocument = TransactionLogFields & mongoose.Document

@Schema({
  timestamps: true,
  collection: 'transactionLogs'
})
export class TransactionLogFields {
  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  ip: string

  @Prop()
  baseUrl: string

  @Prop()
  originalUrl: string

  @Prop()
  headers: mongoose.Schema.Types.Mixed

  @Prop()
  body: mongoose.Schema.Types.Mixed

  @Prop()
  params: mongoose.Schema.Types.Mixed

  @Prop()
  query: mongoose.Schema.Types.Mixed

  @Prop()
  method: string

  @Prop()
  note: string

  @Prop()
  req: mongoose.Schema.Types.Mixed

  @Prop()
  res: mongoose.Schema.Types.Mixed

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}

export const TransactionLogSchema =
  SchemaFactory.createForClass(TransactionLogFields)
TransactionLogSchema.plugin(mongoosePaginate)
TransactionLogSchema.plugin(authorStampCreatePlugin)
