import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import * as mongoose from 'mongoose'

export type TransactionCounterDocument = TransactionCounter & mongoose.Document

@Schema({ timestamps: true, collection: '_transactionCounter' })
export class TransactionCounter {
  @Prop({ required: true })
  prefix: string

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: mongoose.Types.ObjectId

  @Prop({ default: 0 })
  counter: number
}

export const TransactionCounterSchema =
  SchemaFactory.createForClass(TransactionCounter)
