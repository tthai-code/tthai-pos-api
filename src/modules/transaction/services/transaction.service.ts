import { InjectModel } from '@nestjs/mongoose'
import { Inject, Injectable, Scope } from '@nestjs/common'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { ClientSession } from 'mongodb'
import { REQUEST } from '@nestjs/core'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { TransactionDocument } from '../schemas/transaction.schema'
import { TransactionPaginateDto } from '../dto/get-transaction.dto'

@Injectable({ scope: Scope.REQUEST })
export class TransactionService {
  constructor(
    @InjectModel('transaction')
    private readonly TransactionModel: PaginateModel<TransactionDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<TransactionDocument | any> {
    return this.TransactionModel.findById({ _id: id })
  }

  getRequest() {
    return this.request
  }

  count(query) {
    return this.TransactionModel.countDocuments(query)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.TransactionModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<TransactionDocument> {
    return this.TransactionModel
  }

  async getSession(): Promise<ClientSession> {
    await this.TransactionModel.createCollection()

    return await this.TransactionModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<TransactionDocument> {
    if (!payload.porderId) {
      payload.porderId = payload.orderId
    }
    const document = new this.TransactionModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.TransactionModel.aggregate(pipeline)
  }

  async create(payload: any): Promise<TransactionDocument> {
    if (!payload.porderId) {
      payload.porderId = payload.orderId
    }
    const document = new this.TransactionModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, product: any): Promise<TransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    if (!document.porderId && !product.porderId) {
      product.porderId = document.orderId
    }

    return document.set({ ...product }).save()
  }

  async updateOne(condition: any, payload: any): Promise<TransactionDocument> {
    if (!payload.porderId) {
      payload.porderId = payload.orderId
    }
    return this.TransactionModel.updateOne(condition, { ...payload })
  }

  async updateMany(condition: any, payload: any): Promise<Array<any>> {
    return this.TransactionModel.updateMany(condition, { $set: payload })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<TransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    if (!document.porderId && !product.porderId) {
      product.porderId = document.orderId
    }
    return document.set({ ...product }).save({ session })
  }

  async transactionUpdateMany(
    condition: any,
    payload: any,
    session: ClientSession
  ): Promise<any> {
    return this.TransactionModel.updateMany(
      condition,
      { $set: payload },
      { session }
    )
  }

  async delete(id: string): Promise<TransactionDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition, project?: any): Promise<TransactionDocument[]> {
    return this.TransactionModel.find(condition, project)
  }

  findById(id): Promise<any> {
    return this.TransactionModel.findById(id)
  }

  findOne(condition, project?: any): Promise<TransactionDocument> {
    return this.TransactionModel.findOne(condition, project)
  }

  paginate(
    query: any,
    queryParam: TransactionPaginateDto,
    select?: any
  ): Promise<PaginateResult<TransactionDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder },
      select
    }

    return this.TransactionModel.paginate(query, options)
  }
}
