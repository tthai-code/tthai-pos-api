import { InjectModel } from '@nestjs/mongoose'
import { Injectable } from '@nestjs/common'
import { Model } from 'mongoose'
import { TransactionCounterDocument } from '../schemas/transaction-counter.schema'

@Injectable()
export class TransactionCounterService {
  constructor(
    @InjectModel('transaction-counter')
    private readonly TransactionCounterModel: Model<TransactionCounterDocument>
  ) {}

  async getCounter(prefix: string, restaurantId: string) {
    const result = await this.TransactionCounterModel.findOneAndUpdate(
      { prefix, restaurantId },
      { $inc: { counter: 1 } },
      { new: true, upsert: true }
    )

    return result.counter
  }
}
