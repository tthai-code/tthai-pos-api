/* eslint-disable prettier/prettier */
import { Inject, Injectable, Scope } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { PaginateModel, PaginateResult } from 'mongoose-paginate'
import { REQUEST } from '@nestjs/core'
import { TransactionLogPaginateDto } from '../dto/log-transaction.dto'
import { TransactionLogDocument } from '../schemas/log-transaction.schema'

@Injectable({ scope: Scope.REQUEST })
export class TransactionLogService {
  constructor(
    @InjectModel('transactionLog')
    private transactionLogModel: PaginateModel<TransactionLogDocument>,
    @Inject(REQUEST) private request: any
  ) {}

  async create(payload: any): Promise<TransactionLogDocument> {
    const created = new this.transactionLogModel(payload)
    created?.setAuthor(this.request)
    return created.save()
  }

  paginate(
    query: any,
    queryParam: TransactionLogPaginateDto
  ): Promise<PaginateResult<TransactionLogDocument>> {
    const options = {
      page: Number(queryParam.page),
      limit: Number(queryParam.limit),
      sort: { [queryParam.sortBy]: queryParam.sortOrder }
    }

    return this.transactionLogModel.paginate(query, options)
  }
}
