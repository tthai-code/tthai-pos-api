import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class WebBaseSummaryByPaymentMethodObject {
  @ApiProperty()
  label: string
  @ApiProperty()
  amount: number
}

class SummaryByPaymentMethodObject {
  @ApiProperty()
  cash: number
  @ApiProperty()
  credit_card: number
  @ApiProperty()
  debit_card: number
  @ApiProperty()
  others: number
}

class SummaryCloseOfDayObject {
  @ApiProperty()
  total_transaction: number
  @ApiProperty()
  total_collected: number
  @ApiProperty()
  net_sales: number
  @ApiProperty({ type: SummaryByPaymentMethodObject })
  summary_by_payment_method: SummaryByPaymentMethodObject
}

class WebBaseSummaryCloseOfDayObject {
  @ApiProperty()
  total_transaction: number
  @ApiProperty()
  total_collected: number
  @ApiProperty()
  net_sales: number
  @ApiProperty({ type: [WebBaseSummaryByPaymentMethodObject] })
  summary_by_payment_method: WebBaseSummaryByPaymentMethodObject[]
}

export class SummaryCloseOfDayResponse extends ResponseDto<SummaryCloseOfDayObject> {
  @ApiProperty({
    type: SummaryCloseOfDayObject,
    example: {
      total_transaction: 3,
      total_collected: 280.79999999999995,
      net_sales: 240,
      summary_by_payment_method: {
        cash: 2,
        credit_card: 1,
        debit_card: 0,
        others: 0
      }
    }
  })
  data: SummaryCloseOfDayObject
}

export class WebBaseSummaryCloseOfDayResponse extends ResponseDto<WebBaseSummaryCloseOfDayObject> {
  @ApiProperty({
    type: SummaryCloseOfDayObject,
    example: {
      total_transaction: 3,
      total_collected: 27.353433600000002,
      net_sales: 23.99424,
      summary_by_payment_method: [
        {
          label: 'Cash',
          amount: 0
        },
        {
          label: 'Credit Card',
          amount: 0
        },
        {
          label: 'Debit Card',
          amount: 27.353433600000002
        },
        {
          label: 'Check',
          amount: 0
        },
        {
          label: 'Gift Card',
          amount: 0
        },
        {
          label: 'Delivery',
          amount: 0
        },
        {
          label: 'Others',
          amount: 0
        }
      ]
    }
  })
  data: WebBaseSummaryCloseOfDayObject
}
