import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class SuccessResponse {
  @ApiProperty()
  success: boolean
}

export class VoidTransactionResponse extends ResponseDto<SuccessResponse> {
  @ApiProperty({
    type: SuccessResponse,
    example: {
      success: true
    }
  })
  data: SuccessResponse
}
