import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'
import { ItemStatusEnum } from 'src/modules/order/enum/item-status.enum'
import { OrderTypeEnum } from 'src/modules/order/enum/order-type.enum'
import { CardTypeEnum } from '../common/card-type.enum'
import { PaymentMethodEnum } from '../common/payment-method.enum'
import { PaymentStatusEnum } from '../common/payment-status.enum'
import { SplitMethodEnum } from '../common/split-method.enum'
import { TransactionStatusEnum } from '../common/transaction-status.enum'

class ModifierField {
  @ApiProperty()
  id: string
  @ApiProperty()
  label: string
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  name: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  price: number
}

class ItemField {
  @ApiProperty({ enum: Object.values(ItemStatusEnum) })
  item_status: string
  @ApiProperty()
  note: string
  @ApiProperty()
  amount: number
  @ApiProperty()
  unit: number
  @ApiProperty()
  is_contain_alcohol: boolean
  @ApiProperty()
  modifiers: ModifierField[]
  @ApiProperty()
  price: number
  @ApiProperty()
  native_name: string
  @ApiProperty()
  name: string
  @ApiProperty()
  menu_id: string
  @ApiProperty()
  order_id: string
  @ApiProperty()
  porder_id: string
  @ApiProperty()
  id: string
}

class CustomerField {
  @ApiProperty()
  tel: string
  @ApiProperty()
  last_name: string
  @ApiProperty()
  first_name: string
  @ApiProperty()
  id: string
}

class TransactionField extends TimestampResponseDto {
  @ApiProperty()
  response: any
  @ApiProperty({ enum: Object.values(TransactionStatusEnum) })
  transaction_status: string
  @ApiProperty()
  total_paid: number
  @ApiProperty()
  tips: number
  @ApiProperty()
  paid_amount: number
  @ApiProperty({ enum: Object.values(PaymentStatusEnum) })
  payment_status: string
  @ApiProperty()
  close_date: string
  @ApiProperty()
  open_date: string
  @ApiProperty()
  ret_ref: string
  @ApiProperty()
  paid_by: string
  @ApiProperty()
  card_last_four: string
  @ApiProperty({ enum: Object.values(PaymentMethodEnum) })
  payment_method: string
  @ApiProperty({ enum: Object.values(CardTypeEnum) })
  card_type: string
  @ApiProperty()
  split_by: number
  @ApiProperty({ enum: Object.values(SplitMethodEnum) })
  split_method: string
  @ApiProperty()
  total: number
  @ApiProperty()
  convenience_fee: number
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
  @ApiProperty()
  service_charge: number
  @ApiProperty()
  discount: number
  @ApiProperty()
  subtotal: number
  @ApiProperty()
  summary_items: ItemField[]
  @ApiProperty()
  customer: CustomerField
  @ApiProperty({ enum: Object.values(OrderTypeEnum) })
  order_type: string
  @ApiProperty()
  order_id: string
  @ApiProperty()
  porder_id: string
  @ApiProperty()
  txn: string
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
  @ApiProperty()
  ticket_no: string
  @ApiProperty()
  is_void_before_transaction: boolean
}

class TransactionObjectResponse extends PaginateResponseDto<
  Array<TransactionField>
> {
  @ApiProperty({ example: 700 })
  totalCollected: number

  @ApiProperty({ example: 680 })
  netSales: number

  @ApiProperty({
    type: [TransactionField],
    example: {
      status: 'active',
      response: {
        amount: '5.70',
        resptext: 'Approval',
        order_id: 'TESTAMEX01',
        porder_id: 'TESTAMEX01',
        commcard: 'Y',
        cvvresp: 'P',
        respcode: '000',
        batchid: '118',
        avsresp: 'Y',
        entrymode: 'Keyed',
        merchid: '820000003069',
        token: '9472658069221443',
        authcode: 'PPS009',
        respproc: 'RPCT',
        bintype: 'Corp',
        expiry: '1223',
        retref: '314068455663',
        respstat: 'A',
        account: '9472658069221443'
      },
      transaction_status: 'OPEN',
      total_paid: 570,
      tips: 10,
      paid_amount: 560,
      payment_status: 'PAID',
      close_date: null,
      open_date: '2022-11-10T20:19:35.565Z',
      ret_ref: '312573439445',
      paid_by: 'CC TEST',
      card_last_four: '5454',
      payment_method: 'CREDIT',
      card_type: 'MASTERCARD',
      split_by: 1,
      split_method: 'NO_SPLIT',
      total: 560,
      convenience_fee: 0,
      alcohol_tax: 0,
      tax: 0,
      service_charge: 0,
      discount: 0,
      subtotal: 560,
      summary_items: [
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 80,
          unit: 2,
          is_contain_alcohol: true,
          modifiers: [
            {
              id: '634d2a6c57a023457c8e4990',
              label: 'Choice of Protein',
              abbreviation: 'P',
              name: 'No Protein',
              native_name: 'Native Name Test',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Chang Beer',
          menu_id: '634d2a6c57a023457c8e498d',
          order_id: 'HS9SqQB2',
          id: '635ef4cf6813f53e4a6ecd2a'
        },
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 80,
          unit: 2,
          cover_image: '',
          is_contain_alcohol: false,
          modifiers: [
            {
              id: '630fcd08b6fb5ee3bab46885',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Chicken',
              native_name: 'Chicken',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Pad Thai',
          menu_id: '630fc086af525d6ed9fc0d5d',
          order_id: 'HS9SqQB2',
          id: '635f7d600c131551da4c1b7e'
        },
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 80,
          unit: 2,
          cover_image: '',
          is_contain_alcohol: false,
          modifiers: [
            {
              id: '630fcd08b6fb5ee3bab46885',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Chicken',
              native_name: 'Chicken',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Pad Thai',
          menu_id: '630fc086af525d6ed9fc0d5d',
          order_id: 'HS9SqQB2',
          id: '635f83ed4b2d43f09a5d0cb4'
        },
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 80,
          unit: 2,
          cover_image: '',
          is_contain_alcohol: false,
          modifiers: [
            {
              id: '630fcd08b6fb5ee3bab46885',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Pork',
              native_name: 'Pork',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Pad Thai',
          menu_id: '630fc086af525d6ed9fc0d5d',
          order_id: 'HS9SqQB2',
          id: '635fc1b31b42e31b8c3c5b8a'
        },
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 80,
          unit: 2,
          cover_image: '',
          is_contain_alcohol: false,
          modifiers: [
            {
              id: '630fcd08b6fb5ee3bab46885',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Chicken',
              native_name: 'Chicken',
              price: 0
            },
            {
              id: '630fcd08b6fb5ee3bab46885',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Pork',
              native_name: 'Pork',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Pad Thai',
          menu_id: '630fc086af525d6ed9fc0d5d',
          order_id: 'HS9SqQB2',
          id: '635fc1e11b42e31b8c3c5b95'
        },
        {
          item_status: 'IN_PROGRESS',
          note: '',
          amount: 80,
          unit: 2,
          cover_image: '',
          is_contain_alcohol: false,
          modifiers: [
            {
              id: '630fcd08b6fb5ee3bab46885',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Pork',
              native_name: 'Pork',
              price: 0
            },
            {
              id: '630fcd08b6fb5ee3bab46885',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Chicken',
              native_name: 'Chicken',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Pad Thai',
          menu_id: '630fc086af525d6ed9fc0d5d',
          order_id: 'HS9SqQB2',
          id: '635fc1eb1b42e31b8c3c5b9e'
        },
        {
          item_status: 'READY',
          note: '',
          amount: 80,
          unit: 2,
          cover_image: '',
          is_contain_alcohol: false,
          modifiers: [
            {
              id: '630fcd08b6fb5ee3bab46885',
              label: 'Protein Choices',
              abbreviation: 'P',
              name: 'Chicken',
              native_name: 'Chicken',
              price: 0
            }
          ],
          price: 40,
          native_name: 'test nativeName',
          name: 'Pad Thai',
          menu_id: '630fc086af525d6ed9fc0d5d',
          order_id: 'HS9SqQB2',
          id: '63601f35ee84c9491202f524'
        }
      ],
      customer: {
        tel: '',
        last_name: '',
        first_name: 'Guest'
      },
      order_type: 'DINE_IN',
      order_id: 'HS9SqQB2',
      ticket_no: '6',
      is_void_before_transaction: false,
      txn: '1',
      restaurant_id: '630e55a0d9c30fd7cdcb424b',
      created_at: '2022-11-10T20:28:24.513Z',
      updated_at: '2022-11-10T20:28:24.513Z',
      updated_by: {
        username: 'Plini',
        id: '630f1c078653105bf1478b82'
      },
      created_by: {
        username: 'Plini',
        id: '630f1c078653105bf1478b82'
      },
      id: '636d5ee878fb39ea5dd286d7'
    }
  })
  results: Array<TransactionField>
}

export class PaginateTransactionResponse extends ResponseDto<TransactionObjectResponse> {
  @ApiProperty({ type: TransactionObjectResponse })
  data: TransactionObjectResponse
}
