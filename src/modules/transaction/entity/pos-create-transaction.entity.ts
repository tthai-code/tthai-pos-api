import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity'
import { ItemStatusEnum } from 'src/modules/order/enum/item-status.enum'
import { OrderTypeEnum } from 'src/modules/order/enum/order-type.enum'
import { CardTypeEnum } from '../common/card-type.enum'
import { PaymentMethodEnum } from '../common/payment-method.enum'
import { PaymentStatusEnum } from '../common/payment-status.enum'
import { SplitMethodEnum } from '../common/split-method.enum'
import { TransactionStatusEnum } from '../common/transaction-status.enum'

class ModifierField {
  @ApiProperty()
  id: string
  @ApiProperty()
  label: string
  @ApiProperty()
  abbreviation: string
  @ApiProperty()
  name: string
  @ApiProperty()
  native_name: string
  @ApiProperty()
  price: number
}

class ItemField {
  @ApiProperty({ enum: Object.values(ItemStatusEnum) })
  item_status: string
  @ApiProperty()
  note: string
  @ApiProperty()
  amount: number
  @ApiProperty()
  unit: number
  @ApiProperty()
  is_contain_alcohol: boolean
  @ApiProperty()
  modifiers: ModifierField[]
  @ApiProperty()
  price: number
  @ApiProperty()
  native_name: string
  @ApiProperty()
  name: string
  @ApiProperty()
  menu_id: string
  @ApiProperty()
  order_id: string
  @ApiProperty()
  porder_id: string
  @ApiProperty()
  id: string
}

class CustomerField {
  @ApiProperty()
  tel: string
  @ApiProperty()
  last_name: string
  @ApiProperty()
  first_name: string
  @ApiProperty()
  id: string
}

class TransactionField extends TimestampResponseDto {
  @ApiProperty()
  response: any
  @ApiProperty({ enum: Object.values(TransactionStatusEnum) })
  transaction_status: string
  @ApiProperty()
  total_paid: number
  @ApiProperty()
  tips: number
  @ApiProperty()
  paid_amount: number
  @ApiProperty({ enum: Object.values(PaymentStatusEnum) })
  payment_status: string
  @ApiProperty()
  close_date: string
  @ApiProperty()
  open_date: string
  @ApiProperty()
  ret_ref: string
  @ApiProperty()
  paid_by: string
  @ApiProperty()
  card_last_four: string
  @ApiProperty({ enum: Object.values(PaymentMethodEnum) })
  payment_method: string
  @ApiProperty({ enum: Object.values(CardTypeEnum) })
  card_type: string
  @ApiProperty()
  total_split_items: number
  @ApiProperty({ type: [ItemField] })
  split_items: ItemField[]
  @ApiProperty()
  split_by: number
  @ApiProperty({ enum: Object.values(SplitMethodEnum) })
  split_method: string
  @ApiProperty()
  total: number
  @ApiProperty()
  convenience_fee: number
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  tax: number
  @ApiProperty()
  service_charge: number
  @ApiProperty()
  discount: number
  @ApiProperty()
  subtotal: number
  @ApiProperty({ type: [ItemField] })
  summary_items: ItemField[]
  @ApiProperty()
  customer: CustomerField
  @ApiProperty({ enum: Object.values(OrderTypeEnum) })
  order_type: string
  @ApiProperty()
  order_id: string
  @ApiProperty()
  porder_id: string
  @ApiProperty()
  txn: string
  @ApiProperty()
  is_void_before_transaction: boolean
  @ApiProperty()
  restaurant_id: string
  @ApiProperty()
  id: string
}
export class CreateTransactionResponse extends ResponseDto<TransactionField> {
  @ApiProperty({
    type: TransactionField,
    example: {
      status: 'active',
      note: ' ',
      signature: null,
      delivery_source: null,
      refunded_reason: '',
      refunded_amount: 0,
      refunded_items: [],
      response: null,
      transaction_status: 'OPEN',
      total_paid: 24.2,
      tips: 10,
      paid_amount: 14.2,
      payment_status: 'PAID',
      close_date: null,
      open_date: '2023-01-24T05:15:19.926Z',
      batch_id: null,
      ret_ref: null,
      paid_by: 'GUEST',
      card_last_four: null,
      payment_method: 'CASH',
      card_type: null,
      total_split_items: 12,
      split_items: [
        {
          menu_id: '639d4055f643c9055745b36f',
          name: 'TEst 3',
          native_name: 'Test 3',
          price: 12,
          modifiers: [
            {
              abbreviation: 'Test',
              id: '63cecffb6c3735b0588bbc5c',
              label: 'Test Modifiers',
              native_name: 'Test3',
              name: 'Test3',
              price: 0
            },
            {
              abbreviation: 'P',
              id: '63ced04a6c3735b0588bbcb1',
              label: 'Protein Choice',
              native_name: 'Chicken',
              name: 'Chicken',
              price: 0
            }
          ],
          is_contain_alcohol: false,
          cover_image:
            'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341812368128171.png',
          unit: 1,
          amount: 12,
          note: ''
        }
      ],
      split_by: 2,
      split_method: 'BY_ITEMS',
      total: 26.4,
      convenience_fee: 0,
      alcohol_tax: 0,
      tax: 2.2,
      service_charge: 2.2,
      discount: 0,
      subtotal: 22,
      summary_items: [
        {
          menu_id: '639d3fd8d248e2ea7245147c',
          name: 'Test Error',
          native_name: 'Error',
          price: 10,
          modifiers: [
            {
              abbreviation: 'P',
              id: '63a8035744e33943c3e37de4',
              label: 'Protein Choice',
              native_name: 'Pork',
              name: 'Pork',
              price: 0
            }
          ],
          is_contain_alcohol: false,
          cover_image:
            'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341660144217278.jpeg',
          unit: 1,
          amount: 10,
          note: ''
        },
        {
          menu_id: '639d4055f643c9055745b36f',
          name: 'TEst 3',
          native_name: 'Test 3',
          price: 12,
          modifiers: [
            {
              abbreviation: 'Test',
              id: '63cecffb6c3735b0588bbc5c',
              label: 'Test Modifiers',
              native_name: 'Test3',
              name: 'Test3',
              price: 0
            },
            {
              abbreviation: 'P',
              id: '63ced04a6c3735b0588bbcb1',
              label: 'Protein Choice',
              native_name: 'Chicken',
              name: 'Chicken',
              price: 0
            }
          ],
          is_contain_alcohol: false,
          cover_image:
            'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341812368128171.png',
          unit: 1,
          amount: 12,
          note: ''
        }
      ],
      customer: {
        tel: '',
        last_name: '',
        first_name: 'Guest'
      },
      order_type: 'TO_GO',
      order_id: '2MmQ5yTY',
      txn: '001',
      is_void_before_transaction: false,
      restaurant_id: '63984d6690416c27a7415915',
      created_at: '2023-01-24T05:15:38.005Z',
      updated_at: '2023-01-24T05:15:38.005Z',
      updated_by: {
        username: 'Test',
        id: '63a03f43014de42db475bba9'
      },
      created_by: {
        username: 'Test',
        id: '63a03f43014de42db475bba9'
      },
      id: '63cf6979436eeccc39062a1c'
    }
  })
  data: TransactionField
}
