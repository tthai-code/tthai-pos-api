import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class CloseOfDayStatusObject {
  @ApiProperty()
  is_closed: boolean
}

export class CloseOfDayStatusResponse extends ResponseDto<CloseOfDayStatusObject> {
  @ApiProperty({
    type: CloseOfDayStatusObject,
    example: {
      is_closed: false
    }
  })
  data: CloseOfDayStatusObject
}
