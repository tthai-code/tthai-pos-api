import { forwardRef, Global, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { CashDrawerModule } from '../cash-drawer/cash-drawer.module'
import { OrderModule } from '../order/order.module'
import { PaymentGatewayModule } from '../payment-gateway/payment-gateway.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { TableModule } from '../table/table.module'
import { TransactionLogController } from './controllers/log-transaction.controller'
import { POSTransactionController } from './controllers/pos-transaction.controller'
import { TransactionController } from './controllers/transaction.controller'
import { TransactionLogLogic } from './logics/log-transaction.logic'
import { POSTransactionLogic } from './logics/pos-transaction.logic'
import { TransactionLogic } from './logics/transaction.logic'
import { VoidTransactionLogic } from './logics/void-transaction.logic'
import { TransactionLogSchema } from './schemas/log-transaction.schema'
import { TransactionCounterSchema } from './schemas/transaction-counter.schema'
import { TransactionSchema } from './schemas/transaction.schema'
import { TransactionLogService } from './services/log.transaction.service'
import { TransactionCounterService } from './services/transaction-counter.service'
import { TransactionService } from './services/transaction.service'

@Global()
@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    forwardRef(() => OrderModule),
    forwardRef(() => TableModule),
    forwardRef(() => PaymentGatewayModule),
    forwardRef(() => CashDrawerModule),
    MongooseModule.forFeature([
      { name: 'transactionLog', schema: TransactionLogSchema },
      { name: 'transaction', schema: TransactionSchema },
      { name: 'transaction-counter', schema: TransactionCounterSchema }
    ])
  ],
  providers: [
    TransactionLogService,
    TransactionLogLogic,
    TransactionLogic,
    TransactionService,
    TransactionCounterService,
    VoidTransactionLogic,
    POSTransactionLogic
  ],
  controllers: [
    TransactionLogController,
    POSTransactionController,
    TransactionController
  ],
  exports: [
    TransactionLogLogic,
    TransactionLogic,
    TransactionService,
    VoidTransactionLogic
  ]
})
export class TransactionModule {}
