import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { ClientSession } from 'mongoose'
import dayjs from 'src/plugins/dayjs'
import { StatusEnum } from 'src/common/enum/status.enum'
import { runTransaction } from 'src/modules/database/mongoose.transaction'
import { OrderStateEnum } from 'src/modules/order/enum/order-state.enum'
import { OrderService } from 'src/modules/order/services/order.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CreateTransactionPOSDto } from '../dto/create-transaction.dto'
import { TransactionCounterService } from '../services/transaction-counter.service'
import { TransactionService } from '../services/transaction.service'
import { PaymentStatusEnum } from '../common/payment-status.enum'
import { TransactionStatusEnum } from '../common/transaction-status.enum'
import { OrderTypeEnum } from 'src/modules/order/enum/order-type.enum'
import { TableService } from 'src/modules/table/services/table.service'
import { TableStatusEnum } from 'src/modules/table/common/table-status.enum'
import { CardPointeLogic } from 'src/modules/payment-gateway/logics/card-pointe.logic'
import { RefundTransactionDto } from '../dto/refund-transaction.dto'
import { PaymentMethodEnum } from '../common/payment-method.enum'
import { TransactionPaginateDto } from '../dto/get-transaction.dto'
import { AddTipsDto } from '../dto/add-tips.dto'
import { ThirdPartyOrderDocument } from 'src/modules/order/schemas/third-party-order.schema'
import { IThirdPartyOrderTransaction } from '../interfaces/third-party-transaction'
import { CloseOfDayDto } from '../dto/close-of-day.dto'
import { SummaryClosOfDayDto } from '../dto/summary-close.dto'
import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service'
import { ItemStatusEnum } from 'src/modules/order/enum/item-status.enum'
import {
  IOnlineTransaction,
  IPaymentMethodCardType
} from '../interfaces/online-transaction'
import { OrderOnlineDocument } from 'src/modules/order/schemas/order-online.schema'
import { OrderOnlineService } from 'src/modules/order/services/order-online.service'
import { POSCashDrawerLogic } from 'src/modules/cash-drawer/logic/pos-cash-drawer.logic'
import { CashDrawerActionEnum } from 'src/modules/cash-drawer/common/cash-drawer.enum'
import { TransactionDocument } from '../schemas/transaction.schema'
import { OrderDocument } from 'src/modules/order/schemas/order.schema'

@Injectable()
export class TransactionLogic {
  constructor(
    private readonly transactionService: TransactionService,
    private readonly transactionCounterService: TransactionCounterService,
    private readonly restaurantService: RestaurantService,
    private readonly orderService: OrderService,
    private readonly onlineOrderService: OrderOnlineService,
    private readonly tableService: TableService,
    private readonly cardPointeLogic: CardPointeLogic,
    private readonly kitchenQueueService: KitchenQueueService,
    private readonly posCashDrawerLogic: POSCashDrawerLogic
  ) {}

  private async genTransactionId(restaurantId: string): Promise<string> {
    const { timeZone } = await this.restaurantService.findOne({
      _id: restaurantId
    })
    const now = dayjs().tz(timeZone)
    const nowUnix = now.valueOf()
    const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf()
    const endOfDay = now
      .add(1, 'day')
      .hour(3)
      .minute(59)
      .millisecond(999)
      .valueOf()
    let nowFormat = now.subtract(1, 'day').format('YYYYMMDD')

    if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
      nowFormat = now.format('YYYYMMDD')
    }

    const countQueue = await this.transactionCounterService.getCounter(
      nowFormat,
      restaurantId
    )
    return `${countQueue.toString().padStart(3, '0')}`
  }

  private summaryTransactionItem(items: any[], key: string): number {
    return (
      items
        .map((item) => item[key])
        .reduce((sum, current) => sum + current, 0) || 0
    )
  }

  async createTransactionPOS(payload: CreateTransactionPOSDto) {
    const logic = async (session: ClientSession) => {
      const restaurant = await this.restaurantService.findOne({
        _id: payload.restaurantId,
        status: StatusEnum.ACTIVE
      })
      if (!restaurant) throw new NotFoundException('Not found restaurant.')
      const order = await this.orderService.findOne({
        _id: payload.orderId,
        status: StatusEnum.ACTIVE
      })
      if (!order) {
        throw new NotFoundException('Not found order.')
      }
      const txn = await this.genTransactionId(payload.restaurantId)
      payload.txn = txn
      payload.paymentStatus = PaymentStatusEnum.PAID
      payload.transactionStatus = TransactionStatusEnum.OPEN

      const created = await this.transactionService.transactionCreate(
        payload,
        session
      )
      if (payload.orderType === OrderTypeEnum.DINE_IN) {
        await this.tableService.transactionUpdate(
          order.table.id,
          {
            tableStatus: TableStatusEnum.AVAILABLE
          },
          session
        )
      }
      await this.orderService.transactionUpdate(
        payload.orderId,
        { orderStatus: OrderStateEnum.COMPLETED, discount: payload.discount },
        session
      )
      await this.kitchenQueueService.transactionUpdateMany(
        { orderId: payload.orderId },
        { queueStatus: ItemStatusEnum.COMPLETED },
        session
      )
      return created
    }
    const created = await runTransaction(logic)
    if (created.paymentMethod === PaymentMethodEnum.CASH) {
      await this.posCashDrawerLogic.cashSales(payload.restaurantId, {
        date: created.openDate,
        amount: created.totalPaid,
        action: CashDrawerActionEnum.CASH_SALES,
        user: created?.createdBy?.username,
        comment: `Order# ${created.orderId}`
      })
    }

    return created
  }

  async handleVoidCreditDebit(
    transactionId: string,
    restaurantId: string,
    transaction: TransactionDocument
  ) {
    if (!transaction.retRef)
      throw new BadRequestException('This transaction not have a retRef.')
    const { data } = await this.cardPointeLogic.voidTransaction(
      transaction.retRef,
      restaurantId
    )
    if (data.respstat !== 'A') throw new BadRequestException(data.resptext)
    return data
  }

  async handleVoidOrder(
    transaction: TransactionDocument,
    session: ClientSession
  ): Promise<OrderDocument | OrderOnlineDocument> {
    if (
      transaction.orderType === OrderTypeEnum.DINE_IN ||
      transaction.orderType === OrderTypeEnum.TO_GO
    ) {
      return this.orderService.transactionUpdate(
        transaction.orderId,
        { orderStatus: OrderStateEnum.VOID },
        session
      )
    }
    if (transaction.orderType === OrderTypeEnum.ONLINE) {
      return this.onlineOrderService.transactionUpdate(
        transaction.orderId,
        { orderStatus: OrderStateEnum.VOID },
        session
      )
    }
  }

  async voidTransaction(transactionId: string, restaurantId: string) {
    const logic = async (session: ClientSession) => {
      const transaction = await this.transactionService.findOne({
        _id: transactionId,
        status: StatusEnum.ACTIVE
      })
      if (!transaction) throw new NotFoundException('Not found transaction.')
      if (transaction.paymentStatus === PaymentStatusEnum.VOID) {
        throw new BadRequestException('This transaction is already voided.')
      }

      if (
        transaction.paymentMethod === PaymentMethodEnum.CREDIT ||
        transaction.paymentMethod === PaymentMethodEnum.DEBIT
      ) {
        const data = await this.handleVoidCreditDebit(
          transactionId,
          restaurantId,
          transaction
        )
        await this.transactionService.transactionUpdate(
          transactionId,
          { paymentStatus: PaymentStatusEnum.VOID, voidResponse: data },
          session
        )
        await this.handleVoidOrder(transaction, session)

        return { success: true, response: data ? data : null }
      }

      await this.transactionService.transactionUpdate(
        transactionId,
        { paymentStatus: PaymentStatusEnum.VOID },
        session
      )
      await this.handleVoidOrder(transaction, session)

      return { success: true, response: null }
    }
    return runTransaction(logic)
  }

  async closeBatch(restaurantId: string, payload: CloseOfDayDto) {
    const startDate = dayjs(payload.startDate)
      .set('second', 0)
      .set('millisecond', 0)
      .toDate()
    const endDate = dayjs(payload.endDate)
      .set('second', 59)
      .set('millisecond', 999)
      .toDate()
    const transactions = await this.transactionService.getAll({
      restaurantId,
      openDate: {
        $gte: startDate,
        $lte: endDate
      },
      status: StatusEnum.ACTIVE
    })
    const batchIds = []
    for (const transaction of transactions) {
      const { batchId } = transaction
      if (!!batchId && !batchIds.includes(batchId)) batchIds.push(batchId)
    }
    const promises = batchIds.map((item) =>
      this.cardPointeLogic.closeBatch(restaurantId, item)
    )
    await Promise.all(promises)
    const query = {
      restaurantId,
      $or: [
        { batchId: { $in: batchIds } },
        {
          paymentMethod: {
            $nin: [PaymentMethodEnum.CREDIT, PaymentMethodEnum.DEBIT]
          }
        }
      ],
      openDate: {
        $gte: startDate,
        $lte: endDate
      },
      status: StatusEnum.ACTIVE
    }
    await this.transactionService.updateMany(query, {
      transactionStatus: TransactionStatusEnum.CLOSED,
      closeDate: new Date()
    })

    return { success: true }
  }

  async refundTransaction(payload: RefundTransactionDto, restaurantId: string) {
    const transaction = await this.transactionService.findOne({
      _id: payload.transactionId,
      status: StatusEnum.ACTIVE
    })
    if (!transaction) throw new NotFoundException('Not found transaction.')
    if (!transaction.retRef) {
      throw new BadRequestException('This transaction not have a retRef.')
    }
    if (transaction.paymentStatus === PaymentStatusEnum.REFUNDED) {
      throw new BadRequestException('This transaction was already refunded.')
    }
    const { data } = await this.cardPointeLogic.refundTransaction(
      transaction.retRef,
      restaurantId,
      payload.amount
    )
    if (data.respstat !== 'A') throw new BadRequestException(data.resptext)
    await this.transactionService.update(payload.transactionId, {
      paymentStatus: PaymentStatusEnum.REFUNDED,
      refundedItems: payload.summaryItems,
      refundAmount: payload.amount || transaction.totalPaid,
      refundedResponse: data,
      refundedReason: payload.refundedReason
    })
    return { success: true }
  }

  async getTransactionList(
    query: TransactionPaginateDto,
    restaurantId: string
  ) {
    const results = await this.transactionService.paginate(
      query.buildQuery(restaurantId),
      query
    )
    const transactions = await this.transactionService.getAll(
      query.buildQuery(restaurantId)
    )
    const summaryTotal = this.summaryTransactionItem(transactions, 'totalPaid')
    const paidTransaction = transactions.filter(
      (item) =>
        item.paymentStatus !== PaymentStatusEnum.VOID &&
        item.paymentMethod !== PaymentMethodEnum.DE_MINIMIS
    )
    const totalCollected = this.summaryTransactionItem(
      paidTransaction,
      'totalPaid'
    )
    const refundedTransaction = transactions.filter(
      (item) => item.paymentStatus === PaymentStatusEnum.REFUNDED
    )
    const paidOrderIds = paidTransaction.map((item) => item.orderId)
    paidOrderIds.forEach((item, index) => {
      if (paidOrderIds.indexOf(item) !== index) {
        paidTransaction.splice(index, 1)
      }
    })
    const summarySubtotal = this.summaryTransactionItem(
      paidTransaction,
      'subtotal'
    )
    const summaryDiscount = this.summaryTransactionItem(
      paidTransaction,
      'discount'
    )
    const summaryRefunded = this.summaryTransactionItem(
      refundedTransaction,
      'refundedAmount'
    )
    const { docs } = results
    const transformDocs = []
    for (const doc of docs) {
      const kitchenQueue = await this.kitchenQueueService.findOne(
        {
          orderId: doc.orderId
        },
        { ticketNo: 1 },
        { sort: { createdAt: -1 } }
      )
      transformDocs.push({
        ...doc?.toObject(),
        ticketNo: kitchenQueue?.ticketNo || null
      })
    }
    return {
      ...results,
      docs: transformDocs,
      totalCollected: totalCollected - summaryRefunded,
      netSales: summarySubtotal - summaryDiscount - summaryRefunded,
      summaryTotal
    }
  }

  async addTipLogic(payload: AddTipsDto) {
    const transaction = await this.transactionService.findOne({
      _id: payload.transactionId,
      orderType: {
        $in: [OrderTypeEnum.DINE_IN, OrderTypeEnum.TO_GO]
      },
      paymentMethod: {
        $in: [PaymentMethodEnum.CREDIT, PaymentMethodEnum.DEBIT]
      },
      transactionStatus: TransactionStatusEnum.OPEN
    })
    if (!transaction) {
      throw new NotFoundException('Not found transaction.')
    }
    const amount = payload.tips + transaction.paidAmount
    const { data } = await this.cardPointeLogic.captureTransaction(
      transaction.retRef,
      `${transaction.restaurantId}`,
      amount
    )
    if (!['00', '000'].includes(data.respcode)) {
      throw new BadRequestException(data.resptext)
    }
    await this.transactionService.update(payload.transactionId, {
      totalPaid: amount,
      tips: payload.tips,
      signature: payload?.signature,
      response: data
    })
    return { success: true }
  }

  async createThirdPartyOrderTransaction(
    order: ThirdPartyOrderDocument,
    session: ClientSession
  ) {
    const payload: IThirdPartyOrderTransaction = {}
    payload.txn = await this.genTransactionId(order.restaurant.id)
    payload.paymentStatus = PaymentStatusEnum.PAID
    payload.transactionStatus = TransactionStatusEnum.OPEN
    payload.restaurantId = order.restaurant.id
    payload.orderType = OrderTypeEnum.THIRD_PARTY
    payload.customer = order.customer
    payload.orderId = order.id
    payload.summaryItems = order.summaryItems
    payload.subtotal = order.subtotal
    payload.discount = order.discount
    payload.serviceCharge = order.serviceCharge
    payload.tax = order.tax
    payload.alcoholTax = order.alcoholTax
    payload.convenienceFee = order.convenienceFee
    payload.total = order.total
    payload.paymentMethod = PaymentMethodEnum.THIRD_PARTY
    payload.paidBy = order?.customer?.firstName
    payload.openDate = new Date()
    payload.paidAmount = order.total
    payload.tips = order.tips
    payload.totalPaid = order.total + order.tips
    payload.deliverySource = order.deliverySource
    return this.transactionService.transactionCreate(payload, session)
  }

  async summaryCloseOfDay(query: SummaryClosOfDayDto, restaurantId: string) {
    const transactions = await this.transactionService.getAll(
      query.buildQuery(restaurantId)
    )
    const paidTransaction = transactions.filter(
      (item) =>
        item.paymentStatus !== PaymentStatusEnum.VOID &&
        item.paymentMethod !== PaymentMethodEnum.DE_MINIMIS
    )
    const totalCollected = this.summaryTransactionItem(
      paidTransaction,
      'totalPaid'
    )
    const refundedTransaction = transactions.filter(
      (item) => item.paymentStatus === PaymentStatusEnum.REFUNDED
    )
    const paidOrderIds = paidTransaction.map((item) => item.orderId)
    paidOrderIds.forEach((item, index) => {
      if (paidOrderIds.indexOf(item) !== index) {
        paidTransaction.splice(index, 1)
      }
    })
    const summarySubtotal = this.summaryTransactionItem(
      paidTransaction,
      'subtotal'
    )
    const summaryDiscount = this.summaryTransactionItem(
      paidTransaction,
      'discount'
    )
    const summaryRefunded = this.summaryTransactionItem(
      refundedTransaction,
      'refundedAmount'
    )

    return {
      totalTransaction: transactions.length,
      totalCollected: totalCollected - summaryRefunded,
      netSales: summarySubtotal - summaryDiscount - summaryRefunded,
      summaryByPaymentMethod: [
        {
          label: 'Cash',
          amount: this.summaryTransactionItem(
            paidTransaction.filter(
              (item) =>
                item.paymentMethod === PaymentMethodEnum.CASH &&
                item.paymentStatus !== PaymentStatusEnum.REFUNDED
            ),
            'totalPaid'
          )
        },
        {
          label: 'Credit Card',
          amount: this.summaryTransactionItem(
            paidTransaction.filter(
              (item) =>
                item.paymentMethod === PaymentMethodEnum.CREDIT &&
                item.paymentStatus !== PaymentStatusEnum.REFUNDED
            ),
            'totalPaid'
          )
        },
        {
          label: 'Debit Card',
          amount: this.summaryTransactionItem(
            paidTransaction.filter(
              (item) =>
                item.paymentMethod === PaymentMethodEnum.DEBIT &&
                item.paymentStatus !== PaymentStatusEnum.REFUNDED
            ),
            'totalPaid'
          )
        },
        {
          label: 'Check',
          amount: this.summaryTransactionItem(
            paidTransaction.filter(
              (item) =>
                item.paymentMethod === PaymentMethodEnum.CHECK &&
                item.paymentStatus !== PaymentStatusEnum.REFUNDED
            ),
            'totalPaid'
          )
        },
        {
          label: 'Gift Card',
          amount: this.summaryTransactionItem(
            paidTransaction.filter(
              (item) =>
                item.paymentMethod === PaymentMethodEnum.GIFT_CARD &&
                item.paymentStatus !== PaymentStatusEnum.REFUNDED
            ),
            'totalPaid'
          )
        },
        {
          label: 'Delivery',
          amount: this.summaryTransactionItem(
            paidTransaction.filter(
              (item) =>
                item.paymentMethod === PaymentMethodEnum.DELIVERY &&
                item.paymentStatus !== PaymentStatusEnum.REFUNDED
            ),
            'totalPaid'
          )
        },
        {
          label: 'Others',
          amount: this.summaryTransactionItem(
            paidTransaction.filter(
              (item) =>
                item.paymentMethod === PaymentMethodEnum.OTHER &&
                item.paymentStatus !== PaymentStatusEnum.REFUNDED
            ),
            'totalPaid'
          )
        }
      ]
    }
  }

  async createOnlineTransaction(
    order: OrderOnlineDocument,
    payment: IPaymentMethodCardType,
    session: ClientSession
  ) {
    const payload: IOnlineTransaction = {}
    payload.txn = await this.genTransactionId(order.restaurant.id)
    payload.paymentStatus = PaymentStatusEnum.PAID
    payload.transactionStatus = TransactionStatusEnum.OPEN
    payload.restaurantId = order.restaurant.id
    payload.orderType = OrderTypeEnum.ONLINE
    payload.customer = order.customer
    payload.orderId = order.id
    payload.summaryItems = order.summaryItems
    payload.subtotal = order.subtotal
    payload.discount = order.discount
    payload.serviceCharge = order.serviceCharge
    payload.tax = order.tax
    payload.alcoholTax = order.alcoholTax
    payload.convenienceFee = order.convenienceFee
    payload.total = order.total
    payload.retRef = payment.retRef
    payload.batchId = Number(payment.batchId)
    payload.cardType = payment.cardType
    payload.paymentMethod = payment.paymentMethod
    payload.cardLastFour = payment.cardLastFour
    payload.paidBy = payment.paidBy
    payload.openDate = new Date()
    payload.paidAmount = order.total
    payload.tips = order.tips
    payload.totalPaid = order.total
    payload.response = payment.response
    return this.transactionService.transactionCreate(payload, session)
  }
}
