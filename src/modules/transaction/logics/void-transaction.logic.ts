import { Injectable } from '@nestjs/common'
import * as dayjs from 'dayjs'
import { ClientSession } from 'mongoose'
import { OrderItemsLogic } from 'src/modules/order/logics/order-items.logic'
import { OrderDocument } from 'src/modules/order/schemas/order.schema'
import { PaymentMethodEnum } from '../common/payment-method.enum'
import { PaymentStatusEnum } from '../common/payment-status.enum'
import { IVoidTransaction } from '../interfaces/void-transaction.interface'
import { TransactionCounterService } from '../services/transaction-counter.service'
import { TransactionService } from '../services/transaction.service'

@Injectable()
export class VoidTransactionLogic {
  constructor(
    private readonly transactionService: TransactionService,
    private readonly transactionCounterService: TransactionCounterService,
    private readonly orderItemsLogic: OrderItemsLogic
  ) {}

  private async genTransactionId(restaurantId: string): Promise<string> {
    const now = dayjs().format('YYYYMMDD')
    const countQueue = await this.transactionCounterService.getCounter(
      now,
      restaurantId
    )
    return `${countQueue.toString().padStart(3, '0')}`
  }

  async createVoidTransaction(order: OrderDocument, session: ClientSession) {
    const txn = await this.genTransactionId(order.restaurant.id)
    const payload: IVoidTransaction = {
      restaurantId: order.restaurant.id,
      txn,
      orderId: order.id,
      porderId: order.id,
      orderType: order.orderType,
      customer: order.customer,
      summaryItems: order.summaryItems,
      subtotal: order.subtotal,
      discount: order.discount,
      serviceCharge: order.serviceCharge,
      tax: order.tax,
      alcoholTax: order.alcoholTax,
      convenienceFee: order.convenienceFee,
      total: order.total,
      paymentMethod: PaymentMethodEnum.OTHER,
      paymentStatus: PaymentStatusEnum.VOID,
      openDate: new Date(),
      paidAmount: order.total,
      totalPaid: order.total,
      note: 'Void order before transaction',
      isVoidBeforeTransaction: true
    }

    return this.transactionService.transactionCreate(payload, session)
  }
}
