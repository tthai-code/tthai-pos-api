import { Injectable } from '@nestjs/common'
import { TransactionLogService } from '../services/log.transaction.service'

@Injectable()
export class TransactionLogLogic {
  constructor(private readonly transactionLogService: TransactionLogService) {}

  async createLogic(request, res, note?: any) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const requestIp = require('request-ip')

    const reqIp = request.clientIp
      ? request.clientIp
      : requestIp.getClientIp(request)
    const payload = {
      ip: reqIp,
      baseUrl: request.baseUrl,
      originalUrl: request.originalUrl,
      headers: request.headers,
      body: request.body,
      params: request.params,
      query: request.query,
      method: request.method,
      res,
      note
    }

    return await this.transactionLogService.create(payload)
  }
}
