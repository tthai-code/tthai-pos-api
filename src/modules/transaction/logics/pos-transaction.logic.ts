import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { PaymentMethodEnum } from '../common/payment-method.enum'
import { PaymentStatusEnum } from '../common/payment-status.enum'
import { TransactionStatusEnum } from '../common/transaction-status.enum'
import { CloseOfDayDto } from '../dto/close-of-day.dto'
import { TransactionPaginateDto } from '../dto/get-transaction.dto'
import { TransactionService } from '../services/transaction.service'

@Injectable()
export class POSTransactionLogic {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly transactionService: TransactionService,
    private readonly kitchenQueueService: KitchenQueueService
  ) {}

  private paymentMethodStringFormat(text: string) {
    switch (text) {
      case 'CASH':
        return 'Cash'
      case 'CREDIT':
        return 'Credit'
      case 'DEBIT':
        return 'Debit'
      case 'THIRD_PARTY':
        return 'Third Party'
      case 'GIFT_CARD':
        return 'Gift Card'
      case 'CHECK':
        return 'Check'
      case 'DELIVERY':
        return 'Delivery'
      case 'DE_MINIMIS':
        return 'Fringe Benefit'
      case 'OTHER':
        return 'Others'
      default:
        return text
    }
  }

  private summaryTransactionItem(items: any[], key: string): number {
    return (
      items
        .map((item) => item[key])
        .reduce((sum, current) => sum + current, 0) || 0
    )
  }

  async checkCloseOfDayStatus(restaurantId: string, payload: CloseOfDayDto) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const transactions = await this.transactionService.getAll({
      restaurantId,
      openDate: {
        $gte: payload.startDate,
        $lte: payload.endDate
      },
      status: StatusEnum.ACTIVE
    })
    const isClosed = transactions.every(
      (item) => item.transactionStatus === TransactionStatusEnum.CLOSED
    )

    return { isClosed }
  }

  async getPOSTransactionList(
    query: TransactionPaginateDto,
    restaurantId: string
  ) {
    const results = await this.transactionService.paginate(
      query.buildQuery(restaurantId),
      query
    )
    const transactions = await this.transactionService.getAll(
      query.buildQuery(restaurantId)
    )
    const summaryTotal = this.summaryTransactionItem(transactions, 'totalPaid')
    const paidTransaction = transactions.filter(
      (item) =>
        item.paymentStatus !== PaymentStatusEnum.VOID &&
        item.paymentMethod !== PaymentMethodEnum.DE_MINIMIS
    )
    const totalCollected = this.summaryTransactionItem(
      paidTransaction,
      'totalPaid'
    )
    const refundedTransaction = transactions.filter(
      (item) => item.paymentStatus === PaymentStatusEnum.REFUNDED
    )
    const paidOrderIds = paidTransaction.map((item) => item.orderId)
    paidOrderIds.forEach((item, index) => {
      if (paidOrderIds.indexOf(item) !== index) {
        paidTransaction.splice(index, 1)
      }
    })
    const summarySubtotal = this.summaryTransactionItem(
      paidTransaction,
      'subtotal'
    )
    const summaryDiscount = this.summaryTransactionItem(
      paidTransaction,
      'discount'
    )
    const summaryRefunded = this.summaryTransactionItem(
      refundedTransaction,
      'refundedAmount'
    )
    const { docs } = results
    const transformDocs = []
    for (const doc of docs) {
      const kitchenQueue = await this.kitchenQueueService.findOne(
        {
          orderId: doc.orderId
        },
        { ticketNo: 1 },
        { sort: { createdAt: -1 } }
      )
      const paymentMethod = this.paymentMethodStringFormat(doc.paymentMethod)
      transformDocs.push({
        ...doc?.toObject(),
        ticketNo: kitchenQueue?.ticketNo || null,
        paymentMethod
      })
    }
    return {
      ...results,
      docs: transformDocs,
      totalCollected: totalCollected - summaryRefunded,
      netSales: summarySubtotal - summaryDiscount - summaryRefunded,
      summaryTotal
    }
  }
}
