export enum TransactionStatusEnum {
  OPEN = 'OPEN',
  CLOSED = 'CLOSED'
}
