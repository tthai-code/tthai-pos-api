import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator'
import { ItemsDto } from 'src/modules/order/dto/create-order-pos.dto'

export class RefundTransactionDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<transaction-id>' })
  transactionId: string

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({ example: 1.01, description: 'use for partial refund' })
  amount: number

  @IsOptional()
  @Type(() => ItemsDto)
  @ValidateNested({ each: true })
  @ApiPropertyOptional({
    type: () => [ItemsDto],
    description: 'use for partial refund'
  })
  summaryItems: Array<ItemsDto>

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Some Reason' })
  readonly refundedReason: string
}
