import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { Type } from 'class-transformer'
import {
  ArrayNotEmpty,
  IsEnum,
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested
} from 'class-validator'
import { ItemsDto } from 'src/modules/order/dto/create-order-pos.dto'
import { OrderTypeEnum } from 'src/modules/order/enum/order-type.enum'
import { CardTypeEnum } from '../common/card-type.enum'
import { PaymentMethodEnum } from '../common/payment-method.enum'
import { SplitMethodEnum } from '../common/split-method.enum'

class CustomerField {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: null })
  id: string

  @IsString()
  @ApiProperty({ example: 'Guest' })
  firstName: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '' })
  lastName: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '' })
  tel: string
}
export class CreateTransactionPOSDto {
  public txn: string
  public paymentStatus: string
  public transactionStatus: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<restaurant-id>' })
  restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<order-id>' })
  orderId: string

  @IsString()
  @IsOptional()
  @ApiProperty({ example: '<payment-order-id>' })
  PorderId: string

  @IsEnum(OrderTypeEnum)
  @ApiProperty({
    enum: [OrderTypeEnum.DINE_IN, OrderTypeEnum.TO_GO],
    example: OrderTypeEnum.DINE_IN
  })
  orderType: string

  @Type(() => CustomerField)
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => CustomerField })
  customer: CustomerField

  @Type(() => ItemsDto)
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @ApiProperty({ type: () => [ItemsDto] })
  summaryItems: Array<ItemsDto>

  @IsNumber()
  @ApiProperty({ example: 100 })
  subtotal: number

  @IsNumber()
  @ApiProperty({ example: 0 })
  discount: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  serviceCharge: number

  @IsNumber()
  @ApiProperty({ example: 7 })
  tax: number

  @IsNumber()
  @ApiProperty({ example: 0 })
  alcoholTax: number

  @IsNumber()
  @ApiProperty({ example: 0 })
  convenienceFee: number

  @IsNumber()
  @ApiProperty({ example: 117 })
  total: number

  @IsEnum(SplitMethodEnum)
  @ApiProperty({
    enum: Object.values(SplitMethodEnum),
    example: SplitMethodEnum.NO_SPLIT
  })
  splitMethod: string

  @IsNumber()
  @ApiProperty({ example: 1 })
  splitBy: number

  @IsOptional()
  @Type(() => ItemsDto)
  @ValidateNested({ each: true })
  @ApiPropertyOptional({ type: () => [ItemsDto] })
  splitItems: Array<ItemsDto>

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({ example: 11.7 })
  totalSplitItems: number

  @IsOptional()
  @IsIn(Object.values(CardTypeEnum))
  @ApiPropertyOptional({
    enum: Object.values(CardTypeEnum),
    example: CardTypeEnum.MASTERCARD
  })
  cardType: string

  @IsEnum(PaymentMethodEnum)
  @ApiProperty({
    example: PaymentMethodEnum.CREDIT,
    enum: Object.values(PaymentMethodEnum)
  })
  paymentMethod: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: '4321' })
  cardLastFour: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'John Doe' })
  paidBy: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: '<retref>',
    description: 'retref from transaction Payment Gateway API'
  })
  retRef: string

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({
    example: 120,
    description: 'batchId from transaction Payment Gateway API'
  })
  batchId: number

  @IsString()
  @ApiProperty({ example: new Date().toISOString() })
  openDate: string

  @IsNumber()
  @ApiProperty({ example: 117 })
  paidAmount: number

  @IsNumber()
  @ApiProperty({ example: 10 })
  tips: number

  @IsNumber()
  @ApiProperty({ example: 127 })
  totalPaid: number

  @IsOptional()
  @ApiPropertyOptional({
    example: '',
    description: 'response from Payment Gateway API'
  })
  response: any

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'note' })
  note: string
}
