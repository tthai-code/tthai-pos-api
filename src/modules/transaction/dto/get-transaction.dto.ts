import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsNotEmpty, IsOptional, IsString } from 'class-validator'
import * as dayjs from 'dayjs'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'
import { OrderTypeEnum } from 'src/modules/order/enum/order-type.enum'
import { PaymentMethodEnum } from '../common/payment-method.enum'
import { PaymentStatusEnum } from '../common/payment-status.enum'

export class TransactionPaginateDto extends PaginateDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'createdAt' })
  readonly sortBy: string = 'createdAt'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'desc' })
  readonly sortOrder: string = 'desc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: '4321',
    description: 'use for search 4 last card number.'
  })
  readonly search: string = ''

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: PaymentMethodEnum.CREDIT,
    enum: Object.values(PaymentMethodEnum)
  })
  readonly paymentMethod: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: OrderTypeEnum.DINE_IN,
    enum: Object.values(OrderTypeEnum)
  })
  readonly orderType: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: PaymentStatusEnum.PAID,
    enum: Object.values(PaymentStatusEnum)
  })
  readonly paymentStatus: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly startDate: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly endDate: string

  public buildQuery(id: string) {
    const result = {
      $or: [
        {
          orderId: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          porderId: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          cardLastFour: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      openDate: {
        $gte: dayjs(this.startDate).toDate(),
        $lte: dayjs(this.endDate).toDate()
      },
      restaurantId: id,
      orderType: this.orderType,
      paymentMethod: this.paymentMethod,
      paymentStatus: this.paymentStatus,
      status: StatusEnum.ACTIVE
    }
    if (!this.paymentStatus) delete result.paymentStatus
    if (!this.paymentMethod) delete result.paymentMethod
    if (!this.orderType) delete result.orderType
    return result
  }
}
