import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator'

export class AddTipsDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<transaction-id>' })
  readonly transactionId: string

  @IsNumber()
  @ApiProperty({ example: 1.01 })
  readonly tips: number

  @IsOptional()
  @IsString()
  @ApiProperty({ example: '<base64-string>' })
  readonly signature: string
}
