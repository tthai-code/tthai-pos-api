import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class CloseOfDayDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  startDate: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  endDate: string
}
