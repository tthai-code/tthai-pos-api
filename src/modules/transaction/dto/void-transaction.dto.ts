import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class VoidTransactionDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '<transaction-id>' })
  transactionId: string
}
