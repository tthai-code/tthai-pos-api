import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'
import * as dayjs from 'dayjs'
import { StatusEnum } from 'src/common/enum/status.enum'

export class SummaryClosOfDayDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly startDate: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly endDate: string

  public buildQuery(id: string) {
    const result = {
      openDate: {
        $gte: dayjs(this.startDate).toDate(),
        $lte: dayjs(this.endDate).toDate()
      },
      restaurantId: id,
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
