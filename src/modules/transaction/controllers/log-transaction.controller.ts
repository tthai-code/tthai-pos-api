import { Controller, Get, Query } from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import { TransactionLogPaginateDto } from '../dto/log-transaction.dto'
import { TransactionLogService } from '../services/log.transaction.service'

@ApiTags('transaction-logs')
@Controller('v1/transaction-logs')
export class TransactionLogController {
  constructor(private readonly transactionLogService: TransactionLogService) {}

  @Get()
  @ApiOperation({ summary: 'Get Transaction Log List' })
  async getAccessLog(@Query() query: TransactionLogPaginateDto) {
    return await this.transactionLogService.paginate(query.buildQuery(), query)
  }
}
