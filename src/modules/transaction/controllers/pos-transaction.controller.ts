import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Request,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { StatusEnum } from 'src/common/enum/status.enum'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { AddTipsDto } from '../dto/add-tips.dto'
import { CloseOfDayDto } from '../dto/close-of-day.dto'
import { CreateTransactionPOSDto } from '../dto/create-transaction.dto'
import { TransactionPaginateDto } from '../dto/get-transaction.dto'
import { RefundTransactionDto } from '../dto/refund-transaction.dto'
import { SummaryClosOfDayDto } from '../dto/summary-close.dto'
import { VoidTransactionDto } from '../dto/void-transaction.dto'
import { CloseOfDayStatusResponse } from '../entity/pos-close-of-day-status.entity'
import { CreateTransactionResponse } from '../entity/pos-create-transaction.entity'
import { PaginatePOSTransactionResponse } from '../entity/pos-paginate-transaction.entity'
import { SummaryCloseOfDayResponse } from '../entity/summary-close.entity'
import { VoidTransactionResponse } from '../entity/void-transaction.entity'
import { POSTransactionLogic } from '../logics/pos-transaction.logic'
import { TransactionLogic } from '../logics/transaction.logic'
import { TransactionService } from '../services/transaction.service'

@ApiBearerAuth()
@ApiTags('pos/transaction')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/transaction')
export class POSTransactionController {
  constructor(
    private readonly transactionService: TransactionService,
    private readonly transactionLogic: TransactionLogic,
    private readonly posTransactionLogic: POSTransactionLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaginatePOSTransactionResponse })
  @ApiOperation({
    summary: 'Get Transaction List',
    description: 'use *bearer* `staff_token`'
  })
  async getTransactions(
    @Query() query: TransactionPaginateDto,
    @Request() request: any
  ) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    const transactions = await this.posTransactionLogic.getPOSTransactionList(
      query,
      restaurantId
    )
    await this.logLogic.createLogic(
      request,
      'Get POS Transaction List',
      transactions
    )
    return transactions
  }

  @Post()
  @ApiCreatedResponse({ type: () => CreateTransactionResponse })
  @ApiOperation({
    summary: 'Create POS Transaction',
    description: 'use *bearer* `staff_token`'
  })
  async createTransaction(
    @Body() payload: CreateTransactionPOSDto,
    @Request() request: any
  ) {
    const transaction = await this.transactionLogic.createTransactionPOS(
      payload
    )
    await this.logLogic.createLogic(
      request,
      'Create POS Transaction',
      transaction
    )
    return transaction
  }

  @Get(':transactionId')
  @ApiOkResponse({ type: () => CreateTransactionResponse })
  @ApiOperation({
    summary: 'Get Transaction By Transaction ID',
    description: 'use *bearer* `staff_token`'
  })
  async getTransactionById(@Param('transactionId') transactionId: string) {
    return await this.transactionService.findOne({
      _id: transactionId,
      status: StatusEnum.ACTIVE
    })
  }

  @Put('void')
  @ApiOkResponse({ type: () => VoidTransactionResponse })
  @ApiOperation({
    summary: 'Void Transaction By Transaction ID',
    description: 'use *bearer* `staff_token`'
  })
  async voidTransaction(@Body() payload: VoidTransactionDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.voidTransaction(
      payload.transactionId,
      restaurantId
    )
  }

  @Put('refund')
  @ApiOkResponse({ type: () => VoidTransactionResponse })
  @ApiOperation({
    summary: 'Refund Transaction By Transaction ID',
    description: 'use *bearer* `staff_token`'
  })
  async refundTransaction(@Body() payload: RefundTransactionDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.refundTransaction(payload, restaurantId)
  }

  @Post('close-status')
  @ApiCreatedResponse({ type: () => CloseOfDayStatusResponse })
  @ApiOperation({
    summary: 'Get Close of Day Status',
    description: 'use *bearer* `staff_token`'
  })
  async checkCloseOfDayStatus(@Body() payload: CloseOfDayDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.posTransactionLogic.checkCloseOfDayStatus(
      restaurantId,
      payload
    )
  }

  @Get('close-batch/summary')
  @ApiOkResponse({ type: () => SummaryCloseOfDayResponse })
  @ApiOperation({
    summary: 'Get Summary Transaction Before Close of Day',
    description: 'use *bearer* `staff_token`'
  })
  async summaryCloseOfDay(@Query() query: SummaryClosOfDayDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.summaryCloseOfDay(query, restaurantId)
  }

  @Put('close-batch')
  @ApiOkResponse({ type: () => VoidTransactionResponse })
  @ApiOperation({
    summary: 'Close Batch Payment/Run close of day',
    description: 'use *bearer* `staff_token`'
  })
  async closeBatch(@Body() payload: CloseOfDayDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.closeBatch(restaurantId, payload)
  }

  @Patch('/tips')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Tips Transaction By Transaction ID',
    description: 'use *bearer* `staff_token`'
  })
  async tipsTransaction(@Body() payload: AddTipsDto) {
    return await this.transactionLogic.addTipLogic(payload)
  }
}
