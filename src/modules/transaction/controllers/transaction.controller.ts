import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Put,
  Query,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { AddTipsDto } from '../dto/add-tips.dto'
import { CloseOfDayDto } from '../dto/close-of-day.dto'
import { TransactionPaginateDto } from '../dto/get-transaction.dto'
import { RefundTransactionDto } from '../dto/refund-transaction.dto'
import { SummaryClosOfDayDto } from '../dto/summary-close.dto'
import { VoidTransactionDto } from '../dto/void-transaction.dto'
import { PaginateTransactionResponse } from '../entity/paginate-transaction.entity'
import { CreateTransactionResponse } from '../entity/pos-create-transaction.entity'
import { WebBaseSummaryCloseOfDayResponse } from '../entity/summary-close.entity'
import { VoidTransactionResponse } from '../entity/void-transaction.entity'
import { TransactionLogic } from '../logics/transaction.logic'
import { TransactionService } from '../services/transaction.service'

@ApiBearerAuth()
@ApiTags('transaction')
@UseGuards(AdminAuthGuard)
@Controller('v1/transaction')
export class TransactionController {
  constructor(
    private readonly transactionService: TransactionService,
    private readonly transactionLogic: TransactionLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => PaginateTransactionResponse })
  @ApiOperation({
    summary: 'Get Transaction List',
    description: 'use *bearer* `restaurant_token`'
  })
  async getTransactions(@Query() query: TransactionPaginateDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.getTransactionList(query, restaurantId)
  }

  @Get(':transactionId')
  @ApiOkResponse({ type: () => CreateTransactionResponse })
  @ApiOperation({
    summary: 'Get Transaction By Transaction ID',
    description: 'use *bearer* `restaurant_token`'
  })
  async getTransactionById(@Param('transactionId') transactionId: string) {
    return await this.transactionService.findOne({
      _id: transactionId,
      status: StatusEnum.ACTIVE
    })
  }

  @Put('void')
  @ApiOkResponse({ type: () => VoidTransactionResponse })
  @ApiOperation({
    summary: 'Void Transaction By Transaction ID',
    description: 'use *bearer* `restaurant_token`'
  })
  async voidTransaction(@Body() payload: VoidTransactionDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.voidTransaction(
      payload.transactionId,
      restaurantId
    )
  }

  @Put('refund')
  @ApiOkResponse({ type: () => VoidTransactionResponse })
  @ApiOperation({
    summary: 'Refund Transaction By Transaction ID',
    description: 'use *bearer* `restaurant_token`'
  })
  async refundTransaction(@Body() payload: RefundTransactionDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.refundTransaction(payload, restaurantId)
  }

  @Get('close-batch/summary')
  @ApiOkResponse({ type: () => WebBaseSummaryCloseOfDayResponse })
  @ApiOperation({
    summary: 'Get Summary Transaction Before Close of Day',
    description: 'use *bearer* `restaurant_token`'
  })
  async summaryCloseOfDay(@Query() query: SummaryClosOfDayDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.summaryCloseOfDay(query, restaurantId)
  }

  @Put('close-batch')
  @ApiOkResponse({ type: () => VoidTransactionResponse })
  @ApiOperation({
    summary: 'Close Batch Payment/Run close of day',
    description: 'use *bearer* `restaurant_token`'
  })
  async closeBatch(@Body() payload: CloseOfDayDto) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.transactionLogic.closeBatch(restaurantId, payload)
  }

  @Patch('tips')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Tips Transaction By Transaction ID',
    description: 'use *bearer* `restaurant_token`'
  })
  async tipsTransaction(@Body() payload: AddTipsDto) {
    return await this.transactionLogic.addTipLogic(payload)
  }
}
