import { Controller, Get, Query, Request, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { GetSaleReportPOS } from '../dto/get-sale-report.dto'
import { POSReportResponse } from '../entity/pos-report.entity'
import { POSReportLogic } from '../logic/pos-report.logic'

@ApiBearerAuth()
@ApiTags('pos/report')
@UseGuards(StaffAuthGuard)
@Controller('v1/pos/report')
export class POSReportController {
  constructor(
    private readonly posReportLogic: POSReportLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Get('sale')
  @ApiOkResponse({ type: () => POSReportResponse })
  @ApiOperation({
    summary: 'Get Sale Report for POS',
    description: 'use bearer `staff_token`'
  })
  async getSaleReport(
    @Query() query: GetSaleReportPOS,
    @Request() request: any
  ) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    await this.logLogic.createLogic(request, 'Get Sale Report for POS')
    return await this.posReportLogic.getSaleReport(
      restaurantId,
      query.startDate,
      query.endDate,
      query.shiftStartAt,
      query.shiftEndAt
    )
  }
}
