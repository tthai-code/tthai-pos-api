import { Controller, Get, Query, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuperAdminAuthGuard } from 'src/modules/auth/guards/super-admin-auth.guard'
import { AdminReportPaginateDto } from '../dto/admin-report.dto'
import { AdminReportPaginateResponse } from '../entity/admin-report.entity'
import { AdminReportLogic } from '../logic/admin-report.logic'

@ApiBearerAuth()
@ApiTags('admin/report')
@UseGuards(SuperAdminAuthGuard)
@Controller('v1/admin/report')
export class AdminReportController {
  constructor(private readonly adminReportLogic: AdminReportLogic) {}

  @Get('sale')
  @ApiOkResponse({ type: () => AdminReportPaginateResponse })
  @ApiOperation({
    summary: 'Get Report for Admin',
    description: 'use bearer `admin_token`'
  })
  async getReportForAdmin(@Query() query: AdminReportPaginateDto) {
    return await this.adminReportLogic.getReportForAdmin(query)
  }
}
