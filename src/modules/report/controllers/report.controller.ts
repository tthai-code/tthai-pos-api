import { Controller, Get, Query, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { GetSaleReport } from '../dto/get-sale-report.dto'
import { ReportResponse } from '../entity/report.entity'
import { ReportLogic } from '../logic/report.logic'

@ApiBearerAuth()
@ApiTags('report')
@UseGuards(AdminAuthGuard)
@Controller('v1/report')
export class ReportController {
  constructor(private readonly reportLogic: ReportLogic) {}

  @Get('sale')
  @ApiOkResponse({ type: () => ReportResponse })
  @ApiOperation({
    summary: 'Get Sales Report',
    description: 'use bearer `restaurant_token`'
  })
  async getSalesReport(@Query() query: GetSaleReport) {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.reportLogic.getSaleReport(restaurantId, query)
  }
}
