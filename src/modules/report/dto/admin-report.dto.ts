import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'
import * as dayjs from 'dayjs'
import { PaginateDto } from 'src/common/dto/paginate.dto'
import { StatusEnum } from 'src/common/enum/status.enum'
import { PaymentStatusEnum } from 'src/modules/transaction/common/payment-status.enum'

export class AdminReportPaginateDto extends PaginateDto {
  @IsString()
  readonly sortBy: string = 'legalBusinessName'

  @IsString()
  readonly sortOrder: string = 'asc'

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Tim' })
  readonly search: string = ''

  @IsString()
  @ApiProperty({ example: new Date().toISOString() })
  readonly startDate: string

  @IsString()
  @ApiProperty({ example: new Date().toISOString() })
  readonly endDate: string

  public restaurantBuildQuery() {
    const result = {
      $or: [
        {
          legalBusinessName: {
            $regex: this.search,
            $options: 'i'
          }
        },
        {
          dba: {
            $regex: this.search,
            $options: 'i'
          }
        }
      ],
      status: StatusEnum.ACTIVE
    }
    if (!this.search) delete result['$or']
    return result
  }

  public buildQuery(restaurantIds: any[]) {
    const result = {
      restaurantId: { $in: restaurantIds },
      paymentStatus: PaymentStatusEnum.PAID,
      closeDate: {
        $gte: dayjs(this.startDate)
          .set('second', 0)
          .set('millisecond', 0)
          .toDate(),
        $lte: dayjs(this.endDate)
          .set('second', 59)
          .set('millisecond', 999)
          .toDate()
      },
      status: StatusEnum.ACTIVE
    }
    return result
  }
}
