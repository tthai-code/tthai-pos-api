import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsIn, IsNotEmpty, IsOptional, IsString } from 'class-validator'
import { ReportDateEnum } from '../common/report-date.enum'
import { Type } from 'class-transformer'

export class GetSaleReportPOS {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly startDate: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: new Date().toISOString() })
  readonly endDate: string

  @ApiPropertyOptional({ example: '12:00', default: '' })
  @IsString()
  @IsOptional()
  readonly shiftStartAt: string | null | undefined = ''

  @ApiPropertyOptional({ example: '12:00', default: '' })
  @IsString()
  @IsOptional()
  readonly shiftEndAt: string | null | undefined
}

class ShiftFieldsDto {
  @ApiProperty({ example: '12:00' })
  startAt: string

  @ApiProperty({ example: '12:00' })
  endAt: string
}

export class GetSaleReport extends GetSaleReportPOS {
  @IsIn(Object.values(ReportDateEnum))
  @IsString()
  @ApiProperty({
    example: ReportDateEnum.TODAY,
    enum: Object.values(ReportDateEnum),
    description: 'using for transform data to display graph.'
  })
  readonly reportType: string
}
