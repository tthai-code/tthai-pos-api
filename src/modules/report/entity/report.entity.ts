import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class SummaryObject {
  @ApiProperty()
  gross: number
  @ApiProperty()
  discount: number
  @ApiProperty()
  refund: number
  @ApiProperty()
  net: number
  @ApiProperty()
  tips: number
  @ApiProperty()
  service_charges: number
  @ApiProperty()
  convenience_fee: number
  @ApiProperty()
  sale_tax: number
  @ApiProperty()
  alcohol_tax: number
  @ApiProperty()
  total: number
}

class NameOrderObject {
  @ApiProperty()
  name: string
  @ApiProperty()
  orders: number
  @ApiProperty()
  amount: number
}

class SaleTipTaxObject {
  @ApiProperty()
  sales: number
  @ApiProperty()
  tax: number
  @ApiProperty()
  tips: number
}

class PayoutCardObject extends SaleTipTaxObject {
  @ApiProperty()
  type: string
  @ApiProperty()
  amount: number
}

class ThirdPartyReportObject extends SaleTipTaxObject {
  @ApiProperty()
  name: string
  @ApiProperty()
  orders: number
}

class ReportObject {
  @ApiProperty({ type: SummaryObject })
  summary: SummaryObject
  @ApiProperty({ type: [NameOrderObject] })
  transaction_type: NameOrderObject[]
  @ApiProperty({ type: [NameOrderObject] })
  ordering_type: NameOrderObject[]
  @ApiProperty({ type: [PayoutCardObject] })
  payout_card_settlement: PayoutCardObject[]
  @ApiProperty({ type: [ThirdPartyReportObject] })
  third_party_report: ThirdPartyReportObject[]
  @ApiProperty()
  datasets: number[]
}

export class ReportResponse extends ResponseDto<ReportObject> {
  @ApiProperty({
    type: ReportObject,
    example: {
      summary: {
        gross: 240,
        discount: 0,
        refund: 0,
        net: 240,
        tips: 0,
        service_charges: 0,
        convenience_fee: 0,
        sale_tax: 16.799999999999997,
        alcohol_tax: 24,
        total: 280.8
      },
      transaction_type: [
        {
          name: 'Cash',
          orders: 2,
          amount: 187.2
        },
        {
          name: 'Credit Card',
          orders: 1,
          amount: 93.6
        },
        {
          name: 'Debit Card',
          orders: 0,
          amount: 0
        }
      ],
      ordering_type: [
        {
          name: 'Dine In',
          orders: 0,
          amount: 0
        },
        {
          name: 'To Go',
          orders: 3,
          amount: 0
        },
        {
          name: 'Online',
          orders: 0,
          amount: 0
        },
        {
          name: 'TThai App',
          orders: 0,
          amount: 0
        }
      ],
      payout_card_settlement: [
        {
          type: 'Credit',
          sales: 0,
          tips: 0,
          tax: 0,
          amount: 0
        },
        {
          type: 'Debit',
          sales: 0,
          tips: 0,
          tax: 0,
          amount: 0
        }
      ],
      third_party_report: [
        {
          name: 'DoorDash',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        },
        {
          name: 'UberEats',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        },
        {
          name: 'GrubHub',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        },
        {
          name: 'GloriaFood',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        }
      ],
      datasets: [0, 0, 0, 240, 0, 0, 0]
    }
  })
  data: ReportObject
}
