import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class LabelObject {
  @ApiProperty()
  label: string
}

class LabelAmountObject extends LabelObject {
  @ApiProperty()
  amount: number
}

class LabelAmountOrderObject extends LabelAmountObject {
  @ApiProperty()
  orders: number
}

class SummarySalesObject extends LabelObject {
  @ApiProperty()
  sales: number
  @ApiProperty()
  refunds: number
  @ApiProperty()
  net: number
}

class SummaryThirdPartyObject extends LabelObject {
  @ApiProperty()
  orders: number
  @ApiProperty()
  sales: number
  @ApiProperty()
  tax: number
  @ApiProperty()
  tips: number
}

class POSReportObject {
  @ApiProperty({ type: [SummarySalesObject] })
  summary_sales: SummarySalesObject[]
  @ApiProperty({ type: [LabelAmountOrderObject] })
  summary_transaction_type: LabelAmountOrderObject[]
  @ApiProperty({ type: [LabelAmountOrderObject] })
  summary_order_type: LabelAmountOrderObject[]
  @ApiProperty({ type: [LabelAmountObject] })
  summary_payout: LabelAmountObject[]
  @ApiProperty({ type: [SummaryThirdPartyObject] })
  summary_third_party: SummaryThirdPartyObject[]
  @ApiProperty({ type: [LabelAmountObject] })
  summary_void_order: LabelAmountObject[]
  @ApiProperty({ type: [LabelAmountObject] })
  summary_de_minimis: LabelAmountObject[]
}

export class POSReportResponse extends ResponseDto<POSReportObject> {
  @ApiProperty({
    type: POSReportObject,
    example: {
      summary_sales: [
        {
          label: 'Gross Sales',
          sales: 30,
          refunds: 0,
          net: 30
        },
        {
          label: 'Discount',
          sales: 15,
          refunds: 0,
          net: 15
        },
        {
          label: 'Net Sales',
          sales: 15,
          refunds: 0,
          net: 15
        },
        {
          label: 'Tips',
          sales: 0,
          refunds: 0,
          net: 0
        },
        {
          label: 'Service Charges',
          sales: 3,
          refunds: 0,
          net: 3
        },
        {
          label: 'Convenience Fee',
          sales: 0,
          refunds: 0,
          net: 0
        },
        {
          label: 'Sales Tax',
          sales: 3,
          refunds: 0,
          net: 3
        },
        {
          label: 'Alcohol Tax',
          sales: 0,
          refunds: 0,
          net: 0
        },
        {
          label: 'Total',
          sales: 21,
          refunds: 0,
          net: 21
        }
      ],
      summary_transaction_type: [
        {
          label: 'Cash',
          orders: 1,
          amount: 21
        },
        {
          label: 'Credit Card',
          orders: 0,
          amount: 0
        },
        {
          label: 'Debit Card',
          orders: 0,
          amount: 0
        },
        {
          label: 'Check',
          orders: 0,
          amount: 0
        },
        {
          label: 'Gift Card',
          orders: 0,
          amount: 0
        },
        {
          label: 'Delivery',
          orders: 0,
          amount: 0
        },
        {
          label: 'De Minimus',
          orders: 1,
          amount: 11
        },
        {
          label: 'Other',
          orders: 0,
          amount: 0
        }
      ],
      summary_order_type: [
        {
          label: 'Dine In',
          orders: 0,
          amount: 0
        },
        {
          label: 'To Go',
          orders: 1,
          amount: 21
        },
        {
          label: 'Online',
          orders: 0,
          amount: 0
        },
        {
          label: 'EatsZaab',
          orders: 0,
          amount: 0
        }
      ],
      summary_payout: [
        {
          label: 'Cash',
          amount: 21
        },
        {
          label: 'Credit/Debit',
          amount: 0
        }
      ],
      summary_third_party: [
        {
          label: 'DoorDash',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        },
        {
          label: 'UberEats',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        },
        {
          label: 'GrubHub',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        },
        {
          label: 'GloriaFood',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        },
        {
          label: 'Total',
          orders: 0,
          sales: 0,
          tax: 0,
          tips: 0
        }
      ],
      summary_void_order: [
        {
          label: 'Orders',
          amount: 1
        },
        {
          label: 'Amount',
          amount: 10
        },
        {
          label: 'Sales Tax',
          amount: 1
        },
        {
          label: 'Total',
          amount: 11
        }
      ],
      summary_de_minimis: [
        {
          label: 'Orders',
          amount: 1
        },
        {
          label: 'Amount',
          amount: 10
        },
        {
          label: 'Sales Tax',
          amount: 1
        },
        {
          label: 'Total',
          amount: 11
        }
      ]
    }
  })
  data: POSReportObject
}
