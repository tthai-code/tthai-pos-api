import { ApiProperty } from '@nestjs/swagger'
import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy'
import { ResponseDto } from 'src/common/entity/response.entity'

class AdminReportObject {
  restaurant_id: string
  restaurant_name: string
  totalOrders: number
  grossSales: number
  net: number
}

class AdminReportPaginateObject extends PaginateResponseDto<
  AdminReportObject[]
> {
  @ApiProperty({
    type: [AdminReportObject],
    example: [
      {
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        total_orders: 3,
        gross_sales: 162.99,
        net: 190.31,
        restaurant_name: 'Holy Beef'
      },
      {
        restaurant_id: '630eff5751c2eac55f52662c',
        total_orders: 0,
        gross_sales: 0,
        net: 0,
        restaurant_name: 'May Thai Kitchen LLC'
      },
      {
        restaurant_id: '63433e8145728de99075847a',
        total_orders: 0,
        gross_sales: 0,
        net: 0,
        restaurant_name: 'New Jeans'
      }
    ]
  })
  results: AdminReportObject[]
}

export class AdminReportPaginateResponse extends ResponseDto<AdminReportPaginateObject> {
  @ApiProperty({ type: AdminReportPaginateObject })
  data: AdminReportPaginateObject
}
