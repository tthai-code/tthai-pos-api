import { Injectable } from '@nestjs/common'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { TransactionService } from 'src/modules/transaction/services/transaction.service'
import { AdminReportPaginateDto } from '../dto/admin-report.dto'

@Injectable()
export class AdminReportLogic {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly transactionService: TransactionService
  ) {}

  private numberToFixed(number: number, digit: number): number {
    return Number(number.toFixed(digit))
  }

  private summaryTransactionItem(items: any[], key: string): number {
    return (
      items
        .map((item) => item[key])
        .reduce((sum, current) => sum + current, 0) || 0
    )
  }

  private groupBy(array: any[], key: string): any[] {
    const groupByObject = array.reduce(
      (r, v, i, a, k = v[key]) => ((r[k] || (r[k] = [])).push(v), r),
      {}
    )
    const transform = Object.entries(groupByObject).map((e) => ({
      restaurantId: e[0],
      data: e[1]
    }))
    return transform
  }

  private groupBySummary(array: any[], key: string, restaurants: any[]): any[] {
    const groupByObject = array.reduce(
      (r, v, i, a, k = v[key]) => ((r[k] || (r[k] = [])).push(v), r),
      {}
    )
    const result = []
    const transform = Object.entries(groupByObject).map((e: any[]) => ({
      restaurantId: e[0],
      totalOrders: e[1]?.length,
      grossSales: this.numberToFixed(
        this.summaryTransactionItem(e[1], 'subtotal'),
        2
      ),
      net: this.numberToFixed(this.summaryTransactionItem(e[1], 'total'), 2)
    }))
    for (const restaurant of restaurants) {
      const index = transform.findIndex(
        (item) => item.restaurantId === restaurant.id
      )
      if (index !== -1) {
        result.push({
          ...transform[index],
          restaurantName: restaurant.dba || restaurant.legalBusinessName
        })
      } else {
        result.push({
          restaurantId: restaurant.id,
          totalOrders: 0,
          grossSales: 0,
          net: 0,
          restaurantName: restaurant.dba || restaurant.legalBusinessName
        })
      }
    }
    return result
  }

  async getReportForAdmin(query: AdminReportPaginateDto) {
    const restaurants = await this.restaurantService.paginate(
      query.restaurantBuildQuery(),
      query
    )
    const restaurantIds = restaurants.docs.map((item) => item.id)
    const transactions = await this.transactionService.getAll(
      query.buildQuery(restaurantIds)
    )
    const orders = []
    for (const transaction of transactions) {
      const index = orders.findIndex(
        (item) => item.orderId === transaction.orderId
      )
      if (index === -1) orders.push(transaction)
    }

    const groupByRestaurant = this.groupBySummary(
      orders,
      'restaurantId',
      restaurants.docs
    )

    return { ...restaurants, docs: groupByRestaurant }
  }
}
