import { Injectable, NotFoundException } from '@nestjs/common'
import * as dayjs from 'dayjs'
import * as utc from 'dayjs/plugin/utc'
import * as timezone from 'dayjs/plugin/timezone'
import { StatusEnum } from 'src/common/enum/status.enum'
import { ProviderEnum } from 'src/modules/kitchen-hub/common/provider.enum'
import { OrderStateEnum } from 'src/modules/order/enum/order-state.enum'
import { OrderTypeEnum } from 'src/modules/order/enum/order-type.enum'
import { OrderOnlineService } from 'src/modules/order/services/order-online.service'
import { OrderService } from 'src/modules/order/services/order.service'
import { ThirdPartyOrderService } from 'src/modules/order/services/third-party-order.service'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { PaymentMethodEnum } from 'src/modules/transaction/common/payment-method.enum'
import { TransactionService } from 'src/modules/transaction/services/transaction.service'
import { ReportDateEnum } from '../common/report-date.enum'
import { GetSaleReport } from '../dto/get-sale-report.dto'
import { PaymentStatusEnum } from 'src/modules/transaction/common/payment-status.enum'
import { Types as MongooseType } from 'mongoose'

dayjs.extend(utc)
dayjs.extend(timezone)

@Injectable()
export class ReportLogic {
  constructor(
    private readonly orderService: OrderService,
    private readonly thirdPartyOrderService: ThirdPartyOrderService,
    private readonly transactionService: TransactionService,
    private readonly restaurantService: RestaurantService,
    private readonly onlineOrderService: OrderOnlineService
  ) {}

  private getDatesInRange(d1, d2) {
    const startDate = new Date(d1)
    const endDate = new Date(d2)
    const date = new Date(startDate.getTime())

    const dates = []
    while (date <= endDate) {
      dates.push({
        label: dayjs(date).format('YYYY-MM-DD'),
        sales: 0
      })
      date.setDate(date.getDate() + 1)
    }

    return dates
  }

  private summaryTransactionItem(items: any[], key: string): number {
    return (
      items
        .map((item) => item[key])
        .reduce((sum, current) => sum + current, 0) || 0
    )
  }

  private groupByDateAndSummarySubtotal(
    orders: any[],
    format: string,
    timeZone: string
  ) {
    const grouped = orders.reduce(
      (r, v, i, a, k = dayjs(v['orderDate']).tz(timeZone).format(format)) => (
        (r[k] || (r[k] = [])).push(v.subtotal), r
      ),
      {}
    )
    const graph = []
    Object.keys(grouped).map((key) => {
      graph.push({
        label: key,
        sales: grouped[key].reduce((partialSum, a) => partialSum + a, 0)
      })
    })
    const dataSorted = graph.sort((a, b) => a.label - b.label)
    return dataSorted
  }

  private todayData(orders: any[], timeZone: string) {
    const datasets = []
    for (let i = 0; i < 24; i++) {
      datasets.push(0)
    }
    const groupData = this.groupByDateAndSummarySubtotal(orders, 'HH', timeZone)
    groupData.forEach((item) => {
      datasets[Number(item.label)] = item.sales
    })
    return datasets
  }

  private weeklyAndMonthlyAndRangeData(
    orders: any[],
    startDate: string,
    endDate: string,
    timeZone: string
  ) {
    const datasets = this.getDatesInRange(startDate, endDate)
    const groupData = this.groupByDateAndSummarySubtotal(
      orders,
      'YYYY-MM-DD',
      timeZone
    )
    groupData.forEach((item) => {
      const index = datasets.findIndex((data) => data.label === item.label)
      if (index !== -1) {
        datasets[index].sales = item.sales
      }
    })
    const results = datasets.map((item) => item.sales)
    return results
  }

  private yearlyData(orders: any[], timeZone: string) {
    const datasets = []
    for (let i = 0; i < 12; i++) {
      datasets.push(0)
    }
    const groupData = this.groupByDateAndSummarySubtotal(orders, 'MM', timeZone)
    groupData.forEach((item) => {
      datasets[Number(item.label) - 1] = item.sales
    })
    return datasets
  }

  private generateGraphData(
    reportType: string,
    orders: any[],
    startDate: string,
    endDate: string,
    timeZone: string
  ) {
    let datasets = []
    if (reportType === ReportDateEnum.TODAY) {
      datasets = this.todayData(orders, timeZone)
    } else if (
      reportType === ReportDateEnum.WEEKLY ||
      reportType === ReportDateEnum.MONTHLY
    ) {
      datasets = this.weeklyAndMonthlyAndRangeData(
        orders,
        startDate,
        endDate,
        timeZone
      )
    } else if (reportType === ReportDateEnum.YEARLY) {
      datasets = this.yearlyData(orders, timeZone)
    } else if (reportType === ReportDateEnum.RANGE) {
      datasets = this.weeklyAndMonthlyAndRangeData(
        orders,
        startDate,
        endDate,
        timeZone
      )
    }
    return datasets
  }

  private mergeOrderData(
    orderData: any[],
    onlineOrderData: any[],
    tpOrderData: any[]
  ) {
    return [...orderData, ...onlineOrderData, ...tpOrderData]
  }

  async getSaleReport(restaurantId: string, queryParams: GetSaleReport) {
    const { startDate, endDate, reportType, shiftStartAt, shiftEndAt } =
      queryParams
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const aggregatePipeline: any[] = [
      {
        $match: {
          restaurantId: new MongooseType.ObjectId(restaurantId),
          openDate: {
            $gte: dayjs(startDate).toDate(),
            $lte: dayjs(endDate).toDate()
          },
          isVoidBeforeTransaction: false,
          status: StatusEnum.ACTIVE
        }
      }
    ]
    if (shiftStartAt && shiftEndAt) {
      aggregatePipeline.push({
        $addFields: {
          minutes: {
            $add: [
              { $multiply: [{ $hour: '$openDate' }, 60] },
              { $minute: '$openDate' }
            ]
          }
        }
      })
      const startTime =
        Number(shiftStartAt.split(':')[0]) * 60 +
        Number(shiftStartAt.split(':')[1])
      const endTime =
        Number(shiftEndAt.split(':')[0]) * 60 + Number(shiftEndAt.split(':')[1])
      if (endTime < startTime) {
        aggregatePipeline.push({
          $match: {
            $or: [
              {
                minutes: {
                  $gte: startTime
                }
              },
              {
                minutes: {
                  $lte: endTime
                }
              }
            ]
          }
        })
      } else {
        aggregatePipeline.push({
          $match: {
            minutes: {
              $gte: startTime,
              $lte: endTime
            }
          }
        })
      }
    }

    const rawTransactionData = await this.transactionService.aggregate(
      aggregatePipeline
    )

    const transactionData = rawTransactionData.filter(
      (item) =>
        item.paymentMethod !== PaymentMethodEnum.DE_MINIMIS &&
        item.paymentStatus !== PaymentStatusEnum.VOID
    )

    const deMinimisTransactionData = rawTransactionData.filter(
      (item) =>
        item.paymentMethod === PaymentMethodEnum.DE_MINIMIS &&
        item.paymentStatus !== PaymentStatusEnum.VOID
    )

    const refundedTransactionData = rawTransactionData.filter(
      (item) => item.paymentStatus === PaymentStatusEnum.REFUNDED
    )

    const rawOrderIds = rawTransactionData.map((item) => item.orderId)
    const orderIds = transactionData.map((item) => item.orderId)
    const deMinimisOrderIds = deMinimisTransactionData.map(
      (item) => item.orderId
    )

    const rawOrderData = await this.orderService.getAll({
      _id: { $in: rawOrderIds }
    })
    const rawTpOrderData = await this.thirdPartyOrderService.getAll({
      _id: { $in: rawOrderIds }
    })
    const rawOnlineOrderData = await this.onlineOrderService.getAll({
      _id: { $in: rawOrderIds }
    })

    const orderData = rawOrderData.filter((item) => orderIds.includes(item.id))
    const tpOrderData = rawTpOrderData.filter((item) =>
      orderIds.includes(item.id)
    )
    const onlineOrderData = rawOnlineOrderData.filter((item) =>
      orderIds.includes(item.id)
    )
    const deMinimisOrder = rawOrderData.filter((item) =>
      deMinimisOrderIds.includes(item.id)
    )

    const voidOrderData = rawOrderData.filter(
      (item) => item.orderStatus === OrderStateEnum.VOID
    )
    const voidTpOrderData = rawTpOrderData.filter(
      (item) => item.orderStatus === OrderStateEnum.VOID
    )
    const voidOnlineOrderData = rawOnlineOrderData.filter(
      (item) => item.orderStatus === OrderStateEnum.VOID
    )
    const voidOrder = [
      ...voidOrderData,
      ...voidTpOrderData,
      ...voidOnlineOrderData
    ]

    const summaryGrossSale =
      this.summaryTransactionItem(orderData, 'subtotal') +
      this.summaryTransactionItem(tpOrderData, 'subtotal') +
      this.summaryTransactionItem(onlineOrderData, 'subtotal')
    const summaryRefund = this.summaryTransactionItem(
      refundedTransactionData,
      'refundedAmount'
    )
    const summaryDiscount =
      this.summaryTransactionItem(orderData, 'discount') +
      this.summaryTransactionItem(tpOrderData, 'discount') +
      this.summaryTransactionItem(onlineOrderData, 'discount')
    const summaryNet = summaryGrossSale - summaryDiscount - summaryRefund
    const summaryTips = this.summaryTransactionItem(transactionData, 'tips')
    const summaryServiceCharges =
      this.summaryTransactionItem(orderData, 'serviceCharge') +
      this.summaryTransactionItem(tpOrderData, 'serviceCharge') +
      this.summaryTransactionItem(onlineOrderData, 'serviceCharge')
    const summaryConvenienceFee =
      this.summaryTransactionItem(orderData, 'convenienceFee') +
      this.summaryTransactionItem(tpOrderData, 'convenienceFee') +
      this.summaryTransactionItem(onlineOrderData, 'convenienceFee')
    const summaryTax =
      this.summaryTransactionItem(orderData, 'tax') +
      this.summaryTransactionItem(tpOrderData, 'tax') +
      this.summaryTransactionItem(onlineOrderData, 'tax')
    const summaryAlcoholTax =
      this.summaryTransactionItem(orderData, 'alcoholTax') +
      this.summaryTransactionItem(tpOrderData, 'alcoholTax') +
      this.summaryTransactionItem(onlineOrderData, 'alcoholTax')

    const summarySales = {
      gross: summaryGrossSale,
      discount: summaryDiscount,
      refund: summaryRefund,
      net: summaryNet,
      tips: summaryTips,
      serviceCharges: summaryServiceCharges,
      convenienceFee: summaryConvenienceFee,
      saleTax: summaryTax,
      alcoholTax: summaryAlcoholTax,
      total:
        summaryNet +
        summaryTips +
        summaryServiceCharges +
        summaryConvenienceFee +
        summaryTax +
        summaryAlcoholTax
    }

    const transactionTypeCash = transactionData.filter(
      (item) => item.paymentMethod === PaymentMethodEnum.CASH
    )
    const transactionTypeCredit = transactionData.filter(
      (item) => item.paymentMethod === PaymentMethodEnum.CREDIT
    )
    const transactionTypeDebit = transactionData.filter(
      (item) => item.paymentMethod === PaymentMethodEnum.DEBIT
    )
    const transactionTypeCheck = transactionData.filter(
      (item) => item.paymentMethod === PaymentMethodEnum.CHECK
    )
    const transactionTypeGiftCard = transactionData.filter(
      (item) => item.paymentMethod === PaymentMethodEnum.GIFT_CARD
    )
    const transactionTypeDelivery = transactionData.filter(
      (item) => item.paymentMethod === PaymentMethodEnum.DELIVERY
    )

    const summaryCash = this.summaryTransactionItem(
      transactionTypeCash,
      'totalPaid'
    )
    const summaryCredit = this.summaryTransactionItem(
      transactionTypeCredit,
      'totalPaid'
    )
    const summaryDebit = this.summaryTransactionItem(
      transactionTypeDebit,
      'totalPaid'
    )
    const summaryCheck = this.summaryTransactionItem(
      transactionTypeCheck,
      'totalPaid'
    )
    const summaryGiftCard = this.summaryTransactionItem(
      transactionTypeGiftCard,
      'totalPaid'
    )
    const summaryDelivery = this.summaryTransactionItem(
      transactionTypeDelivery,
      'totalPaid'
    )

    const summaryTransactionType = [
      {
        name: 'Cash',
        orders: transactionTypeCash.length,
        amount: summaryCash
      },
      {
        name: 'Credit Card',
        orders: transactionTypeCredit.length,
        amount: summaryCredit
      },
      {
        name: 'Debit Card',
        orders: transactionTypeDebit.length,
        amount: summaryDebit
      },
      {
        name: 'Check',
        orders: transactionTypeCheck.length,
        amount: summaryCheck
      },
      {
        name: 'Gift Card',
        orders: transactionTypeGiftCard.length,
        amount: summaryGiftCard
      },
      {
        name: 'Delivery',
        orders: transactionTypeDelivery.length,
        amount: summaryDelivery
      }
    ]

    const orderTypeDineIn = orderData.filter(
      (item) => item.orderType === OrderTypeEnum.DINE_IN
    )
    const summaryTypeDineIn = orderTypeDineIn.length
    const transactionTypeDineIn = transactionData.filter(
      (item) => item.orderType === OrderTypeEnum.DINE_IN
    )

    const orderTypeToGo = orderData.filter(
      (item) => item.orderType === OrderTypeEnum.TO_GO
    )
    const summaryTypeToGo = orderTypeToGo.length
    const transactionTypeToGo = transactionData.filter(
      (item) => item.orderType === OrderTypeEnum.TO_GO
    )

    const transactionTypeOnline = transactionData.filter(
      (item) => item.orderType === OrderTypeEnum.ONLINE
    )

    const summaryOrderType = [
      {
        name: 'Dine In',
        orders: summaryTypeDineIn,
        amount: this.summaryTransactionItem(transactionTypeDineIn, 'totalPaid')
      },
      {
        name: 'To Go',
        orders: summaryTypeToGo,
        amount: this.summaryTransactionItem(transactionTypeToGo, 'totalPaid')
      },
      {
        name: 'Online',
        orders: onlineOrderData.length,
        amount: this.summaryTransactionItem(transactionTypeOnline, 'totalPaid')
      },
      {
        name: 'EatsZaab',
        orders: 0,
        amount: 0
      }
    ]
    const orderCredit = []
    const orderDebit = []
    for (const transaction of transactionTypeCredit) {
      const index = orderCredit.findIndex(
        (item) => item.orderId === transaction.orderId
      )
      if (index !== -1) {
        orderCredit[index] = {
          ...orderCredit[index],
          subtotal: orderCredit[index].subtotal + transaction.subtotal,
          tax: orderCredit[index].tax + transaction.tax + transaction.alcoholTax
        }
      } else {
        orderCredit.push({
          orderId: transaction.orderId,
          subtotal: transaction.subtotal,
          tax: transaction.tax + transaction.alcoholTax
        })
      }
    }
    for (const transaction of transactionTypeDebit) {
      const index = orderDebit.findIndex(
        (item) => item.orderId === transaction.orderId
      )
      if (index !== -1) {
        orderDebit[index] = {
          ...orderDebit[index],
          subtotal: orderDebit[index].subtotal + transaction.subtotal,
          tax: orderDebit[index].tax + transaction.tax + transaction.alcoholTax
        }
      } else {
        orderDebit.push({
          orderId: transaction.orderId,
          subtotal: transaction.subtotal,
          tax: transaction.tax + transaction.alcoholTax
        })
      }
    }
    const summarySalesCredit = this.summaryTransactionItem(
      orderCredit,
      'subtotal'
    )
    const summaryTaxCredit = this.summaryTransactionItem(orderCredit, 'tax')
    const summaryTipsCredit = this.summaryTransactionItem(
      transactionTypeCredit,
      'tips'
    )
    const summarySalesDebit = this.summaryTransactionItem(
      orderDebit,
      'subtotal'
    )
    const summaryTaxDebit = this.summaryTransactionItem(orderDebit, 'tax')
    const summaryTipsDebit = this.summaryTransactionItem(
      transactionTypeDebit,
      'tips'
    )

    const summaryPayoutCard = [
      {
        type: 'Credit',
        sales: summarySalesCredit,
        tips: summaryTipsCredit,
        tax: summaryTaxCredit,
        amount: summarySalesCredit + summaryTipsCredit + summaryTaxCredit
      },
      {
        type: 'Debit',
        sales: summarySalesDebit,
        tips: summaryTipsDebit,
        tax: summaryTaxDebit,
        amount: summarySalesDebit + summaryTipsDebit + summaryTaxDebit
      }
    ]

    const orderDoorDash = tpOrderData.filter(
      (item) => item.deliverySource === ProviderEnum.DOORDASH
    )
    const transactionDoorDash = transactionData.filter(
      (item) => item.deliverySource === ProviderEnum.DOORDASH
    )
    const salesDoorDash = this.summaryTransactionItem(orderDoorDash, 'subtotal')
    const taxDoorDash =
      this.summaryTransactionItem(orderDoorDash, 'tax') +
      this.summaryTransactionItem(orderDoorDash, 'alcoholTax')
    const tipsDoorDash = this.summaryTransactionItem(
      transactionDoorDash,
      'tips'
    )
    const totalDoorDash = this.summaryTransactionItem(
      transactionDoorDash,
      'totalPaid'
    )

    const orderUberEats = tpOrderData.filter(
      (item) => item.deliverySource === ProviderEnum.UBEREATS
    )
    const transactionUberEats = transactionData.filter(
      (item) => item.deliverySource === ProviderEnum.UBEREATS
    )
    const salesUberEats = this.summaryTransactionItem(orderUberEats, 'subtotal')
    const taxUberEats =
      this.summaryTransactionItem(orderUberEats, 'tax') +
      this.summaryTransactionItem(orderUberEats, 'alcoholTax')
    const tipsUberEats = this.summaryTransactionItem(
      transactionUberEats,
      'tips'
    )
    const totalUberEats = this.summaryTransactionItem(
      transactionUberEats,
      'totalPaid'
    )

    const orderGrubHub = tpOrderData.filter(
      (item) => item.deliverySource === ProviderEnum.GRUBHUB
    )
    const transactionGrubHub = transactionData.filter(
      (item) => item.deliverySource === ProviderEnum.GRUBHUB
    )
    const salesGrubHub = this.summaryTransactionItem(orderGrubHub, 'subtotal')
    const taxGrubHub =
      this.summaryTransactionItem(orderGrubHub, 'tax') +
      this.summaryTransactionItem(orderGrubHub, 'alcoholTax')
    const tipsGrubHub = this.summaryTransactionItem(transactionGrubHub, 'tips')
    const totalGrubHub = this.summaryTransactionItem(
      transactionGrubHub,
      'totalPaid'
    )

    const orderGloriaFood = tpOrderData.filter(
      (item) => item.deliverySource === ProviderEnum.GLORIAFOOD
    )
    const transactionGloriaFood = transactionData.filter(
      (item) => item.deliverySource === ProviderEnum.GLORIAFOOD
    )
    const salesGloriaFood = this.summaryTransactionItem(
      orderGloriaFood,
      'subtotal'
    )
    const taxGloriaFood =
      this.summaryTransactionItem(orderGloriaFood, 'tax') +
      this.summaryTransactionItem(orderGloriaFood, 'alcoholTax')
    const tipsGloriaFood = this.summaryTransactionItem(
      transactionGloriaFood,
      'tips'
    )
    const totalGloriaFood = this.summaryTransactionItem(
      transactionGloriaFood,
      'totalPaid'
    )

    const summaryThirdParty = [
      {
        name: 'DoorDash',
        orders: orderDoorDash.length,
        sales: salesDoorDash,
        tax: taxDoorDash,
        tips: tipsDoorDash,
        total: totalDoorDash
      },
      {
        name: 'UberEats',
        orders: orderUberEats.length,
        sales: salesUberEats,
        tax: taxUberEats,
        tips: tipsUberEats,
        total: totalUberEats
      },
      {
        name: 'GrubHub',
        orders: orderGrubHub.length,
        sales: salesGrubHub,
        tax: taxGrubHub,
        tips: tipsGrubHub,
        total: totalGrubHub
      }
    ]

    if (transactionGloriaFood.length > 0) {
      summaryThirdParty.push({
        name: 'GloriaFood',
        orders: orderGloriaFood.length,
        sales: salesGloriaFood,
        tax: taxGloriaFood,
        tips: tipsGloriaFood,
        total: totalGloriaFood
      })
    }

    const mergeOrder = this.mergeOrderData(
      orderData,
      onlineOrderData,
      tpOrderData
    )
    const { timeZone } = restaurant

    const datasets = this.generateGraphData(
      reportType,
      mergeOrder,
      startDate,
      endDate,
      timeZone
    )
    const deMinimisAmount = this.summaryTransactionItem(
      deMinimisOrder,
      'subtotal'
    )
    const deMinimisSalesTax = this.summaryTransactionItem(deMinimisOrder, 'tax')

    const deMinimisSummary = {
      orders: deMinimisOrderIds.length,
      amount: deMinimisAmount,
      salesTax: deMinimisSalesTax,
      total: deMinimisAmount + deMinimisSalesTax
    }

    const voidAmount = this.summaryTransactionItem(voidOrder, 'subtotal')
    const voidSalesTax = this.summaryTransactionItem(voidOrder, 'tax')

    const voidOrderSummary = {
      orders: voidOrder.length,
      amount: voidAmount,
      salesTax: voidSalesTax,
      total: voidAmount + voidSalesTax
    }

    return {
      summary: summarySales,
      transactionType: summaryTransactionType,
      orderingType: summaryOrderType,
      payoutCardSettlement: summaryPayoutCard,
      thirdPartyReport: summaryThirdParty,
      deMinimisSummary,
      voidOrderSummary,
      datasets
    }
  }
}
