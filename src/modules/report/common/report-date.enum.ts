export enum ReportDateEnum {
  TODAY = 'TODAY',
  WEEKLY = 'WEEKLY',
  MONTHLY = 'MONTHLY',
  YEARLY = 'YEARLY',
  RANGE = 'RANGE'
}
