import { forwardRef, Module } from '@nestjs/common'
import { OrderModule } from '../order/order.module'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { TransactionModule } from '../transaction/transaction.module'
import { AdminReportController } from './controllers/admin-report.controller'
import { POSReportController } from './controllers/pos-report.controller'
import { ReportController } from './controllers/report.controller'
import { AdminReportLogic } from './logic/admin-report.logic'
import { POSReportLogic } from './logic/pos-report.logic'
import { ReportLogic } from './logic/report.logic'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    forwardRef(() => OrderModule),
    forwardRef(() => TransactionModule)
  ],
  providers: [POSReportLogic, ReportLogic, AdminReportLogic],
  controllers: [POSReportController, ReportController, AdminReportController],
  exports: []
})
export class ReportModule {}
