import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class IdResponse {
  @ApiProperty()
  id: string
}
class OtherDeviceObject extends IdResponse {
  @ApiProperty()
  description: string
  @ApiProperty()
  name: string
  @ApiProperty()
  restaurant_id: string
}

export class GetAllOtherDevicesResponse extends ResponseDto<
  OtherDeviceObject[]
> {
  @ApiProperty({
    type: [OtherDeviceObject],
    example: [
      {
        description: 'Test description',
        name: 'Front POS',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        id: '6398e6b4e6b45740eb2192a6'
      }
    ]
  })
  data: OtherDeviceObject[]
}

export class AddOtherDeviceResponse extends ResponseDto<IdResponse> {
  @ApiProperty({
    type: IdResponse,
    example: {
      id: '639b15b458389635b2fb39e2'
    }
  })
  data: IdResponse
}
