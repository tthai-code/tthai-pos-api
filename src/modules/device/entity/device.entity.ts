import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from 'src/common/entity/response.entity'

class RestaurantField {
  @ApiProperty()
  id: string
}

class ActiveDeviceCodeFieldsResponse {
  @ApiProperty()
  restaurant: RestaurantField
}

export class ActiveDeviceCodeResponse extends ResponseDto<ActiveDeviceCodeFieldsResponse> {
  @ApiProperty({
    example: {
      restaurant: {
        id: '630eff5751c2eac55f52662c'
      }
    }
  })
  data: ActiveDeviceCodeFieldsResponse

  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN0YXVyYW50Ijp7ImlkIjoiNjMwZWZmNTc1MWMyZWFjNTVmNTI2NjJjIn0sImlhdCI6MTY2NTU3NTUwNywiZXhwIjoxNjY2MTgwMzA3fQ.wz9TsWDwfXSe4B1CyeHCi21FedVCmJX22NaWWrjzCMM'
  })
  access_token: string
}
