import { Controller, Get, UseGuards } from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { GetAllOtherDevicesResponse } from '../entity/other-device.entity'
import { OtherDeviceLogic } from '../logics/other-device.logic'
import { OtherDeviceService } from '../services/other-device.service'

@ApiBearerAuth()
@ApiTags('other-device')
@UseGuards(AdminAuthGuard)
@Controller('v1/other-device')
export class OtherDeviceController {
  constructor(
    private readonly otherDeviceService: OtherDeviceService,
    private readonly otherDeviceLogic: OtherDeviceLogic
  ) {}

  @Get()
  @ApiOkResponse({ type: () => GetAllOtherDevicesResponse })
  @ApiOperation({
    summary: 'Get all others device',
    description: 'use bearer `restaurant_token`'
  })
  async getAllOtherDevice() {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.otherDeviceLogic.getAllOtherDevice(restaurantId)
  }
}
