import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Request,
  UseGuards
} from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuperAdminAuthGuard } from 'src/modules/auth/guards/super-admin-auth.guard'
import { AuthLogic } from 'src/modules/auth/logics/auth.logic'
import { LogLogic } from 'src/modules/log/logic/log.logic'
import { errorResponse } from 'src/utilities/errorResponse'
import { ActivateDeviceCodeDto } from '../dto/activate-device.dto'
import { ActiveDeviceCodeResponse } from '../entity/device.entity'

@ApiTags('auth/device')
@Controller('v1/activation-code')
export class PublicDeviceController {
  constructor(
    private readonly authLogic: AuthLogic,
    private readonly logLogic: LogLogic
  ) {}

  @Post()
  @ApiOkResponse({ type: () => ActiveDeviceCodeResponse })
  @ApiBadRequestResponse(
    errorResponse(
      400,
      'This activation code is already used.',
      'v1/activate-code'
    )
  )
  @ApiNotFoundResponse(
    errorResponse(404, 'Not found activation code.', 'v1/activate-code')
  )
  @ApiOperation({ summary: 'Activate Code' })
  async activateCode(
    @Body() payload: ActivateDeviceCodeDto,
    @Request() request: any
  ) {
    const activated = await this.authLogic.ActivateDeviceCode(
      payload.activationCode
    )
    await this.logLogic.createLogic(request, 'Activate Code', activated)
    return activated
  }

  @UseGuards(SuperAdminAuthGuard)
  @Get('direct-admin/:restaurantId')
  async deviceLogin(@Param('restaurantId') id: string) {
    return await this.authLogic.DirectAdminActivateDeviceCode(id)
  }
}
