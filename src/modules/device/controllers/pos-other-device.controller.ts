import {
  Body,
  Controller,
  Delete,
  NotFoundException,
  Param,
  Post,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { SuccessObjectResponse } from 'src/common/entity/success.entity'
import { StatusEnum } from 'src/common/enum/status.enum'
import { StaffAuthGuard } from 'src/modules/auth/guards/staff-auth.guard'
import { CreateOtherDeviceDto } from '../dto/create-other-device.dto'
import { AddOtherDeviceResponse } from '../entity/other-device.entity'
import { OtherDeviceLogic } from '../logics/other-device.logic'
import { OtherDeviceService } from '../services/other-device.service'

@ApiBearerAuth()
@ApiTags('pos/other-device')
@UseGuards(StaffAuthGuard)
@Controller('/v1/pos/other-device')
export class POSOtherDeviceController {
  constructor(
    private readonly otherDeviceService: OtherDeviceService,
    private readonly otherDeviceLogic: OtherDeviceLogic
  ) {}

  @Post()
  @ApiCreatedResponse({ type: () => AddOtherDeviceResponse })
  @ApiOperation({
    summary: 'Add Other Device',
    description: 'use bearer `staff_token`'
  })
  async addOtherDevice(@Body() payload: CreateOtherDeviceDto) {
    const restaurantId = StaffAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.otherDeviceLogic.addOtherDevice(restaurantId, payload)
  }

  @Delete(':otherDeviceId')
  @ApiOkResponse({ type: () => SuccessObjectResponse })
  @ApiOperation({
    summary: 'Delete Other Device',
    description: 'use bearer `staff_token`'
  })
  async deleteOtherDevice(@Param('otherDeviceId') id: string) {
    const device = await this.otherDeviceService.findOne(
      { _id: id, status: StatusEnum.ACTIVE },
      { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 }
    )
    if (!device) throw new NotFoundException('Not found device.')
    await this.otherDeviceService.hardDelete(id)
    return { success: true }
  }
}
