import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
  UseGuards
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger'
import { StatusEnum } from 'src/common/enum/status.enum'
import { AdminAuthGuard } from 'src/modules/auth/guards/admin-auth.guard'
import { CreateDeviceDto } from '../dto/create-device.dto'
import { UpdateDeviceDto } from '../dto/update-device.dto'
import { DeviceLogic } from '../logics/device.logic'
import { createDeviceResponse } from '../response/create-device.response'
import { getAllDeviceResponse } from '../response/get-all-device.reponse'
import { getDeviceResponse } from '../response/get-device.response'
import { updateDeviceResponse } from '../response/update-device.response'
import { DeviceService } from '../services/device.service'

@ApiBearerAuth()
@ApiTags('device')
@UseGuards(AdminAuthGuard)
@Controller('v1/device')
export class DeviceController {
  constructor(
    private readonly deviceService: DeviceService,
    private readonly deviceLogic: DeviceLogic
  ) {}

  @Get('limit/capacity')
  @ApiOperation({ summary: 'Get Device Limit' })
  async getDeviceLimit() {
    const restaurantId = AdminAuthGuard.getAuthorizedUser()?.restaurant?.id
    return await this.deviceLogic.getDeviceLimit(restaurantId)
  }

  @Post()
  @ApiCreatedResponse(createDeviceResponse)
  @ApiOperation({ summary: 'Create a device and generate token' })
  async createDevice(@Body() payload: CreateDeviceDto) {
    return await this.deviceLogic.createDevice(payload)
  }

  @Get(':restaurantId/restaurant')
  @ApiOkResponse(getAllDeviceResponse)
  @ApiOperation({ summary: 'Get all device by restaurant' })
  async getAllDeviceByRestaurant(@Param('restaurantId') id: string) {
    return await this.deviceService.getAll({
      restaurantId: id,
      status: StatusEnum.ACTIVE
    })
  }

  @Get(':deviceId')
  @ApiOkResponse(getDeviceResponse)
  @ApiOperation({ summary: 'Get a device by device ID' })
  async getDeviceById(@Param('deviceId') id: string) {
    const device = await this.deviceService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!device) {
      throw new NotFoundException('Not found device.')
    }
    return device
  }

  @Put(':deviceId')
  @ApiOkResponse(updateDeviceResponse)
  @ApiOperation({ summary: 'Update a device setting' })
  async updateDevice(
    @Param('deviceId') id: string,
    @Body() payload: UpdateDeviceDto
  ) {
    const device = await this.deviceService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!device) {
      throw new NotFoundException('Not found device.')
    }
    return await this.deviceService.update(id, payload)
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete a device by ID' })
  async deleteDevice(@Param('id') id: string) {
    const device = await this.deviceService.findOne({
      _id: id,
      status: StatusEnum.ACTIVE
    })
    if (!device) {
      throw new NotFoundException('Not found device.')
    }
    return await this.deviceService.delete(id)
  }
}
