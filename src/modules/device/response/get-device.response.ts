export const getDeviceResponse = {
  schema: {
    type: 'object',
    example: {
      message: 'done',
      data: {
        status: 'active',
        is_activated: false,
        activation_code: 'VPCWv0Y2',
        description: 'Front POS description',
        name: 'Front POS',
        restaurant_id: '630e55a0d9c30fd7cdcb424b',
        created_at: '2022-10-07T07:17:30.599Z',
        updated_at: '2022-10-07T07:26:36.533Z',
        updated_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        created_by: {
          username: 'corywong@mail.com',
          id: '630e53ec9e21d871a49fb4f5'
        },
        id: '633fd28ac0871b45fe02b23a'
      }
    }
  }
}
