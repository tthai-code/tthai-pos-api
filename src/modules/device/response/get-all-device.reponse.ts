export const getAllDeviceResponse = {
  schema: {
    type: 'object',
    example: {
      message: 'done',
      data: [
        {
          status: 'active',
          is_activated: false,
          activation_code: 'eUAfSDXE',
          description: 'Back POS description',
          name: 'Back POS',
          restaurant_id: '630e55a0d9c30fd7cdcb424b',
          created_at: '2022-10-07T07:07:00.534Z',
          updated_at: '2022-10-07T07:07:00.534Z',
          updated_by: {
            username: 'corywong@mail.com',
            id: '630e53ec9e21d871a49fb4f5'
          },
          created_by: {
            username: 'corywong@mail.com',
            id: '630e53ec9e21d871a49fb4f5'
          },
          id: '633fd014ee9016b8b9b5ea42'
        },
        {
          status: 'active',
          is_activated: false,
          activation_code: 'VPCWv0Y2',
          description: 'Front POS description',
          name: 'Front POS',
          restaurant_id: '630e55a0d9c30fd7cdcb424b',
          created_at: '2022-10-07T07:17:30.599Z',
          updated_at: '2022-10-07T07:26:36.533Z',
          updated_by: {
            username: 'corywong@mail.com',
            id: '630e53ec9e21d871a49fb4f5'
          },
          created_by: {
            username: 'corywong@mail.com',
            id: '630e53ec9e21d871a49fb4f5'
          },
          id: '633fd28ac0871b45fe02b23a'
        }
      ]
    }
  }
}
