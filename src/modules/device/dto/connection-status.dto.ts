import { ApiProperty } from '@nestjs/swagger'
import { IsIn, IsString } from 'class-validator'
import { ConnectionStatusEnum } from '../common/device.enum'

export class UpdateConnectionStatusDto {
  @IsIn(Object.values(ConnectionStatusEnum))
  @IsString()
  @ApiProperty({
    enum: Object.values(ConnectionStatusEnum),
    example: ConnectionStatusEnum.CONNECTED
  })
  readonly connectionStatus: string
}
