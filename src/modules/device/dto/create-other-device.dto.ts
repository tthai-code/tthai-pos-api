import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger'
import { IsOptional, IsString } from 'class-validator'

export class CreateOtherDeviceDto {
  public restaurantId: string

  @IsString()
  @ApiProperty({ example: 'Front Cashier' })
  readonly name: string

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ example: 'Terminal' })
  readonly description: string
}
