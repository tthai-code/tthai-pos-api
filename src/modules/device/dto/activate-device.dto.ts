import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class ActivateDeviceCodeDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'VPCWv0Y2' })
  readonly activationCode: string
}
