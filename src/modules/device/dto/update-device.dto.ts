import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class UpdateDeviceDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Front POS' })
  readonly name: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Front POS description' })
  readonly description: string
}
