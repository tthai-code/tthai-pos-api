import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from 'class-validator'

export class CreateDeviceDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: '630e55a0d9c30fd7cdcb424b' })
  readonly restaurantId: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ example: 'Back POS' })
  readonly name: string

  @IsString()
  @ApiProperty({ example: 'Back POS description' })
  readonly description: string
}
