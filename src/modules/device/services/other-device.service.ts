import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { PaginateModel } from 'mongoose-paginate'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { OtherDeviceDocument } from '../schemas/other-device.schema'

@Injectable({ scope: Scope.REQUEST })
export class OtherDeviceService {
  constructor(
    @InjectModel('otherDevices')
    private readonly OtherDeviceModel: PaginateModel<OtherDeviceDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<OtherDeviceDocument | any> {
    return this.OtherDeviceModel.findById({ _id: id })
  }

  async resolveByCondition(condition: any): Promise<OtherDeviceDocument | any> {
    return this.OtherDeviceModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.OtherDeviceModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<OtherDeviceDocument> {
    return this.OtherDeviceModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.OtherDeviceModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.OtherDeviceModel.createCollection()

    return this.OtherDeviceModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<OtherDeviceDocument> {
    const document = new this.OtherDeviceModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<OtherDeviceDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<OtherDeviceDocument> {
    const document = new this.OtherDeviceModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<OtherDeviceDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<OtherDeviceDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<OtherDeviceDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  async hardDelete(id: string): Promise<OtherDeviceDocument> {
    return this.OtherDeviceModel.findOneAndDelete({ _id: id })
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.OtherDeviceModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.OtherDeviceModel.findById(id)
  }

  findOne(condition: any, project?: any): Promise<OtherDeviceDocument> {
    return this.OtherDeviceModel.findOne(condition, project)
  }

  findOneWithSelect(
    condition: any,
    options?: any
  ): Promise<OtherDeviceDocument> {
    return this.OtherDeviceModel.findOne(condition, options)
  }
}
