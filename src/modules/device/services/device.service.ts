import { Inject, Injectable, Scope } from '@nestjs/common'
import { REQUEST } from '@nestjs/core'
import { InjectModel } from '@nestjs/mongoose'
import { ClientSession } from 'mongodb'
import { PaginateModel } from 'mongoose-paginate'
import { Model } from 'mongoose'
import { StatusEnum } from 'src/common/enum/status.enum'
import { DeviceDocument } from '../schemas/device.schema'

@Injectable({ scope: Scope.REQUEST })
export class DeviceService {
  constructor(
    @InjectModel('device')
    private readonly DeviceModel: PaginateModel<DeviceDocument> | any,
    @Inject(REQUEST) private request: any
  ) {}

  async resolveByUrl({ id }): Promise<DeviceDocument | any> {
    return this.DeviceModel.findById({ _id: id })
  }

  async resolveByCondition(condition: any): Promise<DeviceDocument | any> {
    return this.DeviceModel.findOne(condition)
  }

  async isExists(condition: any): Promise<boolean> {
    const result = await this.DeviceModel.findOne(condition)

    return result ? true : false
  }

  getModel(): Model<DeviceDocument> {
    return this.DeviceModel
  }

  async aggregate(pipeline: any[]): Promise<any[]> {
    return this.DeviceModel.aggregate(pipeline)
  }

  async getSession(): Promise<ClientSession> {
    await this.DeviceModel.createCollection()

    return this.DeviceModel.startSession()
  }

  async transactionCreate(
    payload: any,
    session: ClientSession
  ): Promise<DeviceDocument> {
    const document = new this.DeviceModel(payload)
    document?.setAuthor(this.request)

    return document.save({ session })
  }

  async transactionUpdate(
    id: string,
    product: any,
    session: ClientSession
  ): Promise<DeviceDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...product }).save({ session })
  }

  async create(payload: any): Promise<DeviceDocument> {
    const document = new this.DeviceModel(payload)
    document?.setAuthor(this.request)

    return document.save()
  }

  async update(id: string, payload: any): Promise<DeviceDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async findOneAndUpdate(
    condition: any,
    payload: any
  ): Promise<DeviceDocument> {
    const document = await this.resolveByCondition(condition)
    document?.setAuthor(this.request)

    return document.set({ ...payload }).save()
  }

  async delete(id: string): Promise<DeviceDocument> {
    const document = await this.resolveByUrl({ id })
    document?.setAuthor(this.request)

    return document.set({ status: StatusEnum.DELETED }).save()
  }

  getAll(condition: any, project?: any): Promise<any[]> {
    return this.DeviceModel.find(condition, project)
  }

  findById(id: number): Promise<any> {
    return this.DeviceModel.findById(id)
  }

  findOne(condition: any): Promise<DeviceDocument> {
    return this.DeviceModel.findOne(condition)
  }

  findOneWithSelect(condition: any, options?: any): Promise<DeviceDocument> {
    return this.DeviceModel.findOne(condition, options)
  }
}
