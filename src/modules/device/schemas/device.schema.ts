import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Document, Schema as MongooseSchema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type DeviceDocument = DeviceFields & Document

@Schema({ timestamps: true, strict: true, collection: 'devices' })
export class DeviceFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop()
  name: string

  @Prop()
  description: string

  @Prop()
  activationCode: string

  @Prop()
  isActivated: boolean

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const DeviceSchema = SchemaFactory.createForClass(DeviceFields)
DeviceSchema.plugin(mongoosePaginate)
DeviceSchema.plugin(authorStampCreatePlugin)
