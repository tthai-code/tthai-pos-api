import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Types, Schema as MongooseSchema, Document } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate'
import { StatusEnum } from 'src/common/enum/status.enum'
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema'
import { authorStampCreatePlugin } from 'src/modules/database/author-stamp.plugin'

export type OtherDeviceDocument = OtherDeviceFields & Document

@Schema({ timestamps: true, strict: true, collection: 'otherDevices' })
export class OtherDeviceFields {
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'restaurant' })
  restaurantId: Types.ObjectId

  @Prop({ required: true })
  name: string

  @Prop({ default: '' })
  description: string

  @Prop({ default: StatusEnum.ACTIVE, enum: Object.values(StatusEnum) })
  status: string

  @Prop()
  updatedBy: UserStampSchema

  @Prop()
  createdBy: UserStampSchema
}
export const OtherDeviceSchema = SchemaFactory.createForClass(OtherDeviceFields)
OtherDeviceSchema.plugin(authorStampCreatePlugin)
OtherDeviceSchema.plugin(mongoosePaginate)
