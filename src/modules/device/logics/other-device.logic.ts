import { Injectable, NotFoundException } from '@nestjs/common'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantDocument } from 'src/modules/restaurant/schemas/restaurant.schema'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { CreateOtherDeviceDto } from '../dto/create-other-device.dto'
import { OtherDeviceService } from '../services/other-device.service'

@Injectable()
export class OtherDeviceLogic {
  constructor(
    private readonly restaurantService: RestaurantService,
    private readonly otherDeviceService: OtherDeviceService
  ) {}

  private async checkRestaurant(
    restaurantId: string
  ): Promise<RestaurantDocument> {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    return restaurant
  }

  async getAllOtherDevice(restaurantId: string) {
    const restaurant = await this.checkRestaurant(restaurantId)
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    const otherDevices = await this.otherDeviceService.getAll(
      { restaurantId, status: StatusEnum.ACTIVE },
      { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 }
    )
    return otherDevices
  }

  async addOtherDevice(restaurantId: string, payload: CreateOtherDeviceDto) {
    const restaurant = await this.checkRestaurant(restaurantId)
    if (!restaurant) throw new NotFoundException('Not found restaurant.')
    payload.restaurantId = restaurantId
    const device = await this.otherDeviceService.create(payload)
    return { id: device.id }
  }
}
