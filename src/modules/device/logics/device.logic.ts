import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common'
import { classToPlain } from 'class-transformer'
import { StatusEnum } from 'src/common/enum/status.enum'
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service'
import { PackageTypeEnum } from 'src/modules/subscription/common/package-type.enum'
import { SubscriptionStatusEnum } from 'src/modules/subscription/common/subscription-plan.enum'
import { PackageService } from 'src/modules/subscription/services/package.service'
import { SubscriptionService } from 'src/modules/subscription/services/subscription.service'
import { CreateDeviceDto } from '../dto/create-device.dto'
import { DeviceDocument } from '../schemas/device.schema'
import { DeviceService } from '../services/device.service'

@Injectable()
export class DeviceLogic {
  constructor(
    private readonly deviceService: DeviceService,
    private readonly restaurantService: RestaurantService,
    private readonly subscriptionService: SubscriptionService,
    private readonly packageService: PackageService
  ) {}

  private randomActivateCode() {
    const message = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'
    let code = ''
    for (let i = 0; i < 8; i++) {
      code += message.charAt(Math.floor(Math.random() * message.length))
    }

    return code
  }

  private async genOrderUrl() {
    let code = ''
    while (true) {
      code = this.randomActivateCode()
      const isDuplicated = await this.deviceService.findOne({
        activationCode: code
      })
      if (!isDuplicated) {
        break
      }
    }

    return code
  }

  async createDevice(createDevice: CreateDeviceDto): Promise<DeviceDocument> {
    const restaurant = await this.restaurantService.findOne({
      _id: createDevice.restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) {
      throw new NotFoundException('Not found restaurant.')
    }
    const isDeviceOverLimit = await this.checkDeviceLimit(
      createDevice.restaurantId
    )
    if (isDeviceOverLimit <= 0) {
      throw new BadRequestException(
        'Cannot create more device please add more limit capacity.'
      )
    }
    const code = await this.genOrderUrl()
    const payload = classToPlain(createDevice)
    payload.isActivated = false
    payload.activationCode = code
    return this.deviceService.create(payload)
  }

  async getDeviceLimit(restaurantId: string) {
    const restaurant = await this.restaurantService.findOne({
      _id: restaurantId,
      status: StatusEnum.ACTIVE
    })
    if (!restaurant) {
      throw new NotFoundException('Not found restaurant.')
    }
    const iPadPackages = await this.packageService.getAll({
      packageType: PackageTypeEnum.IPAD,
      status: StatusEnum.ACTIVE
    })
    const iPadPackageIds = iPadPackages.map((item) => item.id)
    const subscription = await this.subscriptionService.getAll({
      restaurantId,
      packageId: { $in: iPadPackageIds },
      subscriptionStatus: SubscriptionStatusEnum.ACTIVE,
      status: StatusEnum.ACTIVE
    })
    let limitCapacity = 0
    for (const sub of subscription) {
      const packageIndex = iPadPackages.findIndex(
        (item) => `${item._id}` === sub.packageId
      )
      if (packageIndex > -1) {
        const { iPadCapacity } = iPadPackages[packageIndex]
        const { quantity } = sub
        const calculated = iPadCapacity * quantity
        limitCapacity += calculated
      }
    }
    return { limitCapacity }
  }

  async checkDeviceLimit(restaurantId) {
    const iPadPackages = await this.packageService.getAll({
      packageType: PackageTypeEnum.IPAD,
      status: StatusEnum.ACTIVE
    })
    const iPadPackageIds = iPadPackages.map((item) => item.id)
    const subscription = await this.subscriptionService.getAll({
      restaurantId,
      packageId: { $in: iPadPackageIds },
      subscriptionStatus: SubscriptionStatusEnum.ACTIVE,
      status: StatusEnum.ACTIVE
    })
    let limitCapacity = 0
    for (const sub of subscription) {
      const packageIndex = iPadPackages.findIndex(
        (item) => `${item._id}` === sub.packageId
      )
      if (packageIndex > -1) {
        const { iPadCapacity } = iPadPackages[packageIndex]
        const { quantity } = sub
        const calculated = iPadCapacity * quantity
        limitCapacity += calculated
      }
    }
    const device = await this.deviceService.getAll({
      restaurantId,
      status: StatusEnum.ACTIVE
    })
    return limitCapacity - device.length
  }
}
