import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { RestaurantModule } from '../restaurant/restaurant.module'
import { SubscriptionModule } from '../subscription/subscription.module'
import { DeviceController } from './controllers/device.controller'
import { OtherDeviceController } from './controllers/other-device.controller'
import { POSOtherDeviceController } from './controllers/pos-other-device.controller'
import { PublicDeviceController } from './controllers/public-device.controller'
import { DeviceLogic } from './logics/device.logic'
import { OtherDeviceLogic } from './logics/other-device.logic'
import { DeviceSchema } from './schemas/device.schema'
import { OtherDeviceSchema } from './schemas/other-device.schema'
import { DeviceService } from './services/device.service'
import { OtherDeviceService } from './services/other-device.service'

@Module({
  imports: [
    forwardRef(() => RestaurantModule),
    forwardRef(() => SubscriptionModule),
    MongooseModule.forFeature([
      { name: 'device', schema: DeviceSchema },
      { name: 'otherDevices', schema: OtherDeviceSchema }
    ])
  ],
  providers: [DeviceService, DeviceLogic, OtherDeviceService, OtherDeviceLogic],
  controllers: [
    DeviceController,
    PublicDeviceController,
    POSOtherDeviceController,
    OtherDeviceController
  ],
  exports: [DeviceService]
})
export class DeviceModule {}
