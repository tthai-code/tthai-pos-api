import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { APP_FILTER, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core'
import { MongooseModule } from '@nestjs/mongoose'
import { AppController } from './app.controller'
import { ErrorFilter } from './exception-filter/error.filter'
import { HttpExceptionFilter } from './exception-filter/http-exception.filter'
import { MongooseTransformers } from './interceptors/mongoose.transformer'
import { TransformInterceptor } from './interceptors/transfromer.interceptor'
import { AuthModule } from './modules/auth/auth.module'
import { DatabaseService } from './modules/database/database.service'
import { MongooseConfigService } from './modules/database/mongoose.config'
import { CounterSchema } from './modules/database/schemas/counter.schema'
import { LogModule } from './modules/log/log.module'
import { OrderModule } from './modules/order/order.module'
import { UploadModule } from './modules/upload/upload.module'
import { MenuModule } from './modules/menu/menu.module'
import { MenuCategoriesModule } from './modules/menu-category/menu-categories.module'
import { RestaurantModule } from './modules/restaurant/restaurant.module'
import { UserModule } from './modules/user/user.module'
import { CamelizeKeysPipe } from './pipe/camelize-key.pipe'
import { CustomValidationPipe } from './pipe/custom-validation.pipe'
import { RestaurantAccountModule } from './modules/restaurant-account/restaurant-account.module'
import { StaffModule } from './modules/staff/staff.module'
import { ModifierModule } from './modules/modifier/modifier.module'
import { MenuModifierModule } from './modules/menu-modifier/menu-modifier.module'
import { TableZoneModule } from './modules/table-zone/table-zone.module'
import { TableModule } from './modules/table/table.module'
import { RestaurantHoursModule } from './modules/restaurant-hours/restaurant-hours.module'
import { RestaurantHolidaysModule } from './modules/restaurant-holiday/restaurant-holiday.module'
import { DeviceModule } from './modules/device/device.module'
import { PaymentGatewayModule } from './modules/payment-gateway/payment-gateway.module'
import { ThirdPartyDeliveryModule } from './modules/third-party-delivery/third-party-delivery.module'
import { OnlineReceiptModule } from './modules/online-receipt/oniine.receipt.module'
import { CateringModule } from './modules/catering/catering.module'
import { WebSettingModule } from './modules/web-setting/web-setting.module'
import { TThaiAppModule } from './modules/tthai-app/tthai-app.module'
import { CustomerModule } from './modules/customer/customer.module'
import { SocketModule } from './modules/socket/socket.module'
import { TransactionModule } from './modules/transaction/transaction.module'
import { KitchenHubModule } from './modules/kitchen-hub/kitchen-hub.module'
import { ReportModule } from './modules/report/report.module'
import { SubscriptionModule } from './modules/subscription/subscription.module'
import { BillModule } from './modules/bill/bill.module'
import { CashDrawerModule } from './modules/cash-drawer/cash-drawer.module'
import { DiscountModule } from './modules/discount/discount.module'
import { POSMailerModule } from './modules/mailer/mailer.module'
import { LogLogic } from './modules/log/logic/log.logic'
import { RestaurantShiftsModule } from './modules/restaurant-shifts/restaurant-shifts.module'

const AppProviders = [
  LogLogic,
  {
    provide: APP_PIPE,
    useClass: CamelizeKeysPipe
  },
  {
    provide: APP_PIPE,
    useClass: CustomValidationPipe
  },
  {
    provide: APP_FILTER,
    useClass: ErrorFilter
  },
  {
    provide: APP_FILTER,
    useClass: HttpExceptionFilter
  },
  {
    provide: APP_INTERCEPTOR,
    useClass: TransformInterceptor
  },
  DatabaseService,
  MongooseTransformers
]

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRootAsync({
      useClass: MongooseConfigService
    }),
    MongooseModule.forFeature([{ name: 'Counter', schema: CounterSchema }]),
    AuthModule,
    RestaurantModule,
    RestaurantAccountModule,
    RestaurantHoursModule,
    RestaurantHolidaysModule,
    StaffModule,
    UserModule,
    DeviceModule,
    MenuCategoriesModule,
    ModifierModule,
    MenuModule,
    MenuModifierModule,
    TableZoneModule,
    TableModule,
    PaymentGatewayModule,
    ThirdPartyDeliveryModule,
    TThaiAppModule,
    OnlineReceiptModule,
    CateringModule,
    WebSettingModule,
    OrderModule,
    CustomerModule,
    UploadModule,
    LogModule,
    TransactionModule,
    KitchenHubModule,
    SocketModule,
    SubscriptionModule,
    ReportModule,
    BillModule,
    CashDrawerModule,
    DiscountModule,
    POSMailerModule,
    RestaurantShiftsModule
  ],
  controllers: [AppController],
  providers: [...AppProviders],
  exports: []
})
export class AppModule {}
