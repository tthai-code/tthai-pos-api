import { ApiProperty } from '@nestjs/swagger'
import { ResponseDto } from './response.entity'

export class SuccessField {
  @ApiProperty()
  success: boolean
}

class SuccessWithIdField extends SuccessField {
  @ApiProperty()
  id: string
}
export class SuccessObjectResponse extends ResponseDto<SuccessField> {
  @ApiProperty({ type: SuccessField, example: { success: true } })
  data: SuccessField
}

export class SuccessWithIdResponse extends ResponseDto<SuccessWithIdField> {
  @ApiProperty({
    type: SuccessWithIdField,
    example: { success: true, id: '63a0dabb4d8b0a61654c6d10' }
  })
  data: SuccessWithIdField
}
