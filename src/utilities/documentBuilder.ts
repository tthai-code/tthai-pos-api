import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { RedocModule, RedocOptions } from 'nestjs-redoc'

export async function documentOpenBuilder(app: any) {
  const options = new DocumentBuilder()
    .setTitle('TThai Open API')
    .setDescription('TThai Open API.')
    .setVersion('1.0')
    .addBearerAuth()
    .addBasicAuth()
    .addOAuth2()
    .addApiKey()
    .addCookieAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)
  const redocOptions: RedocOptions = {
    title: 'TThai Open API',
    // logo: {
    //   url: '<url>',
    //   backgroundColor: '#FAFAFA',
    //   altText: 'ava-logo'
    // },
    docName: 'tthai-open-api',
    sortPropsAlphabetically: true,
    pathInMiddlePanel: true,
    hideDownloadButton: false,
    hideHostname: false,
    showExtensions: true,
    expandResponses: 'all',
    // favicon: '<url>',
    tagGroups: [
      {
        name: 'Restaurant',
        tags: ['open/restaurant']
      },
      {
        name: 'Payment Gateway',
        tags: ['open/payment']
      },
      {
        name: 'Web Setting',
        tags: ['online/web-setting']
      }
    ],
    auth: {
      enabled: true,
      user: 'tthai-open',
      password: 'qwerty1234@'
    }
  }
  await RedocModule.setup('/docs/open', app, document, redocOptions)
}

export async function documentSuperAdminBuilder(app: any) {
  const options = new DocumentBuilder()
    .setTitle('TThai Super Admin API')
    .setDescription('TThai Super Admin API.')
    .setVersion('1.0')
    .addBearerAuth()
    .addBasicAuth()
    .addOAuth2()
    .addApiKey()
    .addCookieAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)
  const redocOptions: RedocOptions = {
    title: 'TThai Super Admin API',
    // logo: {
    //   url: '<url>',
    //   backgroundColor: '#FAFAFA',
    //   altText: 'ava-logo'
    // },
    docName: 'tthai-super-api',
    sortPropsAlphabetically: true,
    pathInMiddlePanel: true,
    hideDownloadButton: false,
    hideHostname: false,
    showExtensions: true,
    expandResponses: 'all',
    // favicon: '<url>',
    tagGroups: [
      {
        name: 'Auth',
        tags: ['public/user']
      },
      {
        name: 'User Management',
        tags: ['users-admin']
      },
      {
        name: 'Restaurant Management',
        tags: [
          'admin/restaurant',
          'admin/subscription',
          'admin/payment-gateway'
        ]
      },
      {
        name: 'Report',
        tags: ['admin/report']
      },
      {
        name: 'Package Management',
        tags: ['admin/package']
      },
      {
        name: 'Billing',
        tags: ['admin/billing']
      }
    ]
    // auth: {
    //   enabled: true,
    //   user: 'superadmin',
    //   password: 'qwerty1234@'
    // }
  }
  await RedocModule.setup('/docs/admin', app, document, redocOptions)
}

export async function documentKitchenBuilder(app: any) {
  const options = new DocumentBuilder()
    .setTitle('TThai Kitchen Hub API')
    .setDescription('TThai Kitchen Hub API.')
    .setVersion('1.0')
    .addBearerAuth()
    .addBasicAuth()
    .addOAuth2()
    .addApiKey()
    .addCookieAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)
  const redocOptions: RedocOptions = {
    title: 'TThai Kitchen Hub API',
    // logo: {
    //   url: '<url>',
    //   backgroundColor: '#FAFAFA',
    //   altText: 'ava-logo'
    // },
    docName: 'tthai-kitchen-hub-api',
    sortPropsAlphabetically: true,
    pathInMiddlePanel: true,
    hideDownloadButton: false,
    hideHostname: false,
    showExtensions: true,
    expandResponses: 'all',
    // favicon: '<url>',
    tagGroups: [
      {
        name: 'Kitchen Hub',
        tags: ['open/kitchen-hub']
      }
    ]
    // auth: {
    //   enabled: true,
    //   user: 'superadmin',
    //   password: 'qwerty1234@'
    // }
  }
  await RedocModule.setup('/docs/kitchen-hub', app, document, redocOptions)
}

export async function documentRestaurantBuilder(app: any) {
  const options = new DocumentBuilder()
    .setTitle('TThai Web Base API')
    .setDescription('TThai Web Base API.')
    .setVersion('1.0')
    .addBearerAuth()
    .addBasicAuth()
    .addOAuth2()
    .addApiKey()
    .addCookieAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)
  const redocOptions: RedocOptions = {
    title: 'TThai Web Base API',
    // logo: {
    //   url: '<url>',
    //   backgroundColor: '#FAFAFA',
    //   altText: 'ava-logo'
    // },
    docName: 'tthai-web-base-api',
    sortPropsAlphabetically: true,
    pathInMiddlePanel: true,
    hideDownloadButton: false,
    hideHostname: false,
    showExtensions: true,
    expandResponses: 'all',
    // favicon: '<url>',
    tagGroups: [
      {
        name: 'Restaurant Account',
        tags: ['restaurant-account', 'public/account']
      },
      {
        name: 'Restaurant',
        tags: ['restaurant', 'restaurant-hours', 'restaurant-holidays']
      },
      {
        name: 'Device',
        tags: ['device', 'other-device']
      },
      {
        name: 'Payment Gateway',
        tags: ['payment-gateway-setting']
      },
      {
        name: 'Staff',
        tags: ['staff']
      },
      {
        name: 'Menu',
        tags: ['menu', 'menu-modifier', 'menu-category']
      },
      {
        name: 'Table & Zone',
        tags: ['table', 'table-zone']
      },
      {
        name: 'Restaurant Setting',
        tags: [
          'delivery-provider',
          'third-party-delivery',
          'online-receipt-setting',
          'tthai-app-setting',
          'catering-setting',
          'web-setting'
        ]
      },
      {
        name: 'Bank Account',
        tags: ['bank-account']
      },
      {
        name: 'Transaction',
        tags: ['transaction', 'cash-drawer']
      },
      {
        name: 'Subscription',
        tags: ['subscription', 'package']
      },
      {
        name: 'Report',
        tags: ['report']
      },
      {
        name: 'Upload',
        tags: ['uploads']
      }
    ]
    // auth: {
    //   enabled: true,
    //   user: 'superadmin',
    //   password: 'qwerty1234@'
    // }
  }
  await RedocModule.setup('/docs/restaurant', app, document, redocOptions)
}

export async function documentBuilderOnline(app: any) {
  const options = new DocumentBuilder()
    .setTitle('POS service api')
    .setDescription('POS service api.')
    .setVersion('1.0')
    .addBearerAuth()
    .addBasicAuth()
    .addOAuth2()
    .addApiKey()
    .addCookieAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)
  const redocOptions: RedocOptions = {
    title: 'POS service api',
    // logo: {
    //   url: '<url>',
    //   backgroundColor: '#FAFAFA',
    //   altText: 'ava-logo'
    // },
    docName: 'Online-api',
    sortPropsAlphabetically: true,
    pathInMiddlePanel: true,
    hideDownloadButton: false,
    hideHostname: false,
    showExtensions: true,
    expandResponses: 'all',
    // favicon: '<url>',
    tagGroups: [
      {
        name: 'Customer Online',
        tags: ['customer-online']
      },
      {
        name: 'Order Online',
        tags: ['public/order-hour', 'order-online']
      },
      {
        name: 'Menu Public Online',
        tags: ['menu-public-online']
      },
      {
        name: 'Menu Category Public Online',
        tags: ['menu-category-public-online']
      },
      {
        name: 'Menu Modifier Public Online',
        tags: ['menu-modifier-online']
      },
      {
        name: 'Web Setting',
        tags: ['online/web-setting']
      }
    ],
    auth: {
      enabled: true,
      user: 'admin',
      password: 'qwerty1234@'
    }
  }
  await RedocModule.setup('/docs/online', app, document, redocOptions)
}

export async function documentBuilderReview(app: any) {
  const options = new DocumentBuilder()
    .setTitle('TThai Review service api')
    .setDescription('TThai Review service api.')
    .setVersion('1.0')
    .addBearerAuth()
    .addBasicAuth()
    .addOAuth2()
    .addApiKey()
    .addCookieAuth()
    .build()
  const document = SwaggerModule.createDocument(app, options)
  const redocOptions: RedocOptions = {
    title: 'TThai Review service api',
    // logo: {
    //   url: '<url>',
    //   backgroundColor: '#FAFAFA',
    //   altText: 'ava-logo'
    // },
    docName: 'TThai-review-api',
    sortPropsAlphabetically: true,
    pathInMiddlePanel: true,
    hideDownloadButton: false,
    hideHostname: false,
    showExtensions: true,
    expandResponses: 'all',
    // favicon: '<url>',
    tagGroups: [
      {
        name: 'Restaurant',
        tags: ['open/restaurant', 'open/eats-zaap']
      },
      {
        name: 'Menu & Category',
        tags: ['open/category', 'open/menu']
      },
      {
        name: 'Payment Gateway',
        tags: ['open/payment']
      }
    ],
    auth: {
      enabled: true,
      user: 'tthai',
      password: 'qwerty1234@'
    }
  }
  await RedocModule.setup('/docs/review', app, document, redocOptions)
}
