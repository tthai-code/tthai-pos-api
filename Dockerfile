FROM node:14-alpine As development

WORKDIR /app

COPY package*.json ./

RUN npm ci --include=dev

COPY . .

RUN npm run build

EXPOSE 3001

CMD ["npm", "start"]