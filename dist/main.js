"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const helmet = require("helmet");
const compression = require("compression");
const swagger_1 = require("@nestjs/swagger");
const nestjs_redoc_1 = require("nestjs-redoc");
const express_1 = require("express");
const documentBuilder_1 = require("./utilities/documentBuilder");
const log_logic_1 = require("./modules/log/logic/log.logic");
const http_exception_filter_1 = require("./exception-filter/http-exception.filter");
const error_filter_1 = require("./exception-filter/error.filter");
async function documentBuilder(app) {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('POS service api')
        .setDescription('POS service api.')
        .setVersion('1.0')
        .addBearerAuth()
        .addBasicAuth()
        .addOAuth2()
        .addApiKey()
        .addCookieAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    const redocOptions = {
        title: 'POS service api',
        docName: 'POS-api',
        sortPropsAlphabetically: true,
        pathInMiddlePanel: true,
        hideDownloadButton: false,
        hideHostname: false,
        showExtensions: true,
        expandResponses: 'all',
        auth: {
            enabled: true,
            user: 'admin',
            password: 'qwerty1234@'
        }
    };
    await nestjs_redoc_1.RedocModule.setup('/docs', app, document, redocOptions);
}
async function documentPOSBuilder(app) {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('POS service api')
        .setDescription('POS service api.')
        .setVersion('1.0')
        .addBearerAuth()
        .addBasicAuth()
        .addOAuth2()
        .addApiKey()
        .addCookieAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    const redocOptions = {
        title: 'POS service api',
        docName: 'POS-api',
        sortPropsAlphabetically: true,
        pathInMiddlePanel: true,
        hideDownloadButton: false,
        hideHostname: false,
        showExtensions: true,
        expandResponses: 'all',
        tagGroups: [
            {
                name: 'Authentication',
                tags: ['auth/device', 'pos/staff', 'pos/auth']
            },
            {
                name: 'Restaurant',
                tags: ['pos/restaurant', 'pos/restaurant-shifts', 'pos/discount']
            },
            {
                name: 'Table',
                tags: ['pos/table']
            },
            {
                name: 'Menu',
                tags: ['pos/menu-category', 'pos/menu', 'pos/modifier']
            },
            {
                name: 'Customer',
                tags: ['pos/customer']
            },
            {
                name: 'Order / Kitchen',
                tags: ['pos/order', 'pos/kitchen', 'pos/online-order']
            },
            {
                name: 'Third Party Order',
                tags: ['pos/kitchen-hub']
            },
            {
                name: 'Payment Gateway & Transaction',
                tags: ['pos/payment', 'pos/transaction']
            },
            {
                name: 'Delivery Setting',
                tags: ['pos/delivery-setting']
            },
            {
                name: 'Report',
                tags: ['pos/report']
            },
            {
                name: 'Other Device',
                tags: ['pos/other-device']
            },
            {
                name: 'Cash Drawer',
                tags: ['pos/cash-drawer']
            }
        ],
        auth: {
            enabled: true,
            user: 'tthai',
            password: 'qwerty1234@'
        }
    };
    await nestjs_redoc_1.RedocModule.setup('/docs/pos', app, document, redocOptions);
}
async function bootstrap() {
    const port = process.env.PORT || 3000;
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    await documentBuilder(app);
    await documentPOSBuilder(app);
    await (0, documentBuilder_1.documentOpenBuilder)(app);
    await (0, documentBuilder_1.documentSuperAdminBuilder)(app);
    await (0, documentBuilder_1.documentKitchenBuilder)(app);
    await (0, documentBuilder_1.documentRestaurantBuilder)(app);
    await (0, documentBuilder_1.documentBuilderOnline)(app);
    await (0, documentBuilder_1.documentBuilderReview)(app);
    app.use(helmet());
    app.use(compression());
    app.enableCors();
    app.use((0, express_1.json)({ limit: '100mb' }));
    app.use((0, express_1.urlencoded)({ extended: true, limit: '100mb' }));
    const logLogic = await app.resolve(log_logic_1.LogLogic);
    app.useGlobalFilters(new error_filter_1.ErrorFilter(logLogic));
    app.useGlobalFilters(new http_exception_filter_1.HttpExceptionFilter(logLogic));
    await app.listen(port);
}
bootstrap();
//# sourceMappingURL=main.js.map