"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.decodeBase64toString = exports.stringToBase64 = void 0;
const stringToBase64 = (text) => {
    if (!text)
        return null;
    const encode = Buffer.from(text).toString('base64');
    return encode;
};
exports.stringToBase64 = stringToBase64;
const decodeBase64toString = (text) => {
    if (!text)
        return null;
    const decode = Buffer.from(text, 'base64').toString('utf-8');
    return decode;
};
exports.decodeBase64toString = decodeBase64toString;
//# sourceMappingURL=base64.js.map