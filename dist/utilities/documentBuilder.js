"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.documentBuilderReview = exports.documentBuilderOnline = exports.documentRestaurantBuilder = exports.documentKitchenBuilder = exports.documentSuperAdminBuilder = exports.documentOpenBuilder = void 0;
const swagger_1 = require("@nestjs/swagger");
const nestjs_redoc_1 = require("nestjs-redoc");
async function documentOpenBuilder(app) {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('TThai Open API')
        .setDescription('TThai Open API.')
        .setVersion('1.0')
        .addBearerAuth()
        .addBasicAuth()
        .addOAuth2()
        .addApiKey()
        .addCookieAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    const redocOptions = {
        title: 'TThai Open API',
        docName: 'tthai-open-api',
        sortPropsAlphabetically: true,
        pathInMiddlePanel: true,
        hideDownloadButton: false,
        hideHostname: false,
        showExtensions: true,
        expandResponses: 'all',
        tagGroups: [
            {
                name: 'Restaurant',
                tags: ['open/restaurant']
            },
            {
                name: 'Payment Gateway',
                tags: ['open/payment']
            },
            {
                name: 'Web Setting',
                tags: ['online/web-setting']
            }
        ],
        auth: {
            enabled: true,
            user: 'tthai-open',
            password: 'qwerty1234@'
        }
    };
    await nestjs_redoc_1.RedocModule.setup('/docs/open', app, document, redocOptions);
}
exports.documentOpenBuilder = documentOpenBuilder;
async function documentSuperAdminBuilder(app) {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('TThai Super Admin API')
        .setDescription('TThai Super Admin API.')
        .setVersion('1.0')
        .addBearerAuth()
        .addBasicAuth()
        .addOAuth2()
        .addApiKey()
        .addCookieAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    const redocOptions = {
        title: 'TThai Super Admin API',
        docName: 'tthai-super-api',
        sortPropsAlphabetically: true,
        pathInMiddlePanel: true,
        hideDownloadButton: false,
        hideHostname: false,
        showExtensions: true,
        expandResponses: 'all',
        tagGroups: [
            {
                name: 'Auth',
                tags: ['public/user']
            },
            {
                name: 'User Management',
                tags: ['users-admin']
            },
            {
                name: 'Restaurant Management',
                tags: [
                    'admin/restaurant',
                    'admin/subscription',
                    'admin/payment-gateway'
                ]
            },
            {
                name: 'Report',
                tags: ['admin/report']
            },
            {
                name: 'Package Management',
                tags: ['admin/package']
            },
            {
                name: 'Billing',
                tags: ['admin/billing']
            }
        ]
    };
    await nestjs_redoc_1.RedocModule.setup('/docs/admin', app, document, redocOptions);
}
exports.documentSuperAdminBuilder = documentSuperAdminBuilder;
async function documentKitchenBuilder(app) {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('TThai Kitchen Hub API')
        .setDescription('TThai Kitchen Hub API.')
        .setVersion('1.0')
        .addBearerAuth()
        .addBasicAuth()
        .addOAuth2()
        .addApiKey()
        .addCookieAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    const redocOptions = {
        title: 'TThai Kitchen Hub API',
        docName: 'tthai-kitchen-hub-api',
        sortPropsAlphabetically: true,
        pathInMiddlePanel: true,
        hideDownloadButton: false,
        hideHostname: false,
        showExtensions: true,
        expandResponses: 'all',
        tagGroups: [
            {
                name: 'Kitchen Hub',
                tags: ['open/kitchen-hub']
            }
        ]
    };
    await nestjs_redoc_1.RedocModule.setup('/docs/kitchen-hub', app, document, redocOptions);
}
exports.documentKitchenBuilder = documentKitchenBuilder;
async function documentRestaurantBuilder(app) {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('TThai Web Base API')
        .setDescription('TThai Web Base API.')
        .setVersion('1.0')
        .addBearerAuth()
        .addBasicAuth()
        .addOAuth2()
        .addApiKey()
        .addCookieAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    const redocOptions = {
        title: 'TThai Web Base API',
        docName: 'tthai-web-base-api',
        sortPropsAlphabetically: true,
        pathInMiddlePanel: true,
        hideDownloadButton: false,
        hideHostname: false,
        showExtensions: true,
        expandResponses: 'all',
        tagGroups: [
            {
                name: 'Restaurant Account',
                tags: ['restaurant-account', 'public/account']
            },
            {
                name: 'Restaurant',
                tags: ['restaurant', 'restaurant-hours', 'restaurant-holidays']
            },
            {
                name: 'Device',
                tags: ['device', 'other-device']
            },
            {
                name: 'Payment Gateway',
                tags: ['payment-gateway-setting']
            },
            {
                name: 'Staff',
                tags: ['staff']
            },
            {
                name: 'Menu',
                tags: ['menu', 'menu-modifier', 'menu-category']
            },
            {
                name: 'Table & Zone',
                tags: ['table', 'table-zone']
            },
            {
                name: 'Restaurant Setting',
                tags: [
                    'delivery-provider',
                    'third-party-delivery',
                    'online-receipt-setting',
                    'tthai-app-setting',
                    'catering-setting',
                    'web-setting'
                ]
            },
            {
                name: 'Bank Account',
                tags: ['bank-account']
            },
            {
                name: 'Transaction',
                tags: ['transaction', 'cash-drawer']
            },
            {
                name: 'Subscription',
                tags: ['subscription', 'package']
            },
            {
                name: 'Report',
                tags: ['report']
            },
            {
                name: 'Upload',
                tags: ['uploads']
            }
        ]
    };
    await nestjs_redoc_1.RedocModule.setup('/docs/restaurant', app, document, redocOptions);
}
exports.documentRestaurantBuilder = documentRestaurantBuilder;
async function documentBuilderOnline(app) {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('POS service api')
        .setDescription('POS service api.')
        .setVersion('1.0')
        .addBearerAuth()
        .addBasicAuth()
        .addOAuth2()
        .addApiKey()
        .addCookieAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    const redocOptions = {
        title: 'POS service api',
        docName: 'Online-api',
        sortPropsAlphabetically: true,
        pathInMiddlePanel: true,
        hideDownloadButton: false,
        hideHostname: false,
        showExtensions: true,
        expandResponses: 'all',
        tagGroups: [
            {
                name: 'Customer Online',
                tags: ['customer-online']
            },
            {
                name: 'Order Online',
                tags: ['public/order-hour', 'order-online']
            },
            {
                name: 'Menu Public Online',
                tags: ['menu-public-online']
            },
            {
                name: 'Menu Category Public Online',
                tags: ['menu-category-public-online']
            },
            {
                name: 'Menu Modifier Public Online',
                tags: ['menu-modifier-online']
            },
            {
                name: 'Web Setting',
                tags: ['online/web-setting']
            }
        ],
        auth: {
            enabled: true,
            user: 'admin',
            password: 'qwerty1234@'
        }
    };
    await nestjs_redoc_1.RedocModule.setup('/docs/online', app, document, redocOptions);
}
exports.documentBuilderOnline = documentBuilderOnline;
async function documentBuilderReview(app) {
    const options = new swagger_1.DocumentBuilder()
        .setTitle('TThai Review service api')
        .setDescription('TThai Review service api.')
        .setVersion('1.0')
        .addBearerAuth()
        .addBasicAuth()
        .addOAuth2()
        .addApiKey()
        .addCookieAuth()
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, options);
    const redocOptions = {
        title: 'TThai Review service api',
        docName: 'TThai-review-api',
        sortPropsAlphabetically: true,
        pathInMiddlePanel: true,
        hideDownloadButton: false,
        hideHostname: false,
        showExtensions: true,
        expandResponses: 'all',
        tagGroups: [
            {
                name: 'Restaurant',
                tags: ['open/restaurant', 'open/eats-zaap']
            },
            {
                name: 'Menu & Category',
                tags: ['open/category', 'open/menu']
            },
            {
                name: 'Payment Gateway',
                tags: ['open/payment']
            }
        ],
        auth: {
            enabled: true,
            user: 'tthai',
            password: 'qwerty1234@'
        }
    };
    await nestjs_redoc_1.RedocModule.setup('/docs/review', app, document, redocOptions);
}
exports.documentBuilderReview = documentBuilderReview;
//# sourceMappingURL=documentBuilder.js.map