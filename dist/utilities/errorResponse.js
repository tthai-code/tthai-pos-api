"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorResponse = void 0;
const errorResponse = (statusCode, message, path) => {
    return {
        schema: {
            type: 'object',
            properties: {
                statusCode: { type: 'number', example: statusCode },
                message: { type: 'string', example: message },
                path: { type: 'string', example: path },
                timestamp: { type: 'string', example: new Date().toISOString() }
            }
        }
    };
};
exports.errorResponse = errorResponse;
//# sourceMappingURL=errorResponse.js.map