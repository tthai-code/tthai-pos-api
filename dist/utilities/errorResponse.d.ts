export declare const errorResponse: (statusCode: number, message: string, path: string) => {
    schema: {
        type: string;
        properties: {
            statusCode: {
                type: string;
                example: number;
            };
            message: {
                type: string;
                example: string;
            };
            path: {
                type: string;
                example: string;
            };
            timestamp: {
                type: string;
                example: string;
            };
        };
    };
};
