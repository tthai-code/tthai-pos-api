export declare function documentOpenBuilder(app: any): Promise<void>;
export declare function documentSuperAdminBuilder(app: any): Promise<void>;
export declare function documentKitchenBuilder(app: any): Promise<void>;
export declare function documentRestaurantBuilder(app: any): Promise<void>;
export declare function documentBuilderOnline(app: any): Promise<void>;
export declare function documentBuilderReview(app: any): Promise<void>;
