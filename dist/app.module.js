"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const core_1 = require("@nestjs/core");
const mongoose_1 = require("@nestjs/mongoose");
const app_controller_1 = require("./app.controller");
const error_filter_1 = require("./exception-filter/error.filter");
const http_exception_filter_1 = require("./exception-filter/http-exception.filter");
const mongoose_transformer_1 = require("./interceptors/mongoose.transformer");
const transfromer_interceptor_1 = require("./interceptors/transfromer.interceptor");
const auth_module_1 = require("./modules/auth/auth.module");
const database_service_1 = require("./modules/database/database.service");
const mongoose_config_1 = require("./modules/database/mongoose.config");
const counter_schema_1 = require("./modules/database/schemas/counter.schema");
const log_module_1 = require("./modules/log/log.module");
const order_module_1 = require("./modules/order/order.module");
const upload_module_1 = require("./modules/upload/upload.module");
const menu_module_1 = require("./modules/menu/menu.module");
const menu_categories_module_1 = require("./modules/menu-category/menu-categories.module");
const restaurant_module_1 = require("./modules/restaurant/restaurant.module");
const user_module_1 = require("./modules/user/user.module");
const camelize_key_pipe_1 = require("./pipe/camelize-key.pipe");
const custom_validation_pipe_1 = require("./pipe/custom-validation.pipe");
const restaurant_account_module_1 = require("./modules/restaurant-account/restaurant-account.module");
const staff_module_1 = require("./modules/staff/staff.module");
const modifier_module_1 = require("./modules/modifier/modifier.module");
const menu_modifier_module_1 = require("./modules/menu-modifier/menu-modifier.module");
const table_zone_module_1 = require("./modules/table-zone/table-zone.module");
const table_module_1 = require("./modules/table/table.module");
const restaurant_hours_module_1 = require("./modules/restaurant-hours/restaurant-hours.module");
const restaurant_holiday_module_1 = require("./modules/restaurant-holiday/restaurant-holiday.module");
const device_module_1 = require("./modules/device/device.module");
const payment_gateway_module_1 = require("./modules/payment-gateway/payment-gateway.module");
const third_party_delivery_module_1 = require("./modules/third-party-delivery/third-party-delivery.module");
const oniine_receipt_module_1 = require("./modules/online-receipt/oniine.receipt.module");
const catering_module_1 = require("./modules/catering/catering.module");
const web_setting_module_1 = require("./modules/web-setting/web-setting.module");
const tthai_app_module_1 = require("./modules/tthai-app/tthai-app.module");
const customer_module_1 = require("./modules/customer/customer.module");
const socket_module_1 = require("./modules/socket/socket.module");
const transaction_module_1 = require("./modules/transaction/transaction.module");
const kitchen_hub_module_1 = require("./modules/kitchen-hub/kitchen-hub.module");
const report_module_1 = require("./modules/report/report.module");
const subscription_module_1 = require("./modules/subscription/subscription.module");
const bill_module_1 = require("./modules/bill/bill.module");
const cash_drawer_module_1 = require("./modules/cash-drawer/cash-drawer.module");
const discount_module_1 = require("./modules/discount/discount.module");
const mailer_module_1 = require("./modules/mailer/mailer.module");
const log_logic_1 = require("./modules/log/logic/log.logic");
const restaurant_shifts_module_1 = require("./modules/restaurant-shifts/restaurant-shifts.module");
const AppProviders = [
    log_logic_1.LogLogic,
    {
        provide: core_1.APP_PIPE,
        useClass: camelize_key_pipe_1.CamelizeKeysPipe
    },
    {
        provide: core_1.APP_PIPE,
        useClass: custom_validation_pipe_1.CustomValidationPipe
    },
    {
        provide: core_1.APP_FILTER,
        useClass: error_filter_1.ErrorFilter
    },
    {
        provide: core_1.APP_FILTER,
        useClass: http_exception_filter_1.HttpExceptionFilter
    },
    {
        provide: core_1.APP_INTERCEPTOR,
        useClass: transfromer_interceptor_1.TransformInterceptor
    },
    database_service_1.DatabaseService,
    mongoose_transformer_1.MongooseTransformers
];
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot(),
            mongoose_1.MongooseModule.forRootAsync({
                useClass: mongoose_config_1.MongooseConfigService
            }),
            mongoose_1.MongooseModule.forFeature([{ name: 'Counter', schema: counter_schema_1.CounterSchema }]),
            auth_module_1.AuthModule,
            restaurant_module_1.RestaurantModule,
            restaurant_account_module_1.RestaurantAccountModule,
            restaurant_hours_module_1.RestaurantHoursModule,
            restaurant_holiday_module_1.RestaurantHolidaysModule,
            staff_module_1.StaffModule,
            user_module_1.UserModule,
            device_module_1.DeviceModule,
            menu_categories_module_1.MenuCategoriesModule,
            modifier_module_1.ModifierModule,
            menu_module_1.MenuModule,
            menu_modifier_module_1.MenuModifierModule,
            table_zone_module_1.TableZoneModule,
            table_module_1.TableModule,
            payment_gateway_module_1.PaymentGatewayModule,
            third_party_delivery_module_1.ThirdPartyDeliveryModule,
            tthai_app_module_1.TThaiAppModule,
            oniine_receipt_module_1.OnlineReceiptModule,
            catering_module_1.CateringModule,
            web_setting_module_1.WebSettingModule,
            order_module_1.OrderModule,
            customer_module_1.CustomerModule,
            upload_module_1.UploadModule,
            log_module_1.LogModule,
            transaction_module_1.TransactionModule,
            kitchen_hub_module_1.KitchenHubModule,
            socket_module_1.SocketModule,
            subscription_module_1.SubscriptionModule,
            report_module_1.ReportModule,
            bill_module_1.BillModule,
            cash_drawer_module_1.CashDrawerModule,
            discount_module_1.DiscountModule,
            mailer_module_1.POSMailerModule,
            restaurant_shifts_module_1.RestaurantShiftsModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [...AppProviders],
        exports: []
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map