"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongooseTransformers = void 0;
const common_1 = require("@nestjs/common");
const humps = require("humps");
const util_1 = require("util");
let MongooseTransformers = class MongooseTransformers {
    paginationTransformer(data) {
        data.docs.forEach((doc, index) => {
            data.docs[index] = JSON.parse(JSON.stringify(doc.toObject ? doc.toObject() : doc));
        });
        const response = {
            message: 'done',
            data: Object.assign(Object.assign({}, data), { results: humps.decamelizeKeys(data.docs) })
        };
        delete response.data.docs;
        return response;
    }
    documentTransformer(data) {
        const response = {
            message: 'done',
            data: humps.decamelizeKeys(JSON.parse(JSON.stringify(data.toObject())))
        };
        return response;
    }
    arrayTransformer(data) {
        data.forEach((doc, index) => {
            if (typeof doc.toObject == 'undefined') {
                const stringDoc = JSON.stringify(doc).replace(/_id/g, 'id');
                data[index] = JSON.parse(stringDoc);
            }
            else {
                data[index] = JSON.parse(JSON.stringify(doc.toObject()));
            }
        });
        const response = {
            message: 'done',
            data: humps.decamelizeKeys(data)
        };
        return response;
    }
    arrayAggegeteTransfromer(data) {
        if (typeof data.results.toObject == 'undefined') {
            const stringDoc = JSON.stringify(data).replace(/_id/g, 'id');
            data = JSON.parse(stringDoc);
        }
        const response = {
            message: 'done',
            data: humps.decamelizeKeys(data)
        };
        return response;
    }
    objectArrayTransfromer(data) {
        const objectKey = data ? Object.keys(data) : [];
        for (const key of objectKey) {
            if ((0, util_1.isArray)(data[key])) {
                data[key].forEach((doc, index) => {
                    if (typeof doc.toObject == 'undefined') {
                        const stringDoc = JSON.stringify(doc).replace(/_id/g, 'id');
                        data[key][index] = JSON.parse(stringDoc);
                    }
                    else {
                        data[key][index] = doc._id ? doc.toObject() : doc;
                    }
                });
            }
        }
        const response = {
            message: 'done',
            data: humps.decamelizeKeys(data)
        };
        return response;
    }
    tokenTransformer(data) {
        const _a = JSON.parse(JSON.stringify(data)), { expiresIn, accessToken } = _a, information = __rest(_a, ["expiresIn", "accessToken"]);
        const response = {
            message: 'done',
            data: information,
            expiresIn,
            accessToken
        };
        return humps.decamelizeKeys(response);
    }
    defaultTransformer(data) {
        for (const key in data) {
            if ((0, util_1.isArray)(data[key])) {
                data[key].forEach((doc, index) => {
                    if (typeof doc.toObject == 'undefined') {
                        const stringDoc = JSON.stringify(doc).replace(/_id/g, 'id');
                        data[key][index] = JSON.parse(stringDoc);
                    }
                    else {
                        data[key][index] = doc._id ? doc.toObject() : doc;
                    }
                });
            }
            else if (!data[key]) {
                data[key] = data[key];
            }
            else if (data[key]._id) {
                if (typeof data[key].toObject == 'undefined') {
                    const stringDoc = JSON.stringify(data[key]).replace(/_id/g, 'id');
                    data[key] = JSON.parse(stringDoc);
                }
                else {
                    data[key] = data[key]._id ? data[key].toObject() : data[key];
                }
            }
        }
        const response = {
            message: 'done',
            data: humps.decamelizeKeys(data)
        };
        return response;
    }
};
MongooseTransformers = __decorate([
    (0, common_1.Injectable)()
], MongooseTransformers);
exports.MongooseTransformers = MongooseTransformers;
//# sourceMappingURL=mongoose.transformer.js.map