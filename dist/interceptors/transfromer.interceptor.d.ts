import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { MongooseTransformers } from './mongoose.transformer';
export declare class TransformInterceptor implements NestInterceptor {
    private readonly mongooseTransformers;
    constructor(mongooseTransformers: MongooseTransformers);
    intercept(context: ExecutionContext, next: CallHandler): Observable<any>;
}
