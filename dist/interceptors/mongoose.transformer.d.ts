export declare class MongooseTransformers {
    paginationTransformer(data: any): object;
    documentTransformer(data: any): object;
    arrayTransformer(data: any): object;
    arrayAggegeteTransfromer(data: any): object;
    objectArrayTransfromer(data: any): object;
    tokenTransformer(data: any): object;
    defaultTransformer(data: any): object;
}
