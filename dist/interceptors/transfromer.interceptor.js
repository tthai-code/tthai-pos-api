"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransformInterceptor = void 0;
const common_1 = require("@nestjs/common");
const operators_1 = require("rxjs/operators");
const mongoose_transformer_1 = require("./mongoose.transformer");
const util_1 = require("util");
let TransformInterceptor = class TransformInterceptor {
    constructor(mongooseTransformers) {
        this.mongooseTransformers = mongooseTransformers;
    }
    intercept(context, next) {
        return next.handle().pipe((0, operators_1.map)((data) => {
            const req = context.switchToHttp().getRequest();
            let response;
            const objectKey = data ? Object.keys(data) : [];
            if (data && data.docs) {
                response = this.mongooseTransformers.paginationTransformer(data);
            }
            else if (data && data._id) {
                response = this.mongooseTransformers.documentTransformer(data);
            }
            else if (data && (0, util_1.isArray)(data)) {
                response = this.mongooseTransformers.arrayTransformer(data);
            }
            else if (data && data.accessToken) {
                response = this.mongooseTransformers.tokenTransformer(data);
            }
            else if (data && data.results) {
                response = this.mongooseTransformers.arrayAggegeteTransfromer(data);
            }
            else if (data && (0, util_1.isArray)(data[objectKey[0]])) {
                response = this.mongooseTransformers.objectArrayTransfromer(data);
            }
            else {
                response = this.mongooseTransformers.defaultTransformer(data);
            }
            if (!response.data || !response.data.redirect) {
                req.res.header('X-Build-Number', process.env.BUILDNUMBER);
            }
            return response;
        }));
    }
};
TransformInterceptor = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [mongoose_transformer_1.MongooseTransformers])
], TransformInterceptor);
exports.TransformInterceptor = TransformInterceptor;
//# sourceMappingURL=transfromer.interceptor.js.map