import dayjs from 'src/plugins/dayjs';
export declare class AppController {
    getHello(): {
        date: Date;
        status: string;
    };
    getTimeZone(query: any): Promise<{
        nowFormat: string;
        timeZone: string;
        nowLocale: string;
        now: string;
        ISODate: string;
        dayjs: dayjs.Dayjs;
        UTCDayjs: dayjs.Dayjs;
        timeZoneDayjs: dayjs.Dayjs;
        dayjsFormat: string;
        UTCDayjsFormat: string;
        timeZoneDayjsFormat: string;
        timeZoneString: string;
        dayOfWeekString: any;
    }>;
}
