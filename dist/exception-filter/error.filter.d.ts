import { ExceptionFilter, ArgumentsHost, HttpStatus } from '@nestjs/common';
import { LogLogic } from 'src/modules/log/logic/log.logic';
export declare class ErrorFilter implements ExceptionFilter {
    private readonly logLogic;
    protected context: any;
    protected message: any;
    protected code: HttpStatus;
    constructor(logLogic: LogLogic);
    catch(error: Error, host: ArgumentsHost): Promise<void>;
    protected prepare(error: any): void;
}
