import { ExceptionFilter, ArgumentsHost, HttpException } from '@nestjs/common';
import { LogLogic } from 'src/modules/log/logic/log.logic';
export declare class HttpExceptionFilter implements ExceptionFilter {
    private readonly logLogic;
    constructor(logLogic: LogLogic);
    catch(exception: HttpException, host: ArgumentsHost): Promise<void>;
}
