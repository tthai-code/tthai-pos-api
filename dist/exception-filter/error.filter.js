"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorFilter = void 0;
const common_1 = require("@nestjs/common");
const log_logic_1 = require("../modules/log/logic/log.logic");
let ErrorFilter = class ErrorFilter {
    constructor(logLogic) {
        this.logLogic = logLogic;
        this.code = common_1.HttpStatus.INTERNAL_SERVER_ERROR;
    }
    async catch(error, host) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        this.context = host.switchToHttp();
        this.message = error.message;
        this.prepare(error);
        response.status(500).json({
            message: this.message,
            statusCode: 500,
            timestamp: new Date().toISOString(),
            path: request.url
        });
    }
    prepare(error) {
        console.log(error);
    }
};
ErrorFilter = __decorate([
    (0, common_1.Catch)(),
    __metadata("design:paramtypes", [log_logic_1.LogLogic])
], ErrorFilter);
exports.ErrorFilter = ErrorFilter;
//# sourceMappingURL=error.filter.js.map