import { PipeTransform, ArgumentMetadata } from '@nestjs/common';
export declare class CamelizeKeysPipe implements PipeTransform<any> {
    transform(value: any, metadata: ArgumentMetadata): any;
}
