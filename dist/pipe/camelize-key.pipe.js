"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CamelizeKeysPipe = void 0;
const common_1 = require("@nestjs/common");
const humps = require("humps");
let CamelizeKeysPipe = class CamelizeKeysPipe {
    transform(value, metadata) {
        if (metadata.type !== 'param') {
            value = humps.camelizeKeys(value, (key, convert) => {
                return /^(_|[A-Z]+).*$/.test(key) ? key : convert(key);
            });
        }
        return value;
    }
};
CamelizeKeysPipe = __decorate([
    (0, common_1.Injectable)()
], CamelizeKeysPipe);
exports.CamelizeKeysPipe = CamelizeKeysPipe;
//# sourceMappingURL=camelize-key.pipe.js.map