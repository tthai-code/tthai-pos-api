"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomValidationPipe = void 0;
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
const class_transformer_1 = require("class-transformer");
let CustomValidationPipe = class CustomValidationPipe {
    async transform(value, metadata) {
        const { metatype, type } = metadata;
        if (type === 'body' && value instanceof Object && this.isEmpty(value)) {
            throw new common_1.BadRequestException('No body submitted');
        }
        if (!metatype || !this.toValidate(metatype)) {
            return value;
        }
        const object = (0, class_transformer_1.plainToClass)(metatype, value);
        const errors = await (0, class_validator_1.validate)(object, { whitelist: true });
        if (errors.length > 0) {
            throw new common_1.BadRequestException(this.formatErrors(errors));
        }
        return object;
    }
    toValidate(metatype) {
        const types = [String, Boolean, Number, Array, Object];
        return !types.includes(metatype);
    }
    formatErrors(errors) {
        return errors.map((err) => {
            const lastChild = this.checkChild(err);
            for (const property in lastChild.constraints) {
                if (lastChild.constraints.hasOwnProperty(property)) {
                    return lastChild.constraints[property];
                }
            }
        });
    }
    checkChild(obj) {
        if ((obj.children && obj.children.length === 0) || !obj.children) {
            return obj;
        }
        return this.checkChild(obj.children[0]);
    }
    isEmpty(value) {
        if (Object.keys(value).length > 0) {
            return false;
        }
        return true;
    }
};
CustomValidationPipe = __decorate([
    (0, common_1.Injectable)()
], CustomValidationPipe);
exports.CustomValidationPipe = CustomValidationPipe;
//# sourceMappingURL=custom-validation.pipe.js.map