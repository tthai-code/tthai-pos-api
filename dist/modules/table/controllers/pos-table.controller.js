"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSTableController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const errorResponse_1 = require("../../../utilities/errorResponse");
const get_all_table_dto_1 = require("../dto/get-all-table.dto");
const table_entity_1 = require("../entity/table.entity");
const table_logic_1 = require("../logics/table.logic");
let POSTableController = class POSTableController {
    constructor(tableLogic) {
        this.tableLogic = tableLogic;
    }
    async getAllTables(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.tableLogic.GetAllTable(query.buildQuery(restaurantId), restaurantId);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({
        summary: 'Get All Table for Display on POS',
        description: 'use *bearer* `staff_token`'
    }),
    (0, swagger_1.ApiOkResponse)({ type: () => table_entity_1.POSTableResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', 'v1/pos/table')),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_all_table_dto_1.AllTableDto]),
    __metadata("design:returntype", Promise)
], POSTableController.prototype, "getAllTables", null);
POSTableController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/table'),
    (0, common_1.Controller)('v1/pos/table'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    __metadata("design:paramtypes", [table_logic_1.TableLogic])
], POSTableController);
exports.POSTableController = POSTableController;
//# sourceMappingURL=pos-table.controller.js.map