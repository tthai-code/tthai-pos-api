"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const create_table_dto_1 = require("../dto/create-table.dto");
const get_table_dto_1 = require("../dto/get-table.dto");
const update_table_dto_1 = require("../dto/update-table.dto");
const table_logic_1 = require("../logics/table.logic");
const table_service_1 = require("../services/table.service");
let TableController = class TableController {
    constructor(tableService, tableLogic) {
        this.tableService = tableService;
        this.tableLogic = tableLogic;
    }
    async getTables(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const table = await this.tableService.customPaginate(query.buildQuery(restaurantId), query);
        return table;
    }
    async getTable(id) {
        return await this.tableService.findOne({ _id: id });
    }
    async createTable(payload) {
        return await this.tableLogic.CreateTable(payload);
    }
    async updateTable(id, payload) {
        return await this.tableLogic.UpdateTable(id, payload);
    }
    async deleteTable(id) {
        return await this.tableService.delete(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Get List Paginate Table' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_table_dto_1.TablePaginateDto]),
    __metadata("design:returntype", Promise)
], TableController.prototype, "getTables", null);
__decorate([
    (0, common_1.Get)(':id/table'),
    (0, swagger_1.ApiOperation)({ summary: 'Get a Table By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TableController.prototype, "getTable", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create a Table' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_table_dto_1.CreateTableDto]),
    __metadata("design:returntype", Promise)
], TableController.prototype, "createTable", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update a Table' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_table_dto_1.UpdateTableDto]),
    __metadata("design:returntype", Promise)
], TableController.prototype, "updateTable", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete a Table By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TableController.prototype, "deleteTable", null);
TableController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('table'),
    (0, common_1.Controller)('v1/table'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __metadata("design:paramtypes", [table_service_1.TableService,
        table_logic_1.TableLogic])
], TableController);
exports.TableController = TableController;
//# sourceMappingURL=table.controller.js.map