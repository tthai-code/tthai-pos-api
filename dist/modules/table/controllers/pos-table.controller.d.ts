import { AllTableDto } from '../dto/get-all-table.dto';
import { TableLogic } from '../logics/table.logic';
export declare class POSTableController {
    private readonly tableLogic;
    constructor(tableLogic: TableLogic);
    getAllTables(query: AllTableDto): Promise<{
        tableZone: any;
        tables: {
            id: any;
            tableNo: any;
            maxSize: any;
            minSize: any;
            tableStatus: any;
        }[];
    }[]>;
}
