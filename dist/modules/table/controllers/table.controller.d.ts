import { CreateTableDto } from '../dto/create-table.dto';
import { TablePaginateDto } from '../dto/get-table.dto';
import { UpdateTableDto } from '../dto/update-table.dto';
import { TableLogic } from '../logics/table.logic';
import { TableService } from '../services/table.service';
export declare class TableController {
    private readonly tableService;
    private readonly tableLogic;
    constructor(tableService: TableService, tableLogic: TableLogic);
    getTables(query: TablePaginateDto): Promise<PaginateResult<import("../models/table.schema").TableDocument>>;
    getTable(id: string): Promise<import("../models/table.schema").TableDocument>;
    createTable(payload: CreateTableDto): Promise<import("../models/table.schema").TableDocument>;
    updateTable(id: string, payload: UpdateTableDto): Promise<import("../models/table.schema").TableDocument>;
    deleteTable(id: string): Promise<import("../models/table.schema").TableDocument>;
}
