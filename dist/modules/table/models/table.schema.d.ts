import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type TableDocument = TableFields & Document;
export declare class TableZoneInTableFields {
    id: string;
    name: string;
}
export declare class TableFields {
    restaurantId: Types.ObjectId;
    tableNo: string;
    tableZone: TableZoneInTableFields;
    minSize: number;
    maxSize: number;
    tableStatus: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const TableSchema: MongooseSchema<Document<TableFields, any, any>, import("mongoose").Model<Document<TableFields, any, any>, any, any, any>, any, any>;
