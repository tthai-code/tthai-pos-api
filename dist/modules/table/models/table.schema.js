"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableSchema = exports.TableFields = exports.TableZoneInTableFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const table_status_enum_1 = require("../common/table-status.enum");
let TableZoneInTableFields = class TableZoneInTableFields {
};
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], TableZoneInTableFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], TableZoneInTableFields.prototype, "name", void 0);
TableZoneInTableFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: false, strict: true })
], TableZoneInTableFields);
exports.TableZoneInTableFields = TableZoneInTableFields;
let TableFields = class TableFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], TableFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], TableFields.prototype, "tableNo", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", TableZoneInTableFields)
], TableFields.prototype, "tableZone", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", Number)
], TableFields.prototype, "minSize", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", Number)
], TableFields.prototype, "maxSize", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(table_status_enum_1.TableStatusEnum),
        default: table_status_enum_1.TableStatusEnum.AVAILABLE
    }),
    __metadata("design:type", String)
], TableFields.prototype, "tableStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], TableFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], TableFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], TableFields.prototype, "createdBy", void 0);
TableFields = __decorate([
    (0, mongoose_1.Schema)({
        timestamps: true,
        strict: true,
        collection: 'tables',
        versionKey: false
    })
], TableFields);
exports.TableFields = TableFields;
exports.TableSchema = mongoose_1.SchemaFactory.createForClass(TableFields);
exports.TableSchema.plugin(mongoosePaginate);
exports.TableSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=table.schema.js.map