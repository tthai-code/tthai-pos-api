export declare class CreateTableDto {
    tableZone: any;
    readonly restaurantId: string;
    readonly tableNo: string;
    readonly tableZoneId: string;
    readonly minSize: number;
    readonly maxSize: number;
    readonly tableStatus: string;
    readonly status: string;
}
