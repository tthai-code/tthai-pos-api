import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class TablePaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly zone: string;
    readonly restaurant: string;
    readonly status: string;
    buildQuery(id: string): {
        $or: {
            tableNo: {
                $regex: string;
                $options: string;
            };
        }[];
        'tableZone.id': {
            $eq: string;
        };
        restaurantId: {
            $eq: string;
        };
        status: string | {
            $ne: StatusEnum;
        };
    };
}
