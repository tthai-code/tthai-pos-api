export declare class UpdateTableDto {
    tableZone: any;
    readonly tableNo: string;
    readonly tableZoneId: string;
    readonly minSize: number;
    readonly maxSize: number;
    readonly tableStatus: string;
    readonly status: string;
}
