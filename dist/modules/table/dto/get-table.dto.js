"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TablePaginateDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
class TablePaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'tableNo';
        this.sortOrder = 'asc';
        this.search = '';
        this.zone = '';
    }
    buildQuery(id) {
        const result = {
            $or: [
                {
                    tableNo: {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            'tableZone.id': {
                $eq: this.zone
            },
            restaurantId: { $eq: !id ? this.restaurant : id },
            status: this.status || { $ne: status_enum_1.StatusEnum.DELETED }
        };
        if (!id && !this.restaurant)
            delete result.restaurantId;
        if (!this.zone)
            delete result['tableZone.id'];
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'tableNo' }),
    __metadata("design:type", String)
], TablePaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'asc' }),
    __metadata("design:type", String)
], TablePaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'test' }),
    __metadata("design:type", String)
], TablePaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '<table-zone-id>' }),
    __metadata("design:type", String)
], TablePaginateDto.prototype, "zone", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '<restaurant-id>' }),
    __metadata("design:type", String)
], TablePaginateDto.prototype, "restaurant", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: status_enum_1.StatusEnum.ACTIVE,
        enum: Object.values(status_enum_1.StatusEnum)
    }),
    __metadata("design:type", String)
], TablePaginateDto.prototype, "status", void 0);
exports.TablePaginateDto = TablePaginateDto;
//# sourceMappingURL=get-table.dto.js.map