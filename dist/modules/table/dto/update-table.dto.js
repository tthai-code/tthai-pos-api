"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateTableDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const status_enum_1 = require("../../../common/enum/status.enum");
const table_status_enum_1 = require("../common/table-status.enum");
class UpdateTableDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '1' }),
    __metadata("design:type", String)
], UpdateTableDto.prototype, "tableNo", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '<table-zone-id>' }),
    __metadata("design:type", String)
], UpdateTableDto.prototype, "tableZoneId", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 1 }),
    __metadata("design:type", Number)
], UpdateTableDto.prototype, "minSize", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 3 }),
    __metadata("design:type", Number)
], UpdateTableDto.prototype, "maxSize", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEnum)(table_status_enum_1.TableStatusEnum),
    (0, swagger_1.ApiPropertyOptional)({
        example: table_status_enum_1.TableStatusEnum.AVAILABLE,
        enum: Object.values(table_status_enum_1.TableStatusEnum)
    }),
    __metadata("design:type", String)
], UpdateTableDto.prototype, "tableStatus", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEnum)(status_enum_1.StatusEnum),
    (0, swagger_1.ApiPropertyOptional)({
        example: status_enum_1.StatusEnum.ACTIVE,
        enum: Object.values(status_enum_1.StatusEnum)
    }),
    __metadata("design:type", String)
], UpdateTableDto.prototype, "status", void 0);
exports.UpdateTableDto = UpdateTableDto;
//# sourceMappingURL=update-table.dto.js.map