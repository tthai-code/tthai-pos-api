import { StatusEnum } from 'src/common/enum/status.enum';
export declare class AllTableDto {
    readonly tableStatus: string;
    buildQuery(id: string): {
        restaurantId: string;
        status: StatusEnum;
        tableStatus: string;
    };
}
