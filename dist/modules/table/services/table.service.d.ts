import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { TableDocument } from '../models/table.schema';
import { TablePaginateDto } from '../dto/get-table.dto';
export declare class TableService {
    private readonly TableModel;
    private request;
    constructor(TableModel: PaginateModel<TableDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<TableDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<TableDocument>;
    getSession(): Promise<ClientSession>;
    create(payload: any): Promise<TableDocument>;
    update(id: string, payload: any): Promise<TableDocument>;
    transactionUpdate(id: string, payload: any, session: ClientSession): Promise<TableDocument>;
    delete(id: string): Promise<TableDocument>;
    getAll(condition: any, project?: any, options?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<TableDocument>;
    paginate(query: any, queryParam: TablePaginateDto): Promise<PaginateResult<TableDocument>>;
    customPaginate(query: any, queryParam: TablePaginateDto, select?: any): Promise<PaginateResult<TableDocument>>;
    findOneForOrder(id: string): Promise<TableDocument>;
}
