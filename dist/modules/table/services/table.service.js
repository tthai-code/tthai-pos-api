"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableService = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const status_enum_1 = require("../../../common/enum/status.enum");
const table_status_enum_1 = require("../common/table-status.enum");
let TableService = class TableService {
    constructor(TableModel, request) {
        this.TableModel = TableModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.TableModel.findById({ _id: id });
    }
    async isExists(condition) {
        const result = await this.TableModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.TableModel;
    }
    async getSession() {
        await this.TableModel.createCollection();
        return this.TableModel.startSession();
    }
    async create(payload) {
        const document = new this.TableModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async update(id, payload) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, payload)).save();
    }
    async transactionUpdate(id, payload, session) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, payload)).save({ session });
    }
    async delete(id) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set({ status: status_enum_1.StatusEnum.DELETED }).save();
    }
    getAll(condition, project, options) {
        return this.TableModel.find(condition, project, options);
    }
    findById(id) {
        return this.TableModel.findById(id);
    }
    findOne(condition, project) {
        return this.TableModel.findOne(condition, project);
    }
    paginate(query, queryParam) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder }
        };
        return this.TableModel.paginate(query, options);
    }
    async customPaginate(query, queryParam, select) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder },
            select
        };
        const allDocs = await this.TableModel.find(query, select);
        const docs = await this.TableModel.find(query, select, {
            sort: options.sort,
            collation: { locale: 'en_US', numericOrdering: true },
            limit: options.limit,
            skip: (options.page - 1) * options.limit
        });
        return {
            docs,
            total: allDocs.length,
            limit: options.limit,
            page: options.page,
            pages: Math.ceil(allDocs.length / options.page)
        };
    }
    findOneForOrder(id) {
        return this.TableModel.findOne({
            _id: id,
            tableStatus: table_status_enum_1.TableStatusEnum.AVAILABLE,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { _id: 0, id: '$_id', tableNo: 1 }).lean();
    }
};
TableService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('table')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], TableService);
exports.TableService = TableService;
//# sourceMappingURL=table.service.js.map