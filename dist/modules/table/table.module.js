"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const table_zone_module_1 = require("../table-zone/table-zone.module");
const pos_table_controller_1 = require("./controllers/pos-table.controller");
const table_controller_1 = require("./controllers/table.controller");
const table_logic_1 = require("./logics/table.logic");
const table_schema_1 = require("./models/table.schema");
const table_service_1 = require("./services/table.service");
let TableModule = class TableModule {
};
TableModule = __decorate([
    (0, common_1.Module)({
        imports: [
            table_zone_module_1.TableZoneModule,
            mongoose_1.MongooseModule.forFeature([{ name: 'table', schema: table_schema_1.TableSchema }])
        ],
        providers: [table_service_1.TableService, table_logic_1.TableLogic],
        controllers: [table_controller_1.TableController, pos_table_controller_1.POSTableController],
        exports: [table_service_1.TableService]
    })
], TableModule);
exports.TableModule = TableModule;
//# sourceMappingURL=table.module.js.map