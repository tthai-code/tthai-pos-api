"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableStatusEnum = void 0;
var TableStatusEnum;
(function (TableStatusEnum) {
    TableStatusEnum["AVAILABLE"] = "AVAILABLE";
    TableStatusEnum["RESERVED"] = "RESERVED";
    TableStatusEnum["SEATED"] = "SEATED";
    TableStatusEnum["DISABLED"] = "DISABLED";
})(TableStatusEnum = exports.TableStatusEnum || (exports.TableStatusEnum = {}));
//# sourceMappingURL=table-status.enum.js.map