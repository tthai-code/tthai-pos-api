import { TableStatusEnum } from './table-status.enum';
export declare const getAllTableResponse: {
    schema: {
        type: string;
        properties: {
            message: {
                type: string;
                example: string;
            };
            data: {
                type: string;
                example: {
                    table_zone: string;
                    tables: {
                        id: string;
                        table_status: string;
                        max_size: number;
                        min_size: number;
                        table_no: string;
                    }[];
                }[];
                items: {
                    type: string;
                    properties: {
                        table_zone: {
                            type: string;
                        };
                        tables: {
                            type: string;
                            items: {
                                type: string;
                                properties: {
                                    id: {
                                        type: string;
                                    };
                                    table_no: {
                                        type: string;
                                    };
                                    table_status: {
                                        type: string;
                                        example: TableStatusEnum;
                                        enum: TableStatusEnum[];
                                    };
                                    min_size: {
                                        type: string;
                                    };
                                    max_size: {
                                        type: string;
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
};
