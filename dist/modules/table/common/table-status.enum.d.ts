export declare enum TableStatusEnum {
    AVAILABLE = "AVAILABLE",
    RESERVED = "RESERVED",
    SEATED = "SEATED",
    DISABLED = "DISABLED"
}
