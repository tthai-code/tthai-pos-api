"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllTableResponse = void 0;
const table_status_enum_1 = require("./table-status.enum");
exports.getAllTableResponse = {
    schema: {
        type: 'object',
        properties: {
            message: { type: 'string', example: 'done' },
            data: {
                type: 'array',
                example: [
                    {
                        table_zone: 'Table',
                        tables: [
                            {
                                id: '631280e1d8aafae7cae2452b',
                                table_status: 'AVAILABLE',
                                max_size: 3,
                                min_size: 1,
                                table_no: '1'
                            },
                            {
                                id: '631280ead8aafae7cae2452e',
                                table_status: 'SEATED',
                                max_size: 2,
                                min_size: 1,
                                table_no: '2'
                            }
                        ]
                    },
                    {
                        table_zone: 'Bar',
                        tables: [
                            {
                                id: '631280e1d8aafae7cae2452b',
                                table_status: 'AVAILABLE',
                                max_size: 3,
                                min_size: 1,
                                table_no: '1'
                            },
                            {
                                id: '631280ead8aafae7cae2452e',
                                table_status: 'SEATED',
                                max_size: 2,
                                min_size: 1,
                                table_no: '2'
                            }
                        ]
                    }
                ],
                items: {
                    type: 'object',
                    properties: {
                        table_zone: { type: 'string' },
                        tables: {
                            type: 'array',
                            items: {
                                type: 'object',
                                properties: {
                                    id: { type: 'string' },
                                    table_no: { type: 'string' },
                                    table_status: {
                                        type: 'string',
                                        example: table_status_enum_1.TableStatusEnum.AVAILABLE,
                                        enum: Object.values(table_status_enum_1.TableStatusEnum)
                                    },
                                    min_size: { type: 'number' },
                                    max_size: { type: 'number' }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};
//# sourceMappingURL=get-all-table-res.js.map