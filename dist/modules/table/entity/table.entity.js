"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSTableResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const table_status_enum_1 = require("../common/table-status.enum");
class TableFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableFieldsResponse.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableFieldsResponse.prototype, "table_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TableFieldsResponse.prototype, "max_size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TableFieldsResponse.prototype, "min_size", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(table_status_enum_1.TableStatusEnum) }),
    __metadata("design:type", String)
], TableFieldsResponse.prototype, "table_status", void 0);
class TableZoneFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableZoneFieldsResponse.prototype, "table_zone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [TableFieldsResponse] }),
    __metadata("design:type", Array)
], TableZoneFieldsResponse.prototype, "tables", void 0);
class POSTableResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [TableZoneFieldsResponse],
        example: [
            {
                table_zone: 'Patio',
                tables: [
                    {
                        id: '633699d658fe7dc03f44920e',
                        table_no: '5',
                        max_size: 6,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    },
                    {
                        id: '633699e358fe7dc03f449216',
                        table_no: '6',
                        max_size: 6,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    },
                    {
                        id: '6336b32ec64b8b0998892917',
                        table_no: '7',
                        max_size: 4,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    }
                ]
            },
            {
                table_zone: 'Indoor',
                tables: [
                    {
                        id: '63157c99b66b8e14a6c00f8e',
                        table_no: '1',
                        max_size: 5,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    },
                    {
                        id: '63157f97b66b8e14a6c0103a',
                        table_no: '2',
                        max_size: 5,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    },
                    {
                        id: '633699b458fe7dc03f4491fe',
                        table_no: '3',
                        max_size: 5,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    },
                    {
                        id: '633699c458fe7dc03f449206',
                        table_no: '4',
                        max_size: 3,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    }
                ]
            },
            {
                table_zone: 'Bar',
                tables: [
                    {
                        id: '6336b370c64b8b0998892924',
                        table_no: '8',
                        max_size: 2,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    },
                    {
                        id: '6336b379c64b8b099889292c',
                        table_no: '9',
                        max_size: 2,
                        min_size: 1,
                        table_status: 'AVAILABLE'
                    }
                ]
            }
        ]
    }),
    __metadata("design:type", Array)
], POSTableResponse.prototype, "data", void 0);
exports.POSTableResponse = POSTableResponse;
//# sourceMappingURL=table.entity.js.map