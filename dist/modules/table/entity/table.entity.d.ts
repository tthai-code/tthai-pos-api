import { ResponseDto } from 'src/common/entity/response.entity';
declare class TableFieldsResponse {
    id: string;
    table_no: string;
    max_size: number;
    min_size: number;
    table_status: string;
}
declare class TableZoneFieldsResponse {
    table_zone: string;
    tables: Array<TableFieldsResponse>;
}
export declare class POSTableResponse extends ResponseDto<Array<TableZoneFieldsResponse>> {
    data: Array<TableZoneFieldsResponse>;
}
export {};
