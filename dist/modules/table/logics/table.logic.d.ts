import { TableZoneService } from 'src/modules/table-zone/services/table-zone.service';
import { CreateTableDto } from '../dto/create-table.dto';
import { UpdateTableDto } from '../dto/update-table.dto';
import { TableService } from '../services/table.service';
export declare class TableLogic {
    private readonly tableService;
    private readonly tableZoneService;
    constructor(tableService: TableService, tableZoneService: TableZoneService);
    CreateTable(table: CreateTableDto): Promise<import("../models/table.schema").TableDocument>;
    UpdateTable(id: string, table: UpdateTableDto): Promise<import("../models/table.schema").TableDocument>;
    GetAllTable(query: any, restaurantId: string): Promise<{
        tableZone: any;
        tables: {
            id: any;
            tableNo: any;
            maxSize: any;
            minSize: any;
            tableStatus: any;
        }[];
    }[]>;
}
