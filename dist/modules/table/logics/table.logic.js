"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const table_zone_service_1 = require("../../table-zone/services/table-zone.service");
const table_service_1 = require("../services/table.service");
let TableLogic = class TableLogic {
    constructor(tableService, tableZoneService) {
        this.tableService = tableService;
        this.tableZoneService = tableZoneService;
    }
    async CreateTable(table) {
        const tableZone = await this.tableZoneService.findOne({
            _id: table.tableZoneId,
            restaurantId: table.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { name: 1 });
        if (!tableZone)
            throw new common_1.NotFoundException('Table Zone not found.');
        const existTable = await this.tableService.findOne({
            restaurantId: table.restaurantId,
            tableNo: table.tableNo,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (existTable) {
            throw new common_1.BadRequestException(`Table No. ${table.tableNo}  is already used.`);
        }
        const payload = Object.assign(Object.assign({}, table), { tableZone: {
                id: tableZone.id,
                name: tableZone.name
            } });
        delete payload.tableZoneId;
        return this.tableService.create(payload);
    }
    async UpdateTable(id, table) {
        const oldTable = await this.tableService.findOne({
            _id: id
        });
        if (!oldTable)
            throw new common_1.NotFoundException('Table not found.');
        const tableZone = await this.tableZoneService.findOne({
            _id: table.tableZoneId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { name: 1 });
        if (!tableZone)
            throw new common_1.NotFoundException('Table Zone not found.');
        const existTable = await this.tableService.findOne({
            _id: { $ne: id },
            restaurantId: oldTable.restaurantId,
            tableNo: table.tableNo,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (existTable) {
            throw new common_1.BadRequestException(`Table No. ${table.tableNo}  is already used.`);
        }
        const payload = Object.assign(Object.assign({}, table), { tableZone: {
                id: tableZone.id,
                name: tableZone.name
            } });
        delete payload.tableZoneId;
        return this.tableService.update(id, payload);
    }
    async GetAllTable(query, restaurantId) {
        const tableZones = await this.tableZoneService.getAll({
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { _id: 1, name: 1 });
        const tables = await this.tableService.getAll(query, {
            tableNo: 1,
            maxSize: 1,
            minSize: 1,
            tableStatus: 1,
            tableZone: 1
        }, {
            sort: { tableNo: 1 },
            collation: { locale: 'en_US', numericOrdering: true }
        });
        const payload = tableZones.map((zone) => ({
            tableZone: zone.name,
            tables: tables
                .filter((table) => table['tableZone']['id'] === zone.id)
                .map((item) => ({
                id: item.id,
                tableNo: item.tableNo,
                maxSize: item.maxSize,
                minSize: item.minSize,
                tableStatus: item.tableStatus
            }))
        }));
        return payload;
    }
};
TableLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [table_service_1.TableService,
        table_zone_service_1.TableZoneService])
], TableLogic);
exports.TableLogic = TableLogic;
//# sourceMappingURL=table.logic.js.map