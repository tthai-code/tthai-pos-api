"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeviceModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const subscription_module_1 = require("../subscription/subscription.module");
const device_controller_1 = require("./controllers/device.controller");
const other_device_controller_1 = require("./controllers/other-device.controller");
const pos_other_device_controller_1 = require("./controllers/pos-other-device.controller");
const public_device_controller_1 = require("./controllers/public-device.controller");
const device_logic_1 = require("./logics/device.logic");
const other_device_logic_1 = require("./logics/other-device.logic");
const device_schema_1 = require("./schemas/device.schema");
const other_device_schema_1 = require("./schemas/other-device.schema");
const device_service_1 = require("./services/device.service");
const other_device_service_1 = require("./services/other-device.service");
let DeviceModule = class DeviceModule {
};
DeviceModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            (0, common_1.forwardRef)(() => subscription_module_1.SubscriptionModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'device', schema: device_schema_1.DeviceSchema },
                { name: 'otherDevices', schema: other_device_schema_1.OtherDeviceSchema }
            ])
        ],
        providers: [device_service_1.DeviceService, device_logic_1.DeviceLogic, other_device_service_1.OtherDeviceService, other_device_logic_1.OtherDeviceLogic],
        controllers: [
            device_controller_1.DeviceController,
            public_device_controller_1.PublicDeviceController,
            pos_other_device_controller_1.POSOtherDeviceController,
            other_device_controller_1.OtherDeviceController
        ],
        exports: [device_service_1.DeviceService]
    })
], DeviceModule);
exports.DeviceModule = DeviceModule;
//# sourceMappingURL=device.module.js.map