import { ClientSession } from 'mongodb';
import { PaginateModel } from 'mongoose-paginate';
import { Model } from 'mongoose';
import { OtherDeviceDocument } from '../schemas/other-device.schema';
export declare class OtherDeviceService {
    private readonly OtherDeviceModel;
    private request;
    constructor(OtherDeviceModel: PaginateModel<OtherDeviceDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<OtherDeviceDocument | any>;
    resolveByCondition(condition: any): Promise<OtherDeviceDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<OtherDeviceDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<OtherDeviceDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<OtherDeviceDocument>;
    create(payload: any): Promise<OtherDeviceDocument>;
    update(id: string, payload: any): Promise<OtherDeviceDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<OtherDeviceDocument>;
    delete(id: string): Promise<OtherDeviceDocument>;
    hardDelete(id: string): Promise<OtherDeviceDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<OtherDeviceDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<OtherDeviceDocument>;
}
