import { ClientSession } from 'mongodb';
import { PaginateModel } from 'mongoose-paginate';
import { Model } from 'mongoose';
import { DeviceDocument } from '../schemas/device.schema';
export declare class DeviceService {
    private readonly DeviceModel;
    private request;
    constructor(DeviceModel: PaginateModel<DeviceDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<DeviceDocument | any>;
    resolveByCondition(condition: any): Promise<DeviceDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<DeviceDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<DeviceDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<DeviceDocument>;
    create(payload: any): Promise<DeviceDocument>;
    update(id: string, payload: any): Promise<DeviceDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<DeviceDocument>;
    delete(id: string): Promise<DeviceDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<DeviceDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<DeviceDocument>;
}
