"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectionStatusEnum = void 0;
var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum["CONNECTED"] = "CONNECTED";
    ConnectionStatusEnum["NOT_CONNECTED"] = "NOT_CONNECTED";
})(ConnectionStatusEnum = exports.ConnectionStatusEnum || (exports.ConnectionStatusEnum = {}));
//# sourceMappingURL=device.enum.js.map