export declare enum ConnectionStatusEnum {
    CONNECTED = "CONNECTED",
    NOT_CONNECTED = "NOT_CONNECTED"
}
