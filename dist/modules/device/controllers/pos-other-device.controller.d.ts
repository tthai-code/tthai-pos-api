import { CreateOtherDeviceDto } from '../dto/create-other-device.dto';
import { OtherDeviceLogic } from '../logics/other-device.logic';
import { OtherDeviceService } from '../services/other-device.service';
export declare class POSOtherDeviceController {
    private readonly otherDeviceService;
    private readonly otherDeviceLogic;
    constructor(otherDeviceService: OtherDeviceService, otherDeviceLogic: OtherDeviceLogic);
    addOtherDevice(payload: CreateOtherDeviceDto): Promise<{
        id: any;
    }>;
    deleteOtherDevice(id: string): Promise<{
        success: boolean;
    }>;
}
