"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeviceController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const create_device_dto_1 = require("../dto/create-device.dto");
const update_device_dto_1 = require("../dto/update-device.dto");
const device_logic_1 = require("../logics/device.logic");
const create_device_response_1 = require("../response/create-device.response");
const get_all_device_reponse_1 = require("../response/get-all-device.reponse");
const get_device_response_1 = require("../response/get-device.response");
const update_device_response_1 = require("../response/update-device.response");
const device_service_1 = require("../services/device.service");
let DeviceController = class DeviceController {
    constructor(deviceService, deviceLogic) {
        this.deviceService = deviceService;
        this.deviceLogic = deviceLogic;
    }
    async getDeviceLimit() {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.deviceLogic.getDeviceLimit(restaurantId);
    }
    async createDevice(payload) {
        return await this.deviceLogic.createDevice(payload);
    }
    async getAllDeviceByRestaurant(id) {
        return await this.deviceService.getAll({
            restaurantId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
    }
    async getDeviceById(id) {
        const device = await this.deviceService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!device) {
            throw new common_1.NotFoundException('Not found device.');
        }
        return device;
    }
    async updateDevice(id, payload) {
        const device = await this.deviceService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!device) {
            throw new common_1.NotFoundException('Not found device.');
        }
        return await this.deviceService.update(id, payload);
    }
    async deleteDevice(id) {
        const device = await this.deviceService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!device) {
            throw new common_1.NotFoundException('Not found device.');
        }
        return await this.deviceService.delete(id);
    }
};
__decorate([
    (0, common_1.Get)('limit/capacity'),
    (0, swagger_1.ApiOperation)({ summary: 'Get Device Limit' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "getDeviceLimit", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)(create_device_response_1.createDeviceResponse),
    (0, swagger_1.ApiOperation)({ summary: 'Create a device and generate token' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_device_dto_1.CreateDeviceDto]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "createDevice", null);
__decorate([
    (0, common_1.Get)(':restaurantId/restaurant'),
    (0, swagger_1.ApiOkResponse)(get_all_device_reponse_1.getAllDeviceResponse),
    (0, swagger_1.ApiOperation)({ summary: 'Get all device by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "getAllDeviceByRestaurant", null);
__decorate([
    (0, common_1.Get)(':deviceId'),
    (0, swagger_1.ApiOkResponse)(get_device_response_1.getDeviceResponse),
    (0, swagger_1.ApiOperation)({ summary: 'Get a device by device ID' }),
    __param(0, (0, common_1.Param)('deviceId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "getDeviceById", null);
__decorate([
    (0, common_1.Put)(':deviceId'),
    (0, swagger_1.ApiOkResponse)(update_device_response_1.updateDeviceResponse),
    (0, swagger_1.ApiOperation)({ summary: 'Update a device setting' }),
    __param(0, (0, common_1.Param)('deviceId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_device_dto_1.UpdateDeviceDto]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "updateDevice", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete a device by ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "deleteDevice", null);
DeviceController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('device'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/device'),
    __metadata("design:paramtypes", [device_service_1.DeviceService,
        device_logic_1.DeviceLogic])
], DeviceController);
exports.DeviceController = DeviceController;
//# sourceMappingURL=device.controller.js.map