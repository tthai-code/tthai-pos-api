import { AuthLogic } from 'src/modules/auth/logics/auth.logic';
import { LogLogic } from 'src/modules/log/logic/log.logic';
import { ActivateDeviceCodeDto } from '../dto/activate-device.dto';
export declare class PublicDeviceController {
    private readonly authLogic;
    private readonly logLogic;
    constructor(authLogic: AuthLogic, logLogic: LogLogic);
    activateCode(payload: ActivateDeviceCodeDto, request: any): Promise<{
        accessToken: string;
        restaurant: {
            id: any;
        };
    }>;
    deviceLogin(id: string): Promise<{
        accessToken: string;
        restaurant: {
            id: any;
        };
    }>;
}
