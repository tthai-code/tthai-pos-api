"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSOtherDeviceController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const status_enum_1 = require("../../../common/enum/status.enum");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const create_other_device_dto_1 = require("../dto/create-other-device.dto");
const other_device_entity_1 = require("../entity/other-device.entity");
const other_device_logic_1 = require("../logics/other-device.logic");
const other_device_service_1 = require("../services/other-device.service");
let POSOtherDeviceController = class POSOtherDeviceController {
    constructor(otherDeviceService, otherDeviceLogic) {
        this.otherDeviceService = otherDeviceService;
        this.otherDeviceLogic = otherDeviceLogic;
    }
    async addOtherDevice(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.otherDeviceLogic.addOtherDevice(restaurantId, payload);
    }
    async deleteOtherDevice(id) {
        const device = await this.otherDeviceService.findOne({ _id: id, status: status_enum_1.StatusEnum.ACTIVE }, { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 });
        if (!device)
            throw new common_1.NotFoundException('Not found device.');
        await this.otherDeviceService.hardDelete(id);
        return { success: true };
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => other_device_entity_1.AddOtherDeviceResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Add Other Device',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_other_device_dto_1.CreateOtherDeviceDto]),
    __metadata("design:returntype", Promise)
], POSOtherDeviceController.prototype, "addOtherDevice", null);
__decorate([
    (0, common_1.Delete)(':otherDeviceId'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete Other Device',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('otherDeviceId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSOtherDeviceController.prototype, "deleteOtherDevice", null);
POSOtherDeviceController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/other-device'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('/v1/pos/other-device'),
    __metadata("design:paramtypes", [other_device_service_1.OtherDeviceService,
        other_device_logic_1.OtherDeviceLogic])
], POSOtherDeviceController);
exports.POSOtherDeviceController = POSOtherDeviceController;
//# sourceMappingURL=pos-other-device.controller.js.map