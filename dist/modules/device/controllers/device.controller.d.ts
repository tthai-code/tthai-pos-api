import { CreateDeviceDto } from '../dto/create-device.dto';
import { UpdateDeviceDto } from '../dto/update-device.dto';
import { DeviceLogic } from '../logics/device.logic';
import { DeviceService } from '../services/device.service';
export declare class DeviceController {
    private readonly deviceService;
    private readonly deviceLogic;
    constructor(deviceService: DeviceService, deviceLogic: DeviceLogic);
    getDeviceLimit(): Promise<{
        limitCapacity: number;
    }>;
    createDevice(payload: CreateDeviceDto): Promise<import("../schemas/device.schema").DeviceDocument>;
    getAllDeviceByRestaurant(id: string): Promise<any[]>;
    getDeviceById(id: string): Promise<import("../schemas/device.schema").DeviceDocument>;
    updateDevice(id: string, payload: UpdateDeviceDto): Promise<import("../schemas/device.schema").DeviceDocument>;
    deleteDevice(id: string): Promise<import("../schemas/device.schema").DeviceDocument>;
}
