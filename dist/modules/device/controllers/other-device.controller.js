"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OtherDeviceController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const other_device_entity_1 = require("../entity/other-device.entity");
const other_device_logic_1 = require("../logics/other-device.logic");
const other_device_service_1 = require("../services/other-device.service");
let OtherDeviceController = class OtherDeviceController {
    constructor(otherDeviceService, otherDeviceLogic) {
        this.otherDeviceService = otherDeviceService;
        this.otherDeviceLogic = otherDeviceLogic;
    }
    async getAllOtherDevice() {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.otherDeviceLogic.getAllOtherDevice(restaurantId);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => other_device_entity_1.GetAllOtherDevicesResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get all others device',
        description: 'use bearer `restaurant_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], OtherDeviceController.prototype, "getAllOtherDevice", null);
OtherDeviceController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('other-device'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/other-device'),
    __metadata("design:paramtypes", [other_device_service_1.OtherDeviceService,
        other_device_logic_1.OtherDeviceLogic])
], OtherDeviceController);
exports.OtherDeviceController = OtherDeviceController;
//# sourceMappingURL=other-device.controller.js.map