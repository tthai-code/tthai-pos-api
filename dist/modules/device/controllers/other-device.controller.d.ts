import { OtherDeviceLogic } from '../logics/other-device.logic';
import { OtherDeviceService } from '../services/other-device.service';
export declare class OtherDeviceController {
    private readonly otherDeviceService;
    private readonly otherDeviceLogic;
    constructor(otherDeviceService: OtherDeviceService, otherDeviceLogic: OtherDeviceLogic);
    getAllOtherDevice(): Promise<any[]>;
}
