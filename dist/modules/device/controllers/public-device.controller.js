"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicDeviceController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const super_admin_auth_guard_1 = require("../../auth/guards/super-admin-auth.guard");
const auth_logic_1 = require("../../auth/logics/auth.logic");
const log_logic_1 = require("../../log/logic/log.logic");
const errorResponse_1 = require("../../../utilities/errorResponse");
const activate_device_dto_1 = require("../dto/activate-device.dto");
const device_entity_1 = require("../entity/device.entity");
let PublicDeviceController = class PublicDeviceController {
    constructor(authLogic, logLogic) {
        this.authLogic = authLogic;
        this.logLogic = logLogic;
    }
    async activateCode(payload, request) {
        const activated = await this.authLogic.ActivateDeviceCode(payload.activationCode);
        await this.logLogic.createLogic(request, 'Activate Code', activated);
        return activated;
    }
    async deviceLogin(id) {
        return await this.authLogic.DirectAdminActivateDeviceCode(id);
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOkResponse)({ type: () => device_entity_1.ActiveDeviceCodeResponse }),
    (0, swagger_1.ApiBadRequestResponse)((0, errorResponse_1.errorResponse)(400, 'This activation code is already used.', 'v1/activate-code')),
    (0, swagger_1.ApiNotFoundResponse)((0, errorResponse_1.errorResponse)(404, 'Not found activation code.', 'v1/activate-code')),
    (0, swagger_1.ApiOperation)({ summary: 'Activate Code' }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [activate_device_dto_1.ActivateDeviceCodeDto, Object]),
    __metadata("design:returntype", Promise)
], PublicDeviceController.prototype, "activateCode", null);
__decorate([
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    (0, common_1.Get)('direct-admin/:restaurantId'),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PublicDeviceController.prototype, "deviceLogin", null);
PublicDeviceController = __decorate([
    (0, swagger_1.ApiTags)('auth/device'),
    (0, common_1.Controller)('v1/activation-code'),
    __metadata("design:paramtypes", [auth_logic_1.AuthLogic,
        log_logic_1.LogLogic])
], PublicDeviceController);
exports.PublicDeviceController = PublicDeviceController;
//# sourceMappingURL=public-device.controller.js.map