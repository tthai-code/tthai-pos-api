export declare class CreateDeviceDto {
    readonly restaurantId: string;
    readonly name: string;
    readonly description: string;
}
