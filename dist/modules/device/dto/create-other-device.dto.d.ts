export declare class CreateOtherDeviceDto {
    restaurantId: string;
    readonly name: string;
    readonly description: string;
}
