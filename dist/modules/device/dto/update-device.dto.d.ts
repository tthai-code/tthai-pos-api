export declare class UpdateDeviceDto {
    readonly name: string;
    readonly description: string;
}
