import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type DeviceDocument = DeviceFields & Document;
export declare class DeviceFields {
    restaurantId: Types.ObjectId;
    name: string;
    description: string;
    activationCode: string;
    isActivated: boolean;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const DeviceSchema: MongooseSchema<Document<DeviceFields, any, any>, import("mongoose").Model<Document<DeviceFields, any, any>, any, any, any>, any, any>;
