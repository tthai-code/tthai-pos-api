import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type OtherDeviceDocument = OtherDeviceFields & Document;
export declare class OtherDeviceFields {
    restaurantId: Types.ObjectId;
    name: string;
    description: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const OtherDeviceSchema: MongooseSchema<Document<OtherDeviceFields, any, any>, import("mongoose").Model<Document<OtherDeviceFields, any, any>, any, any, any>, any, any>;
