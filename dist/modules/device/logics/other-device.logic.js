"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OtherDeviceLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_schema_1 = require("../../restaurant/schemas/restaurant.schema");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const other_device_service_1 = require("../services/other-device.service");
let OtherDeviceLogic = class OtherDeviceLogic {
    constructor(restaurantService, otherDeviceService) {
        this.restaurantService = restaurantService;
        this.otherDeviceService = otherDeviceService;
    }
    async checkRestaurant(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        return restaurant;
    }
    async getAllOtherDevice(restaurantId) {
        const restaurant = await this.checkRestaurant(restaurantId);
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const otherDevices = await this.otherDeviceService.getAll({ restaurantId, status: status_enum_1.StatusEnum.ACTIVE }, { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 });
        return otherDevices;
    }
    async addOtherDevice(restaurantId, payload) {
        const restaurant = await this.checkRestaurant(restaurantId);
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        payload.restaurantId = restaurantId;
        const device = await this.otherDeviceService.create(payload);
        return { id: device.id };
    }
};
OtherDeviceLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        other_device_service_1.OtherDeviceService])
], OtherDeviceLogic);
exports.OtherDeviceLogic = OtherDeviceLogic;
//# sourceMappingURL=other-device.logic.js.map