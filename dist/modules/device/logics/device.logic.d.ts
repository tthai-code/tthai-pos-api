import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { PackageService } from 'src/modules/subscription/services/package.service';
import { SubscriptionService } from 'src/modules/subscription/services/subscription.service';
import { CreateDeviceDto } from '../dto/create-device.dto';
import { DeviceDocument } from '../schemas/device.schema';
import { DeviceService } from '../services/device.service';
export declare class DeviceLogic {
    private readonly deviceService;
    private readonly restaurantService;
    private readonly subscriptionService;
    private readonly packageService;
    constructor(deviceService: DeviceService, restaurantService: RestaurantService, subscriptionService: SubscriptionService, packageService: PackageService);
    private randomActivateCode;
    private genOrderUrl;
    createDevice(createDevice: CreateDeviceDto): Promise<DeviceDocument>;
    getDeviceLimit(restaurantId: string): Promise<{
        limitCapacity: number;
    }>;
    checkDeviceLimit(restaurantId: any): Promise<number>;
}
