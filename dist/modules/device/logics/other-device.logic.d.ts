import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CreateOtherDeviceDto } from '../dto/create-other-device.dto';
import { OtherDeviceService } from '../services/other-device.service';
export declare class OtherDeviceLogic {
    private readonly restaurantService;
    private readonly otherDeviceService;
    constructor(restaurantService: RestaurantService, otherDeviceService: OtherDeviceService);
    private checkRestaurant;
    getAllOtherDevice(restaurantId: string): Promise<any[]>;
    addOtherDevice(restaurantId: string, payload: CreateOtherDeviceDto): Promise<{
        id: any;
    }>;
}
