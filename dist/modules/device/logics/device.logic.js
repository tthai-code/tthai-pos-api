"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeviceLogic = void 0;
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const package_type_enum_1 = require("../../subscription/common/package-type.enum");
const subscription_plan_enum_1 = require("../../subscription/common/subscription-plan.enum");
const package_service_1 = require("../../subscription/services/package.service");
const subscription_service_1 = require("../../subscription/services/subscription.service");
const device_service_1 = require("../services/device.service");
let DeviceLogic = class DeviceLogic {
    constructor(deviceService, restaurantService, subscriptionService, packageService) {
        this.deviceService = deviceService;
        this.restaurantService = restaurantService;
        this.subscriptionService = subscriptionService;
        this.packageService = packageService;
    }
    randomActivateCode() {
        const message = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        let code = '';
        for (let i = 0; i < 8; i++) {
            code += message.charAt(Math.floor(Math.random() * message.length));
        }
        return code;
    }
    async genOrderUrl() {
        let code = '';
        while (true) {
            code = this.randomActivateCode();
            const isDuplicated = await this.deviceService.findOne({
                activationCode: code
            });
            if (!isDuplicated) {
                break;
            }
        }
        return code;
    }
    async createDevice(createDevice) {
        const restaurant = await this.restaurantService.findOne({
            _id: createDevice.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant) {
            throw new common_1.NotFoundException('Not found restaurant.');
        }
        const isDeviceOverLimit = await this.checkDeviceLimit(createDevice.restaurantId);
        if (isDeviceOverLimit <= 0) {
            throw new common_1.BadRequestException('Cannot create more device please add more limit capacity.');
        }
        const code = await this.genOrderUrl();
        const payload = (0, class_transformer_1.classToPlain)(createDevice);
        payload.isActivated = false;
        payload.activationCode = code;
        return this.deviceService.create(payload);
    }
    async getDeviceLimit(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant) {
            throw new common_1.NotFoundException('Not found restaurant.');
        }
        const iPadPackages = await this.packageService.getAll({
            packageType: package_type_enum_1.PackageTypeEnum.IPAD,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const iPadPackageIds = iPadPackages.map((item) => item.id);
        const subscription = await this.subscriptionService.getAll({
            restaurantId,
            packageId: { $in: iPadPackageIds },
            subscriptionStatus: subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        let limitCapacity = 0;
        for (const sub of subscription) {
            const packageIndex = iPadPackages.findIndex((item) => `${item._id}` === sub.packageId);
            if (packageIndex > -1) {
                const { iPadCapacity } = iPadPackages[packageIndex];
                const { quantity } = sub;
                const calculated = iPadCapacity * quantity;
                limitCapacity += calculated;
            }
        }
        return { limitCapacity };
    }
    async checkDeviceLimit(restaurantId) {
        const iPadPackages = await this.packageService.getAll({
            packageType: package_type_enum_1.PackageTypeEnum.IPAD,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const iPadPackageIds = iPadPackages.map((item) => item.id);
        const subscription = await this.subscriptionService.getAll({
            restaurantId,
            packageId: { $in: iPadPackageIds },
            subscriptionStatus: subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        let limitCapacity = 0;
        for (const sub of subscription) {
            const packageIndex = iPadPackages.findIndex((item) => `${item._id}` === sub.packageId);
            if (packageIndex > -1) {
                const { iPadCapacity } = iPadPackages[packageIndex];
                const { quantity } = sub;
                const calculated = iPadCapacity * quantity;
                limitCapacity += calculated;
            }
        }
        const device = await this.deviceService.getAll({
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        return limitCapacity - device.length;
    }
};
DeviceLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [device_service_1.DeviceService,
        restaurant_service_1.RestaurantService,
        subscription_service_1.SubscriptionService,
        package_service_1.PackageService])
], DeviceLogic);
exports.DeviceLogic = DeviceLogic;
//# sourceMappingURL=device.logic.js.map