"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActiveDeviceCodeResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class RestaurantField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantField.prototype, "id", void 0);
class ActiveDeviceCodeFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", RestaurantField)
], ActiveDeviceCodeFieldsResponse.prototype, "restaurant", void 0);
class ActiveDeviceCodeResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            restaurant: {
                id: '630eff5751c2eac55f52662c'
            }
        }
    }),
    __metadata("design:type", ActiveDeviceCodeFieldsResponse)
], ActiveDeviceCodeResponse.prototype, "data", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN0YXVyYW50Ijp7ImlkIjoiNjMwZWZmNTc1MWMyZWFjNTVmNTI2NjJjIn0sImlhdCI6MTY2NTU3NTUwNywiZXhwIjoxNjY2MTgwMzA3fQ.wz9TsWDwfXSe4B1CyeHCi21FedVCmJX22NaWWrjzCMM'
    }),
    __metadata("design:type", String)
], ActiveDeviceCodeResponse.prototype, "access_token", void 0);
exports.ActiveDeviceCodeResponse = ActiveDeviceCodeResponse;
//# sourceMappingURL=device.entity.js.map