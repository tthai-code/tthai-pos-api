"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddOtherDeviceResponse = exports.GetAllOtherDevicesResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class IdResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], IdResponse.prototype, "id", void 0);
class OtherDeviceObject extends IdResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OtherDeviceObject.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OtherDeviceObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OtherDeviceObject.prototype, "restaurant_id", void 0);
class GetAllOtherDevicesResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [OtherDeviceObject],
        example: [
            {
                description: 'Test description',
                name: 'Front POS',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                id: '6398e6b4e6b45740eb2192a6'
            }
        ]
    }),
    __metadata("design:type", Array)
], GetAllOtherDevicesResponse.prototype, "data", void 0);
exports.GetAllOtherDevicesResponse = GetAllOtherDevicesResponse;
class AddOtherDeviceResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: IdResponse,
        example: {
            id: '639b15b458389635b2fb39e2'
        }
    }),
    __metadata("design:type", IdResponse)
], AddOtherDeviceResponse.prototype, "data", void 0);
exports.AddOtherDeviceResponse = AddOtherDeviceResponse;
//# sourceMappingURL=other-device.entity.js.map