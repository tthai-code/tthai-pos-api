import { ResponseDto } from 'src/common/entity/response.entity';
declare class IdResponse {
    id: string;
}
declare class OtherDeviceObject extends IdResponse {
    description: string;
    name: string;
    restaurant_id: string;
}
export declare class GetAllOtherDevicesResponse extends ResponseDto<OtherDeviceObject[]> {
    data: OtherDeviceObject[];
}
export declare class AddOtherDeviceResponse extends ResponseDto<IdResponse> {
    data: IdResponse;
}
export {};
