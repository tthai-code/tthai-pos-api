import { ResponseDto } from 'src/common/entity/response.entity';
declare class RestaurantField {
    id: string;
}
declare class ActiveDeviceCodeFieldsResponse {
    restaurant: RestaurantField;
}
export declare class ActiveDeviceCodeResponse extends ResponseDto<ActiveDeviceCodeFieldsResponse> {
    data: ActiveDeviceCodeFieldsResponse;
    access_token: string;
}
export {};
