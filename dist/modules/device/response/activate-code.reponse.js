"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.activateCodeNotFoundResponse = exports.activateCodeIsActivatedResponse = exports.activateCodeResponse = void 0;
exports.activateCodeResponse = {
    schema: {
        type: 'object',
        example: {
            message: 'done',
            data: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5',
                owner_name: 'Cory Wong',
                restaurant: {
                    id: '630e55a0d9c30fd7cdcb424b',
                    legal_business_name: 'Holy Beef',
                    address: {
                        zip_code: '75001',
                        state: 'Dallas',
                        city: 'Addison',
                        address: 'Some Address'
                    },
                    business_phone: '0222222222',
                    email: 'corywong@mail.com',
                    tax_rate: {
                        is_alcohol_tax_active: false,
                        alcohol_tax: 12.5,
                        tax: 10
                    },
                    logo_url: null,
                    printer_setting: {
                        receipt_ip_printer: '0.0.0.0',
                        kitchen_ip_printer: '0.0.0.0'
                    }
                },
                token_expire: 'permanent'
            },
            access_token: '<token>'
        }
    }
};
exports.activateCodeIsActivatedResponse = {
    schema: {
        type: 'object',
        example: {
            statusCode: 400,
            message: 'This activation code is already used.',
            timestamp: '2022-10-07T09:21:23.299Z',
            path: '/v1/activation-code'
        }
    }
};
exports.activateCodeNotFoundResponse = {
    schema: {
        type: 'object',
        example: {
            statusCode: 404,
            message: 'Not found activation code.',
            timestamp: '2022-10-07T09:23:51.271Z',
            path: '/v1/activation-code'
        }
    }
};
//# sourceMappingURL=activate-code.reponse.js.map