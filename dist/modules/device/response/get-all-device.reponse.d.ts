export declare const getAllDeviceResponse: {
    schema: {
        type: string;
        example: {
            message: string;
            data: {
                status: string;
                is_activated: boolean;
                activation_code: string;
                description: string;
                name: string;
                restaurant_id: string;
                created_at: string;
                updated_at: string;
                updated_by: {
                    username: string;
                    id: string;
                };
                created_by: {
                    username: string;
                    id: string;
                };
                id: string;
            }[];
        };
    };
};
