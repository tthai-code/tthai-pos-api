"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateDeviceResponse = void 0;
const status_enum_1 = require("../../../common/enum/status.enum");
exports.updateDeviceResponse = {
    schema: {
        type: 'object',
        example: {
            message: 'done',
            data: {
                created_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                updated_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                status: 'active',
                is_activated: true,
                activation_code: 'VPCWv0Y2',
                description: 'Front POS description',
                name: 'Front POS',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                updated_at: '2022-10-07T09:49:02.166Z',
                created_at: '2022-10-07T07:17:30.599Z',
                id: '633fd28ac0871b45fe02b23a'
            }
        },
        properties: {
            message: { type: 'string' },
            data: {
                type: 'object',
                properties: {
                    status: { type: 'string', enum: Object.values(status_enum_1.StatusEnum) },
                    is_activated: { type: 'boolean' },
                    activation_code: { type: 'string' },
                    description: { type: 'string' },
                    name: { type: 'string' },
                    restaurant_id: { type: 'string' },
                    created_at: { type: 'string' },
                    updated_at: { type: 'string' },
                    updated_by: {
                        type: 'object',
                        properties: {
                            username: { type: 'string' },
                            id: { type: 'string' }
                        }
                    },
                    created_by: {
                        type: 'object',
                        properties: {
                            username: { type: 'string' },
                            id: { type: 'string' }
                        }
                    },
                    id: { type: 'string' }
                }
            }
        }
    }
};
//# sourceMappingURL=update-device.response.js.map