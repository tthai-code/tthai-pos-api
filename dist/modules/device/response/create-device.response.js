"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createDeviceResponse = void 0;
const status_enum_1 = require("../../../common/enum/status.enum");
exports.createDeviceResponse = {
    schema: {
        type: 'object',
        example: {
            message: 'done',
            data: {
                status: 'active',
                is_activated: false,
                activation_code: '9o5nwyl3',
                description: 'Front POS description',
                name: 'Front POS',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                created_at: '2022-10-07T16:46:18.795Z',
                updated_at: '2022-10-07T16:46:18.795Z',
                updated_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                created_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                id: '634057da50a0ef4cf5cee7ea'
            }
        },
        properties: {
            message: { type: 'string' },
            data: {
                type: 'object',
                properties: {
                    status: { type: 'string', enum: Object.values(status_enum_1.StatusEnum) },
                    is_activated: { type: 'boolean' },
                    activation_code: { type: 'string' },
                    description: { type: 'string' },
                    name: { type: 'string' },
                    restaurant_id: { type: 'string' },
                    created_at: { type: 'string' },
                    updated_at: { type: 'string' },
                    updated_by: {
                        type: 'object',
                        properties: {
                            username: { type: 'string' },
                            id: { type: 'string' }
                        }
                    },
                    created_by: {
                        type: 'object',
                        properties: {
                            username: { type: 'string' },
                            id: { type: 'string' }
                        }
                    },
                    id: { type: 'string' }
                }
            }
        }
    }
};
//# sourceMappingURL=create-device.response.js.map