import { StatusEnum } from 'src/common/enum/status.enum';
export declare const createDeviceResponse: {
    schema: {
        type: string;
        example: {
            message: string;
            data: {
                status: string;
                is_activated: boolean;
                activation_code: string;
                description: string;
                name: string;
                restaurant_id: string;
                created_at: string;
                updated_at: string;
                updated_by: {
                    username: string;
                    id: string;
                };
                created_by: {
                    username: string;
                    id: string;
                };
                id: string;
            };
        };
        properties: {
            message: {
                type: string;
            };
            data: {
                type: string;
                properties: {
                    status: {
                        type: string;
                        enum: StatusEnum[];
                    };
                    is_activated: {
                        type: string;
                    };
                    activation_code: {
                        type: string;
                    };
                    description: {
                        type: string;
                    };
                    name: {
                        type: string;
                    };
                    restaurant_id: {
                        type: string;
                    };
                    created_at: {
                        type: string;
                    };
                    updated_at: {
                        type: string;
                    };
                    updated_by: {
                        type: string;
                        properties: {
                            username: {
                                type: string;
                            };
                            id: {
                                type: string;
                            };
                        };
                    };
                    created_by: {
                        type: string;
                        properties: {
                            username: {
                                type: string;
                            };
                            id: {
                                type: string;
                            };
                        };
                    };
                    id: {
                        type: string;
                    };
                };
            };
        };
    };
};
