export declare const activateCodeResponse: {
    schema: {
        type: string;
        example: {
            message: string;
            data: {
                username: string;
                id: string;
                owner_name: string;
                restaurant: {
                    id: string;
                    legal_business_name: string;
                    address: {
                        zip_code: string;
                        state: string;
                        city: string;
                        address: string;
                    };
                    business_phone: string;
                    email: string;
                    tax_rate: {
                        is_alcohol_tax_active: boolean;
                        alcohol_tax: number;
                        tax: number;
                    };
                    logo_url: any;
                    printer_setting: {
                        receipt_ip_printer: string;
                        kitchen_ip_printer: string;
                    };
                };
                token_expire: string;
            };
            access_token: string;
        };
    };
};
export declare const activateCodeIsActivatedResponse: {
    schema: {
        type: string;
        example: {
            statusCode: number;
            message: string;
            timestamp: string;
            path: string;
        };
    };
};
export declare const activateCodeNotFoundResponse: {
    schema: {
        type: string;
        example: {
            statusCode: number;
            message: string;
            timestamp: string;
            path: string;
        };
    };
};
