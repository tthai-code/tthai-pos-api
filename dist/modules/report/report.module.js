"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportModule = void 0;
const common_1 = require("@nestjs/common");
const order_module_1 = require("../order/order.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const transaction_module_1 = require("../transaction/transaction.module");
const admin_report_controller_1 = require("./controllers/admin-report.controller");
const pos_report_controller_1 = require("./controllers/pos-report.controller");
const report_controller_1 = require("./controllers/report.controller");
const admin_report_logic_1 = require("./logic/admin-report.logic");
const pos_report_logic_1 = require("./logic/pos-report.logic");
const report_logic_1 = require("./logic/report.logic");
let ReportModule = class ReportModule {
};
ReportModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            (0, common_1.forwardRef)(() => order_module_1.OrderModule),
            (0, common_1.forwardRef)(() => transaction_module_1.TransactionModule)
        ],
        providers: [pos_report_logic_1.POSReportLogic, report_logic_1.ReportLogic, admin_report_logic_1.AdminReportLogic],
        controllers: [pos_report_controller_1.POSReportController, report_controller_1.ReportController, admin_report_controller_1.AdminReportController],
        exports: []
    })
], ReportModule);
exports.ReportModule = ReportModule;
//# sourceMappingURL=report.module.js.map