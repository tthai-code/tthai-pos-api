"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportDateEnum = void 0;
var ReportDateEnum;
(function (ReportDateEnum) {
    ReportDateEnum["TODAY"] = "TODAY";
    ReportDateEnum["WEEKLY"] = "WEEKLY";
    ReportDateEnum["MONTHLY"] = "MONTHLY";
    ReportDateEnum["YEARLY"] = "YEARLY";
    ReportDateEnum["RANGE"] = "RANGE";
})(ReportDateEnum = exports.ReportDateEnum || (exports.ReportDateEnum = {}));
//# sourceMappingURL=report-date.enum.js.map