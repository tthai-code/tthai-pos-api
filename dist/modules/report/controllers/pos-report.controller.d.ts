import { LogLogic } from 'src/modules/log/logic/log.logic';
import { GetSaleReportPOS } from '../dto/get-sale-report.dto';
import { POSReportLogic } from '../logic/pos-report.logic';
export declare class POSReportController {
    private readonly posReportLogic;
    private readonly logLogic;
    constructor(posReportLogic: POSReportLogic, logLogic: LogLogic);
    getSaleReport(query: GetSaleReportPOS, request: any): Promise<{
        summarySales: {
            label: string;
            sales: number;
            refunds: number;
            net: number;
        }[];
        summaryTransactionType: {
            label: string;
            orders: number;
            amount: number;
        }[];
        summaryOrderType: {
            label: string;
            orders: number;
            amount: number;
        }[];
        summaryPayout: {
            label: string;
            amount: number;
        }[];
        summaryThirdParty: {
            label: string;
            orders: number;
            sales: number;
            tax: number;
            tips: number;
        }[];
        SummaryVoidOrder: {
            label: string;
            amount: number;
        }[];
        SummaryDeMinimis: {
            label: string;
            amount: number;
        }[];
    }>;
}
