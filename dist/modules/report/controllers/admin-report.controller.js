"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminReportController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const super_admin_auth_guard_1 = require("../../auth/guards/super-admin-auth.guard");
const admin_report_dto_1 = require("../dto/admin-report.dto");
const admin_report_entity_1 = require("../entity/admin-report.entity");
const admin_report_logic_1 = require("../logic/admin-report.logic");
let AdminReportController = class AdminReportController {
    constructor(adminReportLogic) {
        this.adminReportLogic = adminReportLogic;
    }
    async getReportForAdmin(query) {
        return await this.adminReportLogic.getReportForAdmin(query);
    }
};
__decorate([
    (0, common_1.Get)('sale'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_report_entity_1.AdminReportPaginateResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Report for Admin',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [admin_report_dto_1.AdminReportPaginateDto]),
    __metadata("design:returntype", Promise)
], AdminReportController.prototype, "getReportForAdmin", null);
AdminReportController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('admin/report'),
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    (0, common_1.Controller)('v1/admin/report'),
    __metadata("design:paramtypes", [admin_report_logic_1.AdminReportLogic])
], AdminReportController);
exports.AdminReportController = AdminReportController;
//# sourceMappingURL=admin-report.controller.js.map