import { GetSaleReport } from '../dto/get-sale-report.dto';
import { ReportLogic } from '../logic/report.logic';
export declare class ReportController {
    private readonly reportLogic;
    constructor(reportLogic: ReportLogic);
    getSalesReport(query: GetSaleReport): Promise<{
        summary: {
            gross: number;
            discount: number;
            refund: number;
            net: number;
            tips: number;
            serviceCharges: number;
            convenienceFee: number;
            saleTax: number;
            alcoholTax: number;
            total: number;
        };
        transactionType: {
            name: string;
            orders: number;
            amount: number;
        }[];
        orderingType: {
            name: string;
            orders: number;
            amount: number;
        }[];
        payoutCardSettlement: {
            type: string;
            sales: number;
            tips: number;
            tax: number;
            amount: number;
        }[];
        thirdPartyReport: {
            name: string;
            orders: number;
            sales: number;
            tax: number;
            tips: number;
            total: number;
        }[];
        deMinimisSummary: {
            orders: number;
            amount: number;
            salesTax: number;
            total: number;
        };
        voidOrderSummary: {
            orders: number;
            amount: number;
            salesTax: number;
            total: number;
        };
        datasets: any[];
    }>;
}
