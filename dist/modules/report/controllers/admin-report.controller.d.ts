import { AdminReportPaginateDto } from '../dto/admin-report.dto';
import { AdminReportLogic } from '../logic/admin-report.logic';
export declare class AdminReportController {
    private readonly adminReportLogic;
    constructor(adminReportLogic: AdminReportLogic);
    getReportForAdmin(query: AdminReportPaginateDto): Promise<any>;
}
