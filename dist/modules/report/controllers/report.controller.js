"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const get_sale_report_dto_1 = require("../dto/get-sale-report.dto");
const report_entity_1 = require("../entity/report.entity");
const report_logic_1 = require("../logic/report.logic");
let ReportController = class ReportController {
    constructor(reportLogic) {
        this.reportLogic = reportLogic;
    }
    async getSalesReport(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.reportLogic.getSaleReport(restaurantId, query);
    }
};
__decorate([
    (0, common_1.Get)('sale'),
    (0, swagger_1.ApiOkResponse)({ type: () => report_entity_1.ReportResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Sales Report',
        description: 'use bearer `restaurant_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_sale_report_dto_1.GetSaleReport]),
    __metadata("design:returntype", Promise)
], ReportController.prototype, "getSalesReport", null);
ReportController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('report'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/report'),
    __metadata("design:paramtypes", [report_logic_1.ReportLogic])
], ReportController);
exports.ReportController = ReportController;
//# sourceMappingURL=report.controller.js.map