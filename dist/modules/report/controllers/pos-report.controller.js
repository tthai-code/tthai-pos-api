"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSReportController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const log_logic_1 = require("../../log/logic/log.logic");
const get_sale_report_dto_1 = require("../dto/get-sale-report.dto");
const pos_report_entity_1 = require("../entity/pos-report.entity");
const pos_report_logic_1 = require("../logic/pos-report.logic");
let POSReportController = class POSReportController {
    constructor(posReportLogic, logLogic) {
        this.posReportLogic = posReportLogic;
        this.logLogic = logLogic;
    }
    async getSaleReport(query, request) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        await this.logLogic.createLogic(request, 'Get Sale Report for POS');
        return await this.posReportLogic.getSaleReport(restaurantId, query.startDate, query.endDate, query.shiftStartAt, query.shiftEndAt);
    }
};
__decorate([
    (0, common_1.Get)('sale'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_report_entity_1.POSReportResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Sale Report for POS',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_sale_report_dto_1.GetSaleReportPOS, Object]),
    __metadata("design:returntype", Promise)
], POSReportController.prototype, "getSaleReport", null);
POSReportController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/report'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/report'),
    __metadata("design:paramtypes", [pos_report_logic_1.POSReportLogic,
        log_logic_1.LogLogic])
], POSReportController);
exports.POSReportController = POSReportController;
//# sourceMappingURL=pos-report.controller.js.map