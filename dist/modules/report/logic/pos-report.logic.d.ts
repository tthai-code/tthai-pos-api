import { OrderOnlineService } from 'src/modules/order/services/order-online.service';
import { OrderService } from 'src/modules/order/services/order.service';
import { ThirdPartyOrderService } from 'src/modules/order/services/third-party-order.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { TransactionService } from 'src/modules/transaction/services/transaction.service';
export declare class POSReportLogic {
    private readonly orderService;
    private readonly thirdPartyOrderService;
    private readonly transactionService;
    private readonly restaurantService;
    private readonly onlineOrderService;
    constructor(orderService: OrderService, thirdPartyOrderService: ThirdPartyOrderService, transactionService: TransactionService, restaurantService: RestaurantService, onlineOrderService: OrderOnlineService);
    private summaryTransactionItem;
    getOffset(timeZone: any): number;
    getSaleReport(restaurantId: string, startDate: string, endDate: string, shiftStartAt: string, shiftEndAt: string): Promise<{
        summarySales: {
            label: string;
            sales: number;
            refunds: number;
            net: number;
        }[];
        summaryTransactionType: {
            label: string;
            orders: number;
            amount: number;
        }[];
        summaryOrderType: {
            label: string;
            orders: number;
            amount: number;
        }[];
        summaryPayout: {
            label: string;
            amount: number;
        }[];
        summaryThirdParty: {
            label: string;
            orders: number;
            sales: number;
            tax: number;
            tips: number;
        }[];
        SummaryVoidOrder: {
            label: string;
            amount: number;
        }[];
        SummaryDeMinimis: {
            label: string;
            amount: number;
        }[];
    }>;
}
