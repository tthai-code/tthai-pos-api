import { OrderOnlineService } from 'src/modules/order/services/order-online.service';
import { OrderService } from 'src/modules/order/services/order.service';
import { ThirdPartyOrderService } from 'src/modules/order/services/third-party-order.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { TransactionService } from 'src/modules/transaction/services/transaction.service';
import { GetSaleReport } from '../dto/get-sale-report.dto';
export declare class ReportLogic {
    private readonly orderService;
    private readonly thirdPartyOrderService;
    private readonly transactionService;
    private readonly restaurantService;
    private readonly onlineOrderService;
    constructor(orderService: OrderService, thirdPartyOrderService: ThirdPartyOrderService, transactionService: TransactionService, restaurantService: RestaurantService, onlineOrderService: OrderOnlineService);
    private getDatesInRange;
    private summaryTransactionItem;
    private groupByDateAndSummarySubtotal;
    private todayData;
    private weeklyAndMonthlyAndRangeData;
    private yearlyData;
    private generateGraphData;
    private mergeOrderData;
    getSaleReport(restaurantId: string, queryParams: GetSaleReport): Promise<{
        summary: {
            gross: number;
            discount: number;
            refund: number;
            net: number;
            tips: number;
            serviceCharges: number;
            convenienceFee: number;
            saleTax: number;
            alcoholTax: number;
            total: number;
        };
        transactionType: {
            name: string;
            orders: number;
            amount: number;
        }[];
        orderingType: {
            name: string;
            orders: number;
            amount: number;
        }[];
        payoutCardSettlement: {
            type: string;
            sales: number;
            tips: number;
            tax: number;
            amount: number;
        }[];
        thirdPartyReport: {
            name: string;
            orders: number;
            sales: number;
            tax: number;
            tips: number;
            total: number;
        }[];
        deMinimisSummary: {
            orders: number;
            amount: number;
            salesTax: number;
            total: number;
        };
        voidOrderSummary: {
            orders: number;
            amount: number;
            salesTax: number;
            total: number;
        };
        datasets: any[];
    }>;
}
