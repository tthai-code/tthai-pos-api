import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { TransactionService } from 'src/modules/transaction/services/transaction.service';
import { AdminReportPaginateDto } from '../dto/admin-report.dto';
export declare class AdminReportLogic {
    private readonly restaurantService;
    private readonly transactionService;
    constructor(restaurantService: RestaurantService, transactionService: TransactionService);
    private numberToFixed;
    private summaryTransactionItem;
    private groupBy;
    private groupBySummary;
    getReportForAdmin(query: AdminReportPaginateDto): Promise<any>;
}
