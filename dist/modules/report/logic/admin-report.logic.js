"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminReportLogic = void 0;
const common_1 = require("@nestjs/common");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const transaction_service_1 = require("../../transaction/services/transaction.service");
let AdminReportLogic = class AdminReportLogic {
    constructor(restaurantService, transactionService) {
        this.restaurantService = restaurantService;
        this.transactionService = transactionService;
    }
    numberToFixed(number, digit) {
        return Number(number.toFixed(digit));
    }
    summaryTransactionItem(items, key) {
        return (items
            .map((item) => item[key])
            .reduce((sum, current) => sum + current, 0) || 0);
    }
    groupBy(array, key) {
        const groupByObject = array.reduce((r, v, i, a, k = v[key]) => ((r[k] || (r[k] = [])).push(v), r), {});
        const transform = Object.entries(groupByObject).map((e) => ({
            restaurantId: e[0],
            data: e[1]
        }));
        return transform;
    }
    groupBySummary(array, key, restaurants) {
        const groupByObject = array.reduce((r, v, i, a, k = v[key]) => ((r[k] || (r[k] = [])).push(v), r), {});
        const result = [];
        const transform = Object.entries(groupByObject).map((e) => {
            var _a;
            return ({
                restaurantId: e[0],
                totalOrders: (_a = e[1]) === null || _a === void 0 ? void 0 : _a.length,
                grossSales: this.numberToFixed(this.summaryTransactionItem(e[1], 'subtotal'), 2),
                net: this.numberToFixed(this.summaryTransactionItem(e[1], 'total'), 2)
            });
        });
        for (const restaurant of restaurants) {
            const index = transform.findIndex((item) => item.restaurantId === restaurant.id);
            if (index !== -1) {
                result.push(Object.assign(Object.assign({}, transform[index]), { restaurantName: restaurant.dba || restaurant.legalBusinessName }));
            }
            else {
                result.push({
                    restaurantId: restaurant.id,
                    totalOrders: 0,
                    grossSales: 0,
                    net: 0,
                    restaurantName: restaurant.dba || restaurant.legalBusinessName
                });
            }
        }
        return result;
    }
    async getReportForAdmin(query) {
        const restaurants = await this.restaurantService.paginate(query.restaurantBuildQuery(), query);
        const restaurantIds = restaurants.docs.map((item) => item.id);
        const transactions = await this.transactionService.getAll(query.buildQuery(restaurantIds));
        const orders = [];
        for (const transaction of transactions) {
            const index = orders.findIndex((item) => item.orderId === transaction.orderId);
            if (index === -1)
                orders.push(transaction);
        }
        const groupByRestaurant = this.groupBySummary(orders, 'restaurantId', restaurants.docs);
        return Object.assign(Object.assign({}, restaurants), { docs: groupByRestaurant });
    }
};
AdminReportLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        transaction_service_1.TransactionService])
], AdminReportLogic);
exports.AdminReportLogic = AdminReportLogic;
//# sourceMappingURL=admin-report.logic.js.map