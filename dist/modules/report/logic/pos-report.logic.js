"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSReportLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs = require("dayjs");
const status_enum_1 = require("../../../common/enum/status.enum");
const provider_enum_1 = require("../../kitchen-hub/common/provider.enum");
const order_state_enum_1 = require("../../order/enum/order-state.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const order_online_service_1 = require("../../order/services/order-online.service");
const order_service_1 = require("../../order/services/order.service");
const third_party_order_service_1 = require("../../order/services/third-party-order.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const payment_method_enum_1 = require("../../transaction/common/payment-method.enum");
const payment_status_enum_1 = require("../../transaction/common/payment-status.enum");
const transaction_service_1 = require("../../transaction/services/transaction.service");
const mongoose_1 = require("mongoose");
let POSReportLogic = class POSReportLogic {
    constructor(orderService, thirdPartyOrderService, transactionService, restaurantService, onlineOrderService) {
        this.orderService = orderService;
        this.thirdPartyOrderService = thirdPartyOrderService;
        this.transactionService = transactionService;
        this.restaurantService = restaurantService;
        this.onlineOrderService = onlineOrderService;
    }
    summaryTransactionItem(items, key) {
        return (items
            .map((item) => item[key])
            .reduce((sum, current) => sum + current, 0) || 0);
    }
    getOffset(timeZone) {
        try {
            const timeZoneName = Intl.DateTimeFormat('ia', {
                timeZoneName: 'short',
                timeZone
            })
                .formatToParts()
                .find((i) => i.type === 'timeZoneName').value;
            const offset = timeZoneName.slice(3);
            if (!offset)
                return 0;
            const matchData = offset.match(/([+-])(\d+)(?::(\d+))?/);
            if (!matchData)
                throw `cannot parse timezone name: ${timeZoneName}`;
            const [, sign, hour, minute] = matchData;
            let result = parseInt(hour) * 60;
            if (sign === '+')
                result *= -1;
            if (minute)
                result += parseInt(minute);
            return result;
        }
        catch (e) {
            return 0;
        }
    }
    async getSaleReport(restaurantId, startDate, endDate, shiftStartAt, shiftEndAt) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const tzoffset = this.getOffset(restaurant.timeZone);
        const aggregatePipeline = [
            {
                $match: {
                    restaurantId: new mongoose_1.Types.ObjectId(restaurantId),
                    openDate: {
                        $gte: dayjs(startDate).toDate(),
                        $lte: dayjs(endDate).toDate()
                    },
                    isVoidBeforeTransaction: false,
                    status: status_enum_1.StatusEnum.ACTIVE
                }
            }
        ];
        if (shiftStartAt && shiftEndAt) {
            aggregatePipeline.push({
                $addFields: {
                    minutes: {
                        $add: [
                            { $multiply: [{ $hour: '$openDate' }, 60] },
                            { $minute: '$openDate' }
                        ]
                    }
                }
            });
            let startTimeHour = Number(shiftStartAt.split(':')[0]) + parseInt((tzoffset / 60));
            const startTimeMin = Number(shiftStartAt.split(':')[0]) + parseInt((tzoffset % 60));
            let endTimeHour = Number(shiftEndAt.split(':')[0]) + parseInt((tzoffset / 60));
            const endTimeMin = Number(shiftEndAt.split(':')[0]) + parseInt((tzoffset % 60));
            if (startTimeHour > 23) {
                startTimeHour -= 24;
            }
            if (endTimeHour > 23) {
                endTimeHour -= 24;
            }
            const startTime = startTimeHour * 60 + startTimeMin;
            const endTime = endTimeHour * 60 + endTimeMin;
            if (endTime < startTime) {
                aggregatePipeline.push({
                    $match: {
                        $or: [
                            {
                                minutes: {
                                    $gte: startTime
                                }
                            },
                            {
                                minutes: {
                                    $lte: endTime
                                }
                            }
                        ]
                    }
                });
            }
            else {
                aggregatePipeline.push({
                    $match: {
                        minutes: {
                            $gte: startTime,
                            $lte: endTime
                        }
                    }
                });
            }
        }
        const rawTransactionData = await this.transactionService.aggregate(aggregatePipeline);
        const transactionData = rawTransactionData.filter((item) => item.paymentMethod !== payment_method_enum_1.PaymentMethodEnum.DE_MINIMIS &&
            item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.VOID);
        const deMinimisTransactionData = rawTransactionData.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.DE_MINIMIS &&
            item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.VOID);
        const refundedTransactionData = rawTransactionData.filter((item) => item.paymentStatus === payment_status_enum_1.PaymentStatusEnum.REFUNDED);
        const rawOrderIds = rawTransactionData.map((item) => item.orderId);
        const orderIds = transactionData.map((item) => item.orderId);
        const deMinimisOrderIds = deMinimisTransactionData.map((item) => item.orderId);
        const rawOrderData = await this.orderService.getAll({
            _id: { $in: rawOrderIds }
        });
        const rawTpOrderData = await this.thirdPartyOrderService.getAll({
            _id: { $in: rawOrderIds }
        });
        const rawOnlineOrderData = await this.onlineOrderService.getAll({
            _id: { $in: rawOrderIds }
        });
        const orderData = rawOrderData.filter((item) => orderIds.includes(item.id));
        const tpOrderData = rawTpOrderData.filter((item) => orderIds.includes(item.id));
        const onlineOrderData = rawOnlineOrderData.filter((item) => orderIds.includes(item.id));
        const deMinimisOrder = rawOrderData.filter((item) => deMinimisOrderIds.includes(item.id));
        const voidOrderData = rawOrderData.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.VOID);
        const voidTpOrderData = rawTpOrderData.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.VOID);
        const voidOnlineOrderData = rawOnlineOrderData.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.VOID);
        const voidOrder = [
            ...voidOrderData,
            ...voidTpOrderData,
            ...voidOnlineOrderData
        ];
        const summaryGrossSale = this.summaryTransactionItem(orderData, 'subtotal') +
            this.summaryTransactionItem(tpOrderData, 'subtotal') +
            this.summaryTransactionItem(onlineOrderData, 'subtotal');
        const summaryRefund = this.summaryTransactionItem(refundedTransactionData, 'refundedAmount');
        const summaryDiscount = this.summaryTransactionItem(orderData, 'discount') +
            this.summaryTransactionItem(tpOrderData, 'discount') +
            this.summaryTransactionItem(onlineOrderData, 'discount');
        const summaryNet = summaryGrossSale - summaryDiscount - summaryRefund;
        const summaryTips = this.summaryTransactionItem(transactionData, 'tips');
        const summaryServiceCharges = this.summaryTransactionItem(orderData, 'serviceCharge') +
            this.summaryTransactionItem(tpOrderData, 'serviceCharge') +
            this.summaryTransactionItem(onlineOrderData, 'serviceCharge');
        const summaryConvenienceFee = this.summaryTransactionItem(orderData, 'convenienceFee') +
            this.summaryTransactionItem(tpOrderData, 'convenienceFee') +
            this.summaryTransactionItem(onlineOrderData, 'convenienceFee');
        const summaryTax = this.summaryTransactionItem(orderData, 'tax') +
            this.summaryTransactionItem(tpOrderData, 'tax') +
            this.summaryTransactionItem(onlineOrderData, 'tax');
        const summaryAlcoholTax = this.summaryTransactionItem(orderData, 'alcoholTax') +
            this.summaryTransactionItem(tpOrderData, 'alcoholTax') +
            this.summaryTransactionItem(onlineOrderData, 'alcoholTax');
        const summarySales = [
            {
                label: 'Gross Sales',
                sales: summaryGrossSale,
                refunds: summaryRefund,
                net: summaryGrossSale - summaryRefund
            },
            {
                label: 'Discount',
                sales: summaryDiscount,
                refunds: 0,
                net: summaryDiscount
            },
            {
                label: 'Net Sales',
                sales: summaryNet,
                refunds: summaryRefund,
                net: summaryNet - summaryRefund
            },
            {
                label: 'Tips',
                sales: summaryTips,
                refunds: 0,
                net: summaryTips
            },
            {
                label: 'Service Charges',
                sales: summaryServiceCharges,
                refunds: 0,
                net: summaryServiceCharges
            },
            {
                label: 'Convenience Fee',
                sales: summaryConvenienceFee,
                refunds: 0,
                net: summaryConvenienceFee
            },
            {
                label: 'Sales Tax',
                sales: summaryTax,
                refunds: 0,
                net: summaryTax
            },
            {
                label: 'Alcohol Tax',
                sales: summaryAlcoholTax,
                refunds: 0,
                net: summaryAlcoholTax
            },
            {
                label: 'Total',
                sales: summaryNet +
                    summaryTips +
                    summaryServiceCharges +
                    summaryTax +
                    summaryAlcoholTax,
                refunds: summaryRefund,
                net: summaryNet -
                    summaryRefund +
                    summaryTips +
                    summaryServiceCharges +
                    summaryTax +
                    summaryAlcoholTax
            }
        ];
        const transactionTypeCash = transactionData.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CASH);
        const transactionTypeCredit = transactionData.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CREDIT);
        const transactionTypeDebit = transactionData.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.DEBIT);
        const transactionTypeCheck = transactionData.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CHECK);
        const transactionTypeGiftCard = transactionData.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.GIFT_CARD);
        const transactionTypeDelivery = transactionData.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.DELIVERY);
        const transactionTypeOther = transactionData.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.OTHER);
        const summaryCash = this.summaryTransactionItem(transactionTypeCash, 'totalPaid');
        const summaryCredit = this.summaryTransactionItem(transactionTypeCredit, 'totalPaid');
        const summaryDebit = this.summaryTransactionItem(transactionTypeDebit, 'totalPaid');
        const summaryCard = summaryCredit + summaryDebit;
        const summaryCheck = this.summaryTransactionItem(transactionTypeCheck, 'totalPaid');
        const summaryGiftCard = this.summaryTransactionItem(transactionTypeGiftCard, 'totalPaid');
        const summaryDelivery = this.summaryTransactionItem(transactionTypeDelivery, 'totalPaid');
        const summaryDeMinimus = this.summaryTransactionItem(deMinimisTransactionData, 'totalPaid');
        const summaryOther = this.summaryTransactionItem(transactionTypeOther, 'totalPaid');
        const summaryTransactionType = [
            {
                label: 'Cash',
                orders: transactionTypeCash.length,
                amount: summaryCash
            },
            {
                label: 'Credit Card',
                orders: transactionTypeCredit.length,
                amount: summaryCredit
            },
            {
                label: 'Debit Card',
                orders: transactionTypeDebit.length,
                amount: summaryDebit
            },
            {
                label: 'Check',
                orders: transactionTypeCheck.length,
                amount: summaryCheck
            },
            {
                label: 'Gift Card',
                orders: transactionTypeGiftCard.length,
                amount: summaryGiftCard
            },
            {
                label: 'Delivery',
                orders: transactionTypeDelivery.length,
                amount: summaryDelivery
            },
            {
                label: 'Fringe Benefit',
                orders: deMinimisTransactionData.length,
                amount: summaryDeMinimus
            },
            {
                label: 'Other',
                orders: transactionTypeOther.length,
                amount: summaryOther
            }
        ];
        const orderTypeDineIn = orderData.filter((item) => item.orderType === order_type_enum_1.OrderTypeEnum.DINE_IN);
        const summaryTypeDineIn = orderTypeDineIn.length;
        const transactionTypeDineIn = transactionData.filter((item) => item.orderType === order_type_enum_1.OrderTypeEnum.DINE_IN);
        const orderTypeToGo = orderData.filter((item) => item.orderType === order_type_enum_1.OrderTypeEnum.TO_GO);
        const summaryTypeToGo = orderTypeToGo.length;
        const transactionTypeToGo = transactionData.filter((item) => item.orderType === order_type_enum_1.OrderTypeEnum.TO_GO);
        const summaryTypeOnline = onlineOrderData.length;
        const transactionTypeOnline = transactionData.filter((item) => item.orderType === order_type_enum_1.OrderTypeEnum.ONLINE);
        const summaryOrderType = [
            {
                label: 'Dine In',
                orders: summaryTypeDineIn,
                amount: this.summaryTransactionItem(transactionTypeDineIn, 'totalPaid')
            },
            {
                label: 'To Go',
                orders: summaryTypeToGo,
                amount: this.summaryTransactionItem(transactionTypeToGo, 'totalPaid')
            },
            {
                label: 'Online',
                orders: summaryTypeOnline,
                amount: this.summaryTransactionItem(transactionTypeOnline, 'totalPaid')
            },
            {
                label: 'EatsZaab',
                orders: 0,
                amount: 0
            }
        ];
        const summaryPayout = [
            {
                label: 'Cash',
                amount: summaryCash
            },
            {
                label: 'Credit/Debit',
                amount: summaryCard
            }
        ];
        const orderDoorDash = tpOrderData.filter((item) => item.deliverySource === provider_enum_1.ProviderEnum.DOORDASH);
        const transactionDoorDash = transactionData.filter((item) => item.deliverySource === provider_enum_1.ProviderEnum.DOORDASH);
        const salesDoorDash = this.summaryTransactionItem(orderDoorDash, 'subtotal');
        const taxDoorDash = this.summaryTransactionItem(orderDoorDash, 'tax') +
            this.summaryTransactionItem(orderDoorDash, 'alcoholTax');
        const tipsDoorDash = this.summaryTransactionItem(transactionDoorDash, 'tips');
        const orderUberEats = tpOrderData.filter((item) => item.deliverySource === provider_enum_1.ProviderEnum.UBEREATS);
        const transactionUberEats = transactionData.filter((item) => item.deliverySource === provider_enum_1.ProviderEnum.UBEREATS);
        const salesUberEats = this.summaryTransactionItem(orderUberEats, 'subtotal');
        const taxUberEats = this.summaryTransactionItem(orderUberEats, 'tax') +
            this.summaryTransactionItem(orderUberEats, 'alcoholTax');
        const tipsUberEats = this.summaryTransactionItem(transactionUberEats, 'tips');
        const orderGrubHub = tpOrderData.filter((item) => item.deliverySource === provider_enum_1.ProviderEnum.GRUBHUB);
        const transactionGrubHub = transactionData.filter((item) => item.deliverySource === provider_enum_1.ProviderEnum.GRUBHUB);
        const salesGrubHub = this.summaryTransactionItem(orderGrubHub, 'subtotal');
        const taxGrubHub = this.summaryTransactionItem(orderGrubHub, 'tax') +
            this.summaryTransactionItem(orderGrubHub, 'alcoholTax');
        const tipsGrubHub = this.summaryTransactionItem(transactionGrubHub, 'tips');
        const orderGloriaFood = tpOrderData.filter((item) => item.deliverySource === provider_enum_1.ProviderEnum.GLORIAFOOD);
        const transactionGloriaFood = transactionData.filter((item) => item.deliverySource === provider_enum_1.ProviderEnum.GLORIAFOOD);
        const salesGloriaFood = this.summaryTransactionItem(orderGloriaFood, 'subtotal');
        const taxGloriaFood = this.summaryTransactionItem(orderGloriaFood, 'tax') +
            this.summaryTransactionItem(orderGloriaFood, 'alcoholTax');
        const tipsGloriaFood = this.summaryTransactionItem(transactionGloriaFood, 'tips');
        const thirdPartyTotal = {
            label: 'Total',
            orders: orderDoorDash.length +
                orderUberEats.length +
                orderGrubHub.length +
                orderGloriaFood.length,
            sales: salesDoorDash + salesUberEats + salesGrubHub + salesGloriaFood,
            tax: taxDoorDash + taxUberEats + taxGrubHub + taxGloriaFood,
            tips: tipsDoorDash + tipsUberEats + tipsGrubHub + tipsGloriaFood
        };
        const summaryThirdParty = [
            {
                label: 'DoorDash',
                orders: orderDoorDash.length,
                sales: salesDoorDash,
                tax: taxDoorDash,
                tips: tipsDoorDash
            },
            {
                label: 'UberEats',
                orders: orderUberEats.length,
                sales: salesUberEats,
                tax: taxUberEats,
                tips: tipsUberEats
            },
            {
                label: 'GrubHub',
                orders: orderGrubHub.length,
                sales: salesGrubHub,
                tax: taxGrubHub,
                tips: tipsGrubHub
            },
            {
                label: 'GloriaFood',
                orders: orderGloriaFood.length,
                sales: salesGloriaFood,
                tax: taxGloriaFood,
                tips: tipsGloriaFood
            },
            thirdPartyTotal
        ];
        const deMinisAmount = this.summaryTransactionItem(deMinimisOrder, 'subtotal');
        const deMinisSalesTax = this.summaryTransactionItem(deMinimisOrder, 'tax');
        const SummaryDeMinimis = [
            {
                label: 'Orders',
                amount: deMinimisOrder.length
            },
            {
                label: 'Amount',
                amount: deMinisAmount
            },
            {
                label: 'Sales Tax',
                amount: deMinisSalesTax
            },
            {
                label: 'Total',
                amount: deMinisAmount + deMinisSalesTax
            }
        ];
        const voidAmount = this.summaryTransactionItem(voidOrder, 'subtotal');
        const voidSalesTax = this.summaryTransactionItem(voidOrder, 'tax');
        const SummaryVoidOrder = [
            {
                label: 'Orders',
                amount: voidOrder.length
            },
            {
                label: 'Amount',
                amount: voidAmount
            },
            {
                label: 'Sales Tax',
                amount: voidSalesTax
            },
            {
                label: 'Total',
                amount: voidAmount + voidSalesTax
            }
        ];
        return {
            summarySales,
            summaryTransactionType,
            summaryOrderType,
            summaryPayout,
            summaryThirdParty,
            SummaryVoidOrder,
            SummaryDeMinimis
        };
    }
};
POSReportLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [order_service_1.OrderService,
        third_party_order_service_1.ThirdPartyOrderService,
        transaction_service_1.TransactionService,
        restaurant_service_1.RestaurantService,
        order_online_service_1.OrderOnlineService])
], POSReportLogic);
exports.POSReportLogic = POSReportLogic;
//# sourceMappingURL=pos-report.logic.js.map