import { ResponseDto } from 'src/common/entity/response.entity';
declare class LabelObject {
    label: string;
}
declare class LabelAmountObject extends LabelObject {
    amount: number;
}
declare class LabelAmountOrderObject extends LabelAmountObject {
    orders: number;
}
declare class SummarySalesObject extends LabelObject {
    sales: number;
    refunds: number;
    net: number;
}
declare class SummaryThirdPartyObject extends LabelObject {
    orders: number;
    sales: number;
    tax: number;
    tips: number;
}
declare class POSReportObject {
    summary_sales: SummarySalesObject[];
    summary_transaction_type: LabelAmountOrderObject[];
    summary_order_type: LabelAmountOrderObject[];
    summary_payout: LabelAmountObject[];
    summary_third_party: SummaryThirdPartyObject[];
    summary_void_order: LabelAmountObject[];
    summary_de_minimis: LabelAmountObject[];
}
export declare class POSReportResponse extends ResponseDto<POSReportObject> {
    data: POSReportObject;
}
export {};
