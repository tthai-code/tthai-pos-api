import { ResponseDto } from 'src/common/entity/response.entity';
declare class SummaryObject {
    gross: number;
    discount: number;
    refund: number;
    net: number;
    tips: number;
    service_charges: number;
    convenience_fee: number;
    sale_tax: number;
    alcohol_tax: number;
    total: number;
}
declare class NameOrderObject {
    name: string;
    orders: number;
    amount: number;
}
declare class SaleTipTaxObject {
    sales: number;
    tax: number;
    tips: number;
}
declare class PayoutCardObject extends SaleTipTaxObject {
    type: string;
    amount: number;
}
declare class ThirdPartyReportObject extends SaleTipTaxObject {
    name: string;
    orders: number;
}
declare class ReportObject {
    summary: SummaryObject;
    transaction_type: NameOrderObject[];
    ordering_type: NameOrderObject[];
    payout_card_settlement: PayoutCardObject[];
    third_party_report: ThirdPartyReportObject[];
    datasets: number[];
}
export declare class ReportResponse extends ResponseDto<ReportObject> {
    data: ReportObject;
}
export {};
