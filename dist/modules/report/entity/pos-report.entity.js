"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSReportResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class LabelObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], LabelObject.prototype, "label", void 0);
class LabelAmountObject extends LabelObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], LabelAmountObject.prototype, "amount", void 0);
class LabelAmountOrderObject extends LabelAmountObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], LabelAmountOrderObject.prototype, "orders", void 0);
class SummarySalesObject extends LabelObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummarySalesObject.prototype, "sales", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummarySalesObject.prototype, "refunds", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummarySalesObject.prototype, "net", void 0);
class SummaryThirdPartyObject extends LabelObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryThirdPartyObject.prototype, "orders", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryThirdPartyObject.prototype, "sales", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryThirdPartyObject.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryThirdPartyObject.prototype, "tips", void 0);
class POSReportObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [SummarySalesObject] }),
    __metadata("design:type", Array)
], POSReportObject.prototype, "summary_sales", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [LabelAmountOrderObject] }),
    __metadata("design:type", Array)
], POSReportObject.prototype, "summary_transaction_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [LabelAmountOrderObject] }),
    __metadata("design:type", Array)
], POSReportObject.prototype, "summary_order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [LabelAmountObject] }),
    __metadata("design:type", Array)
], POSReportObject.prototype, "summary_payout", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [SummaryThirdPartyObject] }),
    __metadata("design:type", Array)
], POSReportObject.prototype, "summary_third_party", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [LabelAmountObject] }),
    __metadata("design:type", Array)
], POSReportObject.prototype, "summary_void_order", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [LabelAmountObject] }),
    __metadata("design:type", Array)
], POSReportObject.prototype, "summary_de_minimis", void 0);
class POSReportResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: POSReportObject,
        example: {
            summary_sales: [
                {
                    label: 'Gross Sales',
                    sales: 30,
                    refunds: 0,
                    net: 30
                },
                {
                    label: 'Discount',
                    sales: 15,
                    refunds: 0,
                    net: 15
                },
                {
                    label: 'Net Sales',
                    sales: 15,
                    refunds: 0,
                    net: 15
                },
                {
                    label: 'Tips',
                    sales: 0,
                    refunds: 0,
                    net: 0
                },
                {
                    label: 'Service Charges',
                    sales: 3,
                    refunds: 0,
                    net: 3
                },
                {
                    label: 'Convenience Fee',
                    sales: 0,
                    refunds: 0,
                    net: 0
                },
                {
                    label: 'Sales Tax',
                    sales: 3,
                    refunds: 0,
                    net: 3
                },
                {
                    label: 'Alcohol Tax',
                    sales: 0,
                    refunds: 0,
                    net: 0
                },
                {
                    label: 'Total',
                    sales: 21,
                    refunds: 0,
                    net: 21
                }
            ],
            summary_transaction_type: [
                {
                    label: 'Cash',
                    orders: 1,
                    amount: 21
                },
                {
                    label: 'Credit Card',
                    orders: 0,
                    amount: 0
                },
                {
                    label: 'Debit Card',
                    orders: 0,
                    amount: 0
                },
                {
                    label: 'Check',
                    orders: 0,
                    amount: 0
                },
                {
                    label: 'Gift Card',
                    orders: 0,
                    amount: 0
                },
                {
                    label: 'Delivery',
                    orders: 0,
                    amount: 0
                },
                {
                    label: 'De Minimus',
                    orders: 1,
                    amount: 11
                },
                {
                    label: 'Other',
                    orders: 0,
                    amount: 0
                }
            ],
            summary_order_type: [
                {
                    label: 'Dine In',
                    orders: 0,
                    amount: 0
                },
                {
                    label: 'To Go',
                    orders: 1,
                    amount: 21
                },
                {
                    label: 'Online',
                    orders: 0,
                    amount: 0
                },
                {
                    label: 'EatsZaab',
                    orders: 0,
                    amount: 0
                }
            ],
            summary_payout: [
                {
                    label: 'Cash',
                    amount: 21
                },
                {
                    label: 'Credit/Debit',
                    amount: 0
                }
            ],
            summary_third_party: [
                {
                    label: 'DoorDash',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                },
                {
                    label: 'UberEats',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                },
                {
                    label: 'GrubHub',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                },
                {
                    label: 'GloriaFood',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                },
                {
                    label: 'Total',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                }
            ],
            summary_void_order: [
                {
                    label: 'Orders',
                    amount: 1
                },
                {
                    label: 'Amount',
                    amount: 10
                },
                {
                    label: 'Sales Tax',
                    amount: 1
                },
                {
                    label: 'Total',
                    amount: 11
                }
            ],
            summary_de_minimis: [
                {
                    label: 'Orders',
                    amount: 1
                },
                {
                    label: 'Amount',
                    amount: 10
                },
                {
                    label: 'Sales Tax',
                    amount: 1
                },
                {
                    label: 'Total',
                    amount: 11
                }
            ]
        }
    }),
    __metadata("design:type", POSReportObject)
], POSReportResponse.prototype, "data", void 0);
exports.POSReportResponse = POSReportResponse;
//# sourceMappingURL=pos-report.entity.js.map