"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class SummaryObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "gross", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "refund", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "net", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "tips", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "service_charges", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "convenience_fee", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "sale_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryObject.prototype, "total", void 0);
class NameOrderObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], NameOrderObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], NameOrderObject.prototype, "orders", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], NameOrderObject.prototype, "amount", void 0);
class SaleTipTaxObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SaleTipTaxObject.prototype, "sales", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SaleTipTaxObject.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SaleTipTaxObject.prototype, "tips", void 0);
class PayoutCardObject extends SaleTipTaxObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PayoutCardObject.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], PayoutCardObject.prototype, "amount", void 0);
class ThirdPartyReportObject extends SaleTipTaxObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ThirdPartyReportObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ThirdPartyReportObject.prototype, "orders", void 0);
class ReportObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: SummaryObject }),
    __metadata("design:type", SummaryObject)
], ReportObject.prototype, "summary", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [NameOrderObject] }),
    __metadata("design:type", Array)
], ReportObject.prototype, "transaction_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [NameOrderObject] }),
    __metadata("design:type", Array)
], ReportObject.prototype, "ordering_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [PayoutCardObject] }),
    __metadata("design:type", Array)
], ReportObject.prototype, "payout_card_settlement", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ThirdPartyReportObject] }),
    __metadata("design:type", Array)
], ReportObject.prototype, "third_party_report", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], ReportObject.prototype, "datasets", void 0);
class ReportResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: ReportObject,
        example: {
            summary: {
                gross: 240,
                discount: 0,
                refund: 0,
                net: 240,
                tips: 0,
                service_charges: 0,
                convenience_fee: 0,
                sale_tax: 16.799999999999997,
                alcohol_tax: 24,
                total: 280.8
            },
            transaction_type: [
                {
                    name: 'Cash',
                    orders: 2,
                    amount: 187.2
                },
                {
                    name: 'Credit Card',
                    orders: 1,
                    amount: 93.6
                },
                {
                    name: 'Debit Card',
                    orders: 0,
                    amount: 0
                }
            ],
            ordering_type: [
                {
                    name: 'Dine In',
                    orders: 0,
                    amount: 0
                },
                {
                    name: 'To Go',
                    orders: 3,
                    amount: 0
                },
                {
                    name: 'Online',
                    orders: 0,
                    amount: 0
                },
                {
                    name: 'TThai App',
                    orders: 0,
                    amount: 0
                }
            ],
            payout_card_settlement: [
                {
                    type: 'Credit',
                    sales: 0,
                    tips: 0,
                    tax: 0,
                    amount: 0
                },
                {
                    type: 'Debit',
                    sales: 0,
                    tips: 0,
                    tax: 0,
                    amount: 0
                }
            ],
            third_party_report: [
                {
                    name: 'DoorDash',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                },
                {
                    name: 'UberEats',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                },
                {
                    name: 'GrubHub',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                },
                {
                    name: 'GloriaFood',
                    orders: 0,
                    sales: 0,
                    tax: 0,
                    tips: 0
                }
            ],
            datasets: [0, 0, 0, 240, 0, 0, 0]
        }
    }),
    __metadata("design:type", ReportObject)
], ReportResponse.prototype, "data", void 0);
exports.ReportResponse = ReportResponse;
//# sourceMappingURL=report.entity.js.map