import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class AdminReportObject {
    restaurant_id: string;
    restaurant_name: string;
    totalOrders: number;
    grossSales: number;
    net: number;
}
declare class AdminReportPaginateObject extends PaginateResponseDto<AdminReportObject[]> {
    results: AdminReportObject[];
}
export declare class AdminReportPaginateResponse extends ResponseDto<AdminReportPaginateObject> {
    data: AdminReportPaginateObject;
}
export {};
