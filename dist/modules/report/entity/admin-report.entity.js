"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminReportPaginateResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
class AdminReportObject {
}
class AdminReportPaginateObject extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [AdminReportObject],
        example: [
            {
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                total_orders: 3,
                gross_sales: 162.99,
                net: 190.31,
                restaurant_name: 'Holy Beef'
            },
            {
                restaurant_id: '630eff5751c2eac55f52662c',
                total_orders: 0,
                gross_sales: 0,
                net: 0,
                restaurant_name: 'May Thai Kitchen LLC'
            },
            {
                restaurant_id: '63433e8145728de99075847a',
                total_orders: 0,
                gross_sales: 0,
                net: 0,
                restaurant_name: 'New Jeans'
            }
        ]
    }),
    __metadata("design:type", Array)
], AdminReportPaginateObject.prototype, "results", void 0);
class AdminReportPaginateResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: AdminReportPaginateObject }),
    __metadata("design:type", AdminReportPaginateObject)
], AdminReportPaginateResponse.prototype, "data", void 0);
exports.AdminReportPaginateResponse = AdminReportPaginateResponse;
//# sourceMappingURL=admin-report.entity.js.map