"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetSaleReport = exports.GetSaleReportPOS = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const report_date_enum_1 = require("../common/report-date.enum");
class GetSaleReportPOS {
    constructor() {
        this.shiftStartAt = '';
    }
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], GetSaleReportPOS.prototype, "startDate", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], GetSaleReportPOS.prototype, "endDate", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ example: '12:00', default: '' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], GetSaleReportPOS.prototype, "shiftStartAt", void 0);
__decorate([
    (0, swagger_1.ApiPropertyOptional)({ example: '12:00', default: '' }),
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], GetSaleReportPOS.prototype, "shiftEndAt", void 0);
exports.GetSaleReportPOS = GetSaleReportPOS;
class ShiftFieldsDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: '12:00' }),
    __metadata("design:type", String)
], ShiftFieldsDto.prototype, "startAt", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: '12:00' }),
    __metadata("design:type", String)
], ShiftFieldsDto.prototype, "endAt", void 0);
class GetSaleReport extends GetSaleReportPOS {
}
__decorate([
    (0, class_validator_1.IsIn)(Object.values(report_date_enum_1.ReportDateEnum)),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({
        example: report_date_enum_1.ReportDateEnum.TODAY,
        enum: Object.values(report_date_enum_1.ReportDateEnum),
        description: 'using for transform data to display graph.'
    }),
    __metadata("design:type", String)
], GetSaleReport.prototype, "reportType", void 0);
exports.GetSaleReport = GetSaleReport;
//# sourceMappingURL=get-sale-report.dto.js.map