export declare class GetSaleReportPOS {
    readonly startDate: string;
    readonly endDate: string;
    readonly shiftStartAt: string | null | undefined;
    readonly shiftEndAt: string | null | undefined;
}
export declare class GetSaleReport extends GetSaleReportPOS {
    readonly reportType: string;
}
