import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
import { PaymentStatusEnum } from 'src/modules/transaction/common/payment-status.enum';
export declare class AdminReportPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly startDate: string;
    readonly endDate: string;
    restaurantBuildQuery(): {
        $or: ({
            legalBusinessName: {
                $regex: string;
                $options: string;
            };
            dba?: undefined;
        } | {
            dba: {
                $regex: string;
                $options: string;
            };
            legalBusinessName?: undefined;
        })[];
        status: StatusEnum;
    };
    buildQuery(restaurantIds: any[]): {
        restaurantId: {
            $in: any[];
        };
        paymentStatus: PaymentStatusEnum;
        closeDate: {
            $gte: Date;
            $lte: Date;
        };
        status: StatusEnum;
    };
}
