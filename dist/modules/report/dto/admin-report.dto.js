"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminReportPaginateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const dayjs = require("dayjs");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
const status_enum_1 = require("../../../common/enum/status.enum");
const payment_status_enum_1 = require("../../transaction/common/payment-status.enum");
class AdminReportPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'legalBusinessName';
        this.sortOrder = 'asc';
        this.search = '';
    }
    restaurantBuildQuery() {
        const result = {
            $or: [
                {
                    legalBusinessName: {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    dba: {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            status: status_enum_1.StatusEnum.ACTIVE
        };
        if (!this.search)
            delete result['$or'];
        return result;
    }
    buildQuery(restaurantIds) {
        const result = {
            restaurantId: { $in: restaurantIds },
            paymentStatus: payment_status_enum_1.PaymentStatusEnum.PAID,
            closeDate: {
                $gte: dayjs(this.startDate)
                    .set('second', 0)
                    .set('millisecond', 0)
                    .toDate(),
                $lte: dayjs(this.endDate)
                    .set('second', 59)
                    .set('millisecond', 999)
                    .toDate()
            },
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminReportPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AdminReportPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Tim' }),
    __metadata("design:type", String)
], AdminReportPaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], AdminReportPaginateDto.prototype, "startDate", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], AdminReportPaginateDto.prototype, "endDate", void 0);
exports.AdminReportPaginateDto = AdminReportPaginateDto;
//# sourceMappingURL=admin-report.dto.js.map