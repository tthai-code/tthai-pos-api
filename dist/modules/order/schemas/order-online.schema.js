"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderOnlineSchema = exports.OrderOnlineFields = exports.CustomerFields = exports.RestaurantFields = exports.ItemFields = exports.StaffFields = exports.TableFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
const order_method_enum_1 = require("../enum/order-method.enum");
const payment_status_enum_1 = require("../enum/payment-status.enum");
const restaurant_schema_1 = require("../../restaurant/schemas/restaurant.schema");
let TableFields = class TableFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], TableFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], TableFields.prototype, "tableNo", void 0);
TableFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], TableFields);
exports.TableFields = TableFields;
let StaffFields = class StaffFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], StaffFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], StaffFields.prototype, "firstName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], StaffFields.prototype, "lastName", void 0);
StaffFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], StaffFields);
exports.StaffFields = StaffFields;
let ModifierFields = class ModifierFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierFields.prototype, "label", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierFields.prototype, "abbreviation", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierFields.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierFields.prototype, "nativeName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], ModifierFields.prototype, "price", void 0);
ModifierFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], ModifierFields);
let ItemFields = class ItemFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], ItemFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ItemFields.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ItemFields.prototype, "nativeName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], ItemFields.prototype, "price", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], ItemFields.prototype, "modifiers", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Boolean)
], ItemFields.prototype, "isContainAlcohol", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: null }),
    __metadata("design:type", String)
], ItemFields.prototype, "coverImage", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], ItemFields.prototype, "unit", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Number)
], ItemFields.prototype, "amount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], ItemFields.prototype, "note", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], ItemFields.prototype, "category", void 0);
ItemFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], ItemFields);
exports.ItemFields = ItemFields;
let RestaurantFields = class RestaurantFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], RestaurantFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "legalBusinessName", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", restaurant_schema_1.AddressFields)
], RestaurantFields.prototype, "address", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "dba", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "businessPhone", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "email", void 0);
RestaurantFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], RestaurantFields);
exports.RestaurantFields = RestaurantFields;
let CustomerFields = class CustomerFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CustomerFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 'Guest' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "firstName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "lastName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "tel", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "email", void 0);
CustomerFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], CustomerFields);
exports.CustomerFields = CustomerFields;
let OrderOnlineFields = class OrderOnlineFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], OrderOnlineFields.prototype, "_id", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", RestaurantFields)
], OrderOnlineFields.prototype, "restaurant", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: new CustomerFields() }),
    __metadata("design:type", CustomerFields)
], OrderOnlineFields.prototype, "customer", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", TableFields)
], OrderOnlineFields.prototype, "table", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", StaffFields)
], OrderOnlineFields.prototype, "staff", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true, enum: Object.values(order_type_enum_1.OrderTypeEnum) }),
    __metadata("design:type", String)
], OrderOnlineFields.prototype, "orderType", void 0);
__decorate([
    (0, mongoose_1.Prop)({ min: 1, default: 1 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "numberOfGuest", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", Date)
], OrderOnlineFields.prototype, "orderDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], OrderOnlineFields.prototype, "pickUpDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true, enum: Object.values(order_method_enum_1.OrderMethod) }),
    __metadata("design:type", String)
], OrderOnlineFields.prototype, "orderMethod", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: payment_status_enum_1.OnlinePaymentStatusEnum.UNPAID,
        enum: Object.values(payment_status_enum_1.OnlinePaymentStatusEnum)
    }),
    __metadata("design:type", String)
], OrderOnlineFields.prototype, "paymentStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], OrderOnlineFields.prototype, "summaryItems", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "subtotal", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "discount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "serviceCharge", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "tax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "alcoholTax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "convenienceFee", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "total", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], OrderOnlineFields.prototype, "tips", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(order_state_enum_1.OrderStateEnum),
        default: order_state_enum_1.OrderStateEnum.PENDING
    }),
    __metadata("design:type", String)
], OrderOnlineFields.prototype, "orderStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], OrderOnlineFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], OrderOnlineFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], OrderOnlineFields.prototype, "createdBy", void 0);
OrderOnlineFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: true, collection: 'ordersOnline' })
], OrderOnlineFields);
exports.OrderOnlineFields = OrderOnlineFields;
exports.OrderOnlineSchema = mongoose_1.SchemaFactory.createForClass(OrderOnlineFields);
exports.OrderOnlineSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
exports.OrderOnlineSchema.plugin(mongoosePaginate);
//# sourceMappingURL=order-online.schema.js.map