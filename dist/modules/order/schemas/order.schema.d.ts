import * as mongoose from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
import { AddressFields } from 'src/modules/restaurant/schemas/restaurant.schema';
export declare type OrderDocument = OrderFields & mongoose.Document;
export declare class TableFields {
    id: string;
    tableNo: string;
}
export declare class StaffFields {
    id: string;
    firstName: string;
    lastName: string;
}
export declare class MenuCategoryOrderItemFields {
    id: string;
    name: string;
}
declare class ModifierFields {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    nativeName: string;
    price: number;
}
export declare class ItemFields {
    id: number;
    name: string;
    nativeName: string;
    price: number;
    modifiers: Array<ModifierFields>;
    isContainAlcohol: boolean;
    coverImage: string;
    unit: number;
    amount: number;
    note: string;
    category: MenuCategoryOrderItemFields[];
}
export declare class RestaurantFields {
    id: string;
    legalBusinessName: string;
    dba: string;
    address: AddressFields;
    businessPhone: string;
    email: string;
}
export declare class CustomerFields {
    id: string;
    firstName: string;
    lastName: string;
    tel: string;
}
export declare class OrderFields {
    _id: string;
    restaurant: RestaurantFields;
    customer: CustomerFields;
    table: TableFields;
    staff: StaffFields;
    orderType: string;
    numberOfGuest: number;
    orderDate: Date;
    pickUpDate: Date;
    summaryItems: Array<ItemFields>;
    subtotal: number;
    discount: number;
    serviceCharge: number;
    tax: number;
    alcoholTax: number;
    convenienceFee: number;
    total: number;
    orderStatus: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const OrderSchema: mongoose.Schema<mongoose.Document<OrderFields, any, any>, mongoose.Model<mongoose.Document<OrderFields, any, any>, any, any, any>, any, any>;
export {};
