/// <reference types="mongoose" />
import { MenuCategoryOrderItemFields } from './order.schema';
export declare type OrderItemsDocument = OrderItemsFields & Document;
declare class ModifierFields {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    nativeName: string;
    price: number;
}
export declare class OrderItemsFields {
    orderId: string;
    menuId: string;
    name: string;
    nativeName: string;
    price: number;
    modifiers: Array<ModifierFields>;
    isContainAlcohol: boolean;
    coverImage: string;
    unit: number;
    amount: number;
    note: string;
    category: MenuCategoryOrderItemFields[];
    itemStatus: string;
}
export declare const OrderItemsSchema: import("mongoose").Schema<import("mongoose").Document<OrderItemsFields, any, any>, import("mongoose").Model<import("mongoose").Document<OrderItemsFields, any, any>, any, any, any>, any, any>;
export {};
