import * as mongoose from 'mongoose';
export declare type OrderCounterDocument = OrderCounter & mongoose.Document;
export declare class OrderCounter {
    prefix: string;
    restaurantId: mongoose.Types.ObjectId;
    counter: number;
}
export declare const OrderCounterSchema: mongoose.Schema<mongoose.Document<OrderCounter, any, any>, mongoose.Model<mongoose.Document<OrderCounter, any, any>, any, any, any>, any, any>;
