import * as mongoose from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
import { CustomerFields, RestaurantFields } from './order.schema';
export declare type ThirdPartyOrderDocument = ThirdPartyOrderFields & mongoose.Document;
export declare class StaffThirdPartyFields {
    id: string;
    firstName: string;
    lastName: string;
}
export declare class ModifierThirdPartyFields {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    nativeName: string;
    price: number;
}
export declare class ItemThirdPartyFields {
    id: number;
    name: string;
    nativeName: string;
    price: number;
    modifiers: Array<ModifierThirdPartyFields>;
    isContainAlcohol: boolean;
    coverImage: string;
    unit: number;
    amount: number;
    note: string;
}
export declare class ThirdPartyOrderFields {
    _id: string;
    restaurant: RestaurantFields;
    customer: CustomerFields;
    staff: StaffThirdPartyFields;
    orderType: string;
    orderMethod: string;
    numberOfGuest: number;
    orderDate: Date;
    completedDate: Date;
    pickUpDate: Date;
    asap: boolean;
    scheduledForDate: Date;
    summaryItems: Array<ItemThirdPartyFields>;
    subtotal: number;
    discount: number;
    serviceCharge: number;
    tax: number;
    alcoholTax: number;
    convenienceFee: number;
    tips: number;
    total: number;
    orderStatus: string;
    deliverySource: string;
    orderKHId: number;
    prepareTime: number;
    status: string;
    note: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const ThirdPartyOrderSchema: mongoose.Schema<mongoose.Document<ThirdPartyOrderFields, any, any>, mongoose.Model<mongoose.Document<ThirdPartyOrderFields, any, any>, any, any, any>, any, any>;
