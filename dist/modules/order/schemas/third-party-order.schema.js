"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThirdPartyOrderSchema = exports.ThirdPartyOrderFields = exports.ItemThirdPartyFields = exports.ModifierThirdPartyFields = exports.StaffThirdPartyFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
const order_schema_1 = require("./order.schema");
let StaffThirdPartyFields = class StaffThirdPartyFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], StaffThirdPartyFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], StaffThirdPartyFields.prototype, "firstName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], StaffThirdPartyFields.prototype, "lastName", void 0);
StaffThirdPartyFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], StaffThirdPartyFields);
exports.StaffThirdPartyFields = StaffThirdPartyFields;
let ModifierThirdPartyFields = class ModifierThirdPartyFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ModifierThirdPartyFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ModifierThirdPartyFields.prototype, "label", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ModifierThirdPartyFields.prototype, "abbreviation", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ModifierThirdPartyFields.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ModifierThirdPartyFields.prototype, "nativeName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], ModifierThirdPartyFields.prototype, "price", void 0);
ModifierThirdPartyFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], ModifierThirdPartyFields);
exports.ModifierThirdPartyFields = ModifierThirdPartyFields;
let ItemThirdPartyFields = class ItemThirdPartyFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], ItemThirdPartyFields.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ItemThirdPartyFields.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ItemThirdPartyFields.prototype, "nativeName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], ItemThirdPartyFields.prototype, "price", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], ItemThirdPartyFields.prototype, "modifiers", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Boolean)
], ItemThirdPartyFields.prototype, "isContainAlcohol", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: null }),
    __metadata("design:type", String)
], ItemThirdPartyFields.prototype, "coverImage", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], ItemThirdPartyFields.prototype, "unit", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], ItemThirdPartyFields.prototype, "amount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], ItemThirdPartyFields.prototype, "note", void 0);
ItemThirdPartyFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], ItemThirdPartyFields);
exports.ItemThirdPartyFields = ItemThirdPartyFields;
let ThirdPartyOrderFields = class ThirdPartyOrderFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ThirdPartyOrderFields.prototype, "_id", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", order_schema_1.RestaurantFields)
], ThirdPartyOrderFields.prototype, "restaurant", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: new order_schema_1.CustomerFields() }),
    __metadata("design:type", order_schema_1.CustomerFields)
], ThirdPartyOrderFields.prototype, "customer", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", StaffThirdPartyFields)
], ThirdPartyOrderFields.prototype, "staff", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: order_type_enum_1.OrderTypeEnum.THIRD_PARTY,
        enum: Object.values(order_type_enum_1.OrderTypeEnum)
    }),
    __metadata("design:type", String)
], ThirdPartyOrderFields.prototype, "orderType", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: order_type_enum_1.OrderMethodEnum.PICKUP,
        enum: Object.values(order_type_enum_1.OrderMethodEnum)
    }),
    __metadata("design:type", String)
], ThirdPartyOrderFields.prototype, "orderMethod", void 0);
__decorate([
    (0, mongoose_1.Prop)({ min: 1, default: 1 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "numberOfGuest", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", Date)
], ThirdPartyOrderFields.prototype, "orderDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], ThirdPartyOrderFields.prototype, "completedDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], ThirdPartyOrderFields.prototype, "pickUpDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: true }),
    __metadata("design:type", Boolean)
], ThirdPartyOrderFields.prototype, "asap", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], ThirdPartyOrderFields.prototype, "scheduledForDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], ThirdPartyOrderFields.prototype, "summaryItems", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "subtotal", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "discount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "serviceCharge", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "tax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "alcoholTax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "convenienceFee", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "tips", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "total", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(order_state_enum_1.OrderStateEnum),
        default: order_state_enum_1.OrderStateEnum.NEW
    }),
    __metadata("design:type", String)
], ThirdPartyOrderFields.prototype, "orderStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ThirdPartyOrderFields.prototype, "deliverySource", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "orderKHId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ThirdPartyOrderFields.prototype, "prepareTime", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], ThirdPartyOrderFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], ThirdPartyOrderFields.prototype, "note", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], ThirdPartyOrderFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], ThirdPartyOrderFields.prototype, "createdBy", void 0);
ThirdPartyOrderFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: true, collection: 'thirdPartyOrders' })
], ThirdPartyOrderFields);
exports.ThirdPartyOrderFields = ThirdPartyOrderFields;
exports.ThirdPartyOrderSchema = mongoose_1.SchemaFactory.createForClass(ThirdPartyOrderFields);
exports.ThirdPartyOrderSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
exports.ThirdPartyOrderSchema.plugin(mongoosePaginate);
//# sourceMappingURL=third-party-order.schema.js.map