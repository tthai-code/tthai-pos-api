import * as mongoose from 'mongoose';
export declare type KitchenCounterDocument = KitchenCounter & mongoose.Document;
export declare class KitchenCounter {
    prefix: string;
    restaurantId: mongoose.Types.ObjectId;
    counter: number;
}
export declare const KitchenCounterSchema: mongoose.Schema<mongoose.Document<KitchenCounter, any, any>, mongoose.Model<mongoose.Document<KitchenCounter, any, any>, any, any, any>, any, any>;
