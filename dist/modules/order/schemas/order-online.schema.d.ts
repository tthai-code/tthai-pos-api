import * as mongoose from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
import { AddressFields } from 'src/modules/restaurant/schemas/restaurant.schema';
import { MenuCategoryOrderItemFields } from './order.schema';
export declare type OrderOnlineDocument = OrderOnlineFields & mongoose.Document;
export declare class TableFields {
    id: string;
    tableNo: string;
}
export declare class StaffFields {
    id: string;
    firstName: string;
    lastName: string;
}
declare class ModifierFields {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    nativeName: string;
    price: number;
}
export declare class ItemFields {
    id: number;
    name: string;
    nativeName: string;
    price: number;
    modifiers: Array<ModifierFields>;
    isContainAlcohol: boolean;
    coverImage: string;
    unit: number;
    amount: number;
    note: string;
    category: MenuCategoryOrderItemFields[];
}
export declare class RestaurantFields {
    id: string;
    legalBusinessName: string;
    address: AddressFields;
    dba: string;
    businessPhone: string;
    email: string;
}
export declare class CustomerFields {
    id: string;
    firstName: string;
    lastName: string;
    tel: string;
    email: string;
}
export declare class OrderOnlineFields {
    _id: string;
    restaurant: RestaurantFields;
    customer: CustomerFields;
    table: TableFields;
    staff: StaffFields;
    orderType: string;
    numberOfGuest: number;
    orderDate: Date;
    pickUpDate: Date;
    orderMethod: string;
    paymentStatus: string;
    summaryItems: Array<ItemFields>;
    subtotal: number;
    discount: number;
    serviceCharge: number;
    tax: number;
    alcoholTax: number;
    convenienceFee: number;
    total: number;
    tips: number;
    orderStatus: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const OrderOnlineSchema: mongoose.Schema<mongoose.Document<OrderOnlineFields, any, any>, mongoose.Model<mongoose.Document<OrderOnlineFields, any, any>, any, any, any>, any, any>;
export {};
