import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type KitchenQueueDocument = KitchenQueueFields & Document;
export declare class KitchenQueueFields {
    restaurantId: Types.ObjectId;
    orderId: string;
    ticketNo: string;
    items: Array<string>;
    isReprint: boolean;
    queueStatus: string;
    status: string;
    printDate: Date;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const KitchenQueueSchema: MongooseSchema<Document<KitchenQueueFields, any, any>, import("mongoose").Model<Document<KitchenQueueFields, any, any>, any, any, any>, any, any>;
