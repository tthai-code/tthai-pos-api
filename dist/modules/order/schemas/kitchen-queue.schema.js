"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenQueueSchema = exports.KitchenQueueFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const item_status_enum_1 = require("../enum/item-status.enum");
let KitchenQueueFields = class KitchenQueueFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], KitchenQueueFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "orderId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "ticketNo", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], KitchenQueueFields.prototype, "items", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: false }),
    __metadata("design:type", Boolean)
], KitchenQueueFields.prototype, "isReprint", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(item_status_enum_1.ItemStatusEnum),
        default: item_status_enum_1.ItemStatusEnum.IN_PROGRESS
    }),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "queueStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: new Date() }),
    __metadata("design:type", Date)
], KitchenQueueFields.prototype, "printDate", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], KitchenQueueFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], KitchenQueueFields.prototype, "createdBy", void 0);
KitchenQueueFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'kitchenQueue' })
], KitchenQueueFields);
exports.KitchenQueueFields = KitchenQueueFields;
exports.KitchenQueueSchema = mongoose_1.SchemaFactory.createForClass(KitchenQueueFields);
exports.KitchenQueueSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
exports.KitchenQueueSchema.plugin(mongoosePaginate);
//# sourceMappingURL=kitchen-queue.schema.js.map