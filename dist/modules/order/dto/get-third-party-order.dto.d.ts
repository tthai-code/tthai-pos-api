import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
export declare class ThirdPartyOrderPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly status: string;
    readonly type: string;
    readonly source: string;
    readonly startDate: string;
    readonly endDate: string;
    buildQuery(restaurantId: string): {
        $or: {
            _id: {
                $regex: string;
                $options: string;
            };
            'customer.firstName': {
                $regex: string;
                $options: string;
            };
            'customer.lastName': {
                $regex: string;
                $options: string;
            };
        }[];
        orderDate: {
            $gte: Date;
            $lte: Date;
        };
        orderMethod: string;
        orderStatus: any;
        deliverySource: string;
        'restaurant.id': string;
        status: StatusEnum;
    };
    buildQueryGetAll(id: string): {
        $or: ({
            _id: {
                $eq: string;
            };
            'customer.firstName'?: undefined;
            'customer.lastName'?: undefined;
        } | {
            'customer.firstName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'customer.lastName'?: undefined;
        } | {
            'customer.lastName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'customer.firstName'?: undefined;
        })[];
        'restaurant.id': string;
        orderDate: {
            $gte: Date;
            $lte: Date;
        };
        status: StatusEnum;
    };
}
