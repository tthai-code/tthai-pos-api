import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
export declare class PaginateKitchenQueue extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly orderId: string;
    buildQuery(id: string): {
        restaurantId: string;
        orderId: string;
        createdAt: {
            $gte: Date;
            $lte: Date;
        };
        status: StatusEnum;
    };
}
