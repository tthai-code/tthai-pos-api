import { MenuCategoryOrderItemDto } from './create-order-pos.dto';
declare class ModifierFieldDto {
    readonly id: string;
    readonly label: string;
    readonly abbreviation: string;
    readonly name: string;
    readonly nativeName: string;
    readonly price: any;
}
declare class ItemsDto {
    readonly menuId: string;
    readonly name: string;
    readonly nativeName: string;
    readonly price: number;
    readonly isContainAlcohol: boolean;
    readonly modifiers: Array<ModifierFieldDto>;
    readonly coverImage: string;
    readonly unit: number;
    readonly amount: number;
    readonly category: MenuCategoryOrderItemDto[];
    readonly note: string;
}
export declare class UpdateOrderItemsDto {
    items: Array<ItemsDto>;
    readonly subtotal: number;
    readonly discount: number;
    readonly serviceCharge: number;
    readonly tax: number;
    readonly alcoholTax: number;
    readonly total: number;
    readonly note: string;
}
export {};
