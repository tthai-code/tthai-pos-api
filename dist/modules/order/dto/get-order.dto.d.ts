import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
import { OrderTypeEnum } from '../enum/order-type.enum';
export declare class ToGoOrderPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly orderStatus: string;
    buildQuery(id: string): {
        $or: ({
            _id: {
                $eq: string;
            };
            'staff.firstName'?: undefined;
            'staff.lastName'?: undefined;
        } | {
            'staff.firstName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'staff.lastName'?: undefined;
        } | {
            'staff.lastName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'staff.firstName'?: undefined;
        })[];
        'restaurant.id': string;
        orderType: OrderTypeEnum;
        orderStatus: string;
        status: StatusEnum;
    };
}
