"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderOnlineCustomerPaginateDto = exports.OrderOnlinePaginateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const dayjs = require("dayjs");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
const status_enum_1 = require("../../../common/enum/status.enum");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
const payment_status_enum_1 = require("../enum/payment-status.enum");
const order_state_enum_2 = require("../enum/order-state.enum");
class OrderOnlinePaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'orderDate';
        this.sortOrder = 'desc';
        this.search = '';
        this.status = order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE;
    }
    buildQuery(id) {
        let orderStatus;
        if (this.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
            orderStatus = {
                $nin: [order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED, order_state_enum_2.OrderStateEnum.VOID]
            };
        }
        else if (this.status === order_state_enum_1.ThirdPartyOrderStatusEnum.UPCOMING) {
            orderStatus = order_state_enum_1.ThirdPartyOrderStatusEnum.UPCOMING;
        }
        else {
            orderStatus = this.status;
        }
        const result = {
            $or: [
                {
                    _id: { $eq: this.search }
                },
                {
                    'customer.firstName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    'customer.lastName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            'restaurant.id': id,
            orderType: order_type_enum_1.OrderTypeEnum.ONLINE,
            orderStatus,
            orderDate: {
                $gte: dayjs(this.startDate).toDate(),
                $lte: dayjs(this.endDate).toDate()
            },
            paymentStatus: payment_status_enum_1.OnlinePaymentStatusEnum.PAID,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
    buildQueryGetAll(id) {
        const result = {
            $or: [
                {
                    _id: { $eq: this.search }
                },
                {
                    'customer.firstName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    'customer.lastName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            'restaurant.id': id,
            orderType: order_type_enum_1.OrderTypeEnum.ONLINE,
            orderDate: {
                $gte: dayjs(this.startDate).toDate(),
                $lte: dayjs(this.endDate).toDate()
            },
            paymentStatus: payment_status_enum_1.OnlinePaymentStatusEnum.PAID,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'orderDate' }),
    __metadata("design:type", String)
], OrderOnlinePaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'desc' }),
    __metadata("design:type", String)
], OrderOnlinePaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'test' }),
    __metadata("design:type", String)
], OrderOnlinePaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(Object.values(order_state_enum_1.ThirdPartyOrderStatusEnum)),
    (0, swagger_1.ApiProperty)({
        enum: Object.values(order_state_enum_1.ThirdPartyOrderStatusEnum),
        example: order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE
    }),
    __metadata("design:type", String)
], OrderOnlinePaginateDto.prototype, "status", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], OrderOnlinePaginateDto.prototype, "startDate", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], OrderOnlinePaginateDto.prototype, "endDate", void 0);
exports.OrderOnlinePaginateDto = OrderOnlinePaginateDto;
class OrderOnlineCustomerPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'orderDate';
        this.sortOrder = 'desc';
        this.search = '';
        this.status = order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE;
    }
    buildQuery(id) {
        let orderStatus;
        if (this.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
            orderStatus = { $ne: order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED };
        }
        else if (this.status === order_state_enum_1.ThirdPartyOrderStatusEnum.UPCOMING) {
            orderStatus = order_state_enum_1.ThirdPartyOrderStatusEnum.UPCOMING;
        }
        else {
            orderStatus = this.status;
        }
        const result = {
            $or: [
                {
                    _id: { $eq: this.search }
                },
                {
                    'customer.firstName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    'customer.lastName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            'customer.id': id,
            orderType: order_type_enum_1.OrderTypeEnum.ONLINE,
            orderStatus,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'orderDate' }),
    __metadata("design:type", String)
], OrderOnlineCustomerPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'desc' }),
    __metadata("design:type", String)
], OrderOnlineCustomerPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'test' }),
    __metadata("design:type", String)
], OrderOnlineCustomerPaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(Object.values(order_state_enum_1.ThirdPartyOrderStatusEnum)),
    (0, swagger_1.ApiProperty)({
        enum: Object.values(order_state_enum_1.ThirdPartyOrderStatusEnum),
        example: order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE
    }),
    __metadata("design:type", String)
], OrderOnlineCustomerPaginateDto.prototype, "status", void 0);
exports.OrderOnlineCustomerPaginateDto = OrderOnlineCustomerPaginateDto;
//# sourceMappingURL=get-order-online.dto.js.map