export declare class PayOnlineOrderDto {
    readonly account: string;
    readonly cardholderName: string;
    readonly expirationMonth: string;
    readonly expirationYear: string;
    readonly cvv: string;
    readonly tips: number;
    readonly timeZone: string;
    readonly offsetTimeZone: number;
}
