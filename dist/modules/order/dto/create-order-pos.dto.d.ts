export declare class ModifierFieldDto {
    readonly id: string;
    readonly label: string;
    readonly abbreviation: string;
    readonly name: string;
    readonly nativeName: string;
    readonly price: any;
}
export declare class MenuCategoryOrderItemDto {
    id: string;
    name: string;
}
export declare class ItemsDto {
    readonly menuId: string;
    readonly name: string;
    readonly nativeName: string;
    readonly price: number;
    readonly isContainAlcohol: boolean;
    readonly modifiers: Array<ModifierFieldDto>;
    readonly coverImage: string;
    readonly unit: number;
    readonly amount: number;
    readonly note: string;
    readonly category: MenuCategoryOrderItemDto[];
}
export declare class CreateOrderDineInPOSDto {
    _id: string;
    restaurant: any;
    table: any;
    staff: any;
    customer: any;
    orderDate: Date;
    orderType: string;
    readonly restaurantId: string;
    readonly tableId: string;
    readonly customerId: string;
    readonly numberOfGuest: number;
    readonly items: Array<ItemsDto>;
    readonly subtotal: number;
    readonly discount: number;
    readonly serviceCharge: number;
    readonly tax: number;
    readonly alcoholTax: number;
    readonly total: number;
}
export declare class CreateOrderToGoPOSDto {
    _id: string;
    restaurant: any;
    staff: any;
    customer: any;
    orderDate: Date;
    orderType: string;
    readonly restaurantId: string;
    readonly customerId: string;
    readonly numberOfGuest: number;
    readonly items: Array<ItemsDto>;
    readonly pickUpDate: string;
    readonly subtotal: number;
    readonly discount: number;
    readonly serviceCharge: number;
    readonly tax: number;
    readonly alcoholTax: number;
    readonly total: number;
}
export declare class CreateOrderOnline {
    _id: string;
    restaurant: any;
    table: any;
    staff: any;
    customer: any;
    orderDate: Date;
    orderType: string;
    orderStatus: string;
    convenienceFee: number;
    serviceCharge: number;
    total: number;
    readonly restaurantId: string;
    readonly customerId: string;
    readonly items: Array<ItemsDto>;
    readonly orderMethod: string;
    readonly pickUpDate: Date;
    readonly subtotal: number;
    readonly discount: number;
    readonly tax: number;
    readonly alcoholTax: number;
}
