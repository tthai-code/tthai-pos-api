"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginateKitchenQueue = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const dayjs = require("dayjs");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
const status_enum_1 = require("../../../common/enum/status.enum");
class PaginateKitchenQueue extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'ticketNo';
        this.sortOrder = 'desc';
        this.orderId = '';
    }
    buildQuery(id) {
        const result = {
            restaurantId: id,
            orderId: this.orderId,
            createdAt: {
                $gte: dayjs().startOf('day').toDate(),
                $lte: dayjs().endOf('day').toDate()
            },
            status: status_enum_1.StatusEnum.ACTIVE
        };
        if (!this.orderId)
            delete result.orderId;
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'ticketNo' }),
    __metadata("design:type", String)
], PaginateKitchenQueue.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'desc' }),
    __metadata("design:type", String)
], PaginateKitchenQueue.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '<order-id>' }),
    __metadata("design:type", String)
], PaginateKitchenQueue.prototype, "orderId", void 0);
exports.PaginateKitchenQueue = PaginateKitchenQueue;
//# sourceMappingURL=get-kitchen.dto.js.map