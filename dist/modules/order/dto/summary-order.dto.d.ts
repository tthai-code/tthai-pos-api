export declare class SummaryOrderDto {
    readonly subtotal: number;
    readonly discount: number;
    readonly serviceCharge: number;
    readonly tax: number;
    readonly alcoholTax: number;
    readonly total: number;
}
