import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
import { OrderTypeEnum } from '../enum/order-type.enum';
import { OnlinePaymentStatusEnum } from '../enum/payment-status.enum';
export declare class OrderOnlinePaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly status: string;
    readonly startDate: string;
    readonly endDate: string;
    buildQuery(id: string): {
        $or: ({
            _id: {
                $eq: string;
            };
            'customer.firstName'?: undefined;
            'customer.lastName'?: undefined;
        } | {
            'customer.firstName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'customer.lastName'?: undefined;
        } | {
            'customer.lastName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'customer.firstName'?: undefined;
        })[];
        'restaurant.id': string;
        orderType: OrderTypeEnum;
        orderStatus: any;
        orderDate: {
            $gte: Date;
            $lte: Date;
        };
        paymentStatus: OnlinePaymentStatusEnum;
        status: StatusEnum;
    };
    buildQueryGetAll(id: string): {
        $or: ({
            _id: {
                $eq: string;
            };
            'customer.firstName'?: undefined;
            'customer.lastName'?: undefined;
        } | {
            'customer.firstName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'customer.lastName'?: undefined;
        } | {
            'customer.lastName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'customer.firstName'?: undefined;
        })[];
        'restaurant.id': string;
        orderType: OrderTypeEnum;
        orderDate: {
            $gte: Date;
            $lte: Date;
        };
        paymentStatus: OnlinePaymentStatusEnum;
        status: StatusEnum;
    };
}
export declare class OrderOnlineCustomerPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly status: string;
    buildQuery(id: string): {
        $or: ({
            _id: {
                $eq: string;
            };
            'customer.firstName'?: undefined;
            'customer.lastName'?: undefined;
        } | {
            'customer.firstName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'customer.lastName'?: undefined;
        } | {
            'customer.lastName': {
                $regex: string;
                $options: string;
            };
            _id?: undefined;
            'customer.firstName'?: undefined;
        })[];
        'customer.id': string;
        orderType: OrderTypeEnum;
        orderStatus: any;
        status: StatusEnum;
    };
}
