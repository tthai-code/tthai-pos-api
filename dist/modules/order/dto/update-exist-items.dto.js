"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateExistOrderItemsDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const create_order_pos_dto_1 = require("./create-order-pos.dto");
class ModifierFieldDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: '<modifier-id>' }),
    __metadata("design:type", String)
], ModifierFieldDto.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Protein Choices' }),
    __metadata("design:type", String)
], ModifierFieldDto.prototype, "label", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'P' }),
    __metadata("design:type", String)
], ModifierFieldDto.prototype, "abbreviation", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Chicken' }),
    __metadata("design:type", String)
], ModifierFieldDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'ไก่' }),
    __metadata("design:type", String)
], ModifierFieldDto.prototype, "nativeName", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 0 }),
    __metadata("design:type", Object)
], ModifierFieldDto.prototype, "price", void 0);
class ItemsDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: '<menu-id>' }),
    __metadata("design:type", String)
], ItemsDto.prototype, "menuId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Ka Prao' }),
    __metadata("design:type", String)
], ItemsDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'กะเพรา' }),
    __metadata("design:type", String)
], ItemsDto.prototype, "nativeName", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 50 }),
    __metadata("design:type", Number)
], ItemsDto.prototype, "price", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)({ example: false }),
    __metadata("design:type", Boolean)
], ItemsDto.prototype, "isContainAlcohol", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => ModifierFieldDto),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiPropertyOptional)({ type: () => [ModifierFieldDto] }),
    __metadata("design:type", Array)
], ItemsDto.prototype, "modifiers", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'https://example.com/' }),
    __metadata("design:type", String)
], ItemsDto.prototype, "coverImage", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 2 }),
    __metadata("design:type", Number)
], ItemsDto.prototype, "unit", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 100 }),
    __metadata("design:type", Number)
], ItemsDto.prototype, "amount", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'test note' }),
    __metadata("design:type", String)
], ItemsDto.prototype, "note", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => create_order_pos_dto_1.MenuCategoryOrderItemDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ type: [create_order_pos_dto_1.MenuCategoryOrderItemDto] }),
    __metadata("design:type", Array)
], ItemsDto.prototype, "category", void 0);
class UpdateExistOrderItemsDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => ItemsDto),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => ItemsDto }),
    __metadata("design:type", ItemsDto)
], UpdateExistOrderItemsDto.prototype, "items", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 100 }),
    __metadata("design:type", Number)
], UpdateExistOrderItemsDto.prototype, "subtotal", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 10 }),
    __metadata("design:type", Number)
], UpdateExistOrderItemsDto.prototype, "discount", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 10 }),
    __metadata("design:type", Number)
], UpdateExistOrderItemsDto.prototype, "serviceCharge", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 7 }),
    __metadata("design:type", Number)
], UpdateExistOrderItemsDto.prototype, "tax", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 0 }),
    __metadata("design:type", Number)
], UpdateExistOrderItemsDto.prototype, "alcoholTax", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 107 }),
    __metadata("design:type", Number)
], UpdateExistOrderItemsDto.prototype, "total", void 0);
exports.UpdateExistOrderItemsDto = UpdateExistOrderItemsDto;
//# sourceMappingURL=update-exist-items.dto.js.map