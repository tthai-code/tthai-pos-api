"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToGoOrderPaginateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
const status_enum_1 = require("../../../common/enum/status.enum");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
class ToGoOrderPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'orderDate';
        this.sortOrder = 'desc';
        this.search = '';
    }
    buildQuery(id) {
        const result = {
            $or: [
                {
                    _id: { $eq: this.search }
                },
                {
                    'staff.firstName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    'staff.lastName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            'restaurant.id': id,
            orderType: order_type_enum_1.OrderTypeEnum.TO_GO,
            orderStatus: this.orderStatus,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        if (!this.orderStatus)
            delete result.orderStatus;
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'orderDate' }),
    __metadata("design:type", String)
], ToGoOrderPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'desc' }),
    __metadata("design:type", String)
], ToGoOrderPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'test' }),
    __metadata("design:type", String)
], ToGoOrderPaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: order_state_enum_1.OrderStateEnum.PENDING,
        enum: Object.values(order_state_enum_1.OrderStateEnum)
    }),
    __metadata("design:type", String)
], ToGoOrderPaginateDto.prototype, "orderStatus", void 0);
exports.ToGoOrderPaginateDto = ToGoOrderPaginateDto;
//# sourceMappingURL=get-order.dto.js.map