"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThirdPartyOrderPaginateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const dayjs = require("dayjs");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
const status_enum_1 = require("../../../common/enum/status.enum");
const provider_enum_1 = require("../../kitchen-hub/common/provider.enum");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
class ThirdPartyOrderPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'createdAt';
        this.sortOrder = 'desc';
        this.search = '';
        this.status = order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE;
        this.source = 'ALL';
    }
    buildQuery(restaurantId) {
        let orderStatus;
        if (this.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
            orderStatus = {
                $nin: [order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED, order_state_enum_1.OrderStateEnum.CANCELED]
            };
        }
        else if (this.status === order_state_enum_1.ThirdPartyOrderStatusEnum.UPCOMING) {
            orderStatus = order_state_enum_1.ThirdPartyOrderStatusEnum.UPCOMING;
        }
        else {
            orderStatus = this.status;
        }
        const result = {
            $or: [
                {
                    _id: {
                        $regex: this.search,
                        $options: 'i'
                    },
                    'customer.firstName': {
                        $regex: this.search,
                        $options: 'i'
                    },
                    'customer.lastName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            orderDate: {
                $gte: dayjs(this.startDate).toDate(),
                $lte: dayjs(this.endDate).toDate()
            },
            orderMethod: this.type,
            orderStatus,
            deliverySource: this.source,
            'restaurant.id': restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        if (this.source === 'ALL')
            delete result.deliverySource;
        return result;
    }
    buildQueryGetAll(id) {
        const result = {
            $or: [
                {
                    _id: { $eq: this.search }
                },
                {
                    'customer.firstName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    'customer.lastName': {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            'restaurant.id': id,
            orderDate: {
                $gte: dayjs(this.startDate).toDate(),
                $lte: dayjs(this.endDate).toDate()
            },
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'createdAt' }),
    __metadata("design:type", String)
], ThirdPartyOrderPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'desc' }),
    __metadata("design:type", String)
], ThirdPartyOrderPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'sT3JdMlf' }),
    __metadata("design:type", String)
], ThirdPartyOrderPaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(Object.values(order_state_enum_1.ThirdPartyOrderStatusEnum)),
    (0, swagger_1.ApiProperty)({
        enum: Object.values(order_state_enum_1.ThirdPartyOrderStatusEnum),
        example: order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE
    }),
    __metadata("design:type", String)
], ThirdPartyOrderPaginateDto.prototype, "status", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)([order_type_enum_1.OrderMethodEnum.PICKUP, order_type_enum_1.OrderMethodEnum.DELIVERY]),
    (0, swagger_1.ApiProperty)({
        enum: [order_type_enum_1.OrderMethodEnum.PICKUP, order_type_enum_1.OrderMethodEnum.DELIVERY],
        example: order_type_enum_1.OrderMethodEnum.PICKUP
    }),
    __metadata("design:type", String)
], ThirdPartyOrderPaginateDto.prototype, "type", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)([
        provider_enum_1.ProviderEnum.DOORDASH,
        provider_enum_1.ProviderEnum.GLORIAFOOD,
        provider_enum_1.ProviderEnum.GRUBHUB,
        provider_enum_1.ProviderEnum.UBEREATS,
        'ALL'
    ]),
    (0, swagger_1.ApiProperty)({
        enum: Object.values(provider_enum_1.ProviderEnum),
        example: provider_enum_1.ProviderEnum.GLORIAFOOD
    }),
    __metadata("design:type", String)
], ThirdPartyOrderPaginateDto.prototype, "source", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], ThirdPartyOrderPaginateDto.prototype, "startDate", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], ThirdPartyOrderPaginateDto.prototype, "endDate", void 0);
exports.ThirdPartyOrderPaginateDto = ThirdPartyOrderPaginateDto;
//# sourceMappingURL=get-third-party-order.dto.js.map