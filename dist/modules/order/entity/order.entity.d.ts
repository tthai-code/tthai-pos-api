import { ResponseDto } from 'src/common/entity/response.entity';
declare class VoidOrderFields {
    id: string;
    orderStatus: string;
}
declare class TableFields {
    id: string;
    table_no: string;
}
declare class MoveTableFields {
    id: string;
    table: TableFields;
}
export declare class VoidOrderResponse extends ResponseDto<VoidOrderFields> {
    data: VoidOrderFields;
}
export declare class MoveTableResponse extends ResponseDto<MoveTableFields> {
    data: MoveTableFields;
}
export {};
