import { ResponseDto } from 'src/common/entity/response.entity';
declare class CreateOnlineOrderObject {
    id: string;
    order_status: string;
    payment_status: string;
}
export declare class CreateOnlineOrderResponse extends ResponseDto<CreateOnlineOrderObject> {
    data: CreateOnlineOrderObject;
}
export {};
