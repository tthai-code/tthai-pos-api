"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOnlineOrderByIdForPOSResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class AddressObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "zip_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "address", void 0);
class RestaurantObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", AddressObject)
], RestaurantObject.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "legal_business_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "id", void 0);
class ModifierItemObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierItemObject.prototype, "price", void 0);
class CustomerObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerObject.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerObject.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerObject.prototype, "tel", void 0);
class StaffObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffObject.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffObject.prototype, "last_name", void 0);
class OnlineOrderObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "tips", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineOrderObject.prototype, "order_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "convenience_fee", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "subtotal", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineOrderObject.prototype, "payment_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineOrderObject.prototype, "order_method", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineOrderObject.prototype, "pick_up_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineOrderObject.prototype, "order_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineOrderObject.prototype, "number_of_guest", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineOrderObject.prototype, "order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", StaffObject)
], OnlineOrderObject.prototype, "staff", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", CustomerObject)
], OnlineOrderObject.prototype, "customer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", RestaurantObject)
], OnlineOrderObject.prototype, "restaurant", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineOrderObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], OnlineOrderObject.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineOrderObject.prototype, "ticket_no", void 0);
class GetOnlineOrderByIdForPOSResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: OnlineOrderObject,
        example: {
            tips: 0,
            order_status: 'NEW',
            total: 107,
            convenience_fee: 0,
            alcohol_tax: 0,
            tax: 7,
            service_charge: 10,
            discount: 10,
            subtotal: 100,
            payment_status: 'UNPAID',
            order_method: 'PICKUP',
            pick_up_date: null,
            order_date: '2022-12-07T08:04:37.051Z',
            number_of_guest: 1,
            order_type: 'ONLINE',
            staff: {
                id: '6390437f6f06b6b4ca69f6d0',
                first_name: 'Cory',
                last_name: 'Wong'
            },
            customer: {
                id: '6390437f6f06b6b4ca69f6d0',
                first_name: 'Tim',
                last_name: 'Henson',
                tel: '5102638229'
            },
            restaurant: {
                email: 'corywong@mail.com',
                business_phone: '0222222222',
                address: {
                    zip_code: '75001',
                    state: 'Dallas',
                    city: 'Addison',
                    address: 'Some Address'
                },
                legal_business_name: 'Holy Beef',
                id: '630e55a0d9c30fd7cdcb424b'
            },
            id: 'Ko9wfPR6',
            items: [
                {
                    menu_id: '630fc086af525d6ed9fc0d5d',
                    name: 'Ka Prao',
                    native_name: 'กะเพรา',
                    price: 50,
                    is_contain_alcohol: false,
                    modifiers: [
                        {
                            id: '630f464bb75d5da2ea75a5aa',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Chicken',
                            native_name: 'ไก่',
                            price: 0
                        }
                    ],
                    cover_image: 'https://example.com/',
                    unit: 2,
                    amount: 100,
                    note: 'test note'
                }
            ],
            ticket_no: '1'
        }
    }),
    __metadata("design:type", OnlineOrderObject)
], GetOnlineOrderByIdForPOSResponse.prototype, "data", void 0);
exports.GetOnlineOrderByIdForPOSResponse = GetOnlineOrderByIdForPOSResponse;
//# sourceMappingURL=pos-get-order.entity.js.map