import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class ModifiersFields {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    native_name: string;
    price: number;
}
declare class ItemsFields {
    item_status: string;
    note: string;
    amount: number;
    unit: number;
    cover_image: string;
    is_contain_alcohol: boolean;
    modifiers: Array<ModifiersFields>;
    price: number;
    native_name: string;
    name: string;
    menu_id: string;
    order_id: string;
    id: string;
}
declare class KitchenQueueFields {
    print_date: string;
    queue_status: string;
    is_reprint: boolean;
    items: Array<ItemsFields>;
    ticket_no: string;
    order_id: string;
    restaurant_id: string;
    id: string;
}
declare class KitchenFieldsPaginate extends PaginateResponseDto<Array<KitchenQueueFields>> {
    results: Array<KitchenQueueFields>;
}
export declare class PaginateKitchenQueueResponse extends ResponseDto<KitchenFieldsPaginate> {
    data: KitchenFieldsPaginate;
}
declare class SuccessFields {
    success: boolean;
}
export declare class UpdateItemStatusResponse extends ResponseDto<SuccessFields> {
    data: SuccessFields;
}
export {};
