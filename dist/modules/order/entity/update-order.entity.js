"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SummaryOrderItemsResponse = exports.DeleteOrderItemResponse = exports.UpdateExistOrderItemsResponse = exports.AddNewOrderItemsResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
class ModifierFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierFields.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "id", void 0);
class ItemFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "item_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemFields.prototype, "amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemFields.prototype, "unit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "cover_image", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], ItemFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => [ModifierFields] }),
    __metadata("design:type", Array)
], ItemFields.prototype, "modifiers", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemFields.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "menu_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "id", void 0);
class AddressFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "zip_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "address", void 0);
class StaffFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffFields.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffFields.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffFields.prototype, "id", void 0);
class TableFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableFields.prototype, "table_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableFields.prototype, "id", void 0);
class CustomerFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFields.prototype, "tel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFields.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFields.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFields.prototype, "id", void 0);
class RestaurantFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => AddressFields }),
    __metadata("design:type", AddressFields)
], RestaurantFields.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "legal_business_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "id", void 0);
class BaseOrderFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseOrderFields.prototype, "order_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BaseOrderFields.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BaseOrderFields.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BaseOrderFields.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BaseOrderFields.prototype, "service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BaseOrderFields.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BaseOrderFields.prototype, "subtotal", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseOrderFields.prototype, "pick_up_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseOrderFields.prototype, "order_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BaseOrderFields.prototype, "number_of_guest", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseOrderFields.prototype, "order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => StaffFields }),
    __metadata("design:type", StaffFields)
], BaseOrderFields.prototype, "staff", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => TableFields }),
    __metadata("design:type", TableFields)
], BaseOrderFields.prototype, "table", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => CustomerFields }),
    __metadata("design:type", CustomerFields)
], BaseOrderFields.prototype, "customer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => RestaurantFields }),
    __metadata("design:type", RestaurantFields)
], BaseOrderFields.prototype, "restaurant", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseOrderFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseOrderFields.prototype, "ticket_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => [ItemFields] }),
    __metadata("design:type", Array)
], BaseOrderFields.prototype, "items", void 0);
class OrderWithTimestampFields extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderWithTimestampFields.prototype, "order_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderWithTimestampFields.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderWithTimestampFields.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderWithTimestampFields.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderWithTimestampFields.prototype, "service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderWithTimestampFields.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderWithTimestampFields.prototype, "subtotal", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderWithTimestampFields.prototype, "pick_up_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderWithTimestampFields.prototype, "order_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderWithTimestampFields.prototype, "number_of_guest", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderWithTimestampFields.prototype, "order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => StaffFields }),
    __metadata("design:type", StaffFields)
], OrderWithTimestampFields.prototype, "staff", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => TableFields }),
    __metadata("design:type", TableFields)
], OrderWithTimestampFields.prototype, "table", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => CustomerFields }),
    __metadata("design:type", CustomerFields)
], OrderWithTimestampFields.prototype, "customer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => RestaurantFields }),
    __metadata("design:type", RestaurantFields)
], OrderWithTimestampFields.prototype, "restaurant", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderWithTimestampFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderWithTimestampFields.prototype, "ticket_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => [ItemFields] }),
    __metadata("design:type", Array)
], OrderWithTimestampFields.prototype, "items", void 0);
class AddNewOrderItemsResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: BaseOrderFields,
        example: {
            order_status: 'PENDING',
            total: 48,
            alcohol_tax: 0,
            tax: 0,
            service_charge: 0,
            discount: 0,
            subtotal: 48,
            pick_up_date: null,
            order_date: '2022-10-31T19:48:37.705Z',
            number_of_guest: 1,
            order_type: 'DINE_IN',
            staff: {
                last_name: 'NP',
                first_name: 'Pavarich',
                id: '6316cc72b889593b028928f1'
            },
            table: {
                table_no: '5',
                id: '633699d658fe7dc03f44920e'
            },
            customer: {
                tel: '',
                last_name: 'Henson',
                first_name: 'Tim',
                id: '635a421765e365cb4f0bb378'
            },
            restaurant: {
                email: 'info@carolinaswise.com',
                business_phone: '5102638229',
                address: {
                    zip_code: '94501',
                    state: 'CA',
                    city: 'Alameda',
                    address: '1319 Park St'
                },
                legal_business_name: 'May Thai Kitchen LLC',
                id: '630eff5751c2eac55f52662c'
            },
            id: 'tyLRbra1',
            ticket_no: '2',
            items: [
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 32,
                    unit: 2,
                    cover_image: null,
                    is_contain_alcohol: false,
                    modifiers: [
                        {
                            name: 'Pork',
                            price: 2,
                            native_name: 'Native Name Test',
                            label: 'Protein Choice',
                            abbreviation: 'P',
                            id: '6337443cc64b8b0998892cd2'
                        }
                    ],
                    price: 14,
                    native_name: 'test nativeName',
                    name: 'Larb',
                    menu_id: '6337443cc64b8b0998892ccf',
                    order_id: 'tyLRbra1',
                    id: '636026fcf3b802dec079707f'
                }
            ]
        }
    }),
    __metadata("design:type", BaseOrderFields)
], AddNewOrderItemsResponse.prototype, "data", void 0);
exports.AddNewOrderItemsResponse = AddNewOrderItemsResponse;
class UpdateExistOrderItemsField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], UpdateExistOrderItemsField.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => ItemFields }),
    __metadata("design:type", ItemFields)
], UpdateExistOrderItemsField.prototype, "items", void 0);
class UpdateExistOrderItemsResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: () => UpdateExistOrderItemsField,
        example: {
            order_id: 'tyLRbra1',
            items: {
                item_status: 'IN_PROGRESS',
                note: 'mild spicy',
                amount: 32,
                unit: 2,
                cover_image: null,
                is_contain_alcohol: false,
                modifiers: [
                    {
                        name: 'Pork',
                        price: 2,
                        native_name: 'Native Name Test',
                        label: 'Protein Choice',
                        abbreviation: 'P',
                        id: '6337443cc64b8b0998892cd2'
                    }
                ],
                price: 14,
                native_name: 'test nativeName',
                name: 'Larb',
                menu_id: '6337443cc64b8b0998892ccf',
                order_id: 'tyLRbra1',
                id: '63602695f3b802dec0797075'
            }
        }
    }),
    __metadata("design:type", UpdateExistOrderItemsField)
], UpdateExistOrderItemsResponse.prototype, "data", void 0);
exports.UpdateExistOrderItemsResponse = UpdateExistOrderItemsResponse;
class DeleteOrderItemResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: () => UpdateExistOrderItemsField,
        example: {
            order_id: 'tyLRbra1',
            items: {
                item_status: 'CANCELED',
                note: '',
                amount: 32,
                unit: 2,
                cover_image: null,
                is_contain_alcohol: false,
                modifiers: [
                    {
                        name: 'Pork',
                        price: 2,
                        native_name: 'Native Name Test',
                        label: 'Protein Choice',
                        abbreviation: 'P',
                        id: '6337443cc64b8b0998892cd2'
                    }
                ],
                price: 14,
                native_name: 'test nativeName',
                name: 'Larb',
                menu_id: '6337443cc64b8b0998892ccf',
                order_id: 'tyLRbra1',
                id: '636026fcf3b802dec079707f'
            }
        }
    }),
    __metadata("design:type", UpdateExistOrderItemsField)
], DeleteOrderItemResponse.prototype, "data", void 0);
exports.DeleteOrderItemResponse = DeleteOrderItemResponse;
class SummaryOrderItemsResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: () => OrderWithTimestampFields,
        example: {
            created_by: {
                username: 'Test',
                id: '63a03f43014de42db475bba9'
            },
            updated_by: {
                username: 'Test',
                id: '63a03f43014de42db475bba9'
            },
            status: 'active',
            order_status: 'PENDING',
            total: 112.8,
            convenience_fee: 0,
            alcohol_tax: 8,
            tax: 5.6,
            service_charge: 5.6,
            discount: 0,
            subtotal: 93.6,
            summary_items: [
                {
                    id: '634d2a6c57a023457c8e498d',
                    name: 'Chang Beer',
                    native_name: 'test nativeName',
                    price: 40,
                    modifiers: [
                        {
                            id: '634d2a6c57a023457c8e4990',
                            label: 'Choice of Protein',
                            abbreviation: 'P',
                            name: 'No Protein',
                            native_name: 'Native Name Test',
                            price: 0
                        }
                    ],
                    is_contain_alcohol: true,
                    unit: 2,
                    amount: 40,
                    note: ''
                }
            ],
            pick_up_date: '2022-12-05T15:40:35.829Z',
            order_date: '2023-01-23T05:04:49.772Z',
            number_of_guest: 1,
            order_type: 'TO_GO',
            staff: {
                last_name: 'Staff',
                first_name: 'Test',
                id: '63a03f43014de42db475bba9'
            },
            table: null,
            customer: {
                tel: '',
                last_name: '',
                first_name: 'Guest',
                id: null
            },
            restaurant: {
                email: 'corywong@mail.com',
                business_phone: '0222222222',
                address: {
                    zip_code: '75001',
                    state: 'Dallas',
                    city: 'Addison',
                    address: 'Some Address'
                },
                dba: 'Test Doing as Business',
                legal_business_name: 'Holy Beef',
                id: '630e55a0d9c30fd7cdcb424b'
            },
            updated_at: '2023-01-23T05:08:22.149Z',
            created_at: '2023-01-23T05:04:49.796Z',
            id: 'irn2gMwJ'
        }
    }),
    __metadata("design:type", OrderWithTimestampFields)
], SummaryOrderItemsResponse.prototype, "data", void 0);
exports.SummaryOrderItemsResponse = SummaryOrderItemsResponse;
//# sourceMappingURL=update-order.entity.js.map