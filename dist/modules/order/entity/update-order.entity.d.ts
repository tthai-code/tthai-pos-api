import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
declare class ModifierFields {
    name: string;
    price: number;
    native_name: string;
    label: string;
    abbreviation: string;
    id: string;
}
declare class ItemFields {
    item_status: string;
    note: string;
    amount: number;
    unit: number;
    cover_image: string;
    is_contain_alcohol: boolean;
    modifiers: Array<ModifierFields>;
    price: number;
    native_name: string;
    name: string;
    menu_id: string;
    order_id: string;
    id: string;
}
declare class AddressFields {
    zip_code: string;
    state: string;
    city: string;
    address: string;
}
declare class StaffFields {
    last_name: string;
    first_name: string;
    id: string;
}
declare class TableFields {
    table_no: string;
    id: string;
}
declare class CustomerFields {
    tel: string;
    last_name: string;
    first_name: string;
    id: string;
}
declare class RestaurantFields {
    email: string;
    business_phone: string;
    address: AddressFields;
    legal_business_name: string;
    id: string;
}
declare class BaseOrderFields {
    order_status: string;
    total: number;
    alcohol_tax: number;
    tax: number;
    service_charge: number;
    discount: number;
    subtotal: number;
    pick_up_date: string;
    order_date: string;
    number_of_guest: number;
    order_type: string;
    staff: StaffFields;
    table: TableFields;
    customer: CustomerFields;
    restaurant: RestaurantFields;
    id: string;
    ticket_no: string;
    items: Array<ItemFields>;
}
declare class OrderWithTimestampFields extends TimestampResponseDto {
    order_status: string;
    total: number;
    alcohol_tax: number;
    tax: number;
    service_charge: number;
    discount: number;
    subtotal: number;
    pick_up_date: string;
    order_date: string;
    number_of_guest: number;
    order_type: string;
    staff: StaffFields;
    table: TableFields;
    customer: CustomerFields;
    restaurant: RestaurantFields;
    id: string;
    ticket_no: string;
    items: Array<ItemFields>;
}
export declare class AddNewOrderItemsResponse extends ResponseDto<BaseOrderFields> {
    data: BaseOrderFields;
}
declare class UpdateExistOrderItemsField {
    order_id: string;
    items: ItemFields;
}
export declare class UpdateExistOrderItemsResponse extends ResponseDto<UpdateExistOrderItemsField> {
    data: UpdateExistOrderItemsField;
}
export declare class DeleteOrderItemResponse extends ResponseDto<UpdateExistOrderItemsField> {
    data: UpdateExistOrderItemsField;
}
export declare class SummaryOrderItemsResponse extends ResponseDto<OrderWithTimestampFields> {
    data: OrderWithTimestampFields;
}
export {};
