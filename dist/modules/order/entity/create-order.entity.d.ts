import { ResponseDto } from 'src/common/entity/response.entity';
declare class AddressFields {
    address: string;
    city: string;
    state: string;
    zip_code: string;
}
declare class RestaurantFields {
    id: string;
    legal_business_name: string;
    email: string;
    business_phone: string;
    address: AddressFields;
}
declare class CustomerFields {
    id: string;
    first_name: string;
    last_name: string;
    tel: string;
}
declare class TableFields {
    id: string;
    table_no: string;
}
declare class StaffFields {
    id: string;
    first_name: string;
    last_name: string;
}
declare class ModifierFields {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    native_name: string;
    price: number;
}
export declare class MenuCategoryOrderItemObject {
    id: string;
    name: string;
}
declare class ItemFields {
    id: string;
    order_id: string;
    menu_id: string;
    name: string;
    native_name: string;
    price: number;
    is_contain_alcohol: boolean;
    modifiers: Array<ModifierFields>;
    unit: number;
    amount: number;
    note: string;
    item_status: string;
    category: MenuCategoryOrderItemObject[];
}
declare class OrderBaseFields {
    id: string;
    restaurant: RestaurantFields;
    customer: CustomerFields;
    staff: StaffFields;
    order_type: string;
    number_of_guest: number;
    order_date: string;
    ticket_no: string;
    items: Array<ItemFields>;
    subtotal: number;
    discount: number;
    service_charge: number;
    tax: number;
    alcohol_tax: number;
    total: number;
    order_status: string;
}
declare class CreateOrderDineInFields extends OrderBaseFields {
    table: TableFields;
}
declare class CreateOrderToGoFields extends OrderBaseFields {
    pickUpDate: string;
}
export declare class CreateOrderCheckoutInResponse extends ResponseDto<OrderBaseFields> {
    data: OrderBaseFields;
}
export declare class CreateOrderDineInResponse extends ResponseDto<CreateOrderDineInFields> {
    data: CreateOrderDineInFields;
}
export declare class CreateOrderToGoResponse extends ResponseDto<CreateOrderToGoFields> {
    data: CreateOrderToGoFields;
}
export {};
