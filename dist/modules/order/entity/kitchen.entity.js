"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateItemStatusResponse = exports.PaginateKitchenQueueResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const item_status_enum_1 = require("../enum/item-status.enum");
class ModifiersFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersFields.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersFields.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifiersFields.prototype, "price", void 0);
class ItemsFields {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(item_status_enum_1.ItemStatusEnum) }),
    __metadata("design:type", String)
], ItemsFields.prototype, "item_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsFields.prototype, "note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemsFields.prototype, "amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemsFields.prototype, "unit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsFields.prototype, "cover_image", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], ItemsFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], ItemsFields.prototype, "modifiers", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemsFields.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsFields.prototype, "menu_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsFields.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsFields.prototype, "id", void 0);
class KitchenQueueFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "print_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(item_status_enum_1.ItemStatusEnum) }),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "queue_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], KitchenQueueFields.prototype, "is_reprint", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], KitchenQueueFields.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "ticket_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenQueueFields.prototype, "id", void 0);
class KitchenFieldsPaginate extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [KitchenQueueFields],
        example: [
            {
                print_date: '2022-10-31T19:15:54.960Z',
                queue_status: 'READY',
                is_reprint: false,
                items: [
                    {
                        item_status: 'READY',
                        note: '',
                        amount: 80,
                        unit: 2,
                        cover_image: '',
                        is_contain_alcohol: false,
                        modifiers: [
                            {
                                id: '630fcd08b6fb5ee3bab46885',
                                label: 'Protein Choices',
                                abbreviation: 'P',
                                name: 'Chicken',
                                native_name: 'Chicken',
                                price: 0
                            }
                        ],
                        price: 40,
                        native_name: 'test nativeName',
                        name: 'Pad Thai',
                        menu_id: '630fc086af525d6ed9fc0d5d',
                        order_id: 'HS9SqQB2',
                        id: '63601f35ee84c9491202f524'
                    }
                ],
                ticket_no: '1',
                order_id: 'HS9SqQB2',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                id: '63601f35ee84c9491202f527'
            }
        ]
    }),
    __metadata("design:type", Array)
], KitchenFieldsPaginate.prototype, "results", void 0);
class PaginateKitchenQueueResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => KitchenFieldsPaginate }),
    __metadata("design:type", KitchenFieldsPaginate)
], PaginateKitchenQueueResponse.prototype, "data", void 0);
exports.PaginateKitchenQueueResponse = PaginateKitchenQueueResponse;
class SuccessFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], SuccessFields.prototype, "success", void 0);
class UpdateItemStatusResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: () => SuccessFields,
        example: {
            success: true
        }
    }),
    __metadata("design:type", SuccessFields)
], UpdateItemStatusResponse.prototype, "data", void 0);
exports.UpdateItemStatusResponse = UpdateItemStatusResponse;
//# sourceMappingURL=kitchen.entity.js.map