import { ResponseDto } from 'src/common/entity/response.entity';
declare class CheckOrderHourObject {
    is_available: boolean;
}
export declare class CheckOrderHourResponse extends ResponseDto<CheckOrderHourObject> {
    data: CheckOrderHourObject;
}
export {};
