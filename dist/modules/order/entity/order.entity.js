"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MoveTableResponse = exports.VoidOrderResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class VoidOrderFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], VoidOrderFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], VoidOrderFields.prototype, "orderStatus", void 0);
class TableFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableFields.prototype, "table_no", void 0);
class MoveTableFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MoveTableFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: TableFields }),
    __metadata("design:type", TableFields)
], MoveTableFields.prototype, "table", void 0);
class VoidOrderResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: VoidOrderFields,
        example: {
            order_status: 'VOID',
            id: 'qMePYBBz'
        }
    }),
    __metadata("design:type", VoidOrderFields)
], VoidOrderResponse.prototype, "data", void 0);
exports.VoidOrderResponse = VoidOrderResponse;
class MoveTableResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: MoveTableFields,
        example: {
            table: {
                table_no: 1,
                id: '631280e1d8aafae7cae2452b'
            },
            id: 'HS9SqQB2'
        }
    }),
    __metadata("design:type", MoveTableFields)
], MoveTableResponse.prototype, "data", void 0);
exports.MoveTableResponse = MoveTableResponse;
//# sourceMappingURL=order.entity.js.map