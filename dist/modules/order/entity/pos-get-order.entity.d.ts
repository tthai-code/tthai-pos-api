import { ResponseDto } from 'src/common/entity/response.entity';
declare class AddressObject {
    zip_code: string;
    state: string;
    city: string;
    address: string;
}
declare class RestaurantObject {
    email: string;
    business_phone: string;
    address: AddressObject;
    legal_business_name: string;
    id: string;
}
declare class ModifierItemObject {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    native_name: string;
    price: number;
}
declare class CustomerObject {
    id: string;
    first_name: string;
    last_name: string;
    tel: string;
}
declare class StaffObject {
    id: string;
    first_name: string;
    last_name: string;
}
declare class OnlineOrderObject {
    tips: number;
    order_status: string;
    total: number;
    convenience_fee: number;
    alcohol_tax: number;
    tax: number;
    service_charge: number;
    discount: number;
    subtotal: number;
    payment_status: string;
    order_method: string;
    pick_up_date: string;
    order_date: string;
    number_of_guest: number;
    order_type: string;
    staff: StaffObject;
    customer: CustomerObject;
    restaurant: RestaurantObject;
    id: string;
    items: ModifierItemObject[];
    ticket_no: string;
}
export declare class GetOnlineOrderByIdForPOSResponse extends ResponseDto<OnlineOrderObject> {
    data: OnlineOrderObject;
}
export {};
