"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginateToGoOrderResponse = exports.GetToGoOrderByIdResponse = exports.GetOrderByTableIdResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const item_status_enum_1 = require("../enum/item-status.enum");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
const create_order_entity_1 = require("./create-order.entity");
class AddressFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "zip_code", void 0);
class RestaurantFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "legal_business_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: AddressFields }),
    __metadata("design:type", AddressFields)
], RestaurantFields.prototype, "address", void 0);
class CustomerFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFields.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFields.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFields.prototype, "tel", void 0);
class TableFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableFields.prototype, "table_no", void 0);
class StaffFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffFields.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffFields.prototype, "last_name", void 0);
class ModifierFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierFields.prototype, "price", void 0);
class ItemFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "menu_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemFields.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], ItemFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ModifierFields] }),
    __metadata("design:type", Array)
], ItemFields.prototype, "modifiers", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemFields.prototype, "unit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemFields.prototype, "amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemFields.prototype, "note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(item_status_enum_1.ItemStatusEnum) }),
    __metadata("design:type", String)
], ItemFields.prototype, "item_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [create_order_entity_1.MenuCategoryOrderItemObject] }),
    __metadata("design:type", Array)
], ItemFields.prototype, "category", void 0);
class OrderBaseFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderBaseFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: RestaurantFields }),
    __metadata("design:type", RestaurantFields)
], OrderBaseFields.prototype, "restaurant", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: CustomerFields }),
    __metadata("design:type", CustomerFields)
], OrderBaseFields.prototype, "customer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: StaffFields }),
    __metadata("design:type", StaffFields)
], OrderBaseFields.prototype, "staff", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(order_type_enum_1.OrderTypeEnum) }),
    __metadata("design:type", String)
], OrderBaseFields.prototype, "order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderBaseFields.prototype, "number_of_guest", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderBaseFields.prototype, "order_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderBaseFields.prototype, "ticket_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], OrderBaseFields.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderBaseFields.prototype, "subtotal", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderBaseFields.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderBaseFields.prototype, "service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderBaseFields.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderBaseFields.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderBaseFields.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(order_state_enum_1.OrderStateEnum) }),
    __metadata("design:type", String)
], OrderBaseFields.prototype, "order_status", void 0);
class GetOrderByTableIdFields extends OrderBaseFields {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: TableFields }),
    __metadata("design:type", TableFields)
], GetOrderByTableIdFields.prototype, "table", void 0);
class GetToGoOrderByIdFields extends OrderBaseFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetToGoOrderByIdFields.prototype, "pick_up_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetToGoOrderByIdFields.prototype, "ticket_no", void 0);
class GetOrderByTableIdResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: GetOrderByTableIdFields,
        example: {
            order_status: 'PENDING',
            total: 40,
            convenience_fee: 0,
            alcohol_tax: 0,
            tax: 0,
            service_charge: 0,
            discount: 0,
            subtotal: 40,
            order_date: '2022-10-30T20:48:42.091Z',
            number_of_guest: 1,
            order_type: 'DINE_IN',
            staff: {
                last_name: 'Plini',
                first_name: 'Plini',
                id: '630f1c078653105bf1478b82'
            },
            table: {
                table_no: 1,
                id: '631280e1d8aafae7cae2452b'
            },
            customer: {
                id: null,
                first_name: 'Guest',
                last_name: '',
                tel: ''
            },
            restaurant: {
                email: 'corywong@mail.com',
                business_phone: '0222222222',
                address: {
                    zip_code: '75001',
                    state: 'Dallas',
                    city: 'Addison',
                    address: 'Some Address'
                },
                legal_business_name: 'Holy Beef',
                id: '630e55a0d9c30fd7cdcb424b'
            },
            id: 'CPbcayEE',
            items: [
                {
                    item_status: 'IN_PROGRESS',
                    category: [
                        {
                            id: '635ee32a19664a40ea51a10c',
                            name: 'Drink'
                        }
                    ],
                    note: '',
                    amount: 40,
                    unit: 1,
                    is_contain_alcohol: true,
                    modifiers: [
                        {
                            id: '634d2a6c57a023457c8e4990',
                            label: 'Choice of Protein',
                            abbreviation: 'P',
                            name: 'No Protein',
                            native_name: 'Native Name Test',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Chang Beer',
                    menu_id: '634d2a6c57a023457c8e498d',
                    order_id: 'CPbcayEE',
                    id: '635ee32a19664a40ea51a10c'
                }
            ]
        }
    }),
    __metadata("design:type", GetOrderByTableIdFields)
], GetOrderByTableIdResponse.prototype, "data", void 0);
exports.GetOrderByTableIdResponse = GetOrderByTableIdResponse;
class GetToGoOrderByIdResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: GetToGoOrderByIdFields,
        example: {
            order_status: 'PENDING',
            total: 80,
            convenience_fee: 0,
            alcohol_tax: 0,
            tax: 0,
            service_charge: 0,
            discount: 0,
            subtotal: 80,
            pick_up_date: '2022-10-22T15:40:35.829Z',
            order_date: '2022-10-30T20:54:48.453Z',
            number_of_guest: 1,
            order_type: 'TO_GO',
            staff: {
                last_name: 'Plini',
                first_name: 'Plini',
                id: '630f1c078653105bf1478b82'
            },
            customer: {
                id: null,
                first_name: 'Guest',
                last_name: '',
                tel: ''
            },
            restaurant: {
                email: 'corywong@mail.com',
                business_phone: '0222222222',
                address: {
                    zip_code: '75001',
                    state: 'Dallas',
                    city: 'Addison',
                    address: 'Some Address'
                },
                legal_business_name: 'Holy Beef',
                id: '630e55a0d9c30fd7cdcb424b'
            },
            id: 'ufgV2657',
            items: [
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 40,
                    unit: 1,
                    is_contain_alcohol: true,
                    modifiers: [
                        {
                            id: '634d2a6c57a023457c8e4990',
                            label: 'Choice of Protein',
                            abbreviation: 'P',
                            name: 'No Protein',
                            native_name: 'Native Name Test',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Chang Beer',
                    menu_id: '634d2a6c57a023457c8e498d',
                    order_id: 'ufgV2657',
                    id: '635ee498d72a2d86521286a1'
                },
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 40,
                    unit: 1,
                    is_contain_alcohol: true,
                    modifiers: [
                        {
                            id: '634d2a6c57a023457c8e4990',
                            label: 'Choice of Protein',
                            abbreviation: 'P',
                            name: 'Chicken',
                            native_name: 'Native Name Test',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Chang Beer',
                    menu_id: '634d2a6c57a023457c8e498d',
                    order_id: 'ufgV2657',
                    id: '635ee498d72a2d86521286a2'
                }
            ],
            ticket_no: 1
        }
    }),
    __metadata("design:type", GetToGoOrderByIdFields)
], GetToGoOrderByIdResponse.prototype, "data", void 0);
exports.GetToGoOrderByIdResponse = GetToGoOrderByIdResponse;
class ToGoOrderFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ToGoOrderFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: RestaurantFields }),
    __metadata("design:type", RestaurantFields)
], ToGoOrderFields.prototype, "restaurant", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: CustomerFields }),
    __metadata("design:type", CustomerFields)
], ToGoOrderFields.prototype, "customer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: StaffFields }),
    __metadata("design:type", StaffFields)
], ToGoOrderFields.prototype, "staff", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(order_type_enum_1.OrderTypeEnum) }),
    __metadata("design:type", String)
], ToGoOrderFields.prototype, "order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ToGoOrderFields.prototype, "pick_up_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ToGoOrderFields.prototype, "number_of_guest", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ToGoOrderFields.prototype, "order_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ToGoOrderFields.prototype, "ticket_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], ToGoOrderFields.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ToGoOrderFields.prototype, "subtotal", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ToGoOrderFields.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ToGoOrderFields.prototype, "service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ToGoOrderFields.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ToGoOrderFields.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ToGoOrderFields.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(order_state_enum_1.OrderStateEnum) }),
    __metadata("design:type", String)
], ToGoOrderFields.prototype, "order_status", void 0);
class PaginateToGoOrder extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [ToGoOrderFields],
        example: [
            {
                status: 'active',
                order_status: 'PENDING',
                total: 80,
                convenience_fee: 0,
                alcohol_tax: 0,
                tax: 0,
                service_charge: 0,
                discount: 0,
                subtotal: 80,
                summary_items: [],
                pick_up_date: '2022-10-22T15:40:35.829Z',
                order_date: '2022-10-30T21:52:24.789Z',
                number_of_guest: 1,
                order_type: 'TO_GO',
                staff: {
                    last_name: 'Plini',
                    first_name: 'Plini',
                    id: '630f1c078653105bf1478b82'
                },
                table: null,
                customer: {
                    tel: '',
                    last_name: '',
                    first_name: 'Guest'
                },
                restaurant: {
                    email: 'corywong@mail.com',
                    business_phone: '0222222222',
                    address: {
                        zip_code: '75001',
                        state: 'Dallas',
                        city: 'Addison',
                        address: 'Some Address'
                    },
                    legal_business_name: 'Holy Beef',
                    id: '630e55a0d9c30fd7cdcb424b'
                },
                created_at: '2022-10-30T21:52:24.820Z',
                updated_at: '2022-10-30T21:52:24.820Z',
                updated_by: {
                    username: 'Plini',
                    id: '630f1c078653105bf1478b82'
                },
                created_by: {
                    username: 'Plini',
                    id: '630f1c078653105bf1478b82'
                },
                id: 'vH0o0Uwj',
                items: [
                    {
                        id: '635ef218376e7921d44f5065',
                        item_status: 'IN_PROGRESS',
                        note: '',
                        amount: 40,
                        unit: 1,
                        is_contain_alcohol: true,
                        modifiers: [
                            {
                                id: '634d2a6c57a023457c8e4990',
                                label: 'Choice of Protein',
                                abbreviation: 'P',
                                name: 'No Protein',
                                native_name: 'Native Name Test',
                                price: 0
                            }
                        ],
                        price: 40,
                        native_name: 'test nativeName',
                        name: 'Chang Beer',
                        menu_id: '634d2a6c57a023457c8e498d',
                        order_id: 'vH0o0Uwj'
                    },
                    {
                        id: '635ef218376e7921d44f5066',
                        item_status: 'IN_PROGRESS',
                        note: '',
                        amount: 40,
                        unit: 1,
                        is_contain_alcohol: true,
                        modifiers: [
                            {
                                id: '634d2a6c57a023457c8e4990',
                                label: 'Choice of Protein',
                                abbreviation: 'P',
                                name: 'Chicken',
                                native_name: 'Native Name Test',
                                price: 0
                            }
                        ],
                        price: 40,
                        native_name: 'test nativeName',
                        name: 'Chang Beer',
                        menu_id: '634d2a6c57a023457c8e498d',
                        order_id: 'vH0o0Uwj'
                    }
                ],
                ticket_no: '1'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateToGoOrder.prototype, "results", void 0);
class PaginateToGoOrderResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: PaginateToGoOrder }),
    __metadata("design:type", PaginateToGoOrder)
], PaginateToGoOrderResponse.prototype, "data", void 0);
exports.PaginateToGoOrderResponse = PaginateToGoOrderResponse;
//# sourceMappingURL=get-order.entity.js.map