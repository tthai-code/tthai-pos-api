/// <reference types="mongoose" />
import { LogLogic } from 'src/modules/log/logic/log.logic';
import { AcceptOrderOnlineDto } from '../dto/accept-order-online.dto';
import { OrderOnlinePaginateDto } from '../dto/get-order-online.dto';
import { OrderOnlineLogic } from '../logics/order-online.logic';
import { POSOnlineOrderLogic } from '../logics/pos-online-order.logic';
import { OrderOnlineService } from '../services/order-online.service';
export declare class POSOnlineOrderController {
    private readonly orderOnlineLogic;
    private readonly orderOnlineService;
    private readonly posOnlineOrderLogic;
    private readonly logLogic;
    constructor(orderOnlineLogic: OrderOnlineLogic, orderOnlineService: OrderOnlineService, posOnlineOrderLogic: POSOnlineOrderLogic, logLogic: LogLogic);
    getAllOrder(query: OrderOnlinePaginateDto, request: any): Promise<any>;
    getOrderDetail(orderId: string): Promise<{
        items: any[];
        customer: {
            id: string;
            firstName: string;
            lastName: string;
            tel: string;
            email: string;
        };
        ticketNo: string;
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order-online.schema").RestaurantFields;
        staff: import("../schemas/order-online.schema").StaffFields;
        total: number;
        paymentStatus: string;
        convenienceFee: number;
        serviceCharge: number;
        table: import("../schemas/order-online.schema").TableFields;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderMethod: string;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/order-online.schema").ItemFields>[];
        orderStatus: string;
        tips: number;
    }>;
    acceptOrder(payload: AcceptOrderOnlineDto): Promise<{
        success: boolean;
    }>;
    cancelOrder(payload: AcceptOrderOnlineDto): Promise<{
        success: boolean;
    }>;
    readyOrder(payload: AcceptOrderOnlineDto): Promise<{
        success: boolean;
    }>;
    upcomingOrder(payload: AcceptOrderOnlineDto): Promise<{
        success: boolean;
    }>;
    completeOrder(payload: AcceptOrderOnlineDto): Promise<{
        success: boolean;
    }>;
}
