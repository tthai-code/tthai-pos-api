import { OrderLogic } from '../logics/order.logic';
import { OrderService } from '../services/order.service';
export declare class OrderController {
    private readonly orderService;
    private readonly orderLogic;
    constructor(orderService: OrderService, orderLogic: OrderLogic);
    getOrderByTable(tableId: string): Promise<import("../schemas/order.schema").OrderDocument>;
    printKitchen(id: string): Promise<import("../schemas/order.schema").OrderDocument>;
    printInvoice(id: string): Promise<import("../schemas/order.schema").OrderDocument>;
}
