"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderOnlineController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const customer_auth_guard_1 = require("../../auth/guards/customer-auth.guard");
const log_logic_1 = require("../../log/logic/log.logic");
const create_order_pos_dto_1 = require("../dto/create-order-pos.dto");
const get_order_online_dto_1 = require("../dto/get-order-online.dto");
const pay_online_order_dto_1 = require("../dto/pay-online-order.dto");
const create_order_entity_1 = require("../entity/create-order.entity");
const get_order_entity_1 = require("../entity/get-order.entity");
const online_order_entity_1 = require("../entity/online-order.entity");
const order_online_logic_1 = require("../logics/order-online.logic");
let OrderOnlineController = class OrderOnlineController {
    constructor(orderOnlineLogic, logLogic) {
        this.orderOnlineLogic = orderOnlineLogic;
        this.logLogic = logLogic;
    }
    async createOrderOnline(payload, request) {
        var _a;
        const customerId = (_a = customer_auth_guard_1.CustomerAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.id;
        const orders = await this.orderOnlineLogic.createOrderOnline(customerId, payload);
        await this.logLogic.createLogic(request, 'Create Order-Online status NEW', orders);
        return orders;
    }
    async updatePaidOrderAndCreateOrderById(orderId, payload, request) {
        const order = await this.orderOnlineLogic.updatePaidOrderOnlineAndCreateOrderById(orderId, payload);
        await this.logLogic.createLogic(request, 'Update Order-Online Pay status Pay and Create Orders', order);
        return order;
    }
    async getOrderAll(query) {
        var _a;
        const customerId = (_a = customer_auth_guard_1.CustomerAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.id;
        return await this.orderOnlineLogic.getOrderOnlineAll(customerId, query);
    }
    async getOrderToGoById(orderId) {
        return await this.orderOnlineLogic.getOrderOnlineById(orderId);
    }
};
__decorate([
    (0, common_1.Post)('checkout'),
    (0, swagger_1.ApiCreatedResponse)({ type: () => online_order_entity_1.CreateOnlineOrderResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create Order-Online status NEW',
        description: 'use *bearer* `customer_token`\n\n NEW Order Online'
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_order_pos_dto_1.CreateOrderOnline, Object]),
    __metadata("design:returntype", Promise)
], OrderOnlineController.prototype, "createOrderOnline", null);
__decorate([
    (0, common_1.Patch)(':orderId/paid'),
    (0, swagger_1.ApiCreatedResponse)({ type: () => create_order_entity_1.CreateOrderCheckoutInResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Update Order-Online Pay status Pay and Create Orders',
        description: 'use *bearer* `customer_token`\n\n Pay Order Online'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, pay_online_order_dto_1.PayOnlineOrderDto, Object]),
    __metadata("design:returntype", Promise)
], OrderOnlineController.prototype, "updatePaidOrderAndCreateOrderById", null);
__decorate([
    (0, common_1.Get)('history'),
    (0, swagger_1.ApiOkResponse)({ type: () => get_order_entity_1.GetToGoOrderByIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Order All',
        description: 'use *bearer* `customer_token`\n\nGet order All'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_order_online_dto_1.OrderOnlineCustomerPaginateDto]),
    __metadata("design:returntype", Promise)
], OrderOnlineController.prototype, "getOrderAll", null);
__decorate([
    (0, common_1.Get)(':orderId'),
    (0, swagger_1.ApiOkResponse)({ type: () => get_order_entity_1.GetToGoOrderByIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Order By ID',
        description: 'use *bearer* `customer_token`\n\nGet order by order id'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OrderOnlineController.prototype, "getOrderToGoById", null);
OrderOnlineController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('order-online'),
    (0, common_1.UseGuards)(customer_auth_guard_1.CustomerAuthGuard),
    (0, common_1.Controller)('v1/public/online-order'),
    __metadata("design:paramtypes", [order_online_logic_1.OrderOnlineLogic,
        log_logic_1.LogLogic])
], OrderOnlineController);
exports.OrderOnlineController = OrderOnlineController;
//# sourceMappingURL=order-public-online.controller.js.map