"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicOrderHourController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const order_hour_entity_1 = require("../entity/order-hour.entity");
const order_hour_logic_1 = require("../logics/order-hour.logic");
let PublicOrderHourController = class PublicOrderHourController {
    constructor(restaurantService, orderHourLogic) {
        this.restaurantService = restaurantService;
        this.orderHourLogic = orderHourLogic;
    }
    async checkHour(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const checkOpenHour = await this.orderHourLogic.checkAvailableOrder(restaurant);
        return { isAvailable: checkOpenHour };
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => order_hour_entity_1.CheckOrderHourResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Check Available Restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PublicOrderHourController.prototype, "checkHour", null);
PublicOrderHourController = __decorate([
    (0, swagger_1.ApiTags)('public/order-hour'),
    (0, common_1.Controller)('v1/public/order-hour'),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        order_hour_logic_1.OrderHourLogic])
], PublicOrderHourController);
exports.PublicOrderHourController = PublicOrderHourController;
//# sourceMappingURL=public-order-hour.controller.js.map