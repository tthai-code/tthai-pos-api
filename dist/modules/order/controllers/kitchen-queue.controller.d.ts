import { ChangeItemStatusDto } from '../dto/change-item-status.dto';
import { PaginateKitchenQueue } from '../dto/get-kitchen.dto';
import { UpdateQueueStatusDto } from '../dto/update-queue-status.dto';
import { KitchenQueueLogic } from '../logics/kitchen-queue.logic';
import { KitchenQueueService } from '../services/kitchen-queue.service';
import { OrderItemsService } from '../services/order-items.service';
export declare class KitchenQueueController {
    private readonly kitchenQueueService;
    private readonly kitchenQueueLogic;
    private readonly orderItemService;
    constructor(kitchenQueueService: KitchenQueueService, kitchenQueueLogic: KitchenQueueLogic, orderItemService: OrderItemsService);
    getKitchenQueues(query: PaginateKitchenQueue): Promise<any>;
    updateItemStatus(orderItemId: string, payload: ChangeItemStatusDto): Promise<{
        success: boolean;
    }>;
    updateQueueStatus(kitchenQueueId: string, payload: UpdateQueueStatusDto): Promise<import("../schemas/kitchen-queue.schema").KitchenQueueDocument>;
}
