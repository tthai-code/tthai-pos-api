"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_logic_1 = require("../logics/order.logic");
const order_service_1 = require("../services/order.service");
let OrderController = class OrderController {
    constructor(orderService, orderLogic) {
        this.orderService = orderService;
        this.orderLogic = orderLogic;
    }
    async getOrderByTable(tableId) {
        const order = await this.orderService.findOne({
            'table.id': tableId,
            status: status_enum_1.StatusEnum.ACTIVE,
            orderStatus: order_state_enum_1.OrderStateEnum.PENDING
        });
        if (!order)
            throw new common_1.NotFoundException();
        return order;
    }
    async printKitchen(id) {
        const order = await this.orderService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE,
            orderStatus: order_state_enum_1.OrderStateEnum.PENDING
        });
        if (!order)
            throw new common_1.NotFoundException('Order not found.');
        return await this.orderService.update(id, { printKitchenDate: new Date() });
    }
    async printInvoice(id) {
        const order = await this.orderService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE,
            orderStatus: order_state_enum_1.OrderStateEnum.PENDING
        });
        if (!order)
            throw new common_1.NotFoundException('Order not found.');
        return await this.orderService.update(id, { printInvoiceDate: new Date() });
    }
};
__decorate([
    (0, common_1.Get)(':tableId/table'),
    (0, swagger_1.ApiOperation)({ summary: 'Get Order By Seated Table ID' }),
    __param(0, (0, common_1.Param)('tableId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OrderController.prototype, "getOrderByTable", null);
__decorate([
    (0, common_1.Patch)(':id/kitchen'),
    (0, swagger_1.ApiOperation)({ summary: 'Print Receipt Kitchen' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OrderController.prototype, "printKitchen", null);
__decorate([
    (0, common_1.Patch)(':id/invoice'),
    (0, swagger_1.ApiOperation)({ summary: 'Print Invoice' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OrderController.prototype, "printInvoice", null);
OrderController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('orders'),
    (0, common_1.Controller)('v1/orders'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __metadata("design:paramtypes", [order_service_1.OrderService,
        order_logic_1.OrderLogic])
], OrderController);
exports.OrderController = OrderController;
//# sourceMappingURL=order.controller.js.map