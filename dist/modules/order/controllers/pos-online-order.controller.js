"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSOnlineOrderController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const log_logic_1 = require("../../log/logic/log.logic");
const accept_order_online_dto_1 = require("../dto/accept-order-online.dto");
const get_order_online_dto_1 = require("../dto/get-order-online.dto");
const pos_get_order_entity_1 = require("../entity/pos-get-order.entity");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_online_logic_1 = require("../logics/order-online.logic");
const pos_online_order_logic_1 = require("../logics/pos-online-order.logic");
const order_online_service_1 = require("../services/order-online.service");
let POSOnlineOrderController = class POSOnlineOrderController {
    constructor(orderOnlineLogic, orderOnlineService, posOnlineOrderLogic, logLogic) {
        this.orderOnlineLogic = orderOnlineLogic;
        this.orderOnlineService = orderOnlineService;
        this.posOnlineOrderLogic = posOnlineOrderLogic;
        this.logLogic = logLogic;
    }
    async getAllOrder(query, request) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const orders = await this.posOnlineOrderLogic.getAllOrder(restaurantId, query);
        await this.logLogic.createLogic(request, 'Get order online list', orders);
        return orders;
    }
    async getOrderDetail(orderId) {
        return await this.orderOnlineLogic.getOrderOnlineByIdForPOS(orderId);
    }
    async acceptOrder(payload) {
        return await this.orderOnlineLogic.acceptOrder(payload.orderId);
    }
    async cancelOrder(payload) {
        return await this.orderOnlineLogic.rejectOrder(payload.orderId);
    }
    async readyOrder(payload) {
        await this.orderOnlineService.update(payload.orderId, {
            orderStatus: order_state_enum_1.OrderStateEnum.READY
        });
        return { success: true };
    }
    async upcomingOrder(payload) {
        await this.orderOnlineService.update(payload.orderId, {
            orderStatus: order_state_enum_1.OrderStateEnum.UPCOMING
        });
        return { success: true };
    }
    async completeOrder(payload) {
        return await this.orderOnlineLogic.completeOrder(payload.orderId);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({
        summary: 'Get order online list',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_order_online_dto_1.OrderOnlinePaginateDto, Object]),
    __metadata("design:returntype", Promise)
], POSOnlineOrderController.prototype, "getAllOrder", null);
__decorate([
    (0, common_1.Get)(':orderId'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_get_order_entity_1.GetOnlineOrderByIdForPOSResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Online Order Detail by ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSOnlineOrderController.prototype, "getOrderDetail", null);
__decorate([
    (0, common_1.Patch)('in-progress'),
    (0, swagger_1.ApiOperation)({
        summary: 'Update online order status to In progress',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_online_dto_1.AcceptOrderOnlineDto]),
    __metadata("design:returntype", Promise)
], POSOnlineOrderController.prototype, "acceptOrder", null);
__decorate([
    (0, common_1.Patch)('cancel'),
    (0, swagger_1.ApiOperation)({
        summary: 'Cancel online order',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_online_dto_1.AcceptOrderOnlineDto]),
    __metadata("design:returntype", Promise)
], POSOnlineOrderController.prototype, "cancelOrder", null);
__decorate([
    (0, common_1.Patch)('ready'),
    (0, swagger_1.ApiOperation)({
        summary: 'Ready online order',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_online_dto_1.AcceptOrderOnlineDto]),
    __metadata("design:returntype", Promise)
], POSOnlineOrderController.prototype, "readyOrder", null);
__decorate([
    (0, common_1.Patch)('upcoming'),
    (0, swagger_1.ApiOperation)({
        summary: 'Upcoming online order',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_online_dto_1.AcceptOrderOnlineDto]),
    __metadata("design:returntype", Promise)
], POSOnlineOrderController.prototype, "upcomingOrder", null);
__decorate([
    (0, common_1.Patch)('complete'),
    (0, swagger_1.ApiOperation)({
        summary: 'Complete online order',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_online_dto_1.AcceptOrderOnlineDto]),
    __metadata("design:returntype", Promise)
], POSOnlineOrderController.prototype, "completeOrder", null);
POSOnlineOrderController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/online-order'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/online-order'),
    __metadata("design:paramtypes", [order_online_logic_1.OrderOnlineLogic,
        order_online_service_1.OrderOnlineService,
        pos_online_order_logic_1.POSOnlineOrderLogic,
        log_logic_1.LogLogic])
], POSOnlineOrderController);
exports.POSOnlineOrderController = POSOnlineOrderController;
//# sourceMappingURL=pos-online-order.controller.js.map