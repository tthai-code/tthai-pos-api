"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSOrderController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const role_decorator_1 = require("../../../decorators/role.decorator");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const log_logic_1 = require("../../log/logic/log.logic");
const staff_role_enum_1 = require("../../staff/common/staff-role.enum");
const create_order_pos_dto_1 = require("../dto/create-order-pos.dto");
const get_order_info_dto_1 = require("../dto/get-order-info.dto");
const get_order_dto_1 = require("../dto/get-order.dto");
const move_order_table_dto_1 = require("../dto/move-order-table.dto");
const summary_order_dto_1 = require("../dto/summary-order.dto");
const update_exist_items_dto_1 = require("../dto/update-exist-items.dto");
const update_items_dto_1 = require("../dto/update-items.dto");
const create_order_entity_1 = require("../entity/create-order.entity");
const get_order_entity_1 = require("../entity/get-order.entity");
const order_entity_1 = require("../entity/order.entity");
const update_order_entity_1 = require("../entity/update-order.entity");
const order_items_logic_1 = require("../logics/order-items.logic");
const order_logic_1 = require("../logics/order.logic");
const pos_order_logic_1 = require("../logics/pos-order.logic");
const order_service_1 = require("../services/order.service");
let POSOrderController = class POSOrderController {
    constructor(orderService, orderLogic, orderItemLogic, posOrderLogic, logLogic) {
        this.orderService = orderService;
        this.orderLogic = orderLogic;
        this.orderItemLogic = orderItemLogic;
        this.posOrderLogic = posOrderLogic;
        this.logLogic = logLogic;
    }
    async createOrderDineInPOS(payload, request) {
        var _a;
        const staffId = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.id;
        const created = await this.orderLogic.createOrderDineInPOS(staffId, payload);
        await this.logLogic.createLogic(request, 'Create Dine In Order POS', created);
        return created;
    }
    async createOrderToGoPOS(payload, request) {
        var _a;
        const staffId = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.id;
        const created = await this.orderLogic.createOrderToGoPOS(staffId, payload);
        await this.logLogic.createLogic(request, 'Create To Go Order POS', created);
        return created;
    }
    async getOrderByTable(tableId) {
        return await this.orderLogic.getOrderByTable(tableId);
    }
    async getOrderToGoById(orderId) {
        return await this.orderLogic.getOrderToGoById(orderId);
    }
    async getPaginateToGoOrder(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.orderLogic.getPaginateOrderToGo(query.buildQuery(restaurantId), query);
    }
    async voidOrderById(orderId, request) {
        const voidOrder = await this.orderLogic.voidOrderById(orderId);
        await this.logLogic.createLogic(request, 'Void Order By ID', voidOrder);
        return voidOrder;
    }
    async moveOrderTableById(orderId, payload, request) {
        const moved = await this.orderLogic.moveOrderTable(orderId, payload);
        await this.logLogic.createLogic(request, 'Move Table Order By ID', moved);
        return moved;
    }
    async addNewOrderItemsById(orderId, payload, request) {
        const updated = await this.orderLogic.updateNewOrderItems(orderId, payload);
        await this.logLogic.createLogic(request, 'Add new order items by order id', updated);
        return updated;
    }
    async updateExistOrderItem(orderItemId, payload, request) {
        const updated = await this.orderItemLogic.updateExistItems(orderItemId, payload);
        await this.logLogic.createLogic(request, 'update exist order item', updated);
        return updated;
    }
    async deleteOrderItem(orderItemId, request) {
        const deleted = await this.orderItemLogic.removeItem(orderItemId);
        await this.logLogic.createLogic(request, 'delete/cancel order item', deleted);
        return deleted;
    }
    async summaryOrderItems(orderId, payload, request) {
        const summary = await this.orderItemLogic.summaryOrder(orderId, payload);
        await this.logLogic.createLogic(request, 'summary order items', summary);
        return summary;
    }
    async getOrderInfoByID(id, query) {
        const order = await this.posOrderLogic.getOrderInfo(id, query.orderType);
        return order;
    }
};
__decorate([
    (0, common_1.Post)('dine-in'),
    (0, swagger_1.ApiCreatedResponse)({ type: () => create_order_entity_1.CreateOrderDineInResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create Dine In Order POS',
        description: 'use *bearer* `staff_token`\n\nDine in Order'
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_order_pos_dto_1.CreateOrderDineInPOSDto, Object]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "createOrderDineInPOS", null);
__decorate([
    (0, common_1.Post)('to-go'),
    (0, swagger_1.ApiCreatedResponse)({ type: () => create_order_entity_1.CreateOrderToGoResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create To Go Order POS',
        description: 'use *bearer* `staff_token`\n\nTO Go Order'
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_order_pos_dto_1.CreateOrderToGoPOSDto, Object]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "createOrderToGoPOS", null);
__decorate([
    (0, common_1.Get)(':tableId/table'),
    (0, swagger_1.ApiOkResponse)({ type: () => get_order_entity_1.GetOrderByTableIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Dine In Order By Table ID',
        description: 'use *bearer* `staff_token`\n\nGet dine-in order by table id while table is seating'
    }),
    __param(0, (0, common_1.Param)('tableId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "getOrderByTable", null);
__decorate([
    (0, common_1.Get)(':orderId/to-go'),
    (0, swagger_1.ApiOkResponse)({ type: () => get_order_entity_1.GetToGoOrderByIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get To Go Order By ID',
        description: 'use *bearer* `staff_token`\n\nGet to-go order by order id'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "getOrderToGoById", null);
__decorate([
    (0, common_1.Get)('to-go'),
    (0, swagger_1.ApiOkResponse)({ type: () => get_order_entity_1.PaginateToGoOrderResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Paginate To Go Order',
        description: 'use *bearer* `staff_token`\n\nGet paginate list to-go order'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_order_dto_1.ToGoOrderPaginateDto]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "getPaginateToGoOrder", null);
__decorate([
    (0, common_1.Patch)(':orderId/void'),
    (0, role_decorator_1.Roles)(staff_role_enum_1.StaffRoleEnum.MANAGER),
    (0, swagger_1.ApiOkResponse)({ type: () => order_entity_1.VoidOrderResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Void Order by ID',
        description: 'use *bearer* `staff_token`\n\nrole manager can void order by id'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "voidOrderById", null);
__decorate([
    (0, common_1.Patch)(':orderId/move-table'),
    (0, swagger_1.ApiOkResponse)({ type: () => order_entity_1.MoveTableResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Move Table Order By ID',
        description: 'use *bearer* `staff_token`\n\nmove table by order id'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, move_order_table_dto_1.MoveOrderTableDto, Object]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "moveOrderTableById", null);
__decorate([
    (0, common_1.Patch)(':orderId/items'),
    (0, swagger_1.ApiOkResponse)({ type: () => update_order_entity_1.AddNewOrderItemsResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Add new order items by order id',
        description: 'use *bearer* `staff_token`\n\nadd new order items by order id'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_items_dto_1.UpdateOrderItemsDto, Object]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "addNewOrderItemsById", null);
__decorate([
    (0, common_1.Patch)(':orderItemId/order-item'),
    (0, swagger_1.ApiOkResponse)({ type: () => update_order_entity_1.UpdateExistOrderItemsResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'update exist order item',
        description: 'use *bearer* `staff_token`\n\nupdate exist order items by order item id'
    }),
    __param(0, (0, common_1.Param)('orderItemId')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_exist_items_dto_1.UpdateExistOrderItemsDto, Object]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "updateExistOrderItem", null);
__decorate([
    (0, common_1.Delete)(':orderItemId/order-item'),
    (0, role_decorator_1.Roles)(staff_role_enum_1.StaffRoleEnum.MANAGER),
    (0, swagger_1.ApiOkResponse)({ type: () => update_order_entity_1.DeleteOrderItemResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'delete/cancel order item',
        description: 'use *bearer* `staff_token`\n\ndelete/cancel order items by order item id'
    }),
    __param(0, (0, common_1.Param)('orderItemId')),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "deleteOrderItem", null);
__decorate([
    (0, common_1.Post)(':orderId/summary'),
    (0, swagger_1.ApiOkResponse)({ type: () => update_order_entity_1.SummaryOrderItemsResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'summary order items',
        description: 'use *bearer* `staff_token`\n\nsummary order items by order id'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, summary_order_dto_1.SummaryOrderDto, Object]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "summaryOrderItems", null);
__decorate([
    (0, common_1.Get)(':orderId/info'),
    (0, swagger_1.ApiOkResponse)({ type: () => get_order_entity_1.GetOrderByTableIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'get order info by order id',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, get_order_info_dto_1.GetOrderInfoDto]),
    __metadata("design:returntype", Promise)
], POSOrderController.prototype, "getOrderInfoByID", null);
POSOrderController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/order'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('/v1/pos/order'),
    __metadata("design:paramtypes", [order_service_1.OrderService,
        order_logic_1.OrderLogic,
        order_items_logic_1.OrderItemsLogic,
        pos_order_logic_1.POSOrderLogic,
        log_logic_1.LogLogic])
], POSOrderController);
exports.POSOrderController = POSOrderController;
//# sourceMappingURL=pos-order.controller.js.map