/// <reference types="mongoose" />
import { LogLogic } from 'src/modules/log/logic/log.logic';
import { CreateOrderOnline } from '../dto/create-order-pos.dto';
import { OrderOnlineCustomerPaginateDto } from '../dto/get-order-online.dto';
import { PayOnlineOrderDto } from '../dto/pay-online-order.dto';
import { OrderOnlineLogic } from '../logics/order-online.logic';
export declare class OrderOnlineController {
    private readonly orderOnlineLogic;
    private readonly logLogic;
    constructor(orderOnlineLogic: OrderOnlineLogic, logLogic: LogLogic);
    createOrderOnline(payload: CreateOrderOnline, request: any): Promise<{
        id: any;
        orderStatus: string;
        paymentStatus: string;
    }>;
    updatePaidOrderAndCreateOrderById(orderId: string, payload: PayOnlineOrderDto, request: any): Promise<import("../schemas/order-online.schema").OrderOnlineDocument>;
    getOrderAll(query: OrderOnlineCustomerPaginateDto): Promise<any>;
    getOrderToGoById(orderId: string): Promise<{
        items: any[];
        customer: {
            id: string;
            firstName: string;
            lastName: string;
            tel: string;
        };
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order-online.schema").RestaurantFields;
        staff: import("../schemas/order-online.schema").StaffFields;
        total: number;
        paymentStatus: string;
        convenienceFee: number;
        serviceCharge: number;
        table: import("../schemas/order-online.schema").TableFields;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderMethod: string;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/order-online.schema").ItemFields>[];
        orderStatus: string;
        tips: number;
    }>;
}
