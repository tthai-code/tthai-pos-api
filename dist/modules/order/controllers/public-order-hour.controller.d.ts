import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { OrderHourLogic } from '../logics/order-hour.logic';
export declare class PublicOrderHourController {
    private readonly restaurantService;
    private readonly orderHourLogic;
    constructor(restaurantService: RestaurantService, orderHourLogic: OrderHourLogic);
    checkHour(restaurantId: string): Promise<{
        isAvailable: boolean;
    }>;
}
