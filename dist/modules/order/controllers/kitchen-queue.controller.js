"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenQueueController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const change_item_status_dto_1 = require("../dto/change-item-status.dto");
const get_kitchen_dto_1 = require("../dto/get-kitchen.dto");
const update_queue_status_dto_1 = require("../dto/update-queue-status.dto");
const kitchen_entity_1 = require("../entity/kitchen.entity");
const kitchen_queue_logic_1 = require("../logics/kitchen-queue.logic");
const kitchen_queue_service_1 = require("../services/kitchen-queue.service");
const order_items_service_1 = require("../services/order-items.service");
let KitchenQueueController = class KitchenQueueController {
    constructor(kitchenQueueService, kitchenQueueLogic, orderItemService) {
        this.kitchenQueueService = kitchenQueueService;
        this.kitchenQueueLogic = kitchenQueueLogic;
        this.orderItemService = orderItemService;
    }
    async getKitchenQueues(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.kitchenQueueLogic.getPaginateKitchenQueue(query.buildQuery(restaurantId), query);
    }
    async updateItemStatus(orderItemId, payload) {
        const updated = await this.orderItemService.update(orderItemId, {
            itemStatus: payload.itemStatus
        });
        if (!updated)
            throw new common_1.BadRequestException();
        return { success: true };
    }
    async updateQueueStatus(kitchenQueueId, payload) {
        return await this.kitchenQueueService.update(kitchenQueueId, payload);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => kitchen_entity_1.PaginateKitchenQueueResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Paginate Kitchen Queue',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_kitchen_dto_1.PaginateKitchenQueue]),
    __metadata("design:returntype", Promise)
], KitchenQueueController.prototype, "getKitchenQueues", null);
__decorate([
    (0, common_1.Patch)(':orderItemId/item'),
    (0, swagger_1.ApiOkResponse)({ type: () => kitchen_entity_1.UpdateItemStatusResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Change Item Status',
        description: 'use *bearer* `staff_token`\n\nchange order item status by order item id'
    }),
    __param(0, (0, common_1.Param)('orderItemId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, change_item_status_dto_1.ChangeItemStatusDto]),
    __metadata("design:returntype", Promise)
], KitchenQueueController.prototype, "updateItemStatus", null);
__decorate([
    (0, common_1.Patch)(':kitchenQueueId'),
    (0, swagger_1.ApiOperation)({
        summary: 'Update Kitchen Queue Status',
        description: 'use *bearer* `staff_token`\n\nchange queue status by kitchen queue id'
    }),
    __param(0, (0, common_1.Param)('kitchenQueueId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_queue_status_dto_1.UpdateQueueStatusDto]),
    __metadata("design:returntype", Promise)
], KitchenQueueController.prototype, "updateQueueStatus", null);
KitchenQueueController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/kitchen'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/kitchen'),
    __metadata("design:paramtypes", [kitchen_queue_service_1.KitchenQueueService,
        kitchen_queue_logic_1.KitchenQueueLogic,
        order_items_service_1.OrderItemsService])
], KitchenQueueController);
exports.KitchenQueueController = KitchenQueueController;
//# sourceMappingURL=kitchen-queue.controller.js.map