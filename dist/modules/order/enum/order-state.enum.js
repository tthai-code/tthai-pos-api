"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThirdPartyOrderStatusEnum = exports.OrderStateEnum = void 0;
var OrderStateEnum;
(function (OrderStateEnum) {
    OrderStateEnum["PENDING"] = "PENDING";
    OrderStateEnum["NEW"] = "NEW";
    OrderStateEnum["IN_PROGRESS"] = "IN_PROGRESS";
    OrderStateEnum["READY"] = "READY";
    OrderStateEnum["UPCOMING"] = "UPCOMING";
    OrderStateEnum["COMPLETED"] = "COMPLETED";
    OrderStateEnum["PAID"] = "PAID";
    OrderStateEnum["CANCELED"] = "CANCELED";
    OrderStateEnum["VOID"] = "VOID";
    OrderStateEnum["REFUNDED"] = "REFUNDED";
})(OrderStateEnum = exports.OrderStateEnum || (exports.OrderStateEnum = {}));
var ThirdPartyOrderStatusEnum;
(function (ThirdPartyOrderStatusEnum) {
    ThirdPartyOrderStatusEnum["ACTIVE"] = "ACTIVE";
    ThirdPartyOrderStatusEnum["NEW"] = "NEW";
    ThirdPartyOrderStatusEnum["IN_PROGRESS"] = "IN_PROGRESS";
    ThirdPartyOrderStatusEnum["READY"] = "READY";
    ThirdPartyOrderStatusEnum["UPCOMING"] = "UPCOMING";
    ThirdPartyOrderStatusEnum["COMPLETED"] = "COMPLETED";
})(ThirdPartyOrderStatusEnum = exports.ThirdPartyOrderStatusEnum || (exports.ThirdPartyOrderStatusEnum = {}));
//# sourceMappingURL=order-state.enum.js.map