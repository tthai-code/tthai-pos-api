"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OnlinePaymentStatusEnum = void 0;
var OnlinePaymentStatusEnum;
(function (OnlinePaymentStatusEnum) {
    OnlinePaymentStatusEnum["PAID"] = "PAID";
    OnlinePaymentStatusEnum["UNPAID"] = "UNPAID";
})(OnlinePaymentStatusEnum = exports.OnlinePaymentStatusEnum || (exports.OnlinePaymentStatusEnum = {}));
//# sourceMappingURL=payment-status.enum.js.map