"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderMethodEnum = exports.OrderTypeEnum = void 0;
var OrderTypeEnum;
(function (OrderTypeEnum) {
    OrderTypeEnum["DINE_IN"] = "DINE_IN";
    OrderTypeEnum["TO_GO"] = "TO_GO";
    OrderTypeEnum["ONLINE"] = "ONLINE";
    OrderTypeEnum["TTHAI_APP"] = "TTHAI_APP";
    OrderTypeEnum["THIRD_PARTY"] = "THIRD_PARTY";
})(OrderTypeEnum = exports.OrderTypeEnum || (exports.OrderTypeEnum = {}));
var OrderMethodEnum;
(function (OrderMethodEnum) {
    OrderMethodEnum["DINE_IN"] = "DINE_IN";
    OrderMethodEnum["DELIVERY"] = "DELIVERY";
    OrderMethodEnum["PICKUP"] = "PICKUP";
    OrderMethodEnum["CURBSIDE"] = "CURBSIDE";
})(OrderMethodEnum = exports.OrderMethodEnum || (exports.OrderMethodEnum = {}));
//# sourceMappingURL=order-type.enum.js.map