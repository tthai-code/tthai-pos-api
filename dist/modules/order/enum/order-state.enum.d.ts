export declare enum OrderStateEnum {
    PENDING = "PENDING",
    NEW = "NEW",
    IN_PROGRESS = "IN_PROGRESS",
    READY = "READY",
    UPCOMING = "UPCOMING",
    COMPLETED = "COMPLETED",
    PAID = "PAID",
    CANCELED = "CANCELED",
    VOID = "VOID",
    REFUNDED = "REFUNDED"
}
export declare enum ThirdPartyOrderStatusEnum {
    ACTIVE = "ACTIVE",
    NEW = "NEW",
    IN_PROGRESS = "IN_PROGRESS",
    READY = "READY",
    UPCOMING = "UPCOMING",
    COMPLETED = "COMPLETED"
}
