export declare enum OnlinePaymentStatusEnum {
    PAID = "PAID",
    UNPAID = "UNPAID"
}
