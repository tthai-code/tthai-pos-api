export declare enum OrderMethod {
    DELIVERY = "DELIVERY",
    PICKUP = "PICKUP"
}
