"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderMethod = void 0;
var OrderMethod;
(function (OrderMethod) {
    OrderMethod["DELIVERY"] = "DELIVERY";
    OrderMethod["PICKUP"] = "PICKUP";
})(OrderMethod = exports.OrderMethod || (exports.OrderMethod = {}));
//# sourceMappingURL=order-method.enum.js.map