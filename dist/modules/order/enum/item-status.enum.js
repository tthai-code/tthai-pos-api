"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ItemStatusEnum = void 0;
var ItemStatusEnum;
(function (ItemStatusEnum) {
    ItemStatusEnum["IN_PROGRESS"] = "IN_PROGRESS";
    ItemStatusEnum["READY"] = "READY";
    ItemStatusEnum["COMPLETED"] = "COMPLETED";
    ItemStatusEnum["CANCELED"] = "CANCELED";
})(ItemStatusEnum = exports.ItemStatusEnum || (exports.ItemStatusEnum = {}));
//# sourceMappingURL=item-status.enum.js.map