"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderHourLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs = require("dayjs");
const utc = require("dayjs/plugin/utc");
const timezone = require("dayjs/plugin/timezone");
const restaurant_holiday_service_1 = require("../../restaurant-holiday/services/restaurant-holiday.service");
const restaurant_day_service_1 = require("../../restaurant-hours/services/restaurant-day.service");
const restaurant_period_service_1 = require("../../restaurant-hours/services/restaurant-period.service");
const restaurant_schema_1 = require("../../restaurant/schemas/restaurant.schema");
const dateTime_1 = require("../../../utilities/dateTime");
const status_enum_1 = require("../../../common/enum/status.enum");
dayjs.extend(utc);
dayjs.extend(timezone);
let OrderHourLogic = class OrderHourLogic {
    constructor(restaurantPeriodService, restaurantDayService, restaurantHolidaysService) {
        this.restaurantPeriodService = restaurantPeriodService;
        this.restaurantDayService = restaurantDayService;
        this.restaurantHolidaysService = restaurantHolidaysService;
    }
    async checkAvailableOrder(restaurant) {
        var _a, _b;
        const { id: restaurantId, timeZone } = restaurant;
        const currentDay = dayjs().tz(timeZone);
        const dayOfWeekString = await (0, dateTime_1.GetDayStringByDay)(currentDay.day());
        const restaurantDay = await this.restaurantDayService.findOne({
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE,
            label: dayOfWeekString
        });
        if (restaurantDay.isClosed) {
            return false;
        }
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE,
            label: dayOfWeekString
        });
        for (const item of restaurantPeriods) {
            const [openHour, openMinute] = (_a = item === null || item === void 0 ? void 0 : item.opensAt) === null || _a === void 0 ? void 0 : _a.split(':');
            const [closeHour, closeMinute] = (_b = item === null || item === void 0 ? void 0 : item.closesAt) === null || _b === void 0 ? void 0 : _b.split(':');
            const opensAt = currentDay
                .hour(+(openHour - 1))
                .minute(+openMinute)
                .second(0)
                .millisecond(0)
                .valueOf();
            const closesAt = currentDay
                .hour(+(closeHour - 1))
                .minute(+closeMinute)
                .second(0)
                .millisecond(0)
                .valueOf();
            const now = currentDay.valueOf();
            if (now > opensAt && now < closesAt) {
                return true;
            }
        }
        return false;
    }
};
OrderHourLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_period_service_1.RestaurantPeriodService,
        restaurant_day_service_1.RestaurantDayService,
        restaurant_holiday_service_1.RestaurantHolidaysService])
], OrderHourLogic);
exports.OrderHourLogic = OrderHourLogic;
//# sourceMappingURL=order-hour.logic.js.map