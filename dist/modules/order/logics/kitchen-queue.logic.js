"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenQueueLogic = void 0;
const common_1 = require("@nestjs/common");
const item_status_enum_1 = require("../enum/item-status.enum");
const kitchen_queue_service_1 = require("../services/kitchen-queue.service");
const order_items_service_1 = require("../services/order-items.service");
let KitchenQueueLogic = class KitchenQueueLogic {
    constructor(kitchenQueueService, orderItemsService) {
        this.kitchenQueueService = kitchenQueueService;
        this.orderItemsService = orderItemsService;
    }
    queueStatusCondition(items) {
        if (items.every((item) => item.itemStatus === item_status_enum_1.ItemStatusEnum.READY)) {
            return item_status_enum_1.ItemStatusEnum.READY;
        }
        else if (items.every((item) => item.itemStatus === item_status_enum_1.ItemStatusEnum.COMPLETED)) {
            return item_status_enum_1.ItemStatusEnum.COMPLETED;
        }
        else if (items.some((item) => item.itemStatus === item_status_enum_1.ItemStatusEnum.IN_PROGRESS)) {
            return item_status_enum_1.ItemStatusEnum.IN_PROGRESS;
        }
        else {
            return item_status_enum_1.ItemStatusEnum.CANCELED;
        }
    }
    async getPaginateKitchenQueue(query, queryParam) {
        var _a, _b;
        const kitchenQueues = await this.kitchenQueueService.paginate(query, queryParam, { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 });
        const orderItemsIds = (_a = kitchenQueues === null || kitchenQueues === void 0 ? void 0 : kitchenQueues.docs) === null || _a === void 0 ? void 0 : _a.map((item) => item.items);
        const items = await this.orderItemsService.getAll({
            _id: { $in: orderItemsIds.flat() }
        });
        const result = (_b = kitchenQueues === null || kitchenQueues === void 0 ? void 0 : kitchenQueues.docs) === null || _b === void 0 ? void 0 : _b.map((queue) => {
            const itemsResult = items
                .filter((item) => queue.items.includes(item.id))
                .map((item) => {
                const result = item.toObject();
                return result;
            });
            return Object.assign(Object.assign({}, queue.toObject()), { items: itemsResult });
        });
        return Object.assign(Object.assign({}, kitchenQueues), { docs: result });
    }
};
KitchenQueueLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [kitchen_queue_service_1.KitchenQueueService,
        order_items_service_1.OrderItemsService])
], KitchenQueueLogic);
exports.KitchenQueueLogic = KitchenQueueLogic;
//# sourceMappingURL=kitchen-queue.logic.js.map