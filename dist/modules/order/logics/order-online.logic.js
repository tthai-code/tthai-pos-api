"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderOnlineLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs_1 = require("../../../plugins/dayjs");
const status_enum_1 = require("../../../common/enum/status.enum");
const customer_service_1 = require("../../customer/services/customer.service");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const staff_service_1 = require("../../staff/services/staff.service");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
const payment_status_enum_1 = require("../enum/payment-status.enum");
const kitchen_counter_service_1 = require("../services/kitchen-counter.service");
const kitchen_queue_service_1 = require("../services/kitchen-queue.service");
const order_counter_service_1 = require("../services/order-counter.service");
const order_items_service_1 = require("../services/order-items.service");
const order_service_1 = require("../services/order.service");
const order_online_service_1 = require("../services/order-online.service");
const socket_gateway_1 = require("../../socket/socket.gateway");
const item_status_enum_1 = require("../enum/item-status.enum");
const transaction_logic_1 = require("../../transaction/logics/transaction.logic");
const card_pointe_logic_1 = require("../../payment-gateway/logics/card-pointe.logic");
const card_pointe_interface_1 = require("../../payment-gateway/interfaces/card-pointe.interface");
const online_transaction_1 = require("../../transaction/interfaces/online-transaction");
const mailer_service_1 = require("../../mailer/services/mailer.service");
const transaction_schema_1 = require("../../transaction/schemas/transaction.schema");
const web_setting_service_1 = require("../../web-setting/services/web-setting.service");
const transaction_service_1 = require("../../transaction/services/transaction.service");
const pos_cash_drawer_logic_1 = require("../../cash-drawer/logic/pos-cash-drawer.logic");
const order_hour_logic_1 = require("./order-hour.logic");
const payment_gateway_service_1 = require("../../payment-gateway/services/payment-gateway.service");
let OrderOnlineLogic = class OrderOnlineLogic {
    constructor(orderService, orderOnlineService, orderCounterService, restaurantService, staffService, kitchenQueueService, kitchenCounterService, orderItemsService, customerService, paymentGatewayService, socketGateway, cardPointeLogic, transactionLogic, transactionService, mailerService, webSettingService, posCashDrawerLogic, orderHourLogic) {
        this.orderService = orderService;
        this.orderOnlineService = orderOnlineService;
        this.orderCounterService = orderCounterService;
        this.restaurantService = restaurantService;
        this.staffService = staffService;
        this.kitchenQueueService = kitchenQueueService;
        this.kitchenCounterService = kitchenCounterService;
        this.orderItemsService = orderItemsService;
        this.customerService = customerService;
        this.paymentGatewayService = paymentGatewayService;
        this.socketGateway = socketGateway;
        this.cardPointeLogic = cardPointeLogic;
        this.transactionLogic = transactionLogic;
        this.transactionService = transactionService;
        this.mailerService = mailerService;
        this.webSettingService = webSettingService;
        this.posCashDrawerLogic = posCashDrawerLogic;
        this.orderHourLogic = orderHourLogic;
    }
    randomOrderId() {
        const message = 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789';
        let id = '';
        for (let i = 0; i < 8; i++) {
            id += message.charAt(Math.floor(Math.random() * message.length));
        }
        return id;
    }
    async genOrderId() {
        let id = '';
        while (true) {
            id = this.randomOrderId();
            const isDuplicated = await this.orderService.findOne({
                _id: id
            });
            if (!isDuplicated) {
                break;
            }
        }
        return id;
    }
    async queueKitchen(restaurantId) {
        const { timeZone } = await this.restaurantService.findOne({
            _id: restaurantId
        });
        const now = (0, dayjs_1.default)().tz(timeZone);
        const nowUnix = now.valueOf();
        const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf();
        const endOfDay = now
            .add(1, 'day')
            .hour(3)
            .minute(59)
            .millisecond(999)
            .valueOf();
        let nowFormat = now.subtract(1, 'day').format('YYYYMMDD');
        if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
            nowFormat = now.format('YYYYMMDD');
        }
        const countQueue = await this.kitchenCounterService.getCounter(nowFormat, restaurantId);
        return `${countQueue}`;
    }
    async createOrderOnline(customerId, createOrder) {
        const logic = async (session) => {
            const [restaurant] = await Promise.all([
                this.restaurantService.findOneForOrder(createOrder.restaurantId)
            ]);
            if (!restaurant)
                throw new common_1.NotFoundException('Restaurant not found.');
            const checkOpenHour = await this.orderHourLogic.checkAvailableOrder(restaurant);
            if (!checkOpenHour) {
                throw new common_1.BadRequestException('Now, We are closed. Apologize for any inconvenience.');
            }
            let customer;
            const customerData = await this.customerService.findOne({
                _id: customerId || createOrder.customerId,
                status: status_enum_1.StatusEnum.ACTIVE
            }, { firstName: 1, lastName: 1, tel: 1, email: 1 });
            if (!customerData && !createOrder.customer) {
                customer = {
                    id: null,
                    firstName: 'Guest',
                    lastName: '',
                    tel: '',
                    email: ''
                };
            }
            if (!!createOrder.customer) {
                customer = createOrder.customer;
            }
            else {
                customer = customerData.toObject();
            }
            const paymentGatewayData = await this.paymentGatewayService.findOne({
                restaurantId: createOrder.restaurantId
            });
            createOrder._id = await this.genOrderId();
            createOrder.orderType = order_type_enum_1.OrderTypeEnum.ONLINE;
            createOrder.orderStatus = order_state_enum_1.OrderStateEnum.NEW;
            createOrder.orderDate = new Date();
            createOrder.restaurant = restaurant;
            createOrder.customer = customer;
            if (!!paymentGatewayData && paymentGatewayData.isConvenienceFee) {
                createOrder.convenienceFee = paymentGatewayData.convenienceFee;
            }
            if (!!paymentGatewayData && paymentGatewayData.isServiceCharge) {
                createOrder.serviceCharge =
                    (Number(createOrder.subtotal) || 0) *
                        ((Number(paymentGatewayData.serviceCharge) || 0) / 100);
            }
            else {
                createOrder.serviceCharge = 0;
            }
            createOrder.total =
                (Number(createOrder.subtotal) || 0) +
                    (Number(createOrder.tax) || 0) +
                    (Number(createOrder.convenienceFee) || 0) +
                    (Number(createOrder.serviceCharge) || 0);
            const rawResult = await this.orderOnlineService.transactionCreate(Object.assign(Object.assign({}, createOrder), { summaryItems: createOrder.items }), session);
            const result = rawResult.toObject();
            return {
                id: result.id,
                orderStatus: result.orderStatus,
                paymentStatus: result.paymentStatus
            };
        };
        return (0, mongoose_transaction_1.runTransaction)(logic);
    }
    async updatePaidOrderOnlineAndCreateOrderById(orderId, payload) {
        const logic = async (session) => {
            const order = await this.orderOnlineService.findOne({
                _id: orderId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!order) {
                throw new common_1.NotFoundException('Not found order.');
            }
            if (order.paymentStatus === payment_status_enum_1.OnlinePaymentStatusEnum.PAID) {
                throw new common_1.BadRequestException('This order is already paid.');
            }
            const { restaurant: { id: restaurantId } } = order;
            const restaurant = await this.restaurantService.findOne({
                _id: restaurantId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            const checkOpenHour = await this.orderHourLogic.checkAvailableOrder(restaurant);
            if (!checkOpenHour) {
                throw new common_1.BadRequestException('Now, We are closed. Apologize for any inconvenience.');
            }
            const amount = Number(order.total) + Number(payload.tips);
            const paymentPayload = {
                account: payload.account,
                expiry: `${payload.expirationMonth}${payload.expirationYear}`,
                amount: amount.toFixed(2),
                orderid: order.id,
                name: payload.cardholderName
            };
            const paymentTransaction = await this.cardPointeLogic.payOnlineTransaction(restaurantId, paymentPayload);
            const { binInfo } = paymentTransaction;
            const { cardType, paymentMethod } = this.cardPointeLogic.identifyPaymentMethod(binInfo.product, binInfo.cardusestring);
            const updated = await this.orderOnlineService.transactionUpdate(orderId, {
                total: amount,
                tips: payload.tips,
                paymentStatus: payment_status_enum_1.OnlinePaymentStatusEnum.PAID
            }, session);
            const paymentObject = {
                retRef: paymentTransaction.retref,
                batchId: paymentTransaction.batchid,
                cardType,
                paymentMethod,
                cardLastFour: payload.account.slice(-4),
                paidBy: payload.cardholderName,
                response: paymentTransaction
            };
            const createdTransaction = await this.transactionLogic.createOnlineTransaction(updated, paymentObject, session);
            const orderObj = order.toObject();
            const itemPayload = orderObj.summaryItems.map((item) => {
                const transform = Object.assign(Object.assign({}, item), { orderId: orderId });
                return transform;
            });
            const itemsData = await this.orderItemsService.transactionCreateMany(itemPayload, session);
            const ticketNo = await this.queueKitchen(orderObj.restaurant.id);
            await this.kitchenQueueService.transactionCreate({
                restaurantId: orderObj.restaurant.id,
                orderId: orderId,
                ticketNo,
                items: itemsData.map((item) => item.id)
            }, session);
            const notifyObject = {
                id: updated.id,
                customer: updated.customer,
                type: updated.orderMethod,
                placedDate: updated.orderDate,
                source: updated.orderType,
                orderStatus: updated.orderStatus
            };
            this.socketGateway.server
                .to(`pos-${order.restaurant.id}`)
                .emit('receiveOnlineOrder', notifyObject);
            await this.sendOnlineOrderReceipt(order, ticketNo, createdTransaction, payload.timeZone, payload.offsetTimeZone);
            return { updated, transaction: createdTransaction };
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return this.orderOnlineService.findOne({ _id: orderId });
    }
    async getOrderOnlineById(orderId) {
        var _a, _b, _c, _d;
        const order = await this.orderOnlineService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            table: 0,
            createdAt: 0,
            updatedAt: 0,
            status: 0,
            createdBy: 0,
            updatedBy: 0
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        const items = await this.orderItemsService.getAll({ orderId: order.id });
        const result = Object.assign(Object.assign({}, order.toObject()), { items: items.length > 0 ? items : order.summaryItems, customer: {
                id: ((_a = order.customer) === null || _a === void 0 ? void 0 : _a.id) || null,
                firstName: (_b = order.customer) === null || _b === void 0 ? void 0 : _b.firstName,
                lastName: (_c = order.customer) === null || _c === void 0 ? void 0 : _c.lastName,
                tel: (_d = order.customer) === null || _d === void 0 ? void 0 : _d.tel
            } });
        delete result.summaryItems;
        return result;
    }
    async getOrderOnlineAll(customerId, query) {
        const order = await this.orderOnlineService.paginate(query.buildQuery(customerId), query, {
            table: 0,
            createdAt: 0,
            updatedAt: 0,
            status: 0,
            createdBy: 0,
            updatedBy: 0
        });
        const { docs } = order;
        const transformPayload = [];
        for (const doc of docs) {
            const kitchenQueue = await this.kitchenQueueService.findOne({
                orderId: doc.id
            });
            transformPayload.push(Object.assign(Object.assign({}, doc === null || doc === void 0 ? void 0 : doc.toObject()), { ticketNo: (kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) || null }));
        }
        return Object.assign(Object.assign({}, order), { docs: transformPayload });
    }
    async acceptOrder(orderId) {
        const order = await this.orderOnlineService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        if (order.orderStatus !== order_state_enum_1.OrderStateEnum.NEW) {
            throw new common_1.BadRequestException(`Order status isn't new order, now order status is ${order.orderStatus}`);
        }
        const logic = async (session) => {
            const updated = await this.orderOnlineService.transactionUpdate(orderId, {
                orderStatus: order_state_enum_1.OrderStateEnum.IN_PROGRESS
            }, session);
            if (!updated)
                throw new common_1.BadRequestException();
            const orderObj = updated.toObject();
            const notifyObject = {
                id: orderObj.id,
                customer: orderObj.customer,
                type: orderObj.orderMethod,
                placedDate: orderObj.orderDate,
                source: orderObj.orderType,
                orderStatus: orderObj.orderStatus
            };
            this.socketGateway.server
                .to(`web-${orderObj.customer.id}`)
                .emit('receiveOrderWeb', notifyObject);
            return await orderObj;
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return { success: true };
    }
    async rejectOrder(orderId) {
        const order = await this.orderOnlineService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        await this.orderOnlineService.update(orderId, {
            orderStatus: order_state_enum_1.OrderStateEnum.VOID
        });
        await this.kitchenQueueService.updateOne({ orderId }, {
            queueStatus: item_status_enum_1.ItemStatusEnum.CANCELED
        });
        await this.orderItemsService.updateMany({ orderId }, { itemStatus: item_status_enum_1.ItemStatusEnum.CANCELED });
        return { success: true };
    }
    async completeOrder(orderId) {
        const order = await this.orderOnlineService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        await this.orderOnlineService.update(orderId, {
            orderStatus: order_state_enum_1.OrderStateEnum.COMPLETED
        });
        await this.kitchenQueueService.updateOne({ orderId }, {
            queueStatus: item_status_enum_1.ItemStatusEnum.COMPLETED
        });
        await this.orderItemsService.updateMany({ orderId }, { itemStatus: item_status_enum_1.ItemStatusEnum.COMPLETED });
        return { success: true };
    }
    async getOrderOnlineByIdForPOS(orderId) {
        var _a, _b, _c, _d, _e, _f;
        const order = await this.orderOnlineService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            table: 0,
            createdAt: 0,
            updatedAt: 0,
            status: 0,
            createdBy: 0,
            updatedBy: 0
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        const items = await this.orderItemsService.getAll({ orderId: order.id });
        const kitchenQueue = await this.kitchenQueueService.findOne({ orderId });
        const result = Object.assign(Object.assign({}, order.toObject()), { items: items.length > 0 ? items : order.summaryItems, customer: {
                id: ((_a = order.customer) === null || _a === void 0 ? void 0 : _a.id) || null,
                firstName: (_b = order.customer) === null || _b === void 0 ? void 0 : _b.firstName,
                lastName: (_c = order.customer) === null || _c === void 0 ? void 0 : _c.lastName,
                tel: (_d = order.customer) === null || _d === void 0 ? void 0 : _d.tel,
                email: (_e = order.customer) === null || _e === void 0 ? void 0 : _e.email
            }, ticketNo: (_f = kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) !== null && _f !== void 0 ? _f : null });
        delete result.summaryItems;
        return result;
    }
    formatPhoneNumber(phoneNumberString) {
        const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
        const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            return '(' + match[1] + ') ' + match[2] + '-' + match[3];
        }
        return null;
    }
    currencyFormat(number) {
        return number.toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD'
        });
    }
    async sendOnlineOrderReceipt(order, ticketNo, transaction, timeZone, offsetTimeZone) {
        var _a;
        const webSetting = await this.webSettingService.findOne({
            restaurantId: (_a = order === null || order === void 0 ? void 0 : order.restaurant) === null || _a === void 0 ? void 0 : _a.id
        });
        const { logoUrl } = webSetting;
        const { restaurant: { dba, legalBusinessName, address: { address, city, state, zipCode }, businessPhone }, orderDate, orderType, id, summaryItems } = order;
        const itemsQuantity = summaryItems.reduce((prev, item) => item.unit + prev, 0);
        const itemList = summaryItems.map((item) => ({
            label: `${item.unit} x ${item.name}`,
            amount: this.currencyFormat(item.amount)
        }));
        const summaryKey = [
            { key: 'discount', label: 'Discount' },
            { key: 'subtotal', label: 'Subtotal' },
            { key: 'tax', label: 'Sales Tax' },
            { key: 'alcoholTax', label: 'Alcohol Tax' },
            { key: 'serviceCharge', label: 'Service Charge' }
        ];
        const summaryList = summaryKey.map((item) => ({
            label: item.label,
            amount: this.currencyFormat(order[item.key])
        }));
        const orderDateTimeZone = (0, dayjs_1.default)(orderDate).tz(timeZone);
        const transactionOpenDateTimeZone = (0, dayjs_1.default)(transaction.openDate).tz(timeZone);
        const utc = this.getUTC(offsetTimeZone);
        const mailBody = {
            logoUrl: logoUrl || process.env.TTHAI_LOGO_URL,
            dba: dba || legalBusinessName,
            total: this.currencyFormat(order.total + transaction.tips),
            orderDate: orderDateTimeZone.format('MMMM D, YYYY'),
            orderTime: `${orderDateTimeZone.format('hh:mmA')} ${utc}`,
            orderType: `Type: ${orderType}`,
            orderId: `Order#${id}`,
            transactionId: `TXN ${transaction.txn}`,
            itemsQuantity: `${itemsQuantity} ${itemsQuantity > 1 ? 'Items' : 'Item'}:`,
            ticketNo: `Ticket#${ticketNo}`,
            itemList,
            summaryList,
            tips: {
                label: 'Tips',
                amount: this.currencyFormat(transaction.tips)
            },
            addressDetail: [
                dba || legalBusinessName,
                address,
                `${city}, ${state} ${zipCode}`,
                this.formatPhoneNumber(businessPhone)
            ],
            paymentDetail: {
                paidBy: transaction.paidBy,
                cardType: transaction.cardType,
                cardLastFour: transaction.cardLastFour,
                date: `${transactionOpenDateTimeZone.format('M/DD/YY, h:mm A')} ${utc}`,
                authCode: `#${transaction === null || transaction === void 0 ? void 0 : transaction.response['authcode']}`
            }
        };
        const payloadHTML = this.mailerService.transformToHTML(mailBody);
        return await this.mailerService.postMail({
            to: order.customer.email,
            from: `${dba} <${process.env.EMAIL_USER}>`,
            subject: `"Your Food Order Ticket Number ${ticketNo} from ${dba}"`,
            text: payloadHTML,
            html: payloadHTML
        });
    }
    getUTC(offset) {
        const utc = (offset / 60) * -1;
        if (utc >= 0) {
            return `(UTC +${utc.toString().padStart(2, '0')}:00)`;
        }
        return `(UTC -${utc.toString().replace('-', '').padStart(2, '0')}:00)`;
    }
};
OrderOnlineLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [order_service_1.OrderService,
        order_online_service_1.OrderOnlineService,
        order_counter_service_1.OrderCounterService,
        restaurant_service_1.RestaurantService,
        staff_service_1.StaffService,
        kitchen_queue_service_1.KitchenQueueService,
        kitchen_counter_service_1.KitchenCounterService,
        order_items_service_1.OrderItemsService,
        customer_service_1.CustomerService,
        payment_gateway_service_1.PaymentGatewayService,
        socket_gateway_1.SocketGateway,
        card_pointe_logic_1.CardPointeLogic,
        transaction_logic_1.TransactionLogic,
        transaction_service_1.TransactionService,
        mailer_service_1.POSMailerService,
        web_setting_service_1.WebSettingService,
        pos_cash_drawer_logic_1.POSCashDrawerLogic,
        order_hour_logic_1.OrderHourLogic])
], OrderOnlineLogic);
exports.OrderOnlineLogic = OrderOnlineLogic;
//# sourceMappingURL=order-online.logic.js.map