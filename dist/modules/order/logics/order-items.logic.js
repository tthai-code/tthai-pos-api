"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderItemsLogic = void 0;
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const status_enum_1 = require("../../../common/enum/status.enum");
const item_status_enum_1 = require("../enum/item-status.enum");
const kitchen_queue_service_1 = require("../services/kitchen-queue.service");
const order_items_service_1 = require("../services/order-items.service");
const order_service_1 = require("../services/order.service");
let OrderItemsLogic = class OrderItemsLogic {
    constructor(orderService, orderItemsService, kitchenQueueService) {
        this.orderService = orderService;
        this.orderItemsService = orderItemsService;
        this.kitchenQueueService = kitchenQueueService;
    }
    async updateExistItems(orderItemId, payload) {
        const item = await this.orderItemsService.findOne({
            _id: orderItemId
        });
        if (!item)
            throw new common_1.NotFoundException('Not found order item.');
        const transform = (0, class_transformer_1.classToPlain)(payload);
        const updated = await this.orderItemsService.update(orderItemId, Object.assign({}, transform.items));
        const order = await this.orderService.update(item.orderId, transform);
        return { orderId: order.id, items: updated };
    }
    async removeItem(orderItemId) {
        const item = await this.orderItemsService.findOne({
            _id: orderItemId,
            itemStatus: { $ne: item_status_enum_1.ItemStatusEnum.CANCELED }
        });
        if (!item)
            throw new common_1.NotFoundException('Not found order item.');
        const updated = await this.orderItemsService.update(orderItemId, {
            itemStatus: item_status_enum_1.ItemStatusEnum.CANCELED
        });
        const summary = updated.amount;
        await this.orderService.updateOne({ _id: item.orderId }, {
            $inc: { subtotal: -summary, total: -summary }
        });
        return { orderId: item.orderId, items: updated };
    }
    async summaryOrder(orderId, payload) {
        var _a;
        const order = await this.orderService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        const items = await this.orderItemsService.getAll({ orderId });
        const results = [];
        for (const item of items) {
            const index = results.findIndex((result) => {
                if (result.menuId !== item.menuId)
                    return false;
                if (item.modifiers.length !== result.modifiers.length)
                    return false;
                const sortedResult = JSON.parse(JSON.stringify(result.modifiers)).sort((a, b) => a.id - b.id || a.name.localeCompare(b.name));
                const sortedItem = JSON.parse(JSON.stringify(item.modifiers)).sort((a, b) => a.id - b.id || a.name.localeCompare(b.name));
                for (let i = 0; i < sortedResult.length; i++) {
                    if (sortedResult[i].id !== sortedItem[i].id ||
                        sortedResult[i].name !== sortedItem[i].name) {
                        return false;
                    }
                }
                return true;
            });
            if (index === -1) {
                results.push(item);
            }
            else {
                results[index].unit += item.unit;
                results[index].amount += item.amount;
            }
        }
        const updated = await this.orderService.update(orderId, {
            subtotal: payload.subtotal,
            discount: payload.discount,
            serviceCharge: payload.serviceCharge,
            tax: payload.tax,
            alcoholTax: payload.alcoholTax,
            total: payload.total,
            summaryItems: results.map((item) => ({
                id: item.menuId,
                name: item.name,
                nativeName: item.nativeName,
                price: item.price,
                modifiers: item.modifiers,
                isContainAlcohol: item.isContainAlcohol,
                coverImage: item.coverImage,
                unit: item.unit,
                amount: item.amount,
                note: item.note,
                category: item.category
            }))
        });
        const result = updated.toObject();
        result.customer = Object.assign(Object.assign({}, result.customer), { id: ((_a = result.customer) === null || _a === void 0 ? void 0 : _a.id) || null });
        return result;
    }
    async summaryOrderForVoid(order) {
        const items = await this.orderItemsService.getAll({ orderId: order.id });
        const results = [];
        for (const item of items) {
            const index = results.findIndex((result) => {
                if (result.menuId !== item.menuId)
                    return false;
                if (item.modifiers.length !== result.modifiers.length)
                    return false;
                const sortedResult = JSON.parse(JSON.stringify(result.modifiers)).sort((a, b) => a.id - b.id || a.name.localeCompare(b.name));
                const sortedItem = JSON.parse(JSON.stringify(item.modifiers)).sort((a, b) => a.id - b.id || a.name.localeCompare(b.name));
                for (let i = 0; i < sortedResult.length; i++) {
                    if (sortedResult[i].id !== sortedItem[i].id ||
                        sortedResult[i].name !== sortedItem[i].name) {
                        return false;
                    }
                }
                return true;
            });
            if (index === -1) {
                results.push(item);
            }
            else {
                results[index].unit += item.unit;
                results[index].amount += item.amount;
            }
        }
        const summaryItems = results.map((item) => ({
            id: item.menuId,
            name: item.name,
            nativeName: item.nativeName,
            price: item.price,
            modifiers: item.modifiers,
            isContainAlcohol: item.isContainAlcohol,
            coverImage: item.coverImage,
            unit: item.unit,
            amount: item.amount,
            note: item.note,
            category: item.category
        }));
        return summaryItems;
    }
};
OrderItemsLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [order_service_1.OrderService,
        order_items_service_1.OrderItemsService,
        kitchen_queue_service_1.KitchenQueueService])
], OrderItemsLogic);
exports.OrderItemsLogic = OrderItemsLogic;
//# sourceMappingURL=order-items.logic.js.map