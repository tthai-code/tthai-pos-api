"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSOrderLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
const kitchen_queue_service_1 = require("../services/kitchen-queue.service");
const order_online_service_1 = require("../services/order-online.service");
const order_service_1 = require("../services/order.service");
const third_party_order_service_1 = require("../services/third-party-order.service");
let POSOrderLogic = class POSOrderLogic {
    constructor(orderService, onlineOrderService, thirdPartyOrderService, kitchenQueueService) {
        this.orderService = orderService;
        this.onlineOrderService = onlineOrderService;
        this.thirdPartyOrderService = thirdPartyOrderService;
        this.kitchenQueueService = kitchenQueueService;
    }
    async getDineInToGoOrder(id, ticketNo) {
        const order = await this.orderService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException(`Order #${id} not found from order type dine in and to go`);
        return Object.assign(Object.assign({}, order.toObject()), { ticketNo });
    }
    async getOnlineOrder(id, ticketNo) {
        const order = await this.onlineOrderService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException(`Order #${id} not found from order type online`);
        return Object.assign(Object.assign({}, order.toObject()), { ticketNo });
    }
    async getThirdPartyOrder(id, ticketNo) {
        const order = await this.thirdPartyOrderService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException(`Order #${id} not found from order type3rd party`);
        return Object.assign(Object.assign({}, order.toObject()), { ticketNo });
    }
    async getOrderInfo(id, orderType) {
        const kitchenQueue = await this.kitchenQueueService.findOne({
            orderId: id
        }, {}, { sort: { createdAt: -1 } });
        const ticketNo = (kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) || null;
        if (orderType === order_type_enum_1.OrderTypeEnum.DINE_IN ||
            orderType === order_type_enum_1.OrderTypeEnum.TO_GO) {
            return this.getDineInToGoOrder(id, ticketNo);
        }
        if (orderType === order_type_enum_1.OrderTypeEnum.ONLINE) {
            return this.getOnlineOrder(id, ticketNo);
        }
        if (orderType === order_type_enum_1.OrderTypeEnum.THIRD_PARTY) {
            return this.getThirdPartyOrder(id, ticketNo);
        }
        return null;
    }
};
POSOrderLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [order_service_1.OrderService,
        order_online_service_1.OrderOnlineService,
        third_party_order_service_1.ThirdPartyOrderService,
        kitchen_queue_service_1.KitchenQueueService])
], POSOrderLogic);
exports.POSOrderLogic = POSOrderLogic;
//# sourceMappingURL=pos-order.logic.js.map