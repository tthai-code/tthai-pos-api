/// <reference types="mongoose" />
import { SummaryOrderDto } from '../dto/summary-order.dto';
import { UpdateExistOrderItemsDto } from '../dto/update-exist-items.dto';
import { OrderDocument } from '../schemas/order.schema';
import { KitchenQueueService } from '../services/kitchen-queue.service';
import { OrderItemsService } from '../services/order-items.service';
import { OrderService } from '../services/order.service';
export declare class OrderItemsLogic {
    private readonly orderService;
    private readonly orderItemsService;
    private readonly kitchenQueueService;
    constructor(orderService: OrderService, orderItemsService: OrderItemsService, kitchenQueueService: KitchenQueueService);
    updateExistItems(orderItemId: string, payload: UpdateExistOrderItemsDto): Promise<{
        orderId: any;
        items: import("../schemas/order-items.schema").OrderItemsDocument;
    }>;
    removeItem(orderItemId: string): Promise<{
        orderId: string;
        items: import("../schemas/order-items.schema").OrderItemsDocument;
    }>;
    summaryOrder(orderId: string, payload: SummaryOrderDto): Promise<import("mongoose").LeanDocument<OrderDocument>>;
    summaryOrderForVoid(order: OrderDocument): Promise<{
        id: any;
        name: any;
        nativeName: any;
        price: any;
        modifiers: any;
        isContainAlcohol: any;
        coverImage: any;
        unit: any;
        amount: any;
        note: any;
        category: any;
    }[]>;
}
