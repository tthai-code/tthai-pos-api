import { OrderOnlinePaginateDto } from '../dto/get-order-online.dto';
import { KitchenQueueService } from '../services/kitchen-queue.service';
import { OrderOnlineService } from '../services/order-online.service';
import { OrderOnlineLogic } from './order-online.logic';
export declare class POSOnlineOrderLogic {
    private readonly orderOnlineLogic;
    private readonly orderOnlineService;
    private readonly kitchenQueueService;
    constructor(orderOnlineLogic: OrderOnlineLogic, orderOnlineService: OrderOnlineService, kitchenQueueService: KitchenQueueService);
    getAllOrder(restaurantId: string, query: OrderOnlinePaginateDto): Promise<any>;
}
