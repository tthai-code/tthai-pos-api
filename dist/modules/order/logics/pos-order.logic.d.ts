/// <reference types="mongoose" />
import { KitchenQueueService } from '../services/kitchen-queue.service';
import { OrderOnlineService } from '../services/order-online.service';
import { OrderService } from '../services/order.service';
import { ThirdPartyOrderService } from '../services/third-party-order.service';
export declare class POSOrderLogic {
    private readonly orderService;
    private readonly onlineOrderService;
    private readonly thirdPartyOrderService;
    private readonly kitchenQueueService;
    constructor(orderService: OrderService, onlineOrderService: OrderOnlineService, thirdPartyOrderService: ThirdPartyOrderService, kitchenQueueService: KitchenQueueService);
    getDineInToGoOrder(id: string, ticketNo: string): Promise<{
        ticketNo: string;
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order.schema").RestaurantFields;
        staff: import("../schemas/order.schema").StaffFields;
        customer: import("../schemas/order.schema").CustomerFields;
        total: number;
        convenienceFee: number;
        serviceCharge: number;
        table: import("../schemas/order.schema").TableFields;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/order.schema").ItemFields>[];
        orderStatus: string;
    }>;
    getOnlineOrder(id: string, ticketNo: string): Promise<{
        ticketNo: string;
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order-online.schema").RestaurantFields;
        staff: import("../schemas/order-online.schema").StaffFields;
        customer: import("../schemas/order-online.schema").CustomerFields;
        total: number;
        paymentStatus: string;
        convenienceFee: number;
        serviceCharge: number;
        table: import("../schemas/order-online.schema").TableFields;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderMethod: string;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/order-online.schema").ItemFields>[];
        orderStatus: string;
        tips: number;
    }>;
    getThirdPartyOrder(id: string, ticketNo: string): Promise<{
        ticketNo: string;
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        note: string;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order.schema").RestaurantFields;
        staff: import("../schemas/third-party-order.schema").StaffThirdPartyFields;
        customer: import("../schemas/order.schema").CustomerFields;
        total: number;
        convenienceFee: number;
        serviceCharge: number;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderMethod: string;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/third-party-order.schema").ItemThirdPartyFields>[];
        orderStatus: string;
        tips: number;
        deliverySource: string;
        completedDate: Date;
        asap: boolean;
        scheduledForDate: Date;
        orderKHId: number;
        prepareTime: number;
    }>;
    getOrderInfo(id: string, orderType: string): Promise<{
        ticketNo: string;
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order.schema").RestaurantFields;
        staff: import("../schemas/order.schema").StaffFields;
        customer: import("../schemas/order.schema").CustomerFields;
        total: number;
        convenienceFee: number;
        serviceCharge: number;
        table: import("../schemas/order.schema").TableFields;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/order.schema").ItemFields>[];
        orderStatus: string;
    } | {
        ticketNo: string;
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        note: string;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order.schema").RestaurantFields;
        staff: import("../schemas/third-party-order.schema").StaffThirdPartyFields;
        customer: import("../schemas/order.schema").CustomerFields;
        total: number;
        convenienceFee: number;
        serviceCharge: number;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderMethod: string;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/third-party-order.schema").ItemThirdPartyFields>[];
        orderStatus: string;
        tips: number;
        deliverySource: string;
        completedDate: Date;
        asap: boolean;
        scheduledForDate: Date;
        orderKHId: number;
        prepareTime: number;
    }>;
}
