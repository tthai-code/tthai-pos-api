import { RestaurantHolidaysService } from 'src/modules/restaurant-holiday/services/restaurant-holiday.service';
import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service';
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service';
import { RestaurantDocument } from 'src/modules/restaurant/schemas/restaurant.schema';
export declare class OrderHourLogic {
    private readonly restaurantPeriodService;
    private readonly restaurantDayService;
    private readonly restaurantHolidaysService;
    constructor(restaurantPeriodService: RestaurantPeriodService, restaurantDayService: RestaurantDayService, restaurantHolidaysService: RestaurantHolidaysService);
    checkAvailableOrder(restaurant: RestaurantDocument): Promise<boolean>;
}
