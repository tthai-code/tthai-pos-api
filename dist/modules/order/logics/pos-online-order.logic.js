"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSOnlineOrderLogic = void 0;
const common_1 = require("@nestjs/common");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
const kitchen_queue_service_1 = require("../services/kitchen-queue.service");
const order_online_service_1 = require("../services/order-online.service");
const order_online_logic_1 = require("./order-online.logic");
let POSOnlineOrderLogic = class POSOnlineOrderLogic {
    constructor(orderOnlineLogic, orderOnlineService, kitchenQueueService) {
        this.orderOnlineLogic = orderOnlineLogic;
        this.orderOnlineService = orderOnlineService;
        this.kitchenQueueService = kitchenQueueService;
    }
    async getAllOrder(restaurantId, query) {
        const allOrder = await this.orderOnlineService.getAll(query.buildQueryGetAll(restaurantId));
        const result = await this.orderOnlineService.paginate(query.buildQuery(restaurantId), query, { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 });
        const { docs } = result;
        const transformPayload = [];
        for (const doc of docs) {
            const kitchenQueue = await this.kitchenQueueService.findOne({
                orderId: doc.id
            });
            transformPayload.push(Object.assign(Object.assign({}, doc === null || doc === void 0 ? void 0 : doc.toObject()), { ticketNo: (kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) || null }));
        }
        return Object.assign(Object.assign({}, result), { docs: transformPayload, status: {
                active: allOrder.filter((item) => item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                    item.orderStatus !== order_state_enum_1.OrderStateEnum.VOID).length,
                new: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.NEW)
                    .length,
                inProgress: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS).length,
                ready: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.READY).length,
                upcoming: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.UPCOMING).length,
                completed: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED).length
            }, type: {
                delivery: allOrder.filter((item) => item.orderMethod === order_type_enum_1.OrderMethodEnum.DELIVERY &&
                    item.orderStatus !== order_state_enum_1.OrderStateEnum.VOID).length,
                pickup: allOrder.filter((item) => item.orderMethod === order_type_enum_1.OrderMethodEnum.PICKUP &&
                    item.orderStatus !== order_state_enum_1.OrderStateEnum.VOID).length
            } });
    }
};
POSOnlineOrderLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [order_online_logic_1.OrderOnlineLogic,
        order_online_service_1.OrderOnlineService,
        kitchen_queue_service_1.KitchenQueueService])
], POSOnlineOrderLogic);
exports.POSOnlineOrderLogic = POSOnlineOrderLogic;
//# sourceMappingURL=pos-online-order.logic.js.map