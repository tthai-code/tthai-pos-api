"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs_1 = require("../../../plugins/dayjs");
const status_enum_1 = require("../../../common/enum/status.enum");
const customer_service_1 = require("../../customer/services/customer.service");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const staff_service_1 = require("../../staff/services/staff.service");
const table_status_enum_1 = require("../../table/common/table-status.enum");
const table_service_1 = require("../../table/services/table.service");
const void_transaction_logic_1 = require("../../transaction/logics/void-transaction.logic");
const item_status_enum_1 = require("../enum/item-status.enum");
const order_state_enum_1 = require("../enum/order-state.enum");
const order_type_enum_1 = require("../enum/order-type.enum");
const kitchen_counter_service_1 = require("../services/kitchen-counter.service");
const kitchen_queue_service_1 = require("../services/kitchen-queue.service");
const order_counter_service_1 = require("../services/order-counter.service");
const order_items_service_1 = require("../services/order-items.service");
const order_service_1 = require("../services/order.service");
const order_items_logic_1 = require("./order-items.logic");
let OrderLogic = class OrderLogic {
    constructor(orderService, orderCounterService, restaurantService, staffService, tableService, kitchenQueueService, kitchenCounterService, orderItemsService, customerService, voidTransactionLogic, orderItemsLogic) {
        this.orderService = orderService;
        this.orderCounterService = orderCounterService;
        this.restaurantService = restaurantService;
        this.staffService = staffService;
        this.tableService = tableService;
        this.kitchenQueueService = kitchenQueueService;
        this.kitchenCounterService = kitchenCounterService;
        this.orderItemsService = orderItemsService;
        this.customerService = customerService;
        this.voidTransactionLogic = voidTransactionLogic;
        this.orderItemsLogic = orderItemsLogic;
    }
    randomOrderId() {
        const message = 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789';
        let id = '';
        for (let i = 0; i < 8; i++) {
            id += message.charAt(Math.floor(Math.random() * message.length));
        }
        return id;
    }
    async genOrderId() {
        let id = '';
        while (true) {
            id = this.randomOrderId();
            const isDuplicated = await this.orderService.findOne({
                _id: id
            });
            if (!isDuplicated) {
                break;
            }
        }
        return id;
    }
    async queueKitchen(restaurantId) {
        const { timeZone } = await this.restaurantService.findOne({
            _id: restaurantId
        });
        const now = (0, dayjs_1.default)().tz(timeZone);
        const nowUnix = now.valueOf();
        const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf();
        const endOfDay = now
            .add(1, 'day')
            .hour(3)
            .minute(59)
            .millisecond(999)
            .valueOf();
        let nowFormat = now.subtract(1, 'day').format('YYYYMMDD');
        if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
            nowFormat = now.format('YYYYMMDD');
        }
        const countQueue = await this.kitchenCounterService.getCounter(nowFormat, restaurantId);
        return `${countQueue}`;
    }
    async createOrderDineInPOS(staffId, createOrder) {
        const logic = async (session) => {
            var _a, _b, _c, _d;
            const [restaurant, staff, table] = await Promise.all([
                this.restaurantService.findOneForOrder(createOrder.restaurantId),
                this.staffService.findOneForOrder(staffId),
                this.tableService.findOneForOrder(createOrder.tableId)
            ]);
            if (!restaurant)
                throw new common_1.NotFoundException('Restaurant not found.');
            if (!staff)
                throw new common_1.NotFoundException('Staff not found.');
            if (!table)
                throw new common_1.BadRequestException('This table is unavailable.');
            let customer;
            const customerData = await this.customerService.findOne({
                _id: createOrder === null || createOrder === void 0 ? void 0 : createOrder.customerId,
                status: status_enum_1.StatusEnum.ACTIVE
            }, { firstName: 1, lastName: 1 });
            if (!customerData) {
                customer = {
                    id: null,
                    firstName: 'Guest',
                    lastName: ''
                };
            }
            else {
                customer = customerData.toObject();
            }
            createOrder._id = await this.genOrderId();
            createOrder.orderType = order_type_enum_1.OrderTypeEnum.DINE_IN;
            createOrder.orderDate = new Date();
            createOrder.restaurant = restaurant;
            createOrder.staff = staff;
            createOrder.table = table;
            createOrder.customer = customer;
            const rawResult = await this.orderService.transactionCreate(createOrder, session);
            const result = rawResult.toObject();
            const itemPayload = createOrder.items.map((item) => {
                const transform = Object.assign(Object.assign({}, item), { orderId: result.id });
                return transform;
            });
            const itemsData = await this.orderItemsService.transactionCreateMany(itemPayload, session);
            if (table && createOrder.orderType === order_type_enum_1.OrderTypeEnum.DINE_IN) {
                await this.tableService.transactionUpdate(createOrder.tableId, {
                    tableStatus: table_status_enum_1.TableStatusEnum.SEATED
                }, session);
            }
            const ticketNo = await this.queueKitchen(createOrder.restaurantId);
            const kitchenReceipt = await this.kitchenQueueService.transactionCreate({
                restaurantId: createOrder.restaurantId,
                orderId: result.id,
                ticketNo,
                items: itemsData.map((item) => item.id)
            }, session);
            return {
                id: result.id,
                restaurant: result.restaurant,
                customer: {
                    id: ((_a = result.customer) === null || _a === void 0 ? void 0 : _a.id) || null,
                    firstName: (_b = result.customer) === null || _b === void 0 ? void 0 : _b.firstName,
                    lastName: (_c = result.customer) === null || _c === void 0 ? void 0 : _c.lastName,
                    tel: (_d = result.customer) === null || _d === void 0 ? void 0 : _d.tel
                },
                table: result.table,
                staff: result.staff,
                orderType: result.orderType,
                numberOfGuest: result.numberOfGuest,
                orderDate: result.orderDate,
                ticketNo: kitchenReceipt.ticketNo,
                items: itemsData,
                subtotal: result.total,
                discount: result.discount,
                serviceCharge: result.serviceCharge,
                tax: result.tax,
                alcoholTax: result.alcoholTax,
                total: result.total,
                orderStatus: result.orderStatus
            };
        };
        return (0, mongoose_transaction_1.runTransaction)(logic);
    }
    async createOrderToGoPOS(staffId, createOrder) {
        const logic = async (session) => {
            var _a, _b, _c, _d;
            const [restaurant, staff] = await Promise.all([
                this.restaurantService.findOneForOrder(createOrder.restaurantId),
                this.staffService.findOneForOrder(staffId)
            ]);
            if (!restaurant)
                throw new common_1.NotFoundException('Restaurant not found.');
            if (!staff)
                throw new common_1.NotFoundException('Staff not found.');
            let customer;
            const customerData = await this.customerService.findOne({
                _id: createOrder === null || createOrder === void 0 ? void 0 : createOrder.customerId,
                status: status_enum_1.StatusEnum.ACTIVE
            }, { firstName: 1, lastName: 1 });
            if (!customerData) {
                customer = {
                    id: null,
                    firstName: 'Guest',
                    lastName: ''
                };
            }
            else {
                customer = customerData.toObject();
            }
            createOrder._id = await this.genOrderId();
            createOrder.orderType = order_type_enum_1.OrderTypeEnum.TO_GO;
            createOrder.orderDate = new Date();
            createOrder.restaurant = restaurant;
            createOrder.staff = staff;
            createOrder.customer = customer;
            const rawResult = await this.orderService.transactionCreate(createOrder, session);
            const result = rawResult.toObject();
            const itemPayload = createOrder.items.map((item) => {
                const transform = Object.assign(Object.assign({}, item), { orderId: result.id });
                return transform;
            });
            const itemsData = await this.orderItemsService.transactionCreateMany(itemPayload, session);
            const ticketNo = await this.queueKitchen(createOrder.restaurantId);
            const kitchenReceipt = await this.kitchenQueueService.transactionCreate({
                restaurantId: createOrder.restaurantId,
                orderId: result.id,
                ticketNo,
                items: itemsData.map((item) => item.id)
            }, session);
            return {
                id: result.id,
                restaurant: result.restaurant,
                customer: {
                    id: ((_a = result.customer) === null || _a === void 0 ? void 0 : _a.id) || null,
                    firstName: (_b = result.customer) === null || _b === void 0 ? void 0 : _b.firstName,
                    lastName: (_c = result.customer) === null || _c === void 0 ? void 0 : _c.lastName,
                    tel: (_d = result.customer) === null || _d === void 0 ? void 0 : _d.tel
                },
                staff: result.staff,
                orderType: result.orderType,
                numberOfGuest: result.numberOfGuest,
                orderDate: result.orderDate,
                pickUpDate: result.pickUpDate,
                ticketNo: kitchenReceipt.ticketNo,
                items: itemsData,
                subtotal: result.total,
                discount: result.discount,
                serviceCharge: result.serviceCharge,
                tax: result.tax,
                alcoholTax: result.alcoholTax,
                total: result.total,
                orderStatus: result.orderStatus
            };
        };
        return (0, mongoose_transaction_1.runTransaction)(logic);
    }
    async getOrderByTable(tableId) {
        var _a, _b, _c, _d;
        const table = await this.tableService.findOne({
            _id: tableId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!table)
            throw new common_1.NotFoundException('Not found table.');
        const result = await this.orderService.findOne({
            'table.id': tableId,
            status: status_enum_1.StatusEnum.ACTIVE,
            orderStatus: order_state_enum_1.OrderStateEnum.PENDING
        }, {
            summaryItems: 0,
            createdAt: 0,
            updatedAt: 0,
            pickUpDate: 0,
            status: 0,
            createdBy: 0,
            updatedBy: 0
        });
        if (!result)
            throw new common_1.NotFoundException('Not found order.');
        const items = await this.orderItemsService.getAll({ orderId: result.id });
        return Object.assign(Object.assign({}, result.toObject()), { items, customer: {
                id: ((_a = result.customer) === null || _a === void 0 ? void 0 : _a.id) || null,
                firstName: (_b = result.customer) === null || _b === void 0 ? void 0 : _b.firstName,
                lastName: (_c = result.customer) === null || _c === void 0 ? void 0 : _c.lastName,
                tel: (_d = result.customer) === null || _d === void 0 ? void 0 : _d.tel
            } });
    }
    async getOrderToGoById(orderId) {
        var _a, _b, _c, _d, _e;
        const order = await this.orderService.findOne({
            _id: orderId,
            orderType: order_type_enum_1.OrderTypeEnum.TO_GO,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            summaryItems: 0,
            table: 0,
            createdAt: 0,
            updatedAt: 0,
            status: 0,
            createdBy: 0,
            updatedBy: 0
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        const items = await this.orderItemsService.getAll({ orderId: order.id });
        const kitchenQueue = await this.kitchenQueueService.findOne({ orderId });
        return Object.assign(Object.assign({}, order.toObject()), { items, customer: {
                id: ((_a = order.customer) === null || _a === void 0 ? void 0 : _a.id) || null,
                firstName: (_b = order.customer) === null || _b === void 0 ? void 0 : _b.firstName,
                lastName: (_c = order.customer) === null || _c === void 0 ? void 0 : _c.lastName,
                tel: (_d = order.customer) === null || _d === void 0 ? void 0 : _d.tel
            }, ticketNo: (_e = kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) !== null && _e !== void 0 ? _e : null });
    }
    async voidOrderById(orderId) {
        const logic = async (session) => {
            const order = await this.orderService.findOne({
                _id: orderId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!order)
                throw new common_1.NotFoundException('Not found order.');
            if (order.orderStatus === order_state_enum_1.OrderStateEnum.VOID)
                throw new common_1.BadRequestException('This order is already void.');
            if (order.orderType === order_type_enum_1.OrderTypeEnum.DINE_IN) {
                await this.tableService.transactionUpdate(order.table.id, {
                    tableStatus: table_status_enum_1.TableStatusEnum.AVAILABLE
                }, session);
            }
            const summaryItems = await this.orderItemsLogic.summaryOrderForVoid(order);
            const updated = await this.orderService.transactionUpdate(orderId, {
                summaryItems,
                orderStatus: order_state_enum_1.OrderStateEnum.VOID
            }, session);
            await this.kitchenQueueService.transactionUpdateMany({ orderId }, { queueStatus: item_status_enum_1.ItemStatusEnum.CANCELED }, session);
            await this.orderItemsService.transactionUpdateMany({ orderId }, { itemStatus: item_status_enum_1.ItemStatusEnum.CANCELED }, session);
            await this.voidTransactionLogic.createVoidTransaction(updated, session);
            return updated;
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return this.orderService.findOne({ _id: orderId }, { orderStatus: 1 });
    }
    async moveOrderTable(orderId, payload) {
        const logic = async (session) => {
            const order = await this.orderService.findOne({
                _id: orderId,
                orderStatus: order_state_enum_1.OrderStateEnum.PENDING,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!order)
                throw new common_1.NotFoundException('Not found order.');
            const orderData = order.toObject();
            if (orderData.orderType !== order_type_enum_1.OrderTypeEnum.DINE_IN)
                throw new common_1.BadRequestException('This order type is not dine-in.');
            if (orderData.table.id === payload.tableId)
                return;
            const table = await this.tableService.findOne({
                _id: payload.tableId,
                status: status_enum_1.StatusEnum.ACTIVE
            }, { _id: 0, id: '$_id', tableNo: 1, tableStatus: 1 });
            if (!table)
                throw new common_1.NotFoundException('Not found table.');
            if (table.tableStatus !== table_status_enum_1.TableStatusEnum.AVAILABLE)
                throw new common_1.BadRequestException('This table is not available.');
            await this.orderService.transactionUpdate(orderId, {
                table
            }, session);
            await this.tableService.transactionUpdate(order.table.id, {
                tableStatus: table_status_enum_1.TableStatusEnum.AVAILABLE
            }, session);
            await this.tableService.transactionUpdate(payload.tableId, {
                tableStatus: table_status_enum_1.TableStatusEnum.SEATED
            }, session);
            return;
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return this.orderService.findOne({ _id: orderId }, { table: 1 });
    }
    async updateNewOrderItems(orderId, payload) {
        const logic = async (session) => {
            const order = await this.orderService.findOne({
                _id: orderId,
                orderStatus: order_state_enum_1.OrderStateEnum.PENDING,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!order)
                throw new common_1.NotFoundException('Not found order.');
            const newItems = payload.items.map((item) => {
                const transform = Object.assign(Object.assign({}, item), { orderId });
                return transform;
            });
            await this.orderService.transactionUpdate(orderId, {
                subtotal: payload.subtotal,
                discount: payload.discount,
                serviceCharge: payload.serviceCharge,
                tax: payload.tax,
                alcoholTax: payload.alcoholTax,
                total: payload.total
            }, session);
            const itemsData = await this.orderItemsService.transactionCreateMany(newItems, session);
            const ticketNo = await this.queueKitchen(order.restaurant.id);
            const kitchenReceipt = await this.kitchenQueueService.transactionCreate({
                restaurantId: order.restaurant.id,
                orderId,
                ticketNo,
                items: itemsData.map((item) => item.id)
            }, session);
            return { kitchenReceipt, itemsData };
        };
        const results = await (0, mongoose_transaction_1.runTransaction)(logic);
        const orderRaw = await this.orderService.findOne({ _id: orderId }, {
            restaurant: 1,
            customer: 1,
            table: 1,
            staff: 1,
            orderType: 1,
            pickUpDate: 1,
            numberOfGuest: 1,
            orderDate: 1,
            subtotal: 1,
            discount: 1,
            serviceCharge: 1,
            tax: 1,
            alcoholTax: 1,
            total: 1,
            orderStatus: 1
        });
        const order = orderRaw.toObject();
        const kitchen = results.kitchenReceipt.toObject();
        return Object.assign(Object.assign({}, order), { ticketNo: kitchen.ticketNo, items: results.itemsData });
    }
    async getPaginateOrderToGo(query, queryParam) {
        var _a;
        const orders = await this.orderService.paginate(query, queryParam);
        const orderIds = (_a = orders === null || orders === void 0 ? void 0 : orders.docs) === null || _a === void 0 ? void 0 : _a.map((item) => item._id);
        const items = await this.orderItemsService.getAll({
            orderId: { $in: orderIds }
        });
        const docs = [];
        for (const doc of orders === null || orders === void 0 ? void 0 : orders.docs) {
            const kitchenQueue = await this.kitchenQueueService.findOne({
                orderId: doc.id
            });
            docs.push(Object.assign(Object.assign({}, doc.toObject()), { items: items
                    .filter((e) => e.orderId === doc.id)
                    .map((item) => {
                    const result = item.toObject();
                    return result;
                }), ticketNo: (kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) || null }));
        }
        return Object.assign(Object.assign({}, orders), { docs });
    }
};
OrderLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [order_service_1.OrderService,
        order_counter_service_1.OrderCounterService,
        restaurant_service_1.RestaurantService,
        staff_service_1.StaffService,
        table_service_1.TableService,
        kitchen_queue_service_1.KitchenQueueService,
        kitchen_counter_service_1.KitchenCounterService,
        order_items_service_1.OrderItemsService,
        customer_service_1.CustomerService,
        void_transaction_logic_1.VoidTransactionLogic,
        order_items_logic_1.OrderItemsLogic])
], OrderLogic);
exports.OrderLogic = OrderLogic;
//# sourceMappingURL=order.logic.js.map