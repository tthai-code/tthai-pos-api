/// <reference types="mongoose" />
import { CustomerService } from 'src/modules/customer/services/customer.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { StaffService } from 'src/modules/staff/services/staff.service';
import { CreateOrderOnline } from '../dto/create-order-pos.dto';
import { KitchenCounterService } from '../services/kitchen-counter.service';
import { KitchenQueueService } from '../services/kitchen-queue.service';
import { OrderCounterService } from '../services/order-counter.service';
import { OrderItemsService } from '../services/order-items.service';
import { OrderService } from '../services/order.service';
import { OrderOnlineService } from '../services/order-online.service';
import { SocketGateway } from 'src/modules/socket/socket.gateway';
import { OrderOnlineCustomerPaginateDto } from '../dto/get-order-online.dto';
import { TransactionLogic } from 'src/modules/transaction/logics/transaction.logic';
import { CardPointeLogic } from 'src/modules/payment-gateway/logics/card-pointe.logic';
import { PayOnlineOrderDto } from '../dto/pay-online-order.dto';
import { POSMailerService } from 'src/modules/mailer/services/mailer.service';
import { OrderOnlineDocument } from '../schemas/order-online.schema';
import { TransactionDocument } from 'src/modules/transaction/schemas/transaction.schema';
import { WebSettingService } from 'src/modules/web-setting/services/web-setting.service';
import { TransactionService } from 'src/modules/transaction/services/transaction.service';
import { POSCashDrawerLogic } from 'src/modules/cash-drawer/logic/pos-cash-drawer.logic';
import { OrderHourLogic } from './order-hour.logic';
import { PaymentGatewayService } from 'src/modules/payment-gateway/services/payment-gateway.service';
export declare class OrderOnlineLogic {
    private readonly orderService;
    private readonly orderOnlineService;
    private readonly orderCounterService;
    private readonly restaurantService;
    private readonly staffService;
    private readonly kitchenQueueService;
    private readonly kitchenCounterService;
    private readonly orderItemsService;
    private readonly customerService;
    private readonly paymentGatewayService;
    private readonly socketGateway;
    private readonly cardPointeLogic;
    private readonly transactionLogic;
    private readonly transactionService;
    private readonly mailerService;
    private readonly webSettingService;
    private readonly posCashDrawerLogic;
    private readonly orderHourLogic;
    constructor(orderService: OrderService, orderOnlineService: OrderOnlineService, orderCounterService: OrderCounterService, restaurantService: RestaurantService, staffService: StaffService, kitchenQueueService: KitchenQueueService, kitchenCounterService: KitchenCounterService, orderItemsService: OrderItemsService, customerService: CustomerService, paymentGatewayService: PaymentGatewayService, socketGateway: SocketGateway, cardPointeLogic: CardPointeLogic, transactionLogic: TransactionLogic, transactionService: TransactionService, mailerService: POSMailerService, webSettingService: WebSettingService, posCashDrawerLogic: POSCashDrawerLogic, orderHourLogic: OrderHourLogic);
    private randomOrderId;
    private genOrderId;
    private queueKitchen;
    createOrderOnline(customerId: string, createOrder: CreateOrderOnline): Promise<{
        id: any;
        orderStatus: string;
        paymentStatus: string;
    }>;
    updatePaidOrderOnlineAndCreateOrderById(orderId: string, payload: PayOnlineOrderDto): Promise<OrderOnlineDocument>;
    getOrderOnlineById(orderId: string): Promise<{
        items: any[];
        customer: {
            id: string;
            firstName: string;
            lastName: string;
            tel: string;
        };
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order-online.schema").RestaurantFields;
        staff: import("../schemas/order-online.schema").StaffFields;
        total: number;
        paymentStatus: string;
        convenienceFee: number;
        serviceCharge: number;
        table: import("../schemas/order-online.schema").TableFields;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderMethod: string;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/order-online.schema").ItemFields>[];
        orderStatus: string;
        tips: number;
    }>;
    getOrderOnlineAll(customerId: string, query: OrderOnlineCustomerPaginateDto): Promise<any>;
    acceptOrder(orderId: string): Promise<{
        success: boolean;
    }>;
    rejectOrder(orderId: string): Promise<{
        success: boolean;
    }>;
    completeOrder(orderId: string): Promise<{
        success: boolean;
    }>;
    getOrderOnlineByIdForPOS(orderId: string): Promise<{
        items: any[];
        customer: {
            id: string;
            firstName: string;
            lastName: string;
            tel: string;
            email: string;
        };
        ticketNo: string;
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../schemas/order-online.schema").RestaurantFields;
        staff: import("../schemas/order-online.schema").StaffFields;
        total: number;
        paymentStatus: string;
        convenienceFee: number;
        serviceCharge: number;
        table: import("../schemas/order-online.schema").TableFields;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderMethod: string;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../schemas/order-online.schema").ItemFields>[];
        orderStatus: string;
        tips: number;
    }>;
    private formatPhoneNumber;
    private currencyFormat;
    sendOnlineOrderReceipt(order: OrderOnlineDocument, ticketNo: string, transaction: TransactionDocument, timeZone: string, offsetTimeZone: number): Promise<SentMessageInfo>;
    private getUTC;
}
