import { PaginateKitchenQueue } from '../dto/get-kitchen.dto';
import { KitchenQueueService } from '../services/kitchen-queue.service';
import { OrderItemsService } from '../services/order-items.service';
export declare class KitchenQueueLogic {
    private readonly kitchenQueueService;
    private readonly orderItemsService;
    constructor(kitchenQueueService: KitchenQueueService, orderItemsService: OrderItemsService);
    private queueStatusCondition;
    getPaginateKitchenQueue(query: any, queryParam: PaginateKitchenQueue): Promise<any>;
}
