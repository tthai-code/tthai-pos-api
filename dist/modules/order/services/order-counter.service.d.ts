import { Model } from 'mongoose';
import { OrderCounterDocument } from '../schemas/order-counter.schema';
export declare class OrderCounterService {
    private readonly orderCounterModel;
    constructor(orderCounterModel: Model<OrderCounterDocument>);
    getCounter(prefix: string, restaurantId: string): Promise<number>;
}
