"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderOnlineService = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
let OrderOnlineService = class OrderOnlineService {
    constructor(OrderOnlineModel, request) {
        this.OrderOnlineModel = OrderOnlineModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.OrderOnlineModel.findById({ _id: id });
    }
    async transactionCreate(payload, session) {
        const document = new this.OrderOnlineModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save({ session });
    }
    async transactionUpdate(id, product, session) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save({ session });
    }
    async update(id, product) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save();
    }
    getAll(condition, project) {
        return this.OrderOnlineModel.find(condition, project);
    }
    findOne(condition, project) {
        return this.OrderOnlineModel.findOne(condition, project);
    }
    paginate(query, queryParam, select) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder },
            select
        };
        return this.OrderOnlineModel.paginate(query, options);
    }
};
OrderOnlineService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('orderOnline')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], OrderOnlineService);
exports.OrderOnlineService = OrderOnlineService;
//# sourceMappingURL=order-online.service.js.map