import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { OrderItemsDocument } from '../schemas/order-items.schema';
export declare class OrderItemsService {
    private readonly OrderItemsModel;
    private request;
    constructor(OrderItemsModel: PaginateModel<OrderItemsDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<OrderItemsDocument | any>;
    resolveByCondition(condition: any): Promise<OrderItemsDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<OrderItemsDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<OrderItemsDocument>;
    transactionCreateMany(payload: any, session: ClientSession): Promise<Array<OrderItemsDocument> | any>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<OrderItemsDocument>;
    transactionUpdateCondition(condition: any, product: any, session: ClientSession): Promise<OrderItemsDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<OrderItemsDocument>;
    create(payload: any): Promise<OrderItemsDocument>;
    createMany(payload: any): Promise<OrderItemsDocument>;
    update(id: string, payload: any): Promise<OrderItemsDocument>;
    updateMany(condition: any, payload: any): Promise<OrderItemsDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<OrderItemsDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<OrderItemsDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<OrderItemsDocument>;
    paginate(query: any, queryParam: any, select?: any): Promise<PaginateResult<OrderItemsDocument>>;
}
