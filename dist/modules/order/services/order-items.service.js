"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderItemsService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const mongoose_1 = require("@nestjs/mongoose");
let OrderItemsService = class OrderItemsService {
    constructor(OrderItemsModel, request) {
        this.OrderItemsModel = OrderItemsModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.OrderItemsModel.findById({ _id: id });
    }
    async resolveByCondition(condition) {
        return this.OrderItemsModel.findOne(condition);
    }
    async isExists(condition) {
        const result = await this.OrderItemsModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.OrderItemsModel;
    }
    async aggregate(pipeline) {
        return this.OrderItemsModel.aggregate(pipeline);
    }
    async getSession() {
        await this.OrderItemsModel.createCollection();
        return this.OrderItemsModel.startSession();
    }
    async transactionCreate(payload, session) {
        const document = new this.OrderItemsModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save({ session });
    }
    async transactionCreateMany(payload, session) {
        return this.OrderItemsModel.insertMany(payload, {
            session,
            ordered: true,
            lean: true
        });
    }
    async transactionUpdate(id, product, session) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save({ session });
    }
    async transactionUpdateCondition(condition, product, session) {
        const document = await this.resolveByCondition(condition);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save({ session });
    }
    async transactionUpdateMany(condition, payload, session) {
        return this.OrderItemsModel.updateMany(condition, { $set: payload }, { session, new: true });
    }
    async create(payload) {
        const document = new this.OrderItemsModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async createMany(payload) {
        return this.OrderItemsModel.insertMany(payload);
    }
    async update(id, payload) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, payload)).save();
    }
    async updateMany(condition, payload) {
        return this.OrderItemsModel.updateMany(condition, { $set: payload });
    }
    async findOneAndUpdate(condition, payload) {
        const document = await this.resolveByCondition(condition);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, payload)).save();
    }
    getAll(condition, project) {
        return this.OrderItemsModel.find(condition, project);
    }
    findById(id) {
        return this.OrderItemsModel.findById(id);
    }
    findOne(condition) {
        return this.OrderItemsModel.findOne(condition);
    }
    findOneWithSelect(condition, options) {
        return this.OrderItemsModel.findOne(condition, options);
    }
    paginate(query, queryParam, select) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder },
            select
        };
        return this.OrderItemsModel.paginate(query, options);
    }
};
OrderItemsService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('orderItems')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], OrderItemsService);
exports.OrderItemsService = OrderItemsService;
//# sourceMappingURL=order-items.service.js.map