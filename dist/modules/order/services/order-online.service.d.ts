import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { OrderOnlineDocument } from '../schemas/order-online.schema';
import { OrderOnlineCustomerPaginateDto, OrderOnlinePaginateDto } from '../dto/get-order-online.dto';
export declare class OrderOnlineService {
    private readonly OrderOnlineModel;
    private request;
    constructor(OrderOnlineModel: PaginateModel<OrderOnlineDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<OrderOnlineDocument | any>;
    transactionCreate(payload: any, session: ClientSession): Promise<OrderOnlineDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<OrderOnlineDocument>;
    update(id: string, product: any): Promise<OrderOnlineDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findOne(condition: any, project?: any): Promise<OrderOnlineDocument>;
    paginate(query: any, queryParam: OrderOnlinePaginateDto | OrderOnlineCustomerPaginateDto, select?: any): Promise<PaginateResult<OrderOnlineDocument>>;
}
