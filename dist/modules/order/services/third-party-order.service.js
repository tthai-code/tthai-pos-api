"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThirdPartyOrderService = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const status_enum_1 = require("../../../common/enum/status.enum");
let ThirdPartyOrderService = class ThirdPartyOrderService {
    constructor(OrderModel, request) {
        this.OrderModel = OrderModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.OrderModel.findById({ _id: id });
    }
    getRequest() {
        return this.request;
    }
    count(query) {
        return this.OrderModel.countDocuments(query);
    }
    async isExists(condition) {
        const result = await this.OrderModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.OrderModel;
    }
    async getSession() {
        await this.OrderModel.createCollection();
        return await this.OrderModel.startSession();
    }
    async transactionCreate(payload, session) {
        const document = new this.OrderModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save({ session });
    }
    async aggregate(pipeline) {
        return this.OrderModel.aggregate(pipeline);
    }
    async create(payload) {
        const document = new this.OrderModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async update(id, product) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save();
    }
    async updateOne(condition, payload) {
        return this.OrderModel.updateOne(condition, Object.assign({}, payload));
    }
    async transactionUpdate(id, product, session) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save({ session });
    }
    async delete(id) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set({ status: status_enum_1.StatusEnum.DELETED }).save();
    }
    getAll(condition, project) {
        return this.OrderModel.find(condition, project);
    }
    findById(id) {
        return this.OrderModel.findById(id);
    }
    findOne(condition, project) {
        return this.OrderModel.findOne(condition, project);
    }
    paginate(query, queryParam, select) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder },
            select
        };
        return this.OrderModel.paginate(query, options);
    }
};
ThirdPartyOrderService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('thirdPartyOrder')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], ThirdPartyOrderService);
exports.ThirdPartyOrderService = ThirdPartyOrderService;
//# sourceMappingURL=third-party-order.service.js.map