import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { KitchenQueueDocument } from '../schemas/kitchen-queue.schema';
import { PaginateKitchenQueue } from '../dto/get-kitchen.dto';
export declare class KitchenQueueService {
    private readonly KitchenQueueModel;
    private request;
    constructor(KitchenQueueModel: PaginateModel<KitchenQueueDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<KitchenQueueDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<KitchenQueueDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<KitchenQueueDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<KitchenQueueDocument>;
    update(id: string, product: any): Promise<KitchenQueueDocument>;
    updateOne(condition: any, payload: any): Promise<KitchenQueueDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<KitchenQueueDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<KitchenQueueDocument>;
    delete(id: string): Promise<KitchenQueueDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any, options?: any): Promise<KitchenQueueDocument>;
    paginate(query: any, queryParam: PaginateKitchenQueue, select?: any): Promise<PaginateResult<KitchenQueueDocument>>;
}
