import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { ThirdPartyOrderDocument } from '../schemas/third-party-order.schema';
import { ThirdPartyOrderPaginateDto } from '../dto/get-third-party-order.dto';
export declare class ThirdPartyOrderService {
    private readonly OrderModel;
    private request;
    constructor(OrderModel: PaginateModel<ThirdPartyOrderDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<ThirdPartyOrderDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<ThirdPartyOrderDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<ThirdPartyOrderDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<ThirdPartyOrderDocument>;
    update(id: string, product: any): Promise<ThirdPartyOrderDocument>;
    updateOne(condition: any, payload: any): Promise<ThirdPartyOrderDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<ThirdPartyOrderDocument>;
    delete(id: string): Promise<ThirdPartyOrderDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<ThirdPartyOrderDocument>;
    paginate(query: any, queryParam: ThirdPartyOrderPaginateDto, select?: any): Promise<PaginateResult<ThirdPartyOrderDocument>>;
}
