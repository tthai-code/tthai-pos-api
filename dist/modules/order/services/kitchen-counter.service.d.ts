import { Model } from 'mongoose';
import { KitchenCounterDocument } from '../schemas/kitchen-counter.shcema';
export declare class KitchenCounterService {
    private readonly kitchenCounterModel;
    constructor(kitchenCounterModel: Model<KitchenCounterDocument>);
    getCounter(prefix: string, restaurantId: string): Promise<number>;
}
