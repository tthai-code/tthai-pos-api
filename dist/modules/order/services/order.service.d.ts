import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { OrderDocument } from '../schemas/order.schema';
import { ToGoOrderPaginateDto } from '../dto/get-order.dto';
export declare class OrderService {
    private readonly OrderModel;
    private request;
    constructor(OrderModel: PaginateModel<OrderDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<OrderDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<OrderDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<OrderDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<OrderDocument>;
    update(id: string, product: any): Promise<OrderDocument>;
    updateOne(condition: any, payload: any): Promise<OrderDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<OrderDocument>;
    delete(id: string): Promise<OrderDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<OrderDocument>;
    paginate(query: any, queryParam: ToGoOrderPaginateDto): Promise<PaginateResult<OrderDocument>>;
}
