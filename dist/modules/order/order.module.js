"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const order_controller_1 = require("./controllers/order.controller");
const order_public_online_controller_1 = require("./controllers/order-public-online.controller");
const order_logic_1 = require("./logics/order.logic");
const order_online_logic_1 = require("./logics/order-online.logic");
const order_counter_schema_1 = require("./schemas/order-counter.schema");
const order_schema_1 = require("./schemas/order.schema");
const order_online_schema_1 = require("./schemas/order-online.schema");
const order_counter_service_1 = require("./services/order-counter.service");
const order_service_1 = require("./services/order.service");
const order_online_service_1 = require("./services/order-online.service");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const staff_module_1 = require("../staff/staff.module");
const table_module_1 = require("../table/table.module");
const kitchen_queue_service_1 = require("./services/kitchen-queue.service");
const pos_order_controller_1 = require("./controllers/pos-order.controller");
const kitchen_queue_schema_1 = require("./schemas/kitchen-queue.schema");
const kitchen_counter_service_1 = require("./services/kitchen-counter.service");
const kitchen_counter_shcema_1 = require("./schemas/kitchen-counter.shcema");
const customer_module_1 = require("../customer/customer.module");
const order_items_service_1 = require("./services/order-items.service");
const order_items_schema_1 = require("./schemas/order-items.schema");
const order_items_logic_1 = require("./logics/order-items.logic");
const kitchen_queue_controller_1 = require("./controllers/kitchen-queue.controller");
const kitchen_queue_logic_1 = require("./logics/kitchen-queue.logic");
const third_party_order_service_1 = require("./services/third-party-order.service");
const third_party_order_schema_1 = require("./schemas/third-party-order.schema");
const socket_module_1 = require("../socket/socket.module");
const pos_online_order_controller_1 = require("./controllers/pos-online-order.controller");
const payment_gateway_module_1 = require("../payment-gateway/payment-gateway.module");
const transaction_module_1 = require("../transaction/transaction.module");
const pos_online_order_logic_1 = require("./logics/pos-online-order.logic");
const web_setting_module_1 = require("../web-setting/web-setting.module");
const cash_drawer_module_1 = require("../cash-drawer/cash-drawer.module");
const order_hour_logic_1 = require("./logics/order-hour.logic");
const public_order_hour_controller_1 = require("./controllers/public-order-hour.controller");
const pos_order_logic_1 = require("./logics/pos-order.logic");
let OrderModule = class OrderModule {
};
OrderModule = __decorate([
    (0, common_1.Module)({
        imports: [
            customer_module_1.CustomerModule,
            staff_module_1.StaffModule,
            table_module_1.TableModule,
            restaurant_module_1.RestaurantModule,
            payment_gateway_module_1.PaymentGatewayModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'order', schema: order_schema_1.OrderSchema },
                { name: 'orderOnline', schema: order_online_schema_1.OrderOnlineSchema },
                { name: 'order-counter', schema: order_counter_schema_1.OrderCounterSchema },
                { name: 'kitchenQueue', schema: kitchen_queue_schema_1.KitchenQueueSchema },
                { name: 'kitchen-counter', schema: kitchen_counter_shcema_1.KitchenCounterSchema },
                { name: 'orderItems', schema: order_items_schema_1.OrderItemsSchema },
                { name: 'thirdPartyOrder', schema: third_party_order_schema_1.ThirdPartyOrderSchema }
            ]),
            (0, common_1.forwardRef)(() => socket_module_1.SocketModule),
            (0, common_1.forwardRef)(() => payment_gateway_module_1.PaymentGatewayModule),
            (0, common_1.forwardRef)(() => transaction_module_1.TransactionModule),
            (0, common_1.forwardRef)(() => web_setting_module_1.WebSettingModule),
            (0, common_1.forwardRef)(() => cash_drawer_module_1.CashDrawerModule)
        ],
        providers: [
            order_service_1.OrderService,
            order_online_service_1.OrderOnlineService,
            order_logic_1.OrderLogic,
            order_online_logic_1.OrderOnlineLogic,
            order_counter_service_1.OrderCounterService,
            order_items_service_1.OrderItemsService,
            kitchen_queue_service_1.KitchenQueueService,
            kitchen_counter_service_1.KitchenCounterService,
            kitchen_queue_logic_1.KitchenQueueLogic,
            order_items_logic_1.OrderItemsLogic,
            third_party_order_service_1.ThirdPartyOrderService,
            pos_online_order_logic_1.POSOnlineOrderLogic,
            order_hour_logic_1.OrderHourLogic,
            pos_order_logic_1.POSOrderLogic
        ],
        controllers: [
            order_controller_1.OrderController,
            order_public_online_controller_1.OrderOnlineController,
            pos_order_controller_1.POSOrderController,
            kitchen_queue_controller_1.KitchenQueueController,
            pos_online_order_controller_1.POSOnlineOrderController,
            public_order_hour_controller_1.PublicOrderHourController
        ],
        exports: [
            order_service_1.OrderService,
            third_party_order_service_1.ThirdPartyOrderService,
            kitchen_queue_service_1.KitchenQueueService,
            kitchen_counter_service_1.KitchenCounterService,
            order_items_service_1.OrderItemsService,
            order_online_service_1.OrderOnlineService,
            order_items_logic_1.OrderItemsLogic
        ]
    })
], OrderModule);
exports.OrderModule = OrderModule;
//# sourceMappingURL=order.module.js.map