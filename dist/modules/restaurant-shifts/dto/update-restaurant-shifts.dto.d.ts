declare class ShiftFieldsDto {
    startAt: string;
    endAt: string;
    readonly status: string;
}
export declare class UpdateShiftsDto {
    readonly shifts: Array<ShiftFieldsDto>;
}
export {};
