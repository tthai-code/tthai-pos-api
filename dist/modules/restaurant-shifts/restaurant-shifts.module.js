"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantShiftsModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const pos_restaurant_shifts_controller_1 = require("./controllers/pos-restaurant-shifts.controller");
const restaurant_shifts_controller_1 = require("./controllers/restaurant-shifts.controller");
const restaurant_shifts_schema_1 = require("./schemas/restaurant-shifts.schema");
const restaurant_shifts_service_1 = require("./services/restaurant-shifts.service");
let RestaurantShiftsModule = class RestaurantShiftsModule {
};
RestaurantShiftsModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'shifts', schema: restaurant_shifts_schema_1.RestaurantShiftsSchema }
            ])
        ],
        providers: [restaurant_shifts_service_1.RestaurantShiftsService],
        controllers: [restaurant_shifts_controller_1.RestaurantShiftsController, pos_restaurant_shifts_controller_1.POSRestaurantShiftsController],
        exports: [restaurant_shifts_service_1.RestaurantShiftsService]
    })
], RestaurantShiftsModule);
exports.RestaurantShiftsModule = RestaurantShiftsModule;
//# sourceMappingURL=restaurant-shifts.module.js.map