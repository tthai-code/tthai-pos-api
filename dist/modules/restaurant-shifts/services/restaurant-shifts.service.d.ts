import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { RestaurantShiftsDocument } from '../schemas/restaurant-shifts.schema';
export declare class RestaurantShiftsService {
    private readonly RestaurantShiftsModel;
    private request;
    constructor(RestaurantShiftsModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<RestaurantShiftsDocument | any>;
    resolveByCondition(condition: any): Promise<RestaurantShiftsDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<RestaurantShiftsDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<RestaurantShiftsDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<RestaurantShiftsDocument>;
    create(payload: any): Promise<RestaurantShiftsDocument>;
    update(id: string, payload: any): Promise<RestaurantShiftsDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<RestaurantShiftsDocument>;
    delete(id: string): Promise<RestaurantShiftsDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<RestaurantShiftsDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<RestaurantShiftsDocument>;
}
