export declare const getShiftsResponse: {
    schema: {
        type: string;
        example: {
            message: string;
            data: {
                restaurant_id: string;
                shifts: {
                    start_at: string;
                    end_at: string;
                    status: string;
                }[];
                updated_by: {
                    username: string;
                    id: string;
                };
                updated_at: string;
                id: string;
            };
        };
    };
};
