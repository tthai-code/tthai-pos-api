import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { RestaurantShiftsService } from '../services/restaurant-shifts.service';
export declare class POSRestaurantShiftsController {
    private readonly restaurantShiftsService;
    private readonly restaurantService;
    constructor(restaurantShiftsService: RestaurantShiftsService, restaurantService: RestaurantService);
    getShiftsByRestaurantID(id: string): Promise<import("../schemas/restaurant-shifts.schema").RestaurantShiftsDocument>;
}
