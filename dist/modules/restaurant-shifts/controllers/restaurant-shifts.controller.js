"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantShiftsController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const update_restaurant_shifts_dto_1 = require("../dto/update-restaurant-shifts.dto");
const restaurant_shifts_reponse_1 = require("../response/restaurant-shifts.reponse");
const restaurant_shifts_service_1 = require("../services/restaurant-shifts.service");
let RestaurantShiftsController = class RestaurantShiftsController {
    constructor(restaurantShiftsService, restaurantService) {
        this.restaurantShiftsService = restaurantShiftsService;
        this.restaurantService = restaurantService;
    }
    async getShiftsByRestaurantID(id) {
        const restaurantId = await this.restaurantService.findOne({ _id: id });
        if (!restaurantId)
            throw new common_1.NotFoundException('Not Found Restaurant.');
        const restaurantShifts = await this.restaurantShiftsService.findOne({
            restaurantId: id
        });
        if (restaurantShifts) {
            return restaurantShifts;
        }
        return {
            restaurant_id: id,
            shifts: []
        };
    }
    async updateShiftsByRestaurantID(id, payload) {
        const restaurantId = await this.restaurantService.findOne({ _id: id });
        if (!restaurantId)
            throw new common_1.NotFoundException('Not Found Restaurant.');
        const isShiftExisted = await this.restaurantShiftsService.findOne({
            restaurantId: id
        });
        if (isShiftExisted) {
            return await this.restaurantShiftsService.findOneAndUpdate({ restaurantId: id }, payload);
        }
        else {
            return await this.restaurantShiftsService.create({
                restaurantId: id,
                shifts: payload.shifts
            });
        }
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId/shift'),
    (0, swagger_1.ApiOkResponse)(restaurant_shifts_reponse_1.getShiftsResponse),
    (0, swagger_1.ApiOperation)({ summary: 'Get Restaurant Shifts by Restaurant ID' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RestaurantShiftsController.prototype, "getShiftsByRestaurantID", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/shift'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Restaurant Shifts by Restaurant ID' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_restaurant_shifts_dto_1.UpdateShiftsDto]),
    __metadata("design:returntype", Promise)
], RestaurantShiftsController.prototype, "updateShiftsByRestaurantID", null);
RestaurantShiftsController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('restaurant-shifts'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/restaurant'),
    __metadata("design:paramtypes", [restaurant_shifts_service_1.RestaurantShiftsService,
        restaurant_service_1.RestaurantService])
], RestaurantShiftsController);
exports.RestaurantShiftsController = RestaurantShiftsController;
//# sourceMappingURL=restaurant-shifts.controller.js.map