import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdateShiftsDto } from '../dto/update-restaurant-shifts.dto';
import { RestaurantShiftsService } from '../services/restaurant-shifts.service';
export declare class RestaurantShiftsController {
    private readonly restaurantShiftsService;
    private readonly restaurantService;
    constructor(restaurantShiftsService: RestaurantShiftsService, restaurantService: RestaurantService);
    getShiftsByRestaurantID(id: string): Promise<import("../schemas/restaurant-shifts.schema").RestaurantShiftsDocument | {
        restaurant_id: string;
        shifts: any[];
    }>;
    updateShiftsByRestaurantID(id: string, payload: UpdateShiftsDto): Promise<import("../schemas/restaurant-shifts.schema").RestaurantShiftsDocument>;
}
