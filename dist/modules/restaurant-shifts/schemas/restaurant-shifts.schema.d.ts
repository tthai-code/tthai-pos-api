import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type RestaurantShiftsDocument = RestaurantShiftsField & Document;
declare class ShiftPeriodFieldsSchema {
    startAt: string;
    endAt: string;
    status: string;
}
export declare class RestaurantShiftsField {
    restaurantId: Types.ObjectId;
    shifts: Array<ShiftPeriodFieldsSchema>;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const RestaurantShiftsSchema: MongooseSchema<Document<RestaurantShiftsField, any, any>, import("mongoose").Model<Document<RestaurantShiftsField, any, any>, any, any, any>, any, any>;
export {};
