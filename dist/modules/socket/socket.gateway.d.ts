import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
export declare class SocketGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    server: Server;
    afterInit(): void;
    handleDisconnect(client: Socket): void;
    handleConnection(client: Socket): void;
    handleJoinRoomPOS(client: Socket, restaurantId: string): void;
    handleLeaveRoomPOS(client: Socket, restaurantId: string): void;
    handleJoinRoomWeb(client: Socket, customerId: string): void;
    handleLeaveRoomWeb(client: Socket, customerId: string): void;
}
