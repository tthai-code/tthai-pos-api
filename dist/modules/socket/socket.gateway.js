"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const socket_io_1 = require("socket.io");
let SocketGateway = class SocketGateway {
    afterInit() {
        console.log('Websocket Server Started, Listening on Port:3001');
    }
    handleDisconnect(client) {
        console.log({
            message: `Client disconnected: ${client.id}`,
            timestamps: new Date()
        });
    }
    handleConnection(client) {
        client.emit('connected', {
            message: `Client connected: ${client.id}`,
            timestamps: new Date()
        });
        console.log({
            message: `Client connected: ${client.id}`,
            timestamps: new Date()
        });
    }
    handleJoinRoomPOS(client, restaurantId) {
        client.join(`pos-${restaurantId}`);
        client.emit('joinedRoomPOS', {
            message: `Client joined room: pos-${restaurantId}`,
            timestamps: new Date()
        });
    }
    handleLeaveRoomPOS(client, restaurantId) {
        client.leave(`pos-${restaurantId}`);
        client.emit('leftRoomPOS', {
            message: `Client left room: pos-${restaurantId}`,
            timestamps: new Date()
        });
    }
    handleJoinRoomWeb(client, customerId) {
        client.join(`web-${customerId}`);
        client.emit('joinedRoomWeb', {
            message: `Client joined room: web-${customerId}`,
            timestamps: new Date()
        });
    }
    handleLeaveRoomWeb(client, customerId) {
        client.leave(`web-${customerId}`);
        client.emit('leftRoomWeb', {
            message: `Client left room: web-${customerId}`,
            timestamps: new Date()
        });
    }
};
__decorate([
    (0, websockets_1.WebSocketServer)(),
    __metadata("design:type", socket_io_1.Server)
], SocketGateway.prototype, "server", void 0);
__decorate([
    (0, websockets_1.SubscribeMessage)('joinRoomPOS'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, String]),
    __metadata("design:returntype", void 0)
], SocketGateway.prototype, "handleJoinRoomPOS", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('leaveRoomPOS'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, String]),
    __metadata("design:returntype", void 0)
], SocketGateway.prototype, "handleLeaveRoomPOS", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('joinRoomWeb'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)('customerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, String]),
    __metadata("design:returntype", void 0)
], SocketGateway.prototype, "handleJoinRoomWeb", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('leaveRoomWeb'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)('customerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, String]),
    __metadata("design:returntype", void 0)
], SocketGateway.prototype, "handleLeaveRoomWeb", null);
SocketGateway = __decorate([
    (0, websockets_1.WebSocketGateway)({
        namespace: 'v1/notify',
        cors: {
            origin: '*'
        }
    })
], SocketGateway);
exports.SocketGateway = SocketGateway;
//# sourceMappingURL=socket.gateway.js.map