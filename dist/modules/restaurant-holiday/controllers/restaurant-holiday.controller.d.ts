import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdateHolidaysDto } from '../dto/update-holiday.dto';
import { RestaurantHolidaysService } from '../services/restaurant-holiday.service';
export declare class RestaurantHolidaysController {
    private readonly restaurantHolidaysService;
    private readonly restaurantService;
    constructor(restaurantHolidaysService: RestaurantHolidaysService, restaurantService: RestaurantService);
    getHolidaysByRestaurantID(id: string): Promise<import("../schemas/restaurant-holiday.schema").RestaurantHolidaysDocument>;
    updateHolidaysByRestaurantID(id: string, payload: UpdateHolidaysDto): Promise<import("../schemas/restaurant-holiday.schema").RestaurantHolidaysDocument>;
}
