"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantHolidaysController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const update_holiday_dto_1 = require("../dto/update-holiday.dto");
const get_holidays_reponse_1 = require("../response/get-holidays.reponse");
const restaurant_holiday_service_1 = require("../services/restaurant-holiday.service");
let RestaurantHolidaysController = class RestaurantHolidaysController {
    constructor(restaurantHolidaysService, restaurantService) {
        this.restaurantHolidaysService = restaurantHolidaysService;
        this.restaurantService = restaurantService;
    }
    async getHolidaysByRestaurantID(id) {
        const restaurantId = await this.restaurantService.findOne({ _id: id });
        if (!restaurantId)
            throw new common_1.NotFoundException('Not Found Restaurant.');
        return await this.restaurantHolidaysService.findOne({ restaurantId: id });
    }
    async updateHolidaysByRestaurantID(id, payload) {
        const restaurantId = await this.restaurantService.findOne({ _id: id });
        if (!restaurantId)
            throw new common_1.NotFoundException('Not Found Restaurant.');
        return await this.restaurantHolidaysService.findOneAndUpdate({ restaurantId: id }, payload);
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId/holiday'),
    (0, swagger_1.ApiOkResponse)(get_holidays_reponse_1.getHolidaysResponse),
    (0, swagger_1.ApiOperation)({ summary: 'Get Restaurant Holidays by Restaurant ID' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RestaurantHolidaysController.prototype, "getHolidaysByRestaurantID", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/holiday'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Restaurant Holidays by Restaurant ID' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_holiday_dto_1.UpdateHolidaysDto]),
    __metadata("design:returntype", Promise)
], RestaurantHolidaysController.prototype, "updateHolidaysByRestaurantID", null);
RestaurantHolidaysController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('restaurant-holidays'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/restaurant'),
    __metadata("design:paramtypes", [restaurant_holiday_service_1.RestaurantHolidaysService,
        restaurant_service_1.RestaurantService])
], RestaurantHolidaysController);
exports.RestaurantHolidaysController = RestaurantHolidaysController;
//# sourceMappingURL=restaurant-holiday.controller.js.map