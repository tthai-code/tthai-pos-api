import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { RestaurantHolidaysDocument } from '../schemas/restaurant-holiday.schema';
export declare class RestaurantHolidaysService {
    private readonly RestaurantHolidaysModel;
    private request;
    constructor(RestaurantHolidaysModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<RestaurantHolidaysDocument | any>;
    resolveByCondition(condition: any): Promise<RestaurantHolidaysDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<RestaurantHolidaysDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<RestaurantHolidaysDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<RestaurantHolidaysDocument>;
    create(payload: any): Promise<RestaurantHolidaysDocument>;
    update(id: string, payload: any): Promise<RestaurantHolidaysDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<RestaurantHolidaysDocument>;
    delete(id: string): Promise<RestaurantHolidaysDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<RestaurantHolidaysDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<RestaurantHolidaysDocument>;
}
