"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantHolidaysModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const restaurant_holiday_controller_1 = require("./controllers/restaurant-holiday.controller");
const restaurant_holiday_schema_1 = require("./schemas/restaurant-holiday.schema");
const restaurant_holiday_service_1 = require("./services/restaurant-holiday.service");
let RestaurantHolidaysModule = class RestaurantHolidaysModule {
};
RestaurantHolidaysModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'restaurantHolidays', schema: restaurant_holiday_schema_1.RestaurantHolidaysSchema }
            ])
        ],
        providers: [restaurant_holiday_service_1.RestaurantHolidaysService],
        controllers: [restaurant_holiday_controller_1.RestaurantHolidaysController],
        exports: [restaurant_holiday_service_1.RestaurantHolidaysService]
    })
], RestaurantHolidaysModule);
exports.RestaurantHolidaysModule = RestaurantHolidaysModule;
//# sourceMappingURL=restaurant-holiday.module.js.map