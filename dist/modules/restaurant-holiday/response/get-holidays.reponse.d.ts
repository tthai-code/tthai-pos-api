export declare const getHolidaysResponse: {
    schema: {
        type: string;
        example: {
            message: string;
            data: {
                restaurant_id: string;
                holidays: {
                    date: string;
                    is_closed: boolean;
                    period: {
                        opens_at: string;
                        closes_at: string;
                    }[];
                }[];
                status: string;
                updated_by: {
                    username: string;
                    id: string;
                };
                updated_at: string;
                id: string;
            };
        };
    };
};
