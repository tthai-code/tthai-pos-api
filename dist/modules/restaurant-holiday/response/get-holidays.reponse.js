"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getHolidaysResponse = void 0;
exports.getHolidaysResponse = {
    schema: {
        type: 'object',
        example: {
            message: 'done',
            data: {
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                holidays: [
                    {
                        date: '05-10-2022',
                        is_closed: false,
                        period: [
                            {
                                opens_at: '09:00AM',
                                closes_at: '12:00AM'
                            }
                        ]
                    },
                    {
                        date: '13-10-2022',
                        is_closed: true,
                        period: []
                    }
                ],
                status: 'active',
                updated_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                updated_at: '2022-10-05T07:01:26.119Z',
                id: '633d2b491ef7a2c53f0c885b'
            }
        }
    }
};
//# sourceMappingURL=get-holidays.reponse.js.map