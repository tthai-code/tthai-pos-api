import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type RestaurantHolidaysDocument = RestaurantHolidaysField & Document;
declare class PeriodFieldsSchema {
    opensAt: string;
    closesAt: string;
}
declare class HolidaysFieldsSchema {
    date: string;
    period: Array<PeriodFieldsSchema>;
    isClosed: boolean;
}
export declare class RestaurantHolidaysField {
    restaurantId: Types.ObjectId;
    holidays: Array<HolidaysFieldsSchema>;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const RestaurantHolidaysSchema: MongooseSchema<Document<RestaurantHolidaysField, any, any>, import("mongoose").Model<Document<RestaurantHolidaysField, any, any>, any, any, any>, any, any>;
export {};
