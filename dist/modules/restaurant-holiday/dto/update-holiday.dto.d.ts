declare class PeriodFieldsDto {
    readonly opensAt: string;
    readonly closesAt: string;
}
declare class HolidayFieldsDto {
    readonly date: string;
    readonly period: Array<PeriodFieldsDto>;
    readonly isClosed: boolean;
}
export declare class UpdateHolidaysDto {
    readonly holidays: Array<HolidayFieldsDto>;
}
export {};
