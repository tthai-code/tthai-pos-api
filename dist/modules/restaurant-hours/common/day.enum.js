"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DayEnum = void 0;
var DayEnum;
(function (DayEnum) {
    DayEnum["MONDAY"] = "MONDAY";
    DayEnum["TUESDAY"] = "TUESDAY";
    DayEnum["WEDNESDAY"] = "WEDNESDAY";
    DayEnum["THURSDAY"] = "THURSDAY";
    DayEnum["FRIDAY"] = "FRIDAY";
    DayEnum["SATURDAY"] = "SATURDAY";
    DayEnum["SUNDAY"] = "SUNDAY";
})(DayEnum = exports.DayEnum || (exports.DayEnum = {}));
//# sourceMappingURL=day.enum.js.map