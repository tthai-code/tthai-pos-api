import { UpdateHoursDto } from '../dto/update-hours';
import { RestaurantHoursLogic } from '../logics/restaurant-hours.logic';
export declare class RestaurantHoursController {
    private readonly restaurantHoursLogic;
    constructor(restaurantHoursLogic: RestaurantHoursLogic);
    getAllRestaurantHours(id: string): Promise<{
        hours: any[];
    }>;
    updateHours(id: string, payload: UpdateHoursDto): Promise<{
        hours: any[];
    }>;
}
