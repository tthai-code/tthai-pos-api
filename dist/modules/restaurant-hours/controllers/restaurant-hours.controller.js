"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantHoursController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const update_hours_1 = require("../dto/update-hours");
const restaurant_hours_entity_1 = require("../entity/restaurant-hours.entity");
const restaurant_hours_logic_1 = require("../logics/restaurant-hours.logic");
let RestaurantHoursController = class RestaurantHoursController {
    constructor(restaurantHoursLogic) {
        this.restaurantHoursLogic = restaurantHoursLogic;
    }
    async getAllRestaurantHours(id) {
        return await this.restaurantHoursLogic.getRestaurantHours(id);
    }
    async updateHours(id, payload) {
        return await this.restaurantHoursLogic.updateHours(id, payload);
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId/hours'),
    (0, swagger_1.ApiOkResponse)({ type: () => restaurant_hours_entity_1.RestaurantHoursResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Restaurant Hours by Restaurant ID' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RestaurantHoursController.prototype, "getAllRestaurantHours", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/hours'),
    (0, swagger_1.ApiOkResponse)({ type: () => restaurant_hours_entity_1.RestaurantHoursResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Update Restaurant Hours by Restaurant ID' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_hours_1.UpdateHoursDto]),
    __metadata("design:returntype", Promise)
], RestaurantHoursController.prototype, "updateHours", null);
RestaurantHoursController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('restaurant-hours'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/restaurant'),
    __metadata("design:paramtypes", [restaurant_hours_logic_1.RestaurantHoursLogic])
], RestaurantHoursController);
exports.RestaurantHoursController = RestaurantHoursController;
//# sourceMappingURL=restaurant-hours.controller.js.map