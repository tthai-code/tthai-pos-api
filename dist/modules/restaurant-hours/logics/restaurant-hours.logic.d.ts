import { CategoryHoursService } from 'src/modules/menu-category/services/category-hours.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdateHoursDto } from '../dto/update-hours';
import { RestaurantDayService } from '../services/restaurant-day.service';
import { RestaurantPeriodService } from '../services/restaurant-period.service';
export declare class RestaurantHoursLogic {
    private readonly restaurantDayService;
    private readonly restaurantPeriodService;
    private readonly restaurantService;
    private readonly categoryHoursService;
    constructor(restaurantDayService: RestaurantDayService, restaurantPeriodService: RestaurantPeriodService, restaurantService: RestaurantService, categoryHoursService: CategoryHoursService);
    private dayScoreQuery;
    private initDay;
    getRestaurantHours(restaurantId: string): Promise<{
        hours: any[];
    }>;
    updateHours(restaurantId: string, payload: UpdateHoursDto): Promise<{
        hours: any[];
    }>;
}
