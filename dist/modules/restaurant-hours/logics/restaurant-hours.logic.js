"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantHoursLogic = void 0;
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const dayjs = require("dayjs");
const status_enum_1 = require("../../../common/enum/status.enum");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const category_hours_service_1 = require("../../menu-category/services/category-hours.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const day_enum_1 = require("../common/day.enum");
const restaurant_day_service_1 = require("../services/restaurant-day.service");
const restaurant_period_service_1 = require("../services/restaurant-period.service");
let RestaurantHoursLogic = class RestaurantHoursLogic {
    constructor(restaurantDayService, restaurantPeriodService, restaurantService, categoryHoursService) {
        this.restaurantDayService = restaurantDayService;
        this.restaurantPeriodService = restaurantPeriodService;
        this.restaurantService = restaurantService;
        this.categoryHoursService = categoryHoursService;
    }
    dayScoreQuery() {
        return {
            $switch: {
                branches: [
                    {
                        case: { $eq: ['$label', day_enum_1.DayEnum.MONDAY] },
                        then: 0
                    },
                    {
                        case: { $eq: ['$label', day_enum_1.DayEnum.TUESDAY] },
                        then: 1
                    },
                    {
                        case: { $eq: ['$label', day_enum_1.DayEnum.WEDNESDAY] },
                        then: 2
                    },
                    {
                        case: { $eq: ['$label', day_enum_1.DayEnum.THURSDAY] },
                        then: 3
                    },
                    {
                        case: { $eq: ['$label', day_enum_1.DayEnum.FRIDAY] },
                        then: 4
                    },
                    {
                        case: { $eq: ['$label', day_enum_1.DayEnum.SATURDAY] },
                        then: 5
                    }
                ],
                default: 6
            }
        };
    }
    initDay(id) {
        const days = Object.values(day_enum_1.DayEnum);
        return days.map((day, index) => ({
            restaurantId: id,
            label: day,
            isClosed: true,
            position: index + 1
        }));
    }
    async getRestaurantHours(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        let restaurantDays = await this.restaurantDayService.getAll({ restaurantId }, {
            _id: 0,
            label: 1,
            isClosed: 1,
            score: this.dayScoreQuery()
        });
        if (restaurantDays.length <= 0) {
            const days = this.initDay(restaurantId);
            await this.restaurantDayService.createMany(days);
            restaurantDays = await this.restaurantDayService.getAll({ restaurantId }, {
                _id: 0,
                label: 1,
                isClosed: 1,
                score: this.dayScoreQuery()
            });
        }
        restaurantDays.sort((a, b) => { var _a, _b; return ((_a = a === null || a === void 0 ? void 0 : a.toObject()) === null || _a === void 0 ? void 0 : _a.score) - ((_b = b === null || b === void 0 ? void 0 : b.toObject()) === null || _b === void 0 ? void 0 : _b.score); });
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { label: 1, opensAt: 1, closesAt: 1 });
        restaurantPeriods.sort((a, b) => {
            const [hourA, minuteA] = a.opensAt.split(':');
            const [hourB, minuteB] = b.opensAt.split(':');
            const dateA = dayjs().hour(hourA).minute(minuteA).toDate();
            const dateB = dayjs().hour(hourB).minute(minuteB).toDate();
            if (dateA < dateB)
                return -1;
            return 1;
        });
        const result = [];
        restaurantDays.forEach((day) => {
            const period = restaurantPeriods
                .filter((item) => item.label === day.label)
                .map((transform) => ({
                opensAt: transform.opensAt,
                closesAt: transform.closesAt
            }));
            result.push({
                label: day.label,
                isClosed: day.isClosed,
                period: period
            });
        });
        return { hours: result };
    }
    async updateHours(restaurantId, payload) {
        const logic = async (session) => {
            const restaurant = await this.restaurantService.findOne({
                _id: restaurantId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!restaurant)
                throw new common_1.NotFoundException('Not found restaurant.');
            const dayPromise = [];
            const createPeriodPromise = [];
            const hours = (0, class_transformer_1.classToPlain)(payload.hours);
            hours.forEach((day) => {
                dayPromise.push(this.restaurantDayService.transactionUpdateCondition({ restaurantId, label: day.label }, { isClosed: day.isClosed }, session));
                day.period.forEach((item) => {
                    createPeriodPromise.push(this.restaurantPeriodService.transactionCreate({
                        restaurantId,
                        label: day.label,
                        opensAt: item.opensAt,
                        closesAt: item.closesAt
                    }, session));
                });
            });
            await this.restaurantPeriodService.transactionDeleteAll({ restaurantId }, session);
            await this.categoryHoursService.transactionDeleteAll({ restaurantId }, session);
            await Promise.all(dayPromise);
            await Promise.all(createPeriodPromise);
            return;
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return this.getRestaurantHours(restaurantId);
    }
};
RestaurantHoursLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_day_service_1.RestaurantDayService,
        restaurant_period_service_1.RestaurantPeriodService,
        restaurant_service_1.RestaurantService,
        category_hours_service_1.CategoryHoursService])
], RestaurantHoursLogic);
exports.RestaurantHoursLogic = RestaurantHoursLogic;
//# sourceMappingURL=restaurant-hours.logic.js.map