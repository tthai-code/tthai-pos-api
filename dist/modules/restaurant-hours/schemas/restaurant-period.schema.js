"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantPeriodSchema = exports.RestaurantPeriodFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const day_enum_1 = require("../common/day.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const status_enum_1 = require("../../../common/enum/status.enum");
let RestaurantPeriodFields = class RestaurantPeriodFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], RestaurantPeriodFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ enum: Object.values(day_enum_1.DayEnum) }),
    __metadata("design:type", String)
], RestaurantPeriodFields.prototype, "label", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], RestaurantPeriodFields.prototype, "opensAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], RestaurantPeriodFields.prototype, "closesAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], RestaurantPeriodFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], RestaurantPeriodFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], RestaurantPeriodFields.prototype, "createdBy", void 0);
RestaurantPeriodFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'restaurantPeriods' })
], RestaurantPeriodFields);
exports.RestaurantPeriodFields = RestaurantPeriodFields;
exports.RestaurantPeriodSchema = mongoose_1.SchemaFactory.createForClass(RestaurantPeriodFields);
exports.RestaurantPeriodSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=restaurant-period.schema.js.map