import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type RestaurantDayDocument = RestaurantDayFields & Document;
export declare class RestaurantDayFields {
    restaurantId: Types.ObjectId;
    label: string;
    isClosed: boolean;
    position: number;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const RestaurantDaySchema: MongooseSchema<Document<RestaurantDayFields, any, any>, import("mongoose").Model<Document<RestaurantDayFields, any, any>, any, any, any>, any, any>;
