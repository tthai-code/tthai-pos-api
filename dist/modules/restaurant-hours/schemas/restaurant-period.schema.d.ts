import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type RestaurantPeriodDocument = RestaurantPeriodFields & Document;
export declare class RestaurantPeriodFields {
    restaurantId: Types.ObjectId;
    label: string;
    opensAt: string;
    closesAt: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const RestaurantPeriodSchema: MongooseSchema<Document<RestaurantPeriodFields, any, any>, import("mongoose").Model<Document<RestaurantPeriodFields, any, any>, any, any, any>, any, any>;
