"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantHoursModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const menu_categories_module_1 = require("../menu-category/menu-categories.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const restaurant_hours_controller_1 = require("./controllers/restaurant-hours.controller");
const restaurant_hours_logic_1 = require("./logics/restaurant-hours.logic");
const restaurant_day_schema_1 = require("./schemas/restaurant-day.schema");
const restaurant_period_schema_1 = require("./schemas/restaurant-period.schema");
const restaurant_day_service_1 = require("./services/restaurant-day.service");
const restaurant_period_service_1 = require("./services/restaurant-period.service");
let RestaurantHoursModule = class RestaurantHoursModule {
};
RestaurantHoursModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => menu_categories_module_1.MenuCategoriesModule),
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'restaurantDay', schema: restaurant_day_schema_1.RestaurantDaySchema },
                { name: 'restaurantPeriod', schema: restaurant_period_schema_1.RestaurantPeriodSchema }
            ])
        ],
        providers: [
            restaurant_period_service_1.RestaurantPeriodService,
            restaurant_day_service_1.RestaurantDayService,
            restaurant_hours_logic_1.RestaurantHoursLogic
        ],
        controllers: [restaurant_hours_controller_1.RestaurantHoursController],
        exports: [restaurant_day_service_1.RestaurantDayService, restaurant_period_service_1.RestaurantPeriodService, restaurant_hours_logic_1.RestaurantHoursLogic]
    })
], RestaurantHoursModule);
exports.RestaurantHoursModule = RestaurantHoursModule;
//# sourceMappingURL=restaurant-hours.module.js.map