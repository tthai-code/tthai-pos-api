declare class PeriodFieldsDto {
    opensAt: string;
    closesAt: string;
}
declare class HoursFieldsDto {
    readonly label: string;
    readonly isClosed: boolean;
    readonly period: Array<PeriodFieldsDto>;
}
export declare class UpdateHoursDto {
    readonly hours: Array<HoursFieldsDto>;
}
export {};
