"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateHoursDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const day_enum_1 = require("../common/day.enum");
class PeriodFieldsDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ type: String, example: '09:00AM' }),
    __metadata("design:type", String)
], PeriodFieldsDto.prototype, "opensAt", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ type: String, example: '12:00AM' }),
    __metadata("design:type", String)
], PeriodFieldsDto.prototype, "closesAt", void 0);
class HoursFieldsDto {
}
__decorate([
    (0, class_validator_1.IsEnum)(day_enum_1.DayEnum),
    (0, swagger_1.ApiProperty)({ enum: Object.values(day_enum_1.DayEnum), example: day_enum_1.DayEnum.MONDAY }),
    __metadata("design:type", String)
], HoursFieldsDto.prototype, "label", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: false }),
    __metadata("design:type", Boolean)
], HoursFieldsDto.prototype, "isClosed", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => PeriodFieldsDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => [PeriodFieldsDto] }),
    __metadata("design:type", Array)
], HoursFieldsDto.prototype, "period", void 0);
class UpdateHoursDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => HoursFieldsDto),
    (0, class_validator_1.ArrayNotEmpty)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({
        type: () => [HoursFieldsDto],
        example: {
            hours: [
                {
                    label: 'MONDAY',
                    isClosed: false,
                    period: [
                        {
                            opensAt: '09:00AM',
                            closesAt: '12:00AM'
                        }
                    ]
                },
                {
                    label: 'TUESDAY',
                    isClosed: true,
                    period: []
                },
                {
                    label: 'WEDNESDAY',
                    isClosed: true,
                    period: []
                },
                {
                    label: 'THURSDAY',
                    isClosed: true,
                    period: []
                },
                {
                    label: 'FRIDAY',
                    isClosed: true,
                    period: []
                },
                {
                    label: 'SATURDAY',
                    isClosed: true,
                    period: []
                },
                {
                    label: 'SUNDAY',
                    isClosed: true,
                    period: []
                }
            ]
        }
    }),
    __metadata("design:type", Array)
], UpdateHoursDto.prototype, "hours", void 0);
exports.UpdateHoursDto = UpdateHoursDto;
//# sourceMappingURL=update-hours.js.map