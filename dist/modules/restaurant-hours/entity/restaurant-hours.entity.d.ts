import { ResponseDto } from 'src/common/entity/response.entity';
declare class PeriodFields {
    opens_at: string;
    closes_at: string;
}
declare class HoursFields {
    label: string;
    is_closed: boolean;
    period: Array<PeriodFields>;
}
declare class RestaurantHoursFieldsResponse {
    hours: Array<HoursFields>;
}
export declare class RestaurantHoursResponse extends ResponseDto<RestaurantHoursFieldsResponse> {
    data: RestaurantHoursFieldsResponse;
}
export {};
