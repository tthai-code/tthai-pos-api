"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantHoursResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class PeriodFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PeriodFields.prototype, "opens_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PeriodFields.prototype, "closes_at", void 0);
class HoursFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], HoursFields.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], HoursFields.prototype, "is_closed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [PeriodFields] }),
    __metadata("design:type", Array)
], HoursFields.prototype, "period", void 0);
class RestaurantHoursFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [HoursFields] }),
    __metadata("design:type", Array)
], RestaurantHoursFieldsResponse.prototype, "hours", void 0);
class RestaurantHoursResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: RestaurantHoursFieldsResponse,
        example: {
            hours: [
                {
                    label: 'MONDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '09:00AM',
                            closes_at: '12:00AM'
                        },
                        {
                            opens_at: '03:00PM',
                            closes_at: '09:00PM'
                        }
                    ]
                },
                {
                    label: 'TUESDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '08:00AM',
                            closes_at: '05:00PM'
                        },
                        {
                            opens_at: '11:00PM',
                            closes_at: '00:30AM'
                        }
                    ]
                },
                {
                    label: 'WEDNESDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'THURSDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'FRIDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'SATURDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'SUNDAY',
                    is_closed: true,
                    period: []
                }
            ]
        }
    }),
    __metadata("design:type", RestaurantHoursFieldsResponse)
], RestaurantHoursResponse.prototype, "data", void 0);
exports.RestaurantHoursResponse = RestaurantHoursResponse;
//# sourceMappingURL=restaurant-hours.entity.js.map