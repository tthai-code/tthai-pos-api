import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { RestaurantDayDocument } from '../schemas/restaurant-day.schema';
export declare class RestaurantDayService {
    private readonly RestaurantDayModel;
    private request;
    constructor(RestaurantDayModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<RestaurantDayDocument | any>;
    resolveByCondition(condition: any): Promise<RestaurantDayDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<RestaurantDayDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<RestaurantDayDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<RestaurantDayDocument>;
    transactionUpdateCondition(condition: any, product: any, session: ClientSession): Promise<RestaurantDayDocument>;
    create(payload: any): Promise<RestaurantDayDocument>;
    createMany(payload: any): Promise<RestaurantDayDocument>;
    update(id: string, payload: any): Promise<RestaurantDayDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<RestaurantDayDocument>;
    getAll(condition: any, project?: any, options?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<RestaurantDayDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<RestaurantDayDocument>;
}
