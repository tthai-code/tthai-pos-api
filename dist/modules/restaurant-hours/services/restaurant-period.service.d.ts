import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { RestaurantPeriodDocument } from '../schemas/restaurant-period.schema';
export declare class RestaurantPeriodService {
    private readonly RestaurantPeriodModel;
    private request;
    constructor(RestaurantPeriodModel: any | Model<RestaurantPeriodDocument>, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<RestaurantPeriodDocument | any>;
    resolveByCondition(condition: any): Promise<RestaurantPeriodDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<RestaurantPeriodDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<RestaurantPeriodDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<RestaurantPeriodDocument>;
    create(payload: any): Promise<RestaurantPeriodDocument>;
    update(id: string, payload: any): Promise<RestaurantPeriodDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<RestaurantPeriodDocument>;
    getAll(condition: any, project?: any, options?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<RestaurantPeriodDocument>;
    deleteAll(condition: any): Promise<void>;
    transactionDeleteAll(condition: any, session: any): Promise<void>;
    findOneWithSelect(condition: any, options?: any): Promise<RestaurantPeriodDocument>;
}
