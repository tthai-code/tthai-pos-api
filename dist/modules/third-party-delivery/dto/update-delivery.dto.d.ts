export declare class UpdateDeliveryDto {
    readonly username: string;
    password: string;
    readonly providerId: string;
    readonly providerStoreId: string;
    readonly note: string;
}
export declare class ConnectProviderDto {
    readonly isActive: boolean;
}
