"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliverProviderPaginateDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
class DeliverProviderPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'createdAt';
        this.sortOrder = 'desc';
        this.search = '';
    }
    buildQuery() {
        return {
            $or: [
                {
                    name: {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            status: this.status || { $ne: status_enum_1.StatusEnum.DELETED }
        };
    }
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'createdAt' }),
    __metadata("design:type", String)
], DeliverProviderPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'desc' }),
    __metadata("design:type", String)
], DeliverProviderPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'test' }),
    __metadata("design:type", String)
], DeliverProviderPaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: status_enum_1.StatusEnum.ACTIVE,
        enum: Object.values(status_enum_1.StatusEnum)
    }),
    __metadata("design:type", String)
], DeliverProviderPaginateDto.prototype, "status", void 0);
exports.DeliverProviderPaginateDto = DeliverProviderPaginateDto;
//# sourceMappingURL=get-paginate-delivery.dto.js.map