export declare class CreateDeliveryProviderDto {
    readonly name: string;
    readonly imageUrl: string;
    readonly status: string;
}
