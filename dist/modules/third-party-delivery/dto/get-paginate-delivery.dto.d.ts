import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class DeliverProviderPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly status: string;
    buildQuery(): {
        $or: {
            name: {
                $regex: string;
                $options: string;
            };
        }[];
        status: string | {
            $ne: StatusEnum;
        };
    };
}
