"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryService = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const status_enum_1 = require("../../../common/enum/status.enum");
let DeliveryService = class DeliveryService {
    constructor(DeliveryModel, request) {
        this.DeliveryModel = DeliveryModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.DeliveryModel.findById({ _id: id });
    }
    async resolveByCondition(condition) {
        return this.DeliveryModel.findOne(condition);
    }
    async isExists(condition) {
        const result = await this.DeliveryModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.DeliveryModel;
    }
    async aggregate(pipeline) {
        return this.DeliveryModel.aggregate(pipeline);
    }
    async getSession() {
        await this.DeliveryModel.createCollection();
        return this.DeliveryModel.startSession();
    }
    async transactionCreate(payload, session) {
        const document = new this.DeliveryModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save({ session });
    }
    async transactionUpdate(id, product, session) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save({ session });
    }
    async create(payload) {
        const document = new this.DeliveryModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async update(id, payload) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, payload)).save();
    }
    async findOneAndUpdate(condition, payload) {
        const document = await this.resolveByCondition(condition);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, payload)).save();
    }
    async delete(id) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set({ status: status_enum_1.StatusEnum.DELETED }).save();
    }
    getAll(condition, project) {
        return this.DeliveryModel.find(condition, project);
    }
    findById(id) {
        return this.DeliveryModel.findById(id);
    }
    findOne(condition) {
        return this.DeliveryModel.findOne(condition);
    }
    findOneWithSelect(condition, options) {
        return this.DeliveryModel.findOne(condition, options);
    }
};
DeliveryService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('delivery')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], DeliveryService);
exports.DeliveryService = DeliveryService;
//# sourceMappingURL=delivery.service.js.map