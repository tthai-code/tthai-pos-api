import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { DeliveryDocument } from '../schemas/delivery.schema';
export declare class DeliveryService {
    private readonly DeliveryModel;
    private request;
    constructor(DeliveryModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<DeliveryDocument | any>;
    resolveByCondition(condition: any): Promise<DeliveryDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<DeliveryDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<DeliveryDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<DeliveryDocument>;
    create(payload: any): Promise<DeliveryDocument>;
    update(id: string, payload: any): Promise<DeliveryDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<DeliveryDocument>;
    delete(id: string): Promise<DeliveryDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<DeliveryDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<DeliveryDocument>;
}
