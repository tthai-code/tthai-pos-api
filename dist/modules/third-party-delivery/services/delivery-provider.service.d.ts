import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { DeliveryProviderDocument } from '../schemas/delivery-provider.schema';
import { DeliverProviderPaginateDto } from '../dto/get-paginate-delivery.dto';
export declare class DeliveryProviderService {
    private readonly DeliveryProviderModel;
    private request;
    constructor(DeliveryProviderModel: PaginateModel<DeliveryProviderDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<DeliveryProviderDocument | any>;
    resolveByCondition(condition: any): Promise<DeliveryProviderDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<DeliveryProviderDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<DeliveryProviderDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<DeliveryProviderDocument>;
    create(payload: any): Promise<DeliveryProviderDocument>;
    update(id: string, payload: any): Promise<DeliveryProviderDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<DeliveryProviderDocument>;
    delete(id: string): Promise<DeliveryProviderDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<DeliveryProviderDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<DeliveryProviderDocument>;
    paginate(query: any, queryParam: DeliverProviderPaginateDto, selectFields?: any): Promise<PaginateResult<DeliveryProviderDocument>>;
}
