"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const update_delivery_dto_1 = require("../dto/update-delivery.dto");
const delivery_entity_1 = require("../entity/delivery.entity");
const delivery_logic_1 = require("../logics/delivery.logic");
const delivery_service_1 = require("../services/delivery.service");
let DeliveryController = class DeliveryController {
    constructor(deliveryService, deliveryLogic) {
        this.deliveryService = deliveryService;
        this.deliveryLogic = deliveryLogic;
    }
    async getDeliveries(id) {
        return await this.deliveryLogic.getDeliverByRestaurantId(id);
    }
    async getDelivery(id) {
        return await this.deliveryLogic.getDelivery(id);
    }
    async updateDelivery(id, payload) {
        await this.deliveryLogic.updateDelivery(id, payload);
        return { success: true };
    }
    async providerConnection(id, payload) {
        return await this.deliveryLogic.handleConnection(id, payload);
    }
    async getUberEatsAuthUrl() {
        return await this.deliveryLogic.getUberEatsAuthUrl();
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId/all'),
    (0, swagger_1.ApiOkResponse)({ type: () => delivery_entity_1.GetAllDeliveryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get all 3rd party delivery by restaurantId' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DeliveryController.prototype, "getDeliveries", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => delivery_entity_1.GetDeliveryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get a 3rd party delivery setting by id' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DeliveryController.prototype, "getDelivery", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => delivery_entity_1.UpdateDeliveryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Update a 3rd delivery setting by id' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_delivery_dto_1.UpdateDeliveryDto]),
    __metadata("design:returntype", Promise)
], DeliveryController.prototype, "updateDelivery", null);
__decorate([
    (0, common_1.Patch)(':id/connect-provider'),
    (0, swagger_1.ApiOperation)({ summary: 'Connect/Disconnect Provider' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_delivery_dto_1.ConnectProviderDto]),
    __metadata("design:returntype", Promise)
], DeliveryController.prototype, "providerConnection", null);
__decorate([
    (0, common_1.Get)('ubereats/auth'),
    (0, swagger_1.ApiOperation)({ summary: 'Get UberEats Auth URL' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DeliveryController.prototype, "getUberEatsAuthUrl", null);
DeliveryController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('third-party-delivery'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/delivery'),
    __metadata("design:paramtypes", [delivery_service_1.DeliveryService,
        delivery_logic_1.DeliveryLogic])
], DeliveryController);
exports.DeliveryController = DeliveryController;
//# sourceMappingURL=delivery.controller.js.map