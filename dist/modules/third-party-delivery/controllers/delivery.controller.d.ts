/// <reference types="mongoose" />
import { ConnectProviderDto, UpdateDeliveryDto } from '../dto/update-delivery.dto';
import { DeliveryLogic } from '../logics/delivery.logic';
import { DeliveryService } from '../services/delivery.service';
export declare class DeliveryController {
    private readonly deliveryService;
    private readonly deliveryLogic;
    constructor(deliveryService: DeliveryService, deliveryLogic: DeliveryLogic);
    getDeliveries(id: string): Promise<any[]>;
    getDelivery(id: string): Promise<{
        id: any;
        deliveryId: import("mongoose").Types.ObjectId;
        deliveryName: string;
        deliveryImageUrl: string;
        providerId: string;
        username: string;
        password: string;
        isActive: boolean;
        providerStoreId: string;
        note: string;
    }>;
    updateDelivery(id: string, payload: UpdateDeliveryDto): Promise<{
        success: boolean;
    }>;
    providerConnection(id: string, payload: ConnectProviderDto): Promise<void>;
    getUberEatsAuthUrl(): Promise<{
        authUrl: any;
    }>;
}
