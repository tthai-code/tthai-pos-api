import { CreateDeliveryProviderDto } from '../dto/create-provider.dto';
import { DeliverProviderPaginateDto } from '../dto/get-paginate-delivery.dto';
import { DeliveryProviderService } from '../services/delivery-provider.service';
export declare class DeliveryProviderController {
    private readonly deliveryProviderService;
    constructor(deliveryProviderService: DeliveryProviderService);
    getDeliveryProviders(query: DeliverProviderPaginateDto): Promise<PaginateResult<import("../schemas/delivery-provider.schema").DeliveryProviderDocument>>;
    createDeliveryProvider(payload: CreateDeliveryProviderDto): Promise<import("../schemas/delivery-provider.schema").DeliveryProviderDocument>;
    getDeliveryProvider(id: string): Promise<import("../schemas/delivery-provider.schema").DeliveryProviderDocument>;
    updateDeliveryProvider(id: string, payload: CreateDeliveryProviderDto): Promise<import("../schemas/delivery-provider.schema").DeliveryProviderDocument>;
    deleteDeliveryProvider(id: string): Promise<import("../schemas/delivery-provider.schema").DeliveryProviderDocument>;
}
