"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateDeliveryResponse = exports.GetAllDeliveryResponse = exports.GetDeliveryResponse = exports.GetDeliveryResponseField = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class GetDeliveryResponseField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetDeliveryResponseField.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetDeliveryResponseField.prototype, "delivery_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetDeliveryResponseField.prototype, "delivery_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetDeliveryResponseField.prototype, "delivery_image_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetDeliveryResponseField.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetDeliveryResponseField.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], GetDeliveryResponseField.prototype, "is_active", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetDeliveryResponseField.prototype, "note", void 0);
exports.GetDeliveryResponseField = GetDeliveryResponseField;
class UpdateResponseField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], UpdateResponseField.prototype, "success", void 0);
class GetDeliveryResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: GetDeliveryResponseField,
        example: {
            id: '634401ae397a09daeae8781f',
            delivery_id: '6343ff580792826b8e690f8d',
            delivery_name: 'Uber Eats',
            delivery_image_url: 'https://storage.googleapis.com/tthai-pos-staging/upload/166540063611168653.png',
            username: null,
            password: null,
            is_active: false,
            note: null
        }
    }),
    __metadata("design:type", GetDeliveryResponseField)
], GetDeliveryResponse.prototype, "data", void 0);
exports.GetDeliveryResponse = GetDeliveryResponse;
class GetAllDeliveryResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [GetDeliveryResponseField],
        example: [
            {
                id: '634400b5df92a1e04f885644',
                delivery_id: '6343b8728702273dc9187eac',
                delivery_name: 'DoorDash',
                delivery_image_url: 'https://storage.googleapis.com/tthai-pos-staging/upload/166538232509038624.png',
                username: null,
                password: null,
                is_active: false,
                note: null
            },
            {
                id: '634401ae397a09daeae8781e',
                delivery_id: '6343fd82a650c90357a350de',
                delivery_name: 'GrabHub',
                delivery_image_url: 'https://storage.googleapis.com/tthai-pos-staging/upload/166540016857811015.png',
                username: null,
                password: null,
                is_active: false,
                note: null
            }
        ]
    }),
    __metadata("design:type", Array)
], GetAllDeliveryResponse.prototype, "data", void 0);
exports.GetAllDeliveryResponse = GetAllDeliveryResponse;
class UpdateDeliveryResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: UpdateResponseField,
        example: {
            success: true
        }
    }),
    __metadata("design:type", UpdateResponseField)
], UpdateDeliveryResponse.prototype, "data", void 0);
exports.UpdateDeliveryResponse = UpdateDeliveryResponse;
//# sourceMappingURL=delivery.entity.js.map