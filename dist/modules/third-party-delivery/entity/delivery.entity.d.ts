import { ResponseDto } from 'src/common/entity/response.entity';
export declare class GetDeliveryResponseField {
    id: string;
    delivery_id: string;
    delivery_name: string;
    delivery_image_url: string;
    username: string;
    password: string;
    is_active: boolean;
    note: string;
}
declare class UpdateResponseField {
    success: boolean;
}
export declare class GetDeliveryResponse extends ResponseDto<GetDeliveryResponseField> {
    data: GetDeliveryResponseField;
}
export declare class GetAllDeliveryResponse extends ResponseDto<Array<GetDeliveryResponseField>> {
    data: Array<GetDeliveryResponseField>;
}
export declare class UpdateDeliveryResponse extends ResponseDto<UpdateResponseField> {
    data: UpdateResponseField;
}
export {};
