import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
import { CreateDeliveryProviderFieldsDto } from './create-delivery-provider.entity';
export declare class PaginateDeliveryProviderResult extends PaginateResponseDto<Array<CreateDeliveryProviderFieldsDto>> {
    results: Array<CreateDeliveryProviderFieldsDto>;
}
export declare class PaginateDeliveryProviderResponse extends ResponseDto<PaginateDeliveryProviderResult> {
    data: PaginateDeliveryProviderResult;
}
