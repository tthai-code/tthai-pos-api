import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
export declare class CreateDeliveryProviderFieldsDto extends TimestampResponseDto {
    id: string;
    name: string;
    imageUrl: string;
}
export declare class CreateDeliveryProviderResponseDto extends ResponseDto<CreateDeliveryProviderFieldsDto> {
    data: CreateDeliveryProviderFieldsDto;
}
export declare class DeleteDeliveryProviderResponse extends ResponseDto<CreateDeliveryProviderFieldsDto> {
    data: CreateDeliveryProviderFieldsDto;
}
