"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteDeliveryProviderResponse = exports.CreateDeliveryProviderResponseDto = exports.CreateDeliveryProviderFieldsDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
class CreateDeliveryProviderFieldsDto extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateDeliveryProviderFieldsDto.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateDeliveryProviderFieldsDto.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateDeliveryProviderFieldsDto.prototype, "imageUrl", void 0);
exports.CreateDeliveryProviderFieldsDto = CreateDeliveryProviderFieldsDto;
class CreateDeliveryProviderResponseDto extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CreateDeliveryProviderFieldsDto,
        example: {
            status: 'active',
            image_url: 'https://storage.googleapis.com/tthai-pos-staging/upload/166546057662955493.png',
            name: 'Postmates',
            created_at: '2022-10-11T03:56:46.826Z',
            updated_at: '2022-10-11T03:56:46.826Z',
            updated_by: {
                username: 'superadmin',
                id: '6318d06a1fa78236d380db83'
            },
            created_by: {
                username: 'superadmin',
                id: '6318d06a1fa78236d380db83'
            },
            id: '6344e97e291d3456a6d5acab'
        }
    }),
    __metadata("design:type", CreateDeliveryProviderFieldsDto)
], CreateDeliveryProviderResponseDto.prototype, "data", void 0);
exports.CreateDeliveryProviderResponseDto = CreateDeliveryProviderResponseDto;
class DeleteDeliveryProviderResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CreateDeliveryProviderFieldsDto,
        example: {
            message: 'done',
            data: {
                created_by: {
                    username: 'superadmin',
                    id: '6318d06a1fa78236d380db83'
                },
                updated_by: {
                    username: 'superadmin',
                    id: '6318d06a1fa78236d380db83'
                },
                status: 'deleted',
                image_url: 'https://storage.googleapis.com/tthai-pos-staging/upload/166538232509038624.png',
                name: 'DoorDash',
                updated_at: '2022-10-11T04:48:36.936Z',
                created_at: '2022-10-10T06:15:14.721Z',
                id: '6343b8728702273dc9187eac'
            }
        }
    }),
    __metadata("design:type", CreateDeliveryProviderFieldsDto)
], DeleteDeliveryProviderResponse.prototype, "data", void 0);
exports.DeleteDeliveryProviderResponse = DeleteDeliveryProviderResponse;
//# sourceMappingURL=create-delivery-provider.entity.js.map