/// <reference types="mongoose" />
import { DeliverySettingService } from 'src/modules/kitchen-hub/services/delivery-setting.service';
import { KitchenHubHttpService } from 'src/modules/kitchen-hub/services/kitchen-hub-http.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { ConnectProviderDto, UpdateDeliveryDto } from '../dto/update-delivery.dto';
import { DeliveryProviderService } from '../services/delivery-provider.service';
import { DeliveryService } from '../services/delivery.service';
export declare class DeliveryLogic {
    private readonly deliveryService;
    private readonly restaurantService;
    private readonly deliveryProviderService;
    private readonly deliverySettingService;
    private readonly httpService;
    constructor(deliveryService: DeliveryService, restaurantService: RestaurantService, deliveryProviderService: DeliveryProviderService, deliverySettingService: DeliverySettingService, httpService: KitchenHubHttpService);
    private transformGetDeliveries;
    getDeliverByRestaurantId(id: string): Promise<any[]>;
    getDelivery(id: string): Promise<{
        id: any;
        deliveryId: import("mongoose").Types.ObjectId;
        deliveryName: string;
        deliveryImageUrl: string;
        providerId: string;
        username: string;
        password: string;
        isActive: boolean;
        providerStoreId: string;
        note: string;
    }>;
    getUberEatsAuthUrl(): Promise<{
        authUrl: any;
    }>;
    updateDelivery(id: string, payload: UpdateDeliveryDto): Promise<{
        success: boolean;
    }>;
    handleConnection(id: string, payload: ConnectProviderDto): Promise<void>;
}
