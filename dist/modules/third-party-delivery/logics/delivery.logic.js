"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const kitchen_hub_interface_1 = require("../../kitchen-hub/interfaces/kitchen-hub.interface");
const delivery_setting_service_1 = require("../../kitchen-hub/services/delivery-setting.service");
const kitchen_hub_http_service_1 = require("../../kitchen-hub/services/kitchen-hub-http.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const base64_1 = require("../../../utilities/base64");
const delivery_provider_service_1 = require("../services/delivery-provider.service");
const delivery_service_1 = require("../services/delivery.service");
let DeliveryLogic = class DeliveryLogic {
    constructor(deliveryService, restaurantService, deliveryProviderService, deliverySettingService, httpService) {
        this.deliveryService = deliveryService;
        this.restaurantService = restaurantService;
        this.deliveryProviderService = deliveryProviderService;
        this.deliverySettingService = deliverySettingService;
        this.httpService = httpService;
    }
    transformGetDeliveries(delivery, deliveryProvider) {
        const result = [];
        delivery.forEach((item) => {
            var _a, _b, _c, _d;
            const index = deliveryProvider.findIndex((elm) => JSON.stringify(elm._id) === JSON.stringify(item.deliveryId));
            if (((_a = deliveryProvider[index]) === null || _a === void 0 ? void 0 : _a.status) === status_enum_1.StatusEnum.ACTIVE) {
                result.push({
                    id: item._id,
                    deliveryId: item.deliveryId,
                    deliveryName: (_b = deliveryProvider[index]) === null || _b === void 0 ? void 0 : _b.name,
                    deliveryImageUrl: (_c = deliveryProvider[index]) === null || _c === void 0 ? void 0 : _c.imageUrl,
                    providerId: (_d = deliveryProvider[index]) === null || _d === void 0 ? void 0 : _d.providerId,
                    username: item.username,
                    password: item.password,
                    isActive: item.isActive,
                    providerStoreId: item.providerStoreId,
                    note: item.note
                });
            }
        });
        return result;
    }
    async getDeliverByRestaurantId(id) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const deliveryProvider = await this.deliveryProviderService.getAll({
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const deliverySetting = await this.deliverySettingService.findOne({
            restaurantId: id
        }, { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 });
        if (!deliverySetting) {
            const { data } = await this.httpService.createStore(restaurant.dba);
            if (!(data === null || data === void 0 ? void 0 : data.ok))
                throw new common_1.BadRequestException(data === null || data === void 0 ? void 0 : data.error_code);
            await this.deliverySettingService.create({
                restaurantId: id,
                storeId: data.result.id
            });
        }
        if (!deliveryProvider)
            throw new common_1.BadRequestException();
        const delivery = await this.deliveryService.getAll({
            restaurantId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (deliveryProvider.length !== delivery.length) {
            const deliveryIds = deliveryProvider.map((item) => `${item._id}`);
            const existDeliveryIds = delivery.map((item) => `${item.deliveryId}`);
            const unCreatedDelivery = [];
            deliveryIds.forEach((item, index) => {
                if (!existDeliveryIds.includes(item)) {
                    unCreatedDelivery.push({
                        restaurantId: id,
                        deliveryId: item,
                        providerId: deliveryProvider[index].providerId
                    });
                }
            });
            const created = await this.deliveryService
                .getModel()
                .insertMany(unCreatedDelivery);
            if (!created)
                throw new common_1.BadRequestException();
            const newDelivery = await this.deliveryService.getAll({
                restaurantId: id,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            return this.transformGetDeliveries(newDelivery, deliveryProvider);
        }
        return this.transformGetDeliveries(delivery, deliveryProvider);
    }
    async getDelivery(id) {
        const delivery = await this.deliveryService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!delivery)
            throw new common_1.NotFoundException('Not found delivery setting.');
        const deliveryProvider = await this.deliveryProviderService.findOne({
            _id: delivery.deliveryId
        });
        if (!deliveryProvider)
            throw new common_1.NotFoundException('Not found 3rd delivery');
        return {
            id: delivery._id,
            deliveryId: delivery.deliveryId,
            deliveryName: deliveryProvider.name,
            deliveryImageUrl: deliveryProvider.imageUrl,
            providerId: deliveryProvider === null || deliveryProvider === void 0 ? void 0 : deliveryProvider.providerId,
            username: delivery.username,
            password: delivery.password,
            isActive: delivery.isActive,
            providerStoreId: delivery.providerStoreId,
            note: delivery.note
        };
    }
    async getUberEatsAuthUrl() {
        var _a;
        const payload = {
            provider_id: 'ubereats'
        };
        const { data } = await this.httpService.authProvider(payload);
        if (!data.ok)
            throw new common_1.BadRequestException(data === null || data === void 0 ? void 0 : data.error_code);
        return { authUrl: ((_a = data === null || data === void 0 ? void 0 : data.result) === null || _a === void 0 ? void 0 : _a.auth_url) || '/' };
    }
    async updateDelivery(id, payload) {
        const delivery = await this.deliveryService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!delivery) {
            throw new common_1.NotFoundException('Not found 3rd delivery setting.');
        }
        const authPayload = {
            login: payload.username,
            password: payload.password,
            provider_id: payload.providerId
        };
        if (payload.providerId !== 'ubereats') {
            const { data } = await this.httpService.authProvider(authPayload);
            if (!(data === null || data === void 0 ? void 0 : data.ok))
                throw new common_1.BadRequestException(data === null || data === void 0 ? void 0 : data.error_code);
            const { result } = data;
            if (!(result === null || result === void 0 ? void 0 : result.authenticated)) {
                throw new common_1.BadRequestException('Invalid credentials');
            }
        }
        payload.password = (0, base64_1.stringToBase64)(payload.password);
        await this.deliveryService.update(id, payload);
        return { success: true };
    }
    async handleConnection(id, payload) {
        const delivery = await this.deliveryService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!delivery) {
            throw new common_1.NotFoundException('Not found 3rd delivery setting.');
        }
        if (delivery.isActive === payload.isActive) {
            const message = delivery.isActive
                ? `${delivery.providerId} is already connected`
                : `${delivery.providerId} is already disconnected`;
            throw new common_1.BadRequestException(message);
        }
        const deliverySetting = await this.deliverySettingService.findOne({
            restaurantId: delivery.restaurantId
        }, { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 });
        if (payload.isActive) {
            const connectPayload = {
                login: delivery.username,
                password: (0, base64_1.decodeBase64toString)(delivery.password),
                provider_id: delivery.providerId,
                provider_store_id: delivery.providerStoreId,
                store_id: deliverySetting.storeId
            };
            const { data: connected } = await this.httpService.connectProvider(connectPayload);
            if (!(connected === null || connected === void 0 ? void 0 : connected.ok))
                throw new common_1.BadRequestException(connected === null || connected === void 0 ? void 0 : connected.error_code);
        }
        else {
            const disconnectPayload = {
                provider_id: delivery.providerId,
                store_id: deliverySetting.storeId
            };
            const { data: disconnected } = await this.httpService.disconnectProvider(disconnectPayload);
            if (!(disconnected === null || disconnected === void 0 ? void 0 : disconnected.ok))
                throw new common_1.BadRequestException(disconnected === null || disconnected === void 0 ? void 0 : disconnected.error_code);
        }
        await this.deliveryService.update(id, { isActive: payload.isActive });
        return;
    }
};
DeliveryLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [delivery_service_1.DeliveryService,
        restaurant_service_1.RestaurantService,
        delivery_provider_service_1.DeliveryProviderService,
        delivery_setting_service_1.DeliverySettingService,
        kitchen_hub_http_service_1.KitchenHubHttpService])
], DeliveryLogic);
exports.DeliveryLogic = DeliveryLogic;
//# sourceMappingURL=delivery.logic.js.map