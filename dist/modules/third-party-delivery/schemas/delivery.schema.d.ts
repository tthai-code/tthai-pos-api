import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type DeliveryDocument = DeliveryFields & Document;
export declare class DeliveryFields {
    restaurantId: Types.ObjectId;
    deliveryId: Types.ObjectId;
    providerId: string;
    isActive: boolean;
    username: string;
    password: string;
    note: string;
    providerStoreId: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const DeliverySchema: MongooseSchema<Document<DeliveryFields, any, any>, import("mongoose").Model<Document<DeliveryFields, any, any>, any, any, any>, any, any>;
