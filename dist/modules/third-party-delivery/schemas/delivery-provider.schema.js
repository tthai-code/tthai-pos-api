"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryProviderSchema = exports.DeliveryProviderFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
let DeliveryProviderFields = class DeliveryProviderFields {
};
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], DeliveryProviderFields.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], DeliveryProviderFields.prototype, "providerId", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], DeliveryProviderFields.prototype, "imageUrl", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], DeliveryProviderFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], DeliveryProviderFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], DeliveryProviderFields.prototype, "createdBy", void 0);
DeliveryProviderFields = __decorate([
    (0, mongoose_1.Schema)({
        timestamps: true,
        strict: true,
        collection: 'deliveryProviders'
    })
], DeliveryProviderFields);
exports.DeliveryProviderFields = DeliveryProviderFields;
exports.DeliveryProviderSchema = mongoose_1.SchemaFactory.createForClass(DeliveryProviderFields);
exports.DeliveryProviderSchema.plugin(mongoosePaginate);
exports.DeliveryProviderSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=delivery-provider.schema.js.map