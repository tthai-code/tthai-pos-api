/// <reference types="mongoose" />
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type DeliveryProviderDocument = DeliveryProviderFields & Document;
export declare class DeliveryProviderFields {
    name: string;
    providerId: string;
    imageUrl: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const DeliveryProviderSchema: import("mongoose").Schema<import("mongoose").Document<DeliveryProviderFields, any, any>, import("mongoose").Model<import("mongoose").Document<DeliveryProviderFields, any, any>, any, any, any>, any, any>;
