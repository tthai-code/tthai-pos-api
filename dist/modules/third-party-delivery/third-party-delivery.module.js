"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThirdPartyDeliveryModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const kitchen_hub_module_1 = require("../kitchen-hub/kitchen-hub.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const delivery_provider_controller_1 = require("./controllers/delivery-provider.controller");
const delivery_controller_1 = require("./controllers/delivery.controller");
const delivery_logic_1 = require("./logics/delivery.logic");
const delivery_provider_schema_1 = require("./schemas/delivery-provider.schema");
const delivery_schema_1 = require("./schemas/delivery.schema");
const delivery_provider_service_1 = require("./services/delivery-provider.service");
const delivery_service_1 = require("./services/delivery.service");
let ThirdPartyDeliveryModule = class ThirdPartyDeliveryModule {
};
ThirdPartyDeliveryModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            (0, common_1.forwardRef)(() => kitchen_hub_module_1.KitchenHubModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'deliveryProvider', schema: delivery_provider_schema_1.DeliveryProviderSchema },
                { name: 'delivery', schema: delivery_schema_1.DeliverySchema }
            ])
        ],
        providers: [delivery_provider_service_1.DeliveryProviderService, delivery_service_1.DeliveryService, delivery_logic_1.DeliveryLogic],
        controllers: [delivery_provider_controller_1.DeliveryProviderController, delivery_controller_1.DeliveryController],
        exports: [delivery_provider_service_1.DeliveryProviderService, delivery_service_1.DeliveryService]
    })
], ThirdPartyDeliveryModule);
exports.ThirdPartyDeliveryModule = ThirdPartyDeliveryModule;
//# sourceMappingURL=third-party-delivery.module.js.map