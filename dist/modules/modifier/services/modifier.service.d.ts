import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ModifierDocument } from '../models/modifier.schema';
import { ModifierPaginateDto } from '../dto/get-modifier.dto';
import { POSModifierPaginateDto } from '../dto/pos-get-modifier.dto';
export declare class ModifierService {
    private readonly ModifierModel;
    private request;
    constructor(ModifierModel: PaginateModel<ModifierDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<ModifierDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<ModifierDocument>;
    create(payload: any): Promise<ModifierDocument>;
    update(id: string, payload: any): Promise<ModifierDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<ModifierDocument>;
    delete(id: string): Promise<ModifierDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<ModifierDocument>;
    paginate(query: any, queryParam: ModifierPaginateDto | POSModifierPaginateDto, select?: any): Promise<PaginateResult<ModifierDocument>>;
}
