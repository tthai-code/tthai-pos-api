import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service';
import { CreateModifierDto } from '../dto/create-modifier.dto';
import { UpdateModifierDto } from '../dto/update-modifier.dto';
import { ModifierService } from '../services/modifier.service';
export declare class ModifierLogic {
    private readonly modifierService;
    private readonly menuModifierService;
    constructor(modifierService: ModifierService, menuModifierService: MenuModifierService);
    createModifier(payload: CreateModifierDto): Promise<import("../models/modifier.schema").ModifierDocument>;
    updateModifier(id: string, payload: UpdateModifierDto): Promise<import("../models/modifier.schema").ModifierDocument>;
}
