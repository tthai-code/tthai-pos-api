"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModifierLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const menu_modifier_service_1 = require("../../menu-modifier/services/menu-modifier.service");
const modifier_service_1 = require("../services/modifier.service");
let ModifierLogic = class ModifierLogic {
    constructor(modifierService, menuModifierService) {
        this.modifierService = modifierService;
        this.menuModifierService = menuModifierService;
    }
    async createModifier(payload) {
        const isExist = await this.modifierService.findOne({
            restaurantId: payload.restaurantId,
            position: payload.position,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (isExist) {
            throw new common_1.BadRequestException(`Position ${payload.position} is already used.`);
        }
        return await this.modifierService.create(payload);
    }
    async updateModifier(id, payload) {
        const modifier = await this.modifierService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!modifier)
            throw new common_1.NotFoundException('Not found modifier.');
        const isExist = await this.modifierService.findOne({
            _id: { $ne: id },
            restaurantId: modifier.restaurantId,
            position: payload.position,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (isExist) {
            throw new common_1.BadRequestException(`Position ${payload.position} is already used.`);
        }
        const logic = async (session) => {
            const updated = await this.modifierService.transactionUpdate(id, payload, session);
            await this.menuModifierService.transactionUpdateMany({ modifierId: id }, payload, session);
            return updated;
        };
        return (0, mongoose_transaction_1.runTransaction)(logic);
    }
};
ModifierLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [modifier_service_1.ModifierService,
        menu_modifier_service_1.MenuModifierService])
], ModifierLogic);
exports.ModifierLogic = ModifierLogic;
//# sourceMappingURL=modifier.logic.js.map