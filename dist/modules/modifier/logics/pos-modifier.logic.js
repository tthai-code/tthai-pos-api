"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSModifierLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const menu_modifier_service_1 = require("../../menu-modifier/services/menu-modifier.service");
const modifier_service_1 = require("../services/modifier.service");
let POSModifierLogic = class POSModifierLogic {
    constructor(modifierService, menuModifierService) {
        this.modifierService = modifierService;
        this.menuModifierService = menuModifierService;
    }
    async getListModifier(restaurantId, query) {
        return await this.modifierService.paginate(query.buildQuery(restaurantId), query, {
            status: 0,
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0
        });
    }
    async getModifierById(id) {
        const modifier = await this.modifierService.findOne({ _id: id, status: status_enum_1.StatusEnum.ACTIVE }, {
            status: 0,
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0
        });
        if (!modifier)
            throw new common_1.NotFoundException('Not found modifier.');
        return modifier;
    }
    async createModifier(restaurantId, payload) {
        payload.restaurantId = restaurantId;
        const created = await this.modifierService.create(payload);
        return { success: true, id: created.id };
    }
    async updateModifier(id, payload) {
        const logic = async (session) => {
            const modifier = await this.modifierService.findOne({
                _id: id,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!modifier)
                throw new common_1.NotFoundException('Not found modifier.');
            const updated = await this.modifierService.transactionUpdate(id, payload, session);
            await this.menuModifierService.transactionUpdateMany({ modifierId: id }, payload, session);
            return updated;
        };
        const result = await (0, mongoose_transaction_1.runTransaction)(logic);
        return { success: true, id: result.id };
    }
    async deleteModifier(id) {
        const modifier = await this.modifierService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!modifier)
            throw new common_1.NotFoundException('Not found modifier.');
        await this.modifierService.delete(id);
        await this.menuModifierService.deleteMany({ modifierId: id });
        return { success: true };
    }
    async createMenuModifier(menuId, modifierId) {
        const isExist = await this.menuModifierService.findOne({
            menuId,
            modifierId,
            status: { $ne: status_enum_1.StatusEnum.DELETED }
        });
        if (isExist)
            throw new common_1.BadRequestException('Menu Modifier is exist.');
        const modifier = await this.modifierService.findOne({
            _id: modifierId
        }, {
            label: 1,
            abbreviation: 1,
            type: 1,
            items: 1,
            maxSelected: 1
        });
        await this.menuModifierService.create({
            modifierId,
            menuId,
            label: modifier.label,
            abbreviation: modifier.abbreviation,
            type: modifier.type,
            items: modifier.items,
            maxSelected: modifier.maxSelected
        });
        return { success: true };
    }
    async deleteMenuModifier(menuModifierId) {
        const menuModifier = await this.menuModifierService.findOne({
            _id: menuModifierId
        });
        if (!menuModifier)
            throw new common_1.NotFoundException('Not found menu modifier.');
        await this.menuModifierService.delete(menuModifierId);
        return { success: true };
    }
};
POSModifierLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [modifier_service_1.ModifierService,
        menu_modifier_service_1.MenuModifierService])
], POSModifierLogic);
exports.POSModifierLogic = POSModifierLogic;
//# sourceMappingURL=pos-modifier.logic.js.map