import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service';
import { POSCreateModifierDto } from '../dto/create-modifier.dto';
import { POSModifierPaginateDto } from '../dto/pos-get-modifier.dto';
import { ModifierService } from '../services/modifier.service';
export declare class POSModifierLogic {
    private readonly modifierService;
    private readonly menuModifierService;
    constructor(modifierService: ModifierService, menuModifierService: MenuModifierService);
    getListModifier(restaurantId: string, query: POSModifierPaginateDto): Promise<PaginateResult<import("../models/modifier.schema").ModifierDocument>>;
    getModifierById(id: string): Promise<import("../models/modifier.schema").ModifierDocument>;
    createModifier(restaurantId: string, payload: POSCreateModifierDto): Promise<{
        success: boolean;
        id: any;
    }>;
    updateModifier(id: string, payload: POSCreateModifierDto): Promise<{
        success: boolean;
        id: any;
    }>;
    deleteModifier(id: string): Promise<{
        success: boolean;
    }>;
    createMenuModifier(menuId: string, modifierId: string): Promise<{
        success: boolean;
    }>;
    deleteMenuModifier(menuModifierId: string): Promise<{
        success: boolean;
    }>;
}
