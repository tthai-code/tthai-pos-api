"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModifierModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const menu_modifier_module_1 = require("../menu-modifier/menu-modifier.module");
const modifier_controller_1 = require("./controllers/modifier.controller");
const pos_modifier_controller_1 = require("./controllers/pos-modifier.controller");
const modifier_logic_1 = require("./logics/modifier.logic");
const pos_modifier_logic_1 = require("./logics/pos-modifier.logic");
const modifier_schema_1 = require("./models/modifier.schema");
const modifier_service_1 = require("./services/modifier.service");
let ModifierModule = class ModifierModule {
};
ModifierModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => menu_modifier_module_1.MenuModifierModule),
            mongoose_1.MongooseModule.forFeature([{ name: 'modifier', schema: modifier_schema_1.ModifierSchema }])
        ],
        providers: [modifier_service_1.ModifierService, modifier_logic_1.ModifierLogic, pos_modifier_logic_1.POSModifierLogic],
        controllers: [modifier_controller_1.ModifierController, pos_modifier_controller_1.POSModifierController],
        exports: [modifier_service_1.ModifierService]
    })
], ModifierModule);
exports.ModifierModule = ModifierModule;
//# sourceMappingURL=modifier.module.js.map