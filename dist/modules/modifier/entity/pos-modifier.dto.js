"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSModifierResponse = exports.POSModifierPaginateResponse = exports.POSCreateUpdateModifierResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const success_entity_1 = require("../../../common/entity/success.entity");
const modifier_type_enum_1 = require("../common/modifier-type.enum");
class POSCreateUpdateModifier extends success_entity_1.SuccessField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], POSCreateUpdateModifier.prototype, "id", void 0);
class POSCreateUpdateModifierResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: POSCreateUpdateModifier,
        example: {
            success: true,
            id: '63b649273965c0c664bd2d46'
        }
    }),
    __metadata("design:type", POSCreateUpdateModifier)
], POSCreateUpdateModifierResponse.prototype, "data", void 0);
exports.POSCreateUpdateModifierResponse = POSCreateUpdateModifierResponse;
class ModifierItemsObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemsObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemsObject.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierItemsObject.prototype, "price", void 0);
class ModifierObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], ModifierObject.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierObject.prototype, "max_selected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(modifier_type_enum_1.ModifierTypeEnum) }),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierObject.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "id", void 0);
class PaginateModifier extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [ModifierObject],
        example: [
            {
                items: [
                    {
                        name: 'Beef',
                        native_name: 'Beef',
                        price: 2.5
                    },
                    {
                        name: 'Pork',
                        native_name: 'Pork',
                        price: 1
                    },
                    {
                        name: 'Chicken',
                        native_name: 'Chicken',
                        price: 0
                    }
                ],
                max_selected: 1,
                type: 'REQUIRED',
                label: 'Protein Choices',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                abbreviation: 'P',
                position: 999,
                id: '630f464bb75d5da2ea75a5aa'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateModifier.prototype, "results", void 0);
class POSModifierPaginateResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: PaginateModifier }),
    __metadata("design:type", PaginateModifier)
], POSModifierPaginateResponse.prototype, "data", void 0);
exports.POSModifierPaginateResponse = POSModifierPaginateResponse;
class POSModifierResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: ModifierObject,
        example: {
            items: [
                {
                    name: 'Beef',
                    native_name: 'Beef',
                    price: 2.5
                },
                {
                    name: 'Pork',
                    native_name: 'Pork',
                    price: 1
                },
                {
                    name: 'Chicken',
                    native_name: 'Chicken',
                    price: 0
                }
            ],
            max_selected: 1,
            type: 'REQUIRED',
            label: 'Protein Choices',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            abbreviation: 'P',
            position: 999,
            id: '630f464bb75d5da2ea75a5aa'
        }
    }),
    __metadata("design:type", ModifierObject)
], POSModifierResponse.prototype, "data", void 0);
exports.POSModifierResponse = POSModifierResponse;
//# sourceMappingURL=pos-modifier.dto.js.map