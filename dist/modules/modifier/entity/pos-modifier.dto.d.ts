import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
import { SuccessField } from 'src/common/entity/success.entity';
declare class POSCreateUpdateModifier extends SuccessField {
    id: string;
}
export declare class POSCreateUpdateModifierResponse extends ResponseDto<POSCreateUpdateModifier> {
    data: POSCreateUpdateModifier;
}
declare class ModifierItemsObject {
    name: string;
    native_name: string;
    price: number;
}
declare class ModifierObject {
    items: ModifierItemsObject[];
    max_selected: number;
    type: string;
    label: string;
    restaurant_id: string;
    abbreviation: string;
    position: number;
    id: string;
}
declare class PaginateModifier extends PaginateResponseDto<ModifierObject[]> {
    results: ModifierObject[];
}
export declare class POSModifierPaginateResponse extends ResponseDto<PaginateModifier> {
    data: PaginateModifier;
}
export declare class POSModifierResponse extends ResponseDto<ModifierObject> {
    data: ModifierObject;
}
export {};
