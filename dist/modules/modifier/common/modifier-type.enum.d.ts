export declare enum ModifierTypeEnum {
    OPTIONAL = "OPTIONAL",
    REQUIRED = "REQUIRED"
}
