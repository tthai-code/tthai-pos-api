"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModifierTypeEnum = void 0;
var ModifierTypeEnum;
(function (ModifierTypeEnum) {
    ModifierTypeEnum["OPTIONAL"] = "OPTIONAL";
    ModifierTypeEnum["REQUIRED"] = "REQUIRED";
})(ModifierTypeEnum = exports.ModifierTypeEnum || (exports.ModifierTypeEnum = {}));
//# sourceMappingURL=modifier-type.enum.js.map