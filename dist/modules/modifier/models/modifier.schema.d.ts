import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type ModifierDocument = ModifierFields & Document;
export declare class ModifierItemsFields {
    name: string;
    nativeName: string;
    price: number;
}
export declare class ModifierFields {
    restaurantId: Types.ObjectId;
    label: string;
    abbreviation: string;
    type: string;
    maxSelected: number;
    items: Array<ModifierItemsFields>;
    position: number;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const ModifierSchema: MongooseSchema<Document<ModifierFields, any, any>, import("mongoose").Model<Document<ModifierFields, any, any>, any, any, any>, any, any>;
