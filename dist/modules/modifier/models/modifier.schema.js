"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModifierSchema = exports.ModifierFields = exports.ModifierItemsFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const modifier_type_enum_1 = require("../common/modifier-type.enum");
let ModifierItemsFields = class ModifierItemsFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierItemsFields.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierItemsFields.prototype, "nativeName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ModifierItemsFields.prototype, "price", void 0);
ModifierItemsFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: false, _id: false, strict: true })
], ModifierItemsFields);
exports.ModifierItemsFields = ModifierItemsFields;
let ModifierFields = class ModifierFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], ModifierFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierFields.prototype, "label", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], ModifierFields.prototype, "abbreviation", void 0);
__decorate([
    (0, mongoose_1.Prop)({ enum: modifier_type_enum_1.ModifierTypeEnum, default: modifier_type_enum_1.ModifierTypeEnum.OPTIONAL }),
    __metadata("design:type", String)
], ModifierFields.prototype, "type", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ModifierFields.prototype, "maxSelected", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], ModifierFields.prototype, "items", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 999 }),
    __metadata("design:type", Number)
], ModifierFields.prototype, "position", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], ModifierFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], ModifierFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], ModifierFields.prototype, "createdBy", void 0);
ModifierFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'modifiers' })
], ModifierFields);
exports.ModifierFields = ModifierFields;
exports.ModifierSchema = mongoose_1.SchemaFactory.createForClass(ModifierFields);
exports.ModifierSchema.plugin(mongoosePaginate);
exports.ModifierSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=modifier.schema.js.map