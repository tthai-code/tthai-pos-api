import { CreateModifierDto } from '../dto/create-modifier.dto';
import { GetAllModifierDto } from '../dto/get-all-modifier.dto';
import { ModifierPaginateDto } from '../dto/get-modifier.dto';
import { UpdateModifierDto } from '../dto/update-modifier.dto';
import { ModifierLogic } from '../logics/modifier.logic';
import { ModifierService } from '../services/modifier.service';
export declare class ModifierController {
    private readonly modifierService;
    private readonly modifierLogic;
    constructor(modifierService: ModifierService, modifierLogic: ModifierLogic);
    getModifiers(query: ModifierPaginateDto): Promise<PaginateResult<import("../models/modifier.schema").ModifierDocument>>;
    getAllModifiers(query: GetAllModifierDto): Promise<any[]>;
    getModifier(id: string): Promise<import("../models/modifier.schema").ModifierDocument>;
    createModifier(payload: CreateModifierDto): Promise<import("../models/modifier.schema").ModifierDocument>;
    updateModifier(id: string, payload: UpdateModifierDto): Promise<import("../models/modifier.schema").ModifierDocument>;
    deleteModifier(id: string): Promise<import("../models/modifier.schema").ModifierDocument>;
}
