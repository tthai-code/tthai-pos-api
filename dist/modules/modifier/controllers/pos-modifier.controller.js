"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSModifierController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const create_modifier_dto_1 = require("../dto/create-modifier.dto");
const pos_get_modifier_dto_1 = require("../dto/pos-get-modifier.dto");
const pos_modifier_dto_1 = require("../entity/pos-modifier.dto");
const pos_modifier_logic_1 = require("../logics/pos-modifier.logic");
let POSModifierController = class POSModifierController {
    constructor(posModifierLogic) {
        this.posModifierLogic = posModifierLogic;
    }
    async getModifierList(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard === null || staff_auth_guard_1.StaffAuthGuard === void 0 ? void 0 : staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.posModifierLogic.getListModifier(restaurantId, query);
    }
    async getModifier(id) {
        return await this.posModifierLogic.getModifierById(id);
    }
    async createModifier(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard === null || staff_auth_guard_1.StaffAuthGuard === void 0 ? void 0 : staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.posModifierLogic.createModifier(restaurantId, payload);
    }
    async updateModifier(id, payload) {
        return await this.posModifierLogic.updateModifier(id, payload);
    }
    async deleteModifier(id) {
        return await this.posModifierLogic.deleteModifier(id);
    }
    async addModifierToMenu(menuId, modifierId) {
        return await this.posModifierLogic.createMenuModifier(menuId, modifierId);
    }
    async removeMenuModifier(menuModifierId) {
        return await this.posModifierLogic.deleteMenuModifier(menuModifierId);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_modifier_dto_1.POSModifierPaginateResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Paginate Modifier List',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [pos_get_modifier_dto_1.POSModifierPaginateDto]),
    __metadata("design:returntype", Promise)
], POSModifierController.prototype, "getModifierList", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_modifier_dto_1.POSModifierResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Modifier By ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSModifierController.prototype, "getModifier", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => pos_modifier_dto_1.POSCreateUpdateModifierResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create a Modifier',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_modifier_dto_1.POSCreateModifierDto]),
    __metadata("design:returntype", Promise)
], POSModifierController.prototype, "createModifier", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_modifier_dto_1.POSCreateUpdateModifierResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Update a Modifier by ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, create_modifier_dto_1.POSCreateModifierDto]),
    __metadata("design:returntype", Promise)
], POSModifierController.prototype, "updateModifier", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete Modifier by ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSModifierController.prototype, "deleteModifier", null);
__decorate([
    (0, common_1.Post)(':modifierId/menu/:menuId'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Add Modifier to Menu by ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('menuId')),
    __param(1, (0, common_1.Param)('modifierId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], POSModifierController.prototype, "addModifierToMenu", null);
__decorate([
    (0, common_1.Delete)(':menuModifierId/menu'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Remove Modifier from Menu by menuModifierId',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('menuModifierId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSModifierController.prototype, "removeMenuModifier", null);
POSModifierController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/modifier'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/modifier'),
    __metadata("design:paramtypes", [pos_modifier_logic_1.POSModifierLogic])
], POSModifierController);
exports.POSModifierController = POSModifierController;
//# sourceMappingURL=pos-modifier.controller.js.map