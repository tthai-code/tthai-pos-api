import { POSCreateModifierDto } from '../dto/create-modifier.dto';
import { POSModifierPaginateDto } from '../dto/pos-get-modifier.dto';
import { POSModifierLogic } from '../logics/pos-modifier.logic';
export declare class POSModifierController {
    private readonly posModifierLogic;
    constructor(posModifierLogic: POSModifierLogic);
    getModifierList(query: POSModifierPaginateDto): Promise<PaginateResult<import("../models/modifier.schema").ModifierDocument>>;
    getModifier(id: string): Promise<import("../models/modifier.schema").ModifierDocument>;
    createModifier(payload: POSCreateModifierDto): Promise<{
        success: boolean;
        id: any;
    }>;
    updateModifier(id: string, payload: POSCreateModifierDto): Promise<{
        success: boolean;
        id: any;
    }>;
    deleteModifier(id: string): Promise<{
        success: boolean;
    }>;
    addModifierToMenu(menuId: string, modifierId: string): Promise<{
        success: boolean;
    }>;
    removeMenuModifier(menuModifierId: string): Promise<{
        success: boolean;
    }>;
}
