"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModifierController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const menu_modifier_entity_1 = require("../../menu-modifier/entitiy/menu-modifier.entity");
const create_modifier_dto_1 = require("../dto/create-modifier.dto");
const get_all_modifier_dto_1 = require("../dto/get-all-modifier.dto");
const get_modifier_dto_1 = require("../dto/get-modifier.dto");
const update_modifier_dto_1 = require("../dto/update-modifier.dto");
const modifier_logic_1 = require("../logics/modifier.logic");
const modifier_service_1 = require("../services/modifier.service");
let ModifierController = class ModifierController {
    constructor(modifierService, modifierLogic) {
        this.modifierService = modifierService;
        this.modifierLogic = modifierLogic;
    }
    async getModifiers(query) {
        var _a;
        const restaurantId = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant.id;
        return await this.modifierService.paginate(query.buildQuery(restaurantId), query);
    }
    async getAllModifiers(query) {
        var _a;
        const restaurantId = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant.id;
        return await this.modifierService.getAll(query.buildQuery(restaurantId), {
            label: 1,
            abbreviation: 1,
            items: 1,
            type: 1,
            maxSelected: 1
        });
    }
    async getModifier(id) {
        return await this.modifierService.findOne({ _id: id });
    }
    async createModifier(payload) {
        return await this.modifierLogic.createModifier(payload);
    }
    async updateModifier(id, payload) {
        return await this.modifierLogic.updateModifier(id, payload);
    }
    async deleteModifier(id) {
        return await this.modifierService.delete(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Get Modifier List' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_modifier_dto_1.ModifierPaginateDto]),
    __metadata("design:returntype", Promise)
], ModifierController.prototype, "getModifiers", null);
__decorate([
    (0, common_1.Get)('all'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_modifier_entity_1.GetAllMenuModifierResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get All Modifier List' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_all_modifier_dto_1.GetAllModifierDto]),
    __metadata("design:returntype", Promise)
], ModifierController.prototype, "getAllModifiers", null);
__decorate([
    (0, common_1.Get)(':id/modifier'),
    (0, swagger_1.ApiOperation)({ summary: 'Get a Modifier' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ModifierController.prototype, "getModifier", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create Modifier' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_modifier_dto_1.CreateModifierDto]),
    __metadata("design:returntype", Promise)
], ModifierController.prototype, "createModifier", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Modifier' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_modifier_dto_1.UpdateModifierDto]),
    __metadata("design:returntype", Promise)
], ModifierController.prototype, "updateModifier", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete Modifier' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ModifierController.prototype, "deleteModifier", null);
ModifierController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('modifier'),
    (0, common_1.Controller)('v1/modifier'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __metadata("design:paramtypes", [modifier_service_1.ModifierService,
        modifier_logic_1.ModifierLogic])
], ModifierController);
exports.ModifierController = ModifierController;
//# sourceMappingURL=modifier.controller.js.map