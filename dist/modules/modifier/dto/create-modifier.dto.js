"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSCreateModifierDto = exports.CreateModifierDto = exports.ModifierItemsDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const status_enum_1 = require("../../../common/enum/status.enum");
const modifier_type_enum_1 = require("../common/modifier-type.enum");
class ModifierItemsDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Beef' }),
    __metadata("design:type", String)
], ModifierItemsDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Beef' }),
    __metadata("design:type", String)
], ModifierItemsDto.prototype, "nativeName", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 2.5 }),
    __metadata("design:type", Number)
], ModifierItemsDto.prototype, "price", void 0);
exports.ModifierItemsDto = ModifierItemsDto;
class CreateModifierDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '<restaurant-id>' }),
    __metadata("design:type", String)
], CreateModifierDto.prototype, "restaurantId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Protein Choices' }),
    __metadata("design:type", String)
], CreateModifierDto.prototype, "label", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'P' }),
    __metadata("design:type", String)
], CreateModifierDto.prototype, "abbreviation", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(modifier_type_enum_1.ModifierTypeEnum),
    (0, swagger_1.ApiProperty)({ example: modifier_type_enum_1.ModifierTypeEnum.REQUIRED, enum: modifier_type_enum_1.ModifierTypeEnum }),
    __metadata("design:type", String)
], CreateModifierDto.prototype, "type", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 1 }),
    __metadata("design:type", Number)
], CreateModifierDto.prototype, "maxSelected", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => ModifierItemsDto),
    (0, class_validator_1.ArrayNotEmpty)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => [ModifierItemsDto] }),
    __metadata("design:type", Array)
], CreateModifierDto.prototype, "items", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 1 }),
    __metadata("design:type", Number)
], CreateModifierDto.prototype, "position", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(status_enum_1.StatusEnum),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: status_enum_1.StatusEnum.ACTIVE, enum: status_enum_1.StatusEnum }),
    __metadata("design:type", String)
], CreateModifierDto.prototype, "status", void 0);
exports.CreateModifierDto = CreateModifierDto;
class POSCreateModifierDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Protein Choices' }),
    __metadata("design:type", String)
], POSCreateModifierDto.prototype, "label", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'P' }),
    __metadata("design:type", String)
], POSCreateModifierDto.prototype, "abbreviation", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(modifier_type_enum_1.ModifierTypeEnum),
    (0, swagger_1.ApiProperty)({ example: modifier_type_enum_1.ModifierTypeEnum.REQUIRED, enum: modifier_type_enum_1.ModifierTypeEnum }),
    __metadata("design:type", String)
], POSCreateModifierDto.prototype, "type", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 1 }),
    __metadata("design:type", Number)
], POSCreateModifierDto.prototype, "maxSelected", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => ModifierItemsDto),
    (0, class_validator_1.ArrayNotEmpty)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => [ModifierItemsDto] }),
    __metadata("design:type", Array)
], POSCreateModifierDto.prototype, "items", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: 1,
        default: 999,
        description: 'use to sort modifier position for show'
    }),
    __metadata("design:type", Number)
], POSCreateModifierDto.prototype, "position", void 0);
exports.POSCreateModifierDto = POSCreateModifierDto;
//# sourceMappingURL=create-modifier.dto.js.map