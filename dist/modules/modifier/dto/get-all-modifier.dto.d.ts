import { StatusEnum } from 'src/common/enum/status.enum';
export declare class GetAllModifierDto {
    readonly restaurant: string;
    buildQuery(id: string): {
        restaurantId: {
            $eq: string;
        };
        status: StatusEnum;
    };
}
