import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class POSModifierPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    buildQuery(id: string): {
        $or: {
            label: {
                $regex: string;
                $options: string;
            };
            'items.name': {
                $regex: string;
                $options: string;
            };
        }[];
        restaurantId: string;
        status: StatusEnum;
    };
}
