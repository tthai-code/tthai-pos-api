export declare class ModifierItemsDto {
    readonly name: string;
    readonly nativeName: string;
    readonly price: number;
}
export declare class UpdateModifierDto {
    readonly label: string;
    readonly abbreviation: string;
    readonly type: string;
    readonly maxSelected: number;
    readonly items: Array<ModifierItemsDto>;
    readonly position: number;
    readonly status: string;
}
