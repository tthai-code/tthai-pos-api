"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const admin_stategy_1 = require("./passport/admin-stategy");
const passport_1 = require("@nestjs/passport");
const user_module_1 = require("../user/user.module");
const auth_logic_1 = require("./logics/auth.logic");
const auth_1 = require("./constants/auth");
const member_stategy_1 = require("./passport/member-stategy");
const basic_strategy_1 = require("./passport/basic-strategy");
const restaurant_account_module_1 = require("../restaurant-account/restaurant-account.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const account_strategy_1 = require("./passport/account-strategy");
const staff_module_1 = require("../staff/staff.module");
const device_module_1 = require("../device/device.module");
const device_strategy_1 = require("./passport/device-strategy");
const staff_strategy_1 = require("./passport/staff-strategy");
const super_admin_strategy_1 = require("./passport/super-admin.strategy");
const auth_header_api_key_strategy_1 = require("./passport/auth-header-api-key.strategy");
const config_1 = require("@nestjs/config");
const customer_strategy_1 = require("./passport/customer-strategy");
const customer_module_1 = require("../customer/customer.module");
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            common_1.HttpModule,
            passport_1.PassportModule,
            config_1.ConfigModule,
            user_module_1.UserModule,
            restaurant_account_module_1.RestaurantAccountModule,
            restaurant_module_1.RestaurantModule,
            device_module_1.DeviceModule,
            staff_module_1.StaffModule,
            customer_module_1.CustomerModule,
            jwt_1.JwtModule.register({
                secret: auth_1.jwtConstants.secretAdmin,
                signOptions: {
                    expiresIn: '7d'
                }
            })
        ],
        controllers: [],
        providers: [
            admin_stategy_1.AdminStrategy,
            member_stategy_1.MemberStrategy,
            auth_logic_1.AuthLogic,
            basic_strategy_1.BasicStrategy,
            account_strategy_1.AccountStrategy,
            device_strategy_1.DeviceStrategy,
            staff_strategy_1.StaffStrategy,
            super_admin_strategy_1.SuperAdminStrategy,
            auth_header_api_key_strategy_1.HeaderApiKeyStrategy,
            customer_strategy_1.CustomerStrategy
        ],
        exports: [auth_logic_1.AuthLogic]
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map