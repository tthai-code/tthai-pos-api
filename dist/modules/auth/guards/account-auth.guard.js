"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AccountAuthGuard_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountAuthGuard = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const core_1 = require("@nestjs/core");
let AccountAuthGuard = AccountAuthGuard_1 = class AccountAuthGuard extends (0, passport_1.AuthGuard)('account-auth') {
    constructor(reflector) {
        super();
        this.reflector = reflector;
        this.user = {
            id: 0,
            username: 'system'
        };
        AccountAuthGuard_1._this = this;
    }
    canActivate(context) {
        const request = context.switchToHttp().getRequest();
        const roles = this.reflector.get('roles', context.getHandler());
        AccountAuthGuard_1.setRequest(request);
        this.guardRole = roles;
        return super.canActivate(context);
    }
    handleRequest(err, user, info) {
        if (err || !user) {
            throw err || new common_1.UnauthorizedException();
        }
        this.setAuthorizedUser(user);
        if (!this.guardRole) {
            return user;
        }
        const hasRole = !!this.guardRole.some((item) => item === user.role);
        if (!hasRole) {
            throw new common_1.UnauthorizedException(info);
        }
        return user;
    }
    static getTokenFromRequest() {
        return AccountAuthGuard_1._this.request.headers.authorization;
    }
    static setRequest(request) {
        return (AccountAuthGuard_1._this.request = request);
    }
    static getRequest() {
        return AccountAuthGuard_1._this.request;
    }
    static getParamsFromRequest() {
        return AccountAuthGuard_1._this.request.params;
    }
    static getAuthorizedUser() {
        return AccountAuthGuard_1._this.user;
    }
    setAuthorizedUser(user) {
        AccountAuthGuard_1._this.user = user;
    }
};
AccountAuthGuard = AccountAuthGuard_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __metadata("design:paramtypes", [core_1.Reflector])
], AccountAuthGuard);
exports.AccountAuthGuard = AccountAuthGuard;
//# sourceMappingURL=account-auth.guard.js.map