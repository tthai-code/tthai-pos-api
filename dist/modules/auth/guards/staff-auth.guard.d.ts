/// <reference types="qs" />
import { ExecutionContext } from '@nestjs/common';
import { Request } from 'express';
import { Reflector } from '@nestjs/core';
declare const StaffAuthGuard_base: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;
export declare class StaffAuthGuard extends StaffAuthGuard_base {
    private readonly reflector;
    private static _this;
    private guardRole;
    private request;
    private user;
    constructor(reflector: Reflector);
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | import("rxjs").Observable<boolean>;
    handleRequest(err: any, user: any, info: any): any;
    static getTokenFromRequest(): string;
    static setRequest(request: Request): Request<import("express-serve-static-core").ParamsDictionary, any, any, import("qs").ParsedQs, Record<string, any>>;
    static getRequest(): any;
    static getParamsFromRequest(): import("express-serve-static-core").ParamsDictionary;
    static getAuthorizedUser(): any;
    private setAuthorizedUser;
}
export {};
