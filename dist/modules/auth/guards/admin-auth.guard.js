"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var AdminAuthGuard_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminAuthGuard = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const core_1 = require("@nestjs/core");
let AdminAuthGuard = AdminAuthGuard_1 = class AdminAuthGuard extends (0, passport_1.AuthGuard)('admin-auth') {
    constructor(reflector) {
        super();
        this.reflector = reflector;
        this.user = {
            id: 0,
            username: 'system'
        };
        AdminAuthGuard_1._this = this;
    }
    canActivate(context) {
        const request = context.switchToHttp().getRequest();
        const roles = this.reflector.get('roles', context.getHandler());
        AdminAuthGuard_1.setRequest(request);
        this.guardRole = roles;
        return super.canActivate(context);
    }
    handleRequest(err, user, info) {
        if (err || !user) {
            throw err || new common_1.UnauthorizedException();
        }
        this.setAuthorizedUser(user);
        if (!this.guardRole) {
            return user;
        }
        const hasRole = !!this.guardRole.some((item) => item === user.role);
        if (!hasRole) {
            throw new common_1.UnauthorizedException(info);
        }
        return user;
    }
    static getTokenFromRequest() {
        return AdminAuthGuard_1._this.request.headers.authorization;
    }
    static setRequest(request) {
        return (AdminAuthGuard_1._this.request = request);
    }
    static getRequest() {
        return AdminAuthGuard_1._this.request;
    }
    static getParamsFromRequest() {
        return AdminAuthGuard_1._this.request.params;
    }
    static getAuthorizedUser() {
        return AdminAuthGuard_1._this.user;
    }
    setAuthorizedUser(user) {
        AdminAuthGuard_1._this.user = user;
    }
};
AdminAuthGuard = AdminAuthGuard_1 = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __metadata("design:paramtypes", [core_1.Reflector])
], AdminAuthGuard);
exports.AdminAuthGuard = AdminAuthGuard;
//# sourceMappingURL=admin-auth.guard.js.map