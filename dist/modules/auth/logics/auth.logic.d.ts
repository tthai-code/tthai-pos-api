/// <reference types="mongoose" />
import { UserService } from 'src/modules/user/services/user.service';
import { LoginDto } from '../../user/dto/login.dto';
import { JwtService } from '@nestjs/jwt';
import { RestaurantAccountService } from 'src/modules/restaurant-account/services/restaurant-account.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { StaffLoginDto } from 'src/modules/staff/dto/staff-login.dto';
import { CustomerLoginDto, CustomerSocialLoginDto } from 'src/modules/customer/dto/customer-login.dto';
import { StaffService } from 'src/modules/staff/services/staff.service';
import { DeviceService } from 'src/modules/device/services/device.service';
import { CustomerService } from 'src/modules/customer/services/customer.service';
export declare class AuthLogic {
    private readonly userService;
    private readonly accountService;
    private readonly restaurantService;
    private readonly staffService;
    private readonly deviceService;
    private readonly customerService;
    private readonly jwtService;
    constructor(userService: UserService, accountService: RestaurantAccountService, restaurantService: RestaurantService, staffService: StaffService, deviceService: DeviceService, customerService: CustomerService, jwtService: JwtService);
    jwtSignToken(payload: any, expiresIn: string, secret: string): string;
    loginLogic(login: LoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        role: string;
    }>;
    RestaurantLoginLogic(login: LoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        ownerName: string;
        restaurant: import("../../restaurant/schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
    RestaurantLoginAfterCreated(username: string): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        ownerName: string;
        restaurant: import("../../restaurant/schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
    StaffLogin(restaurantId: string, login: StaffLoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        role: string;
        ownerName: string;
        restaurant: import("../../restaurant/schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
    ActivateDeviceCode(code: string): Promise<{
        accessToken: string;
        restaurant: {
            id: any;
        };
    }>;
    DirectAdminActivateDeviceCode(restaurantId: string): Promise<{
        accessToken: string;
        restaurant: {
            id: any;
        };
    }>;
    CustomerLogin(login: CustomerLoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        address: import("../../customer/schemas/customer.schema").AddressFields;
        uidSocial: string;
        loginType: string;
        tel: string;
        email: string;
        lastName: string;
        firstName: string;
        restaurantId: import("mongoose").Types.ObjectId;
        id: any;
    }>;
    CustomerSocialLogin(login: CustomerSocialLoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        address: import("../../customer/schemas/customer.schema").AddressFields;
        uidSocial: string;
        loginType: string;
        tel: string;
        email: string;
        lastName: string;
        firstName: string;
        restaurantId: import("mongoose").Types.ObjectId;
        id: any;
    }>;
    DirectAdminLoginLogic(restaurantId: string): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        ownerName: string;
        restaurant: import("../../restaurant/schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
}
