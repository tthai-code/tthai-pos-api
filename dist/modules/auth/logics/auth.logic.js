"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthLogic = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("../../user/services/user.service");
const jwt_1 = require("@nestjs/jwt");
const bcrypt = require("bcryptjs");
const auth_1 = require("../constants/auth");
const restaurant_account_service_1 = require("../../restaurant-account/services/restaurant-account.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const status_enum_1 = require("../../../common/enum/status.enum");
const staff_login_dto_1 = require("../../staff/dto/staff-login.dto");
const customer_login_dto_1 = require("../../customer/dto/customer-login.dto");
const staff_service_1 = require("../../staff/services/staff.service");
const device_service_1 = require("../../device/services/device.service");
const customer_service_1 = require("../../customer/services/customer.service");
const customer_type_enum_1 = require("../../customer/common/enum/customer-type.enum");
let AuthLogic = class AuthLogic {
    constructor(userService, accountService, restaurantService, staffService, deviceService, customerService, jwtService) {
        this.userService = userService;
        this.accountService = accountService;
        this.restaurantService = restaurantService;
        this.staffService = staffService;
        this.deviceService = deviceService;
        this.customerService = customerService;
        this.jwtService = jwtService;
    }
    jwtSignToken(payload, expiresIn, secret) {
        return this.jwtService.sign(payload, {
            expiresIn,
            secret
        });
    }
    async loginLogic(login) {
        const userData = await this.userService.findOneWithPassword({
            username: login.username
        });
        const now = new Date();
        if (!userData) {
            throw new common_1.UnauthorizedException();
        }
        const validateUser = await bcrypt.compare(login.password, userData.password);
        if (!validateUser) {
            throw new common_1.UnauthorizedException();
        }
        const payload = {
            username: userData.username,
            id: userData.id,
            role: userData.role
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretSuperAdmin
        });
        return Object.assign(Object.assign({}, payload), { accessToken, tokenExpire: now.setDate(now.getDate() + 7) });
    }
    async RestaurantLoginLogic(login) {
        const accountData = await this.accountService.findOneWithPassword({
            username: login.username
        });
        const now = new Date();
        if (!accountData) {
            throw new common_1.UnauthorizedException();
        }
        const restaurant = await this.restaurantService
            .getModel()
            .findOne({
            accountId: accountData._id,
            status: status_enum_1.StatusEnum.ACTIVE
        })
            .select({
            _id: 0,
            id: '$_id',
            legalBusinessName: 1,
            address: 1,
            businessPhone: 1,
            email: 1,
            taxRate: 1,
            logoUrl: 1,
            printerSetting: 1,
            authorizedPersonFirstName: 1,
            authorizedPersonLastName: 1,
            defaultServiceCharge: 1,
            timeZone: 1
        });
        const validateUser = await bcrypt.compare(login.password, accountData.password);
        if (!validateUser) {
            throw new common_1.UnauthorizedException();
        }
        const payload = {
            username: accountData.username,
            id: accountData.id,
            ownerName: restaurant &&
                restaurant.authorizedPersonFirstName &&
                restaurant.authorizedPersonLastName
                ? `${restaurant.authorizedPersonFirstName} ${restaurant.authorizedPersonLastName}`
                : 'User Admin',
            restaurant: restaurant ? restaurant : null
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretAdmin
        });
        return Object.assign(Object.assign({}, payload), { accessToken, tokenExpire: now.setDate(now.getDate() + 7) });
    }
    async RestaurantLoginAfterCreated(username) {
        const accountData = await this.accountService.findOneWithPassword({
            username
        });
        const now = new Date();
        if (!accountData) {
            throw new common_1.UnauthorizedException();
        }
        const restaurant = await this.restaurantService
            .getModel()
            .findOne({
            accountId: accountData._id,
            status: status_enum_1.StatusEnum.ACTIVE
        })
            .select({
            _id: 0,
            id: '$_id',
            legalBusinessName: 1,
            address: 1,
            businessPhone: 1,
            email: 1,
            taxRate: 1,
            logoUrl: 1,
            printerSetting: 1,
            authorizedPersonFirstName: 1,
            authorizedPersonLastName: 1,
            defaultServiceCharge: 1,
            timeZone: 1
        });
        const payload = {
            username: accountData.username,
            id: accountData.id,
            ownerName: restaurant &&
                restaurant.authorizedPersonFirstName &&
                restaurant.authorizedPersonLastName
                ? `${restaurant.authorizedPersonFirstName} ${restaurant.authorizedPersonLastName}`
                : 'User Admin',
            restaurant: restaurant ? restaurant : null
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretAdmin
        });
        return Object.assign(Object.assign({}, payload), { accessToken, tokenExpire: now.setDate(now.getDate() + 7) });
    }
    async StaffLogin(restaurantId, login) {
        const staff = await this.staffService.getModel().findOne({
            restaurantId,
            passCode: login.passCode,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, passCode: 0 });
        if (!staff)
            throw new common_1.UnauthorizedException();
        const now = new Date();
        const restaurant = await this.restaurantService
            .getModel()
            .findOne({
            _id: staff.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        })
            .select({
            _id: 0,
            id: '$_id',
            legalBusinessName: 1,
            dba: 1,
            address: 1,
            businessPhone: 1,
            email: 1,
            taxRate: 1,
            logoUrl: 1,
            authorizedPersonFirstName: 1,
            authorizedPersonLastName: 1,
            defaultServiceCharge: 1,
            timeZone: 1
        });
        const payload = {
            username: staff.firstName,
            id: staff.id,
            role: staff.role,
            ownerName: restaurant &&
                restaurant.authorizedPersonFirstName &&
                restaurant.authorizedPersonLastName
                ? `${restaurant.authorizedPersonFirstName} ${restaurant.authorizedPersonLastName}`
                : 'User Admin',
            restaurant: restaurant ? restaurant : null
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretStaff
        });
        return Object.assign(Object.assign({}, payload), { accessToken, tokenExpire: now.setDate(now.getDate() + 7) });
    }
    async ActivateDeviceCode(code) {
        const device = await this.deviceService.findOne({
            activationCode: code,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!device)
            throw new common_1.NotFoundException('Not found activation code.');
        if (device.isActivated) {
            throw new common_1.BadRequestException('This activation code is already used.');
        }
        const restaurant = await this.restaurantService.findOne({
            _id: device.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.BadRequestException();
        const activated = await this.deviceService.update(device._id, {
            isActivated: true
        });
        if (!activated)
            throw new common_1.BadRequestException();
        const payload = {
            restaurant: {
                id: restaurant._id
            }
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretDevice
        });
        return Object.assign(Object.assign({}, payload), { accessToken });
    }
    async DirectAdminActivateDeviceCode(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.BadRequestException();
        const payload = {
            restaurant: {
                id: restaurant._id
            }
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretDevice
        });
        return Object.assign(Object.assign({}, payload), { accessToken });
    }
    async CustomerLogin(login) {
        const customer = await this.customerService.findOne({
            email: login.email,
            restaurantId: login.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE,
            loginType: customer_type_enum_1.CustomerTypeEnum.NORMAL
        });
        if (!customer)
            throw new common_1.UnauthorizedException('User is not exist.');
        const validateUser = await bcrypt.compare(login.password, customer.password);
        if (!validateUser) {
            throw new common_1.UnauthorizedException('Your account or password is incorrect.');
        }
        const now = new Date();
        const payload = {
            address: customer.address,
            uidSocial: customer.uidSocial,
            loginType: customer.loginType,
            tel: customer.tel,
            email: customer.email,
            lastName: customer.lastName,
            firstName: customer.firstName,
            restaurantId: customer.restaurantId,
            id: customer.id
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretCustomer
        });
        return Object.assign(Object.assign({}, payload), { accessToken, tokenExpire: now.setDate(now.getDate() + 7) });
    }
    async CustomerSocialLogin(login) {
        const customer = await this.customerService.findOne({
            restaurantId: login.restaurantId,
            uidSocial: login.uidSocial,
            loginType: login.loginType,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!customer) {
            throw new common_1.UnauthorizedException(`Can't find your account`);
        }
        const now = new Date();
        const payload = {
            address: customer.address,
            uidSocial: customer.uidSocial,
            loginType: customer.loginType,
            tel: customer.tel,
            email: customer.email,
            lastName: customer.lastName,
            firstName: customer.firstName,
            restaurantId: customer.restaurantId,
            id: customer.id
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretCustomer
        });
        return Object.assign(Object.assign({}, payload), { accessToken, tokenExpire: now.setDate(now.getDate() + 7) });
    }
    async DirectAdminLoginLogic(restaurantId) {
        const now = new Date();
        const restaurant = await this.restaurantService
            .getModel()
            .findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        })
            .select({
            _id: 0,
            id: '$_id',
            legalBusinessName: 1,
            address: 1,
            businessPhone: 1,
            email: 1,
            taxRate: 1,
            logoUrl: 1,
            printerSetting: 1,
            authorizedPersonFirstName: 1,
            authorizedPersonLastName: 1,
            defaultServiceCharge: 1,
            accountId: 1,
            timeZone: 1
        });
        const accountData = await this.accountService.findOneWithPassword({
            _id: restaurant.accountId
        });
        delete restaurant.accountId;
        const payload = {
            username: accountData.username,
            id: accountData.id,
            ownerName: restaurant &&
                restaurant.authorizedPersonFirstName &&
                restaurant.authorizedPersonLastName
                ? `${restaurant.authorizedPersonFirstName} ${restaurant.authorizedPersonLastName}`
                : 'User Admin',
            restaurant: restaurant ? restaurant : null
        };
        const accessToken = this.jwtService.sign(payload, {
            expiresIn: '7d',
            secret: auth_1.jwtConstants.secretAdmin
        });
        return Object.assign(Object.assign({}, payload), { accessToken, tokenExpire: now.setDate(now.getDate() + 7) });
    }
};
AuthLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService,
        restaurant_account_service_1.RestaurantAccountService,
        restaurant_service_1.RestaurantService,
        staff_service_1.StaffService,
        device_service_1.DeviceService,
        customer_service_1.CustomerService,
        jwt_1.JwtService])
], AuthLogic);
exports.AuthLogic = AuthLogic;
//# sourceMappingURL=auth.logic.js.map