declare const StaffStrategy_base: new (...args: any[]) => any;
export declare class StaffStrategy extends StaffStrategy_base {
    private readonly routeMapping;
    constructor();
    validate({ params }: {
        params: any;
    }, payload: any): Promise<any>;
    protected assignRouteParams(params: object): Promise<void>;
}
export {};
