"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerStrategy = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const passport_jwt_1 = require("passport-jwt");
const auth_1 = require("../constants/auth");
let CustomerStrategy = class CustomerStrategy extends (0, passport_1.PassportStrategy)(passport_jwt_1.Strategy, 'customer-auth') {
    constructor() {
        super({
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: auth_1.jwtConstants.secretCustomer,
            passReqToCallback: true
        });
    }
    async validate({ params }, payload) {
        const userId = params.userId || params.user;
        if (!payload.hasOwnProperty('id') &&
            userId &&
            payload.id !== Number(userId)) {
            throw new common_1.ForbiddenException();
        }
        return payload;
    }
    async assignRouteParams(params) {
        const paramsKeys = Object.keys(params);
        const filterKeys = paramsKeys.filter((key) => this.routeMapping.hasOwnProperty(key));
        const mappingPromises = filterKeys.map((key) => this.routeMapping[key].resolveByUrl(params));
        const promiseResults = await Promise.all(mappingPromises);
        if (promiseResults.some((doc) => !doc)) {
            throw new common_1.NotFoundException();
        }
        filterKeys.forEach((key, index) => {
            params[key] = promiseResults[index];
        });
    }
};
CustomerStrategy = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], CustomerStrategy);
exports.CustomerStrategy = CustomerStrategy;
//# sourceMappingURL=customer-strategy.js.map