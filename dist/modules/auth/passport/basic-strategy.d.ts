import { BasicStrategy as Strategy } from 'passport-http';
declare const BasicStrategy_base: new (...args: any[]) => Strategy;
export declare class BasicStrategy extends BasicStrategy_base {
    constructor();
    validate: (_: any, username: string, password: string) => Promise<boolean>;
}
export {};
