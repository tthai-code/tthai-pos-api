declare const CustomerStrategy_base: new (...args: any[]) => any;
export declare class CustomerStrategy extends CustomerStrategy_base {
    private readonly routeMapping;
    constructor();
    validate({ params }: {
        params: any;
    }, payload: any): Promise<any>;
    protected assignRouteParams(params: object): Promise<void>;
}
export {};
