declare const DeviceStrategy_base: new (...args: any[]) => any;
export declare class DeviceStrategy extends DeviceStrategy_base {
    private readonly routeMapping;
    constructor();
    validate({ params }: {
        params: any;
    }, payload: any): Promise<any>;
    protected assignRouteParams(params: object): Promise<void>;
}
export {};
