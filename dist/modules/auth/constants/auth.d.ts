export declare const jwtConstants: {
    secretAdmin: string;
    secretSuperAdmin: string;
    secretMember: string;
    secretAccount: string;
    secretDevice: string;
    secretStaff: string;
    secretCustomer: string;
};
