import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
export declare class PackagePaginate extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    buildQuery(): {
        $or: {
            name: {
                $regex: string;
                $options: string;
            };
        }[];
        status: StatusEnum;
    };
}
