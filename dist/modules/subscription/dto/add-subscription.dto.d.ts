export declare class AddSubscriptionDto {
    packageId: string;
    quantity: number;
    paymentPlan: string;
}
