export declare class EditSubscriptionDto {
    price: number;
    amount: number;
    quantity: number;
    paymentPlan: string;
}
