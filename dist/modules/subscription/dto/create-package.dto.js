"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePackageDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const package_type_enum_1 = require("../common/package-type.enum");
class CreatePackageDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'More iPad' }),
    __metadata("design:type", String)
], CreatePackageDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: 'เพิ่ม iPad 1 เครื่อง ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)'
    }),
    __metadata("design:type", String)
], CreatePackageDto.prototype, "description", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 20 }),
    __metadata("design:type", Number)
], CreatePackageDto.prototype, "monthlyPrice", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 200 }),
    __metadata("design:type", Number)
], CreatePackageDto.prototype, "yearlyPrice", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)({ example: false }),
    __metadata("design:type", Boolean)
], CreatePackageDto.prototype, "isActiveDefault", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: 1,
        description: 'required when packageType has `IPAD`'
    }),
    __metadata("design:type", Number)
], CreatePackageDto.prototype, "iPadCapacity", void 0);
__decorate([
    (0, class_validator_1.IsArray)(),
    (0, class_validator_1.IsIn)(Object.values(package_type_enum_1.PackageTypeEnum), { each: true }),
    (0, class_validator_1.IsString)({ each: true }),
    (0, swagger_1.ApiProperty)({
        example: [package_type_enum_1.PackageTypeEnum.IPAD],
        description: `each value must be one in \`${Object.values(package_type_enum_1.PackageTypeEnum)}\``
    }),
    __metadata("design:type", Array)
], CreatePackageDto.prototype, "packageType", void 0);
exports.CreatePackageDto = CreatePackageDto;
//# sourceMappingURL=create-package.dto.js.map