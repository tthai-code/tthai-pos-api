"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionPaginateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
const status_enum_1 = require("../../../common/enum/status.enum");
class SubscriptionPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'createdAt';
        this.sortOrder = 'desc';
        this.search = '';
    }
    buildQuery(restaurantId) {
        const result = {
            $or: [
                {
                    name: {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], SubscriptionPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], SubscriptionPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'iPad' }),
    __metadata("design:type", String)
], SubscriptionPaginateDto.prototype, "search", void 0);
exports.SubscriptionPaginateDto = SubscriptionPaginateDto;
//# sourceMappingURL=get-subscription.dto.js.map