export declare class CreatePackageDto {
    isFree: boolean;
    readonly name: string;
    readonly description: string;
    readonly monthlyPrice: number;
    readonly yearlyPrice: number;
    readonly isActiveDefault: boolean;
    readonly iPadCapacity: number;
    readonly packageType: string[];
}
