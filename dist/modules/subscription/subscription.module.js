"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const payment_gateway_module_1 = require("../payment-gateway/payment-gateway.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const admin_package_controller_1 = require("./controllers/admin-package.controller");
const admin_subscription_controller_1 = require("./controllers/admin-subscription.controller");
const package_controller_1 = require("./controllers/package.controller");
const subscription_controller_1 = require("./controllers/subscription.controller");
const admin_subscription_logic_1 = require("./logic/admin-subscription.logic");
const package_logic_1 = require("./logic/package.logic");
const subscription_logic_1 = require("./logic/subscription.logic");
const package_schema_1 = require("./schemas/package.schema");
const subscription_transaction_schema_1 = require("./schemas/subscription-transaction.schema");
const subscription_schema_1 = require("./schemas/subscription.schema");
const package_service_1 = require("./services/package.service");
const subscription_transaction_service_1 = require("./services/subscription-transaction.service");
const subscription_service_1 = require("./services/subscription.service");
let SubscriptionModule = class SubscriptionModule {
};
SubscriptionModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            (0, common_1.forwardRef)(() => payment_gateway_module_1.PaymentGatewayModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'package', schema: package_schema_1.PackageSchema },
                { name: 'subscription', schema: subscription_schema_1.SubscriptionSchema },
                { name: 'subscriptionTransaction', schema: subscription_transaction_schema_1.SubscriptionTransactionSchema }
            ])
        ],
        providers: [
            package_service_1.PackageService,
            subscription_service_1.SubscriptionService,
            subscription_logic_1.SubscriptionLogic,
            admin_subscription_logic_1.AdminSubscriptionLogic,
            subscription_transaction_service_1.SubscriptionTransactionService,
            package_logic_1.PackageLogic
        ],
        controllers: [
            admin_package_controller_1.AdminPackageController,
            subscription_controller_1.SubscriptionController,
            admin_subscription_controller_1.AdminSubscriptionController,
            package_controller_1.PackageController
        ],
        exports: [subscription_service_1.SubscriptionService, package_service_1.PackageService, subscription_logic_1.SubscriptionLogic]
    })
], SubscriptionModule);
exports.SubscriptionModule = SubscriptionModule;
//# sourceMappingURL=subscription.module.js.map