"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionResponse = exports.GetSubscriptionListResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
class SubscriptionObject extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: [
            {
                status: 'active',
                auto_renew_at: '2022-12-31T17:00:00.000Z',
                last_renew_at: null,
                subscribed_at: '2022-12-02T13:16:42.926Z',
                subscription_status: 'ACTIVE',
                price: 20,
                quantity: 1,
                payment_plan: 'MONTHLY',
                name: 'More iPad',
                package_id: '6389d131200bb5f8fcd11cf9',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                created_at: '2022-12-02T13:16:42.938Z',
                updated_at: '2022-12-02T13:16:42.938Z',
                updated_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                created_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                id: '6389fabad4ee57a0d96a0cdc'
            }
        ]
    }),
    __metadata("design:type", Object)
], SubscriptionObject.prototype, "results", void 0);
class GetSubscriptionListResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", SubscriptionObject)
], GetSubscriptionListResponse.prototype, "data", void 0);
exports.GetSubscriptionListResponse = GetSubscriptionListResponse;
class SubscriptionResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            status: 'active',
            auto_renew_at: '2022-12-31T17:00:00.000Z',
            last_renew_at: null,
            subscribed_at: '2022-12-02T13:16:42.926Z',
            subscription_status: 'ACTIVE',
            price: 20,
            quantity: 1,
            payment_plan: 'MONTHLY',
            name: 'More iPad',
            package_id: '6389d131200bb5f8fcd11cf9',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-12-02T13:16:42.938Z',
            updated_at: '2022-12-02T13:16:42.938Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '6389fabad4ee57a0d96a0cdc'
        }
    }),
    __metadata("design:type", Object)
], SubscriptionResponse.prototype, "data", void 0);
exports.SubscriptionResponse = SubscriptionResponse;
//# sourceMappingURL=subscription.entity.js.map