import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class SubscriptionObject extends PaginateResponseDto<any> {
    results: any;
}
export declare class GetSubscriptionListResponse extends ResponseDto<SubscriptionObject> {
    data: SubscriptionObject;
}
export declare class SubscriptionResponse extends ResponseDto<any> {
    data: any;
}
export {};
