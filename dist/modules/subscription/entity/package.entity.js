"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllPackageResponse = exports.PackagePaginateResponse = exports.PackageResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
class PackageResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            created_by: {
                username: 'superadmin',
                id: '6318d06a1fa78236d380db83'
            },
            updated_by: {
                username: 'superadmin',
                id: '6318d06a1fa78236d380db83'
            },
            status: 'active',
            package_type: ['IPAD'],
            i_pad_capacity: 1,
            is_active_default: true,
            is_free: true,
            daily_price: 0,
            yearly_price: 0,
            monthly_price: 0,
            description: 'ฟรีค่าบริการรายเดือนสำหรับ iPad เครื่องแรก ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)',
            name: 'iPad',
            updated_at: '2022-12-20T07:56:47.996Z',
            created_at: '2022-12-02T10:20:25.976Z',
            id: '6389d169200bb5f8fcd11cfb'
        }
    }),
    __metadata("design:type", Object)
], PackageResponse.prototype, "data", void 0);
exports.PackageResponse = PackageResponse;
class PackagePaginate extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: [
            {
                created_by: {
                    username: 'superadmin',
                    id: '6318d06a1fa78236d380db83'
                },
                updated_by: {
                    username: 'superadmin',
                    id: '6318d06a1fa78236d380db83'
                },
                status: 'active',
                package_type: ['IPAD'],
                i_pad_capacity: 1,
                is_active_default: true,
                is_free: true,
                daily_price: 0,
                yearly_price: 0,
                monthly_price: 0,
                description: 'ฟรีค่าบริการรายเดือนสำหรับ iPad เครื่องแรก ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)',
                name: 'iPad',
                updated_at: '2022-12-20T07:56:47.996Z',
                created_at: '2022-12-02T10:20:25.976Z',
                id: '6389d169200bb5f8fcd11cfb'
            }
        ]
    }),
    __metadata("design:type", Object)
], PackagePaginate.prototype, "results", void 0);
class PackagePaginateResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", PackagePaginate)
], PackagePaginateResponse.prototype, "data", void 0);
exports.PackagePaginateResponse = PackagePaginateResponse;
class GetAllPackageResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: [
            {
                daily_price: 0.7,
                yearly_price: 200,
                monthly_price: 20,
                description: 'เพิ่ม iPad 1 เครื่อง ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)',
                name: 'More iPad',
                i_pad_capacity: 1,
                is_active_default: false,
                is_free: false,
                package_type: ['IPAD'],
                id: '6389d131200bb5f8fcd11cf9'
            },
            {
                daily_price: 0,
                yearly_price: 0,
                monthly_price: 0,
                description: 'ฟรีค่าบริการรายเดือนสำหรับ iPad เครื่องแรก ไม่จำกัด User (ไม่รวมค่าอุปกรณ์);รับ Order และชำระเงินที่โต๊ะ สะดวกและรวดเร็ว;เชื่อมต่อผ่านระบบ WiFi หรือ ระบบเครือข่าย Cellular (Verizon, AT&T, T-mobile)',
                name: 'iPad',
                is_free: true,
                i_pad_capacity: 1,
                is_active_default: true,
                package_type: ['IPAD'],
                id: '6389d169200bb5f8fcd11cfb'
            }
        ]
    }),
    __metadata("design:type", Object)
], GetAllPackageResponse.prototype, "data", void 0);
exports.GetAllPackageResponse = GetAllPackageResponse;
//# sourceMappingURL=package.entity.js.map