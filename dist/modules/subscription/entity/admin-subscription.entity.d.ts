import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
declare class SubscriptionObject extends TimestampResponseDto {
    auto_renew_at: Date | string;
    last_renew_at: Date | string;
    subscribed_at: Date | string;
    subscription_status: string;
    price: number;
    quantity: number;
    payment_plan: string;
    name: string;
    package_id: string;
    restaurant_id: string;
    id: string;
}
declare class SubscriptionPaginateObject extends PaginateResponseDto<SubscriptionObject[]> {
    results: SubscriptionObject[];
}
export declare class AdminSubscriptionPaginateResponse extends ResponseDto<SubscriptionPaginateObject> {
    data: SubscriptionPaginateObject;
}
export declare class AdminSubscriptionResponse extends ResponseDto<SubscriptionObject> {
    data: SubscriptionObject;
}
export {};
