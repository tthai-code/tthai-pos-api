import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
export declare class PackageResponse extends ResponseDto<any> {
    data: any;
}
declare class PackagePaginate extends PaginateResponseDto<any> {
    results: any;
}
export declare class PackagePaginateResponse extends ResponseDto<PackagePaginate> {
    data: PackagePaginate;
}
export declare class GetAllPackageResponse extends ResponseDto<any> {
    data: any;
}
export {};
