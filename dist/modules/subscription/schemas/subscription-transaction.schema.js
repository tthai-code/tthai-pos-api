"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionTransactionSchema = exports.SubscriptionTransactionField = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const subscription_payment_status_enum_1 = require("../common/subscription-payment-status.enum");
let SubscriptionTransactionField = class SubscriptionTransactionField {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], SubscriptionTransactionField.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'subscription' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], SubscriptionTransactionField.prototype, "subscriptionId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.Mixed, default: null }),
    __metadata("design:type", Object)
], SubscriptionTransactionField.prototype, "response", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], SubscriptionTransactionField.prototype, "subscribedAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], SubscriptionTransactionField.prototype, "autoRenewAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(subscription_payment_status_enum_1.SubscriptionPaymentStatus),
        default: subscription_payment_status_enum_1.SubscriptionPaymentStatus.PENDING
    }),
    __metadata("design:type", String)
], SubscriptionTransactionField.prototype, "paymentStatus", void 0);
SubscriptionTransactionField = __decorate([
    (0, mongoose_1.Schema)({
        timestamps: true,
        strict: true,
        collection: 'subscriptionTransactions'
    })
], SubscriptionTransactionField);
exports.SubscriptionTransactionField = SubscriptionTransactionField;
exports.SubscriptionTransactionSchema = mongoose_1.SchemaFactory.createForClass(SubscriptionTransactionField);
exports.SubscriptionTransactionSchema.plugin(mongoosePaginate);
//# sourceMappingURL=subscription-transaction.schema.js.map