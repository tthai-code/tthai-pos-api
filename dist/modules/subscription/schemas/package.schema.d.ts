import { Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type PackageDocument = PackageFields & Document;
export declare class PackageFields {
    name: string;
    description: string;
    monthlyPrice: number;
    yearlyPrice: number;
    isFree: boolean;
    isActiveDefault: boolean;
    iPadCapacity: number;
    packageType: string[];
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const PackageSchema: import("mongoose").Schema<Document<PackageFields, any, any>, import("mongoose").Model<Document<PackageFields, any, any>, any, any, any>, any, any>;
