import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type SubscriptionDocument = SubscriptionFields & Document;
export declare class SubscriptionFields {
    restaurantId: Types.ObjectId;
    packageId: string;
    name: string;
    paymentPlan: string;
    quantity: number;
    price: number;
    amount: number;
    subscriptionStatus: string;
    subscribedAt: Date;
    lastRenewAt: Date;
    autoRenewAt: Date;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const SubscriptionSchema: MongooseSchema<Document<SubscriptionFields, any, any>, import("mongoose").Model<Document<SubscriptionFields, any, any>, any, any, any>, any, any>;
