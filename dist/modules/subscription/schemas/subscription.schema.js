"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionSchema = exports.SubscriptionFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoosePaginate = require("mongoose-paginate");
const mongoose_2 = require("mongoose");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const subscription_plan_enum_1 = require("../common/subscription-plan.enum");
let SubscriptionFields = class SubscriptionFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], SubscriptionFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], SubscriptionFields.prototype, "packageId", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], SubscriptionFields.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ enum: Object.values(subscription_plan_enum_1.SubscriptionPlanEnum), required: true }),
    __metadata("design:type", String)
], SubscriptionFields.prototype, "paymentPlan", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 1 }),
    __metadata("design:type", Number)
], SubscriptionFields.prototype, "quantity", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], SubscriptionFields.prototype, "price", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], SubscriptionFields.prototype, "amount", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(subscription_plan_enum_1.SubscriptionStatusEnum),
        default: subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE
    }),
    __metadata("design:type", String)
], SubscriptionFields.prototype, "subscriptionStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], SubscriptionFields.prototype, "subscribedAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], SubscriptionFields.prototype, "lastRenewAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], SubscriptionFields.prototype, "autoRenewAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], SubscriptionFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], SubscriptionFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], SubscriptionFields.prototype, "createdBy", void 0);
SubscriptionFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'subscription' })
], SubscriptionFields);
exports.SubscriptionFields = SubscriptionFields;
exports.SubscriptionSchema = mongoose_1.SchemaFactory.createForClass(SubscriptionFields);
exports.SubscriptionSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
exports.SubscriptionSchema.plugin(mongoosePaginate);
//# sourceMappingURL=subscription.schema.js.map