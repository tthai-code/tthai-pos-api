import { Document, Schema as MongooseSchema, Types } from 'mongoose';
export declare type SubscriptionTransactionDocument = SubscriptionTransactionField & Document;
export declare class SubscriptionTransactionField {
    restaurantId: Types.ObjectId;
    subscriptionId: Types.ObjectId;
    response: any;
    subscribedAt: Date;
    autoRenewAt: Date;
    paymentStatus: string;
}
export declare const SubscriptionTransactionSchema: MongooseSchema<Document<SubscriptionTransactionField, any, any>, import("mongoose").Model<Document<SubscriptionTransactionField, any, any>, any, any, any>, any, any>;
