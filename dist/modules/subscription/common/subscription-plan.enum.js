"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionStatusEnum = exports.SubscriptionPlanEnum = void 0;
var SubscriptionPlanEnum;
(function (SubscriptionPlanEnum) {
    SubscriptionPlanEnum["YEARLY"] = "YEARLY";
    SubscriptionPlanEnum["MONTHLY"] = "MONTHLY";
    SubscriptionPlanEnum["FREE"] = "FREE";
})(SubscriptionPlanEnum = exports.SubscriptionPlanEnum || (exports.SubscriptionPlanEnum = {}));
var SubscriptionStatusEnum;
(function (SubscriptionStatusEnum) {
    SubscriptionStatusEnum["ACTIVE"] = "ACTIVE";
    SubscriptionStatusEnum["INACTIVE"] = "INACTIVE";
})(SubscriptionStatusEnum = exports.SubscriptionStatusEnum || (exports.SubscriptionStatusEnum = {}));
//# sourceMappingURL=subscription-plan.enum.js.map