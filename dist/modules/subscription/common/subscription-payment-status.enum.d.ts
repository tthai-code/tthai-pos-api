export declare enum SubscriptionPaymentStatus {
    SUCCESSFUL = "SUCCESSFUL",
    PENDING = "PENDING",
    FAILURE = "FAILURE"
}
