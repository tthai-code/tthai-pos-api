"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionPaymentStatus = void 0;
var SubscriptionPaymentStatus;
(function (SubscriptionPaymentStatus) {
    SubscriptionPaymentStatus["SUCCESSFUL"] = "SUCCESSFUL";
    SubscriptionPaymentStatus["PENDING"] = "PENDING";
    SubscriptionPaymentStatus["FAILURE"] = "FAILURE";
})(SubscriptionPaymentStatus = exports.SubscriptionPaymentStatus || (exports.SubscriptionPaymentStatus = {}));
//# sourceMappingURL=subscription-payment-status.enum.js.map