"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageTypeEnum = void 0;
var PackageTypeEnum;
(function (PackageTypeEnum) {
    PackageTypeEnum["IPAD"] = "IPAD";
    PackageTypeEnum["WEBSITE"] = "WEBSITE";
    PackageTypeEnum["CATERING"] = "CATERING";
    PackageTypeEnum["DELIVERY"] = "DELIVERY";
    PackageTypeEnum["RESERVATION"] = "RESERVATION";
    PackageTypeEnum["ACCOUNTING"] = "ACCOUNTING";
})(PackageTypeEnum = exports.PackageTypeEnum || (exports.PackageTypeEnum = {}));
//# sourceMappingURL=package-type.enum.js.map