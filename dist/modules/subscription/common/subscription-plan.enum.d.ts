export declare enum SubscriptionPlanEnum {
    YEARLY = "YEARLY",
    MONTHLY = "MONTHLY",
    FREE = "FREE"
}
export declare enum SubscriptionStatusEnum {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}
