export declare enum PackageTypeEnum {
    IPAD = "IPAD",
    WEBSITE = "WEBSITE",
    CATERING = "CATERING",
    DELIVERY = "DELIVERY",
    RESERVATION = "RESERVATION",
    ACCOUNTING = "ACCOUNTING"
}
