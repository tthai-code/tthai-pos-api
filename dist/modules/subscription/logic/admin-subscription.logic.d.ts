import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { EditSubscriptionDto } from '../dto/edit-subscription.dto';
import { SubscriptionPaginateDto } from '../dto/get-subscription.dto';
import { PackageService } from '../services/package.service';
import { SubscriptionService } from '../services/subscription.service';
export declare class AdminSubscriptionLogic {
    private readonly subscriptionService;
    private readonly restaurantService;
    private readonly packageService;
    constructor(subscriptionService: SubscriptionService, restaurantService: RestaurantService, packageService: PackageService);
    getSubscriptionByRestaurant(restaurantId: string, query: SubscriptionPaginateDto): Promise<PaginateResult<import("../schemas/subscription.schema").SubscriptionDocument>>;
    unsubscribe(subscriptionId: string): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
    subscribe(subscriptionId: string): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
    updateSubscription(subscriptionId: string, payload: EditSubscriptionDto): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
}
