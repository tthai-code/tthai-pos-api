"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const dayjs_1 = require("../../../plugins/dayjs");
const package_service_1 = require("../services/package.service");
let PackageLogic = class PackageLogic {
    constructor(packageService) {
        this.packageService = packageService;
    }
    async getPackageList(query) {
        const packageList = await this.packageService.paginate(query.buildQuery(), query);
        const { docs } = packageList;
        const dayInMonth = (0, dayjs_1.default)().daysInMonth();
        const resultDocs = docs.map((item) => (Object.assign(Object.assign({}, item.toObject()), { dailyPrice: item.monthlyPrice / dayInMonth })));
        return Object.assign(Object.assign({}, packageList), { docs: resultDocs });
    }
    async getPackageByID(id) {
        const packageData = await this.packageService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 });
        if (!packageData) {
            throw new common_1.NotFoundException('Not found package.');
        }
        const dayInMonth = (0, dayjs_1.default)().daysInMonth();
        return Object.assign(Object.assign({}, packageData.toObject()), { dailyPrice: packageData.monthlyPrice / dayInMonth });
    }
};
PackageLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [package_service_1.PackageService])
], PackageLogic);
exports.PackageLogic = PackageLogic;
//# sourceMappingURL=package.logic.js.map