"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminSubscriptionLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const subscription_plan_enum_1 = require("../common/subscription-plan.enum");
const package_service_1 = require("../services/package.service");
const subscription_service_1 = require("../services/subscription.service");
let AdminSubscriptionLogic = class AdminSubscriptionLogic {
    constructor(subscriptionService, restaurantService, packageService) {
        this.subscriptionService = subscriptionService;
        this.restaurantService = restaurantService;
        this.packageService = packageService;
    }
    async getSubscriptionByRestaurant(restaurantId, query) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: { $ne: status_enum_1.StatusEnum.DELETED }
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        return this.subscriptionService.paginate(query.buildQuery(restaurantId), query);
    }
    async unsubscribe(subscriptionId) {
        const subscription = await this.subscriptionService.findOne({
            _id: subscriptionId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!subscription) {
            throw new common_1.NotFoundException('Not found subscription by this id.');
        }
        if (subscription.subscriptionStatus === subscription_plan_enum_1.SubscriptionStatusEnum.INACTIVE) {
            throw new common_1.BadRequestException('Subscription is already inactive.');
        }
        return this.subscriptionService.updateOne({ _id: subscriptionId }, { subscriptionStatus: subscription_plan_enum_1.SubscriptionStatusEnum.INACTIVE });
    }
    async subscribe(subscriptionId) {
        const subscription = await this.subscriptionService.findOne({
            _id: subscriptionId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!subscription) {
            throw new common_1.NotFoundException('Not found subscription by this id.');
        }
        if (subscription.subscriptionStatus === subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE) {
            throw new common_1.BadRequestException('Subscription is already active.');
        }
        return this.subscriptionService.updateOne({ _id: subscriptionId }, { subscriptionStatus: subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE });
    }
    async updateSubscription(subscriptionId, payload) {
        const subscription = await this.subscriptionService.findOne({
            _id: subscriptionId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!subscription) {
            throw new common_1.NotFoundException('Not found subscription by this id.');
        }
        const packageData = await this.packageService.findOne({
            _id: subscription.packageId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!packageData)
            throw new common_1.NotFoundException('Not found package.');
        const paymentPlan = payload.paymentPlan;
        if (paymentPlan === subscription_plan_enum_1.SubscriptionPlanEnum.MONTHLY) {
            payload.price = packageData.monthlyPrice;
        }
        else if (paymentPlan === subscription_plan_enum_1.SubscriptionPlanEnum.YEARLY) {
            payload.price = packageData.yearlyPrice;
        }
        else {
            payload.price = 0;
            payload.paymentPlan = subscription_plan_enum_1.SubscriptionPlanEnum.FREE;
        }
        payload.amount = payload.price * payload.quantity;
        return this.subscriptionService.updateOne({ _id: subscriptionId }, payload);
    }
};
AdminSubscriptionLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [subscription_service_1.SubscriptionService,
        restaurant_service_1.RestaurantService,
        package_service_1.PackageService])
], AdminSubscriptionLogic);
exports.AdminSubscriptionLogic = AdminSubscriptionLogic;
//# sourceMappingURL=admin-subscription.logic.js.map