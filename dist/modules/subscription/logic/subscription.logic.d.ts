import { BankAccountService } from 'src/modules/payment-gateway/services/bank-account.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { AddSubscriptionDto } from '../dto/add-subscription.dto';
import { PackageService } from '../services/package.service';
import { SubscriptionService } from '../services/subscription.service';
export declare class SubscriptionLogic {
    private readonly subscriptionService;
    private readonly packageService;
    private readonly restaurantService;
    private readonly bankAccountService;
    constructor(subscriptionService: SubscriptionService, packageService: PackageService, restaurantService: RestaurantService, bankAccountService: BankAccountService);
    initDefaultPackage(restaurantId: string): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
    addSubscription(restaurantId: string, payload: AddSubscriptionDto): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
}
