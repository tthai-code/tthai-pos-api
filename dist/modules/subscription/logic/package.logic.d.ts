import { PackagePaginate } from '../dto/get-package.dto';
import { PackageService } from '../services/package.service';
export declare class PackageLogic {
    private readonly packageService;
    constructor(packageService: PackageService);
    getPackageList(query: PackagePaginate): Promise<any>;
    getPackageByID(id: string): Promise<{
        dailyPrice: number;
        description: string;
        name: string;
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        monthlyPrice: number;
        yearlyPrice: number;
        isActiveDefault: boolean;
        iPadCapacity: number;
        packageType: string[];
        isFree: boolean;
    }>;
}
