"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs_1 = require("../../../plugins/dayjs");
const status_enum_1 = require("../../../common/enum/status.enum");
const ach_bank_type_enum_1 = require("../../payment-gateway/common/ach-bank.type.enum");
const bank_account_service_1 = require("../../payment-gateway/services/bank-account.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const subscription_plan_enum_1 = require("../common/subscription-plan.enum");
const package_service_1 = require("../services/package.service");
const subscription_service_1 = require("../services/subscription.service");
let SubscriptionLogic = class SubscriptionLogic {
    constructor(subscriptionService, packageService, restaurantService, bankAccountService) {
        this.subscriptionService = subscriptionService;
        this.packageService = packageService;
        this.restaurantService = restaurantService;
        this.bankAccountService = bankAccountService;
    }
    async initDefaultPackage(restaurantId) {
        const defaultPackages = await this.packageService.getAll({
            isActiveDefault: true,
            isFree: true,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const subscription = await this.subscriptionService.getAll({
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE,
            subscriptionStatus: subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE
        });
        const defaultPackageIds = defaultPackages.map((item) => `${item._id}`);
        const subscriptionIds = subscription.map((item) => item.packageId);
        const isAlreadyInit = subscriptionIds.some((item) => defaultPackageIds.includes(item));
        if (!isAlreadyInit) {
            const subscriptionsPayload = [];
            const now = (0, dayjs_1.default)().tz('Etc/GMT+5');
            for (const item of defaultPackages) {
                const subscribe = {
                    restaurantId,
                    packageId: item.id,
                    name: item.name,
                    paymentPlan: subscription_plan_enum_1.SubscriptionPlanEnum.FREE,
                    quantity: 1,
                    price: 0,
                    amount: 0,
                    subscribedAt: now.toDate(),
                    autoRenewAt: now.add(1, 'month').date(1).startOf('day').toDate()
                };
                subscriptionsPayload.push(subscribe);
            }
            return this.subscriptionService.createMany(subscriptionsPayload);
        }
        return null;
    }
    async addSubscription(restaurantId, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const bankAccount = await this.bankAccountService.findOne({
            restaurantId,
            token: { $ne: null },
            bankAccountStatus: ach_bank_type_enum_1.BankAccountStatusEnum.ACTIVE,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!bankAccount) {
            throw new common_1.BadRequestException('Must be active ACH Bank Account before subscribe.');
        }
        const packageData = await this.packageService.findOne({
            _id: payload.packageId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!packageData)
            throw new common_1.NotFoundException('Not found package.');
        let price;
        let renewType;
        const paymentPlan = payload.paymentPlan;
        if (paymentPlan === subscription_plan_enum_1.SubscriptionPlanEnum.MONTHLY) {
            price = packageData.monthlyPrice;
            renewType = 'month';
        }
        else if (paymentPlan === subscription_plan_enum_1.SubscriptionPlanEnum.YEARLY) {
            price = packageData.yearlyPrice;
            renewType = 'year';
        }
        else {
            price = 0;
            payload.paymentPlan = subscription_plan_enum_1.SubscriptionPlanEnum.FREE;
            renewType = 'month';
        }
        const now = (0, dayjs_1.default)().tz('Etc/GMT+5');
        const transform = {
            restaurantId,
            packageId: payload.packageId,
            name: packageData.name,
            paymentPlan,
            quantity: payload.quantity,
            price,
            amount: payload.quantity * price,
            subscribedAt: now.toDate(),
            autoRenewAt: now.add(1, renewType).date(1).startOf('day').toDate()
        };
        return this.subscriptionService.create(transform);
    }
};
SubscriptionLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [subscription_service_1.SubscriptionService,
        package_service_1.PackageService,
        restaurant_service_1.RestaurantService,
        bank_account_service_1.BankAccountService])
], SubscriptionLogic);
exports.SubscriptionLogic = SubscriptionLogic;
//# sourceMappingURL=subscription.logic.js.map