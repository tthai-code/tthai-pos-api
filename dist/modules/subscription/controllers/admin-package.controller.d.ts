import { CreatePackageDto } from '../dto/create-package.dto';
import { PackagePaginate } from '../dto/get-package.dto';
import { PackageLogic } from '../logic/package.logic';
import { PackageService } from '../services/package.service';
export declare class AdminPackageController {
    private readonly packageService;
    private readonly packageLogic;
    constructor(packageService: PackageService, packageLogic: PackageLogic);
    getPackages(query: PackagePaginate): Promise<any>;
    createPackage(payload: CreatePackageDto): Promise<import("../schemas/package.schema").PackageDocument>;
    getPackage(id: string): Promise<{
        dailyPrice: number;
        description: string;
        name: string;
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        monthlyPrice: number;
        yearlyPrice: number;
        isActiveDefault: boolean;
        iPadCapacity: number;
        packageType: string[];
        isFree: boolean;
    }>;
    updatePackage(id: string, payload: CreatePackageDto): Promise<import("../schemas/package.schema").PackageDocument>;
    deletePackage(id: string): Promise<import("../schemas/package.schema").PackageDocument>;
}
