import { AddSubscriptionDto } from '../dto/add-subscription.dto';
import { EditSubscriptionDto } from '../dto/edit-subscription.dto';
import { SubscriptionPaginateDto } from '../dto/get-subscription.dto';
import { AdminSubscriptionLogic } from '../logic/admin-subscription.logic';
import { SubscriptionLogic } from '../logic/subscription.logic';
import { SubscriptionService } from '../services/subscription.service';
export declare class AdminSubscriptionController {
    private readonly subscriptionService;
    private readonly adminSubscriptionLogic;
    private readonly subscriptionLogic;
    constructor(subscriptionService: SubscriptionService, adminSubscriptionLogic: AdminSubscriptionLogic, subscriptionLogic: SubscriptionLogic);
    getSubscription(restaurantId: string, query: SubscriptionPaginateDto): Promise<PaginateResult<import("../schemas/subscription.schema").SubscriptionDocument>>;
    getSubscriptionById(subscriptionId: string): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
    unsubscribe(subscriptionId: string): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
    subscribe(subscriptionId: string): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
    updateSubscription(subscriptionId: string, payload: EditSubscriptionDto): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
    addSubscription(restaurantId: string, payload: AddSubscriptionDto): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
}
