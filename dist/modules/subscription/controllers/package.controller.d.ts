import { PackageService } from '../services/package.service';
export declare class PackageController {
    private readonly packageService;
    constructor(packageService: PackageService);
    getAllPackage(): Promise<any[]>;
}
