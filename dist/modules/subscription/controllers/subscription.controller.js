"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const add_subscription_dto_1 = require("../dto/add-subscription.dto");
const get_subscription_dto_1 = require("../dto/get-subscription.dto");
const subscription_entity_1 = require("../entity/subscription.entity");
const subscription_logic_1 = require("../logic/subscription.logic");
const subscription_service_1 = require("../services/subscription.service");
let SubscriptionController = class SubscriptionController {
    constructor(subscriptionService, subscriptionLogic) {
        this.subscriptionService = subscriptionService;
        this.subscriptionLogic = subscriptionLogic;
    }
    async getSubscriptionList(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.subscriptionService.paginate(query.buildQuery(restaurantId), query);
    }
    async addSubscription(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.subscriptionLogic.addSubscription(restaurantId, payload);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => subscription_entity_1.GetSubscriptionListResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Subscription List',
        description: 'use bearer `restaurant_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_subscription_dto_1.SubscriptionPaginateDto]),
    __metadata("design:returntype", Promise)
], SubscriptionController.prototype, "getSubscriptionList", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => subscription_entity_1.SubscriptionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Add New Subscription',
        description: 'use bearer `restaurant_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [add_subscription_dto_1.AddSubscriptionDto]),
    __metadata("design:returntype", Promise)
], SubscriptionController.prototype, "addSubscription", null);
SubscriptionController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('subscription'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/subscription'),
    __metadata("design:paramtypes", [subscription_service_1.SubscriptionService,
        subscription_logic_1.SubscriptionLogic])
], SubscriptionController);
exports.SubscriptionController = SubscriptionController;
//# sourceMappingURL=subscription.controller.js.map