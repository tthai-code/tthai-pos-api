"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PackageController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const package_entity_1 = require("../entity/package.entity");
const package_service_1 = require("../services/package.service");
let PackageController = class PackageController {
    constructor(packageService) {
        this.packageService = packageService;
    }
    async getAllPackage() {
        return await this.packageService.getAll({ status: status_enum_1.StatusEnum.ACTIVE, isActiveDefault: false }, { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 });
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => package_entity_1.GetAllPackageResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get All Package list for subscribe.',
        description: 'use bearer `restaurant_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PackageController.prototype, "getAllPackage", null);
PackageController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('package'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/package'),
    __metadata("design:paramtypes", [package_service_1.PackageService])
], PackageController);
exports.PackageController = PackageController;
//# sourceMappingURL=package.controller.js.map