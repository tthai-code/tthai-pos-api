"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminSubscriptionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const super_admin_auth_guard_1 = require("../../auth/guards/super-admin-auth.guard");
const add_subscription_dto_1 = require("../dto/add-subscription.dto");
const edit_subscription_dto_1 = require("../dto/edit-subscription.dto");
const get_subscription_dto_1 = require("../dto/get-subscription.dto");
const admin_subscription_entity_1 = require("../entity/admin-subscription.entity");
const admin_subscription_logic_1 = require("../logic/admin-subscription.logic");
const subscription_logic_1 = require("../logic/subscription.logic");
const subscription_service_1 = require("../services/subscription.service");
let AdminSubscriptionController = class AdminSubscriptionController {
    constructor(subscriptionService, adminSubscriptionLogic, subscriptionLogic) {
        this.subscriptionService = subscriptionService;
        this.adminSubscriptionLogic = adminSubscriptionLogic;
        this.subscriptionLogic = subscriptionLogic;
    }
    async getSubscription(restaurantId, query) {
        return await this.adminSubscriptionLogic.getSubscriptionByRestaurant(restaurantId, query);
    }
    async getSubscriptionById(subscriptionId) {
        const subscription = await this.subscriptionService.findOne({
            _id: subscriptionId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!subscription)
            throw new common_1.NotFoundException('Not found subscription.');
        return subscription;
    }
    async unsubscribe(subscriptionId) {
        return await this.adminSubscriptionLogic.unsubscribe(subscriptionId);
    }
    async subscribe(subscriptionId) {
        return await this.adminSubscriptionLogic.subscribe(subscriptionId);
    }
    async updateSubscription(subscriptionId, payload) {
        return await this.adminSubscriptionLogic.updateSubscription(subscriptionId, payload);
    }
    async addSubscription(restaurantId, payload) {
        return await this.subscriptionLogic.addSubscription(restaurantId, payload);
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId/restaurant'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_subscription_entity_1.AdminSubscriptionPaginateResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Subscription by Restaurant',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, get_subscription_dto_1.SubscriptionPaginateDto]),
    __metadata("design:returntype", Promise)
], AdminSubscriptionController.prototype, "getSubscription", null);
__decorate([
    (0, common_1.Get)(':subscriptionId'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_subscription_entity_1.AdminSubscriptionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get a subscription by id',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('subscriptionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminSubscriptionController.prototype, "getSubscriptionById", null);
__decorate([
    (0, common_1.Patch)(':subscriptionId/unsubscribe'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_subscription_entity_1.AdminSubscriptionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Unsubscribe by id',
        description: 'user bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('subscriptionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminSubscriptionController.prototype, "unsubscribe", null);
__decorate([
    (0, common_1.Patch)(':subscriptionId/subscribe'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_subscription_entity_1.AdminSubscriptionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Subscribe by id',
        description: 'user bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('subscriptionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminSubscriptionController.prototype, "subscribe", null);
__decorate([
    (0, common_1.Put)(':subscriptionId'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_subscription_entity_1.AdminSubscriptionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Edit Subscription by id',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('subscriptionId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, edit_subscription_dto_1.EditSubscriptionDto]),
    __metadata("design:returntype", Promise)
], AdminSubscriptionController.prototype, "updateSubscription", null);
__decorate([
    (0, common_1.Post)(':restaurantId'),
    (0, swagger_1.ApiCreatedResponse)({ type: () => admin_subscription_entity_1.AdminSubscriptionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Add Subscription for restaurant.',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, add_subscription_dto_1.AddSubscriptionDto]),
    __metadata("design:returntype", Promise)
], AdminSubscriptionController.prototype, "addSubscription", null);
AdminSubscriptionController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('admin/subscription'),
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    (0, common_1.Controller)('/v1/admin/subscription'),
    __metadata("design:paramtypes", [subscription_service_1.SubscriptionService,
        admin_subscription_logic_1.AdminSubscriptionLogic,
        subscription_logic_1.SubscriptionLogic])
], AdminSubscriptionController);
exports.AdminSubscriptionController = AdminSubscriptionController;
//# sourceMappingURL=admin-subscription.controller.js.map