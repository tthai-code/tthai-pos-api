import { AddSubscriptionDto } from '../dto/add-subscription.dto';
import { SubscriptionPaginateDto } from '../dto/get-subscription.dto';
import { SubscriptionLogic } from '../logic/subscription.logic';
import { SubscriptionService } from '../services/subscription.service';
export declare class SubscriptionController {
    private readonly subscriptionService;
    private readonly subscriptionLogic;
    constructor(subscriptionService: SubscriptionService, subscriptionLogic: SubscriptionLogic);
    getSubscriptionList(query: SubscriptionPaginateDto): Promise<PaginateResult<import("../schemas/subscription.schema").SubscriptionDocument>>;
    addSubscription(payload: AddSubscriptionDto): Promise<import("../schemas/subscription.schema").SubscriptionDocument>;
}
