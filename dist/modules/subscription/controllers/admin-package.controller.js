"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminPackageController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const super_admin_auth_guard_1 = require("../../auth/guards/super-admin-auth.guard");
const create_package_dto_1 = require("../dto/create-package.dto");
const get_package_dto_1 = require("../dto/get-package.dto");
const package_entity_1 = require("../entity/package.entity");
const package_logic_1 = require("../logic/package.logic");
const package_service_1 = require("../services/package.service");
let AdminPackageController = class AdminPackageController {
    constructor(packageService, packageLogic) {
        this.packageService = packageService;
        this.packageLogic = packageLogic;
    }
    async getPackages(query) {
        return await this.packageLogic.getPackageList(query);
    }
    async createPackage(payload) {
        payload.isFree = payload.monthlyPrice + payload.yearlyPrice === 0;
        return await this.packageService.create(payload);
    }
    async getPackage(id) {
        return await this.packageLogic.getPackageByID(id);
    }
    async updatePackage(id, payload) {
        payload.isFree = payload.monthlyPrice + payload.yearlyPrice === 0;
        return await this.packageService.update(id, payload);
    }
    async deletePackage(id) {
        return await this.packageService.delete(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => package_entity_1.PackagePaginateResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Package List',
        description: 'use barer `admin_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_package_dto_1.PackagePaginate]),
    __metadata("design:returntype", Promise)
], AdminPackageController.prototype, "getPackages", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => package_entity_1.PackageResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create a  Package',
        description: 'use barer `admin_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_package_dto_1.CreatePackageDto]),
    __metadata("design:returntype", Promise)
], AdminPackageController.prototype, "createPackage", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => package_entity_1.PackageResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get a Package by ID',
        description: 'use barer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminPackageController.prototype, "getPackage", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => package_entity_1.PackageResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Update Package by ID',
        description: 'use barer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, create_package_dto_1.CreatePackageDto]),
    __metadata("design:returntype", Promise)
], AdminPackageController.prototype, "updatePackage", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => package_entity_1.PackageResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete Package by ID',
        description: 'use barer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminPackageController.prototype, "deletePackage", null);
AdminPackageController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('admin/package'),
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    (0, common_1.Controller)('v1/admin/package'),
    __metadata("design:paramtypes", [package_service_1.PackageService,
        package_logic_1.PackageLogic])
], AdminPackageController);
exports.AdminPackageController = AdminPackageController;
//# sourceMappingURL=admin-package.controller.js.map