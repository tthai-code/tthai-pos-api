import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { SubscriptionTransactionDocument } from '../schemas/subscription-transaction.schema';
export declare class SubscriptionTransactionService {
    private readonly SubscriptionTransactionModel;
    private request;
    constructor(SubscriptionTransactionModel: PaginateModel<SubscriptionTransactionDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<SubscriptionTransactionDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<SubscriptionTransactionDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<SubscriptionTransactionDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<SubscriptionTransactionDocument>;
    createMany(payload: any): Promise<SubscriptionTransactionDocument>;
    update(id: string, product: any): Promise<SubscriptionTransactionDocument>;
    updateOne(condition: any, payload: any): Promise<SubscriptionTransactionDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<SubscriptionTransactionDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
    delete(id: string): Promise<SubscriptionTransactionDocument>;
    getAll(condition: any, project?: any, options?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<SubscriptionTransactionDocument>;
    paginate(query: any, queryParam: any, select?: any): Promise<PaginateResult<SubscriptionTransactionDocument>>;
}
