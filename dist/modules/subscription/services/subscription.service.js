"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionService = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const status_enum_1 = require("../../../common/enum/status.enum");
let SubscriptionService = class SubscriptionService {
    constructor(SubscriptionModel, request) {
        this.SubscriptionModel = SubscriptionModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.SubscriptionModel.findById({ _id: id });
    }
    getRequest() {
        return this.request;
    }
    count(query) {
        return this.SubscriptionModel.countDocuments(query);
    }
    async isExists(condition) {
        const result = await this.SubscriptionModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.SubscriptionModel;
    }
    async getSession() {
        await this.SubscriptionModel.createCollection();
        return await this.SubscriptionModel.startSession();
    }
    async transactionCreate(payload, session) {
        const document = new this.SubscriptionModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save({ session });
    }
    async aggregate(pipeline) {
        return this.SubscriptionModel.aggregate(pipeline);
    }
    async create(payload) {
        const document = new this.SubscriptionModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async createMany(payload) {
        return this.SubscriptionModel.insertMany(payload);
    }
    async update(id, product) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save();
    }
    async updateOne(condition, payload) {
        return this.SubscriptionModel.findOneAndUpdate(condition, { $set: Object.assign({}, payload) }, { new: true });
    }
    async transactionUpdate(id, product, session) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, product)).save({ session });
    }
    async transactionUpdateMany(condition, payload, session) {
        return this.SubscriptionModel.updateMany(condition, { $set: payload }, { session });
    }
    async delete(id) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set({ status: status_enum_1.StatusEnum.DELETED }).save();
    }
    getAll(condition, project) {
        return this.SubscriptionModel.find(condition, project);
    }
    findById(id) {
        return this.SubscriptionModel.findById(id);
    }
    findOne(condition, project) {
        return this.SubscriptionModel.findOne(condition, project);
    }
    paginate(query, queryParam, select) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder },
            select
        };
        return this.SubscriptionModel.paginate(query, options);
    }
};
SubscriptionService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('subscription')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], SubscriptionService);
exports.SubscriptionService = SubscriptionService;
//# sourceMappingURL=subscription.service.js.map