import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { SubscriptionDocument } from '../schemas/subscription.schema';
export declare class SubscriptionService {
    private readonly SubscriptionModel;
    private request;
    constructor(SubscriptionModel: PaginateModel<SubscriptionDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<SubscriptionDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<SubscriptionDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<SubscriptionDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<SubscriptionDocument>;
    createMany(payload: any): Promise<SubscriptionDocument>;
    update(id: string, product: any): Promise<SubscriptionDocument>;
    updateOne(condition: any, payload: any): Promise<SubscriptionDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<SubscriptionDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
    delete(id: string): Promise<SubscriptionDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<SubscriptionDocument>;
    paginate(query: any, queryParam: any, select?: any): Promise<PaginateResult<SubscriptionDocument>>;
}
