import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { PackageDocument } from '../schemas/package.schema';
import { PackagePaginate } from '../dto/get-package.dto';
export declare class PackageService {
    private readonly PackageModel;
    private request;
    constructor(PackageModel: PaginateModel<PackageDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<PackageDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<PackageDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<PackageDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<PackageDocument>;
    update(id: string, product: any): Promise<PackageDocument>;
    updateOne(condition: any, payload: any): Promise<PackageDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<PackageDocument>;
    delete(id: string): Promise<PackageDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<PackageDocument>;
    paginate(query: any, queryParam: PackagePaginate, select?: any): Promise<PaginateResult<PackageDocument>>;
}
