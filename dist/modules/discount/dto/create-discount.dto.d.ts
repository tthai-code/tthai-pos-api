export declare class CreateDiscountDto {
    restaurantId: string;
    readonly name: string;
    readonly discount: number;
}
