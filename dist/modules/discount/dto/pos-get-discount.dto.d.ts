import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
export declare class POSPaginateDiscountDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    buildQuery(restaurantId: string): {
        $or: {
            name: {
                $regex: string;
                $options: string;
            };
        }[];
        restaurantId: string;
        status: StatusEnum;
    };
}
