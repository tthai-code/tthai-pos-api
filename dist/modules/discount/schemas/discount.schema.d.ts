import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type DiscountDocument = DiscountFields & Document;
export declare class DiscountFields {
    restaurantId: Types.ObjectId;
    name: string;
    discount: number;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const DiscountSchema: MongooseSchema<Document<DiscountFields, any, any>, import("mongoose").Model<Document<DiscountFields, any, any>, any, any, any>, any, any>;
