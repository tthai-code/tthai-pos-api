import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { POSPaginateDiscountDto } from '../dto/pos-get-discount.dto';
import { DiscountDocument } from '../schemas/discount.schema';
export declare class DiscountService {
    private readonly DiscountModel;
    private request;
    constructor(DiscountModel: PaginateModel<DiscountDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<DiscountDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<DiscountDocument>;
    create(payload: any): Promise<DiscountDocument>;
    update(id: string, payload: any): Promise<DiscountDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<DiscountDocument>;
    delete(id: string): Promise<DiscountDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<DiscountDocument>;
    paginate(query: any, queryParam: POSPaginateDiscountDto, select?: any): Promise<PaginateResult<DiscountDocument>>;
}
