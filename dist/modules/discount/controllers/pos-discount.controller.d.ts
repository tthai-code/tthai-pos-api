import { CreateDiscountDto } from '../dto/create-discount.dto';
import { POSPaginateDiscountDto } from '../dto/pos-get-discount.dto';
import { POSDiscountLogic } from '../logic/pos-discount.logic';
export declare class POSDiscountController {
    private readonly posDiscountLogic;
    constructor(posDiscountLogic: POSDiscountLogic);
    getDiscountList(query: POSPaginateDiscountDto): Promise<PaginateResult<import("../schemas/discount.schema").DiscountDocument>>;
    getDiscount(id: string): Promise<import("../schemas/discount.schema").DiscountDocument>;
    createDiscount(payload: CreateDiscountDto): Promise<{
        success: boolean;
        id: any;
    }>;
    updateDiscount(id: string, payload: CreateDiscountDto): Promise<{
        success: boolean;
        id: any;
    }>;
    deleteDiscount(id: string): Promise<{
        success: boolean;
    }>;
}
