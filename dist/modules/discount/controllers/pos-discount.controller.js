"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSDiscountController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const create_discount_dto_1 = require("../dto/create-discount.dto");
const pos_get_discount_dto_1 = require("../dto/pos-get-discount.dto");
const pos_discount_entity_1 = require("../entity/pos-discount.entity");
const pos_discount_logic_1 = require("../logic/pos-discount.logic");
let POSDiscountController = class POSDiscountController {
    constructor(posDiscountLogic) {
        this.posDiscountLogic = posDiscountLogic;
    }
    async getDiscountList(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.posDiscountLogic.getDiscountList(restaurantId, query);
    }
    async getDiscount(id) {
        return await this.posDiscountLogic.getDiscount(id);
    }
    async createDiscount(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.posDiscountLogic.createDiscount(restaurantId, payload);
    }
    async updateDiscount(id, payload) {
        return await this.posDiscountLogic.updateDiscount(id, payload);
    }
    async deleteDiscount(id) {
        return await this.posDiscountLogic.deleteDiscount(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_discount_entity_1.PaginateDiscountResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Discount List',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [pos_get_discount_dto_1.POSPaginateDiscountDto]),
    __metadata("design:returntype", Promise)
], POSDiscountController.prototype, "getDiscountList", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_discount_entity_1.DiscountResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Discount by ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSDiscountController.prototype, "getDiscount", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => pos_discount_entity_1.CreateUpdateDiscountResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create a Discount',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_discount_dto_1.CreateDiscountDto]),
    __metadata("design:returntype", Promise)
], POSDiscountController.prototype, "createDiscount", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_discount_entity_1.CreateUpdateDiscountResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Update Discount by ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, create_discount_dto_1.CreateDiscountDto]),
    __metadata("design:returntype", Promise)
], POSDiscountController.prototype, "updateDiscount", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete Discount by ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSDiscountController.prototype, "deleteDiscount", null);
POSDiscountController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/discount'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/discount'),
    __metadata("design:paramtypes", [pos_discount_logic_1.POSDiscountLogic])
], POSDiscountController);
exports.POSDiscountController = POSDiscountController;
//# sourceMappingURL=pos-discount.controller.js.map