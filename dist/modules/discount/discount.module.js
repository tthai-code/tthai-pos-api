"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiscountModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const pos_discount_controller_1 = require("./controllers/pos-discount.controller");
const pos_discount_logic_1 = require("./logic/pos-discount.logic");
const discount_schema_1 = require("./schemas/discount.schema");
const discount_service_1 = require("./services/discount.service");
let DiscountModule = class DiscountModule {
};
DiscountModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([{ name: 'discount', schema: discount_schema_1.DiscountSchema }])
        ],
        providers: [discount_service_1.DiscountService, pos_discount_logic_1.POSDiscountLogic],
        controllers: [pos_discount_controller_1.POSDiscountController],
        exports: []
    })
], DiscountModule);
exports.DiscountModule = DiscountModule;
//# sourceMappingURL=discount.module.js.map