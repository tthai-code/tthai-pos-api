import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CreateDiscountDto } from '../dto/create-discount.dto';
import { POSPaginateDiscountDto } from '../dto/pos-get-discount.dto';
import { DiscountService } from '../services/discount.service';
export declare class POSDiscountLogic {
    private readonly discountService;
    private readonly restaurantService;
    constructor(discountService: DiscountService, restaurantService: RestaurantService);
    getDiscount(id: string): Promise<import("../schemas/discount.schema").DiscountDocument>;
    getDiscountList(restaurantId: string, query: POSPaginateDiscountDto): Promise<PaginateResult<import("../schemas/discount.schema").DiscountDocument>>;
    createDiscount(restaurantId: string, payload: CreateDiscountDto): Promise<{
        success: boolean;
        id: any;
    }>;
    updateDiscount(id: string, payload: CreateDiscountDto): Promise<{
        success: boolean;
        id: any;
    }>;
    deleteDiscount(id: string): Promise<{
        success: boolean;
    }>;
}
