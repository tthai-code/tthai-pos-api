"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSDiscountLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const discount_service_1 = require("../services/discount.service");
let POSDiscountLogic = class POSDiscountLogic {
    constructor(discountService, restaurantService) {
        this.discountService = discountService;
        this.restaurantService = restaurantService;
    }
    async getDiscount(id) {
        const discount = await this.discountService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            status: 0,
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0
        });
        if (!discount)
            throw new common_1.NotFoundException('Not found discount.');
        return discount;
    }
    async getDiscountList(restaurantId, query) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        return await this.discountService.paginate(query.buildQuery(restaurantId), query, {
            status: 0,
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0
        });
    }
    async createDiscount(restaurantId, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        payload.restaurantId = restaurantId;
        const created = await this.discountService.create(payload);
        return {
            success: true,
            id: created.id
        };
    }
    async updateDiscount(id, payload) {
        const discount = await this.discountService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!discount)
            throw new common_1.NotFoundException('Not found discount.');
        payload.restaurantId = `${discount.restaurantId}`;
        const updated = await this.discountService.update(id, payload);
        return {
            success: true,
            id: updated.id
        };
    }
    async deleteDiscount(id) {
        const discount = await this.discountService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!discount)
            throw new common_1.NotFoundException('Not found discount.');
        await this.discountService.delete(id);
        return { success: true };
    }
};
POSDiscountLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [discount_service_1.DiscountService,
        restaurant_service_1.RestaurantService])
], POSDiscountLogic);
exports.POSDiscountLogic = POSDiscountLogic;
//# sourceMappingURL=pos-discount.logic.js.map