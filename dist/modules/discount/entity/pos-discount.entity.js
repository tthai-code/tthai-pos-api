"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiscountResponse = exports.PaginateDiscountResponse = exports.CreateUpdateDiscountResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const success_entity_1 = require("../../../common/entity/success.entity");
class CreateUpdateDiscountObject extends success_entity_1.SuccessField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreateUpdateDiscountObject.prototype, "id", void 0);
class CreateUpdateDiscountResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CreateUpdateDiscountObject,
        example: {
            success: true,
            id: '63b69a7dc2f8dde0dffce9b9'
        }
    }),
    __metadata("design:type", CreateUpdateDiscountObject)
], CreateUpdateDiscountResponse.prototype, "data", void 0);
exports.CreateUpdateDiscountResponse = CreateUpdateDiscountResponse;
class DiscountObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DiscountObject.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], DiscountObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], DiscountObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], DiscountObject.prototype, "id", void 0);
class PaginateDiscountObject extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [DiscountObject],
        example: [
            {
                discount: 4,
                name: 'Special',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                id: '63b69a7dc2f8dde0dffce9b9'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateDiscountObject.prototype, "results", void 0);
class PaginateDiscountResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: PaginateDiscountObject }),
    __metadata("design:type", PaginateDiscountObject)
], PaginateDiscountResponse.prototype, "data", void 0);
exports.PaginateDiscountResponse = PaginateDiscountResponse;
class DiscountResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: DiscountObject,
        example: {
            discount: 4,
            name: 'Special',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            id: '63b69a7dc2f8dde0dffce9b9'
        }
    }),
    __metadata("design:type", DiscountObject)
], DiscountResponse.prototype, "data", void 0);
exports.DiscountResponse = DiscountResponse;
//# sourceMappingURL=pos-discount.entity.js.map