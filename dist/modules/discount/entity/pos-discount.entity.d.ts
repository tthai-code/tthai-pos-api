import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
import { SuccessField } from 'src/common/entity/success.entity';
declare class CreateUpdateDiscountObject extends SuccessField {
    id: string;
}
export declare class CreateUpdateDiscountResponse extends ResponseDto<CreateUpdateDiscountObject> {
    data: CreateUpdateDiscountObject;
}
declare class DiscountObject {
    discount: number;
    name: string;
    restaurant_id: string;
    id: string;
}
declare class PaginateDiscountObject extends PaginateResponseDto<DiscountObject[]> {
    results: DiscountObject[];
}
export declare class PaginateDiscountResponse extends ResponseDto<PaginateDiscountObject> {
    data: PaginateDiscountObject;
}
export declare class DiscountResponse extends ResponseDto<DiscountObject> {
    data: DiscountObject;
}
export {};
