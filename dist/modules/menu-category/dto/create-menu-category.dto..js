"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateMenuCategoryDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const day_enum_1 = require("../../restaurant-hours/common/day.enum");
class CategoryHoursFields {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '634cfb9f984f966064984acd' }),
    __metadata("design:type", String)
], CategoryHoursFields.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(day_enum_1.DayEnum),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ enum: Object.values(day_enum_1.DayEnum), example: day_enum_1.DayEnum.MONDAY }),
    __metadata("design:type", String)
], CategoryHoursFields.prototype, "label", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '09:00AM' }),
    __metadata("design:type", String)
], CategoryHoursFields.prototype, "opensAt", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '12:00AM' }),
    __metadata("design:type", String)
], CategoryHoursFields.prototype, "closesAt", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)({ example: true }),
    __metadata("design:type", Boolean)
], CategoryHoursFields.prototype, "isSelected", void 0);
class CreateMenuCategoryDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '630e55a0d9c30fd7cdcb424b' }),
    __metadata("design:type", String)
], CreateMenuCategoryDto.prototype, "restaurantId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Best Seller' }),
    __metadata("design:type", String)
], CreateMenuCategoryDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'description test' }),
    __metadata("design:type", String)
], CreateMenuCategoryDto.prototype, "description", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 7 }),
    __metadata("design:type", Number)
], CreateMenuCategoryDto.prototype, "position", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)({ example: true }),
    __metadata("design:type", Boolean)
], CreateMenuCategoryDto.prototype, "isShow", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)({ example: false }),
    __metadata("design:type", Boolean)
], CreateMenuCategoryDto.prototype, "isContainAlcohol", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)({ example: true }),
    __metadata("design:type", Boolean)
], CreateMenuCategoryDto.prototype, "isDisplayOnWebAndApp", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => CategoryHoursFields),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({
        type: () => [CategoryHoursFields]
    }),
    __metadata("design:type", Array)
], CreateMenuCategoryDto.prototype, "hours", void 0);
exports.CreateMenuCategoryDto = CreateMenuCategoryDto;
//# sourceMappingURL=create-menu-category.dto..js.map