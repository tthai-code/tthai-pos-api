import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class GetMenuCategoryListDto {
    readonly restaurantId: string;
    buildQuery(): {
        restaurantId: string;
        isDisplayOnWebAndApp: boolean;
        isShow: boolean;
        status: StatusEnum;
    };
}
export declare class MenuCategoriesPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly restaurant: string;
    readonly status: string;
    buildQuery(id: string): {
        $or: {
            name: {
                $regex: string;
                $options: string;
            };
        }[];
        restaurantId: {
            $eq: string;
        };
        status: string;
    };
    buildQueryPOS(id: string): {
        restaurantId: string;
        isShow: boolean;
        status: StatusEnum;
    };
}
