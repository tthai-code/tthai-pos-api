declare class CategoryHoursFields {
    id: string;
    label: string;
    opensAt: string;
    closesAt: string;
    isSelected: boolean;
}
export declare class CreateMenuCategoryDto {
    readonly restaurantId: string;
    readonly name: string;
    readonly description: string;
    position: number;
    readonly isShow: boolean;
    readonly isContainAlcohol: boolean;
    readonly isDisplayOnWebAndApp: boolean;
    readonly hours: Array<CategoryHoursFields>;
}
export {};
