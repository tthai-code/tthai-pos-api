import { PositionObject } from 'src/modules/menu/dto/update-menu-position.dto';
export declare class UpdateCategoryPositionDto {
    items: PositionObject[];
}
