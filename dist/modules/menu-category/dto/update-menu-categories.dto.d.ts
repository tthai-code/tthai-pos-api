declare class CategoryHoursFields {
    id: string;
    label: string;
    opensAt: string;
    closesAt: string;
    isSelected: boolean;
}
export declare class UpdateMenuCategoryDto {
    readonly name: string;
    readonly position: number;
    readonly description: string;
    readonly isShow: boolean;
    readonly isContainAlcohol: boolean;
    readonly isDisplayOnWebAndApp: boolean;
    readonly hours: Array<CategoryHoursFields>;
}
export {};
