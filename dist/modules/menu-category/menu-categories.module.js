"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuCategoriesModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_hours_module_1 = require("../restaurant-hours/restaurant-hours.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const menu_module_1 = require("../menu/menu.module");
const restaurant_holiday_module_1 = require("../restaurant-holiday/restaurant-holiday.module");
const menu_category_controller_1 = require("./controllers/menu-category.controller");
const pos_menu_category_controller_1 = require("./controllers/pos-menu-category.controller");
const menu_category_public_online_controller_1 = require("./controllers/menu-category-public-online.controller");
const menu_category_logic_1 = require("./logics/menu-category.logic");
const menu_category_public_online_logic_1 = require("./logics/menu-category-public-online-logic");
const category_hours_schema_1 = require("./schemas/category-hours.schema");
const menu_category_schema_1 = require("./schemas/menu-category.schema");
const category_hours_service_1 = require("./services/category-hours.service");
const menu_category_service_1 = require("./services/menu-category.service");
const menu_category_public_online_service_1 = require("./services/menu-category-public-online.service");
const menu_category_open_controller_1 = require("./logics/menu-category-open.controller");
const open_category_controller_1 = require("./controllers/open-category.controller");
const web_setting_module_1 = require("../web-setting/web-setting.module");
let MenuCategoriesModule = class MenuCategoriesModule {
};
MenuCategoriesModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            (0, common_1.forwardRef)(() => restaurant_hours_module_1.RestaurantHoursModule),
            (0, common_1.forwardRef)(() => restaurant_holiday_module_1.RestaurantHolidaysModule),
            (0, common_1.forwardRef)(() => menu_module_1.MenuModule),
            (0, common_1.forwardRef)(() => web_setting_module_1.WebSettingModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'menuCategories', schema: menu_category_schema_1.MenuCategoriesSchema },
                { name: 'categoryHours', schema: category_hours_schema_1.CategoryHoursSchema }
            ])
        ],
        providers: [
            menu_category_service_1.MenuCategoriesService,
            menu_category_public_online_service_1.MenuCategoriesOnlineService,
            menu_category_logic_1.MenuCategoryLogic,
            menu_category_public_online_logic_1.MenuCategoryOnlineLogic,
            category_hours_service_1.CategoryHoursService,
            menu_category_open_controller_1.OpenMenuCategoryLogic
        ],
        controllers: [
            pos_menu_category_controller_1.POSMenuCategoryController,
            menu_category_controller_1.MenuCategoriesController,
            menu_category_public_online_controller_1.MenuCategoriesOnlineController,
            open_category_controller_1.OpenCategoryController
        ],
        exports: [
            menu_category_service_1.MenuCategoriesService,
            category_hours_service_1.CategoryHoursService,
            menu_category_public_online_logic_1.MenuCategoryOnlineLogic
        ]
    })
], MenuCategoriesModule);
exports.MenuCategoriesModule = MenuCategoriesModule;
//# sourceMappingURL=menu-categories.module.js.map