"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSMenuCategoryController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const errorResponse_1 = require("../../../utilities/errorResponse");
const get_menu_categories_dto_1 = require("../dto/get-menu-categories.dto");
const menu_category_entity_1 = require("../entity/menu-category.entity");
const menu_category_service_1 = require("../services/menu-category.service");
let POSMenuCategoryController = class POSMenuCategoryController {
    constructor(menuCategoryService) {
        this.menuCategoryService = menuCategoryService;
        this.menuCategoryPaginateDto = new get_menu_categories_dto_1.MenuCategoriesPaginateDto();
    }
    async getAllCategory() {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.menuCategoryService.getAll(this.menuCategoryPaginateDto.buildQueryPOS(restaurantId), {
            name: 1,
            isContainAlcohol: 1,
            position: 1
        }, { sort: { position: 1 } });
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_category_entity_1.POSMenuCategoryResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', '/v1/pos/menu-category')),
    (0, swagger_1.ApiOperation)({
        summary: 'Get all category for display on POS',
        description: 'use *bearer* `staff_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], POSMenuCategoryController.prototype, "getAllCategory", null);
POSMenuCategoryController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/menu-category'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/menu-category'),
    __metadata("design:paramtypes", [menu_category_service_1.MenuCategoriesService])
], POSMenuCategoryController);
exports.POSMenuCategoryController = POSMenuCategoryController;
//# sourceMappingURL=pos-menu-category.controller.js.map