"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuCategoriesController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const get_menu_categories_dto_1 = require("../dto/get-menu-categories.dto");
const create_menu_category_dto_1 = require("../dto/create-menu-category.dto.");
const menu_category_service_1 = require("../services/menu-category.service");
const update_menu_categories_dto_1 = require("../dto/update-menu-categories.dto");
const menu_category_logic_1 = require("../logics/menu-category.logic");
const category_hours_service_1 = require("../services/category-hours.service");
const menu_category_entity_1 = require("../entity/menu-category.entity");
const update_category_position_dto_1 = require("../dto/update-category-position.dto");
let MenuCategoriesController = class MenuCategoriesController {
    constructor(menuCategoriesService, menuCategoryLogic, categoryHoursService) {
        this.menuCategoriesService = menuCategoriesService;
        this.menuCategoryLogic = menuCategoryLogic;
        this.categoryHoursService = categoryHoursService;
    }
    async getMenuCategories(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.menuCategoriesService.paginate(query.buildQuery(restaurantId), query);
    }
    async getMenuCategory(id) {
        return await this.menuCategoryLogic.getCategoryById(id);
    }
    async createMenuCategory(menuCategory) {
        return this.menuCategoryLogic.createMenuCategory(menuCategory);
    }
    async updateMenuCategory(category, id) {
        return this.menuCategoryLogic.updateMenuCategory(id, category);
    }
    async deleteMenuCategory(id) {
        await this.categoryHoursService.deleteMany({ categoryId: id });
        return this.menuCategoriesService.update(id, { status: status_enum_1.StatusEnum.DELETED });
    }
    async getCategoryHoursForCreate() {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.menuCategoryLogic.getCategoryHoursForCreate(restaurantId);
    }
    async updateCategoryPosition(payload) {
        return await this.menuCategoryLogic.updateCategoryPosition(payload);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_category_entity_1.PaginateMenuCategoryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu Categories List' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_menu_categories_dto_1.MenuCategoriesPaginateDto]),
    __metadata("design:returntype", Promise)
], MenuCategoriesController.prototype, "getMenuCategories", null);
__decorate([
    (0, common_1.Get)(':id/category'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_category_entity_1.GetMenuCategoryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu Category By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuCategoriesController.prototype, "getMenuCategory", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => menu_category_entity_1.CreatedMenuCategoryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Create Menu Category' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_menu_category_dto_1.CreateMenuCategoryDto]),
    __metadata("design:returntype", Promise)
], MenuCategoriesController.prototype, "createMenuCategory", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_category_entity_1.CreatedMenuCategoryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Update Menu Category' }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_menu_categories_dto_1.UpdateMenuCategoryDto, String]),
    __metadata("design:returntype", Promise)
], MenuCategoriesController.prototype, "updateMenuCategory", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_category_entity_1.DeletedMenuCategoryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Delete Menu Category' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuCategoriesController.prototype, "deleteMenuCategory", null);
__decorate([
    (0, common_1.Get)('/hours'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_category_entity_1.CategoryHoursResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get all category hours for create category' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MenuCategoriesController.prototype, "getCategoryHoursForCreate", null);
__decorate([
    (0, common_1.Put)('position/all'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Category Position' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_category_position_dto_1.UpdateCategoryPositionDto]),
    __metadata("design:returntype", Promise)
], MenuCategoriesController.prototype, "updateCategoryPosition", null);
MenuCategoriesController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('menu-category'),
    (0, common_1.Controller)('v1/menu-category'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __metadata("design:paramtypes", [menu_category_service_1.MenuCategoriesService,
        menu_category_logic_1.MenuCategoryLogic,
        category_hours_service_1.CategoryHoursService])
], MenuCategoriesController);
exports.MenuCategoriesController = MenuCategoriesController;
//# sourceMappingURL=menu-category.controller.js.map