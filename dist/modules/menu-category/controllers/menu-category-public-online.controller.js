"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuCategoriesOnlineController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const menu_category_public_online_service_1 = require("../services/menu-category-public-online.service");
const menu_category_public_online_logic_1 = require("../logics/menu-category-public-online-logic");
const category_hours_service_1 = require("../services/category-hours.service");
const get_menu_dto_1 = require("../../menu/dto/get-menu.dto");
const menu_category_entity_1 = require("../entity/menu-category.entity");
const get_menu_categories_dto_1 = require("../dto/get-menu-categories.dto");
const online_category_entity_1 = require("../entity/online-category.entity");
let MenuCategoriesOnlineController = class MenuCategoriesOnlineController {
    constructor(menuCategoriesOnlineService, menuCategoryOnlineLogic, categoryHoursService) {
        this.menuCategoriesOnlineService = menuCategoriesOnlineService;
        this.menuCategoryOnlineLogic = menuCategoryOnlineLogic;
        this.categoryHoursService = categoryHoursService;
    }
    async getMenuCategories(query) {
        return await this.menuCategoryOnlineLogic.getMenuCategoryByRestaurantId(query.restaurantId, query);
    }
    async getMenuCategoryList(query) {
        return await this.menuCategoriesOnlineService.getAll(query.buildQuery(), {
            status: 0,
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0
        }, { sort: { position: 1 } });
    }
    async getMenuCategory(id) {
        return await this.menuCategoryOnlineLogic.getCategoryById(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => online_category_entity_1.GetOnlineCategoryWithMenuResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Menu Categories List Online',
        description: 'use query limit=9'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_menu_dto_1.MenuPaginateDto]),
    __metadata("design:returntype", Promise)
], MenuCategoriesOnlineController.prototype, "getMenuCategories", null);
__decorate([
    (0, common_1.Get)('list'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_category_entity_1.GetOnlineMenuCategoryListResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu Category List' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_menu_categories_dto_1.GetMenuCategoryListDto]),
    __metadata("design:returntype", Promise)
], MenuCategoriesOnlineController.prototype, "getMenuCategoryList", null);
__decorate([
    (0, common_1.Get)(':id/category'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_category_entity_1.GetMenuCategoryResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu Category By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuCategoriesOnlineController.prototype, "getMenuCategory", null);
MenuCategoriesOnlineController = __decorate([
    (0, swagger_1.ApiTags)('menu-category-public-online'),
    (0, common_1.Controller)('v1/public/online/menu-category'),
    __metadata("design:paramtypes", [menu_category_public_online_service_1.MenuCategoriesOnlineService,
        menu_category_public_online_logic_1.MenuCategoryOnlineLogic,
        category_hours_service_1.CategoryHoursService])
], MenuCategoriesOnlineController);
exports.MenuCategoriesOnlineController = MenuCategoriesOnlineController;
//# sourceMappingURL=menu-category-public-online.controller.js.map