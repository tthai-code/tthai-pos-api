import { MenuCategoriesService } from '../services/menu-category.service';
export declare class POSMenuCategoryController {
    private readonly menuCategoryService;
    constructor(menuCategoryService: MenuCategoriesService);
    private readonly menuCategoryPaginateDto;
    getAllCategory(): Promise<any[]>;
}
