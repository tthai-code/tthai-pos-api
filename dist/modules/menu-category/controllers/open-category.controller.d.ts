import { GetMenuCategoryListDto } from '../dto/get-menu-categories.dto';
import { OpenMenuCategoryLogic } from '../logics/menu-category-open.controller';
export declare class OpenCategoryController {
    private readonly openMenuCategoryLogic;
    constructor(openMenuCategoryLogic: OpenMenuCategoryLogic);
    getAllCategory(query: GetMenuCategoryListDto): Promise<any[]>;
}
