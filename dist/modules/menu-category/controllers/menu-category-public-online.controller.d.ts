import { MenuCategoriesOnlineService } from '../services/menu-category-public-online.service';
import { MenuCategoryOnlineLogic } from '../logics/menu-category-public-online-logic';
import { CategoryHoursService } from '../services/category-hours.service';
import { MenuPaginateDto } from 'src/modules/menu/dto/get-menu.dto';
import { GetMenuCategoryListDto } from '../dto/get-menu-categories.dto';
export declare class MenuCategoriesOnlineController {
    private readonly menuCategoriesOnlineService;
    private readonly menuCategoryOnlineLogic;
    private readonly categoryHoursService;
    constructor(menuCategoriesOnlineService: MenuCategoriesOnlineService, menuCategoryOnlineLogic: MenuCategoryOnlineLogic, categoryHoursService: CategoryHoursService);
    getMenuCategories(query: MenuPaginateDto): Promise<any[]>;
    getMenuCategoryList(query: GetMenuCategoryListDto): Promise<any[]>;
    getMenuCategory(id: string): Promise<{
        name: string;
        position: number;
        description: string;
        isShow: boolean;
        isContainAlcohol: boolean;
        isDisplayOnWebAndApp: boolean;
        hours: {
            id: any;
            label: any;
            opensAt: any;
            closesAt: any;
            isSelected: boolean;
        }[];
    }>;
}
