"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenCategoryController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const get_menu_categories_dto_1 = require("../dto/get-menu-categories.dto");
const open_category_entity_1 = require("../entity/open-category.entity");
const menu_category_open_controller_1 = require("../logics/menu-category-open.controller");
let OpenCategoryController = class OpenCategoryController {
    constructor(openMenuCategoryLogic) {
        this.openMenuCategoryLogic = openMenuCategoryLogic;
    }
    async getAllCategory(query) {
        return await this.openMenuCategoryLogic.getCategoryList(query);
    }
};
__decorate([
    (0, common_1.Get)('category'),
    (0, swagger_1.ApiOkResponse)({ type: open_category_entity_1.OpenCategoryListResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get all menu category is display on app.',
        description: 'use basic auth'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_menu_categories_dto_1.GetMenuCategoryListDto]),
    __metadata("design:returntype", Promise)
], OpenCategoryController.prototype, "getAllCategory", null);
OpenCategoryController = __decorate([
    (0, swagger_1.ApiBasicAuth)(),
    (0, swagger_1.ApiTags)('open/category'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    (0, common_1.Controller)('v1/open/menu'),
    __metadata("design:paramtypes", [menu_category_open_controller_1.OpenMenuCategoryLogic])
], OpenCategoryController);
exports.OpenCategoryController = OpenCategoryController;
//# sourceMappingURL=open-category.controller.js.map