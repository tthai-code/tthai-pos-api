import { MenuCategoriesPaginateDto } from '../dto/get-menu-categories.dto';
import { CreateMenuCategoryDto } from '../dto/create-menu-category.dto.';
import { MenuCategoriesService } from '../services/menu-category.service';
import { UpdateMenuCategoryDto } from '../dto/update-menu-categories.dto';
import { MenuCategoryLogic } from '../logics/menu-category.logic';
import { CategoryHoursService } from '../services/category-hours.service';
import { UpdateCategoryPositionDto } from '../dto/update-category-position.dto';
export declare class MenuCategoriesController {
    private readonly menuCategoriesService;
    private readonly menuCategoryLogic;
    private readonly categoryHoursService;
    constructor(menuCategoriesService: MenuCategoriesService, menuCategoryLogic: MenuCategoryLogic, categoryHoursService: CategoryHoursService);
    getMenuCategories(query: MenuCategoriesPaginateDto): Promise<PaginateResult<import("../schemas/menu-category.schema").MenuCategoriesDocument>>;
    getMenuCategory(id: string): Promise<{
        name: string;
        position: number;
        description: string;
        isShow: boolean;
        isContainAlcohol: boolean;
        isDisplayOnWebAndApp: boolean;
        hours: {
            id: any;
            label: any;
            opensAt: any;
            closesAt: any;
            isSelected: boolean;
        }[];
    }>;
    createMenuCategory(menuCategory: CreateMenuCategoryDto): Promise<import("../schemas/menu-category.schema").MenuCategoriesDocument>;
    updateMenuCategory(category: UpdateMenuCategoryDto, id: string): Promise<import("../schemas/menu-category.schema").MenuCategoriesDocument>;
    deleteMenuCategory(id: string): Promise<import("../schemas/menu-category.schema").MenuCategoriesDocument>;
    getCategoryHoursForCreate(): Promise<{
        id: any;
        label: any;
        opensAt: any;
        closesAt: any;
        isSelected: boolean;
    }[]>;
    updateCategoryPosition(payload: UpdateCategoryPositionDto): Promise<{
        success: boolean;
    }>;
}
