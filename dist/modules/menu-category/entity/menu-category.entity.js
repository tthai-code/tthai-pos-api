"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOnlineMenuCategoryListResponse = exports.POSMenuCategoryResponse = exports.CategoryHoursResponse = exports.DeletedMenuCategoryResponse = exports.CreatedMenuCategoryResponse = exports.GetMenuCategoryWithMenuResponse = exports.GetMenuCategoryResponse = exports.PaginateMenuCategoryResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
const menu_entity_1 = require("../../menu/entity/menu.entity");
class CategoryHoursFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryHoursFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryHoursFields.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryHoursFields.prototype, "opens_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryHoursFields.prototype, "closes_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CategoryHoursFields.prototype, "is_selected", void 0);
class POSMenuCategoryFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], POSMenuCategoryFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], POSMenuCategoryFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], POSMenuCategoryFields.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], POSMenuCategoryFields.prototype, "is_contain_alcohol", void 0);
class BaseMenuCategoryFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseMenuCategoryFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseMenuCategoryFields.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseMenuCategoryFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BaseMenuCategoryFields.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BaseMenuCategoryFields.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], BaseMenuCategoryFields.prototype, "is_show", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], BaseMenuCategoryFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], BaseMenuCategoryFields.prototype, "is_display_on_web_and_app", void 0);
class CreatedMenuCategoryFields extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedMenuCategoryFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedMenuCategoryFields.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedMenuCategoryFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedMenuCategoryFields.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CreatedMenuCategoryFields.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CreatedMenuCategoryFields.prototype, "is_show", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CreatedMenuCategoryFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CreatedMenuCategoryFields.prototype, "is_display_on_web_and_app", void 0);
class MenuCategoryWithHoursFields extends BaseMenuCategoryFields {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [CategoryHoursFields] }),
    __metadata("design:type", Array)
], MenuCategoryWithHoursFields.prototype, "hours", void 0);
class MenuCategoryWithMenu extends BaseMenuCategoryFields {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [CategoryHoursFields] }),
    __metadata("design:type", Array)
], MenuCategoryWithMenu.prototype, "menus", void 0);
class PaginateMenuCategoryFields extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PaginateMenuCategoryFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PaginateMenuCategoryFields.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PaginateMenuCategoryFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PaginateMenuCategoryFields.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], PaginateMenuCategoryFields.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], PaginateMenuCategoryFields.prototype, "is_show", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], PaginateMenuCategoryFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], PaginateMenuCategoryFields.prototype, "is_display_on_web_and_app", void 0);
class PaginateMenuCategoryResponse extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [PaginateMenuCategoryFields],
        example: [
            {
                status: 'active',
                is_display_on_web_and_app: true,
                is_contain_alcohol: false,
                is_show: true,
                description: 'test desc',
                position: 2,
                name: 'Best Seller',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                created_at: '2022-10-17T07:38:35.687Z',
                updated_at: '2022-10-17T08:02:32.237Z',
                updated_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                created_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                id: '634d067bcc64f608779162f5'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateMenuCategoryResponse.prototype, "results", void 0);
exports.PaginateMenuCategoryResponse = PaginateMenuCategoryResponse;
class GetMenuCategoryResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: MenuCategoryWithHoursFields,
        example: {
            name: 'Best Seller',
            position: 2,
            description: 'test desc',
            is_show: true,
            is_contain_alcohol: false,
            is_display_on_web_and_app: true,
            hours: [
                {
                    id: '634d03669a40151f6e4243a0',
                    label: 'MONDAY',
                    opens_at: '09:00AM',
                    closes_at: '12:00AM',
                    is_selected: true
                },
                {
                    id: '634d03669a40151f6e4243a1',
                    label: 'MONDAY',
                    opens_at: '03:00PM',
                    closes_at: '09:00PM',
                    is_selected: true
                },
                {
                    id: '634d03669a40151f6e4243a2',
                    label: 'TUESDAY',
                    opens_at: '08:00AM',
                    closes_at: '05:00PM',
                    is_selected: false
                },
                {
                    id: '634d03669a40151f6e4243a3',
                    label: 'TUESDAY',
                    opens_at: '11:00PM',
                    closes_at: '00:30AM',
                    is_selected: false
                }
            ]
        }
    }),
    __metadata("design:type", MenuCategoryWithHoursFields)
], GetMenuCategoryResponse.prototype, "data", void 0);
exports.GetMenuCategoryResponse = GetMenuCategoryResponse;
class GetMenuCategoryWithMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CreatedMenuCategoryFields,
        example: {
            status: 'active',
            is_display_on_web_and_app: true,
            is_contain_alcohol: false,
            is_show: true,
            description: 'test desc',
            position: 2,
            name: 'Best Seller',
            menus: [
                {
                    id: '6310667a062dfd651b6e5023',
                    status: 'active',
                    note: '',
                    cover_image: null,
                    image_urls: [
                        'https://storage.googleapis.com/tthai-pos-staging/upload/166456465419581603.webp'
                    ],
                    price: 12,
                    category: ['6336bb21c64b8b0998892950', '634d067bcc64f608779162f5'],
                    name: 'Mushroom Dumplings',
                    restaurant_id: '630eff5751c2eac55f52662c',
                    __v: 3,
                    description: 'Pan grilled dumplings stuffed with shiitake mushroom and chive. Served with ginger sauce.',
                    native_name: 'เกี๊ยวเห็ดหอม',
                    position: 999
                }
            ],
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-10-17T07:34:07.985Z',
            updated_at: '2022-10-17T07:34:07.985Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '634d056f24810adce81a5620'
        }
    }),
    __metadata("design:type", CreatedMenuCategoryFields)
], GetMenuCategoryWithMenuResponse.prototype, "data", void 0);
exports.GetMenuCategoryWithMenuResponse = GetMenuCategoryWithMenuResponse;
class CreatedMenuCategoryResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CreatedMenuCategoryFields,
        example: {
            status: 'active',
            is_display_on_web_and_app: true,
            is_contain_alcohol: false,
            is_show: true,
            description: 'test desc',
            position: 2,
            name: 'Best Seller',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-10-17T07:34:07.985Z',
            updated_at: '2022-10-17T07:34:07.985Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '634d056f24810adce81a5620'
        }
    }),
    __metadata("design:type", CreatedMenuCategoryFields)
], CreatedMenuCategoryResponse.prototype, "data", void 0);
exports.CreatedMenuCategoryResponse = CreatedMenuCategoryResponse;
class DeletedMenuCategoryResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CreatedMenuCategoryFields,
        example: {
            status: 'deleted',
            is_display_on_web_and_app: true,
            is_contain_alcohol: false,
            is_show: true,
            description: 'test desc',
            position: 2,
            name: 'Best Seller',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-10-17T07:34:07.985Z',
            updated_at: '2022-10-17T07:34:07.985Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '634d056f24810adce81a5620'
        }
    }),
    __metadata("design:type", CreatedMenuCategoryFields)
], DeletedMenuCategoryResponse.prototype, "data", void 0);
exports.DeletedMenuCategoryResponse = DeletedMenuCategoryResponse;
class CategoryHoursResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [CategoryHoursFields],
        example: [
            {
                id: '634d0c62eb8b609259fb3c72',
                label: 'MONDAY',
                opens_at: '09:00AM',
                closes_at: '12:00AM',
                is_selected: false
            },
            {
                id: '634d0c62eb8b609259fb3c73',
                label: 'MONDAY',
                opens_at: '03:00PM',
                closes_at: '09:00PM',
                is_selected: false
            },
            {
                id: '634d0c62eb8b609259fb3c74',
                label: 'TUESDAY',
                opens_at: '08:00AM',
                closes_at: '05:00PM',
                is_selected: false
            },
            {
                id: '634d0c62eb8b609259fb3c75',
                label: 'TUESDAY',
                opens_at: '11:00PM',
                closes_at: '00:30AM',
                is_selected: false
            }
        ]
    }),
    __metadata("design:type", Array)
], CategoryHoursResponse.prototype, "data", void 0);
exports.CategoryHoursResponse = CategoryHoursResponse;
class POSMenuCategoryResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [POSMenuCategoryFields],
        example: [
            {
                is_contain_alcohol: false,
                position: 2,
                name: 'Best Seller',
                id: '634d067bcc64f608779162f5'
            }
        ]
    }),
    __metadata("design:type", Array)
], POSMenuCategoryResponse.prototype, "data", void 0);
exports.POSMenuCategoryResponse = POSMenuCategoryResponse;
class OnlineMenuCategoryFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineMenuCategoryFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineMenuCategoryFields.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineMenuCategoryFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineMenuCategoryFields.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OnlineMenuCategoryFields.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], OnlineMenuCategoryFields.prototype, "is_show", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], OnlineMenuCategoryFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], OnlineMenuCategoryFields.prototype, "is_display_on_web_and_app", void 0);
class GetOnlineMenuCategoryListResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [OnlineMenuCategoryFields],
        example: [
            {
                name: 'Appetizers',
                restaurant_id: '630eff5751c2eac55f52662c',
                is_contain_alcohol: false,
                is_display_on_web_and_app: true,
                is_show: true,
                position: 1,
                description: '',
                id: '6336bb21c64b8b0998892950'
            },
            {
                name: 'Soup',
                restaurant_id: '630eff5751c2eac55f52662c',
                is_contain_alcohol: false,
                is_display_on_web_and_app: true,
                is_show: true,
                position: 2,
                description: '',
                id: '6337412cc64b8b0998892c62'
            }
        ]
    }),
    __metadata("design:type", Array)
], GetOnlineMenuCategoryListResponse.prototype, "data", void 0);
exports.GetOnlineMenuCategoryListResponse = GetOnlineMenuCategoryListResponse;
//# sourceMappingURL=menu-category.entity.js.map