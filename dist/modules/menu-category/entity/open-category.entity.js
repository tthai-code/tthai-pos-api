"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenCategoryListResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class CategoryObject {
}
class OpenCategoryListResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CategoryObject,
        example: [
            {
                name: 'Appetizers',
                restaurant_id: '630eff5751c2eac55f52662c',
                is_contain_alcohol: false,
                description: '',
                id: '6336bb21c64b8b0998892950'
            },
            {
                name: 'Soup',
                restaurant_id: '630eff5751c2eac55f52662c',
                is_contain_alcohol: false,
                description: '',
                id: '6337412cc64b8b0998892c62'
            }
        ]
    }),
    __metadata("design:type", Array)
], OpenCategoryListResponse.prototype, "data", void 0);
exports.OpenCategoryListResponse = OpenCategoryListResponse;
//# sourceMappingURL=open-category.entity.js.map