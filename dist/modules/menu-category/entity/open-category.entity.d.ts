import { ResponseDto } from 'src/common/entity/response.entity';
declare class CategoryObject {
    name: string;
    restaurant_id: string;
    is_contain_alcohol: boolean;
    description: string;
    id: string;
}
export declare class OpenCategoryListResponse extends ResponseDto<CategoryObject[]> {
    data: CategoryObject[];
}
export {};
