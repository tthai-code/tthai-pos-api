"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOnlineCategoryWithMenuResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class MenuObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuObject.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuObject.prototype, "note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuObject.prototype, "cover_image", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], MenuObject.prototype, "image_urls", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], MenuObject.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], MenuObject.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuObject.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuObject.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], MenuObject.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], MenuObject.prototype, "is_available", void 0);
class CategoryObjectWithMenu {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryObjectWithMenu.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CategoryObjectWithMenu.prototype, "is_display_on_web_and_app", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CategoryObjectWithMenu.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CategoryObjectWithMenu.prototype, "is_show", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CategoryObjectWithMenu.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryObjectWithMenu.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryObjectWithMenu.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryObjectWithMenu.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryObjectWithMenu.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CategoryObjectWithMenu.prototype, "is_available", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [MenuObject] }),
    __metadata("design:type", Array)
], CategoryObjectWithMenu.prototype, "menus", void 0);
class GetOnlineCategoryWithMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [CategoryObjectWithMenu],
        example: [
            {
                status: 'active',
                is_display_on_web_and_app: true,
                is_contain_alcohol: false,
                is_show: true,
                position: 1,
                name: 'Appetizers',
                restaurant_id: '630eff5751c2eac55f52662c',
                description: '',
                id: '6336bb21c64b8b0998892950',
                is_available: true,
                menus: [
                    {
                        status: 'active',
                        note: '',
                        cover_image: null,
                        image_urls: [],
                        price: 13,
                        category: ['6336bb21c64b8b0998892950'],
                        name: 'Chicken Satay',
                        restaurant_id: '630eff5751c2eac55f52662c',
                        description: 'test desc',
                        native_name: 'test nativeName',
                        position: 999,
                        id: '63373f60c64b8b0998892bd7',
                        is_available: true
                    }
                ]
            }
        ]
    }),
    __metadata("design:type", Array)
], GetOnlineCategoryWithMenuResponse.prototype, "data", void 0);
exports.GetOnlineCategoryWithMenuResponse = GetOnlineCategoryWithMenuResponse;
//# sourceMappingURL=online-category.entity.js.map