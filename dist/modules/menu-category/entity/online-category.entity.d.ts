import { ResponseDto } from 'src/common/entity/response.entity';
declare class MenuObject {
    status: string;
    note: string;
    cover_image: string;
    image_urls: string[];
    price: number;
    category: string[];
    name: string;
    restaurant_id: string;
    description: string;
    native_name: string;
    position: number;
    id: string;
    is_available: boolean;
}
declare class CategoryObjectWithMenu {
    status: string;
    is_display_on_web_and_app: boolean;
    is_contain_alcohol: boolean;
    is_show: boolean;
    position: number;
    name: string;
    restaurant_id: string;
    description: string;
    id: string;
    is_available: boolean;
    menus: MenuObject[];
}
export declare class GetOnlineCategoryWithMenuResponse extends ResponseDto<CategoryObjectWithMenu[]> {
    data: CategoryObjectWithMenu[];
}
export {};
