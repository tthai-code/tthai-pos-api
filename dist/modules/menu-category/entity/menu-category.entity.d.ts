import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
declare class CategoryHoursFields {
    id: string;
    label: string;
    opens_at: string;
    closes_at: string;
    is_selected: boolean;
}
declare class POSMenuCategoryFields {
    id: string;
    name: string;
    position: number;
    is_contain_alcohol: boolean;
}
declare class BaseMenuCategoryFields {
    id: string;
    restaurant_id: string;
    name: string;
    description: string;
    position: number;
    is_show: boolean;
    is_contain_alcohol: boolean;
    is_display_on_web_and_app: boolean;
}
declare class CreatedMenuCategoryFields extends TimestampResponseDto {
    id: string;
    restaurant_id: string;
    name: string;
    description: string;
    position: number;
    is_show: boolean;
    is_contain_alcohol: boolean;
    is_display_on_web_and_app: boolean;
}
declare class MenuCategoryWithHoursFields extends BaseMenuCategoryFields {
    hours: Array<CategoryHoursFields>;
}
declare class PaginateMenuCategoryFields extends TimestampResponseDto {
    id: string;
    restaurant_id: string;
    name: string;
    description: string;
    position: number;
    is_show: boolean;
    is_contain_alcohol: boolean;
    is_display_on_web_and_app: boolean;
}
export declare class PaginateMenuCategoryResponse extends PaginateResponseDto<Array<PaginateMenuCategoryFields>> {
    results: Array<PaginateMenuCategoryFields>;
}
export declare class GetMenuCategoryResponse extends ResponseDto<MenuCategoryWithHoursFields> {
    data: MenuCategoryWithHoursFields;
}
export declare class GetMenuCategoryWithMenuResponse extends ResponseDto<CreatedMenuCategoryFields> {
    data: CreatedMenuCategoryFields;
}
export declare class CreatedMenuCategoryResponse extends ResponseDto<CreatedMenuCategoryFields> {
    data: CreatedMenuCategoryFields;
}
export declare class DeletedMenuCategoryResponse extends ResponseDto<CreatedMenuCategoryFields> {
    data: CreatedMenuCategoryFields;
}
export declare class CategoryHoursResponse extends ResponseDto<Array<CategoryHoursFields>> {
    data: Array<CategoryHoursFields>;
}
export declare class POSMenuCategoryResponse extends ResponseDto<Array<POSMenuCategoryFields>> {
    data: Array<POSMenuCategoryFields>;
}
declare class OnlineMenuCategoryFields {
    id: string;
    restaurant_id: string;
    name: string;
    description: string;
    position: number;
    is_show: boolean;
    is_contain_alcohol: boolean;
    is_display_on_web_and_app: boolean;
}
export declare class GetOnlineMenuCategoryListResponse extends ResponseDto<OnlineMenuCategoryFields[]> {
    data: OnlineMenuCategoryFields[];
}
export {};
