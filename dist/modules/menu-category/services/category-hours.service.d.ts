import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { CategoryHoursDocument } from '../schemas/category-hours.schema';
export declare class CategoryHoursService {
    private readonly CategoryHoursModel;
    private request;
    constructor(CategoryHoursModel: Model<CategoryHoursDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<CategoryHoursDocument | any>;
    resolveByCondition(condition: any, project?: any): Promise<CategoryHoursDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<CategoryHoursDocument>;
    getSession(): Promise<ClientSession>;
    create(payload: any): Promise<CategoryHoursDocument>;
    update(id: string, MenuCategories: any): Promise<CategoryHoursDocument>;
    delete(id: string): Promise<CategoryHoursDocument>;
    createMany(payload: any): Promise<CategoryHoursDocument>;
    deleteMany(condition: any): Promise<void>;
    transactionDeleteAll(condition: any, session: any): Promise<void>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<CategoryHoursDocument>;
}
