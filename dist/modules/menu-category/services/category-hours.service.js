"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryHoursService = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const status_enum_1 = require("../../../common/enum/status.enum");
let CategoryHoursService = class CategoryHoursService {
    constructor(CategoryHoursModel, request) {
        this.CategoryHoursModel = CategoryHoursModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.CategoryHoursModel.findById({ _id: id });
    }
    async resolveByCondition(condition, project) {
        return this.CategoryHoursModel.find(condition, project);
    }
    async isExists(condition) {
        const result = await this.CategoryHoursModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.CategoryHoursModel;
    }
    async getSession() {
        await this.CategoryHoursModel.createCollection();
        return this.CategoryHoursModel.startSession();
    }
    async create(payload) {
        const document = new this.CategoryHoursModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async update(id, MenuCategories) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, MenuCategories)).save();
    }
    async delete(id) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set({ status: status_enum_1.StatusEnum.DELETED }).save();
    }
    async createMany(payload) {
        return this.CategoryHoursModel.insertMany(payload);
    }
    async deleteMany(condition) {
        return this.CategoryHoursModel.deleteMany(condition);
    }
    async transactionDeleteAll(condition, session) {
        return this.CategoryHoursModel.deleteMany(condition, { session });
    }
    getAll(condition, project) {
        return this.CategoryHoursModel.find(condition, project);
    }
    findById(id) {
        return this.CategoryHoursModel.findById(id);
    }
    findOne(condition) {
        return this.CategoryHoursModel.findOne(condition);
    }
};
CategoryHoursService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('categoryHours')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], CategoryHoursService);
exports.CategoryHoursService = CategoryHoursService;
//# sourceMappingURL=category-hours.service.js.map