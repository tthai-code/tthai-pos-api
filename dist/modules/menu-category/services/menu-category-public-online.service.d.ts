import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { MenuCategoriesDocument } from '../schemas/menu-category.schema';
import { MenuCategoriesPaginateDto } from '../dto/get-menu-categories.dto';
export declare class MenuCategoriesOnlineService {
    private readonly MenuCategoriesModel;
    private request;
    constructor(MenuCategoriesModel: PaginateModel<MenuCategoriesDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<MenuCategoriesDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<MenuCategoriesDocument>;
    getSession(): Promise<ClientSession>;
    create(payload: any): Promise<MenuCategoriesDocument>;
    update(id: string, MenuCategories: any): Promise<MenuCategoriesDocument>;
    delete(id: string): Promise<MenuCategoriesDocument>;
    getAll(condition: any, project?: any, options?: any): Promise<any[]>;
    getAllWithSorted(condition: any, project?: any, sort?: {
        position: number;
    }): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<MenuCategoriesDocument>;
    paginate(query: any, queryParam: MenuCategoriesPaginateDto, select?: any): Promise<PaginateResult<MenuCategoriesDocument>>;
}
