import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type MenuCategoriesDocument = MenuCategoriesFields & Document;
export declare class MenuCategoriesFields {
    restaurantId: Types.ObjectId;
    name: string;
    description: string;
    position: number;
    isShow: boolean;
    isContainAlcohol: boolean;
    isDisplayOnWebAndApp: boolean;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const MenuCategoriesSchema: MongooseSchema<Document<MenuCategoriesFields, any, any>, import("mongoose").Model<Document<MenuCategoriesFields, any, any>, any, any, any>, any, any>;
