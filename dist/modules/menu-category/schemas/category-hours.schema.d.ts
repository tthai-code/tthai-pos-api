import { Types, Schema as MongooseSchema, Document } from 'mongoose';
export declare type CategoryHoursDocument = CategoryHoursFields & Document;
export declare class CategoryHoursFields {
    restaurantId: Types.ObjectId;
    categoryId: Types.ObjectId;
    restaurantPeriodId: Types.ObjectId;
}
export declare const CategoryHoursSchema: MongooseSchema<Document<CategoryHoursFields, any, any>, import("mongoose").Model<Document<CategoryHoursFields, any, any>, any, any, any>, any, any>;
