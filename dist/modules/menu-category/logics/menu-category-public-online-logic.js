"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuCategoryOnlineLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs = require("dayjs");
const utc = require("dayjs/plugin/utc");
const timezone = require("dayjs/plugin/timezone");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_day_service_1 = require("../../restaurant-hours/services/restaurant-day.service");
const restaurant_period_service_1 = require("../../restaurant-hours/services/restaurant-period.service");
const restaurant_holiday_service_1 = require("../../restaurant-holiday/services/restaurant-holiday.service");
const menu_service_1 = require("../../menu/services/menu.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const category_hours_service_1 = require("../services/category-hours.service");
const menu_category_service_1 = require("../services/menu-category.service");
const menu_category_public_online_service_1 = require("../services/menu-category-public-online.service");
const dateTime_1 = require("../../../utilities/dateTime");
const get_menu_dto_1 = require("../../menu/dto/get-menu.dto");
const web_setting_service_1 = require("../../web-setting/services/web-setting.service");
const restaurant_period_schema_1 = require("../../restaurant-hours/schemas/restaurant-period.schema");
dayjs.extend(utc);
dayjs.extend(timezone);
let MenuCategoryOnlineLogic = class MenuCategoryOnlineLogic {
    constructor(menuCategoryService, menuCategoriesOnlineService, categoryHoursService, restaurantService, restaurantPeriodService, restaurantHolidaysService, restaurantDayService, menuService, webSettingService) {
        this.menuCategoryService = menuCategoryService;
        this.menuCategoriesOnlineService = menuCategoriesOnlineService;
        this.categoryHoursService = categoryHoursService;
        this.restaurantService = restaurantService;
        this.restaurantPeriodService = restaurantPeriodService;
        this.restaurantHolidaysService = restaurantHolidaysService;
        this.restaurantDayService = restaurantDayService;
        this.menuService = menuService;
        this.webSettingService = webSettingService;
    }
    filterAvailablePeriod(timeZone, restaurantPeriods) {
        const day = dayjs().tz(timeZone);
        return restaurantPeriods.filter((item) => {
            var _a, _b;
            const [openHour, openMinute] = (_a = item === null || item === void 0 ? void 0 : item.opensAt) === null || _a === void 0 ? void 0 : _a.split(':');
            const [closeHour, closeMinute] = (_b = item === null || item === void 0 ? void 0 : item.closesAt) === null || _b === void 0 ? void 0 : _b.split(':');
            const opensAt = day
                .hour(+(Number(openHour) - 1))
                .minute(+openMinute)
                .second(0)
                .millisecond(0)
                .valueOf();
            const closesAt = day
                .hour(+(Number(closeHour) - 1))
                .minute(+closeMinute)
                .second(0)
                .millisecond(0)
                .valueOf();
            const now = day.valueOf();
            return !(now < opensAt || now > closesAt);
        });
    }
    async getAvailableStatusForCategory(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const dayOfWeekString = await (0, dateTime_1.GetDayStringByDay)(dayjs().tz(restaurant.timeZone).day());
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE,
            label: dayOfWeekString
        });
        const availablePeriod = this.filterAvailablePeriod(restaurant.timeZone, restaurantPeriods);
        const periodIds = availablePeriod.map((item) => item.id);
        const categoryHours = await this.categoryHoursService.getAll({
            restaurantPeriodId: { $in: periodIds }
        });
        const categoryIds = await categoryHours.map((item) => `${item.categoryId}`);
        return categoryIds;
    }
    async getMenuCategoryByRestaurantId(restaurantId, queryParam) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const dayOfWeekString = await (0, dateTime_1.GetDayStringByDay)(dayjs().tz(restaurant.timeZone).day());
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE,
            label: dayOfWeekString
        });
        const availablePeriod = this.filterAvailablePeriod(restaurant.timeZone, restaurantPeriods);
        const periodIds = availablePeriod.map((item) => item.id);
        const categoryHours = await this.categoryHoursService.getAll({
            restaurantPeriodId: { $in: periodIds }
        });
        const categoryIds = await categoryHours.map((item) => `${item.categoryId}`);
        const categories = await this.menuCategoriesOnlineService.getAll({
            restaurantId,
            isShow: true,
            isDisplayOnWebAndApp: true,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { createdAt: 0, updatedAt: 0, updatedBy: 0, createdBy: 0 }, { sort: { position: 1 } });
        const webSetting = await this.webSettingService.findOne({ restaurantId });
        const { isOnlineOrdering } = webSetting;
        const categoriesWithMenu = [];
        for (let i = 0; i < categories.length; i++) {
            const isAvailable = categoryIds.includes(`${categories[i]._id}`);
            const menus = await this.menuService.paginate({ category: categories[i].id, status: status_enum_1.StatusEnum.ACTIVE }, queryParam, { createdAt: 0, updatedAt: 0, updatedBy: 0, createdBy: 0 });
            if (menus.docs.length > 0) {
                categoriesWithMenu.push({
                    status: categories[i].status,
                    isDisplayOnWebAndApp: categories[i].isDisplayOnWebAndApp,
                    isContainAlcohol: categories[i].isContainAlcohol,
                    isShow: categories[i].isShow,
                    position: categories[i].position,
                    name: categories[i].name,
                    restaurantId: categories[i].restaurantId,
                    description: categories[i].description,
                    id: categories[i]._id,
                    isAvailable: isOnlineOrdering && isAvailable,
                    menus: menus.docs.map((item) => (Object.assign(Object.assign({}, item.toObject()), { isAvailable: isOnlineOrdering && isAvailable })))
                });
            }
        }
        return categoriesWithMenu;
    }
    async getCategoryById(id) {
        const category = await this.menuCategoryService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!category)
            throw new common_1.NotFoundException('Not found menu category.');
        const restaurantDays = await this.restaurantDayService.getAll({ restaurantId: category.restaurantId, isClosed: false }, { _id: 0, label: 1, isClosed: 1 });
        if (restaurantDays.length <= 0) {
            return {
                name: category.name,
                position: category.position,
                description: category.description,
                isShow: category.isShow,
                isContainAlcohol: category.isContainAlcohol,
                isDisplayOnWebAndApp: category.isDisplayOnWebAndApp,
                hours: []
            };
        }
        const openDays = restaurantDays.map((item) => item.label);
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId: category.restaurantId,
            label: { $in: openDays },
            status: status_enum_1.StatusEnum.ACTIVE
        }, { label: 1, opensAt: 1, closesAt: 1 });
        const categoryHours = await this.categoryHoursService.getAll({
            categoryId: id
        });
        const selectedPeriod = categoryHours.map((selected) => `${selected.restaurantPeriodId}`);
        const hours = restaurantPeriods.map((item) => ({
            id: item._id,
            label: item.label,
            opensAt: item.opensAt,
            closesAt: item.closesAt,
            isSelected: selectedPeriod.includes(`${item._id}`)
        }));
        return {
            name: category.name,
            position: category.position,
            description: category.description,
            isShow: category.isShow,
            isContainAlcohol: category.isContainAlcohol,
            isDisplayOnWebAndApp: category.isDisplayOnWebAndApp,
            hours
        };
    }
};
MenuCategoryOnlineLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [menu_category_service_1.MenuCategoriesService,
        menu_category_public_online_service_1.MenuCategoriesOnlineService,
        category_hours_service_1.CategoryHoursService,
        restaurant_service_1.RestaurantService,
        restaurant_period_service_1.RestaurantPeriodService,
        restaurant_holiday_service_1.RestaurantHolidaysService,
        restaurant_day_service_1.RestaurantDayService,
        menu_service_1.MenuService,
        web_setting_service_1.WebSettingService])
], MenuCategoryOnlineLogic);
exports.MenuCategoryOnlineLogic = MenuCategoryOnlineLogic;
//# sourceMappingURL=menu-category-public-online-logic.js.map