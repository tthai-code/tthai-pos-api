"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenMenuCategoryLogic = void 0;
const common_1 = require("@nestjs/common");
const restaurant_holiday_service_1 = require("../../restaurant-holiday/services/restaurant-holiday.service");
const restaurant_day_service_1 = require("../../restaurant-hours/services/restaurant-day.service");
const restaurant_period_service_1 = require("../../restaurant-hours/services/restaurant-period.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const category_hours_service_1 = require("../services/category-hours.service");
const menu_category_service_1 = require("../services/menu-category.service");
let OpenMenuCategoryLogic = class OpenMenuCategoryLogic {
    constructor(menuCategoryService, categoryHoursService, restaurantService, restaurantPeriodService, restaurantHolidaysService, restaurantDayService) {
        this.menuCategoryService = menuCategoryService;
        this.categoryHoursService = categoryHoursService;
        this.restaurantService = restaurantService;
        this.restaurantPeriodService = restaurantPeriodService;
        this.restaurantHolidaysService = restaurantHolidaysService;
        this.restaurantDayService = restaurantDayService;
    }
    async getCategoryList(query) {
        const categories = await this.menuCategoryService.getAll(query.buildQuery(), {
            position: 0,
            isShow: 0,
            isDisplayOnWebAndApp: 0,
            status: 0,
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0
        });
        return categories;
    }
};
OpenMenuCategoryLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [menu_category_service_1.MenuCategoriesService,
        category_hours_service_1.CategoryHoursService,
        restaurant_service_1.RestaurantService,
        restaurant_period_service_1.RestaurantPeriodService,
        restaurant_holiday_service_1.RestaurantHolidaysService,
        restaurant_day_service_1.RestaurantDayService])
], OpenMenuCategoryLogic);
exports.OpenMenuCategoryLogic = OpenMenuCategoryLogic;
//# sourceMappingURL=menu-category-open.controller.js.map