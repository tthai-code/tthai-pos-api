import { RestaurantHolidaysService } from 'src/modules/restaurant-holiday/services/restaurant-holiday.service';
import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service';
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CategoryHoursService } from '../services/category-hours.service';
import { MenuCategoriesService } from '../services/menu-category.service';
import { GetMenuCategoryListDto } from '../dto/get-menu-categories.dto';
export declare class OpenMenuCategoryLogic {
    private readonly menuCategoryService;
    private readonly categoryHoursService;
    private readonly restaurantService;
    private readonly restaurantPeriodService;
    private readonly restaurantHolidaysService;
    private readonly restaurantDayService;
    constructor(menuCategoryService: MenuCategoriesService, categoryHoursService: CategoryHoursService, restaurantService: RestaurantService, restaurantPeriodService: RestaurantPeriodService, restaurantHolidaysService: RestaurantHolidaysService, restaurantDayService: RestaurantDayService);
    getCategoryList(query: GetMenuCategoryListDto): Promise<any[]>;
}
