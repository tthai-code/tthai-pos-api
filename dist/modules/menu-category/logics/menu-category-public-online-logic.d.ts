import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service';
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service';
import { RestaurantHolidaysService } from 'src/modules/restaurant-holiday/services/restaurant-holiday.service';
import { MenuService } from 'src/modules/menu/services/menu.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CategoryHoursService } from '../services/category-hours.service';
import { MenuCategoriesService } from '../services/menu-category.service';
import { MenuCategoriesOnlineService } from '../services/menu-category-public-online.service';
import { MenuPaginateDto } from 'src/modules/menu/dto/get-menu.dto';
import { WebSettingService } from 'src/modules/web-setting/services/web-setting.service';
export declare class MenuCategoryOnlineLogic {
    private readonly menuCategoryService;
    private readonly menuCategoriesOnlineService;
    private readonly categoryHoursService;
    private readonly restaurantService;
    private readonly restaurantPeriodService;
    private readonly restaurantHolidaysService;
    private readonly restaurantDayService;
    private readonly menuService;
    private readonly webSettingService;
    constructor(menuCategoryService: MenuCategoriesService, menuCategoriesOnlineService: MenuCategoriesOnlineService, categoryHoursService: CategoryHoursService, restaurantService: RestaurantService, restaurantPeriodService: RestaurantPeriodService, restaurantHolidaysService: RestaurantHolidaysService, restaurantDayService: RestaurantDayService, menuService: MenuService, webSettingService: WebSettingService);
    private filterAvailablePeriod;
    getAvailableStatusForCategory(restaurantId: string): Promise<string[]>;
    getMenuCategoryByRestaurantId(restaurantId: string, queryParam: MenuPaginateDto): Promise<any[]>;
    getCategoryById(id: string): Promise<{
        name: string;
        position: number;
        description: string;
        isShow: boolean;
        isContainAlcohol: boolean;
        isDisplayOnWebAndApp: boolean;
        hours: {
            id: any;
            label: any;
            opensAt: any;
            closesAt: any;
            isSelected: boolean;
        }[];
    }>;
}
