import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service';
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CreateMenuCategoryDto } from '../dto/create-menu-category.dto.';
import { UpdateCategoryPositionDto } from '../dto/update-category-position.dto';
import { UpdateMenuCategoryDto } from '../dto/update-menu-categories.dto';
import { CategoryHoursService } from '../services/category-hours.service';
import { MenuCategoriesService } from '../services/menu-category.service';
export declare class MenuCategoryLogic {
    private readonly menuCategoryService;
    private readonly categoryHoursService;
    private readonly restaurantService;
    private readonly restaurantPeriodService;
    private readonly restaurantDayService;
    constructor(menuCategoryService: MenuCategoriesService, categoryHoursService: CategoryHoursService, restaurantService: RestaurantService, restaurantPeriodService: RestaurantPeriodService, restaurantDayService: RestaurantDayService);
    createMenuCategory(createCategory: CreateMenuCategoryDto): Promise<import("../schemas/menu-category.schema").MenuCategoriesDocument>;
    getCategoryHoursForCreate(restaurantId: string): Promise<{
        id: any;
        label: any;
        opensAt: any;
        closesAt: any;
        isSelected: boolean;
    }[]>;
    getCategoryById(id: string): Promise<{
        name: string;
        position: number;
        description: string;
        isShow: boolean;
        isContainAlcohol: boolean;
        isDisplayOnWebAndApp: boolean;
        hours: {
            id: any;
            label: any;
            opensAt: any;
            closesAt: any;
            isSelected: boolean;
        }[];
    }>;
    updateMenuCategory(id: string, payload: UpdateMenuCategoryDto): Promise<import("../schemas/menu-category.schema").MenuCategoriesDocument>;
    updateCategoryPosition(payload: UpdateCategoryPositionDto): Promise<{
        success: boolean;
    }>;
}
