"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuCategoryLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_day_service_1 = require("../../restaurant-hours/services/restaurant-day.service");
const restaurant_period_service_1 = require("../../restaurant-hours/services/restaurant-period.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const category_hours_service_1 = require("../services/category-hours.service");
const menu_category_service_1 = require("../services/menu-category.service");
let MenuCategoryLogic = class MenuCategoryLogic {
    constructor(menuCategoryService, categoryHoursService, restaurantService, restaurantPeriodService, restaurantDayService) {
        this.menuCategoryService = menuCategoryService;
        this.categoryHoursService = categoryHoursService;
        this.restaurantService = restaurantService;
        this.restaurantPeriodService = restaurantPeriodService;
        this.restaurantDayService = restaurantDayService;
    }
    async createMenuCategory(createCategory) {
        const restaurant = await this.restaurantService.findOne({
            _id: createCategory.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const lastPosition = await this.menuCategoryService.findOne({
            restaurantId: createCategory.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {}, { sort: { position: -1 } });
        const position = (lastPosition === null || lastPosition === void 0 ? void 0 : lastPosition.position) || 0;
        createCategory.position = position + 1;
        const created = await this.menuCategoryService.create(createCategory);
        if (!created)
            throw new common_1.BadRequestException();
        const hoursPayload = createCategory.hours
            .filter((period) => period.isSelected)
            .map((period) => ({
            restaurantId: createCategory.restaurantId,
            categoryId: created._id,
            restaurantPeriodId: period.id
        }));
        await this.categoryHoursService.createMany(hoursPayload);
        return created;
    }
    async getCategoryHoursForCreate(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const restaurantDays = await this.restaurantDayService.getAll({ restaurantId, isClosed: false }, { _id: 0, label: 1, isClosed: 1 });
        if (restaurantDays.length <= 0)
            return [];
        const openDays = restaurantDays.map((item) => item.label);
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId,
            label: { $in: openDays },
            status: status_enum_1.StatusEnum.ACTIVE
        }, { label: 1, opensAt: 1, closesAt: 1 });
        const result = restaurantPeriods.map((item) => ({
            id: item._id,
            label: item.label,
            opensAt: item.opensAt,
            closesAt: item.closesAt,
            isSelected: false
        }));
        return result;
    }
    async getCategoryById(id) {
        const category = await this.menuCategoryService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!category)
            throw new common_1.NotFoundException('Not found menu category.');
        const restaurantDays = await this.restaurantDayService.getAll({ restaurantId: category.restaurantId, isClosed: false }, { _id: 0, label: 1, isClosed: 1 });
        if (restaurantDays.length <= 0) {
            return {
                name: category.name,
                position: category.position,
                description: category.description,
                isShow: category.isShow,
                isContainAlcohol: category.isContainAlcohol,
                isDisplayOnWebAndApp: category.isDisplayOnWebAndApp,
                hours: []
            };
        }
        const openDays = restaurantDays.map((item) => item.label);
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId: category.restaurantId,
            label: { $in: openDays },
            status: status_enum_1.StatusEnum.ACTIVE
        }, { label: 1, opensAt: 1, closesAt: 1 });
        const categoryHours = await this.categoryHoursService.getAll({
            categoryId: id
        });
        const selectedPeriod = categoryHours.map((selected) => `${selected.restaurantPeriodId}`);
        const hours = restaurantPeriods.map((item) => ({
            id: item._id,
            label: item.label,
            opensAt: item.opensAt,
            closesAt: item.closesAt,
            isSelected: selectedPeriod.includes(`${item._id}`)
        }));
        return {
            name: category.name,
            position: category.position,
            description: category.description,
            isShow: category.isShow,
            isContainAlcohol: category.isContainAlcohol,
            isDisplayOnWebAndApp: category.isDisplayOnWebAndApp,
            hours
        };
    }
    async updateMenuCategory(id, payload) {
        const category = await this.menuCategoryService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!category)
            throw new common_1.NotFoundException('Not found menu category.');
        const updated = await this.menuCategoryService.update(id, payload);
        if (!updated)
            throw new common_1.BadRequestException();
        await this.categoryHoursService.deleteMany({ categoryId: id });
        const hoursPayload = payload.hours
            .filter((period) => period.isSelected)
            .map((period) => ({
            restaurantId: category.restaurantId,
            categoryId: updated._id,
            restaurantPeriodId: period.id
        }));
        await this.categoryHoursService.createMany(hoursPayload);
        return updated;
    }
    async updateCategoryPosition(payload) {
        const { items } = payload;
        const bulkPayload = items.map((item) => ({
            updateOne: {
                filter: { _id: item.id },
                update: { position: item.position }
            }
        }));
        await this.menuCategoryService.bulkWrite(bulkPayload, { ordered: true });
        return { success: true };
    }
};
MenuCategoryLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [menu_category_service_1.MenuCategoriesService,
        category_hours_service_1.CategoryHoursService,
        restaurant_service_1.RestaurantService,
        restaurant_period_service_1.RestaurantPeriodService,
        restaurant_day_service_1.RestaurantDayService])
], MenuCategoryLogic);
exports.MenuCategoryLogic = MenuCategoryLogic;
//# sourceMappingURL=menu-category.logic.js.map