import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { StaffDocument } from '../models/staff.schema';
import { Model } from 'mongoose';
import { StaffPaginateDto } from '../dto/get-staff.dto';
export declare class StaffService {
    private readonly StaffModel;
    private request;
    constructor(StaffModel: PaginateModel<StaffDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<StaffDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<StaffDocument>;
    create(payload: any): Promise<StaffDocument>;
    update(id: string, Member: any): Promise<StaffDocument>;
    delete(id: string): Promise<StaffDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any): Promise<StaffDocument>;
    findOneWithPassCode(condition: any): Promise<StaffDocument>;
    paginate(query: any, queryParam: StaffPaginateDto): Promise<PaginateResult<StaffDocument>>;
    findOneForOrder(id: string): Promise<StaffDocument>;
}
