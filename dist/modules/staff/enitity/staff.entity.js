"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffLoginResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const staff_role_enum_1 = require("../common/staff-role.enum");
class AddressResponseFieldResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressResponseFieldResponse.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressResponseFieldResponse.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressResponseFieldResponse.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressResponseFieldResponse.prototype, "zip_code", void 0);
class TagRateFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], TagRateFieldsResponse.prototype, "is_alcohol_tax_active", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TagRateFieldsResponse.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TagRateFieldsResponse.prototype, "tax", void 0);
class RestaurantFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFieldsResponse.prototype, "legal_business_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFieldsResponse.prototype, "dba", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFieldsResponse.prototype, "authorized_person_first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFieldsResponse.prototype, "authorized_person_last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFieldsResponse.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFieldsResponse.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: AddressResponseFieldResponse }),
    __metadata("design:type", AddressResponseFieldResponse)
], RestaurantFieldsResponse.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: TagRateFieldsResponse }),
    __metadata("design:type", TagRateFieldsResponse)
], RestaurantFieldsResponse.prototype, "taxRate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFieldsResponse.prototype, "logo_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFieldsResponse.prototype, "timeZone", void 0);
class StaffLoginFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffLoginFieldsResponse.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffLoginFieldsResponse.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(staff_role_enum_1.StaffRoleEnum) }),
    __metadata("design:type", String)
], StaffLoginFieldsResponse.prototype, "role", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffLoginFieldsResponse.prototype, "owner_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: RestaurantFieldsResponse }),
    __metadata("design:type", RestaurantFieldsResponse)
], StaffLoginFieldsResponse.prototype, "restaurant", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], StaffLoginFieldsResponse.prototype, "token_expire", void 0);
class StaffLoginResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: StaffLoginFieldsResponse,
        example: {
            username: 'Pavarich',
            id: '6316cc72b889593b028928f1',
            role: 'MANAGER',
            owner_name: 'Paweena Naka',
            restaurant: {
                time_zone: 'Etc/UTC',
                default_service_charge: 10,
                tax_rate: {
                    is_alcohol_tax_active: false,
                    alcohol_tax: 25.2,
                    tax: 14
                },
                logo_url: null,
                email: 'info@carolinaswise.com',
                authorized_person_last_name: 'Naka',
                authorized_person_first_name: 'Paweena',
                business_phone: '5102638229',
                address: {
                    zip_code: '94501',
                    state: 'CA',
                    city: 'Alameda',
                    address: '1319 Park St'
                },
                dba: 'Test Doing as Business',
                legal_business_name: 'May Thai Kitchen LLC',
                id: '630eff5751c2eac55f52662c'
            },
            token_expire: 1667447220531
        }
    }),
    __metadata("design:type", StaffLoginFieldsResponse)
], StaffLoginResponse.prototype, "data", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlBhdmFyaWNoIiwiaWQiOiI2MzE2Y2M3MmI4ODk1OTNiMDI4OTI4ZjEiLCJyb2xlIjoiTUFOQUdFUiIsIm93bmVyTmFtZSI6IlBhd2VlbmEgTmFrYSIsInJlc3RhdXJhbnQiOnsidGF4UmF0ZSI6eyJpc0FsY29ob2xUYXhBY3RpdmUiOmZhbHNlLCJhbGNvaG9sVGF4IjoyNS4yLCJ0YXgiOjE0fSwibG9nb1VybCI6bnVsbCwiZW1haWwiOiJpbmZvQGNhcm9saW5hc3dpc2UuY29tIiwiYXV0aG9yaXplZFBlcnNvbkxhc3ROYW1lIjoiTmFrYSIsImF1dGhvcml6ZWRQZXJzb25GaXJzdE5hbWUiOiJQYXdlZW5hIiwiYnVzaW5lc3NQaG9uZSI6IjUxMDI2MzgyMjkiLCJhZGRyZXNzIjp7InppcENvZGUiOiI5NDUwMSIsInN0YXRlIjoiQ0EiLCJjaXR5IjoiQWxhbWVkYSIsImFkZHJlc3MiOiIxMzE5IFBhcmsgU3QifSwibGVnYWxCdXNpbmVzc05hbWUiOiJNYXkgVGhhaSBLaXRjaGVuIExMQyIsImlkIjoiNjMwZWZmNTc1MWMyZWFjNTVmNTI2NjJjIn0sImlhdCI6MTY2Njg0MjQyMCwiZXhwIjoxNjY3NDQ3MjIwfQ.Ad3BUtkeCoOBgJFiD1bfvLPNOdILkNOhvy56_WnorNo'
    }),
    __metadata("design:type", String)
], StaffLoginResponse.prototype, "access_token", void 0);
exports.StaffLoginResponse = StaffLoginResponse;
//# sourceMappingURL=staff.entity.js.map