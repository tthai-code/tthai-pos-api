import { ResponseDto } from 'src/common/entity/response.entity';
declare class AddressResponseFieldResponse {
    address: string;
    city: string;
    state: string;
    zip_code: string;
}
declare class TagRateFieldsResponse {
    is_alcohol_tax_active: boolean;
    alcohol_tax: number;
    tax: number;
}
declare class RestaurantFieldsResponse {
    legal_business_name: string;
    dba: string;
    authorized_person_first_name: string;
    authorized_person_last_name: string;
    email: string;
    business_phone: string;
    address: AddressResponseFieldResponse;
    taxRate: TagRateFieldsResponse;
    logo_url: string;
    timeZone: string;
}
declare class StaffLoginFieldsResponse {
    username: string;
    id: string;
    role: string;
    owner_name: string;
    restaurant: RestaurantFieldsResponse;
    token_expire: number;
}
export declare class StaffLoginResponse extends ResponseDto<StaffLoginFieldsResponse> {
    data: StaffLoginFieldsResponse;
    access_token: string;
}
export {};
