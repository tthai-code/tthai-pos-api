export declare class UpdateStaffDto {
    readonly passCode: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string;
    readonly tel: string;
    readonly role: string;
    readonly status: string;
}
