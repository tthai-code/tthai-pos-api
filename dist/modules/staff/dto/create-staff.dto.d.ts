export declare class CreateStaffDto {
    readonly restaurantId: string;
    readonly passCode: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string;
    readonly tel: string;
    readonly role: string;
}
