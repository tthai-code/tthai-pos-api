import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class StaffPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly status: string;
    readonly search: string;
    readonly restaurant: string;
    buildQuery(id: string): {
        $or: ({
            firstName: {
                $regex: string;
                $options: string;
            };
            lastName?: undefined;
            email?: undefined;
            tel?: undefined;
        } | {
            lastName: {
                $regex: string;
                $options: string;
            };
            firstName?: undefined;
            email?: undefined;
            tel?: undefined;
        } | {
            email: {
                $regex: string;
                $options: string;
            };
            firstName?: undefined;
            lastName?: undefined;
            tel?: undefined;
        } | {
            tel: {
                $regex: string;
                $options: string;
            };
            firstName?: undefined;
            lastName?: undefined;
            email?: undefined;
        })[];
        restaurantId: string;
        status: string | {
            $ne: StatusEnum;
        };
    };
}
