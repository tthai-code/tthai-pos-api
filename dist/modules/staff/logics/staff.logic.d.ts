import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CreateStaffDto } from '../dto/create-staff.dto';
import { UpdateStaffDto } from '../dto/update-staff.dto';
import { StaffService } from '../services/staff.service';
export declare class StaffLogic {
    private readonly staffService;
    private readonly restaurantService;
    constructor(staffService: StaffService, restaurantService: RestaurantService);
    CreateStaff(payload: CreateStaffDto): Promise<import("../models/staff.schema").StaffDocument>;
    UpdateStaff(id: string, restaurantId: string, payload: UpdateStaffDto): Promise<import("../models/staff.schema").StaffDocument>;
}
