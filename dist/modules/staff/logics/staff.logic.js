"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const staff_service_1 = require("../services/staff.service");
let StaffLogic = class StaffLogic {
    constructor(staffService, restaurantService) {
        this.staffService = staffService;
        this.restaurantService = restaurantService;
    }
    async CreateStaff(payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: payload.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Restaurant not found.');
        const staff = await this.staffService.findOne({
            restaurantId: payload.restaurantId,
            passCode: payload.passCode,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (staff)
            throw new common_1.BadRequestException(`Pass code is exist.`);
        const created = await this.staffService.create(payload);
        if (!created)
            throw new common_1.BadRequestException('Can not create staff.');
        return created;
    }
    async UpdateStaff(id, restaurantId, payload) {
        const staff = await this.staffService.findOne({
            passCode: payload.passCode,
            status: status_enum_1.StatusEnum.ACTIVE,
            restaurantId,
            _id: { $ne: id }
        });
        if (staff)
            throw new common_1.BadRequestException(`Pass code is exist.`);
        const updated = await this.staffService.update(id, payload);
        if (!updated)
            throw new common_1.BadRequestException('Can not update staff.');
        return updated;
    }
};
StaffLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [staff_service_1.StaffService,
        restaurant_service_1.RestaurantService])
], StaffLogic);
exports.StaffLogic = StaffLogic;
//# sourceMappingURL=staff.logic.js.map