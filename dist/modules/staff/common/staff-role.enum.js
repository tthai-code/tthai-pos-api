"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffRoleEnum = void 0;
var StaffRoleEnum;
(function (StaffRoleEnum) {
    StaffRoleEnum["MANAGER"] = "MANAGER";
    StaffRoleEnum["STAFF"] = "STAFF";
})(StaffRoleEnum = exports.StaffRoleEnum || (exports.StaffRoleEnum = {}));
//# sourceMappingURL=staff-role.enum.js.map