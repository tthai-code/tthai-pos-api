"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSStaffController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const device_auth_guard_1 = require("../../auth/guards/device-auth.guard");
const auth_logic_1 = require("../../auth/logics/auth.logic");
const errorResponse_1 = require("../../../utilities/errorResponse");
const staff_login_dto_1 = require("../dto/staff-login.dto");
const staff_entity_1 = require("../enitity/staff.entity");
let POSStaffController = class POSStaffController {
    constructor(authLogic) {
        this.authLogic = authLogic;
    }
    async staffLogin(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = device_auth_guard_1.DeviceAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return this.authLogic.StaffLogin(restaurantId, payload);
    }
};
__decorate([
    (0, common_1.Post)('login'),
    (0, swagger_1.ApiOperation)({
        summary: 'Staff Login',
        description: 'use *bearer* `device_token`'
    }),
    (0, swagger_1.ApiOkResponse)({ type: () => staff_entity_1.StaffLoginResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Not found staff.', '/v1/staff/login')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [staff_login_dto_1.StaffLoginDto]),
    __metadata("design:returntype", Promise)
], POSStaffController.prototype, "staffLogin", null);
POSStaffController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/staff'),
    (0, common_1.Controller)('v1/staff'),
    (0, common_1.UseGuards)(device_auth_guard_1.DeviceAuthGuard),
    __metadata("design:paramtypes", [auth_logic_1.AuthLogic])
], POSStaffController);
exports.POSStaffController = POSStaffController;
//# sourceMappingURL=pos-staff.controller.js.map