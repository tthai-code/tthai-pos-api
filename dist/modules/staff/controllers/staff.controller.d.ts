import { AuthLogic } from 'src/modules/auth/logics/auth.logic';
import { CreateStaffDto } from '../dto/create-staff.dto';
import { StaffPaginateDto } from '../dto/get-staff.dto';
import { UpdateStaffDto } from '../dto/update-staff.dto';
import { StaffLogic } from '../logics/staff.logic';
import { StaffService } from '../services/staff.service';
export declare class StaffController {
    private readonly staffService;
    private readonly staffLogic;
    private readonly authLogic;
    constructor(staffService: StaffService, staffLogic: StaffLogic, authLogic: AuthLogic);
    getStaffs(query: StaffPaginateDto): Promise<PaginateResult<import("../models/staff.schema").StaffDocument>>;
    getStaff(id: string): Promise<import("../models/staff.schema").StaffDocument>;
    createStaff(payload: CreateStaffDto): Promise<import("../models/staff.schema").StaffDocument>;
    updateStaff(id: string, payload: UpdateStaffDto): Promise<import("../models/staff.schema").StaffDocument>;
    deleteStaff(id: string): Promise<import("../models/staff.schema").StaffDocument>;
}
