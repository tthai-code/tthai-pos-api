/// <reference types="mongoose" />
import { AuthLogic } from 'src/modules/auth/logics/auth.logic';
import { StaffLoginDto } from '../dto/staff-login.dto';
export declare class POSStaffController {
    private readonly authLogic;
    constructor(authLogic: AuthLogic);
    staffLogin(payload: StaffLoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        role: string;
        ownerName: string;
        restaurant: import("../../restaurant/schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
}
