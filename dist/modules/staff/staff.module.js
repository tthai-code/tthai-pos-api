"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const pos_staff_controller_1 = require("./controllers/pos-staff.controller");
const staff_controller_1 = require("./controllers/staff.controller");
const staff_logic_1 = require("./logics/staff.logic");
const staff_schema_1 = require("./models/staff.schema");
const staff_service_1 = require("./services/staff.service");
let StaffModule = class StaffModule {
};
StaffModule = __decorate([
    (0, common_1.Module)({
        imports: [
            restaurant_module_1.RestaurantModule,
            mongoose_1.MongooseModule.forFeature([{ name: 'staff', schema: staff_schema_1.StaffSchema }])
        ],
        providers: [staff_service_1.StaffService, staff_logic_1.StaffLogic],
        controllers: [staff_controller_1.StaffController, pos_staff_controller_1.POSStaffController],
        exports: [staff_service_1.StaffService]
    })
], StaffModule);
exports.StaffModule = StaffModule;
//# sourceMappingURL=staff.module.js.map