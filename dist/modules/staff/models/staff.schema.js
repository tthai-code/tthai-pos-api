"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaffSchema = exports.StaffFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const staff_role_enum_1 = require("../common/staff-role.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
let StaffFields = class StaffFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], StaffFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], StaffFields.prototype, "passCode", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], StaffFields.prototype, "firstName", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], StaffFields.prototype, "lastName", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], StaffFields.prototype, "email", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], StaffFields.prototype, "tel", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: staff_role_enum_1.StaffRoleEnum.STAFF, enum: Object.values(staff_role_enum_1.StaffRoleEnum) }),
    __metadata("design:type", String)
], StaffFields.prototype, "role", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], StaffFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], StaffFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], StaffFields.prototype, "createdBy", void 0);
StaffFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'staffs' })
], StaffFields);
exports.StaffFields = StaffFields;
exports.StaffSchema = mongoose_1.SchemaFactory.createForClass(StaffFields);
exports.StaffSchema.plugin(mongoosePaginate);
exports.StaffSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=staff.schema.js.map