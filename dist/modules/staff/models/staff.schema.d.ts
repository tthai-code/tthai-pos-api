import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type StaffDocument = StaffFields & Document;
export declare class StaffFields {
    restaurantId: Types.ObjectId;
    passCode: string;
    firstName: string;
    lastName: string;
    email: string;
    tel: string;
    role: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const StaffSchema: MongooseSchema<Document<StaffFields, any, any>, import("mongoose").Model<Document<StaffFields, any, any>, any, any, any>, any, any>;
