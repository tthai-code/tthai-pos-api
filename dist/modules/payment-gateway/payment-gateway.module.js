"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentGatewayModule = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const subscription_module_1 = require("../subscription/subscription.module");
const admin_payment_controller_1 = require("./controllers/admin-payment.controller");
const bank_account_controller_1 = require("./controllers/bank-account.controller");
const payment_gateway_open_controller_1 = require("./controllers/payment-gateway-open.controller");
const payment_gateway_controller_1 = require("./controllers/payment-gateway.controller");
const pos_payment_controller_1 = require("./controllers/pos-payment.controller");
const admin_payment_logic_1 = require("./logics/admin-payment.logic");
const bank_account_logic_1 = require("./logics/bank-account.logic");
const card_pointe_logic_1 = require("./logics/card-pointe.logic");
const co_pilot_logic_1 = require("./logics/co-pilot.logic");
const payment_gateway_logic_1 = require("./logics/payment-gateway.logic");
const bank_account_schema_1 = require("./schemas/bank-account.schema");
const payment_gateway_schema_1 = require("./schemas/payment-gateway.schema");
const bank_account_service_1 = require("./services/bank-account.service");
const co_pilot_service_1 = require("./services/co-pilot.service");
const payment_gateway_http_service_1 = require("./services/payment-gateway-http.service");
const payment_gateway_service_1 = require("./services/payment-gateway.service");
let PaymentGatewayModule = class PaymentGatewayModule {
};
PaymentGatewayModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule,
            axios_1.HttpModule,
            (0, common_1.forwardRef)(() => subscription_module_1.SubscriptionModule),
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'paymentGateway', schema: payment_gateway_schema_1.PaymentGatewaySchema },
                { name: 'bankAccount', schema: bank_account_schema_1.BankAccountSchema }
            ])
        ],
        providers: [
            payment_gateway_service_1.PaymentGatewayService,
            payment_gateway_logic_1.PaymentGatewayLogic,
            payment_gateway_http_service_1.PaymentGatewayHttpService,
            card_pointe_logic_1.CardPointeLogic,
            co_pilot_service_1.CoPilotService,
            co_pilot_logic_1.CoPilotLogic,
            bank_account_service_1.BankAccountService,
            bank_account_logic_1.BankAccountLogic,
            admin_payment_logic_1.AdminPaymentLogic
        ],
        controllers: [
            payment_gateway_controller_1.PaymentGatewayController,
            payment_gateway_open_controller_1.PaymentGatewayOpenController,
            pos_payment_controller_1.POSPaymentGatewayController,
            bank_account_controller_1.BankAccountController,
            admin_payment_controller_1.AdminPaymentGatewayController
        ],
        exports: [
            payment_gateway_service_1.PaymentGatewayService,
            payment_gateway_logic_1.PaymentGatewayLogic,
            card_pointe_logic_1.CardPointeLogic,
            bank_account_service_1.BankAccountService,
            bank_account_logic_1.BankAccountLogic,
            payment_gateway_http_service_1.PaymentGatewayHttpService
        ]
    })
], PaymentGatewayModule);
exports.PaymentGatewayModule = PaymentGatewayModule;
//# sourceMappingURL=payment-gateway.module.js.map