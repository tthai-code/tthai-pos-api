import { HttpService } from '@nestjs/axios';
import { ICapturePartialAmount, IRecurring, IRefundWithRetRef, IVoidByOrderId, IVoidRetRef } from '../interfaces/card-pointe.interface';
export interface IAuthBasic {
    username: string;
    password: string;
}
export interface IValidateMerchant {
    site: string;
    acctupdater: string;
    cvv: string;
    cardproc: string;
    fee_type: string;
    enabled: boolean;
    echeck: string;
    merchid: string;
    avs: string;
}
export interface IGetProfile {
    profileId: string;
    accountId: string;
    merchantId: string;
}
export declare class PaymentGatewayHttpService {
    private readonly httpService;
    private request;
    constructor(httpService: HttpService, request: any);
    private readonly url;
    private readonly csurl;
    private readonly ACHMerchantID;
    private readonly username;
    private readonly password;
    getTThaiMerchant(): any;
    healthCheckPaymentGateway(auth: IAuthBasic): Promise<any>;
    validateMerchantId(merchantId: string, auth: IAuthBasic): Promise<any>;
    tokenizeCard(account: string, auth: IAuthBasic): Promise<any>;
    binToken(merchantId: string, token: string, auth: IAuthBasic): Promise<any>;
    authorization(payload: any, auth: IAuthBasic): Promise<any>;
    capture(payload: ICapturePartialAmount, auth: IAuthBasic): Promise<any>;
    authACH(payload: IRecurring): Promise<any>;
    recurring(payload: IRecurring): Promise<any>;
    voidRetRef(payload: IVoidRetRef, auth: IAuthBasic): Promise<any>;
    voidByOrderId(payload: IVoidByOrderId, auth: IAuthBasic): Promise<any>;
    refundRetRef(payload: IRefundWithRetRef, auth: IAuthBasic): Promise<any>;
    createProfile(payload: any, auth: IAuthBasic): Promise<any>;
    getProfile(payload: IGetProfile, auth: IAuthBasic): Promise<any>;
    deleteProfile(payload: IGetProfile, auth: IAuthBasic): Promise<any>;
    inquireRetRef(retRef: string, merchantId: string, auth: IAuthBasic): Promise<any>;
    inquireOrderId(orderId: string, merchantId: string, auth: IAuthBasic): Promise<any>;
    openBatch(merchantId: string, batchSource: string, auth: IAuthBasic): Promise<any>;
    closeBatch(merchantId: string, auth: IAuthBasic, batchId?: string | number): Promise<any>;
}
