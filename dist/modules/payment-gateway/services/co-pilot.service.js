"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoPilotService = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const core_1 = require("@nestjs/core");
let CoPilotService = class CoPilotService {
    constructor(httpService, configService, request) {
        this.httpService = httpService;
        this.configService = configService;
        this.request = request;
        this.url = this.configService.get('CO_PILOT_API_URL');
        this.tokenUrl = this.configService.get('CO_PILOT_TOKEN_URL');
        this.username = this.configService.get('CO_PILOT_USERNAME');
        this.password = this.configService.get('CO_PILOT_PASSWORD');
        this.clientSecret = this.configService.get('CO_PILOT_CLIENT_SECRET');
        this.templateId = this.configService.get('CO_PILOT_TEMPLATE_ID');
        this.salesCode = this.configService.get('CO_PILOT_SALES_CODE');
        this.clientId = this.configService.get('CO_PILOT_CLIENT_ID');
    }
    async tokenize() {
        const params = new URLSearchParams();
        params.append('username', this.username);
        params.append('password', this.password);
        params.append('grant_type', 'password');
        params.append('client_id', this.clientId);
        params.append('client_secret', this.clientSecret);
        return this.httpService
            .post(this.tokenUrl, params, {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        })
            .toPromise();
    }
    async createMerchantMinimum(payload, token) {
        payload.templateId = this.templateId;
        payload.merchant.salesCode = this.salesCode;
        return this.httpService
            .post(`${this.url}/merchant`, payload, {
            headers: {
                Authorization: `Bearer ${token}`,
                'X-CopilotAPI-Version': '1.0'
            }
        })
            .toPromise();
    }
    async signatureMerchant(merchantId, token) {
        return this.httpService
            .put(`${this.url}/merchant/${merchantId}/signature`, {}, {
            headers: {
                Authorization: `Bearer ${token}`,
                'X-CopilotAPI-Version': '1.0'
            }
        })
            .toPromise();
    }
    async getMerchantSignatureStatus(merchantId, token) {
        return this.httpService
            .get(`${this.url}/merchant/${merchantId}`, {
            headers: {
                Authorization: `Bearer ${token}`,
                'X-CopilotAPI-Version': '1.0'
            }
        })
            .toPromise();
    }
};
CoPilotService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(2, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [axios_1.HttpService,
        config_1.ConfigService, Object])
], CoPilotService);
exports.CoPilotService = CoPilotService;
//# sourceMappingURL=co-pilot.service.js.map