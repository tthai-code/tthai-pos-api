import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { PaymentGatewayDocument } from '../schemas/payment-gateway.schema';
export declare class PaymentGatewayService {
    private readonly PaymentGatewayModel;
    private request;
    constructor(PaymentGatewayModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<PaymentGatewayDocument | any>;
    resolveByCondition(condition: any): Promise<PaymentGatewayDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<PaymentGatewayDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<PaymentGatewayDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<PaymentGatewayDocument>;
    create(payload: any): Promise<PaymentGatewayDocument>;
    update(id: string, payload: any): Promise<PaymentGatewayDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<PaymentGatewayDocument>;
    delete(id: string): Promise<PaymentGatewayDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<PaymentGatewayDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<PaymentGatewayDocument>;
}
