import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { ICreateMerchant } from '../interfaces/co-pilot.interface';
export declare class CoPilotService {
    private readonly httpService;
    private readonly configService;
    private request;
    constructor(httpService: HttpService, configService: ConfigService, request: any);
    private readonly url;
    private readonly tokenUrl;
    private readonly username;
    private readonly password;
    private readonly clientSecret;
    private readonly templateId;
    private readonly salesCode;
    private readonly clientId;
    tokenize(): Promise<any>;
    createMerchantMinimum(payload: ICreateMerchant, token: string): Promise<any>;
    signatureMerchant(merchantId: string, token: string): Promise<any>;
    getMerchantSignatureStatus(merchantId: string, token: string): Promise<any>;
}
