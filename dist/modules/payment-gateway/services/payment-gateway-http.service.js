"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentGatewayHttpService = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
let PaymentGatewayHttpService = class PaymentGatewayHttpService {
    constructor(httpService, request) {
        this.httpService = httpService;
        this.request = request;
        this.url = process.env.PAYMENT_API_URL;
        this.csurl = process.env.PAYMENT_API_CSURL;
        this.ACHMerchantID = process.env.MERCHANT_ACH_ID;
        this.username = process.env.PAYMENT_USERNAME;
        this.password = process.env.PAYMENT_PASSWORD;
    }
    getTThaiMerchant() {
        return {
            mid: this.ACHMerchantID,
            username: this.username,
            password: this.password
        };
    }
    async healthCheckPaymentGateway(auth) {
        return this.httpService.get(this.url, { auth }).toPromise();
    }
    async validateMerchantId(merchantId, auth) {
        return this.httpService
            .get(`${this.url}/inquireMerchant/${merchantId}`, { auth })
            .toPromise();
    }
    async tokenizeCard(account, auth) {
        return this.httpService.post(this.csurl, { account }, { auth }).toPromise();
    }
    async binToken(merchantId, token, auth) {
        return this.httpService
            .get(`${this.url}/bin/${merchantId}/${token}`, { auth })
            .toPromise();
    }
    async authorization(payload, auth) {
        return this.httpService
            .put(`${this.url}/auth`, payload, { auth })
            .toPromise();
    }
    async capture(payload, auth) {
        return this.httpService
            .put(`${this.url}/capture`, payload, { auth })
            .toPromise();
    }
    async authACH(payload) {
        payload.merchid = this.ACHMerchantID;
        payload.capture = 'y';
        payload.currency = 'USD';
        payload.ecomind = 'E';
        payload.bin = 'y';
        return this.httpService
            .put(`${this.url}/auth`, payload, {
            auth: { username: this.username, password: this.password }
        })
            .toPromise();
    }
    async recurring(payload) {
        payload.merchid = this.ACHMerchantID;
        payload.capture = 'y';
        payload.currency = 'USD';
        payload.ecomind = 'R';
        payload.cof = 'C';
        payload.cofscheduled = 'Y';
        payload.bin = 'y';
        return this.httpService
            .put(`${this.url}/auth`, payload, {
            auth: { username: this.username, password: this.password }
        })
            .toPromise();
    }
    async voidRetRef(payload, auth) {
        return this.httpService
            .put(`${this.url}/void`, payload, { auth })
            .toPromise();
    }
    async voidByOrderId(payload, auth) {
        return this.httpService
            .put(`${this.url}/voidByOrderId`, payload, { auth })
            .toPromise();
    }
    async refundRetRef(payload, auth) {
        return this.httpService
            .put(`${this.url}/refund`, payload, { auth })
            .toPromise();
    }
    async createProfile(payload, auth) {
        return this.httpService
            .put(`${this.url}/profile`, payload, { auth })
            .toPromise();
    }
    async getProfile(payload, auth) {
        return this.httpService
            .get(`${this.url}/profile/${payload.profileId}/${payload.accountId}/${payload.merchantId}`, { auth })
            .toPromise();
    }
    async deleteProfile(payload, auth) {
        return this.httpService
            .delete(`${this.url}/profile/${payload.profileId}/${payload.accountId}/${payload.merchantId}`, { auth })
            .toPromise();
    }
    async inquireRetRef(retRef, merchantId, auth) {
        return this.httpService
            .get(`${this.url}/inquire/${retRef}/${merchantId}`, { auth })
            .toPromise();
    }
    async inquireOrderId(orderId, merchantId, auth) {
        return this.httpService
            .get(`${this.url}/inquireByOrderid/${orderId}/${merchantId}`, { auth })
            .toPromise();
    }
    async openBatch(merchantId, batchSource, auth) {
        return this.httpService
            .get(`${this.url}/openbatch/${merchantId}/${batchSource}`, { auth })
            .toPromise();
    }
    async closeBatch(merchantId, auth, batchId) {
        const url = !!batchId
            ? `${this.url}/closebatch/${merchantId}/${batchId}`
            : `${this.url}/closebatch/${merchantId}`;
        return this.httpService.get(url, { auth }).toPromise();
    }
};
PaymentGatewayHttpService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [axios_1.HttpService, Object])
], PaymentGatewayHttpService);
exports.PaymentGatewayHttpService = PaymentGatewayHttpService;
//# sourceMappingURL=payment-gateway-http.service.js.map