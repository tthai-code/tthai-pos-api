import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { BankAccountDocument } from '../schemas/bank-account.schema';
export declare class BankAccountService {
    private readonly BankAccountModel;
    private request;
    constructor(BankAccountModel: Model<BankAccountDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<BankAccountDocument | any>;
    resolveByCondition(condition: any): Promise<BankAccountDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<BankAccountDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<BankAccountDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<BankAccountDocument>;
    create(payload: any): Promise<BankAccountDocument>;
    update(id: string, payload: any): Promise<BankAccountDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<BankAccountDocument>;
    delete(id: string): Promise<BankAccountDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<BankAccountDocument>;
    updateMany(condition: any, payload: any): Promise<any>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
}
