export interface IAuthNoCapture {
    merchid: string;
    account: string;
    expiry: string;
    amount: string;
    currency: string;
    name: string;
}
export interface IAuthWithCapture extends IAuthNoCapture {
    orderid?: string;
    batchid?: string;
    batchsource?: string;
    capture: string;
    receipt?: string;
    ecomid?: string;
    cvv2?: string;
    postal?: string;
}
export interface ICustomMerchant {
    customemerchant: string;
}
export interface ICustomProduct {
    customproduct: string;
}
export interface ICustomPhone {
    customphone: string;
}
export interface ICustomState {
    customstate: string;
}
export interface ICustomPostal {
    custompostal: string;
}
export interface IAuthWithCaptureSoftDelete extends IAuthWithCapture {
    userfields: ICustomMerchant[] | ICustomProduct[] | ICustomPhone[] | ICustomState[] | ICustomPostal[];
}
export interface IAuthIngenicoTrack extends IAuthNoCapture {
    track: string;
}
export interface IAuthCreateProfile extends IAuthNoCapture {
    address: string;
    city: string;
    region: string;
    country: string;
    postal: string;
    profile: string;
}
export interface IAuthExistProfile {
    merchid: string;
    profile: string;
    amount: string;
    currency: string;
}
export interface ICapturePartialAmount {
    retref: string;
    merchid: string;
    amount: string | number;
}
export interface IVoidRetRef {
    merchid: string;
    retref: string;
}
export interface IVoidByOrderId {
    merchid: string;
    orderid: string;
}
export interface IRefundWithRetRef {
    merchid: string;
    retref: string;
    amount?: number | string;
}
export declare type IRefundNoRetRef = IAuthNoCapture;
export interface IProfileCreateUpdate {
    name: string;
    address: string;
    city: string;
    region: string;
    country: string;
    postal: string;
    merchid: string;
    account: string;
    expiry: string;
    currency: string;
}
export interface IProfileNewAccountTokenCard extends IProfileCreateUpdate {
    profile: string;
    profileupdate: string;
    defaultacct: string;
}
export interface ISignatureCaptureRetRef {
    merchid: string;
    retref: string;
    signature: string;
}
export interface IRecurring {
    merchid?: string;
    account: string;
    accttype: string;
    amount: string;
    currency?: string | number;
    name: string;
    capture?: string;
    ecomind?: string;
    cof?: string;
    cofscheduled?: string;
    orderid?: string;
    bin?: string;
}
export interface IOnlinePayment {
    merchid?: string;
    account: string;
    expiry: string;
    amount: string | number;
    orderid: string;
    currency?: string;
    name: string;
    capture?: string;
    ecomind?: string;
    bin?: string;
}
