import { PaymentGatewayLogic } from '../logics/payment-gateway.logic';
export declare class PaymentGatewayOpenController {
    private readonly paymentGatewayLogic;
    constructor(paymentGatewayLogic: PaymentGatewayLogic);
    getPaymentSetting(id: string): Promise<import("../logics/payment-gateway.logic").IPaymentGateway>;
}
