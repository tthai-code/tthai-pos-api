"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSPaymentGatewayController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const get_pos_payment_response_1 = require("../entity/get-pos-payment.response");
const payment_gateway_logic_1 = require("../logics/payment-gateway.logic");
const payment_gateway_service_1 = require("../services/payment-gateway.service");
let POSPaymentGatewayController = class POSPaymentGatewayController {
    constructor(paymentGatewayService, paymentGatewayLogic) {
        this.paymentGatewayService = paymentGatewayService;
        this.paymentGatewayLogic = paymentGatewayLogic;
    }
    async getPaymentGateway() {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.paymentGatewayLogic.getPaymentGatewayWithDecode(restaurantId);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => get_pos_payment_response_1.PaymentGatewayResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Payment Gateway API Config',
        description: 'use *bearer* `staff_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], POSPaymentGatewayController.prototype, "getPaymentGateway", null);
POSPaymentGatewayController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/payment'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/payment'),
    __metadata("design:paramtypes", [payment_gateway_service_1.PaymentGatewayService,
        payment_gateway_logic_1.PaymentGatewayLogic])
], POSPaymentGatewayController);
exports.POSPaymentGatewayController = POSPaymentGatewayController;
//# sourceMappingURL=pos-payment.controller.js.map