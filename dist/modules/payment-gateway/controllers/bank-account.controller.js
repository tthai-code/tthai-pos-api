"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BankAccountController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const bank_account_dto_1 = require("../dto/bank-account.dto");
const bank_account_entity_1 = require("../entity/bank-account.entity");
const bank_account_logic_1 = require("../logics/bank-account.logic");
const bank_account_service_1 = require("../services/bank-account.service");
let BankAccountController = class BankAccountController {
    constructor(bankAccountLogic, bankAccountService) {
        this.bankAccountLogic = bankAccountLogic;
        this.bankAccountService = bankAccountService;
    }
    async getBankAccount() {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.bankAccountLogic.getBankAccount(restaurantId);
    }
    async getBankAccounts() {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.bankAccountLogic.getBankAccounts(restaurantId);
    }
    async updateBankAccount(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.bankAccountLogic.updateBankAccount(restaurantId, payload);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => bank_account_entity_1.GetActiveBankAccountResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Bank Account by Restaurant ID',
        description: 'use bearer token `restaurant_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BankAccountController.prototype, "getBankAccount", null);
__decorate([
    (0, common_1.Get)('all'),
    (0, swagger_1.ApiOkResponse)({ type: () => bank_account_entity_1.GetBankAccountResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Bank Account List by Restaurant ID',
        description: 'use bearer token `restaurant_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BankAccountController.prototype, "getBankAccounts", null);
__decorate([
    (0, common_1.Put)(),
    (0, swagger_1.ApiOkResponse)({ type: () => bank_account_entity_1.UpdateBankAccountResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create New Bank Account Information (ACH)',
        description: 'use bearer token `restaurant_token`\n\nInit or Change New Bank Account'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [bank_account_dto_1.UpdateBankAccountDto]),
    __metadata("design:returntype", Promise)
], BankAccountController.prototype, "updateBankAccount", null);
BankAccountController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('bank-account'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('/v1/bank-account'),
    __metadata("design:paramtypes", [bank_account_logic_1.BankAccountLogic,
        bank_account_service_1.BankAccountService])
], BankAccountController);
exports.BankAccountController = BankAccountController;
//# sourceMappingURL=bank-account.controller.js.map