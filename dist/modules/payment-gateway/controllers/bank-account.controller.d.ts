import { UpdateBankAccountDto } from '../dto/bank-account.dto';
import { BankAccountLogic } from '../logics/bank-account.logic';
import { BankAccountService } from '../services/bank-account.service';
export declare class BankAccountController {
    private readonly bankAccountLogic;
    private readonly bankAccountService;
    constructor(bankAccountLogic: BankAccountLogic, bankAccountService: BankAccountService);
    getBankAccount(): Promise<import("../schemas/bank-account.schema").BankAccountDocument>;
    getBankAccounts(): Promise<any[]>;
    updateBankAccount(payload: UpdateBankAccountDto): Promise<import("../schemas/bank-account.schema").BankAccountDocument>;
}
