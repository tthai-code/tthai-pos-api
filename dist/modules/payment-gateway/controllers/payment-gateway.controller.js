"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentGatewayController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const state_code_enum_1 = require("../common/state-code.enum");
const create_merchant_dto_1 = require("../dto/create-merchant.dto");
const update_payment_dto_1 = require("../dto/update-payment.dto");
const create_merchant_entity_1 = require("../entity/create-merchant.entity");
const get_merchant_status_entity_1 = require("../entity/get-merchant-status.entity");
const get_payment_response_1 = require("../entity/get-payment.response");
const update_payment_reponse_1 = require("../entity/update-payment.reponse");
const co_pilot_logic_1 = require("../logics/co-pilot.logic");
const payment_gateway_logic_1 = require("../logics/payment-gateway.logic");
let PaymentGatewayController = class PaymentGatewayController {
    constructor(paymentGatewayLogic, coPilotLogic) {
        this.paymentGatewayLogic = paymentGatewayLogic;
        this.coPilotLogic = coPilotLogic;
    }
    async getPayment(id) {
        return await this.paymentGatewayLogic.getPaymentGateway(id);
    }
    async updatePayment(id, payload) {
        return await this.paymentGatewayLogic.updatePaymentGateway(id, payload);
    }
    async getStateCodes() {
        return state_code_enum_1.stateCodes;
    }
    async createMerchant(merchant) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.coPilotLogic.createMerchant(merchant, restaurantId);
    }
    async getMerchantStatus() {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.coPilotLogic.getMerchantSignatureStatus(restaurantId);
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => get_payment_response_1.GetPaymentResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Payment Gateway Setting' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PaymentGatewayController.prototype, "getPayment", null);
__decorate([
    (0, common_1.Put)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => update_payment_reponse_1.UpdatePaymentResponseDto }),
    (0, swagger_1.ApiOperation)({ summary: 'Update Payment Gateway Setting' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_payment_dto_1.UpdatePaymentDto]),
    __metadata("design:returntype", Promise)
], PaymentGatewayController.prototype, "updatePayment", null);
__decorate([
    (0, common_1.Get)('merchant/state'),
    (0, swagger_1.ApiOperation)({ summary: 'Get State Code List' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PaymentGatewayController.prototype, "getStateCodes", null);
__decorate([
    (0, common_1.Post)('merchant'),
    (0, swagger_1.ApiOkResponse)({ type: () => create_merchant_entity_1.CreateMerchantResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Create Merchant from CoPilot' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_merchant_dto_1.CreateMerchantDto]),
    __metadata("design:returntype", Promise)
], PaymentGatewayController.prototype, "createMerchant", null);
__decorate([
    (0, common_1.Get)(':restaurantId/merchant'),
    (0, swagger_1.ApiOkResponse)({ type: () => get_merchant_status_entity_1.SignatureStatusResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Merchant Status' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PaymentGatewayController.prototype, "getMerchantStatus", null);
PaymentGatewayController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('payment-gateway-setting'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('/v1/payment-gateway'),
    __metadata("design:paramtypes", [payment_gateway_logic_1.PaymentGatewayLogic,
        co_pilot_logic_1.CoPilotLogic])
], PaymentGatewayController);
exports.PaymentGatewayController = PaymentGatewayController;
//# sourceMappingURL=payment-gateway.controller.js.map