import { GetAdminPaymentStatusDto, UpdateBankAccountStatusDto } from '../dto/admin-update-payment.dto';
import { UpdatePaymentDto } from '../dto/update-payment.dto';
import { AdminPaymentLogic } from '../logics/admin-payment.logic';
import { PaymentGatewayLogic } from '../logics/payment-gateway.logic';
export declare class AdminPaymentGatewayController {
    private readonly adminPaymentLogic;
    private readonly paymentGatewayLogic;
    constructor(adminPaymentLogic: AdminPaymentLogic, paymentGatewayLogic: PaymentGatewayLogic);
    getPaymentStatus(query: GetAdminPaymentStatusDto): Promise<import("../schemas/payment-gateway.schema").PaymentGatewayDocument | {
        isBankAccountApproved: boolean;
        midCoPilot: any;
    }>;
    updateBankAccountApproved(query: GetAdminPaymentStatusDto, payload: UpdateBankAccountStatusDto): Promise<{
        success: boolean;
    }>;
    getCoPilotStatus(query: GetAdminPaymentStatusDto): Promise<{
        coPilotMerchantId: any;
        boardingProcessStatus: string;
    }>;
    getPaymentGatewaySetting(id: string): Promise<import("../logics/payment-gateway.logic").IPaymentGateway>;
    updateMIDPayment(id: string, payload: UpdatePaymentDto): Promise<{
        success: boolean;
    }>;
}
