import { PaymentGatewayLogic } from '../logics/payment-gateway.logic';
import { PaymentGatewayService } from '../services/payment-gateway.service';
export declare class POSPaymentGatewayController {
    private readonly paymentGatewayService;
    private readonly paymentGatewayLogic;
    constructor(paymentGatewayService: PaymentGatewayService, paymentGatewayLogic: PaymentGatewayLogic);
    getPaymentGateway(): Promise<import("../logics/payment-gateway.logic").IPaymentGateway>;
}
