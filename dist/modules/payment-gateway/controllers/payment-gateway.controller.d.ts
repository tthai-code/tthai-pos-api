import { CreateMerchantDto } from '../dto/create-merchant.dto';
import { UpdatePaymentDto } from '../dto/update-payment.dto';
import { CoPilotLogic } from '../logics/co-pilot.logic';
import { IPaymentGateway, PaymentGatewayLogic } from '../logics/payment-gateway.logic';
export declare class PaymentGatewayController {
    private readonly paymentGatewayLogic;
    private readonly coPilotLogic;
    constructor(paymentGatewayLogic: PaymentGatewayLogic, coPilotLogic: CoPilotLogic);
    getPayment(id: string): Promise<IPaymentGateway>;
    updatePayment(id: string, payload: UpdatePaymentDto): Promise<import("../schemas/payment-gateway.schema").PaymentGatewayDocument>;
    getStateCodes(): Promise<{
        code: string;
        name: string;
    }[]>;
    createMerchant(merchant: CreateMerchantDto): Promise<{
        signatureUrl: any;
    }>;
    getMerchantStatus(): Promise<any>;
}
