"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminPaymentGatewayController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const super_admin_auth_guard_1 = require("../../auth/guards/super-admin-auth.guard");
const admin_update_payment_dto_1 = require("../dto/admin-update-payment.dto");
const update_payment_dto_1 = require("../dto/update-payment.dto");
const admin_payment_entity_1 = require("../entity/admin-payment.entity");
const admin_payment_logic_1 = require("../logics/admin-payment.logic");
const payment_gateway_logic_1 = require("../logics/payment-gateway.logic");
let AdminPaymentGatewayController = class AdminPaymentGatewayController {
    constructor(adminPaymentLogic, paymentGatewayLogic) {
        this.adminPaymentLogic = adminPaymentLogic;
        this.paymentGatewayLogic = paymentGatewayLogic;
    }
    async getPaymentStatus(query) {
        return await this.adminPaymentLogic.getPaymentStatus(query.restaurantId);
    }
    async updateBankAccountApproved(query, payload) {
        return await this.adminPaymentLogic.updateBankAccountApproved(query.restaurantId, payload);
    }
    async getCoPilotStatus(query) {
        return await this.adminPaymentLogic.getCoPilotStatus(query.restaurantId);
    }
    async getPaymentGatewaySetting(id) {
        return await this.paymentGatewayLogic.getPaymentGatewayWithDecode(id);
    }
    async updateMIDPayment(id, payload) {
        await this.paymentGatewayLogic.updatePaymentGateway(id, payload);
        return { success: true };
    }
};
__decorate([
    (0, common_1.Get)('status'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_payment_entity_1.GetAdminPaymentStatusResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Payment Gateway Approved Status' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [admin_update_payment_dto_1.GetAdminPaymentStatusDto]),
    __metadata("design:returntype", Promise)
], AdminPaymentGatewayController.prototype, "getPaymentStatus", null);
__decorate([
    (0, common_1.Patch)('bank-account'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Payment Gateway Approved Status' }),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [admin_update_payment_dto_1.GetAdminPaymentStatusDto,
        admin_update_payment_dto_1.UpdateBankAccountStatusDto]),
    __metadata("design:returntype", Promise)
], AdminPaymentGatewayController.prototype, "updateBankAccountApproved", null);
__decorate([
    (0, common_1.Get)('co-pilot'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_payment_entity_1.GetCoPilotPaymentStatusResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get CoPilot Application Status',
        description: 'จะ get ก็ต่อเมื่อหลังจาก get `/v1/admin/payment-gateway/status` แล้ว `midCoPilot` ไม่ใช่ `null`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [admin_update_payment_dto_1.GetAdminPaymentStatusDto]),
    __metadata("design:returntype", Promise)
], AdminPaymentGatewayController.prototype, "getCoPilotStatus", null);
__decorate([
    (0, common_1.Get)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_payment_entity_1.GetPaymentGatewayResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Payment Gateway Setting Detail',
        description: 'ใช้ตอนกด Edit เพื่อจะดูหรือแก้ไขข้อมูลในหน้า Super Admin'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminPaymentGatewayController.prototype, "getPaymentGatewaySetting", null);
__decorate([
    (0, common_1.Put)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Update Payment Gateway Setting' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_payment_dto_1.UpdatePaymentDto]),
    __metadata("design:returntype", Promise)
], AdminPaymentGatewayController.prototype, "updateMIDPayment", null);
AdminPaymentGatewayController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('admin/payment-gateway'),
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    (0, common_1.Controller)('v1/admin/payment-gateway'),
    __metadata("design:paramtypes", [admin_payment_logic_1.AdminPaymentLogic,
        payment_gateway_logic_1.PaymentGatewayLogic])
], AdminPaymentGatewayController);
exports.AdminPaymentGatewayController = AdminPaymentGatewayController;
//# sourceMappingURL=admin-payment.controller.js.map