export declare class GetAdminPaymentStatusDto {
    readonly restaurantId: string;
}
export declare class UpdateBankAccountStatusDto {
    readonly isBankAccountApproved: boolean;
}
