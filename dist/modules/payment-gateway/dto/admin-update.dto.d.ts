export declare class UpdateAdminPaymentDto {
    restaurantId: string;
    paymentStatus: string;
    readonly mid: string;
    readonly username: string;
    password: string;
    readonly isConvenienceFee: boolean;
    readonly convenienceFee: number;
}
