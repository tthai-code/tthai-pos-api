export declare class UpdateBankAccountDto {
    restaurantId: string;
    token: string;
    readonly name: string;
    readonly printName: string;
    readonly title: string;
    readonly accountType: string;
    accountNumber: string;
    routingNumber: string;
    readonly signature: string;
    date: string;
}
