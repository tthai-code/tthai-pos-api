export declare class GetPaymentGatewayDto {
    readonly restaurantId: string;
}
export declare class BinTokenDto extends GetPaymentGatewayDto {
    readonly token: string;
}
