"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateBankAccountDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const ach_bank_type_enum_1 = require("../common/ach-bank.type.enum");
const dayjs = require("dayjs");
class UpdateBankAccountDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Some Account Name' }),
    __metadata("design:type", String)
], UpdateBankAccountDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Authorized person name' }),
    __metadata("design:type", String)
], UpdateBankAccountDto.prototype, "printName", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Title' }),
    __metadata("design:type", String)
], UpdateBankAccountDto.prototype, "title", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(ach_bank_type_enum_1.ACHBankAccountTypeEnum),
    (0, swagger_1.ApiProperty)({
        example: ach_bank_type_enum_1.ACHBankAccountTypeEnum.SAVING,
        enum: Object.values(ach_bank_type_enum_1.ACHBankAccountTypeEnum)
    }),
    __metadata("design:type", String)
], UpdateBankAccountDto.prototype, "accountType", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '1234123412341234' }),
    __metadata("design:type", String)
], UpdateBankAccountDto.prototype, "accountNumber", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.Length)(9, 9),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '011401533' }),
    __metadata("design:type", String)
], UpdateBankAccountDto.prototype, "routingNumber", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Some Name' }),
    __metadata("design:type", String)
], UpdateBankAccountDto.prototype, "signature", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({
        example: dayjs().toISOString(),
        description: 'Format `ISO string`'
    }),
    __metadata("design:type", String)
], UpdateBankAccountDto.prototype, "date", void 0);
exports.UpdateBankAccountDto = UpdateBankAccountDto;
//# sourceMappingURL=bank-account.dto.js.map