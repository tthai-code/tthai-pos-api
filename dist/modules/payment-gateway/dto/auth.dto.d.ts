interface IAuthorization {
    merchid: string;
    account: string;
    expiry: string;
    amount: string;
    currency: string;
    name: string;
}
export declare class AuthNoCaptureDto implements IAuthorization {
    merchid: string;
    restaurantId: string;
    account: string;
    readonly expiry: string;
    readonly amount: string;
    readonly currency: string;
    readonly name: string;
    readonly orderid: string;
}
export declare class AuthWithCaptureDto extends AuthNoCaptureDto {
    ecomind: string;
    capture: string;
    readonly cvv2: string;
    readonly postal: string;
}
export {};
