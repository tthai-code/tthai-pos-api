"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateMerchantDto = exports.OwnerShipObjectDto = exports.OwnerObjectDto = exports.BankDetailObjectDto = exports.BankObjectDto = exports.DemographicObjectDto = exports.BusinessAddressObjectDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const bank_account_type_enum_1 = require("../common/bank-account-type.enum");
const state_code_enum_1 = require("../common/state-code.enum");
class BusinessAddressObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '1000 Continental Dr' }),
    __metadata("design:type", String)
], BusinessAddressObjectDto.prototype, "address1", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'King of Prussia' }),
    __metadata("design:type", String)
], BusinessAddressObjectDto.prototype, "city", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '19406' }),
    __metadata("design:type", String)
], BusinessAddressObjectDto.prototype, "zip", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(state_code_enum_1.stateCodes.map((item) => item.code)),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'PA', enum: state_code_enum_1.stateCodes.map((item) => item.code) }),
    __metadata("design:type", String)
], BusinessAddressObjectDto.prototype, "stateCd", void 0);
exports.BusinessAddressObjectDto = BusinessAddressObjectDto;
class DemographicObjectDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => BusinessAddressObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, swagger_1.ApiProperty)({ type: BusinessAddressObjectDto }),
    __metadata("design:type", BusinessAddressObjectDto)
], DemographicObjectDto.prototype, "businessAddress", void 0);
exports.DemographicObjectDto = DemographicObjectDto;
class BankObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankObjectDto.prototype, "bankAcctNum", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankObjectDto.prototype, "bankRoutingNum", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(bank_account_type_enum_1.bankAcctTypes.map((item) => item.code)),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ enum: bank_account_type_enum_1.bankAcctTypes.map((item) => item.code) }),
    __metadata("design:type", String)
], BankObjectDto.prototype, "bankAcctTypeCd", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankObjectDto.prototype, "bankName", void 0);
exports.BankObjectDto = BankObjectDto;
class BankDetailObjectDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => BankObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, swagger_1.ApiProperty)({
        type: BankObjectDto,
        example: {
            bankAcctNum: '12345678',
            bankRoutingNum: '325272212',
            bankAcctTypeCd: 'SAVINGS',
            bankName: 'Dep Bank'
        }
    }),
    __metadata("design:type", BankObjectDto)
], BankDetailObjectDto.prototype, "depositBank", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => BankObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, swagger_1.ApiProperty)({
        type: BankObjectDto,
        example: {
            bankAcctNum: '12345678',
            bankRoutingNum: '325272212',
            bankAcctTypeCd: 'SAVINGS',
            bankName: 'With Bank'
        }
    }),
    __metadata("design:type", BankObjectDto)
], BankDetailObjectDto.prototype, "withdrawalBank", void 0);
exports.BankDetailObjectDto = BankDetailObjectDto;
class OwnerObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'testing@test.com' }),
    __metadata("design:type", String)
], OwnerObjectDto.prototype, "ownerEmail", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'test testing' }),
    __metadata("design:type", String)
], OwnerObjectDto.prototype, "ownerName", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'OWNER' }),
    __metadata("design:type", String)
], OwnerObjectDto.prototype, "ownerTitle", void 0);
exports.OwnerObjectDto = OwnerObjectDto;
class OwnerShipObjectDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => OwnerObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, swagger_1.ApiProperty)({ type: OwnerObjectDto }),
    __metadata("design:type", OwnerObjectDto)
], OwnerShipObjectDto.prototype, "owner", void 0);
exports.OwnerShipObjectDto = OwnerShipObjectDto;
class CreateMerchantDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'TestakaBusinessName' }),
    __metadata("design:type", String)
], CreateMerchantDto.prototype, "akaBusinessName", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'TestdbaName' }),
    __metadata("design:type", String)
], CreateMerchantDto.prototype, "dbaName", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'TestLegalBusinessName' }),
    __metadata("design:type", String)
], CreateMerchantDto.prototype, "legalBusinessName", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => DemographicObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, swagger_1.ApiProperty)({ type: DemographicObjectDto }),
    __metadata("design:type", DemographicObjectDto)
], CreateMerchantDto.prototype, "demographic", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => BankDetailObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, swagger_1.ApiProperty)({ type: BankDetailObjectDto }),
    __metadata("design:type", BankDetailObjectDto)
], CreateMerchantDto.prototype, "bankDetail", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => OwnerShipObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, swagger_1.ApiProperty)({ type: OwnerShipObjectDto }),
    __metadata("design:type", OwnerShipObjectDto)
], CreateMerchantDto.prototype, "ownership", void 0);
exports.CreateMerchantDto = CreateMerchantDto;
//# sourceMappingURL=create-merchant.dto.js.map