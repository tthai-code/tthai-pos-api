export declare class UpdatePaymentDto {
    restaurantId: string;
    paymentStatus: string;
    readonly mid: string;
    readonly username: string;
    password: string;
    readonly isConvenienceFee: boolean;
    readonly convenienceFee: number;
    readonly isServiceCharge: boolean;
    readonly serviceCharge: number;
}
