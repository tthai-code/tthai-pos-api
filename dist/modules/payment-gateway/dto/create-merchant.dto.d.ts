export declare class BusinessAddressObjectDto {
    readonly address1: string;
    readonly city: string;
    readonly zip: string;
    readonly stateCd: string;
}
export declare class DemographicObjectDto {
    readonly businessAddress: BusinessAddressObjectDto;
}
export declare class BankObjectDto {
    readonly bankAcctNum: string;
    readonly bankRoutingNum: string;
    readonly bankAcctTypeCd: string;
    readonly bankName: string;
}
export declare class BankDetailObjectDto {
    readonly depositBank: BankObjectDto;
    readonly withdrawalBank: BankObjectDto;
}
export declare class OwnerObjectDto {
    readonly ownerEmail: string;
    readonly ownerName: string;
    readonly ownerTitle: string;
}
export declare class OwnerShipObjectDto {
    readonly owner: OwnerObjectDto;
}
export declare class CreateMerchantDto {
    salesCode: string;
    readonly akaBusinessName: string;
    readonly dbaName: string;
    readonly legalBusinessName: string;
    readonly demographic: DemographicObjectDto;
    readonly bankDetail: BankDetailObjectDto;
    readonly ownership: OwnerShipObjectDto;
}
