export declare enum PaymentGatewayStatusEnum {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}
