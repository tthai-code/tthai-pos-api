export declare enum ACHBankAccountTypeEnum {
    CHECKING = "CHECKING",
    SAVING = "SAVING"
}
export declare enum BankAccountStatusEnum {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}
