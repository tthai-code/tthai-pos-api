"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bankAcctTypes = void 0;
exports.bankAcctTypes = [
    {
        code: 'BIZ',
        name: 'Business Checking'
    },
    {
        code: 'GL',
        name: 'General Ledger'
    },
    {
        code: 'SAVINGS',
        name: 'Savings'
    }
];
//# sourceMappingURL=bank-account-type.enum.js.map