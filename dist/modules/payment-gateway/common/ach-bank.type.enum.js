"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BankAccountStatusEnum = exports.ACHBankAccountTypeEnum = void 0;
var ACHBankAccountTypeEnum;
(function (ACHBankAccountTypeEnum) {
    ACHBankAccountTypeEnum["CHECKING"] = "CHECKING";
    ACHBankAccountTypeEnum["SAVING"] = "SAVING";
})(ACHBankAccountTypeEnum = exports.ACHBankAccountTypeEnum || (exports.ACHBankAccountTypeEnum = {}));
var BankAccountStatusEnum;
(function (BankAccountStatusEnum) {
    BankAccountStatusEnum["ACTIVE"] = "ACTIVE";
    BankAccountStatusEnum["INACTIVE"] = "INACTIVE";
})(BankAccountStatusEnum = exports.BankAccountStatusEnum || (exports.BankAccountStatusEnum = {}));
//# sourceMappingURL=ach-bank.type.enum.js.map