"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoPilotStatusEnum = void 0;
var CoPilotStatusEnum;
(function (CoPilotStatusEnum) {
    CoPilotStatusEnum["INPROG"] = "Not Submitted.";
    CoPilotStatusEnum["OFS"] = "Pending Signature.";
    CoPilotStatusEnum["QUALIFY"] = "Qualifying.";
    CoPilotStatusEnum["UNDER"] = "Underwriting.";
    CoPilotStatusEnum["DECLINED"] = "Declined.";
    CoPilotStatusEnum["BOARDING"] = "Boarding";
    CoPilotStatusEnum["BOARDED"] = "Boarded";
    CoPilotStatusEnum["LIVE"] = "Live";
    CoPilotStatusEnum["CANCELLED"] = "Cancelled";
    CoPilotStatusEnum["DEFAULT"] = "-";
})(CoPilotStatusEnum = exports.CoPilotStatusEnum || (exports.CoPilotStatusEnum = {}));
//# sourceMappingURL=co-pilot-status.enum.js.map