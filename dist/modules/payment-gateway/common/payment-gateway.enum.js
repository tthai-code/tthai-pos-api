"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentGatewayStatusEnum = void 0;
var PaymentGatewayStatusEnum;
(function (PaymentGatewayStatusEnum) {
    PaymentGatewayStatusEnum["ACTIVE"] = "ACTIVE";
    PaymentGatewayStatusEnum["INACTIVE"] = "INACTIVE";
})(PaymentGatewayStatusEnum = exports.PaymentGatewayStatusEnum || (exports.PaymentGatewayStatusEnum = {}));
//# sourceMappingURL=payment-gateway.enum.js.map