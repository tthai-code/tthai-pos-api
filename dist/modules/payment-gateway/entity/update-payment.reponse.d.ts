import { ResponseDto } from 'src/common/entity/response.entity';
export declare class UpdatePaymentResponse {
    restaurant_id: string;
    mid: string;
    username: string;
    password: string;
    is_convenience_fee: boolean;
    convenience_fee: number;
    is_service_charge: boolean;
    service_charge: number;
}
export declare class UpdatePaymentResponseDto extends ResponseDto<UpdatePaymentResponse> {
    data: UpdatePaymentResponse;
}
