import { ResponseDto } from 'src/common/entity/response.entity';
declare class CreateMerchantField {
    signature_url: string;
}
export declare class CreateMerchantResponse extends ResponseDto<CreateMerchantField> {
    data: CreateMerchantField;
}
export {};
