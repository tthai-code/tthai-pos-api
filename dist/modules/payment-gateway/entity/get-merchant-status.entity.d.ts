import { ResponseDto } from 'src/common/entity/response.entity';
declare class SignerField {
    email: string;
    firstName: string;
    lastName: string;
    ipAddress: string;
    personalGuaranteeFlg: boolean;
}
declare class SignatureStatusField {
    signedSiteUserId: number;
    signedDateTime: string;
    signer: SignerField;
    signatureUrl: string;
    signatureStatusCd: string;
}
declare class MerchantStatusField {
    errors: string;
    signatureStatus: SignatureStatusField;
}
export declare class SignatureStatusResponse extends ResponseDto<MerchantStatusField> {
    data: MerchantStatusField;
}
export {};
