"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateBankAccountResponse = exports.GetBankAccountResponse = exports.GetActiveBankAccountResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const ach_bank_type_enum_1 = require("../common/ach-bank.type.enum");
class BankAccountField {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(ach_bank_type_enum_1.BankAccountStatusEnum) }),
    __metadata("design:type", String)
], BankAccountField.prototype, "bank_account_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "token", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ description: 'Format ISO Date' }),
    __metadata("design:type", String)
], BankAccountField.prototype, "date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "signature", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "routing_number", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "account_number", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(ach_bank_type_enum_1.ACHBankAccountTypeEnum) }),
    __metadata("design:type", String)
], BankAccountField.prototype, "account_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "print_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BankAccountField.prototype, "id", void 0);
class GetActiveBankAccountResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: BankAccountField,
        example: {
            bank_account_status: 'ACTIVE',
            token: '9031395982141234',
            date: '2022-11-24T10:48:15.658Z',
            signature: 'Cory Wong 1',
            routing_number: 'XXXXXX808',
            account_number: 'XXXXXXXXXXXX1234',
            account_type: 'SAVING',
            title: 'Mr.',
            print_name: 'Cory Wong',
            name: 'Cory Wong 1',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            id: '63844b25c05b5be2ac6c6657'
        }
    }),
    __metadata("design:type", BankAccountField)
], GetActiveBankAccountResponse.prototype, "data", void 0);
exports.GetActiveBankAccountResponse = GetActiveBankAccountResponse;
class GetBankAccountResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [BankAccountField],
        example: [
            {
                bank_account_status: 'ACTIVE',
                token: '9031395982141234',
                date: '2022-11-24T10:48:15.658Z',
                signature: 'Cory Wong',
                routing_number: 'XXXXXX808',
                account_number: 'XXXXXXXXXXXX1234',
                account_type: 'SAVING',
                title: 'Mr.',
                print_name: 'Cory Wong',
                name: 'Cory Wong',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                id: '63844ba6438534c4ecaf0b49'
            },
            {
                bank_account_status: 'INACTIVE',
                token: '9031395982141234',
                date: '2022-11-24T10:48:15.658Z',
                signature: 'Cory Wong 1',
                routing_number: 'XXXXXX808',
                account_number: 'XXXXXXXXXXXX1234',
                account_type: 'SAVING',
                title: 'Mr.',
                print_name: 'Cory Wong',
                name: 'Cory Wong 1',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                id: '63844b25c05b5be2ac6c6657'
            }
        ]
    }),
    __metadata("design:type", Array)
], GetBankAccountResponse.prototype, "data", void 0);
exports.GetBankAccountResponse = GetBankAccountResponse;
class UpdateBankAccountResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: BankAccountField,
        example: {
            bank_account_status: 'ACTIVE',
            token: '9031395982141234',
            date: '2022-11-24T10:48:15.658Z',
            signature: 'Cory Wong',
            routing_number: 'XXXXXX808',
            account_number: 'XXXXXXXXXXXX1234',
            account_type: 'SAVING',
            title: 'Mr.',
            print_name: 'Cory Wong',
            name: 'Cory Wong',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            id: '63844ba6438534c4ecaf0b49'
        }
    }),
    __metadata("design:type", BankAccountField)
], UpdateBankAccountResponse.prototype, "data", void 0);
exports.UpdateBankAccountResponse = UpdateBankAccountResponse;
//# sourceMappingURL=bank-account.entity.js.map