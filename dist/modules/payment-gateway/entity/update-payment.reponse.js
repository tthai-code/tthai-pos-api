"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePaymentResponseDto = exports.UpdatePaymentResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class UpdatePaymentResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], UpdatePaymentResponse.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], UpdatePaymentResponse.prototype, "mid", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], UpdatePaymentResponse.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], UpdatePaymentResponse.prototype, "password", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], UpdatePaymentResponse.prototype, "is_convenience_fee", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdatePaymentResponse.prototype, "convenience_fee", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], UpdatePaymentResponse.prototype, "is_service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], UpdatePaymentResponse.prototype, "service_charge", void 0);
exports.UpdatePaymentResponse = UpdatePaymentResponse;
class UpdatePaymentResponseDto extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: UpdatePaymentResponse,
        example: {
            created_by: {
                username: 'system',
                id: '0'
            },
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            status: 'active',
            convenience_fee: 0.99,
            is_convenience_fee: true,
            service_charge: 0.99,
            is_service_charge: true,
            password: 'dGVzdDEyMzQ=',
            username: 'testing',
            mid: '820000003069',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            updated_at: '2022-10-09T20:00:19.328Z',
            created_at: '2022-10-09T19:56:13.672Z',
            id: '6343275d77f67acb062e51cc'
        }
    }),
    __metadata("design:type", UpdatePaymentResponse)
], UpdatePaymentResponseDto.prototype, "data", void 0);
exports.UpdatePaymentResponseDto = UpdatePaymentResponseDto;
//# sourceMappingURL=update-payment.reponse.js.map