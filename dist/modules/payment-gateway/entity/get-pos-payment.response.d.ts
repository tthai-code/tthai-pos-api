import { ResponseDto } from 'src/common/entity/response.entity';
declare class PaymentGatewayFields {
    mid: string;
    username: string;
    password: string;
    is_convenience_fee: boolean;
    convenience_fee: number;
}
export declare class PaymentGatewayResponse extends ResponseDto<PaymentGatewayFields> {
    data: PaymentGatewayFields;
}
export {};
