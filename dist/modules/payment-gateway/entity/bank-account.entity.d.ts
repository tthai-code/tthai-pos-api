import { ResponseDto } from 'src/common/entity/response.entity';
declare class BankAccountField {
    bank_account_status: string;
    token: string;
    date: string;
    signature: string;
    routing_number: string;
    account_number: string;
    account_type: string;
    title: string;
    print_name: string;
    name: string;
    restaurant_id: string;
    id: string;
}
export declare class GetActiveBankAccountResponse extends ResponseDto<BankAccountField> {
    data: BankAccountField;
}
export declare class GetBankAccountResponse extends ResponseDto<BankAccountField[]> {
    data: BankAccountField[];
}
export declare class UpdateBankAccountResponse extends ResponseDto<BankAccountField> {
    data: BankAccountField;
}
export {};
