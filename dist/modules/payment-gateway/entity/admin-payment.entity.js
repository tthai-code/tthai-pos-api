"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetPaymentGatewayResponse = exports.GetCoPilotPaymentStatusResponse = exports.GetAdminPaymentStatusResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const co_pilot_status_enum_1 = require("../common/co-pilot-status.enum");
class GetAdminPaymentStatusObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetAdminPaymentStatusObject.prototype, "mid_co_pilot", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], GetAdminPaymentStatusObject.prototype, "is_bank_account_approved", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetAdminPaymentStatusObject.prototype, "id", void 0);
class GetCoPilotPaymentStatusObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], GetCoPilotPaymentStatusObject.prototype, "co_pilot_merchant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(co_pilot_status_enum_1.CoPilotStatusEnum) }),
    __metadata("design:type", String)
], GetCoPilotPaymentStatusObject.prototype, "boarding_process_status", void 0);
class GetAdminPaymentStatusResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: GetAdminPaymentStatusObject,
        example: {
            mid_co_pilot: '103527650',
            is_bank_account_approved: true,
            id: '63984d6690416c27a741592a'
        }
    }),
    __metadata("design:type", GetAdminPaymentStatusObject)
], GetAdminPaymentStatusResponse.prototype, "data", void 0);
exports.GetAdminPaymentStatusResponse = GetAdminPaymentStatusResponse;
class GetCoPilotPaymentStatusResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: GetCoPilotPaymentStatusObject,
        example: {
            co_pilot_merchant_id: '103527650',
            boarding_process_status: 'Pending Signature.'
        }
    }),
    __metadata("design:type", GetCoPilotPaymentStatusObject)
], GetCoPilotPaymentStatusResponse.prototype, "data", void 0);
exports.GetCoPilotPaymentStatusResponse = GetCoPilotPaymentStatusResponse;
class PaymentGatewaySettingObject {
}
class GetPaymentGatewayResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: PaymentGatewaySettingObject,
        example: {
            mid: '820000003069',
            username: 'testing',
            password: 'testing123',
            is_convenience_fee: true,
            convenience_fee: 0.99
        }
    }),
    __metadata("design:type", PaymentGatewaySettingObject)
], GetPaymentGatewayResponse.prototype, "data", void 0);
exports.GetPaymentGatewayResponse = GetPaymentGatewayResponse;
//# sourceMappingURL=admin-payment.entity.js.map