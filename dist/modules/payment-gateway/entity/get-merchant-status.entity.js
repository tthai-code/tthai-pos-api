"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignatureStatusResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class SignerField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignerField.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignerField.prototype, "firstName", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignerField.prototype, "lastName", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignerField.prototype, "ipAddress", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], SignerField.prototype, "personalGuaranteeFlg", void 0);
class SignatureStatusField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SignatureStatusField.prototype, "signedSiteUserId", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignatureStatusField.prototype, "signedDateTime", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", SignerField)
], SignatureStatusField.prototype, "signer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignatureStatusField.prototype, "signatureUrl", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SignatureStatusField.prototype, "signatureStatusCd", void 0);
class MerchantStatusField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MerchantStatusField.prototype, "errors", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", SignatureStatusField)
], MerchantStatusField.prototype, "signatureStatus", void 0);
class SignatureStatusResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: MerchantStatusField,
        example: {
            errors: null,
            signatureStatus: {
                signedSiteUserId: 237322,
                signedDateTime: '07/19/2017 04:44:28 PM',
                signer: {
                    email: 'name@email.com',
                    firstName: 'Forename',
                    lastName: 'Surname',
                    ipAddress: '192.168.125.126',
                    personalGuaranteeFlg: true
                },
                signatureUrl: 'https://cardpointe-uat.cardconnect.com/account/login#/password/02d34909-93b3-4b50-adf4-1ff91e92b33c/c08709da-8af1-4f35-8e0f-f52eca60ef81?newuser',
                signatureStatusCd: 'SIGNED'
            }
        }
    }),
    __metadata("design:type", MerchantStatusField)
], SignatureStatusResponse.prototype, "data", void 0);
exports.SignatureStatusResponse = SignatureStatusResponse;
//# sourceMappingURL=get-merchant-status.entity.js.map