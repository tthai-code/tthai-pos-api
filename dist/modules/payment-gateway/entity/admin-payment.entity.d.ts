import { ResponseDto } from 'src/common/entity/response.entity';
declare class GetAdminPaymentStatusObject {
    mid_co_pilot: string;
    is_bank_account_approved: boolean;
    id: string;
}
declare class GetCoPilotPaymentStatusObject {
    co_pilot_merchant_id: number;
    boarding_process_status: string;
}
export declare class GetAdminPaymentStatusResponse extends ResponseDto<GetAdminPaymentStatusObject> {
    data: GetAdminPaymentStatusObject;
}
export declare class GetCoPilotPaymentStatusResponse extends ResponseDto<GetCoPilotPaymentStatusObject> {
    data: GetCoPilotPaymentStatusObject;
}
declare class PaymentGatewaySettingObject {
    mid: string;
    username: string;
    password: string;
    is_convenience_fee: boolean;
    convenience_fee: number;
}
export declare class GetPaymentGatewayResponse extends ResponseDto<PaymentGatewaySettingObject> {
    data: PaymentGatewaySettingObject;
}
export {};
