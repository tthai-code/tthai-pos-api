import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type PaymentGatewayDocument = PaymentGatewayFields & Document;
export declare class PaymentGatewayFields {
    restaurantId: Types.ObjectId;
    mid: string;
    username: string;
    password: string;
    isConvenienceFee: boolean;
    convenienceFee: number;
    isServiceCharge: boolean;
    serviceCharge: number;
    midCoPilot: string;
    paymentStatus: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const PaymentGatewaySchema: MongooseSchema<Document<PaymentGatewayFields, any, any>, import("mongoose").Model<Document<PaymentGatewayFields, any, any>, any, any, any>, any, any>;
