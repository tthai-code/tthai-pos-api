import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type BankAccountDocument = BankAccountFields & Document;
export declare class BankAccountFields {
    restaurantId: Types.ObjectId;
    name: string;
    printName: string;
    title: string;
    accountType: string;
    accountNumber: string;
    routingNumber: string;
    signature: string;
    date: Date;
    token: string;
    bankAccountStatus: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const BankAccountSchema: MongooseSchema<Document<BankAccountFields, any, any>, import("mongoose").Model<Document<BankAccountFields, any, any>, any, any, any>, any, any>;
