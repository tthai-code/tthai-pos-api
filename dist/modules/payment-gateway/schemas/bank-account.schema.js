"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BankAccountSchema = exports.BankAccountFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const ach_bank_type_enum_1 = require("../common/ach-bank.type.enum");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
let BankAccountFields = class BankAccountFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], BankAccountFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "printName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "title", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(ach_bank_type_enum_1.ACHBankAccountTypeEnum),
        default: ach_bank_type_enum_1.ACHBankAccountTypeEnum.SAVING
    }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "accountType", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "accountNumber", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "routingNumber", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "signature", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], BankAccountFields.prototype, "date", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "token", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: ach_bank_type_enum_1.BankAccountStatusEnum.ACTIVE,
        enum: Object.values(ach_bank_type_enum_1.BankAccountStatusEnum)
    }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "bankAccountStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], BankAccountFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], BankAccountFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], BankAccountFields.prototype, "createdBy", void 0);
BankAccountFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'bankAccounts' })
], BankAccountFields);
exports.BankAccountFields = BankAccountFields;
exports.BankAccountSchema = mongoose_1.SchemaFactory.createForClass(BankAccountFields);
exports.BankAccountSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=bank-account.schema.js.map