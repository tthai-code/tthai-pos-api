"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminPaymentLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const payment_gateway_service_1 = require("../services/payment-gateway.service");
const co_pilot_logic_1 = require("./co-pilot.logic");
const payment_gateway_logic_1 = require("./payment-gateway.logic");
let AdminPaymentLogic = class AdminPaymentLogic {
    constructor(paymentGatewayService, paymentGatewayLogic, restaurantService, coPilotLogic) {
        this.paymentGatewayService = paymentGatewayService;
        this.paymentGatewayLogic = paymentGatewayLogic;
        this.restaurantService = restaurantService;
        this.coPilotLogic = coPilotLogic;
    }
    displayCoPilotStatus(status = '') {
        switch (status) {
            case 'INPROG':
                return 'Not Submitted.';
            case 'OFS':
                return 'Pending Signature.';
            case 'QUALIFY':
                return 'Qualifying.';
            case 'UNDER':
                return 'Underwriting.';
            case 'DECLINED':
                return 'Declined.';
            case 'BOARDING':
                return 'Boarding';
            case 'BOARDED':
                return 'Boarded';
            case 'LIVE':
                return 'Live';
            case 'CANCELLED':
                return 'Cancelled';
            default:
                return '-';
        }
    }
    async getPaymentStatus(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: { $ne: status_enum_1.StatusEnum.DELETED }
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const payment = await this.paymentGatewayService.findOne({ restaurantId }, { isBankAccountApproved: 1, midCoPilot: 1 });
        if (!payment) {
            return {
                isBankAccountApproved: false,
                midCoPilot: null
            };
        }
        return payment;
    }
    async updateBankAccountApproved(id, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        await this.paymentGatewayService.findOneAndUpdate({ restaurantId: id }, payload);
        return { success: true };
    }
    async getCoPilotStatus(restaurantId) {
        var _a;
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const data = await this.coPilotLogic.getMerchantSignatureStatus(restaurantId);
        const { merchant } = data;
        const result = {
            coPilotMerchantId: merchant === null || merchant === void 0 ? void 0 : merchant.merchantId,
            boardingProcessStatus: this.displayCoPilotStatus((_a = merchant === null || merchant === void 0 ? void 0 : merchant.merchantStatus) === null || _a === void 0 ? void 0 : _a.boardingProcessStatusCd)
        };
        return result;
    }
    async updatePaymentMerchantId(restaurantId, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        await this.paymentGatewayService.findOneAndUpdate({ restaurantId }, payload);
        return { success: true };
    }
};
AdminPaymentLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [payment_gateway_service_1.PaymentGatewayService,
        payment_gateway_logic_1.PaymentGatewayLogic,
        restaurant_service_1.RestaurantService,
        co_pilot_logic_1.CoPilotLogic])
], AdminPaymentLogic);
exports.AdminPaymentLogic = AdminPaymentLogic;
//# sourceMappingURL=admin-payment.logic.js.map