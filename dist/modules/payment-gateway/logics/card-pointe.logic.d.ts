import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { AuthNoCaptureDto, AuthWithCaptureDto } from '../dto/auth.dto';
import { TokenizeCardDto } from '../dto/tokenize.dto';
import { IOnlinePayment } from '../interfaces/card-pointe.interface';
import { PaymentGatewayHttpService } from '../services/payment-gateway-http.service';
import { PaymentGatewayService } from '../services/payment-gateway.service';
import { IPaymentGateway, PaymentGatewayLogic } from './payment-gateway.logic';
export declare class CardPointeLogic {
    private readonly paymentService;
    private readonly paymentLogic;
    private readonly httpService;
    private readonly restaurantService;
    constructor(paymentService: PaymentGatewayService, paymentLogic: PaymentGatewayLogic, httpService: PaymentGatewayHttpService, restaurantService: RestaurantService);
    identifyPaymentMethod(product: string, paymentMethod: string): {
        cardType: any;
        paymentMethod: any;
    };
    findOneRestaurantPayment(restaurantId: any): Promise<IPaymentGateway>;
    tokenizeAndBinToken(payload: TokenizeCardDto): Promise<{
        ending: string;
        token: any;
        binToken: any;
    }>;
    authorizeNoCapture(payload: AuthNoCaptureDto): Promise<AuthNoCaptureDto>;
    authorizeWithCapture(payload: AuthWithCaptureDto): Promise<any>;
    voidTransaction(retRef: string, restaurantId: string): Promise<any>;
    closeBatch(restaurantId: string, batchId?: number): Promise<any>;
    refundTransaction(retRef: string, restaurantId: string, amount?: number): Promise<any>;
    captureTransaction(retRef: string, restaurantId: string, amount?: number): Promise<any>;
    payOnlineTransaction(restaurantId: string, payload: IOnlinePayment): Promise<any>;
}
