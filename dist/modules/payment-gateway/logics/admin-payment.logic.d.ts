import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdateBankAccountStatusDto } from '../dto/admin-update-payment.dto';
import { PaymentGatewayService } from '../services/payment-gateway.service';
import { CoPilotLogic } from './co-pilot.logic';
import { PaymentGatewayLogic } from './payment-gateway.logic';
export declare class AdminPaymentLogic {
    private readonly paymentGatewayService;
    private readonly paymentGatewayLogic;
    private readonly restaurantService;
    private readonly coPilotLogic;
    constructor(paymentGatewayService: PaymentGatewayService, paymentGatewayLogic: PaymentGatewayLogic, restaurantService: RestaurantService, coPilotLogic: CoPilotLogic);
    private displayCoPilotStatus;
    getPaymentStatus(restaurantId: string): Promise<import("../schemas/payment-gateway.schema").PaymentGatewayDocument | {
        isBankAccountApproved: boolean;
        midCoPilot: any;
    }>;
    updateBankAccountApproved(id: string, payload: UpdateBankAccountStatusDto): Promise<{
        success: boolean;
    }>;
    getCoPilotStatus(restaurantId: string): Promise<{
        coPilotMerchantId: any;
        boardingProcessStatus: string;
    }>;
    updatePaymentMerchantId(restaurantId: string, payload: any): Promise<{
        success: boolean;
    }>;
}
