import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { SubscriptionLogic } from 'src/modules/subscription/logic/subscription.logic';
import { UpdateBankAccountDto } from '../dto/bank-account.dto';
import { BankAccountService } from '../services/bank-account.service';
import { PaymentGatewayHttpService } from '../services/payment-gateway-http.service';
import { PaymentGatewayLogic } from './payment-gateway.logic';
export declare class BankAccountLogic {
    private readonly bankAccountService;
    private readonly restaurantService;
    private readonly cardPointeService;
    private readonly paymentLogic;
    private readonly subscriptionLogic;
    constructor(bankAccountService: BankAccountService, restaurantService: RestaurantService, cardPointeService: PaymentGatewayHttpService, paymentLogic: PaymentGatewayLogic, subscriptionLogic: SubscriptionLogic);
    private censorText;
    getBankAccount(restaurantId: string): Promise<import("../schemas/bank-account.schema").BankAccountDocument>;
    getBankAccounts(restaurantId: string): Promise<any[]>;
    getBankAccountForAdmin(restaurantId: string): Promise<any[]>;
    getBankAccountById(id: string): Promise<import("../schemas/bank-account.schema").BankAccountDocument>;
    updateBankAccount(restaurantId: string, payload: UpdateBankAccountDto): Promise<import("../schemas/bank-account.schema").BankAccountDocument>;
}
