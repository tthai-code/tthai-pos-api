"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BankAccountLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const subscription_logic_1 = require("../../subscription/logic/subscription.logic");
const ach_bank_type_enum_1 = require("../common/ach-bank.type.enum");
const bank_account_service_1 = require("../services/bank-account.service");
const payment_gateway_http_service_1 = require("../services/payment-gateway-http.service");
const payment_gateway_logic_1 = require("./payment-gateway.logic");
let BankAccountLogic = class BankAccountLogic {
    constructor(bankAccountService, restaurantService, cardPointeService, paymentLogic, subscriptionLogic) {
        this.bankAccountService = bankAccountService;
        this.restaurantService = restaurantService;
        this.cardPointeService = cardPointeService;
        this.paymentLogic = paymentLogic;
        this.subscriptionLogic = subscriptionLogic;
    }
    censorText(text, last) {
        const length = text.length;
        return text.slice(-last).padStart(length, 'X');
    }
    async getBankAccount(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const bankAccount = await this.bankAccountService.findOne({ restaurantId, bankAccountStatus: ach_bank_type_enum_1.BankAccountStatusEnum.ACTIVE }, { status: 0, updatedBy: 0, updatedAt: 0, createdBy: 0, createdAt: 0 });
        return bankAccount;
    }
    async getBankAccounts(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const bankAccount = await this.bankAccountService.getAll({ restaurantId }, { status: 0, updatedBy: 0, updatedAt: 0, createdBy: 0, createdAt: 0 });
        return bankAccount;
    }
    async getBankAccountForAdmin(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: { $ne: status_enum_1.StatusEnum.DELETED }
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const bankAccount = await this.bankAccountService.getAll({ restaurantId }, { date: 1 });
        return bankAccount;
    }
    async getBankAccountById(id) {
        return this.bankAccountService.findOne({ _id: id }, { status: 0, updatedBy: 0, updatedAt: 0, createdBy: 0, createdAt: 0 });
    }
    async updateBankAccount(restaurantId, payload) {
        const logic = async (session) => {
            const payment = this.cardPointeService.getTThaiMerchant();
            const account = `${payload.routingNumber}/${payload.accountNumber}`;
            const { username, password } = payment;
            const auth = {
                username,
                password
            };
            const { data } = await this.cardPointeService.tokenizeCard(account, auth);
            if (!data.token)
                throw new common_1.BadRequestException(data);
            const { token } = data;
            payload.accountNumber = this.censorText(payload.accountNumber, 4);
            payload.routingNumber = this.censorText(payload.routingNumber, 3);
            payload.token = token;
            payload.restaurantId = restaurantId;
            const updated = await this.bankAccountService.transactionCreate(payload, session);
            if (!updated)
                throw new common_1.BadRequestException();
            await this.bankAccountService.transactionUpdateMany({ _id: { $ne: updated._id }, restaurantId }, { bankAccountStatus: ach_bank_type_enum_1.BankAccountStatusEnum.INACTIVE }, session);
            return updated;
        };
        const updated = await (0, mongoose_transaction_1.runTransaction)(logic);
        await this.subscriptionLogic.initDefaultPackage(restaurantId);
        return this.bankAccountService.findOne({ _id: updated._id }, { status: 0, updatedBy: 0, updatedAt: 0, createdBy: 0, createdAt: 0 });
    }
};
BankAccountLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [bank_account_service_1.BankAccountService,
        restaurant_service_1.RestaurantService,
        payment_gateway_http_service_1.PaymentGatewayHttpService,
        payment_gateway_logic_1.PaymentGatewayLogic,
        subscription_logic_1.SubscriptionLogic])
], BankAccountLogic);
exports.BankAccountLogic = BankAccountLogic;
//# sourceMappingURL=bank-account.logic.js.map