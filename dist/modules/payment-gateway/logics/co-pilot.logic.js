"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoPilotLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const co_pilot_service_1 = require("../services/co-pilot.service");
const payment_gateway_service_1 = require("../services/payment-gateway.service");
let CoPilotLogic = class CoPilotLogic {
    constructor(coPilotService, paymentGatewayService, restaurantService) {
        this.coPilotService = coPilotService;
        this.paymentGatewayService = paymentGatewayService;
        this.restaurantService = restaurantService;
    }
    async createMerchant(merchant, restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const payment = await this.paymentGatewayService.findOne({ restaurantId });
        if (payment.midCoPilot) {
            throw new common_1.BadRequestException('This merchant already submit.');
        }
        const payload = {
            merchant
        };
        const { data: token } = await this.coPilotService.tokenize();
        const accessToken = token['access_token'];
        const { data } = await this.coPilotService.createMerchantMinimum(payload, accessToken);
        if (!data.merchantId)
            throw new common_1.BadRequestException();
        const { data: signature } = await this.coPilotService.signatureMerchant(data.merchantId, accessToken);
        if (!signature.signatureUrl)
            throw new common_1.BadRequestException();
        await this.paymentGatewayService.findOneAndUpdate({ restaurantId }, { midCoPilot: data.merchantId });
        return { signatureUrl: signature.signatureUrl };
    }
    async getMerchantSignatureStatus(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const payment = await this.paymentGatewayService.findOne({ restaurantId });
        if (!payment.midCoPilot) {
            throw new common_1.BadRequestException('This merchant is not submit yet.');
        }
        const { data: token } = await this.coPilotService.tokenize();
        const accessToken = token['access_token'];
        const { data } = await this.coPilotService.getMerchantSignatureStatus(payment.midCoPilot, accessToken);
        if (!data)
            throw new common_1.BadRequestException();
        return data;
    }
};
CoPilotLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [co_pilot_service_1.CoPilotService,
        payment_gateway_service_1.PaymentGatewayService,
        restaurant_service_1.RestaurantService])
], CoPilotLogic);
exports.CoPilotLogic = CoPilotLogic;
//# sourceMappingURL=co-pilot.logic.js.map