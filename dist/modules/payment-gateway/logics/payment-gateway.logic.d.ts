import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdatePaymentDto } from '../dto/update-payment.dto';
import { PaymentGatewayDocument } from '../schemas/payment-gateway.schema';
import { PaymentGatewayHttpService } from '../services/payment-gateway-http.service';
import { PaymentGatewayService } from '../services/payment-gateway.service';
export interface IPaymentGateway {
    mid: string;
    username: string;
    password: string;
    isConvenienceFee: boolean;
    convenienceFee: number;
    isServiceCharge: boolean;
    serviceCharge: number;
    midCoPilot?: string;
    paymentStatus?: string;
}
export declare class PaymentGatewayLogic {
    private readonly paymentGatewayService;
    private readonly restaurantService;
    private readonly httpService;
    constructor(paymentGatewayService: PaymentGatewayService, restaurantService: RestaurantService, httpService: PaymentGatewayHttpService);
    private stringToBase64;
    private decodeBase64toString;
    getPaymentGateway(id: string): Promise<IPaymentGateway>;
    updatePaymentGateway(id: string, payload: UpdatePaymentDto): Promise<PaymentGatewayDocument>;
    getPaymentGatewayWithDecode(restaurantId: string): Promise<IPaymentGateway>;
}
