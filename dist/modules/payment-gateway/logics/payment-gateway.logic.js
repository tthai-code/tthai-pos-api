"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentGatewayLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const payment_gateway_enum_1 = require("../common/payment-gateway.enum");
const payment_gateway_http_service_1 = require("../services/payment-gateway-http.service");
const payment_gateway_service_1 = require("../services/payment-gateway.service");
let PaymentGatewayLogic = class PaymentGatewayLogic {
    constructor(paymentGatewayService, restaurantService, httpService) {
        this.paymentGatewayService = paymentGatewayService;
        this.restaurantService = restaurantService;
        this.httpService = httpService;
    }
    stringToBase64(text) {
        const encode = Buffer.from(text).toString('base64');
        return encode;
    }
    decodeBase64toString(text) {
        const decode = Buffer.from(text, 'base64').toString('utf-8');
        return decode;
    }
    async getPaymentGateway(id) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        let payment = await this.paymentGatewayService.findOne({
            restaurantId: id
        });
        if (!payment) {
            await this.paymentGatewayService.create({ restaurantId: id });
            payment = await this.paymentGatewayService.findOne({
                restaurantId: id
            });
        }
        return {
            mid: payment.mid,
            username: payment.username,
            password: payment.password,
            isConvenienceFee: payment.isConvenienceFee,
            convenienceFee: payment.convenienceFee,
            isServiceCharge: payment.isServiceCharge,
            serviceCharge: payment.serviceCharge,
            midCoPilot: payment.midCoPilot,
            paymentStatus: payment.paymentStatus
        };
    }
    async updatePaymentGateway(id, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const { data } = await this.httpService.validateMerchantId(payload.mid, {
            username: payload.username,
            password: payload.password
        });
        if (data.merchid)
            payload.paymentStatus = payment_gateway_enum_1.PaymentGatewayStatusEnum.ACTIVE;
        payload.password = this.stringToBase64(payload.password);
        payload.restaurantId = id;
        return this.paymentGatewayService.findOneAndUpdate({ restaurantId: id }, payload);
    }
    async getPaymentGatewayWithDecode(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: { $ne: status_enum_1.StatusEnum.DELETED }
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        let payment = await this.paymentGatewayService.findOne({
            restaurantId: restaurantId
        });
        if (!payment) {
            await this.paymentGatewayService.create({ restaurantId });
            payment = await this.paymentGatewayService.findOne({
                restaurantId: restaurantId
            });
        }
        return {
            mid: payment.mid,
            username: payment.username,
            password: payment.password
                ? this.decodeBase64toString(payment.password)
                : '',
            isConvenienceFee: payment.isConvenienceFee,
            convenienceFee: payment.convenienceFee,
            isServiceCharge: payment.isServiceCharge,
            serviceCharge: payment.serviceCharge
        };
    }
};
PaymentGatewayLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [payment_gateway_service_1.PaymentGatewayService,
        restaurant_service_1.RestaurantService,
        payment_gateway_http_service_1.PaymentGatewayHttpService])
], PaymentGatewayLogic);
exports.PaymentGatewayLogic = PaymentGatewayLogic;
//# sourceMappingURL=payment-gateway.logic.js.map