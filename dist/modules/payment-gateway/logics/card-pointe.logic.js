"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardPointeLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const card_type_enum_1 = require("../../transaction/common/card-type.enum");
const payment_method_enum_1 = require("../../transaction/common/payment-method.enum");
const payment_gateway_http_service_1 = require("../services/payment-gateway-http.service");
const payment_gateway_service_1 = require("../services/payment-gateway.service");
const payment_gateway_logic_1 = require("./payment-gateway.logic");
let CardPointeLogic = class CardPointeLogic {
    constructor(paymentService, paymentLogic, httpService, restaurantService) {
        this.paymentService = paymentService;
        this.paymentLogic = paymentLogic;
        this.httpService = httpService;
        this.restaurantService = restaurantService;
    }
    identifyPaymentMethod(product, paymentMethod) {
        let cardType;
        if (product === 'A') {
            cardType = card_type_enum_1.CardTypeEnum.AMEX;
        }
        else if (product === 'D') {
            cardType = card_type_enum_1.CardTypeEnum.DISCOVER;
        }
        else if (product === 'M') {
            cardType = card_type_enum_1.CardTypeEnum.MASTERCARD;
        }
        else if (product === 'V') {
            cardType = card_type_enum_1.CardTypeEnum.VISA;
        }
        else {
            cardType = card_type_enum_1.CardTypeEnum.NON_CO_BRAND;
        }
        let method;
        const regex = /credit/i.test(paymentMethod);
        if (regex) {
            method = payment_method_enum_1.PaymentMethodEnum.CREDIT;
        }
        else {
            method = payment_method_enum_1.PaymentMethodEnum.DEBIT;
        }
        return { cardType, paymentMethod: method };
    }
    async findOneRestaurantPayment(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        return this.paymentLogic.getPaymentGatewayWithDecode(restaurantId);
    }
    async tokenizeAndBinToken(payload) {
        const payment = await this.findOneRestaurantPayment(payload.restaurantId);
        const { username, password, mid } = payment;
        const auth = { username, password };
        const { data: token } = await this.httpService.tokenizeCard(payload.account, auth);
        const { data: binToken } = await this.httpService.binToken(mid, token.token, auth);
        return {
            ending: payload.account.slice(-4),
            token: token.token,
            binToken
        };
    }
    async authorizeNoCapture(payload) {
        return payload;
    }
    async authorizeWithCapture(payload) {
        const payment = await this.findOneRestaurantPayment(payload.restaurantId);
        const tokenized = await this.tokenizeAndBinToken({
            restaurantId: payload.restaurantId,
            account: payload.account
        });
        delete payload.restaurantId;
        payload.merchid = payment.mid;
        payload.account = tokenized.token;
        payload.ecomind = 'E';
        payload.capture = 'Y';
        const auth = { username: payment.username, password: payment.password };
        return this.httpService.authorization(payload, auth);
    }
    async voidTransaction(retRef, restaurantId) {
        const payment = await this.findOneRestaurantPayment(restaurantId);
        const { username, password, mid } = payment;
        return this.httpService.voidRetRef({
            merchid: mid,
            retref: retRef
        }, { username, password });
    }
    async closeBatch(restaurantId, batchId) {
        const payment = await this.findOneRestaurantPayment(restaurantId);
        const { username, password, mid } = payment;
        return this.httpService.closeBatch(mid, { username, password }, batchId);
    }
    async refundTransaction(retRef, restaurantId, amount) {
        const payment = await this.findOneRestaurantPayment(restaurantId);
        const { username, password, mid } = payment;
        const payload = { merchid: mid, retref: retRef, amount: amount.toFixed(2) };
        if (!amount)
            delete payload.amount;
        return this.httpService.refundRetRef(payload, { username, password });
    }
    async captureTransaction(retRef, restaurantId, amount) {
        const payment = await this.findOneRestaurantPayment(restaurantId);
        const { username, password, mid } = payment;
        const payload = {
            retref: retRef,
            merchid: mid,
            amount
        };
        return this.httpService.capture(payload, { username, password });
    }
    async payOnlineTransaction(restaurantId, payload) {
        const payment = await this.findOneRestaurantPayment(restaurantId);
        const { username, password, mid } = payment;
        payload.merchid = mid;
        payload.ecomind = 'E';
        payload.capture = 'Y';
        payload.bin = 'Y';
        const auth = { username, password };
        const { data } = await this.httpService.authorization(payload, auth);
        if (!['00', '000'].includes(data.respcode)) {
            throw new common_1.BadRequestException(data.resptext);
        }
        return data;
    }
};
CardPointeLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [payment_gateway_service_1.PaymentGatewayService,
        payment_gateway_logic_1.PaymentGatewayLogic,
        payment_gateway_http_service_1.PaymentGatewayHttpService,
        restaurant_service_1.RestaurantService])
], CardPointeLogic);
exports.CardPointeLogic = CardPointeLogic;
//# sourceMappingURL=card-pointe.logic.js.map