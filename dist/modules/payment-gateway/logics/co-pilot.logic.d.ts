import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CreateMerchantDto } from '../dto/create-merchant.dto';
import { CoPilotService } from '../services/co-pilot.service';
import { PaymentGatewayService } from '../services/payment-gateway.service';
export declare class CoPilotLogic {
    private readonly coPilotService;
    private readonly paymentGatewayService;
    private readonly restaurantService;
    constructor(coPilotService: CoPilotService, paymentGatewayService: PaymentGatewayService, restaurantService: RestaurantService);
    createMerchant(merchant: CreateMerchantDto, restaurantId: string): Promise<{
        signatureUrl: any;
    }>;
    getMerchantSignatureStatus(restaurantId: string): Promise<any>;
}
