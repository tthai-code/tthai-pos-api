"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateCustomerOnlineDto = exports.CreateCustomerDto = exports.AddressFieldDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const customer_type_enum_1 = require("../common/enum/customer-type.enum");
const class_transformer_1 = require("class-transformer");
class AddressFieldDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Some Address' }),
    __metadata("design:type", String)
], AddressFieldDto.prototype, "address", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Addison' }),
    __metadata("design:type", String)
], AddressFieldDto.prototype, "city", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Dallas' }),
    __metadata("design:type", String)
], AddressFieldDto.prototype, "state", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '75001' }),
    __metadata("design:type", String)
], AddressFieldDto.prototype, "zipCode", void 0);
exports.AddressFieldDto = AddressFieldDto;
class CreateCustomerDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Tim' }),
    __metadata("design:type", String)
], CreateCustomerDto.prototype, "firstName", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Henson' }),
    __metadata("design:type", String)
], CreateCustomerDto.prototype, "lastName", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '5102638229' }),
    __metadata("design:type", String)
], CreateCustomerDto.prototype, "tel", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_transformer_1.Type)(() => AddressFieldDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiPropertyOptional)({ type: () => AddressFieldDto }),
    __metadata("design:type", AddressFieldDto)
], CreateCustomerDto.prototype, "address", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'admin@gmail.com' }),
    __metadata("design:type", String)
], CreateCustomerDto.prototype, "email", void 0);
exports.CreateCustomerDto = CreateCustomerDto;
class CreateCustomerOnlineDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '630e55a0d9c30fd7cdcb424b' }),
    __metadata("design:type", String)
], CreateCustomerOnlineDto.prototype, "restaurantId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Tim' }),
    __metadata("design:type", String)
], CreateCustomerOnlineDto.prototype, "firstName", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'admin@gmail.com' }),
    __metadata("design:type", String)
], CreateCustomerOnlineDto.prototype, "email", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'admin1234' }),
    __metadata("design:type", String)
], CreateCustomerOnlineDto.prototype, "password", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Henson' }),
    __metadata("design:type", String)
], CreateCustomerOnlineDto.prototype, "lastName", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '5102638229' }),
    __metadata("design:type", String)
], CreateCustomerOnlineDto.prototype, "tel", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({
        enum: Object.values(customer_type_enum_1.CustomerTypeEnum)
    }),
    __metadata("design:type", String)
], CreateCustomerOnlineDto.prototype, "loginType", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '<uid-social>' }),
    __metadata("design:type", String)
], CreateCustomerOnlineDto.prototype, "uidSocial", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => AddressFieldDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => AddressFieldDto }),
    __metadata("design:type", AddressFieldDto)
], CreateCustomerOnlineDto.prototype, "address", void 0);
exports.CreateCustomerOnlineDto = CreateCustomerOnlineDto;
//# sourceMappingURL=create-customer.dto.js.map