"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSCustomerPaginateDto = void 0;
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
class POSCustomerPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'createdAt';
        this.sortOrder = 'desc';
        this.search = '';
    }
    buildQuery(id) {
        const result = {
            $or: [
                {
                    firstName: {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    lastName: {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    tel: {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            restaurantId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], POSCustomerPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], POSCustomerPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: 'Tim',
        description: 'Search by firstName, lastName or tel'
    }),
    __metadata("design:type", String)
], POSCustomerPaginateDto.prototype, "search", void 0);
exports.POSCustomerPaginateDto = POSCustomerPaginateDto;
//# sourceMappingURL=paginate-customer.dto.js.map