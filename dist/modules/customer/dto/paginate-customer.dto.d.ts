import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class POSCustomerPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    buildQuery(id: string): {
        $or: ({
            firstName: {
                $regex: string;
                $options: string;
            };
            lastName?: undefined;
            tel?: undefined;
        } | {
            lastName: {
                $regex: string;
                $options: string;
            };
            firstName?: undefined;
            tel?: undefined;
        } | {
            tel: {
                $regex: string;
                $options: string;
            };
            firstName?: undefined;
            lastName?: undefined;
        })[];
        restaurantId: string;
        status: StatusEnum;
    };
}
