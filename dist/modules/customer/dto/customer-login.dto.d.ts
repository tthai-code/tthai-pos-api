export declare class CustomerLoginDto {
    readonly restaurantId: string;
    readonly email: string;
    readonly password: string;
}
export declare class CustomerSocialLoginDto {
    readonly restaurantId: string;
    readonly uidSocial: string;
    readonly loginType: string;
}
