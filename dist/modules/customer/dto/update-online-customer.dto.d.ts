declare class AddressFieldDto {
    readonly address: string;
    readonly city: string;
    readonly state: string;
    readonly zipCode: string;
}
export declare class UpdateCustomerOnlineDto {
    readonly firstName: string;
    readonly lastName: string;
    readonly tel: string;
    readonly email: string;
    readonly address: AddressFieldDto;
}
export {};
