export declare class UpdateCustomerDto {
    firstName: string;
    lastName: string;
    tel: string;
}
