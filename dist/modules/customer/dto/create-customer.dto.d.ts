export declare class AddressFieldDto {
    readonly address: string;
    readonly city: string;
    readonly state: string;
    readonly zipCode: string;
}
export declare class CreateCustomerDto {
    loginType: string;
    firstName: string;
    lastName: string;
    tel: string;
    readonly address: AddressFieldDto;
    email: string;
}
export declare class CreateCustomerOnlineDto {
    restaurantId: string;
    firstName: string;
    email: string;
    password: string;
    lastName: string;
    tel: string;
    loginType: string;
    uidSocial: string;
    readonly address: AddressFieldDto;
}
