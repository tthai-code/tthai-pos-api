export declare class CustomerGuestOnlineDto {
    loginType: string;
    readonly restaurantId: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly tel: string;
    readonly email: string;
}
