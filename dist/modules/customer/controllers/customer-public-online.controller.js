"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerOnlineController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const errorResponse_1 = require("../../../utilities/errorResponse");
const paginate_customer_dto_1 = require("../dto/paginate-customer.dto");
const customer_entity_1 = require("../entity/customer.entity");
const customer_logic_1 = require("../logics/customer.logic");
const customer_service_1 = require("../services/customer.service");
const auth_logic_1 = require("../../auth/logics/auth.logic");
const customer_login_dto_1 = require("../dto/customer-login.dto");
const create_customer_dto_1 = require("../dto/create-customer.dto");
const customer_auth_guard_1 = require("../../auth/guards/customer-auth.guard");
const update_online_customer_dto_1 = require("../dto/update-online-customer.dto");
const success_entity_1 = require("../../../common/entity/success.entity");
const continue_guest_dto_1 = require("../dto/continue-guest.dto");
const customer_online_logic_1 = require("../logics/customer-online.logic");
const customer_online_entity_1 = require("../entity/customer-online.entity");
let CustomerOnlineController = class CustomerOnlineController {
    constructor(customerService, customerLogic, authLogic, customerOnlineLogic) {
        this.customerService = customerService;
        this.customerLogic = customerLogic;
        this.authLogic = authLogic;
        this.customerOnlineLogic = customerOnlineLogic;
    }
    async customerLogin(payload) {
        return this.authLogic.CustomerLogin(payload);
    }
    async customerSocialLogin(payload) {
        return this.authLogic.CustomerSocialLogin(payload);
    }
    async createCustomer(payload) {
        return await this.customerLogic.createCustomerOnline(payload);
    }
    async getCustomers(query) {
        return await this.customerService.paginate({}, query, {
            password: 0
        });
    }
    async getCustomer(id) {
        return await this.customerLogic.getCustomer(id);
    }
    async updateCustomerInfo(payload) {
        var _a;
        const customerId = (_a = customer_auth_guard_1.CustomerAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.id;
        return await this.customerLogic.updateCustomerOnline(customerId, payload);
    }
    async continueAsGuest(payload) {
        return await this.customerOnlineLogic.continueWithGuest(payload);
    }
};
__decorate([
    (0, common_1.Post)('login'),
    (0, swagger_1.ApiOperation)({
        summary: 'Normal Login'
    }),
    (0, swagger_1.ApiOkResponse)({ type: () => customer_entity_1.CustomerResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Not found staff.', '/v1/staff/login')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [customer_login_dto_1.CustomerLoginDto]),
    __metadata("design:returntype", Promise)
], CustomerOnlineController.prototype, "customerLogin", null);
__decorate([
    (0, common_1.Post)('socialLogin'),
    (0, swagger_1.ApiOperation)({
        summary: 'Social Login'
    }),
    (0, swagger_1.ApiOkResponse)({ type: () => customer_entity_1.CustomerResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Not found staff.', '/v1/staff/login')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [customer_login_dto_1.CustomerSocialLoginDto]),
    __metadata("design:returntype", Promise)
], CustomerOnlineController.prototype, "customerSocialLogin", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => customer_entity_1.CreatedCustomerResponse }),
    (0, swagger_1.ApiBadRequestResponse)((0, errorResponse_1.errorResponse)(400, 'This phone number is already exist.', 'v1/public/online/customer')),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', 'v1/public/online/customer')),
    (0, swagger_1.ApiOperation)({
        summary: 'Create a new customer',
        description: ''
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_customer_dto_1.CreateCustomerOnlineDto]),
    __metadata("design:returntype", Promise)
], CustomerOnlineController.prototype, "createCustomer", null);
__decorate([
    (0, common_1.Get)('all'),
    (0, swagger_1.ApiOkResponse)({ type: () => customer_entity_1.PaginateCustomerResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', 'v1/pos/customer')),
    (0, swagger_1.ApiOperation)({
        summary: 'Get paginate customer list',
        description: ''
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [paginate_customer_dto_1.POSCustomerPaginateDto]),
    __metadata("design:returntype", Promise)
], CustomerOnlineController.prototype, "getCustomers", null);
__decorate([
    (0, common_1.Get)(':customerId'),
    (0, swagger_1.ApiOkResponse)({ type: () => customer_entity_1.CustomerResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', '/v1/pos/customer/:customerId')),
    (0, swagger_1.ApiNotFoundResponse)((0, errorResponse_1.errorResponse)(404, 'Not found customer.', '/v1/pos/customer/:customerId')),
    (0, swagger_1.ApiOperation)({
        summary: 'Get a customer by id',
        description: ''
    }),
    __param(0, (0, common_1.Param)('customerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CustomerOnlineController.prototype, "getCustomer", null);
__decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(customer_auth_guard_1.CustomerAuthGuard),
    (0, common_1.Put)('info'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Update Customer Information',
        description: 'use bearer `customer token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_online_customer_dto_1.UpdateCustomerOnlineDto]),
    __metadata("design:returntype", Promise)
], CustomerOnlineController.prototype, "updateCustomerInfo", null);
__decorate([
    (0, common_1.Post)('guest'),
    (0, swagger_1.ApiCreatedResponse)({ type: () => customer_online_entity_1.CustomerGuestResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Continue With Guest',
        description: 'use to create customer and get `customer_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [continue_guest_dto_1.CustomerGuestOnlineDto]),
    __metadata("design:returntype", Promise)
], CustomerOnlineController.prototype, "continueAsGuest", null);
CustomerOnlineController = __decorate([
    (0, swagger_1.ApiTags)('customer-online'),
    (0, common_1.Controller)('v1/public/online/customer'),
    __metadata("design:paramtypes", [customer_service_1.CustomerService,
        customer_logic_1.CustomerLogic,
        auth_logic_1.AuthLogic,
        customer_online_logic_1.CustomerOnlineLogic])
], CustomerOnlineController);
exports.CustomerOnlineController = CustomerOnlineController;
//# sourceMappingURL=customer-public-online.controller.js.map