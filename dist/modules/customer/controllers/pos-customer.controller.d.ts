import { CreateCustomerDto } from '../dto/create-customer.dto';
import { POSCustomerPaginateDto } from '../dto/paginate-customer.dto';
import { UpdateCustomerDto } from '../dto/update-customer.dto';
import { CustomerLogic } from '../logics/customer.logic';
import { CustomerService } from '../services/customer.service';
export declare class POSCustomerController {
    private readonly customerService;
    private readonly customerLogic;
    constructor(customerService: CustomerService, customerLogic: CustomerLogic);
    createCustomer(payload: CreateCustomerDto): Promise<import("../schemas/customer.schema").CustomerDocument>;
    getCustomers(query: POSCustomerPaginateDto): Promise<PaginateResult<import("../schemas/customer.schema").CustomerDocument>>;
    getCustomer(id: string): Promise<import("../schemas/customer.schema").CustomerDocument>;
    updateCustomer(id: string, payload: UpdateCustomerDto): Promise<import("../schemas/customer.schema").CustomerDocument>;
    deleteCustomer(id: string): Promise<import("../schemas/customer.schema").CustomerDocument>;
}
