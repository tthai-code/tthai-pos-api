"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSCustomerController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const errorResponse_1 = require("../../../utilities/errorResponse");
const create_customer_dto_1 = require("../dto/create-customer.dto");
const paginate_customer_dto_1 = require("../dto/paginate-customer.dto");
const update_customer_dto_1 = require("../dto/update-customer.dto");
const customer_entity_1 = require("../entity/customer.entity");
const customer_logic_1 = require("../logics/customer.logic");
const customer_service_1 = require("../services/customer.service");
let POSCustomerController = class POSCustomerController {
    constructor(customerService, customerLogic) {
        this.customerService = customerService;
        this.customerLogic = customerLogic;
    }
    async createCustomer(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.customerLogic.createCustomer(restaurantId, payload);
    }
    async getCustomers(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.customerService.paginate(query.buildQuery(restaurantId), query, {
            firstName: 1,
            lastName: 1,
            tel: 1
        });
    }
    async getCustomer(id) {
        return await this.customerLogic.getCustomer(id);
    }
    async updateCustomer(id, payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.customerLogic.updateCustomer(id, restaurantId, payload);
    }
    async deleteCustomer(id) {
        const customer = await this.customerService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!customer)
            throw new common_1.NotFoundException('Not found customer.');
        return await this.customerService.delete(id);
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => customer_entity_1.CreatedCustomerResponse }),
    (0, swagger_1.ApiBadRequestResponse)((0, errorResponse_1.errorResponse)(400, 'This phone number is already exist.', '/v1/pos/customer')),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', '/v1/pos/customer')),
    (0, swagger_1.ApiOperation)({
        summary: 'Create a new customer',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_customer_dto_1.CreateCustomerDto]),
    __metadata("design:returntype", Promise)
], POSCustomerController.prototype, "createCustomer", null);
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => customer_entity_1.PaginateCustomerResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', 'v1/pos/customer')),
    (0, swagger_1.ApiOperation)({
        summary: 'Get paginate customer list',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [paginate_customer_dto_1.POSCustomerPaginateDto]),
    __metadata("design:returntype", Promise)
], POSCustomerController.prototype, "getCustomers", null);
__decorate([
    (0, common_1.Get)(':customerId'),
    (0, swagger_1.ApiOkResponse)({ type: () => customer_entity_1.CustomerResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', '/v1/pos/customer/:customerId')),
    (0, swagger_1.ApiNotFoundResponse)((0, errorResponse_1.errorResponse)(404, 'Not found customer.', '/v1/pos/customer/:customerId')),
    (0, swagger_1.ApiOperation)({
        summary: 'Get a customer by id',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Param)('customerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSCustomerController.prototype, "getCustomer", null);
__decorate([
    (0, common_1.Put)(':customerId'),
    (0, swagger_1.ApiOkResponse)({ type: () => customer_entity_1.CreatedCustomerResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', '/v1/pos/customer/:customerId')),
    (0, swagger_1.ApiNotFoundResponse)((0, errorResponse_1.errorResponse)(404, 'Not found customer.', '/v1/pos/customer/:customerId')),
    (0, swagger_1.ApiOperation)({
        summary: 'Update a customer by id',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Param)('customerId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_customer_dto_1.UpdateCustomerDto]),
    __metadata("design:returntype", Promise)
], POSCustomerController.prototype, "updateCustomer", null);
__decorate([
    (0, common_1.Delete)(':customerId'),
    (0, swagger_1.ApiOkResponse)({ type: () => customer_entity_1.DeletedCustomerResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', '/v1/pos/customer/:customerId')),
    (0, swagger_1.ApiNotFoundResponse)((0, errorResponse_1.errorResponse)(404, 'Not found customer.', '/v1/pos/customer/:customerId')),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete a customer by id',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Param)('customerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSCustomerController.prototype, "deleteCustomer", null);
POSCustomerController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/customer'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/customer'),
    __metadata("design:paramtypes", [customer_service_1.CustomerService,
        customer_logic_1.CustomerLogic])
], POSCustomerController);
exports.POSCustomerController = POSCustomerController;
//# sourceMappingURL=pos-customer.controller.js.map