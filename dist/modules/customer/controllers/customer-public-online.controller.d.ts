/// <reference types="mongoose" />
import { POSCustomerPaginateDto } from '../dto/paginate-customer.dto';
import { CustomerLogic } from '../logics/customer.logic';
import { CustomerService } from '../services/customer.service';
import { AuthLogic } from 'src/modules/auth/logics/auth.logic';
import { CustomerLoginDto, CustomerSocialLoginDto } from '../dto/customer-login.dto';
import { CreateCustomerOnlineDto } from '../dto/create-customer.dto';
import { UpdateCustomerOnlineDto } from '../dto/update-online-customer.dto';
import { CustomerGuestOnlineDto } from '../dto/continue-guest.dto';
import { CustomerOnlineLogic } from '../logics/customer-online.logic';
export declare class CustomerOnlineController {
    private readonly customerService;
    private readonly customerLogic;
    private readonly authLogic;
    private readonly customerOnlineLogic;
    constructor(customerService: CustomerService, customerLogic: CustomerLogic, authLogic: AuthLogic, customerOnlineLogic: CustomerOnlineLogic);
    customerLogin(payload: CustomerLoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        address: import("../schemas/customer.schema").AddressFields;
        uidSocial: string;
        loginType: string;
        tel: string;
        email: string;
        lastName: string;
        firstName: string;
        restaurantId: import("mongoose").Types.ObjectId;
        id: any;
    }>;
    customerSocialLogin(payload: CustomerSocialLoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        address: import("../schemas/customer.schema").AddressFields;
        uidSocial: string;
        loginType: string;
        tel: string;
        email: string;
        lastName: string;
        firstName: string;
        restaurantId: import("mongoose").Types.ObjectId;
        id: any;
    }>;
    createCustomer(payload: CreateCustomerOnlineDto): Promise<import("../schemas/customer.schema").CustomerDocument>;
    getCustomers(query: POSCustomerPaginateDto): Promise<PaginateResult<import("../schemas/customer.schema").CustomerDocument>>;
    getCustomer(id: string): Promise<import("../schemas/customer.schema").CustomerDocument>;
    updateCustomerInfo(payload: UpdateCustomerOnlineDto): Promise<{
        success: boolean;
    }>;
    continueAsGuest(payload: CustomerGuestOnlineDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        address: import("../schemas/customer.schema").AddressFields;
        uidSocial: string;
        loginType: string;
        tel: string;
        email: string;
        lastName: string;
        firstName: string;
        restaurantId: import("mongoose").Types.ObjectId;
        id: any;
    }>;
}
