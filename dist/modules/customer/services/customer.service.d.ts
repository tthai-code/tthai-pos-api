import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { Model } from 'mongoose';
import { CustomerDocument } from '../schemas/customer.schema';
import { POSCustomerPaginateDto } from '../dto/paginate-customer.dto';
export declare class CustomerService {
    private readonly CustomerModel;
    private request;
    constructor(CustomerModel: PaginateModel<CustomerDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<CustomerDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<CustomerDocument>;
    create(payload: any): Promise<CustomerDocument>;
    update(id: string, User: any): Promise<CustomerDocument>;
    delete(id: string): Promise<CustomerDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any, options?: any): Promise<CustomerDocument>;
    paginate(query: any, queryParam: POSCustomerPaginateDto, select?: any): Promise<PaginateResult<CustomerDocument>>;
}
