"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const pos_customer_controller_1 = require("./controllers/pos-customer.controller");
const customer_public_online_controller_1 = require("./controllers/customer-public-online.controller");
const customer_logic_1 = require("./logics/customer.logic");
const customer_schema_1 = require("./schemas/customer.schema");
const customer_service_1 = require("./services/customer.service");
const customer_online_logic_1 = require("./logics/customer-online.logic");
let CustomerModule = class CustomerModule {
};
CustomerModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([{ name: 'customer', schema: customer_schema_1.CustomerSchema }])
        ],
        providers: [customer_service_1.CustomerService, customer_logic_1.CustomerLogic, customer_online_logic_1.CustomerOnlineLogic],
        controllers: [pos_customer_controller_1.POSCustomerController, customer_public_online_controller_1.CustomerOnlineController],
        exports: [customer_service_1.CustomerService]
    })
], CustomerModule);
exports.CustomerModule = CustomerModule;
//# sourceMappingURL=customer.module.js.map