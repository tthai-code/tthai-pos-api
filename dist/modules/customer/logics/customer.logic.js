"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerLogic = void 0;
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const status_enum_1 = require("../../../common/enum/status.enum");
const customer_type_enum_1 = require("../common/enum/customer-type.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const customer_service_1 = require("../services/customer.service");
const bcrypt = require("bcryptjs");
let CustomerLogic = class CustomerLogic {
    constructor(customerService, restaurantService) {
        this.customerService = customerService;
        this.restaurantService = restaurantService;
    }
    async getCustomer(customerId) {
        const customer = await this.customerService.findOne({
            _id: customerId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { password: 0, createdAt: 0, updatedAt: 0, updatedBy: 0, createdBy: 0 });
        if (!customer)
            throw new common_1.NotFoundException('Not found customer.');
        return customer;
    }
    async createCustomer(restaurantId, createCustomer) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        createCustomer.loginType = customer_type_enum_1.CustomerTypeEnum.GUEST;
        const payload = (0, class_transformer_1.classToPlain)(createCustomer);
        return this.customerService.create(Object.assign(Object.assign({}, payload), { restaurantId }));
    }
    async createCustomerOnline(createCustomer) {
        const restaurantId = createCustomer.restaurantId;
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        if (createCustomer.loginType !== customer_type_enum_1.CustomerTypeEnum.GUEST) {
            const isExist = await this.customerService.findOne({
                restaurantId: restaurantId,
                $or: [
                    {
                        tel: createCustomer.tel
                    },
                    {
                        email: createCustomer.email
                    }
                ],
                status: status_enum_1.StatusEnum.ACTIVE,
                loginType: { $ne: customer_type_enum_1.CustomerTypeEnum.GUEST }
            });
            if (isExist) {
                throw new common_1.BadRequestException('This phone number or email is already exist.');
            }
        }
        const salt = bcrypt.genSaltSync(10);
        const payload = (0, class_transformer_1.classToPlain)(createCustomer);
        if (payload.password !== '') {
            payload.password = bcrypt.hashSync(payload.password, salt);
        }
        return this.customerService.create(Object.assign(Object.assign({}, payload), { restaurantId }));
    }
    async updateCustomer(customerId, restaurantId, updateCustomer) {
        const customer = await this.customerService.findOne({
            _id: customerId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!customer)
            throw new common_1.NotFoundException('Not found customer.');
        const isExist = await this.customerService.findOne({
            _id: { $ne: customerId },
            restaurantId,
            tel: updateCustomer.tel,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (isExist) {
            throw new common_1.BadRequestException('This phone number is already exist.');
        }
        return this.customerService.update(customerId, updateCustomer);
    }
    async updateCustomerOnline(customerId, payload) {
        const customer = await this.customerService.findOne({
            _id: customerId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!customer)
            throw new common_1.NotFoundException('Not found customer.');
        const isExist = await this.customerService.findOne({
            _id: { $ne: customerId },
            restaurantId: customer.restaurantId,
            $or: [
                {
                    tel: payload.tel
                },
                {
                    email: payload.email
                }
            ],
            loginType: { $ne: customer_type_enum_1.CustomerTypeEnum.GUEST },
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (isExist) {
            throw new common_1.BadRequestException('This phone number or email is already exist.');
        }
        await this.customerService.update(customerId, payload);
        return { success: true };
    }
};
CustomerLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [customer_service_1.CustomerService,
        restaurant_service_1.RestaurantService])
], CustomerLogic);
exports.CustomerLogic = CustomerLogic;
//# sourceMappingURL=customer.logic.js.map