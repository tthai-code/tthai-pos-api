import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CreateCustomerDto, CreateCustomerOnlineDto } from '../dto/create-customer.dto';
import { UpdateCustomerDto } from '../dto/update-customer.dto';
import { CustomerService } from '../services/customer.service';
import { UpdateCustomerOnlineDto } from '../dto/update-online-customer.dto';
export declare class CustomerLogic {
    private customerService;
    private restaurantService;
    constructor(customerService: CustomerService, restaurantService: RestaurantService);
    getCustomer(customerId: string): Promise<import("../schemas/customer.schema").CustomerDocument>;
    createCustomer(restaurantId: string, createCustomer: CreateCustomerDto): Promise<import("../schemas/customer.schema").CustomerDocument>;
    createCustomerOnline(createCustomer: CreateCustomerOnlineDto): Promise<import("../schemas/customer.schema").CustomerDocument>;
    updateCustomer(customerId: string, restaurantId: string, updateCustomer: UpdateCustomerDto): Promise<import("../schemas/customer.schema").CustomerDocument>;
    updateCustomerOnline(customerId: string, payload: UpdateCustomerOnlineDto): Promise<{
        success: boolean;
    }>;
}
