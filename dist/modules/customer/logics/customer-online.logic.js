"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerOnlineLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const auth_1 = require("../../auth/constants/auth");
const auth_logic_1 = require("../../auth/logics/auth.logic");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const customer_type_enum_1 = require("../common/enum/customer-type.enum");
const customer_service_1 = require("../services/customer.service");
let CustomerOnlineLogic = class CustomerOnlineLogic {
    constructor(customerService, restaurantService, authLogic) {
        this.customerService = customerService;
        this.restaurantService = restaurantService;
        this.authLogic = authLogic;
    }
    async continueWithGuest(payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: payload.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        payload.loginType = customer_type_enum_1.CustomerTypeEnum.GUEST;
        const customer = await this.customerService.create(payload);
        const transform = {
            address: customer === null || customer === void 0 ? void 0 : customer.address,
            uidSocial: customer === null || customer === void 0 ? void 0 : customer.uidSocial,
            loginType: customer === null || customer === void 0 ? void 0 : customer.loginType,
            tel: customer === null || customer === void 0 ? void 0 : customer.tel,
            email: customer === null || customer === void 0 ? void 0 : customer.email,
            lastName: customer === null || customer === void 0 ? void 0 : customer.lastName,
            firstName: customer === null || customer === void 0 ? void 0 : customer.firstName,
            restaurantId: customer === null || customer === void 0 ? void 0 : customer.restaurantId,
            id: customer === null || customer === void 0 ? void 0 : customer.id
        };
        const accessToken = this.authLogic.jwtSignToken(transform, '7d', auth_1.jwtConstants.secretCustomer);
        const now = new Date();
        return Object.assign(Object.assign({}, transform), { accessToken, tokenExpire: now.setDate(now.getDate() + 7) });
    }
};
CustomerOnlineLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [customer_service_1.CustomerService,
        restaurant_service_1.RestaurantService,
        auth_logic_1.AuthLogic])
], CustomerOnlineLogic);
exports.CustomerOnlineLogic = CustomerOnlineLogic;
//# sourceMappingURL=customer-online.logic.js.map