/// <reference types="mongoose" />
import { AuthLogic } from 'src/modules/auth/logics/auth.logic';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CustomerGuestOnlineDto } from '../dto/continue-guest.dto';
import { CustomerService } from '../services/customer.service';
export declare class CustomerOnlineLogic {
    private readonly customerService;
    private readonly restaurantService;
    private readonly authLogic;
    constructor(customerService: CustomerService, restaurantService: RestaurantService, authLogic: AuthLogic);
    continueWithGuest(payload: CustomerGuestOnlineDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        address: import("../schemas/customer.schema").AddressFields;
        uidSocial: string;
        loginType: string;
        tel: string;
        email: string;
        lastName: string;
        firstName: string;
        restaurantId: import("mongoose").Types.ObjectId;
        id: any;
    }>;
}
