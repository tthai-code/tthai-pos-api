import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type CustomerDocument = CustomerFields & Document;
export declare class AddressFields {
    address: string;
    city: string;
    state: string;
    zipCode: string;
}
export declare class CustomerFields {
    restaurantId: Types.ObjectId;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    tel: string;
    status: string;
    loginType: string;
    uidSocial: string;
    address: AddressFields;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const CustomerSchema: MongooseSchema<Document<CustomerFields, any, any>, import("mongoose").Model<Document<CustomerFields, any, any>, any, any, any>, any, any>;
