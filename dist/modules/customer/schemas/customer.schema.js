"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerSchema = exports.CustomerFields = exports.AddressFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const customer_type_enum_1 = require("../common/enum/customer-type.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
let AddressFields = class AddressFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], AddressFields.prototype, "address", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], AddressFields.prototype, "city", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], AddressFields.prototype, "state", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], AddressFields.prototype, "zipCode", void 0);
AddressFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], AddressFields);
exports.AddressFields = AddressFields;
let CustomerFields = class CustomerFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], CustomerFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], CustomerFields.prototype, "firstName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "lastName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "email", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "password", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "tel", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], CustomerFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: customer_type_enum_1.CustomerTypeEnum.NORMAL,
        enum: Object.values(customer_type_enum_1.CustomerTypeEnum)
    }),
    __metadata("design:type", String)
], CustomerFields.prototype, "loginType", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CustomerFields.prototype, "uidSocial", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: new AddressFields()
    }),
    __metadata("design:type", AddressFields)
], CustomerFields.prototype, "address", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], CustomerFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], CustomerFields.prototype, "createdBy", void 0);
CustomerFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'customers' })
], CustomerFields);
exports.CustomerFields = CustomerFields;
exports.CustomerSchema = mongoose_1.SchemaFactory.createForClass(CustomerFields);
exports.CustomerSchema.plugin(mongoosePaginate);
exports.CustomerSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=customer.schema.js.map