"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerTypeEnum = void 0;
var CustomerTypeEnum;
(function (CustomerTypeEnum) {
    CustomerTypeEnum["NORMAL"] = "NORMAL";
    CustomerTypeEnum["GUEST"] = "GUEST";
    CustomerTypeEnum["GOOGLE"] = "GOOGLE";
    CustomerTypeEnum["FACEBOOK"] = "FACEBOOK";
    CustomerTypeEnum["APPLE"] = "APPLE";
})(CustomerTypeEnum = exports.CustomerTypeEnum || (exports.CustomerTypeEnum = {}));
//# sourceMappingURL=customer-type.enum.js.map