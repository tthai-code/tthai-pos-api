import { ResponseDto } from 'src/common/entity/response.entity';
declare class AddressObject {
    zip_code: string;
    state: string;
    city: string;
    address: string;
}
declare class CustomerGuestObject {
    address: AddressObject;
    uid_social: string;
    login_type: string;
    tel: string;
    email: string;
    last_name: string;
    first_name: string;
    restaurant_id: string;
    id: string;
    token_expire: number;
}
export declare class CustomerGuestResponse extends ResponseDto<CustomerGuestObject> {
    data: CustomerGuestObject;
    access_token: string;
}
export {};
