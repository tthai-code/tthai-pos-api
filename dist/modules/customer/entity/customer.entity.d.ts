import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
declare class CreatedCustomerFieldsWithTimestampResponse extends TimestampResponseDto {
    id: string;
    restaurant_id: string;
    first_name: string;
    last_name: string;
    tel: string;
    status: string;
}
declare class CustomerFieldResponse {
    id: string;
    first_name: string;
    last_name: string;
    tel: string;
}
declare class PaginateCustomerFields extends PaginateResponseDto<Array<CustomerFieldResponse>> {
    results: Array<CustomerFieldResponse>;
}
export declare class PaginateCustomerResponse extends ResponseDto<PaginateCustomerFields> {
    data: PaginateCustomerFields;
}
export declare class CreatedCustomerResponse extends ResponseDto<CreatedCustomerFieldsWithTimestampResponse> {
    data: CreatedCustomerFieldsWithTimestampResponse;
}
export declare class CustomerResponse extends ResponseDto<CustomerFieldResponse> {
    data: CustomerFieldResponse;
}
export declare class DeletedCustomerResponse extends ResponseDto<CreatedCustomerFieldsWithTimestampResponse> {
    data: CreatedCustomerFieldsWithTimestampResponse;
}
export {};
