"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerGuestResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const customer_type_enum_1 = require("../common/enum/customer-type.enum");
class AddressObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "zip_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "address", void 0);
class CustomerGuestObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", AddressObject)
], CustomerGuestObject.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerGuestObject.prototype, "uid_social", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(customer_type_enum_1.CustomerTypeEnum) }),
    __metadata("design:type", String)
], CustomerGuestObject.prototype, "login_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerGuestObject.prototype, "tel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerGuestObject.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerGuestObject.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerGuestObject.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerGuestObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerGuestObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CustomerGuestObject.prototype, "token_expire", void 0);
class CustomerGuestResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CustomerGuestObject,
        example: {
            address: {
                zip_code: '',
                state: '',
                city: '',
                address: ''
            },
            uid_social: '',
            login_type: 'GUEST',
            tel: '7523123453',
            email: '',
            last_name: 'Henson',
            first_name: 'Tim',
            restaurant_id: '63984d6690416c27a7415915',
            id: '63bfd33151f3530d7650ea1d',
            token_expire: 1674120625297
        }
    }),
    __metadata("design:type", CustomerGuestObject)
], CustomerGuestResponse.prototype, "data", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: '<customer_token>' }),
    __metadata("design:type", String)
], CustomerGuestResponse.prototype, "access_token", void 0);
exports.CustomerGuestResponse = CustomerGuestResponse;
//# sourceMappingURL=customer-online.entity.js.map