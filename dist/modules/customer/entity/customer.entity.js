"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeletedCustomerResponse = exports.CustomerResponse = exports.CreatedCustomerResponse = exports.PaginateCustomerResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
class CreatedCustomerFieldsWithTimestampResponse extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedCustomerFieldsWithTimestampResponse.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedCustomerFieldsWithTimestampResponse.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedCustomerFieldsWithTimestampResponse.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedCustomerFieldsWithTimestampResponse.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedCustomerFieldsWithTimestampResponse.prototype, "tel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CreatedCustomerFieldsWithTimestampResponse.prototype, "status", void 0);
class CustomerFieldResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFieldResponse.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFieldResponse.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFieldResponse.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerFieldResponse.prototype, "tel", void 0);
class PaginateCustomerFields extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: [
            {
                tel: '5102638229',
                last_name: 'Henson',
                first_name: 'Tim',
                id: '635a421765e365cb4f0bb378'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateCustomerFields.prototype, "results", void 0);
class PaginateCustomerResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: PaginateCustomerFields }),
    __metadata("design:type", PaginateCustomerFields)
], PaginateCustomerResponse.prototype, "data", void 0);
exports.PaginateCustomerResponse = PaginateCustomerResponse;
class CreatedCustomerResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CreatedCustomerFieldsWithTimestampResponse,
        example: {
            status: 'active',
            tel: '5102638229',
            last_name: 'Henson',
            first_name: 'Tim',
            restaurant_id: '630eff5751c2eac55f52662c',
            created_at: '2022-10-27T08:32:23.856Z',
            updated_at: '2022-10-27T08:32:23.856Z',
            updated_by: {
                username: 'Pavarich',
                id: '6316cc72b889593b028928f1'
            },
            created_by: {
                username: 'Pavarich',
                id: '6316cc72b889593b028928f1'
            },
            id: '635a421765e365cb4f0bb378'
        }
    }),
    __metadata("design:type", CreatedCustomerFieldsWithTimestampResponse)
], CreatedCustomerResponse.prototype, "data", void 0);
exports.CreatedCustomerResponse = CreatedCustomerResponse;
class CustomerResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CustomerFieldResponse,
        example: {
            tel: '5102638229',
            last_name: 'Henson',
            first_name: 'Tim',
            id: '635a421765e365cb4f0bb378'
        }
    }),
    __metadata("design:type", CustomerFieldResponse)
], CustomerResponse.prototype, "data", void 0);
exports.CustomerResponse = CustomerResponse;
class DeletedCustomerResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CreatedCustomerFieldsWithTimestampResponse,
        example: {
            created_by: {
                username: 'Pavarich',
                id: '6316cc72b889593b028928f1'
            },
            updated_by: {
                username: 'Pavarich',
                id: '6316cc72b889593b028928f1'
            },
            status: 'deleted',
            tel: '5102638229',
            last_name: 'Henson',
            first_name: 'Tim',
            restaurant_id: '630eff5751c2eac55f52662c',
            updated_at: '2022-10-27T08:41:23.531Z',
            created_at: '2022-10-27T08:32:23.856Z',
            id: '635a421765e365cb4f0bb378'
        }
    }),
    __metadata("design:type", CreatedCustomerFieldsWithTimestampResponse)
], DeletedCustomerResponse.prototype, "data", void 0);
exports.DeletedCustomerResponse = DeletedCustomerResponse;
//# sourceMappingURL=customer.entity.js.map