"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuModifierService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const mongoose_1 = require("@nestjs/mongoose");
let MenuModifierService = class MenuModifierService {
    constructor(MenuModifierModel, request) {
        this.MenuModifierModel = MenuModifierModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.MenuModifierModel.findById({ _id: id });
    }
    async isExists(condition) {
        const result = await this.MenuModifierModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.MenuModifierModel;
    }
    async create(payload) {
        const document = new this.MenuModifierModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async update(id, menu) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, menu)).save();
    }
    async transactionCreateMany(payload, session) {
        return this.MenuModifierModel.insertMany(payload, { session });
    }
    async createMany(payload) {
        return this.MenuModifierModel.insertMany(payload);
    }
    async transactionUpdateMany(condition, payload, session) {
        return this.MenuModifierModel.updateMany(condition, { $set: Object.assign({}, payload) }, {
            session,
            upsert: true,
            new: true
        });
    }
    async delete(id) {
        return this.MenuModifierModel.deleteOne({ _id: id });
    }
    async deleteMany(condition) {
        return this.MenuModifierModel.deleteMany(condition);
    }
    getAll(condition, project, options) {
        return this.MenuModifierModel.find(condition, project, options);
    }
    findById(id) {
        return this.MenuModifierModel.findById(id);
    }
    findOne(condition) {
        return this.MenuModifierModel.findOne(condition);
    }
    paginate(query, queryParam) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder }
        };
        return this.MenuModifierModel.paginate(query, options);
    }
};
MenuModifierService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('menuModifier')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], MenuModifierService);
exports.MenuModifierService = MenuModifierService;
//# sourceMappingURL=menu-modifier.service.js.map