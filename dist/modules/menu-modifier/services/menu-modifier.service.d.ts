import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { MenuModifierDocument } from '../schemas/menu-modifier.schema';
import { Model } from 'mongoose';
import { ClientSession } from 'mongodb';
import { MenuModifierPaginateDto } from '../dto/get-menu-modifier.dto';
export declare class MenuModifierService {
    private readonly MenuModifierModel;
    private request;
    constructor(MenuModifierModel: PaginateModel<MenuModifierDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<MenuModifierDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<MenuModifierDocument>;
    create(payload: any): Promise<MenuModifierDocument>;
    update(id: string, menu: any): Promise<MenuModifierDocument>;
    transactionCreateMany(payload: any, session: ClientSession): Promise<any>;
    createMany(payload: any[]): Promise<any>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<MenuModifierDocument>;
    delete(id: string): Promise<MenuModifierDocument>;
    deleteMany(condition: any): Promise<any>;
    getAll(condition: any, project?: any, options?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any): Promise<MenuModifierDocument>;
    paginate(query: any, queryParam: MenuModifierPaginateDto): Promise<PaginateResult<MenuModifierDocument>>;
}
