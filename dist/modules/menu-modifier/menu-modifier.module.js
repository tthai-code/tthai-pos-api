"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuModifierModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const modifier_module_1 = require("../modifier/modifier.module");
const menu_modifier_controller_1 = require("./controllers/menu-modifier.controller");
const menu_modifier_public_online_controller_1 = require("./controllers/menu-modifier-public-online.controller");
const menu_modifier_logic_1 = require("./logics/menu-modifier.logic");
const menu_modifier_schema_1 = require("./schemas/menu-modifier.schema");
const menu_modifier_service_1 = require("./services/menu-modifier.service");
const menu_module_1 = require("../menu/menu.module");
let MenuModifierModule = class MenuModifierModule {
};
MenuModifierModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => menu_module_1.MenuModule),
            (0, common_1.forwardRef)(() => modifier_module_1.ModifierModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'menuModifier', schema: menu_modifier_schema_1.MenuModifierSchema }
            ])
        ],
        providers: [menu_modifier_service_1.MenuModifierService, menu_modifier_logic_1.MenuModifierLogic],
        controllers: [menu_modifier_controller_1.MenuModifierController, menu_modifier_public_online_controller_1.MenuModifierOnlineController],
        exports: [menu_modifier_service_1.MenuModifierService]
    })
], MenuModifierModule);
exports.MenuModifierModule = MenuModifierModule;
//# sourceMappingURL=menu-modifier.module.js.map