"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuModifierController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const create_menu_modifier_dto_1 = require("../dto/create-menu-modifier.dto");
const delete_menu_modifier_dto_1 = require("../dto/delete-menu-modifier.dto");
const menu_modifier_entity_1 = require("../entitiy/menu-modifier.entity");
const menu_modifier_logic_1 = require("../logics/menu-modifier.logic");
const menu_modifier_service_1 = require("../services/menu-modifier.service");
let MenuModifierController = class MenuModifierController {
    constructor(menuModifierService, menuModifierLogic) {
        this.menuModifierService = menuModifierService;
        this.menuModifierLogic = menuModifierLogic;
    }
    async getMenus(menuId) {
        return await this.menuModifierService.getAll({
            menuId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            items: 1,
            maxSelected: 1,
            type: 1,
            label: 1,
            abbreviation: 1,
            modifierId: 1,
            id: 1,
            position: 1
        }, {
            sort: {
                type: -1,
                position: 1
            }
        });
    }
    async createMenu(modifier) {
        return this.menuModifierLogic.CreateMenuModifier(modifier);
    }
    async activeMenuModifier(id) {
        return await this.menuModifierService.update(id, {
            status: status_enum_1.StatusEnum.ACTIVE
        });
    }
    async inactiveMenuModifier(id) {
        return await this.menuModifierService.update(id, {
            status: status_enum_1.StatusEnum.INACTIVE
        });
    }
    async deleteMenu(id) {
        return this.menuModifierService.delete(id);
    }
    async addBulkMenuModifiers(menuId, payload) {
        return await this.menuModifierLogic.addBulkMenuModifiers(menuId, payload);
    }
    async deleteManyModifier(menuId, payload) {
        return await this.menuModifierLogic.deleteBulkMenuModifier(menuId, payload);
    }
};
__decorate([
    (0, common_1.Get)(':menuId'),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu Modifier List By Menu ID' }),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_modifier_entity_1.GetAllMenuModifierResponse }),
    __param(0, (0, common_1.Param)('menuId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuModifierController.prototype, "getMenus", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create Menu Modifier' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_menu_modifier_dto_1.CreateMenuModifierDto]),
    __metadata("design:returntype", Promise)
], MenuModifierController.prototype, "createMenu", null);
__decorate([
    (0, common_1.Patch)(':id/active'),
    (0, swagger_1.ApiOperation)({ summary: 'Active Menu Modifier By Id' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuModifierController.prototype, "activeMenuModifier", null);
__decorate([
    (0, common_1.Patch)(':id/inactive'),
    (0, swagger_1.ApiOperation)({ summary: 'Active Menu Modifier By Id' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuModifierController.prototype, "inactiveMenuModifier", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete Menu Modifier' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuModifierController.prototype, "deleteMenu", null);
__decorate([
    (0, common_1.Post)(':menuId/add-bulk'),
    (0, swagger_1.ApiOperation)({
        summary: 'Add Bulk Menu Modifiers',
        description: 'use bearer `restaurant_token`'
    }),
    __param(0, (0, common_1.Param)('menuId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, create_menu_modifier_dto_1.AddBulkMenuModifiersDto]),
    __metadata("design:returntype", Promise)
], MenuModifierController.prototype, "addBulkMenuModifiers", null);
__decorate([
    (0, common_1.Put)(':menuId/delete-bulk'),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete Many Menu Modifier',
        description: 'use bearer `restaurant_token`'
    }),
    __param(0, (0, common_1.Param)('menuId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, delete_menu_modifier_dto_1.DeleteManyMenuModifiersDto]),
    __metadata("design:returntype", Promise)
], MenuModifierController.prototype, "deleteManyModifier", null);
MenuModifierController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('menu-modifier'),
    (0, common_1.Controller)('v1/menu-modifier'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __metadata("design:paramtypes", [menu_modifier_service_1.MenuModifierService,
        menu_modifier_logic_1.MenuModifierLogic])
], MenuModifierController);
exports.MenuModifierController = MenuModifierController;
//# sourceMappingURL=menu-modifier.controller.js.map