import { AddBulkMenuModifiersDto, CreateMenuModifierDto } from '../dto/create-menu-modifier.dto';
import { DeleteManyMenuModifiersDto } from '../dto/delete-menu-modifier.dto';
import { MenuModifierLogic } from '../logics/menu-modifier.logic';
import { MenuModifierService } from '../services/menu-modifier.service';
export declare class MenuModifierController {
    private readonly menuModifierService;
    private readonly menuModifierLogic;
    constructor(menuModifierService: MenuModifierService, menuModifierLogic: MenuModifierLogic);
    getMenus(menuId: string): Promise<any[]>;
    createMenu(modifier: CreateMenuModifierDto): Promise<import("../schemas/menu-modifier.schema").MenuModifierDocument>;
    activeMenuModifier(id: string): Promise<import("../schemas/menu-modifier.schema").MenuModifierDocument>;
    inactiveMenuModifier(id: string): Promise<import("../schemas/menu-modifier.schema").MenuModifierDocument>;
    deleteMenu(id: string): Promise<import("../schemas/menu-modifier.schema").MenuModifierDocument>;
    addBulkMenuModifiers(menuId: string, payload: AddBulkMenuModifiersDto): Promise<{
        success: boolean;
    }>;
    deleteManyModifier(menuId: string, payload: DeleteManyMenuModifiersDto): Promise<{
        success: boolean;
    }>;
}
