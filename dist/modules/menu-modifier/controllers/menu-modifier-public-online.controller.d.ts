import { MenuModifierLogic } from '../logics/menu-modifier.logic';
import { MenuModifierService } from '../services/menu-modifier.service';
export declare class MenuModifierOnlineController {
    private readonly menuModifierService;
    private readonly menuModifierLogic;
    constructor(menuModifierService: MenuModifierService, menuModifierLogic: MenuModifierLogic);
    getMenus(menuId: string): Promise<any[]>;
}
