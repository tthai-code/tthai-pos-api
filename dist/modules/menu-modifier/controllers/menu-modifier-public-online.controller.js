"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuModifierOnlineController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const menu_modifier_entity_1 = require("../entitiy/menu-modifier.entity");
const menu_modifier_logic_1 = require("../logics/menu-modifier.logic");
const menu_modifier_service_1 = require("../services/menu-modifier.service");
let MenuModifierOnlineController = class MenuModifierOnlineController {
    constructor(menuModifierService, menuModifierLogic) {
        this.menuModifierService = menuModifierService;
        this.menuModifierLogic = menuModifierLogic;
    }
    async getMenus(menuId) {
        return await this.menuModifierService.getAll({
            menuId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            items: 1,
            maxSelected: 1,
            type: 1,
            label: 1,
            abbreviation: 1,
            id: 1
        }, {
            sort: {
                type: -1,
                position: 1
            }
        });
    }
};
__decorate([
    (0, common_1.Get)(':menuId'),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu Modifier List By Menu ID' }),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_modifier_entity_1.GetAllMenuModifierResponse }),
    __param(0, (0, common_1.Param)('menuId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuModifierOnlineController.prototype, "getMenus", null);
MenuModifierOnlineController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('menu-modifier-online'),
    (0, common_1.Controller)('v1/public/online/menu-modifier'),
    __metadata("design:paramtypes", [menu_modifier_service_1.MenuModifierService,
        menu_modifier_logic_1.MenuModifierLogic])
], MenuModifierOnlineController);
exports.MenuModifierOnlineController = MenuModifierOnlineController;
//# sourceMappingURL=menu-modifier-public-online.controller.js.map