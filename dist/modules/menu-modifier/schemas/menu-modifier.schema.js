"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuModifierSchema = exports.MenuModifierFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const modifier_type_enum_1 = require("../../modifier/common/modifier-type.enum");
let ModifierOptions = class ModifierOptions {
};
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], ModifierOptions.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], ModifierOptions.prototype, "nativeName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], ModifierOptions.prototype, "price", void 0);
ModifierOptions = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: false, strict: true })
], ModifierOptions);
let MenuModifierFields = class MenuModifierFields {
};
__decorate([
    (0, mongoose_1.Prop)({
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'menu'
    }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], MenuModifierFields.prototype, "menuId", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        type: mongoose_2.Schema.Types.ObjectId,
        ref: 'modifier'
    }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], MenuModifierFields.prototype, "modifierId", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], MenuModifierFields.prototype, "label", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], MenuModifierFields.prototype, "abbreviation", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", Array)
], MenuModifierFields.prototype, "items", void 0);
__decorate([
    (0, mongoose_1.Prop)({ enum: Object.values(modifier_type_enum_1.ModifierTypeEnum) }),
    __metadata("design:type", String)
], MenuModifierFields.prototype, "type", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], MenuModifierFields.prototype, "maxSelected", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 999 }),
    __metadata("design:type", Number)
], MenuModifierFields.prototype, "position", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], MenuModifierFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], MenuModifierFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], MenuModifierFields.prototype, "createdBy", void 0);
MenuModifierFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'menuModifiers' })
], MenuModifierFields);
exports.MenuModifierFields = MenuModifierFields;
exports.MenuModifierSchema = mongoose_1.SchemaFactory.createForClass(MenuModifierFields);
exports.MenuModifierSchema.plugin(mongoosePaginate);
exports.MenuModifierSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=menu-modifier.schema.js.map