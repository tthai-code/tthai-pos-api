import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type MenuModifierDocument = MenuModifierFields & Document;
declare class ModifierOptions {
    name: string;
    nativeName: string;
    price: number;
}
export declare class MenuModifierFields {
    menuId: Types.ObjectId;
    modifierId: Types.ObjectId;
    label: string;
    abbreviation: string;
    items: Array<ModifierOptions>;
    type: string;
    maxSelected: number;
    position: number;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const MenuModifierSchema: MongooseSchema<Document<MenuModifierFields, any, any>, import("mongoose").Model<Document<MenuModifierFields, any, any>, any, any, any>, any, any>;
export {};
