import { ResponseDto } from 'src/common/entity/response.entity';
declare class ItemsResponseFields {
    name: string;
    native_name: string;
    price: number;
}
export declare class MenuModifierResponseFields {
    type: string;
    items: Array<ItemsResponseFields>;
    label: string;
    abbreviation: string;
    id: string;
}
export declare class GetAllMenuModifierResponse extends ResponseDto<Array<MenuModifierResponseFields>> {
    data: Array<MenuModifierResponseFields>;
}
export {};
