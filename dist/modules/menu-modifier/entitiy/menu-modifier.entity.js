"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllMenuModifierResponse = exports.MenuModifierResponseFields = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const modifier_type_enum_1 = require("../../modifier/common/modifier-type.enum");
class ItemsResponseFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsResponseFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemsResponseFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemsResponseFields.prototype, "price", void 0);
class MenuModifierResponseFields {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(modifier_type_enum_1.ModifierTypeEnum) }),
    __metadata("design:type", String)
], MenuModifierResponseFields.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: ItemsResponseFields }),
    __metadata("design:type", Array)
], MenuModifierResponseFields.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuModifierResponseFields.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuModifierResponseFields.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuModifierResponseFields.prototype, "id", void 0);
exports.MenuModifierResponseFields = MenuModifierResponseFields;
class GetAllMenuModifierResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: MenuModifierResponseFields,
        example: {
            message: 'done',
            data: [
                {
                    type: 'REQUIRED',
                    items: [
                        {
                            name: 'Not Spicy',
                            native_name: 'Not Spicy',
                            price: 0
                        },
                        {
                            name: 'Quite Spicy',
                            native_name: 'Quite Spicy',
                            price: 0
                        },
                        {
                            name: 'Spicy',
                            native_name: 'Spicy',
                            price: 0
                        }
                    ],
                    label: 'Spicy Choices',
                    id: '630fc086af525d6ed9fc0d61'
                },
                {
                    type: 'REQUIRED',
                    items: [
                        {
                            name: 'Beef',
                            native_name: 'Beef',
                            price: 2.5
                        },
                        {
                            name: 'Pork',
                            native_name: 'Pork',
                            price: 1
                        },
                        {
                            name: 'Chicken',
                            native_name: 'Chicken',
                            price: 0
                        }
                    ],
                    label: 'Protein Choices',
                    abbreviation: 'P',
                    id: '630fcd08b6fb5ee3bab46885'
                }
            ]
        }
    }),
    __metadata("design:type", Array)
], GetAllMenuModifierResponse.prototype, "data", void 0);
exports.GetAllMenuModifierResponse = GetAllMenuModifierResponse;
//# sourceMappingURL=menu-modifier.entity.js.map