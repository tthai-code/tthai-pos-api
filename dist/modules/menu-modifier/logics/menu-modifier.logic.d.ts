import { MenuService } from 'src/modules/menu/services/menu.service';
import { ModifierService } from 'src/modules/modifier/services/modifier.service';
import { AddBulkMenuModifiersDto, CreateMenuModifierDto } from '../dto/create-menu-modifier.dto';
import { DeleteManyMenuModifiersDto } from '../dto/delete-menu-modifier.dto';
import { MenuModifierService } from '../services/menu-modifier.service';
export declare class MenuModifierLogic {
    private readonly modifierService;
    private readonly menuModifierService;
    private readonly menuService;
    constructor(modifierService: ModifierService, menuModifierService: MenuModifierService, menuService: MenuService);
    CreateMenuModifier(createMenuModifier: CreateMenuModifierDto): Promise<import("../schemas/menu-modifier.schema").MenuModifierDocument>;
    addBulkMenuModifiers(menuId: string, payload: AddBulkMenuModifiersDto): Promise<{
        success: boolean;
    }>;
    deleteBulkMenuModifier(menuId: string, payload: DeleteManyMenuModifiersDto): Promise<{
        success: boolean;
    }>;
}
