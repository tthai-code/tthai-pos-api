"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuModifierLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const menu_service_1 = require("../../menu/services/menu.service");
const modifier_service_1 = require("../../modifier/services/modifier.service");
const menu_modifier_service_1 = require("../services/menu-modifier.service");
let MenuModifierLogic = class MenuModifierLogic {
    constructor(modifierService, menuModifierService, menuService) {
        this.modifierService = modifierService;
        this.menuModifierService = menuModifierService;
        this.menuService = menuService;
    }
    async CreateMenuModifier(createMenuModifier) {
        const isExist = await this.menuModifierService.findOne({
            menuId: createMenuModifier.menuId,
            modifierId: createMenuModifier.modifierId,
            status: { $ne: status_enum_1.StatusEnum.DELETED }
        });
        if (isExist)
            throw new common_1.BadRequestException('Menu Modifier is exist.');
        const modifier = await this.modifierService.getModel().findOne({
            _id: createMenuModifier.modifierId
        }, {
            label: 1,
            abbreviation: 1,
            type: 1,
            items: 1,
            maxSelected: 1
        });
        const created = await this.menuModifierService.create({
            modifierId: modifier._id,
            menuId: createMenuModifier.menuId,
            label: modifier.label,
            abbreviation: modifier.abbreviation,
            type: modifier.type,
            items: modifier.items,
            maxSelected: modifier.maxSelected,
            status: createMenuModifier.status ? createMenuModifier.status : 'active'
        });
        return created;
    }
    async addBulkMenuModifiers(menuId, payload) {
        const menu = await this.menuService.findOne({
            _id: menuId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!menu)
            throw new common_1.NotFoundException('Menu not found.');
        const { items } = payload;
        const modifierIds = items.map((item) => item.modifierId);
        const menuModifiers = await this.menuModifierService.getAll({
            _id: { $in: modifierIds },
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (menuModifiers.length > 0) {
            const modifierNames = menuModifiers.map((item) => item.label);
            throw new common_1.BadRequestException(`Modifiers Label (${modifierNames.join(',')}) are already exist.`);
        }
        const modifiers = await this.modifierService.getAll({
            _id: { $in: modifierIds },
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const existModifiers = modifiers.filter((item) => modifierIds.includes(item.id));
        if (existModifiers.length > 0) {
            const createPayload = existModifiers.map((item) => ({
                menuId,
                modifierId: item.id,
                label: item.label,
                abbreviation: item.abbreviation,
                type: item.type,
                items: item.items,
                maxSelected: item.maxSelected,
                position: item.position
            }));
            await this.menuModifierService.createMany(createPayload);
        }
        return { success: true };
    }
    async deleteBulkMenuModifier(menuId, payload) {
        const menu = await this.menuService.findOne({
            _id: menuId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!menu)
            throw new common_1.NotFoundException('Menu not found.');
        const { items } = payload;
        const menuModifiers = await this.menuModifierService.getAll({
            _id: { $in: items },
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (menuModifiers.length > 0) {
            const menuModifierIds = menuModifiers.map((item) => item.id);
            await this.menuModifierService.deleteMany({
                _id: { $in: menuModifierIds },
                status: status_enum_1.StatusEnum.ACTIVE
            });
        }
        return { success: true };
    }
};
MenuModifierLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [modifier_service_1.ModifierService,
        menu_modifier_service_1.MenuModifierService,
        menu_service_1.MenuService])
], MenuModifierLogic);
exports.MenuModifierLogic = MenuModifierLogic;
//# sourceMappingURL=menu-modifier.logic.js.map