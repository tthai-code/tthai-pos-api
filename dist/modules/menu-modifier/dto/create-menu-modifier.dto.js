"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddBulkMenuModifiersDto = exports.AddMenuModifierObjectDto = exports.CreateMenuModifierDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const status_enum_1 = require("../../../common/enum/status.enum");
class CreateMenuModifierDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '630a6824a411620066d39161' }),
    __metadata("design:type", String)
], CreateMenuModifierDto.prototype, "menuId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: '6308974b19416019c26ae15d' }),
    __metadata("design:type", String)
], CreateMenuModifierDto.prototype, "modifierId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsEnum)(status_enum_1.StatusEnum),
    (0, swagger_1.ApiPropertyOptional)({
        example: status_enum_1.StatusEnum.ACTIVE,
        enum: Object.values(status_enum_1.StatusEnum)
    }),
    __metadata("design:type", String)
], CreateMenuModifierDto.prototype, "status", void 0);
exports.CreateMenuModifierDto = CreateMenuModifierDto;
class AddMenuModifierObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '6308974b19416019c26ae15d' }),
    __metadata("design:type", String)
], AddMenuModifierObjectDto.prototype, "modifierId", void 0);
exports.AddMenuModifierObjectDto = AddMenuModifierObjectDto;
class AddBulkMenuModifiersDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => AddMenuModifierObjectDto),
    (0, class_validator_1.ArrayNotEmpty)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: [AddMenuModifierObjectDto] }),
    __metadata("design:type", Array)
], AddBulkMenuModifiersDto.prototype, "items", void 0);
exports.AddBulkMenuModifiersDto = AddBulkMenuModifiersDto;
//# sourceMappingURL=create-menu-modifier.dto.js.map