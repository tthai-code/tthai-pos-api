export declare class CreateMenuModifierDto {
    readonly menuId: string;
    readonly modifierId: string;
    readonly status: string;
}
export declare class AddMenuModifierObjectDto {
    readonly modifierId: string;
}
export declare class AddBulkMenuModifiersDto {
    readonly items: AddMenuModifierObjectDto[];
}
