import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class MenuModifierPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly status: string;
    readonly search: string;
    readonly restaurant: string;
    buildQuery(id: string): {
        $or: ({
            name: {
                $regex: string;
                $options: string;
            };
            'options.name'?: undefined;
        } | {
            'options.name': {
                $regex: string;
                $options: string;
            };
            name?: undefined;
        })[];
        restaurantId: {
            $eq: string;
        };
        status: string | {
            $ne: StatusEnum;
        };
    };
}
