import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type MenuDocument = MenuFields & Document;
export declare class MenuFields {
    restaurantId: Types.ObjectId;
    name: string;
    nativeName: string;
    category: Array<string>;
    position: number;
    description: string;
    price: number;
    imageUrls: Array<string>;
    coverImage: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const MenuSchema: MongooseSchema<Document<MenuFields, any, any>, import("mongoose").Model<Document<MenuFields, any, any>, any, any, any>, any, any>;
