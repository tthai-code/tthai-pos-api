export declare class PositionObject {
    id: string;
    position: number;
}
export declare class UpdateMenuPositionDto {
    items: PositionObject[];
}
