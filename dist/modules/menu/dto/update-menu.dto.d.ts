export declare class UpdateMenuDto {
    readonly name: string;
    readonly nativeName: string;
    readonly description: string;
    position: number;
    price: number;
    readonly category: Array<string>;
    readonly imageUrls: Array<string>;
    readonly coverImage: string;
    readonly status: string;
}
