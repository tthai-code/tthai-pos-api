import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class POSMenuPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly category: string;
    buildQuery(id: string): {
        $or: ({
            name: {
                $regex: string;
                $options: string;
            };
            nativeName?: undefined;
        } | {
            nativeName: {
                $regex: string;
                $options: string;
            };
            name?: undefined;
        })[];
        category: string;
        restaurantId: string;
        status: StatusEnum;
    };
}
