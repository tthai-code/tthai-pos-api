import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { Model } from 'mongoose';
import { MenuDocument } from '../schemas/menu.schema';
import { MenuPaginateDto } from '../dto/get-menu.dto';
import { POSMenuPaginateDto } from '../dto/get-pos-menu.dto';
import { OpenMenuPaginateDto } from '../dto/get-open-menu.dto';
export declare class MenuService {
    private readonly MenuModel;
    private request;
    constructor(MenuModel: PaginateModel<MenuDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<MenuDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<MenuDocument>;
    create(payload: any): Promise<MenuDocument>;
    update(id: string, menu: any): Promise<MenuDocument>;
    delete(id: string): Promise<MenuDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any, options?: any): Promise<MenuDocument>;
    paginate(query: any, queryParam: MenuPaginateDto | POSMenuPaginateDto | OpenMenuPaginateDto, select?: any): Promise<PaginateResult<MenuDocument>>;
    bulkWrite(payload: any[], options?: any): any;
}
