"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuOnlineController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const menu_modifier_service_1 = require("../../menu-modifier/services/menu-modifier.service");
const errorResponse_1 = require("../../../utilities/errorResponse");
const get_menu_dto_1 = require("../dto/get-menu.dto");
const menu_entity_1 = require("../entity/menu.entity");
const pos_menu_entity_1 = require("../entity/pos-menu.entity");
const menu_logic_1 = require("../logics/menu.logic");
const menu_service_1 = require("../services/menu.service");
let MenuOnlineController = class MenuOnlineController {
    constructor(menuService, menuLogic, menuModifierService) {
        this.menuService = menuService;
        this.menuLogic = menuLogic;
        this.menuModifierService = menuModifierService;
    }
    async getMenus(query) {
        return await this.menuLogic.getPaginateMenuWeb(query);
    }
    async getMenu(id) {
        return await this.menuLogic.getMenuForWebById(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu List Online' }),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_entity_1.OnlineMenuPaginateResponse }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_menu_dto_1.MenuPaginateDto]),
    __metadata("design:returntype", Promise)
], MenuOnlineController.prototype, "getMenus", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_menu_entity_1.OnlineMenuResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(404, 'Not found menu.', '/v1/public/online/menu/:id')),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu By ID for Online' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuOnlineController.prototype, "getMenu", null);
MenuOnlineController = __decorate([
    (0, swagger_1.ApiTags)('menu-public-online'),
    (0, common_1.Controller)('v1/public/online/menu'),
    __metadata("design:paramtypes", [menu_service_1.MenuService,
        menu_logic_1.MenuLogic,
        menu_modifier_service_1.MenuModifierService])
], MenuOnlineController);
exports.MenuOnlineController = MenuOnlineController;
//# sourceMappingURL=menu-public-online.controller.js.map