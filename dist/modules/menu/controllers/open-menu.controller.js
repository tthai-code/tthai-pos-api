"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenMenuController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const get_open_menu_dto_1 = require("../dto/get-open-menu.dto");
const open_menu_entity_1 = require("../entity/open-menu.entity");
const open_menu_logic_1 = require("../logics/open-menu.logic");
let OpenMenuController = class OpenMenuController {
    constructor(openMenuLogic) {
        this.openMenuLogic = openMenuLogic;
    }
    async getMenuPaginate(query) {
        return await this.openMenuLogic.getPaginateMenuApp(query);
    }
    async getMenuById(id) {
        return await this.openMenuLogic.getMenuForAppById(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: open_menu_entity_1.PaginateOpenMenuResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Paginate Menu for app.',
        description: 'use basic auth'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_open_menu_dto_1.OpenMenuPaginateDto]),
    __metadata("design:returntype", Promise)
], OpenMenuController.prototype, "getMenuPaginate", null);
__decorate([
    (0, common_1.Get)(':menuId'),
    (0, swagger_1.ApiOkResponse)({ type: open_menu_entity_1.GetOpenMenuResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Menu By ID',
        description: 'use basic auth'
    }),
    __param(0, (0, common_1.Param)('menuId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OpenMenuController.prototype, "getMenuById", null);
OpenMenuController = __decorate([
    (0, swagger_1.ApiBasicAuth)(),
    (0, swagger_1.ApiTags)('open/menu'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    (0, common_1.Controller)('v1/open/menu'),
    __metadata("design:paramtypes", [open_menu_logic_1.OpenMenuLogic])
], OpenMenuController);
exports.OpenMenuController = OpenMenuController;
//# sourceMappingURL=open-menu.controller.js.map