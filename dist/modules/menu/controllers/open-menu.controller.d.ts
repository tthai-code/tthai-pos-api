import { OpenMenuPaginateDto } from '../dto/get-open-menu.dto';
import { OpenMenuLogic } from '../logics/open-menu.logic';
export declare class OpenMenuController {
    private readonly openMenuLogic;
    constructor(openMenuLogic: OpenMenuLogic);
    getMenuPaginate(query: OpenMenuPaginateDto): Promise<any>;
    getMenuById(id: string): Promise<{
        id: any;
        name: string;
        nativeName: string;
        description: string;
        position: number;
        price: number;
        coverImage: string;
        imageUrls: string[];
        category: any[];
        isContainAlcohol: boolean;
        modifiers: any[];
        isAvailable: boolean;
    }>;
}
