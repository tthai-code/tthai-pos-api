import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service';
import { MenuPaginateDto } from '../dto/get-menu.dto';
import { MenuLogic } from '../logics/menu.logic';
import { MenuService } from '../services/menu.service';
export declare class MenuOnlineController {
    private readonly menuService;
    private readonly menuLogic;
    private readonly menuModifierService;
    constructor(menuService: MenuService, menuLogic: MenuLogic, menuModifierService: MenuModifierService);
    getMenus(query: MenuPaginateDto): Promise<any>;
    getMenu(id: string): Promise<{
        id: any;
        name: string;
        nativeName: string;
        description: string;
        position: number;
        price: number;
        coverImage: string;
        imageUrls: string[];
        category: any[];
        isContainAlcohol: boolean;
        modifiers: any[];
        isAvailable: boolean;
    }>;
}
