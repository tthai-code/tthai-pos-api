"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSMenuController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const menu_service_1 = require("../services/menu.service");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const menu_logic_1 = require("../logics/menu.logic");
const get_pos_menu_dto_1 = require("../dto/get-pos-menu.dto");
const pos_menu_entity_1 = require("../entity/pos-menu.entity");
const errorResponse_1 = require("../../../utilities/errorResponse");
let POSMenuController = class POSMenuController {
    constructor(menuService, menuLogic) {
        this.menuService = menuService;
        this.menuLogic = menuLogic;
    }
    async getMenus(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.menuLogic.getPaginateMenuPOS(restaurantId, query);
    }
    async getAllMenu() {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.menuLogic.getAllMenuPOS(restaurantId);
    }
    async getMenu(id) {
        return await this.menuLogic.getMenuForPOSById(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_menu_entity_1.PaginatePOSMenuResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(401, 'Unauthorized', '/v1/pos/menu')),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Paginate Menu List for POS',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_pos_menu_dto_1.POSMenuPaginateDto]),
    __metadata("design:returntype", Promise)
], POSMenuController.prototype, "getMenus", null);
__decorate([
    (0, common_1.Get)('all-menu'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_menu_entity_1.POSAllMenuResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get All Menu for POS',
        description: 'use *bearer* `staff_token`\n\nuse this route after login with staff.'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], POSMenuController.prototype, "getAllMenu", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_menu_entity_1.POSMenuResponse }),
    (0, swagger_1.ApiUnauthorizedResponse)((0, errorResponse_1.errorResponse)(404, 'Not found menu.', '/v1/pos/menu/:id')),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Menu By ID for POS',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSMenuController.prototype, "getMenu", null);
POSMenuController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/menu'),
    (0, common_1.Controller)('v1/pos/menu'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    __metadata("design:paramtypes", [menu_service_1.MenuService,
        menu_logic_1.MenuLogic])
], POSMenuController);
exports.POSMenuController = POSMenuController;
//# sourceMappingURL=pos-menu.controller.js.map