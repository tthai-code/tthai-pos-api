"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const menu_modifier_service_1 = require("../../menu-modifier/services/menu-modifier.service");
const create_menu_dto_1 = require("../dto/create-menu.dto");
const get_menu_dto_1 = require("../dto/get-menu.dto");
const update_menu_position_dto_1 = require("../dto/update-menu-position.dto");
const update_menu_dto_1 = require("../dto/update-menu.dto");
const menu_entity_1 = require("../entity/menu.entity");
const menu_logic_1 = require("../logics/menu.logic");
const menu_service_1 = require("../services/menu.service");
let MenuController = class MenuController {
    constructor(menuService, menuLogic, menuModifierService) {
        this.menuService = menuService;
        this.menuLogic = menuLogic;
        this.menuModifierService = menuModifierService;
    }
    async getMenus(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.menuService.paginate(query.buildQuery(restaurantId), query);
    }
    async getMenu(id) {
        return await this.menuLogic.getMenuById(id);
    }
    async createMenu(menu) {
        return this.menuLogic.createLogic(menu);
    }
    async updateMenu(menu, id) {
        return this.menuLogic.updateLogic(id, menu);
    }
    async deleteMenu(id) {
        await this.menuModifierService.deleteMany({ menuId: id });
        return this.menuService.update(id, { status: status_enum_1.StatusEnum.DELETED });
    }
    async updatePosition(payload) {
        return await this.menuLogic.updateMenuPosition(payload);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu List' }),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_entity_1.PaginateMenuResponse }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_menu_dto_1.MenuPaginateDto]),
    __metadata("design:returntype", Promise)
], MenuController.prototype, "getMenus", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_entity_1.GetMenuResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Menu By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuController.prototype, "getMenu", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => menu_entity_1.CreatedMenuResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Create Menu' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_menu_dto_1.CreateMenuDto]),
    __metadata("design:returntype", Promise)
], MenuController.prototype, "createMenu", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_entity_1.CreatedMenuResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Update Menu' }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_menu_dto_1.UpdateMenuDto, String]),
    __metadata("design:returntype", Promise)
], MenuController.prototype, "updateMenu", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => menu_entity_1.DeletedMenuResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Delete Menu' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MenuController.prototype, "deleteMenu", null);
__decorate([
    (0, common_1.Put)('position/all'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Position Menu' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_menu_position_dto_1.UpdateMenuPositionDto]),
    __metadata("design:returntype", Promise)
], MenuController.prototype, "updatePosition", null);
MenuController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('menu'),
    (0, common_1.Controller)('v1/menu'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __metadata("design:paramtypes", [menu_service_1.MenuService,
        menu_logic_1.MenuLogic,
        menu_modifier_service_1.MenuModifierService])
], MenuController);
exports.MenuController = MenuController;
//# sourceMappingURL=menu.controller.js.map