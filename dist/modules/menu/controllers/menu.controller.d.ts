import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service';
import { CreateMenuDto } from '../dto/create-menu.dto';
import { MenuPaginateDto } from '../dto/get-menu.dto';
import { UpdateMenuPositionDto } from '../dto/update-menu-position.dto';
import { UpdateMenuDto } from '../dto/update-menu.dto';
import { MenuLogic } from '../logics/menu.logic';
import { MenuService } from '../services/menu.service';
export declare class MenuController {
    private readonly menuService;
    private readonly menuLogic;
    private readonly menuModifierService;
    constructor(menuService: MenuService, menuLogic: MenuLogic, menuModifierService: MenuModifierService);
    getMenus(query: MenuPaginateDto): Promise<PaginateResult<import("../schemas/menu.schema").MenuDocument>>;
    getMenu(id: string): Promise<{
        id: any;
        name: string;
        nativeName: string;
        description: string;
        position: number;
        price: number;
        coverImage: string;
        imageUrls: string[];
        category: any[];
    }>;
    createMenu(menu: CreateMenuDto): Promise<import("../schemas/menu.schema").MenuDocument>;
    updateMenu(menu: UpdateMenuDto, id: string): Promise<import("../schemas/menu.schema").MenuDocument>;
    deleteMenu(id: string): Promise<import("../schemas/menu.schema").MenuDocument>;
    updatePosition(payload: UpdateMenuPositionDto): Promise<{
        success: boolean;
    }>;
}
