import { MenuService } from '../services/menu.service';
import { MenuLogic } from '../logics/menu.logic';
import { POSMenuPaginateDto } from '../dto/get-pos-menu.dto';
export declare class POSMenuController {
    private readonly menuService;
    private readonly menuLogic;
    constructor(menuService: MenuService, menuLogic: MenuLogic);
    getMenus(query: POSMenuPaginateDto): Promise<any>;
    getAllMenu(): Promise<any[]>;
    getMenu(id: string): Promise<{
        id: any;
        name: string;
        nativeName: string;
        description: string;
        position: number;
        price: number;
        coverImage: string;
        imageUrls: string[];
        category: any[];
        isContainAlcohol: boolean;
        modifiers: any[];
    }>;
}
