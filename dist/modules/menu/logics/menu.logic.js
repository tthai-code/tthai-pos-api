"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuLogic = void 0;
const common_1 = require("@nestjs/common");
const menu_category_service_1 = require("../../menu-category/services/menu-category.service");
const menu_service_1 = require("../services/menu.service");
const status_enum_1 = require("../../../common/enum/status.enum");
const modifier_service_1 = require("../../modifier/services/modifier.service");
const menu_modifier_service_1 = require("../../menu-modifier/services/menu-modifier.service");
const menu_category_public_online_logic_1 = require("../../menu-category/logics/menu-category-public-online-logic");
const web_setting_service_1 = require("../../web-setting/services/web-setting.service");
let MenuLogic = class MenuLogic {
    constructor(menuCategoriesService, menuService, modifierService, menuModifierService, menuCategoryOnlineLogic, webSettingService) {
        this.menuCategoriesService = menuCategoriesService;
        this.menuService = menuService;
        this.modifierService = modifierService;
        this.menuModifierService = menuModifierService;
        this.menuCategoryOnlineLogic = menuCategoryOnlineLogic;
        this.webSettingService = webSettingService;
    }
    async createLogic(payload) {
        const lastPosition = await this.menuService.findOne({
            restaurantId: payload.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {}, { sort: { position: -1 } });
        const position = (lastPosition === null || lastPosition === void 0 ? void 0 : lastPosition.position) | 0;
        payload.position = position + 1;
        const menu = await this.menuService.create(payload);
        const modifier = await this.modifierService.getAll({
            _id: { $in: payload.modifiers },
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            label: 1,
            abbreviation: 1,
            type: 1,
            items: 1,
            maxSelected: 1,
            position: 1
        });
        const menuModifierPayload = modifier.map((item) => {
            const payload = Object.assign(Object.assign({}, item._doc), { modifierId: item._doc._id, menuId: menu.id });
            delete payload._id;
            return this.menuModifierService.create(payload);
        });
        await Promise.all(menuModifierPayload);
        return menu;
    }
    async updateLogic(id, payload) {
        const menuData = await this.menuService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!menuData) {
            throw new common_1.NotFoundException(`Menu id ${id} not found.`);
        }
        return this.menuService.update(id, payload);
    }
    async getMenuById(id) {
        const menu = await this.menuService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!menu)
            throw new common_1.NotFoundException('Not found menu.');
        const category = await this.menuCategoriesService.getAll({ _id: { $in: menu.category }, isShow: true, status: status_enum_1.StatusEnum.ACTIVE }, { name: 1 });
        return {
            id: menu._id,
            name: menu.name,
            nativeName: menu.nativeName,
            description: menu.description,
            position: menu.position,
            price: menu.price,
            coverImage: menu.coverImage,
            imageUrls: menu.imageUrls,
            category
        };
    }
    async getMenuForPOSById(id) {
        const menu = await this.menuService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!menu)
            throw new common_1.NotFoundException('Not found menu.');
        const category = await this.menuCategoriesService.getAll({ _id: { $in: menu.category }, isShow: true, status: status_enum_1.StatusEnum.ACTIVE }, { name: 1, isContainAlcohol: 1 });
        const modifiers = await this.menuModifierService.getAll({
            menuId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            label: 1,
            abbreviation: 1,
            items: 1,
            type: 1,
            maxSelected: 1
        }, {
            sort: {
                type: -1,
                position: 1
            }
        });
        return {
            id: menu._id,
            name: menu.name,
            nativeName: menu.nativeName,
            description: menu.description,
            position: menu.position,
            price: menu.price,
            coverImage: menu.coverImage,
            imageUrls: menu.imageUrls,
            category,
            isContainAlcohol: category.length > 0
                ? category.some((item) => item.isContainAlcohol)
                : false,
            modifiers
        };
    }
    async getPaginateMenuPOS(restaurantId, query) {
        const menuPaginate = await this.menuService.paginate(query.buildQuery(restaurantId), query);
        if (!menuPaginate.docs)
            return menuPaginate;
        const { docs } = menuPaginate;
        const results = [];
        const category = await this.menuCategoriesService.getAll({ restaurantId, isShow: true, status: status_enum_1.StatusEnum.ACTIVE }, { name: 1, isContainAlcohol: 1 });
        const containAlcohol = category.filter((item) => item.isContainAlcohol);
        for (const menu of docs) {
            const modifiers = await this.menuModifierService.getAll({
                menuId: menu.id,
                status: status_enum_1.StatusEnum.ACTIVE
            }, {
                _id: 0,
                id: '$_id',
                label: 1,
                abbreviation: 1,
                items: 1,
                type: 1,
                maxSelected: 1,
                position: 1
            }, {
                sort: { type: -1, position: 1 }
            });
            const menuCategory = [];
            menu.category.forEach((e) => {
                const index = category.findIndex((item) => `${item._id}` === e);
                if (index > -1) {
                    menuCategory.push({
                        id: e,
                        name: category[index].name,
                        isContainAlcohol: category[index].isContainAlcohol
                    });
                }
            });
            results.push(Object.assign(Object.assign({}, menu.toObject()), { modifiers, category: menuCategory, isContainAlcohol: category.length > 0
                    ? containAlcohol.some((item) => menu.category.includes(item.id))
                    : false }));
        }
        return Object.assign(Object.assign({}, menuPaginate), { docs: results });
    }
    async getPaginateMenuWeb(query) {
        const menuPaginate = await this.menuService.paginate(query.buildQuery(query.restaurantId), query, { status: 0, createdAt: 0, createdBy: 0, updatedBy: 0, updatedAt: 0 });
        if (!menuPaginate.docs)
            return menuPaginate;
        const webSetting = await this.webSettingService.findOne({
            restaurantId: query.restaurantId
        });
        let availableCategory = [];
        if (webSetting.isOnlineOrdering) {
            availableCategory =
                await this.menuCategoryOnlineLogic.getAvailableStatusForCategory(query.restaurantId);
        }
        const { docs } = menuPaginate;
        const results = [];
        for (const menu of docs) {
            const isAvailable = availableCategory.findIndex((item) => menu.category.findIndex((e) => e === item) > -1) > -1;
            const modifiers = await this.menuModifierService.getAll({
                menuId: menu.id,
                status: status_enum_1.StatusEnum.ACTIVE
            }, {
                _id: 0,
                id: '$_id',
                label: 1,
                abbreviation: 1,
                items: 1,
                type: 1,
                maxSelected: 1
            }, {
                sort: {
                    type: -1,
                    position: 1
                }
            });
            results.push(Object.assign(Object.assign({}, menu.toObject()), { isAvailable,
                modifiers }));
        }
        return Object.assign(Object.assign({}, menuPaginate), { docs: results });
    }
    async getMenuForWebById(id) {
        const menu = await this.menuService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { status: 0, createdAt: 0, createdBy: 0, updatedBy: 0, updatedAt: 0 });
        if (!menu)
            throw new common_1.NotFoundException('Not found menu.');
        const category = await this.menuCategoriesService.getAll({ _id: { $in: menu.category }, isShow: true, status: status_enum_1.StatusEnum.ACTIVE }, { name: 1, isContainAlcohol: 1 });
        const webSetting = await this.webSettingService.findOne({
            restaurantId: menu.restaurantId
        });
        let availableCategory = [];
        if (webSetting.isOnlineOrdering) {
            availableCategory =
                await this.menuCategoryOnlineLogic.getAvailableStatusForCategory(`${menu.restaurantId}`);
        }
        const isAvailable = availableCategory.findIndex((item) => menu.category.findIndex((e) => e === item) > -1) > -1;
        const modifiers = await this.menuModifierService.getAll({
            menuId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            label: 1,
            abbreviation: 1,
            items: 1,
            type: 1,
            maxSelected: 1
        }, {
            sort: {
                type: -1,
                position: 1
            }
        });
        return {
            id: menu._id,
            name: menu.name,
            nativeName: menu.nativeName,
            description: menu.description,
            position: menu.position,
            price: menu.price,
            coverImage: menu.coverImage,
            imageUrls: menu.imageUrls,
            category,
            isContainAlcohol: category.length > 0
                ? category.some((item) => item.isContainAlcohol)
                : false,
            modifiers,
            isAvailable
        };
    }
    async updateMenuPosition(payload) {
        const { items } = payload;
        const bulkPayload = items.map((item) => ({
            updateOne: {
                filter: { _id: item.id },
                update: { position: item.position }
            }
        }));
        await this.menuService.bulkWrite(bulkPayload, { ordered: true });
        return { success: true };
    }
    async getAllMenuPOS(restaurantId) {
        const menuPaginate = await this.menuService.getAll({
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const results = [];
        const category = await this.menuCategoriesService.getAll({ restaurantId, isShow: true, status: status_enum_1.StatusEnum.ACTIVE }, { name: 1, isContainAlcohol: 1 });
        const containAlcohol = category.filter((item) => item.isContainAlcohol);
        for (const menu of menuPaginate) {
            const modifiers = await this.menuModifierService.getAll({
                menuId: menu.id,
                status: status_enum_1.StatusEnum.ACTIVE
            }, {
                _id: 0,
                id: '$_id',
                label: 1,
                abbreviation: 1,
                items: 1,
                type: 1,
                maxSelected: 1,
                position: 1
            }, {
                sort: { type: -1, position: 1 }
            });
            const menuCategory = [];
            menu.category.forEach((e) => {
                const index = category.findIndex((item) => `${item._id}` === e);
                if (index > -1) {
                    menuCategory.push({
                        id: e,
                        name: category[index].name,
                        isContainAlcohol: category[index].isContainAlcohol
                    });
                }
            });
            results.push(Object.assign(Object.assign({}, menu.toObject()), { modifiers, category: menuCategory, isContainAlcohol: category.length > 0
                    ? containAlcohol.some((item) => menu.category.includes(item.id))
                    : false }));
        }
        return results;
    }
};
MenuLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [menu_category_service_1.MenuCategoriesService,
        menu_service_1.MenuService,
        modifier_service_1.ModifierService,
        menu_modifier_service_1.MenuModifierService,
        menu_category_public_online_logic_1.MenuCategoryOnlineLogic,
        web_setting_service_1.WebSettingService])
], MenuLogic);
exports.MenuLogic = MenuLogic;
//# sourceMappingURL=menu.logic.js.map