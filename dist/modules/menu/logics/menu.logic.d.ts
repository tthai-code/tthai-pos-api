import { MenuCategoriesService } from 'src/modules/menu-category/services/menu-category.service';
import { MenuService } from '../services/menu.service';
import { CreateMenuDto } from '../dto/create-menu.dto';
import { ModifierService } from 'src/modules/modifier/services/modifier.service';
import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service';
import { UpdateMenuDto } from '../dto/update-menu.dto';
import { POSMenuPaginateDto } from '../dto/get-pos-menu.dto';
import { MenuPaginateDto } from '../dto/get-menu.dto';
import { MenuCategoryOnlineLogic } from 'src/modules/menu-category/logics/menu-category-public-online-logic';
import { WebSettingService } from 'src/modules/web-setting/services/web-setting.service';
import { UpdateMenuPositionDto } from '../dto/update-menu-position.dto';
export declare class MenuLogic {
    private readonly menuCategoriesService;
    private readonly menuService;
    private readonly modifierService;
    private readonly menuModifierService;
    private readonly menuCategoryOnlineLogic;
    private readonly webSettingService;
    constructor(menuCategoriesService: MenuCategoriesService, menuService: MenuService, modifierService: ModifierService, menuModifierService: MenuModifierService, menuCategoryOnlineLogic: MenuCategoryOnlineLogic, webSettingService: WebSettingService);
    createLogic(payload: CreateMenuDto): Promise<import("../schemas/menu.schema").MenuDocument>;
    updateLogic(id: string, payload: UpdateMenuDto): Promise<import("../schemas/menu.schema").MenuDocument>;
    getMenuById(id: string): Promise<{
        id: any;
        name: string;
        nativeName: string;
        description: string;
        position: number;
        price: number;
        coverImage: string;
        imageUrls: string[];
        category: any[];
    }>;
    getMenuForPOSById(id: string): Promise<{
        id: any;
        name: string;
        nativeName: string;
        description: string;
        position: number;
        price: number;
        coverImage: string;
        imageUrls: string[];
        category: any[];
        isContainAlcohol: boolean;
        modifiers: any[];
    }>;
    getPaginateMenuPOS(restaurantId: string, query: POSMenuPaginateDto): Promise<any>;
    getPaginateMenuWeb(query: MenuPaginateDto): Promise<any>;
    getMenuForWebById(id: string): Promise<{
        id: any;
        name: string;
        nativeName: string;
        description: string;
        position: number;
        price: number;
        coverImage: string;
        imageUrls: string[];
        category: any[];
        isContainAlcohol: boolean;
        modifiers: any[];
        isAvailable: boolean;
    }>;
    updateMenuPosition(payload: UpdateMenuPositionDto): Promise<{
        success: boolean;
    }>;
    getAllMenuPOS(restaurantId: string): Promise<any[]>;
}
