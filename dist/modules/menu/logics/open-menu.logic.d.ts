import { MenuCategoryOnlineLogic } from 'src/modules/menu-category/logics/menu-category-public-online-logic';
import { MenuCategoriesService } from 'src/modules/menu-category/services/menu-category.service';
import { MenuModifierService } from 'src/modules/menu-modifier/services/menu-modifier.service';
import { ModifierService } from 'src/modules/modifier/services/modifier.service';
import { OpenMenuPaginateDto } from '../dto/get-open-menu.dto';
import { MenuService } from '../services/menu.service';
export declare class OpenMenuLogic {
    private readonly menuCategoriesService;
    private readonly menuService;
    private readonly modifierService;
    private readonly menuModifierService;
    private readonly menuCategoryOnlineLogic;
    constructor(menuCategoriesService: MenuCategoriesService, menuService: MenuService, modifierService: ModifierService, menuModifierService: MenuModifierService, menuCategoryOnlineLogic: MenuCategoryOnlineLogic);
    getPaginateMenuApp(query: OpenMenuPaginateDto): Promise<any>;
    getMenuForAppById(id: string): Promise<{
        id: any;
        name: string;
        nativeName: string;
        description: string;
        position: number;
        price: number;
        coverImage: string;
        imageUrls: string[];
        category: any[];
        isContainAlcohol: boolean;
        modifiers: any[];
        isAvailable: boolean;
    }>;
}
