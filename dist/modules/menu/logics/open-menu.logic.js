"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenMenuLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const menu_category_public_online_logic_1 = require("../../menu-category/logics/menu-category-public-online-logic");
const menu_category_service_1 = require("../../menu-category/services/menu-category.service");
const menu_modifier_service_1 = require("../../menu-modifier/services/menu-modifier.service");
const modifier_service_1 = require("../../modifier/services/modifier.service");
const menu_service_1 = require("../services/menu.service");
let OpenMenuLogic = class OpenMenuLogic {
    constructor(menuCategoriesService, menuService, modifierService, menuModifierService, menuCategoryOnlineLogic) {
        this.menuCategoriesService = menuCategoriesService;
        this.menuService = menuService;
        this.modifierService = modifierService;
        this.menuModifierService = menuModifierService;
        this.menuCategoryOnlineLogic = menuCategoryOnlineLogic;
    }
    async getPaginateMenuApp(query) {
        const menuPaginate = await this.menuService.paginate(query.buildQuery(query.restaurantId), query, { status: 0, createdAt: 0, createdBy: 0, updatedBy: 0, updatedAt: 0 });
        if (!menuPaginate.docs)
            return menuPaginate;
        const availableCategory = await this.menuCategoryOnlineLogic.getAvailableStatusForCategory(query.restaurantId);
        const { docs } = menuPaginate;
        const results = [];
        for (const menu of docs) {
            const isAvailable = availableCategory.findIndex((item) => menu.category.findIndex((e) => e === item) > -1) > -1;
            const modifiers = await this.menuModifierService.getAll({
                menuId: menu.id,
                status: status_enum_1.StatusEnum.ACTIVE
            }, {
                _id: 0,
                id: '$_id',
                label: 1,
                abbreviation: 1,
                items: 1,
                type: 1,
                maxSelected: 1
            });
            results.push(Object.assign(Object.assign({}, menu.toObject()), { isAvailable,
                modifiers }));
        }
        return Object.assign(Object.assign({}, menuPaginate), { docs: results });
    }
    async getMenuForAppById(id) {
        const menu = await this.menuService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { status: 0, createdAt: 0, createdBy: 0, updatedBy: 0, updatedAt: 0 });
        if (!menu)
            throw new common_1.NotFoundException('Not found menu.');
        const category = await this.menuCategoriesService.getAll({ _id: { $in: menu.category }, isShow: true, status: status_enum_1.StatusEnum.ACTIVE }, { name: 1, isContainAlcohol: 1 });
        const availableCategory = await this.menuCategoryOnlineLogic.getAvailableStatusForCategory(`${menu.restaurantId}`);
        const isAvailable = availableCategory.findIndex((item) => menu.category.findIndex((e) => e === item) > -1) > -1;
        const modifiers = await this.menuModifierService.getAll({
            menuId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            label: 1,
            abbreviation: 1,
            items: 1,
            type: 1,
            maxSelected: 1
        });
        return {
            id: menu._id,
            name: menu.name,
            nativeName: menu.nativeName,
            description: menu.description,
            position: menu.position,
            price: menu.price,
            coverImage: menu.coverImage,
            imageUrls: menu.imageUrls,
            category,
            isContainAlcohol: category.length > 0
                ? category.some((item) => item.isContainAlcohol)
                : false,
            modifiers,
            isAvailable
        };
    }
};
OpenMenuLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [menu_category_service_1.MenuCategoriesService,
        menu_service_1.MenuService,
        modifier_service_1.ModifierService,
        menu_modifier_service_1.MenuModifierService,
        menu_category_public_online_logic_1.MenuCategoryOnlineLogic])
], OpenMenuLogic);
exports.OpenMenuLogic = OpenMenuLogic;
//# sourceMappingURL=open-menu.logic.js.map