"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const menu_categories_module_1 = require("../menu-category/menu-categories.module");
const menu_modifier_module_1 = require("../menu-modifier/menu-modifier.module");
const modifier_module_1 = require("../modifier/modifier.module");
const pos_menu_controller_1 = require("./controllers/pos-menu.controller");
const menu_controller_1 = require("./controllers/menu.controller");
const menu_public_online_controller_1 = require("./controllers/menu-public-online.controller");
const menu_logic_1 = require("./logics/menu.logic");
const menu_schema_1 = require("./schemas/menu.schema");
const menu_service_1 = require("./services/menu.service");
const open_menu_controller_1 = require("./controllers/open-menu.controller");
const open_menu_logic_1 = require("./logics/open-menu.logic");
const web_setting_module_1 = require("../web-setting/web-setting.module");
let MenuModule = class MenuModule {
};
MenuModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => menu_categories_module_1.MenuCategoriesModule),
            (0, common_1.forwardRef)(() => modifier_module_1.ModifierModule),
            (0, common_1.forwardRef)(() => menu_modifier_module_1.MenuModifierModule),
            (0, common_1.forwardRef)(() => web_setting_module_1.WebSettingModule),
            mongoose_1.MongooseModule.forFeature([{ name: 'menu', schema: menu_schema_1.MenuSchema }])
        ],
        providers: [menu_service_1.MenuService, menu_logic_1.MenuLogic, open_menu_logic_1.OpenMenuLogic],
        controllers: [
            menu_controller_1.MenuController,
            pos_menu_controller_1.POSMenuController,
            menu_public_online_controller_1.MenuOnlineController,
            open_menu_controller_1.OpenMenuController
        ],
        exports: [menu_service_1.MenuService, menu_logic_1.MenuLogic]
    })
], MenuModule);
exports.MenuModule = MenuModule;
//# sourceMappingURL=menu.module.js.map