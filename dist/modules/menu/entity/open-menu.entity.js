"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOpenMenuResponse = exports.PaginateOpenMenuResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const modifier_type_enum_1 = require("../../modifier/common/modifier-type.enum");
class ModifierItemObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierItemObject.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "native_name", void 0);
class ModifierObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierObject.prototype, "max_selected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(modifier_type_enum_1.ModifierTypeEnum) }),
    __metadata("design:type", String)
], ModifierObject.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ModifierItemObject] }),
    __metadata("design:type", Array)
], ModifierObject.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "id", void 0);
class OpenMenuPaginateObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OpenMenuPaginateObject.prototype, "note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OpenMenuPaginateObject.prototype, "cover_image", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], OpenMenuPaginateObject.prototype, "image_urls", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OpenMenuPaginateObject.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'array',
        items: {
            type: 'string'
        }
    }),
    __metadata("design:type", Array)
], OpenMenuPaginateObject.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OpenMenuPaginateObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OpenMenuPaginateObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OpenMenuPaginateObject.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OpenMenuPaginateObject.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OpenMenuPaginateObject.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OpenMenuPaginateObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], OpenMenuPaginateObject.prototype, "is_available", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], OpenMenuPaginateObject.prototype, "modifiers", void 0);
class CategoryObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CategoryObject.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryObject.prototype, "id", void 0);
class OpenMenuObject extends OpenMenuPaginateObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [CategoryObject] }),
    __metadata("design:type", Array)
], OpenMenuObject.prototype, "category", void 0);
class OpenMenuPaginate extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [OpenMenuPaginateObject],
        example: [
            {
                note: '',
                cover_image: null,
                image_urls: [],
                price: 13,
                category: ['63374377c64b8b0998892cb4'],
                name: 'Som Tum Thai',
                restaurant_id: '630eff5751c2eac55f52662c',
                description: 'test desc',
                native_name: 'test nativeName',
                position: 999,
                id: '633744a3c64b8b0998892cf1',
                is_available: false,
                modifiers: [
                    {
                        max_selected: 1,
                        type: 'REQUIRED',
                        items: [
                            {
                                name: 'No Spicy',
                                price: 0,
                                native_name: 'ไม่เผ็ด'
                            },
                            {
                                name: 'Mild',
                                price: 0,
                                native_name: 'เผ็ดน้อย'
                            },
                            {
                                name: 'Medium',
                                price: 0,
                                native_name: 'เผ็ดปานกลาง'
                            },
                            {
                                name: 'Hot',
                                price: 0,
                                native_name: 'เผ็ด'
                            },
                            {
                                name: 'Thai Hot',
                                price: 0,
                                native_name: 'เผ็ดมาก'
                            }
                        ],
                        label: 'Spicy Level',
                        abbreviation: 'S',
                        id: '633744a3c64b8b0998892cf5'
                    },
                    {
                        max_selected: 1,
                        type: 'REQUIRED',
                        items: [
                            {
                                name: 'Tofu',
                                price: 0,
                                native_name: 'Native Name Test'
                            },
                            {
                                name: 'Veggie',
                                price: 0,
                                native_name: 'Native Name Test'
                            },
                            {
                                name: 'Beef',
                                price: 3,
                                native_name: 'Native Name Test'
                            },
                            {
                                name: 'Shrimp',
                                price: 4,
                                native_name: 'Native Name Test'
                            },
                            {
                                name: 'Seafood',
                                price: 6,
                                native_name: 'Native Name Test'
                            },
                            {
                                name: 'Chicken',
                                price: 2,
                                native_name: 'Native Name Test'
                            },
                            {
                                name: 'Pork',
                                price: 2,
                                native_name: 'Native Name Test'
                            },
                            {
                                name: 'No Protein',
                                price: 0,
                                native_name: 'Native Name Test'
                            }
                        ],
                        label: 'Protein Choice',
                        abbreviation: 'P',
                        id: '633744a3c64b8b0998892cf4'
                    }
                ]
            }
        ]
    }),
    __metadata("design:type", Array)
], OpenMenuPaginate.prototype, "results", void 0);
class PaginateOpenMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: OpenMenuPaginate }),
    __metadata("design:type", OpenMenuPaginate)
], PaginateOpenMenuResponse.prototype, "data", void 0);
exports.PaginateOpenMenuResponse = PaginateOpenMenuResponse;
class GetOpenMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: OpenMenuObject,
        example: {
            id: '633742e5c64b8b0998892c83',
            name: 'Coconut Lemongrass Soup',
            native_name: 'test nativeName',
            description: 'test desc',
            position: 999,
            price: 15,
            cover_image: null,
            image_urls: [],
            category: [
                {
                    name: 'Soup',
                    is_contain_alcohol: false,
                    id: '6337412cc64b8b0998892c62'
                }
            ],
            is_contain_alcohol: false,
            modifiers: [],
            is_available: true
        }
    }),
    __metadata("design:type", OpenMenuObject)
], GetOpenMenuResponse.prototype, "data", void 0);
exports.GetOpenMenuResponse = GetOpenMenuResponse;
//# sourceMappingURL=open-menu.entity.js.map