import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class ModifierItemObject {
    name: string;
    price: number;
    native_name: string;
}
declare class ModifierObject {
    max_selected: number;
    type: string;
    items: ModifierItemObject[];
    label: string;
    abbreviation: string;
    id: string;
}
declare class OpenMenuPaginateObject {
    note: string;
    cover_image: string;
    image_urls: string[];
    price: number;
    category: string[] | CategoryObject[];
    name: string;
    restaurant_id: string;
    description: string;
    native_name: string;
    position: number;
    id: string;
    is_available: boolean;
    modifiers: ModifierObject[];
}
declare class CategoryObject {
    name: string;
    is_contain_alcohol: boolean;
    id: string;
}
declare class OpenMenuObject extends OpenMenuPaginateObject {
    category: CategoryObject[];
}
declare class OpenMenuPaginate extends PaginateResponseDto<OpenMenuPaginateObject[]> {
    results: OpenMenuPaginateObject[];
}
export declare class PaginateOpenMenuResponse extends ResponseDto<OpenMenuPaginate> {
    data: OpenMenuPaginate;
}
export declare class GetOpenMenuResponse extends ResponseDto<OpenMenuObject> {
    data: OpenMenuObject;
}
export {};
