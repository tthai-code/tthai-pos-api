import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
declare class ModifierItemFields {
    name: string;
    native_name: string;
    price: number;
}
declare class ModifierFields {
    max_selected: number;
    type: string;
    items: Array<ModifierItemFields>;
    abbreviation: string;
    label: string;
    id: string;
}
declare class CategoryFields {
    is_contain_alcohol: boolean;
    name: string;
    id: string;
}
declare class MenuFields {
    id: string;
    name: string;
    native_name: string;
    description: string;
    category: Array<CategoryFields>;
    position: number;
    price: number;
    is_contain_alcohol: boolean;
    cover_image: string;
    image_urls: Array<string>;
    modifiers: Array<ModifierFields>;
}
declare class OnlineMenuField extends MenuFields {
    is_available: boolean;
}
export declare class MenuFieldsWithTimestamps extends TimestampResponseDto {
    id: string;
    name: string;
    native_name: string;
    description: string;
    category: Array<string>;
    position: number;
    price: number;
    is_contain_alcohol: boolean;
    cover_image: string;
    image_urls: Array<string>;
    modifiers: Array<ModifierFields>;
}
export declare class POSMenuResponse extends ResponseDto<MenuFields> {
    data: MenuFields;
}
export declare class POSAllMenuResponse extends ResponseDto<MenuFields[]> {
    data: MenuFields[];
}
export declare class PaginatePOSMenuFields extends PaginateResponseDto<Array<MenuFieldsWithTimestamps>> {
    results: Array<MenuFieldsWithTimestamps>;
}
export declare class PaginatePOSMenuResponse extends ResponseDto<PaginatePOSMenuFields> {
    data: PaginatePOSMenuFields;
}
export declare class OnlineMenuResponse extends ResponseDto<OnlineMenuField> {
    data: OnlineMenuField;
}
export {};
