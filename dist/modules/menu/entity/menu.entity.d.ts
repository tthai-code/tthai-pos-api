import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
declare class MenuFields extends TimestampResponseDto {
    id: string;
    restaurant_id: string;
    name: string;
    native_name: string;
    description: string;
    category: Array<string>;
    position: number;
    price: number;
    cover_image: string;
    image_urls: Array<string>;
}
declare class ModifierItemObject {
    name: string;
    native_name: string;
    price: number;
}
declare class ModifierObject {
    max_selected: number;
    type: string;
    items: ModifierItemObject[];
    abbreviation: string;
    label: string;
    id: string;
}
declare class MenuFieldsWithModifier extends MenuFields {
    modifiers: ModifierObject[];
}
declare class OnlineMenuFieldsWithModifier extends MenuFieldsWithModifier {
    is_available: boolean;
}
declare class CategoryFields {
    is_contain_alcohol: boolean;
    name: string;
    id: string;
}
declare class GetMenuFields extends TimestampResponseDto {
    id: string;
    name: string;
    native_name: string;
    description: string;
    category: Array<CategoryFields>;
    position: number;
    price: number;
    cover_image: string;
    image_urls: Array<string>;
}
export declare class PaginateMenuResponse extends PaginateResponseDto<Array<MenuFieldsWithModifier>> {
    results: Array<MenuFieldsWithModifier>;
}
export declare class CreatedMenuResponse extends ResponseDto<MenuFields> {
    data: MenuFields;
}
export declare class DeletedMenuResponse extends ResponseDto<MenuFields> {
    data: MenuFields;
}
export declare class GetMenuResponse extends ResponseDto<GetMenuFields> {
    data: GetMenuFields;
}
declare class OnlineMenuPaginateField extends PaginateResponseDto<OnlineMenuFieldsWithModifier[]> {
    results: OnlineMenuFieldsWithModifier[];
}
export declare class OnlineMenuPaginateResponse extends ResponseDto<OnlineMenuPaginateField> {
    data: OnlineMenuPaginateField;
}
export {};
