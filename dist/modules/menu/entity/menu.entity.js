"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OnlineMenuPaginateResponse = exports.GetMenuResponse = exports.DeletedMenuResponse = exports.CreatedMenuResponse = exports.PaginateMenuResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
const modifier_type_enum_1 = require("../../modifier/common/modifier-type.enum");
class MenuFields extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], MenuFields.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], MenuFields.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], MenuFields.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "cover_image", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], MenuFields.prototype, "image_urls", void 0);
class ModifierItemObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemObject.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierItemObject.prototype, "price", void 0);
class ModifierObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierObject.prototype, "max_selected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(modifier_type_enum_1.ModifierTypeEnum) }),
    __metadata("design:type", String)
], ModifierObject.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ModifierItemObject] }),
    __metadata("design:type", Array)
], ModifierObject.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierObject.prototype, "id", void 0);
class MenuFieldsWithModifier extends MenuFields {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ModifierObject] }),
    __metadata("design:type", Array)
], MenuFieldsWithModifier.prototype, "modifiers", void 0);
class OnlineMenuFieldsWithModifier extends MenuFieldsWithModifier {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], OnlineMenuFieldsWithModifier.prototype, "is_available", void 0);
class CategoryFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CategoryFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryFields.prototype, "id", void 0);
class GetMenuFields extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetMenuFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetMenuFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetMenuFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetMenuFields.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [CategoryFields] }),
    __metadata("design:type", Array)
], GetMenuFields.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], GetMenuFields.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], GetMenuFields.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetMenuFields.prototype, "cover_image", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], GetMenuFields.prototype, "image_urls", void 0);
class PaginateMenuResponse extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [MenuFieldsWithModifier],
        example: [
            {
                status: 'active',
                cover_image: '',
                image_urls: [],
                modifiers: [
                    {
                        max_selected: 1,
                        type: 'REQUIRED',
                        items: [
                            {
                                name: 'Shrimp',
                                native_name: 'Native Name Test',
                                price: 2
                            },
                            {
                                name: 'No Protein',
                                native_name: 'Native Name Test',
                                price: 0
                            }
                        ],
                        abbreviation: 'P',
                        label: 'Choice of Protein',
                        id: '634d2a6c57a023457c8e4990'
                    }
                ],
                price: 40,
                description: 'Test Desc',
                position: 1,
                category: ['634d067bcc64f608779162f5'],
                native_name: 'เบียร์ช้าง',
                name: 'Chang Beer',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                created_at: '2022-10-17T10:11:56.416Z',
                updated_at: '2022-10-17T10:47:34.775Z',
                updated_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                created_by: {
                    username: 'corywong@mail.com',
                    id: '630e53ec9e21d871a49fb4f5'
                },
                id: '634d2a6c57a023457c8e498d'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateMenuResponse.prototype, "results", void 0);
exports.PaginateMenuResponse = PaginateMenuResponse;
class CreatedMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: MenuFields,
        example: {
            status: 'active',
            cover_image: '',
            image_urls: [],
            price: 40,
            description: 'Test Desc',
            position: 1,
            category: ['634d067bcc64f608779162f5'],
            native_name: 'เบียร์ช้าง',
            name: 'Chang Beer',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-10-17T10:11:56.416Z',
            updated_at: '2022-10-17T10:11:56.416Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '634d2a6c57a023457c8e498d'
        }
    }),
    __metadata("design:type", MenuFields)
], CreatedMenuResponse.prototype, "data", void 0);
exports.CreatedMenuResponse = CreatedMenuResponse;
class DeletedMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: MenuFields,
        example: {
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            status: 'deleted',
            cover_image: null,
            image_urls: [],
            price: 15,
            description: null,
            position: null,
            category: [
                {
                    name: 'A La Carte',
                    id: '630f2b664c861c248e7758f6'
                }
            ],
            native_name: null,
            name: 'Pad Krapow',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            updated_at: '2022-10-17T11:07:49.684Z',
            created_at: '2022-09-21T12:34:25.125Z',
            id: '632b04d1322209df557131ad'
        }
    }),
    __metadata("design:type", MenuFields)
], DeletedMenuResponse.prototype, "data", void 0);
exports.DeletedMenuResponse = DeletedMenuResponse;
class GetMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: GetMenuFields,
        example: {
            id: '634d2a6c57a023457c8e498d',
            name: 'Chang Beer',
            native_name: 'เบียร์ช้าง',
            description: 'Test Desc',
            position: 1,
            price: 40,
            cover_image: '',
            image_urls: [],
            category: [
                {
                    name: 'Best Seller',
                    id: '634d067bcc64f608779162f5'
                }
            ]
        }
    }),
    __metadata("design:type", GetMenuFields)
], GetMenuResponse.prototype, "data", void 0);
exports.GetMenuResponse = GetMenuResponse;
class OnlineMenuPaginateField extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [OnlineMenuFieldsWithModifier],
        example: [
            {
                note: '',
                cover_image: null,
                image_urls: [],
                price: 15,
                category: ['6337412cc64b8b0998892c62'],
                name: 'Coconut Lemongrass Soup',
                restaurant_id: '630eff5751c2eac55f52662c',
                description: 'test desc',
                native_name: 'test nativeName',
                position: 999,
                id: '633742e5c64b8b0998892c83',
                is_available: true,
                modifiers: []
            }
        ]
    }),
    __metadata("design:type", Array)
], OnlineMenuPaginateField.prototype, "results", void 0);
class OnlineMenuPaginateResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: OnlineMenuPaginateField }),
    __metadata("design:type", OnlineMenuPaginateField)
], OnlineMenuPaginateResponse.prototype, "data", void 0);
exports.OnlineMenuPaginateResponse = OnlineMenuPaginateResponse;
//# sourceMappingURL=menu.entity.js.map