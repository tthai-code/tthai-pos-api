"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OnlineMenuResponse = exports.PaginatePOSMenuResponse = exports.PaginatePOSMenuFields = exports.POSAllMenuResponse = exports.POSMenuResponse = exports.MenuFieldsWithTimestamps = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
const modifier_type_enum_1 = require("../../modifier/common/modifier-type.enum");
class ModifierItemFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierItemFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierItemFields.prototype, "price", void 0);
class ModifierFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierFields.prototype, "max_selected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(modifier_type_enum_1.ModifierTypeEnum) }),
    __metadata("design:type", String)
], ModifierFields.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ModifierItemFields] }),
    __metadata("design:type", Array)
], ModifierFields.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierFields.prototype, "id", void 0);
class CategoryFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CategoryFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CategoryFields.prototype, "id", void 0);
class MenuFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [CategoryFields] }),
    __metadata("design:type", Array)
], MenuFields.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], MenuFields.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], MenuFields.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], MenuFields.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFields.prototype, "cover_image", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], MenuFields.prototype, "image_urls", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ModifierFields] }),
    __metadata("design:type", Array)
], MenuFields.prototype, "modifiers", void 0);
class OnlineMenuField extends MenuFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], OnlineMenuField.prototype, "is_available", void 0);
class MenuFieldsWithTimestamps extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFieldsWithTimestamps.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFieldsWithTimestamps.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFieldsWithTimestamps.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFieldsWithTimestamps.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], MenuFieldsWithTimestamps.prototype, "category", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], MenuFieldsWithTimestamps.prototype, "position", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], MenuFieldsWithTimestamps.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], MenuFieldsWithTimestamps.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], MenuFieldsWithTimestamps.prototype, "cover_image", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], MenuFieldsWithTimestamps.prototype, "image_urls", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ModifierFields] }),
    __metadata("design:type", Array)
], MenuFieldsWithTimestamps.prototype, "modifiers", void 0);
exports.MenuFieldsWithTimestamps = MenuFieldsWithTimestamps;
class POSMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: MenuFields,
        example: {
            id: '6310685f062dfd651b6e5071',
            name: 'Spicy Wings of Heaven',
            native_name: 'ปีกไก่ทอด',
            description: 'Chicken wings tossed with house sauce topped with crispy basil.',
            position: 999,
            price: 12,
            cover_image: null,
            image_urls: [
                'https://storage.googleapis.com/tthai-pos-staging/upload/166456399070133572.jpg'
            ],
            category: [
                {
                    name: 'Appetizers',
                    is_contain_alcohol: false,
                    id: '6336bb21c64b8b0998892950'
                }
            ],
            is_contain_alcohol: false,
            modifiers: [
                {
                    max_selected: 1,
                    type: 'REQUIRED',
                    items: [
                        {
                            name: 'No Spicy',
                            price: 0,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Mild',
                            price: 0,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Medium',
                            price: 0,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Hot',
                            price: 0,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Thai Hot',
                            price: 0,
                            native_name: 'Native Name Test'
                        }
                    ],
                    abbreviation: 'P',
                    label: 'Spicy Level',
                    id: '63525eb44f459775fd5126ce'
                }
            ]
        }
    }),
    __metadata("design:type", MenuFields)
], POSMenuResponse.prototype, "data", void 0);
exports.POSMenuResponse = POSMenuResponse;
class POSAllMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [MenuFields],
        example: [
            {
                status: 'active',
                cover_image: 'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341660144217278.jpeg',
                image_urls: [
                    'https://tthai-pos-storage.s3.amazonaws.com/upload/staging/16733630954401957.jpeg'
                ],
                price: 10,
                description: 'Test Position',
                position: 4,
                category: [
                    {
                        id: '63bef3df75f22af7cdd40f8f',
                        name: 'Recommended',
                        is_contain_alcohol: false
                    }
                ],
                native_name: 'Error',
                name: 'Test Error',
                restaurant_id: '63984d6690416c27a7415915',
                created_at: '2022-12-17T04:04:40.657Z',
                updated_at: '2023-02-06T07:13:44.930Z',
                updated_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                created_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                id: '639d3fd8d248e2ea7245147c',
                modifiers: [
                    {
                        position: 999,
                        max_selected: 1,
                        type: 'REQUIRED',
                        items: [
                            {
                                name: 'Pork',
                                native_name: 'Pork',
                                price: 0
                            },
                            {
                                name: 'Chicken',
                                native_name: 'Chicken',
                                price: 0
                            }
                        ],
                        abbreviation: 'P',
                        label: 'Protein Choice',
                        id: '63a8035744e33943c3e37de4'
                    }
                ],
                is_contain_alcohol: false
            },
            {
                status: 'active',
                cover_image: 'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341812368128171.png',
                image_urls: [],
                price: 12,
                description: 'Error',
                position: 3,
                category: [
                    {
                        id: '63bef3df75f22af7cdd40f8f',
                        name: 'Recommended',
                        is_contain_alcohol: false
                    },
                    {
                        id: '63bff8382402d33518f0d428',
                        name: 'Test Cate',
                        is_contain_alcohol: false
                    }
                ],
                native_name: 'Test 3',
                name: 'TEst 3',
                restaurant_id: '63984d6690416c27a7415915',
                created_at: '2022-12-17T04:06:45.203Z',
                updated_at: '2023-02-06T07:13:44.930Z',
                updated_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                created_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                id: '639d4055f643c9055745b36f',
                modifiers: [
                    {
                        position: 1,
                        max_selected: 3,
                        type: 'REQUIRED',
                        items: [
                            {
                                name: 'Test1',
                                native_name: 'Test1',
                                price: 0
                            },
                            {
                                name: 'Test3',
                                native_name: 'Test3',
                                price: 0
                            },
                            {
                                name: 'Test2',
                                native_name: 'Test2',
                                price: 0
                            }
                        ],
                        abbreviation: 'Test',
                        label: 'Test Modifiers',
                        id: '63cecffb6c3735b0588bbc5c'
                    },
                    {
                        position: 999,
                        max_selected: 1,
                        type: 'REQUIRED',
                        items: [
                            {
                                name: 'Pork',
                                native_name: 'Pork',
                                price: 0
                            },
                            {
                                name: 'Chicken',
                                native_name: 'Chicken',
                                price: 0
                            }
                        ],
                        abbreviation: 'P',
                        label: 'Protein Choice',
                        id: '63ced04a6c3735b0588bbcb1'
                    },
                    {
                        position: 2,
                        max_selected: 1,
                        type: 'OPTIONAL',
                        items: [
                            {
                                name: 'Test',
                                native_name: 'Position',
                                price: 0
                            }
                        ],
                        abbreviation: 'Test',
                        label: 'Test Position',
                        id: '63cecfdb6c3735b0588bbc45'
                    },
                    {
                        position: 3,
                        max_selected: 5,
                        type: 'OPTIONAL',
                        items: [
                            {
                                name: 'POS 01',
                                native_name: 'POS 01',
                                price: 0
                            },
                            {
                                name: 'POS 02',
                                native_name: 'POS 02',
                                price: 0
                            }
                        ],
                        abbreviation: 'EX',
                        label: 'Extra',
                        id: '63cecfdb6c3735b0588bbc46'
                    },
                    {
                        position: 998,
                        max_selected: 1,
                        type: 'OPTIONAL',
                        items: [
                            {
                                name: 'Low',
                                native_name: 'Low',
                                price: 0
                            },
                            {
                                name: 'Med',
                                native_name: 'Med',
                                price: 0
                            },
                            {
                                name: 'High',
                                native_name: 'High',
                                price: 0
                            }
                        ],
                        abbreviation: 'S',
                        label: 'Spicy Level',
                        id: '63ced0246c3735b0588bbc74'
                    }
                ],
                is_contain_alcohol: false
            },
            {
                status: 'active',
                cover_image: null,
                image_urls: [],
                price: 10,
                description: 'TEst',
                position: 2,
                category: [
                    {
                        id: '63bef3df75f22af7cdd40f8f',
                        name: 'Recommended',
                        is_contain_alcohol: false
                    }
                ],
                native_name: 'Test',
                name: 'Test Position',
                restaurant_id: '63984d6690416c27a7415915',
                created_at: '2022-12-23T10:47:20.716Z',
                updated_at: '2023-02-06T07:13:44.930Z',
                updated_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                created_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                id: '63a587381ad2246f4c2dfc07',
                modifiers: [],
                is_contain_alcohol: false
            },
            {
                status: 'active',
                cover_image: 'https://tthai-pos-storage.s3.amazonaws.com/upload/staging/167484345986176682.png',
                image_urls: [],
                price: 0.01,
                description: '123213',
                position: 5,
                category: [
                    {
                        id: '63bef3df75f22af7cdd40f8f',
                        name: 'Recommended',
                        is_contain_alcohol: false
                    }
                ],
                native_name: 'Test',
                name: 'Test New Positon',
                restaurant_id: '63984d6690416c27a7415915',
                created_at: '2023-01-27T18:17:57.150Z',
                updated_at: '2023-02-06T07:13:44.930Z',
                updated_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                created_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                id: '63d415557cd459af1c03af61',
                modifiers: [
                    {
                        position: 2,
                        max_selected: 1,
                        type: 'OPTIONAL',
                        items: [
                            {
                                name: 'Test',
                                native_name: 'Position',
                                price: 0
                            }
                        ],
                        abbreviation: 'Test',
                        label: 'Test Position',
                        id: '63d415557cd459af1c03af64'
                    }
                ],
                is_contain_alcohol: false
            },
            {
                status: 'active',
                cover_image: null,
                image_urls: [],
                price: 0,
                description: null,
                position: 6,
                category: [],
                native_name: 'pos 5',
                name: 'Test pos 5',
                restaurant_id: '63984d6690416c27a7415915',
                created_at: '2023-01-27T18:18:35.848Z',
                updated_at: '2023-02-06T07:13:44.930Z',
                updated_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                created_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                id: '63d4157b72edd9b398138859',
                modifiers: [],
                is_contain_alcohol: false
            },
            {
                status: 'active',
                cover_image: 'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/production/167566694197439914.avif',
                image_urls: [],
                price: 1,
                description: null,
                position: 1,
                category: [
                    {
                        id: '63bef3df75f22af7cdd40f8f',
                        name: 'Recommended',
                        is_contain_alcohol: false
                    }
                ],
                native_name: 'เทส avif',
                name: 'avif test 01',
                restaurant_id: '63984d6690416c27a7415915',
                created_at: '2023-02-06T07:02:31.758Z',
                updated_at: '2023-02-06T07:13:44.930Z',
                updated_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                created_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                id: '63e0a6077205023612e25feb',
                modifiers: [
                    {
                        position: 3,
                        max_selected: 5,
                        type: 'OPTIONAL',
                        items: [
                            {
                                name: 'POS 01',
                                native_name: 'POS 01',
                                price: 0
                            },
                            {
                                name: 'POS 02',
                                native_name: 'POS 02',
                                price: 0
                            }
                        ],
                        abbreviation: 'EX',
                        label: 'Extra',
                        id: '63e0a6077205023612e25fee'
                    }
                ],
                is_contain_alcohol: false
            }
        ]
    }),
    __metadata("design:type", Array)
], POSAllMenuResponse.prototype, "data", void 0);
exports.POSAllMenuResponse = POSAllMenuResponse;
class PaginatePOSMenuFields extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [MenuFieldsWithTimestamps],
        example: [
            {
                status: 'active',
                cover_image: 'https://tthai-pos-storage.s3.amazonaws.com/upload/staging/167484345986176682.png',
                image_urls: [],
                price: 0.01,
                description: '123213',
                position: 4,
                category: [
                    {
                        id: '63bef3df75f22af7cdd40f8f',
                        name: 'Recommended',
                        is_contain_alcohol: false
                    }
                ],
                native_name: 'Test',
                name: 'Test New Positon',
                restaurant_id: '63984d6690416c27a7415915',
                created_at: '2023-01-27T18:17:57.150Z',
                updated_at: '2023-01-27T18:33:55.223Z',
                updated_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                created_by: {
                    username: 'test03@gmail.com',
                    id: '6398079c8f56875d327ef2f6'
                },
                id: '63d415557cd459af1c03af61',
                modifiers: [
                    {
                        position: 2,
                        max_selected: 1,
                        type: 'OPTIONAL',
                        items: [
                            {
                                name: 'Test',
                                native_name: 'Position',
                                price: 0
                            }
                        ],
                        abbreviation: 'Test',
                        label: 'Test Position',
                        id: '63d415557cd459af1c03af64'
                    }
                ],
                is_contain_alcohol: false
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginatePOSMenuFields.prototype, "results", void 0);
exports.PaginatePOSMenuFields = PaginatePOSMenuFields;
class PaginatePOSMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [PaginatePOSMenuFields]
    }),
    __metadata("design:type", PaginatePOSMenuFields)
], PaginatePOSMenuResponse.prototype, "data", void 0);
exports.PaginatePOSMenuResponse = PaginatePOSMenuResponse;
class OnlineMenuResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: OnlineMenuField,
        example: {
            id: '633744a3c64b8b0998892cf1',
            name: 'Som Tum Thai',
            native_name: 'test nativeName',
            description: 'test desc',
            position: 999,
            price: 13,
            cover_image: null,
            image_urls: [],
            category: [
                {
                    name: 'Salads',
                    is_contain_alcohol: false,
                    id: '63374377c64b8b0998892cb4'
                }
            ],
            is_contain_alcohol: false,
            modifiers: [
                {
                    max_selected: 1,
                    type: 'REQUIRED',
                    items: [
                        {
                            name: 'Tofu',
                            price: 0,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Veggie',
                            price: 0,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Beef',
                            price: 3,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Shrimp',
                            price: 4,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Seafood',
                            price: 6,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Chicken',
                            price: 2,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'Pork',
                            price: 2,
                            native_name: 'Native Name Test'
                        },
                        {
                            name: 'No Protein',
                            price: 0,
                            native_name: 'Native Name Test'
                        }
                    ],
                    label: 'Protein Choice',
                    abbreviation: 'P',
                    id: '633744a3c64b8b0998892cf4'
                },
                {
                    max_selected: 1,
                    type: 'REQUIRED',
                    items: [
                        {
                            name: 'No Spicy',
                            price: 0,
                            native_name: 'ไม่เผ็ด'
                        },
                        {
                            name: 'Mild',
                            price: 0,
                            native_name: 'เผ็ดน้อย'
                        },
                        {
                            name: 'Medium',
                            price: 0,
                            native_name: 'เผ็ดปานกลาง'
                        },
                        {
                            name: 'Hot',
                            price: 0,
                            native_name: 'เผ็ด'
                        },
                        {
                            name: 'Thai Hot',
                            price: 0,
                            native_name: 'เผ็ดมาก'
                        }
                    ],
                    label: 'Spicy Level',
                    abbreviation: 'S',
                    id: '633744a3c64b8b0998892cf5'
                }
            ],
            is_available: false
        }
    }),
    __metadata("design:type", OnlineMenuField)
], OnlineMenuResponse.prototype, "data", void 0);
exports.OnlineMenuResponse = OnlineMenuResponse;
//# sourceMappingURL=pos-menu.entity.js.map