/// <reference types="mongoose" />
export declare type BillItemsDocument = BillItemsFields & Document;
export declare class BillItemsFields {
    restaurantId: string;
    billId: string;
    description: string;
    quantity: number;
    amount: number;
}
export declare const BillItemsSchema: import("mongoose").Schema<import("mongoose").Document<BillItemsFields, any, any>, import("mongoose").Model<import("mongoose").Document<BillItemsFields, any, any>, any, any, any>, any, any>;
