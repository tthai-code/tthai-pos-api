import { Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type BillDocument = BillFields & Document;
export declare class BillItemsObjectField {
    description: string;
    quantity: number;
    amount: number;
}
export declare class BillFields {
    restaurantId: string;
    startedPeriod: Date;
    endedPeriod: Date;
    invoiceDate: Date;
    dueDate: Date;
    amountDue: number;
    items: BillItemsObjectField[];
    billStatus: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const BillSchema: import("mongoose").Schema<Document<BillFields, any, any>, import("mongoose").Model<Document<BillFields, any, any>, any, any, any>, any, any>;
