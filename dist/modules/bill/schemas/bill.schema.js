"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillSchema = exports.BillFields = exports.BillItemsObjectField = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const bill_status_enum_1 = require("../common/bill-status.enum");
let BillItemsObjectField = class BillItemsObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], BillItemsObjectField.prototype, "description", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 1 }),
    __metadata("design:type", Number)
], BillItemsObjectField.prototype, "quantity", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], BillItemsObjectField.prototype, "amount", void 0);
BillItemsObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: false, strict: true })
], BillItemsObjectField);
exports.BillItemsObjectField = BillItemsObjectField;
let BillFields = class BillFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], BillFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Date)
], BillFields.prototype, "startedPeriod", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Date)
], BillFields.prototype, "endedPeriod", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Date)
], BillFields.prototype, "invoiceDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], BillFields.prototype, "dueDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], BillFields.prototype, "amountDue", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], BillFields.prototype, "items", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: bill_status_enum_1.BillStatusEnum.UNPAID, enum: Object.values(bill_status_enum_1.BillStatusEnum) }),
    __metadata("design:type", String)
], BillFields.prototype, "billStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], BillFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], BillFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], BillFields.prototype, "createdBy", void 0);
BillFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'bills' })
], BillFields);
exports.BillFields = BillFields;
exports.BillSchema = mongoose_1.SchemaFactory.createForClass(BillFields);
exports.BillSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
exports.BillSchema.plugin(mongoosePaginate);
//# sourceMappingURL=bill.schema.js.map