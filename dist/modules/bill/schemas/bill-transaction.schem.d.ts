import { Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type BillTransactionDocument = BillTransactionField & Document;
export declare class BillTransactionField {
    restaurantId: string;
    billId: string;
    response: MongooseSchema.Types.Mixed;
    paymentStatus: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const BillTransactionsSchema: MongooseSchema<Document<BillTransactionField, any, any>, import("mongoose").Model<Document<BillTransactionField, any, any>, any, any, any>, any, any>;
