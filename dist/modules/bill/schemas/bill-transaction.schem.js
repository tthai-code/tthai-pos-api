"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillTransactionsSchema = exports.BillTransactionField = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const subscription_payment_status_enum_1 = require("../../subscription/common/subscription-payment-status.enum");
let BillTransactionField = class BillTransactionField {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], BillTransactionField.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], BillTransactionField.prototype, "billId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", mongoose_2.Schema.Types.Mixed)
], BillTransactionField.prototype, "response", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(subscription_payment_status_enum_1.SubscriptionPaymentStatus),
        default: subscription_payment_status_enum_1.SubscriptionPaymentStatus.PENDING
    }),
    __metadata("design:type", String)
], BillTransactionField.prototype, "paymentStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], BillTransactionField.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], BillTransactionField.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], BillTransactionField.prototype, "createdBy", void 0);
BillTransactionField = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'billTransactions' })
], BillTransactionField);
exports.BillTransactionField = BillTransactionField;
exports.BillTransactionsSchema = mongoose_1.SchemaFactory.createForClass(BillTransactionField);
exports.BillTransactionsSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
exports.BillTransactionsSchema.plugin(mongoosePaginate);
//# sourceMappingURL=bill-transaction.schem.js.map