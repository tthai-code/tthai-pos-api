import { ClientSession, Model } from 'mongoose';
import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { BillTransactionDocument } from '../schemas/bill-transaction.schem';
export declare class BillTransactionService {
    private readonly BillTransactionModel;
    private request;
    constructor(BillTransactionModel: PaginateModel<BillTransactionDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<BillTransactionDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<BillTransactionDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<BillTransactionDocument>;
    transactionCreateMany(payload: any, session: ClientSession): Promise<BillTransactionDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<BillTransactionDocument>;
    update(id: string, product: any): Promise<BillTransactionDocument>;
    updateOne(condition: any, payload: any): Promise<BillTransactionDocument>;
    updateMany(condition: any, payload: any): Promise<Array<any>>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<BillTransactionDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
    delete(id: string): Promise<BillTransactionDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<BillTransactionDocument>;
    paginate(query: any, queryParam: any, select?: any): Promise<PaginateResult<BillTransactionDocument>>;
}
