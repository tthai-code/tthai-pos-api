import { ClientSession, Model } from 'mongoose';
import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { AllBillingPaginateDto, BillingPaginateDto } from '../dto/get-billing.dto';
import { BillDocument } from '../schemas/bill.schema';
export declare class BillService {
    private readonly BillModel;
    private request;
    constructor(BillModel: PaginateModel<BillDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<BillDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<BillDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<BillDocument>;
    createMany(payload: any): Promise<any>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<BillDocument>;
    update(id: string, product: any): Promise<BillDocument>;
    updateOne(condition: any, payload: any): Promise<BillDocument>;
    updateMany(condition: any, payload: any): Promise<Array<any>>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<BillDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
    delete(id: string): Promise<BillDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<BillDocument>;
    paginate(query: any, queryParam: AllBillingPaginateDto | BillingPaginateDto, select?: any): PaginateResult<BillDocument>;
}
