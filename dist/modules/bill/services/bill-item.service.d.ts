import { ClientSession, Model } from 'mongoose';
import { BillItemsDocument } from '../schemas/bill-item.schema';
export declare class BillItemsService {
    private readonly BillItemsModel;
    private request;
    constructor(BillItemsModel: Model<BillItemsDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<BillItemsDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<BillItemsDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<BillItemsDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<BillItemsDocument>;
    update(id: string, product: any): Promise<BillItemsDocument>;
    updateOne(condition: any, payload: any): Promise<BillItemsDocument>;
    updateMany(condition: any, payload: any): Promise<Array<any>>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<BillItemsDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
    delete(id: string): Promise<BillItemsDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<BillItemsDocument>;
}
