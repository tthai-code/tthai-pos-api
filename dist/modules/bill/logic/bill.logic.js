"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs = require("dayjs");
const status_enum_1 = require("../../../common/enum/status.enum");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const order_service_1 = require("../../order/services/order.service");
const ach_bank_type_enum_1 = require("../../payment-gateway/common/ach-bank.type.enum");
const card_pointe_interface_1 = require("../../payment-gateway/interfaces/card-pointe.interface");
const bank_account_service_1 = require("../../payment-gateway/services/bank-account.service");
const payment_gateway_http_service_1 = require("../../payment-gateway/services/payment-gateway-http.service");
const payment_gateway_service_1 = require("../../payment-gateway/services/payment-gateway.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const subscription_payment_status_enum_1 = require("../../subscription/common/subscription-payment-status.enum");
const subscription_plan_enum_1 = require("../../subscription/common/subscription-plan.enum");
const subscription_service_1 = require("../../subscription/services/subscription.service");
const payment_method_enum_1 = require("../../transaction/common/payment-method.enum");
const payment_status_enum_1 = require("../../transaction/common/payment-status.enum");
const transaction_service_1 = require("../../transaction/services/transaction.service");
const bill_status_enum_1 = require("../common/bill-status.enum");
const bill_item_service_1 = require("../services/bill-item.service");
const bill_transaction_service_1 = require("../services/bill-transaction.service");
const bill_service_1 = require("../services/bill.service");
let BillLogic = class BillLogic {
    constructor(billService, billItemsService, billTransactionService, restaurantService, paymentGatewayService, bankAccountService, transactionService, subscriptionService, orderService, paymentGatewayHttpService) {
        this.billService = billService;
        this.billItemsService = billItemsService;
        this.billTransactionService = billTransactionService;
        this.restaurantService = restaurantService;
        this.paymentGatewayService = paymentGatewayService;
        this.bankAccountService = bankAccountService;
        this.transactionService = transactionService;
        this.subscriptionService = subscriptionService;
        this.orderService = orderService;
        this.paymentGatewayHttpService = paymentGatewayHttpService;
    }
    numberToFixed(number, digit) {
        return Number(number.toFixed(digit));
    }
    summaryTransactionItem(items, key) {
        return (items
            .map((item) => item[key])
            .reduce((sum, current) => sum + current, 0) || 0);
    }
    groupBy(array, key) {
        const groupByObject = array.reduce((r, v, i, a, k = v[key]) => ((r[k] || (r[k] = [])).push(v), r), {});
        const transform = Object.entries(groupByObject).map((e) => ({
            restaurantId: e[0],
            data: e[1]
        }));
        return transform;
    }
    async getAllRestaurantHavingBankAccount() {
        const restaurants = await this.restaurantService.getAll({
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const activeRestaurantId = restaurants.map((item) => item.id);
        const bankAccounts = await this.bankAccountService.getAll({
            restaurantId: { $in: activeRestaurantId },
            status: status_enum_1.StatusEnum.ACTIVE,
            bankAccountStatus: ach_bank_type_enum_1.BankAccountStatusEnum.ACTIVE
        });
        if (bankAccounts.length <= 0)
            return [];
        const restaurantIds = bankAccounts.map((item) => item.restaurantId);
        return restaurantIds;
    }
    async createMonthlyBill(createBill) {
        const restaurantIds = await this.getAllRestaurantHavingBankAccount();
        const subscription = await this.subscriptionService.getAll({
            restaurantId: { $in: restaurantIds },
            autoRenewAt: dayjs(createBill.invoiceDate).toDate(),
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const transaction = await this.transactionService.getAll({
            restaurantId: { $in: restaurantIds },
            paymentMethod: {
                $in: [payment_method_enum_1.PaymentMethodEnum.CREDIT, payment_method_enum_1.PaymentMethodEnum.DEBIT]
            },
            openDate: {
                $gte: dayjs(createBill.startedPeriod).toDate(),
                $lte: dayjs(createBill.endedPeriod).toDate()
            },
            paymentStatus: payment_status_enum_1.PaymentStatusEnum.PAID,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const posProcessingFee = await this.restaurantService.getAll({
            _id: { $in: restaurantIds },
            status: status_enum_1.StatusEnum.ACTIVE
        }, { posProcessingFee: 1 });
        const restaurantBillPayload = [];
        for (const restaurantId of restaurantIds) {
            const restaurantSubscription = subscription.filter((item) => `${item.restaurantId}` === `${restaurantId}`);
            const restaurantTransaction = transaction.filter((item) => `${item.restaurantId}` === `${restaurantId}`);
            const posProcessingFeeIndex = posProcessingFee.findIndex((item) => `${item._id}` === `${restaurantId}`);
            const { debitCard, creditCard } = posProcessingFee[posProcessingFeeIndex].posProcessingFee;
            const debitTransaction = restaurantTransaction.filter((transaction) => transaction.paymentMethod === payment_method_enum_1.PaymentMethodEnum.DEBIT);
            const creditTransaction = restaurantTransaction.filter((transaction) => transaction.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CREDIT);
            const debitSales = this.summaryTransactionItem(debitTransaction, 'totalPaid');
            const creditSales = this.summaryTransactionItem(creditTransaction, 'totalPaid');
            const calculatedDebitSales = this.numberToFixed((debitSales * debitCard) / 100, 2);
            const calculatedCreditSales = this.numberToFixed((creditSales * creditCard) / 100, 2);
            const calculatedSubscription = this.summaryTransactionItem(restaurantSubscription, 'amount');
            const amountDue = calculatedCreditSales + calculatedDebitSales + calculatedSubscription;
            const subscriptionItems = restaurantSubscription.map((item) => ({
                description: item.name,
                quantity: item.quantity,
                amount: item.amount
            }));
            const items = [
                {
                    description: 'Credit Card Sales',
                    quantity: creditSales,
                    amount: calculatedCreditSales
                },
                {
                    description: 'Debit Card Sales',
                    quantity: debitSales,
                    amount: calculatedDebitSales
                },
                ...subscriptionItems
            ];
            restaurantBillPayload.push({
                restaurantId,
                startedPeriod: dayjs(createBill.startedPeriod).toDate(),
                endedPeriod: dayjs(createBill.endedPeriod).toDate(),
                invoiceDate: dayjs(createBill.invoiceDate).toDate(),
                dueDate: dayjs(createBill.invoiceDate).add(2, 'day').toDate(),
                amountDue,
                items
            });
        }
        return await this.billService.createMany(restaurantBillPayload);
    }
    async payMonthlyBill(invoiceDate) {
        const bills = await this.billService.getAll({
            invoiceDate: dayjs(invoiceDate).toDate(),
            billStatus: bill_status_enum_1.BillStatusEnum.UNPAID,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const restaurantIds = bills.map((item) => item.restaurantId);
        const bankAccounts = await this.bankAccountService.getAll({
            restaurantId: { $in: restaurantIds },
            bankAccountStatus: ach_bank_type_enum_1.BankAccountStatusEnum.ACTIVE,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const recurringBills = [];
        for (const bill of bills) {
            const index = bankAccounts.findIndex((item) => `${item.restaurantId}` === bill.restaurantId);
            if (index !== -1) {
                const bankAccount = bankAccounts[index];
                const recurringObject = {
                    account: bankAccount.token,
                    accttype: bankAccount.accountType === ach_bank_type_enum_1.ACHBankAccountTypeEnum.SAVING
                        ? 'ESAV'
                        : 'ECHK',
                    amount: bill.amountDue.toFixed(2),
                    name: bankAccount.name
                };
                recurringBills.push(this.paymentGatewayHttpService.recurring(recurringObject));
            }
        }
        const paidBills = await Promise.all(recurringBills);
        const resultBillTransactions = [];
        for (let i = 0; i < paidBills.length; i++) {
            const { data } = paidBills[i];
            const bill = bills[i];
            const transactionObject = {
                restaurantId: bill.restaurantId,
                billId: bill.id,
                response: data,
                paymentStatus: subscription_payment_status_enum_1.SubscriptionPaymentStatus.PENDING
            };
            if (!['00', '000'].includes(data.respcode)) {
                transactionObject.paymentStatus = subscription_payment_status_enum_1.SubscriptionPaymentStatus.FAILURE;
            }
            resultBillTransactions.push(transactionObject);
        }
        const successBillIds = resultBillTransactions
            .filter((transaction) => transaction.paymentStatus === subscription_payment_status_enum_1.SubscriptionPaymentStatus.PENDING)
            .map((item) => item.billId);
        const successRestaurantIds = resultBillTransactions
            .filter((transaction) => transaction.paymentStatus === subscription_payment_status_enum_1.SubscriptionPaymentStatus.PENDING)
            .map((item) => item.restaurantId);
        const logic = async (session) => {
            const billTransactions = await this.billTransactionService.transactionCreateMany(resultBillTransactions, session);
            await this.billService.transactionUpdateMany({ _id: { $in: successBillIds } }, { billStatus: bill_status_enum_1.BillStatusEnum.PAID }, session);
            await this.subscriptionService.transactionUpdateMany({
                restaurantId: { $in: successRestaurantIds },
                paymentPlan: { $ne: subscription_plan_enum_1.SubscriptionPlanEnum.YEARLY },
                subscriptionStatus: subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE
            }, {
                lastRenewAt: dayjs().toDate(),
                autoRenewAt: dayjs(invoiceDate).add(1, 'month').toDate()
            }, session);
            await this.subscriptionService.transactionUpdateMany({
                restaurantId: { $in: successRestaurantIds },
                paymentPlan: subscription_plan_enum_1.SubscriptionPlanEnum.YEARLY,
                subscriptionStatus: subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE
            }, {
                lastRenewAt: dayjs().toDate(),
                autoRenewAt: dayjs(invoiceDate).add(1, 'year').toDate()
            }, session);
            return billTransactions;
        };
        return (0, mongoose_transaction_1.runTransaction)(logic);
    }
};
BillLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [bill_service_1.BillService,
        bill_item_service_1.BillItemsService,
        bill_transaction_service_1.BillTransactionService,
        restaurant_service_1.RestaurantService,
        payment_gateway_service_1.PaymentGatewayService,
        bank_account_service_1.BankAccountService,
        transaction_service_1.TransactionService,
        subscription_service_1.SubscriptionService,
        order_service_1.OrderService,
        payment_gateway_http_service_1.PaymentGatewayHttpService])
], BillLogic);
exports.BillLogic = BillLogic;
//# sourceMappingURL=bill.logic.js.map