"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminBillLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const subscription_payment_status_enum_1 = require("../../subscription/common/subscription-payment-status.enum");
const bill_transaction_service_1 = require("../services/bill-transaction.service");
const bill_service_1 = require("../services/bill.service");
let AdminBillLogic = class AdminBillLogic {
    constructor(restaurantService, billService, billTransactionService) {
        this.restaurantService = restaurantService;
        this.billService = billService;
        this.billTransactionService = billTransactionService;
    }
    async getAllBilling(query) {
        const restaurant = await this.restaurantService.getAll({
            $or: [
                {
                    legalBusinessName: { $regex: query.search, $options: 'i' }
                },
                {
                    dba: { $regex: query.search, $options: 'i' }
                }
            ],
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const restaurantIds = restaurant.map((item) => item.id);
        const billing = await this.billService.paginate(query.buildQuery(restaurantIds), query, { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 });
        const { docs } = billing;
        const results = [];
        for (const bill of docs) {
            const index = restaurant.findIndex((item) => `${item.id}` === `${bill.restaurantId}`);
            const restaurantName = restaurant[index].legalBusinessName;
            results.push(Object.assign(Object.assign({}, bill.toObject()), { restaurantName }));
        }
        return Object.assign(Object.assign({}, billing), { docs: results });
    }
    async getBillingById(id) {
        const billing = await this.billService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!billing) {
            throw new common_1.NotFoundException('Not found billing from id.');
        }
        const restaurant = await this.restaurantService.findOne({
            _id: billing.restaurantId
        });
        const transaction = await this.billTransactionService.findOne({
            billId: id,
            paymentStatus: { $ne: subscription_payment_status_enum_1.SubscriptionPaymentStatus.FAILURE }
        });
        const result = Object.assign(Object.assign({}, billing.toObject()), { restaurant: (restaurant === null || restaurant === void 0 ? void 0 : restaurant.toObject()) || null, transaction: (transaction === null || transaction === void 0 ? void 0 : transaction.toObject()) || null });
        return result;
    }
};
AdminBillLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        bill_service_1.BillService,
        bill_transaction_service_1.BillTransactionService])
], AdminBillLogic);
exports.AdminBillLogic = AdminBillLogic;
//# sourceMappingURL=admin-bill.logic.js.map