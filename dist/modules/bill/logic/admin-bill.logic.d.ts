/// <reference types="mongoose" />
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { AllBillingPaginateDto } from '../dto/get-billing.dto';
import { BillTransactionService } from '../services/bill-transaction.service';
import { BillService } from '../services/bill.service';
export declare class AdminBillLogic {
    private readonly restaurantService;
    private readonly billService;
    private readonly billTransactionService;
    constructor(restaurantService: RestaurantService, billService: BillService, billTransactionService: BillTransactionService);
    getAllBilling(query: AllBillingPaginateDto): Promise<any>;
    getBillingById(id: string): Promise<{
        restaurant: import("mongoose").LeanDocument<import("../../restaurant/schemas/restaurant.schema").RestaurantDocument>;
        transaction: import("mongoose").LeanDocument<import("../schemas/bill-transaction.schem").BillTransactionDocument>;
        items: import("mongoose").LeanDocument<import("../schemas/bill.schema").BillItemsObjectField>[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        restaurantId: string;
        invoiceDate: Date;
        startedPeriod: Date;
        endedPeriod: Date;
        dueDate: Date;
        amountDue: number;
        billStatus: string;
    }>;
}
