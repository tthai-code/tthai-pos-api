"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MonthlyBillController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const create_bill_dto_1 = require("../dto/create-bill.dto");
const pay_bill_dto_1 = require("../dto/pay-bill.dto");
const bill_logic_1 = require("../logic/bill.logic");
let MonthlyBillController = class MonthlyBillController {
    constructor(billLogic) {
        this.billLogic = billLogic;
    }
    async createMonthlyBill(payload) {
        return await this.billLogic.createMonthlyBill(payload);
    }
    async payMonthlyBill(payload) {
        return await this.billLogic.payMonthlyBill(payload.invoiceDate);
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({
        summary: 'Create Monthly Bill',
        description: 'use basic auth'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_bill_dto_1.CreateBillDto]),
    __metadata("design:returntype", Promise)
], MonthlyBillController.prototype, "createMonthlyBill", null);
__decorate([
    (0, common_1.Post)('/pay'),
    (0, swagger_1.ApiOperation)({ summary: 'Pay Monthly Bill', description: 'use basic auth' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [pay_bill_dto_1.PayMonthlyBillDto]),
    __metadata("design:returntype", Promise)
], MonthlyBillController.prototype, "payMonthlyBill", null);
MonthlyBillController = __decorate([
    (0, swagger_1.ApiBasicAuth)(),
    (0, swagger_1.ApiTags)('cron/bill'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    (0, common_1.Controller)('v1/cron/bill'),
    __metadata("design:paramtypes", [bill_logic_1.BillLogic])
], MonthlyBillController);
exports.MonthlyBillController = MonthlyBillController;
//# sourceMappingURL=monthly-bill.controller.js.map