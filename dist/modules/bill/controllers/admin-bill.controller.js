"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminBillController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const super_admin_auth_guard_1 = require("../../auth/guards/super-admin-auth.guard");
const get_billing_dto_1 = require("../dto/get-billing.dto");
const admin_bill_entity_1 = require("../entity/admin-bill.entity");
const admin_bill_logic_1 = require("../logic/admin-bill.logic");
const bill_service_1 = require("../services/bill.service");
let AdminBillController = class AdminBillController {
    constructor(billService, adminBillLogic) {
        this.billService = billService;
        this.adminBillLogic = adminBillLogic;
    }
    async getAllBilling(query) {
        return await this.adminBillLogic.getAllBilling(query);
    }
    async getBillRestaurant(query) {
        return await this.billService.paginate(query.buildQuery(), query, {
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0,
            status: 0
        });
    }
    async getBill(billId) {
        return await this.adminBillLogic.getBillingById(billId);
    }
};
__decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    (0, swagger_1.ApiTags)('admin/billing'),
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_bill_entity_1.PaginateAdminBillResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get All Billing for Super Admin',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_billing_dto_1.AllBillingPaginateDto]),
    __metadata("design:returntype", Promise)
], AdminBillController.prototype, "getAllBilling", null);
__decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    (0, swagger_1.ApiTags)('admin/restaurant'),
    (0, common_1.Get)('restaurant'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_bill_entity_1.PaginateBillResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Billing for Admin by Restaurant',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_billing_dto_1.BillingPaginateDto]),
    __metadata("design:returntype", Promise)
], AdminBillController.prototype, "getBillRestaurant", null);
__decorate([
    (0, swagger_1.ApiTags)('admin/billing'),
    (0, common_1.Get)('/:billId/billing'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_bill_entity_1.BillResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Billing By ID'
    }),
    __param(0, (0, common_1.Param)('billId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminBillController.prototype, "getBill", null);
AdminBillController = __decorate([
    (0, common_1.Controller)('v1/admin/bill'),
    __metadata("design:paramtypes", [bill_service_1.BillService,
        admin_bill_logic_1.AdminBillLogic])
], AdminBillController);
exports.AdminBillController = AdminBillController;
//# sourceMappingURL=admin-bill.controller.js.map