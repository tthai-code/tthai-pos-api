import { CreateBillDto } from '../dto/create-bill.dto';
import { PayMonthlyBillDto } from '../dto/pay-bill.dto';
import { BillLogic } from '../logic/bill.logic';
export declare class MonthlyBillController {
    private readonly billLogic;
    constructor(billLogic: BillLogic);
    createMonthlyBill(payload: CreateBillDto): Promise<any>;
    payMonthlyBill(payload: PayMonthlyBillDto): Promise<import("../schemas/bill-transaction.schem").BillTransactionDocument>;
}
