/// <reference types="mongoose" />
import { AllBillingPaginateDto, BillingPaginateDto } from '../dto/get-billing.dto';
import { AdminBillLogic } from '../logic/admin-bill.logic';
import { BillService } from '../services/bill.service';
export declare class AdminBillController {
    private readonly billService;
    private readonly adminBillLogic;
    constructor(billService: BillService, adminBillLogic: AdminBillLogic);
    getAllBilling(query: AllBillingPaginateDto): Promise<any>;
    getBillRestaurant(query: BillingPaginateDto): Promise<PaginateResult<import("../schemas/bill.schema").BillDocument>>;
    getBill(billId: string): Promise<{
        restaurant: import("mongoose").LeanDocument<import("../../restaurant/schemas/restaurant.schema").RestaurantDocument>;
        transaction: import("mongoose").LeanDocument<import("../schemas/bill-transaction.schem").BillTransactionDocument>;
        items: import("mongoose").LeanDocument<import("../schemas/bill.schema").BillItemsObjectField>[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        restaurantId: string;
        invoiceDate: Date;
        startedPeriod: Date;
        endedPeriod: Date;
        dueDate: Date;
        amountDue: number;
        billStatus: string;
    }>;
}
