export declare enum BillStatusEnum {
    PAID = "PAID",
    UNPAID = "UNPAID"
}
