"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillStatusEnum = void 0;
var BillStatusEnum;
(function (BillStatusEnum) {
    BillStatusEnum["PAID"] = "PAID";
    BillStatusEnum["UNPAID"] = "UNPAID";
})(BillStatusEnum = exports.BillStatusEnum || (exports.BillStatusEnum = {}));
//# sourceMappingURL=bill-status.enum.js.map