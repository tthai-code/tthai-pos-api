"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const order_module_1 = require("../order/order.module");
const payment_gateway_module_1 = require("../payment-gateway/payment-gateway.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const subscription_module_1 = require("../subscription/subscription.module");
const transaction_module_1 = require("../transaction/transaction.module");
const admin_bill_controller_1 = require("./controllers/admin-bill.controller");
const monthly_bill_controller_1 = require("./controllers/monthly-bill.controller");
const admin_bill_logic_1 = require("./logic/admin-bill.logic");
const bill_logic_1 = require("./logic/bill.logic");
const bill_item_schema_1 = require("./schemas/bill-item.schema");
const bill_transaction_schem_1 = require("./schemas/bill-transaction.schem");
const bill_schema_1 = require("./schemas/bill.schema");
const bill_item_service_1 = require("./services/bill-item.service");
const bill_transaction_service_1 = require("./services/bill-transaction.service");
const bill_service_1 = require("./services/bill.service");
let BillModule = class BillModule {
};
BillModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            (0, common_1.forwardRef)(() => payment_gateway_module_1.PaymentGatewayModule),
            (0, common_1.forwardRef)(() => transaction_module_1.TransactionModule),
            (0, common_1.forwardRef)(() => subscription_module_1.SubscriptionModule),
            (0, common_1.forwardRef)(() => order_module_1.OrderModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'bill', schema: bill_schema_1.BillSchema },
                { name: 'billItems', schema: bill_item_schema_1.BillItemsSchema },
                { name: 'billTransactions', schema: bill_transaction_schem_1.BillTransactionsSchema }
            ])
        ],
        providers: [
            bill_service_1.BillService,
            bill_item_service_1.BillItemsService,
            bill_transaction_service_1.BillTransactionService,
            bill_logic_1.BillLogic,
            admin_bill_logic_1.AdminBillLogic
        ],
        controllers: [monthly_bill_controller_1.MonthlyBillController, admin_bill_controller_1.AdminBillController],
        exports: []
    })
], BillModule);
exports.BillModule = BillModule;
//# sourceMappingURL=bill.module.js.map