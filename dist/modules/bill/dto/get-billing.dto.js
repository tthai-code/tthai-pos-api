"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillingPaginateDto = exports.AllBillingPaginateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const dayjs_1 = require("../../../plugins/dayjs");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
const status_enum_1 = require("../../../common/enum/status.enum");
class AllBillingPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'createdAt';
        this.sortOrder = 'desc';
        this.search = '';
    }
    buildQuery(restaurantIds) {
        const result = {
            invoiceDate: {
                $gte: (0, dayjs_1.default)(this.startDate).tz('Etc/GMT+5').startOf('day').toDate(),
                $lte: (0, dayjs_1.default)(this.endDate)
                    .add(1, 'day')
                    .tz('Etc/GMT+5')
                    .startOf('day')
                    .toDate()
            },
            restaurantId: { $in: restaurantIds },
            status: status_enum_1.StatusEnum.ACTIVE
        };
        console.log(result);
        if (!this.startDate || !this.endDate)
            delete result.invoiceDate;
        if (restaurantIds.length <= 0)
            delete result.restaurantId;
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AllBillingPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], AllBillingPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'iPad' }),
    __metadata("design:type", String)
], AllBillingPaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], AllBillingPaginateDto.prototype, "startDate", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], AllBillingPaginateDto.prototype, "endDate", void 0);
exports.AllBillingPaginateDto = AllBillingPaginateDto;
class BillingPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'invoiceDate';
        this.sortOrder = 'desc';
    }
    buildQuery() {
        const result = {
            restaurantId: this.restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], BillingPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], BillingPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '<restaurant-id>' }),
    __metadata("design:type", String)
], BillingPaginateDto.prototype, "restaurantId", void 0);
exports.BillingPaginateDto = BillingPaginateDto;
//# sourceMappingURL=get-billing.dto.js.map