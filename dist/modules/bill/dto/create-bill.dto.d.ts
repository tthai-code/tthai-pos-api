export declare class CreateBillDto {
    readonly invoiceDate: string | Date;
    readonly startedPeriod: string | Date;
    readonly endedPeriod: string | Date;
}
