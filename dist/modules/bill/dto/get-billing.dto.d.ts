import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
export declare class AllBillingPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly startDate: string;
    readonly endDate: string;
    buildQuery(restaurantIds: string[]): {
        invoiceDate: {
            $gte: Date;
            $lte: Date;
        };
        restaurantId: {
            $in: string[];
        };
        status: StatusEnum;
    };
}
export declare class BillingPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly restaurantId: string;
    buildQuery(): {
        restaurantId: string;
        status: StatusEnum;
    };
}
