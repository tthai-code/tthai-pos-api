import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class BillItemsObject {
    description: string;
    quantity: number;
    amount: number;
}
declare class BillingObject {
    bill_status: string;
    items: BillItemsObject[];
    amount_due: number;
    due_date: string;
    invoice_date: string;
    ended_period: string;
    started_period: string;
    restaurant_id: string;
    id: string;
}
declare class AdminBillingObject extends BillingObject {
    restaurant_name: string;
}
declare class PaginateBillObject extends PaginateResponseDto<BillingObject[]> {
    results: BillingObject[];
}
declare class PaginateAdminBillObject extends PaginateResponseDto<AdminBillingObject[]> {
    results: AdminBillingObject[];
}
export declare class PaginateBillResponse extends ResponseDto<PaginateBillObject> {
    data: PaginateBillObject;
}
export declare class PaginateAdminBillResponse extends ResponseDto<PaginateAdminBillObject> {
    data: PaginateAdminBillObject;
}
export declare class BillResponse extends ResponseDto<BillingObject> {
    data: BillingObject;
}
export {};
