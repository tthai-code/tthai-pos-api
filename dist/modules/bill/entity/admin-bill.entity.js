"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillResponse = exports.PaginateAdminBillResponse = exports.PaginateBillResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const bill_status_enum_1 = require("../common/bill-status.enum");
class BillItemsObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BillItemsObject.prototype, "description", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BillItemsObject.prototype, "quantity", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BillItemsObject.prototype, "amount", void 0);
class BillingObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(bill_status_enum_1.BillStatusEnum) }),
    __metadata("design:type", String)
], BillingObject.prototype, "bill_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [BillItemsObject] }),
    __metadata("design:type", Array)
], BillingObject.prototype, "items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], BillingObject.prototype, "amount_due", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ description: 'Same as Transferred Date' }),
    __metadata("design:type", String)
], BillingObject.prototype, "due_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BillingObject.prototype, "invoice_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BillingObject.prototype, "ended_period", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BillingObject.prototype, "started_period", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BillingObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], BillingObject.prototype, "id", void 0);
class AdminBillingObject extends BillingObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminBillingObject.prototype, "restaurant_name", void 0);
class PaginateBillObject extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [BillingObject],
        example: [
            {
                bill_status: 'UNPAID',
                items: [
                    {
                        description: 'Credit Card Sales',
                        quantity: 0,
                        amount: 0
                    },
                    {
                        description: 'Debit Card Sales',
                        quantity: 0,
                        amount: 0
                    },
                    {
                        description: 'More iPad',
                        quantity: 2,
                        amount: 40
                    },
                    {
                        description: 'test package 2.1',
                        quantity: 1,
                        amount: 0
                    },
                    {
                        description: 'iPad',
                        quantity: 1,
                        amount: 0
                    }
                ],
                amount_due: 40,
                due_date: null,
                invoice_date: '2022-11-30T17:00:00.000Z',
                ended_period: '2022-11-30T16:59:59.999Z',
                started_period: '2022-10-31T17:00:00.000Z',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                id: '6392f2cf9aef71b576bcb5bb'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateBillObject.prototype, "results", void 0);
class PaginateAdminBillObject extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [BillingObject],
        example: [
            {
                bill_status: 'UNPAID',
                items: [
                    {
                        description: 'Credit Card Sales',
                        quantity: 0,
                        amount: 0
                    },
                    {
                        description: 'Debit Card Sales',
                        quantity: 0,
                        amount: 0
                    },
                    {
                        description: 'More iPad',
                        quantity: 2,
                        amount: 40
                    },
                    {
                        description: 'test package 2.1',
                        quantity: 1,
                        amount: 0
                    },
                    {
                        description: 'iPad',
                        quantity: 1,
                        amount: 0
                    }
                ],
                amount_due: 40,
                due_date: null,
                invoice_date: '2022-11-30T17:00:00.000Z',
                ended_period: '2022-11-30T16:59:59.999Z',
                started_period: '2022-10-31T17:00:00.000Z',
                restaurant_id: '630e55a0d9c30fd7cdcb424b',
                restaurant_name: 'Holy Beef',
                id: '6392f2cf9aef71b576bcb5bb'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateAdminBillObject.prototype, "results", void 0);
class PaginateBillResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: PaginateBillObject }),
    __metadata("design:type", PaginateBillObject)
], PaginateBillResponse.prototype, "data", void 0);
exports.PaginateBillResponse = PaginateBillResponse;
class PaginateAdminBillResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: PaginateAdminBillObject }),
    __metadata("design:type", PaginateAdminBillObject)
], PaginateAdminBillResponse.prototype, "data", void 0);
exports.PaginateAdminBillResponse = PaginateAdminBillResponse;
class BillResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: BillingObject,
        example: {
            bill_status: 'UNPAID',
            items: [
                {
                    description: 'Credit Card Sales',
                    quantity: 0,
                    amount: 0
                },
                {
                    description: 'Debit Card Sales',
                    quantity: 0,
                    amount: 0
                },
                {
                    description: 'More iPad',
                    quantity: 2,
                    amount: 40
                },
                {
                    description: 'test package 2.1',
                    quantity: 1,
                    amount: 0
                },
                {
                    description: 'iPad',
                    quantity: 1,
                    amount: 0
                }
            ],
            amount_due: 40,
            due_date: null,
            invoice_date: '2022-11-30T17:00:00.000Z',
            ended_period: '2022-11-30T16:59:59.999Z',
            started_period: '2022-10-31T17:00:00.000Z',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            id: '6392f2cf9aef71b576bcb5bb'
        }
    }),
    __metadata("design:type", BillingObject)
], BillResponse.prototype, "data", void 0);
exports.BillResponse = BillResponse;
//# sourceMappingURL=admin-bill.entity.js.map