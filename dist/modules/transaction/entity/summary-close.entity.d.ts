import { ResponseDto } from 'src/common/entity/response.entity';
declare class WebBaseSummaryByPaymentMethodObject {
    label: string;
    amount: number;
}
declare class SummaryByPaymentMethodObject {
    cash: number;
    credit_card: number;
    debit_card: number;
    others: number;
}
declare class SummaryCloseOfDayObject {
    total_transaction: number;
    total_collected: number;
    net_sales: number;
    summary_by_payment_method: SummaryByPaymentMethodObject;
}
declare class WebBaseSummaryCloseOfDayObject {
    total_transaction: number;
    total_collected: number;
    net_sales: number;
    summary_by_payment_method: WebBaseSummaryByPaymentMethodObject[];
}
export declare class SummaryCloseOfDayResponse extends ResponseDto<SummaryCloseOfDayObject> {
    data: SummaryCloseOfDayObject;
}
export declare class WebBaseSummaryCloseOfDayResponse extends ResponseDto<WebBaseSummaryCloseOfDayObject> {
    data: WebBaseSummaryCloseOfDayObject;
}
export {};
