"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginateTransactionResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
const item_status_enum_1 = require("../../order/enum/item-status.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const card_type_enum_1 = require("../common/card-type.enum");
const payment_method_enum_1 = require("../common/payment-method.enum");
const payment_status_enum_1 = require("../common/payment-status.enum");
const split_method_enum_1 = require("../common/split-method.enum");
const transaction_status_enum_1 = require("../common/transaction-status.enum");
class ModifierField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierField.prototype, "price", void 0);
class ItemField {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(item_status_enum_1.ItemStatusEnum) }),
    __metadata("design:type", String)
], ItemField.prototype, "item_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemField.prototype, "amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemField.prototype, "unit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], ItemField.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], ItemField.prototype, "modifiers", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemField.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "menu_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "porder_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "id", void 0);
class CustomerField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerField.prototype, "tel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerField.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerField.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerField.prototype, "id", void 0);
class TransactionField extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Object)
], TransactionField.prototype, "response", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(transaction_status_enum_1.TransactionStatusEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "transaction_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "total_paid", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "tips", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "paid_amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(payment_status_enum_1.PaymentStatusEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "payment_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "close_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "open_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "ret_ref", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "paid_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "card_last_four", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(payment_method_enum_1.PaymentMethodEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "payment_method", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(card_type_enum_1.CardTypeEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "card_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "split_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(split_method_enum_1.SplitMethodEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "split_method", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "convenience_fee", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "subtotal", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], TransactionField.prototype, "summary_items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", CustomerField)
], TransactionField.prototype, "customer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(order_type_enum_1.OrderTypeEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "porder_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "txn", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "ticket_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], TransactionField.prototype, "is_void_before_transaction", void 0);
class TransactionObjectResponse extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: 700 }),
    __metadata("design:type", Number)
], TransactionObjectResponse.prototype, "totalCollected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 680 }),
    __metadata("design:type", Number)
], TransactionObjectResponse.prototype, "netSales", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [TransactionField],
        example: {
            status: 'active',
            response: {
                amount: '5.70',
                resptext: 'Approval',
                order_id: 'TESTAMEX01',
                porder_id: 'TESTAMEX01',
                commcard: 'Y',
                cvvresp: 'P',
                respcode: '000',
                batchid: '118',
                avsresp: 'Y',
                entrymode: 'Keyed',
                merchid: '820000003069',
                token: '9472658069221443',
                authcode: 'PPS009',
                respproc: 'RPCT',
                bintype: 'Corp',
                expiry: '1223',
                retref: '314068455663',
                respstat: 'A',
                account: '9472658069221443'
            },
            transaction_status: 'OPEN',
            total_paid: 570,
            tips: 10,
            paid_amount: 560,
            payment_status: 'PAID',
            close_date: null,
            open_date: '2022-11-10T20:19:35.565Z',
            ret_ref: '312573439445',
            paid_by: 'CC TEST',
            card_last_four: '5454',
            payment_method: 'CREDIT',
            card_type: 'MASTERCARD',
            split_by: 1,
            split_method: 'NO_SPLIT',
            total: 560,
            convenience_fee: 0,
            alcohol_tax: 0,
            tax: 0,
            service_charge: 0,
            discount: 0,
            subtotal: 560,
            summary_items: [
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 80,
                    unit: 2,
                    is_contain_alcohol: true,
                    modifiers: [
                        {
                            id: '634d2a6c57a023457c8e4990',
                            label: 'Choice of Protein',
                            abbreviation: 'P',
                            name: 'No Protein',
                            native_name: 'Native Name Test',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Chang Beer',
                    menu_id: '634d2a6c57a023457c8e498d',
                    order_id: 'HS9SqQB2',
                    id: '635ef4cf6813f53e4a6ecd2a'
                },
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 80,
                    unit: 2,
                    cover_image: '',
                    is_contain_alcohol: false,
                    modifiers: [
                        {
                            id: '630fcd08b6fb5ee3bab46885',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Chicken',
                            native_name: 'Chicken',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Pad Thai',
                    menu_id: '630fc086af525d6ed9fc0d5d',
                    order_id: 'HS9SqQB2',
                    id: '635f7d600c131551da4c1b7e'
                },
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 80,
                    unit: 2,
                    cover_image: '',
                    is_contain_alcohol: false,
                    modifiers: [
                        {
                            id: '630fcd08b6fb5ee3bab46885',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Chicken',
                            native_name: 'Chicken',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Pad Thai',
                    menu_id: '630fc086af525d6ed9fc0d5d',
                    order_id: 'HS9SqQB2',
                    id: '635f83ed4b2d43f09a5d0cb4'
                },
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 80,
                    unit: 2,
                    cover_image: '',
                    is_contain_alcohol: false,
                    modifiers: [
                        {
                            id: '630fcd08b6fb5ee3bab46885',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Pork',
                            native_name: 'Pork',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Pad Thai',
                    menu_id: '630fc086af525d6ed9fc0d5d',
                    order_id: 'HS9SqQB2',
                    id: '635fc1b31b42e31b8c3c5b8a'
                },
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 80,
                    unit: 2,
                    cover_image: '',
                    is_contain_alcohol: false,
                    modifiers: [
                        {
                            id: '630fcd08b6fb5ee3bab46885',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Chicken',
                            native_name: 'Chicken',
                            price: 0
                        },
                        {
                            id: '630fcd08b6fb5ee3bab46885',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Pork',
                            native_name: 'Pork',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Pad Thai',
                    menu_id: '630fc086af525d6ed9fc0d5d',
                    order_id: 'HS9SqQB2',
                    id: '635fc1e11b42e31b8c3c5b95'
                },
                {
                    item_status: 'IN_PROGRESS',
                    note: '',
                    amount: 80,
                    unit: 2,
                    cover_image: '',
                    is_contain_alcohol: false,
                    modifiers: [
                        {
                            id: '630fcd08b6fb5ee3bab46885',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Pork',
                            native_name: 'Pork',
                            price: 0
                        },
                        {
                            id: '630fcd08b6fb5ee3bab46885',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Chicken',
                            native_name: 'Chicken',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Pad Thai',
                    menu_id: '630fc086af525d6ed9fc0d5d',
                    order_id: 'HS9SqQB2',
                    id: '635fc1eb1b42e31b8c3c5b9e'
                },
                {
                    item_status: 'READY',
                    note: '',
                    amount: 80,
                    unit: 2,
                    cover_image: '',
                    is_contain_alcohol: false,
                    modifiers: [
                        {
                            id: '630fcd08b6fb5ee3bab46885',
                            label: 'Protein Choices',
                            abbreviation: 'P',
                            name: 'Chicken',
                            native_name: 'Chicken',
                            price: 0
                        }
                    ],
                    price: 40,
                    native_name: 'test nativeName',
                    name: 'Pad Thai',
                    menu_id: '630fc086af525d6ed9fc0d5d',
                    order_id: 'HS9SqQB2',
                    id: '63601f35ee84c9491202f524'
                }
            ],
            customer: {
                tel: '',
                last_name: '',
                first_name: 'Guest'
            },
            order_type: 'DINE_IN',
            order_id: 'HS9SqQB2',
            ticket_no: '6',
            is_void_before_transaction: false,
            txn: '1',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-11-10T20:28:24.513Z',
            updated_at: '2022-11-10T20:28:24.513Z',
            updated_by: {
                username: 'Plini',
                id: '630f1c078653105bf1478b82'
            },
            created_by: {
                username: 'Plini',
                id: '630f1c078653105bf1478b82'
            },
            id: '636d5ee878fb39ea5dd286d7'
        }
    }),
    __metadata("design:type", Array)
], TransactionObjectResponse.prototype, "results", void 0);
class PaginateTransactionResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: TransactionObjectResponse }),
    __metadata("design:type", TransactionObjectResponse)
], PaginateTransactionResponse.prototype, "data", void 0);
exports.PaginateTransactionResponse = PaginateTransactionResponse;
//# sourceMappingURL=paginate-transaction.entity.js.map