import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
declare class ModifierField {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    native_name: string;
    price: number;
}
declare class ItemField {
    item_status: string;
    note: string;
    amount: number;
    unit: number;
    is_contain_alcohol: boolean;
    modifiers: ModifierField[];
    price: number;
    native_name: string;
    name: string;
    menu_id: string;
    order_id: string;
    porder_id: string;
    id: string;
}
declare class CustomerField {
    tel: string;
    last_name: string;
    first_name: string;
    id: string;
}
declare class TransactionField extends TimestampResponseDto {
    response: any;
    transaction_status: string;
    total_paid: number;
    tips: number;
    paid_amount: number;
    payment_status: string;
    close_date: string;
    open_date: string;
    ret_ref: string;
    paid_by: string;
    card_last_four: string;
    payment_method: string;
    card_type: string;
    total_split_items: number;
    split_items: ItemField[];
    split_by: number;
    split_method: string;
    total: number;
    convenience_fee: number;
    alcohol_tax: number;
    tax: number;
    service_charge: number;
    discount: number;
    subtotal: number;
    summary_items: ItemField[];
    customer: CustomerField;
    order_type: string;
    order_id: string;
    porder_id: string;
    txn: string;
    is_void_before_transaction: boolean;
    restaurant_id: string;
    id: string;
}
export declare class CreateTransactionResponse extends ResponseDto<TransactionField> {
    data: TransactionField;
}
export {};
