"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CloseOfDayStatusResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class CloseOfDayStatusObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CloseOfDayStatusObject.prototype, "is_closed", void 0);
class CloseOfDayStatusResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CloseOfDayStatusObject,
        example: {
            is_closed: false
        }
    }),
    __metadata("design:type", CloseOfDayStatusObject)
], CloseOfDayStatusResponse.prototype, "data", void 0);
exports.CloseOfDayStatusResponse = CloseOfDayStatusResponse;
//# sourceMappingURL=pos-close-of-day-status.entity.js.map