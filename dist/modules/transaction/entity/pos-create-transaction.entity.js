"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTransactionResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
const item_status_enum_1 = require("../../order/enum/item-status.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const card_type_enum_1 = require("../common/card-type.enum");
const payment_method_enum_1 = require("../common/payment-method.enum");
const payment_status_enum_1 = require("../common/payment-status.enum");
const split_method_enum_1 = require("../common/split-method.enum");
const transaction_status_enum_1 = require("../common/transaction-status.enum");
class ModifierField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifierField.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifierField.prototype, "price", void 0);
class ItemField {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(item_status_enum_1.ItemStatusEnum) }),
    __metadata("design:type", String)
], ItemField.prototype, "item_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemField.prototype, "amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemField.prototype, "unit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], ItemField.prototype, "is_contain_alcohol", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], ItemField.prototype, "modifiers", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemField.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "menu_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "porder_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemField.prototype, "id", void 0);
class CustomerField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerField.prototype, "tel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerField.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerField.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerField.prototype, "id", void 0);
class TransactionField extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Object)
], TransactionField.prototype, "response", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(transaction_status_enum_1.TransactionStatusEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "transaction_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "total_paid", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "tips", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "paid_amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(payment_status_enum_1.PaymentStatusEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "payment_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "close_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "open_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "ret_ref", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "paid_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "card_last_four", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(payment_method_enum_1.PaymentMethodEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "payment_method", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(card_type_enum_1.CardTypeEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "card_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "total_split_items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ItemField] }),
    __metadata("design:type", Array)
], TransactionField.prototype, "split_items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "split_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(split_method_enum_1.SplitMethodEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "split_method", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "convenience_fee", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TransactionField.prototype, "subtotal", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ItemField] }),
    __metadata("design:type", Array)
], TransactionField.prototype, "summary_items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", CustomerField)
], TransactionField.prototype, "customer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(order_type_enum_1.OrderTypeEnum) }),
    __metadata("design:type", String)
], TransactionField.prototype, "order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "order_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "porder_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "txn", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], TransactionField.prototype, "is_void_before_transaction", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TransactionField.prototype, "id", void 0);
class CreateTransactionResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: TransactionField,
        example: {
            status: 'active',
            note: ' ',
            signature: null,
            delivery_source: null,
            refunded_reason: '',
            refunded_amount: 0,
            refunded_items: [],
            response: null,
            transaction_status: 'OPEN',
            total_paid: 24.2,
            tips: 10,
            paid_amount: 14.2,
            payment_status: 'PAID',
            close_date: null,
            open_date: '2023-01-24T05:15:19.926Z',
            batch_id: null,
            ret_ref: null,
            paid_by: 'GUEST',
            card_last_four: null,
            payment_method: 'CASH',
            card_type: null,
            total_split_items: 12,
            split_items: [
                {
                    menu_id: '639d4055f643c9055745b36f',
                    name: 'TEst 3',
                    native_name: 'Test 3',
                    price: 12,
                    modifiers: [
                        {
                            abbreviation: 'Test',
                            id: '63cecffb6c3735b0588bbc5c',
                            label: 'Test Modifiers',
                            native_name: 'Test3',
                            name: 'Test3',
                            price: 0
                        },
                        {
                            abbreviation: 'P',
                            id: '63ced04a6c3735b0588bbcb1',
                            label: 'Protein Choice',
                            native_name: 'Chicken',
                            name: 'Chicken',
                            price: 0
                        }
                    ],
                    is_contain_alcohol: false,
                    cover_image: 'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341812368128171.png',
                    unit: 1,
                    amount: 12,
                    note: ''
                }
            ],
            split_by: 2,
            split_method: 'BY_ITEMS',
            total: 26.4,
            convenience_fee: 0,
            alcohol_tax: 0,
            tax: 2.2,
            service_charge: 2.2,
            discount: 0,
            subtotal: 22,
            summary_items: [
                {
                    menu_id: '639d3fd8d248e2ea7245147c',
                    name: 'Test Error',
                    native_name: 'Error',
                    price: 10,
                    modifiers: [
                        {
                            abbreviation: 'P',
                            id: '63a8035744e33943c3e37de4',
                            label: 'Protein Choice',
                            native_name: 'Pork',
                            name: 'Pork',
                            price: 0
                        }
                    ],
                    is_contain_alcohol: false,
                    cover_image: 'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341660144217278.jpeg',
                    unit: 1,
                    amount: 10,
                    note: ''
                },
                {
                    menu_id: '639d4055f643c9055745b36f',
                    name: 'TEst 3',
                    native_name: 'Test 3',
                    price: 12,
                    modifiers: [
                        {
                            abbreviation: 'Test',
                            id: '63cecffb6c3735b0588bbc5c',
                            label: 'Test Modifiers',
                            native_name: 'Test3',
                            name: 'Test3',
                            price: 0
                        },
                        {
                            abbreviation: 'P',
                            id: '63ced04a6c3735b0588bbcb1',
                            label: 'Protein Choice',
                            native_name: 'Chicken',
                            name: 'Chicken',
                            price: 0
                        }
                    ],
                    is_contain_alcohol: false,
                    cover_image: 'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/staging/167341812368128171.png',
                    unit: 1,
                    amount: 12,
                    note: ''
                }
            ],
            customer: {
                tel: '',
                last_name: '',
                first_name: 'Guest'
            },
            order_type: 'TO_GO',
            order_id: '2MmQ5yTY',
            txn: '001',
            is_void_before_transaction: false,
            restaurant_id: '63984d6690416c27a7415915',
            created_at: '2023-01-24T05:15:38.005Z',
            updated_at: '2023-01-24T05:15:38.005Z',
            updated_by: {
                username: 'Test',
                id: '63a03f43014de42db475bba9'
            },
            created_by: {
                username: 'Test',
                id: '63a03f43014de42db475bba9'
            },
            id: '63cf6979436eeccc39062a1c'
        }
    }),
    __metadata("design:type", TransactionField)
], CreateTransactionResponse.prototype, "data", void 0);
exports.CreateTransactionResponse = CreateTransactionResponse;
//# sourceMappingURL=pos-create-transaction.entity.js.map