import { ResponseDto } from 'src/common/entity/response.entity';
declare class CloseOfDayStatusObject {
    is_closed: boolean;
}
export declare class CloseOfDayStatusResponse extends ResponseDto<CloseOfDayStatusObject> {
    data: CloseOfDayStatusObject;
}
export {};
