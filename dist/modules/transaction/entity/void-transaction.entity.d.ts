import { ResponseDto } from 'src/common/entity/response.entity';
declare class SuccessResponse {
    success: boolean;
}
export declare class VoidTransactionResponse extends ResponseDto<SuccessResponse> {
    data: SuccessResponse;
}
export {};
