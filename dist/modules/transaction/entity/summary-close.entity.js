"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebBaseSummaryCloseOfDayResponse = exports.SummaryCloseOfDayResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class WebBaseSummaryByPaymentMethodObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebBaseSummaryByPaymentMethodObject.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WebBaseSummaryByPaymentMethodObject.prototype, "amount", void 0);
class SummaryByPaymentMethodObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryByPaymentMethodObject.prototype, "cash", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryByPaymentMethodObject.prototype, "credit_card", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryByPaymentMethodObject.prototype, "debit_card", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryByPaymentMethodObject.prototype, "others", void 0);
class SummaryCloseOfDayObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryCloseOfDayObject.prototype, "total_transaction", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryCloseOfDayObject.prototype, "total_collected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryCloseOfDayObject.prototype, "net_sales", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: SummaryByPaymentMethodObject }),
    __metadata("design:type", SummaryByPaymentMethodObject)
], SummaryCloseOfDayObject.prototype, "summary_by_payment_method", void 0);
class WebBaseSummaryCloseOfDayObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WebBaseSummaryCloseOfDayObject.prototype, "total_transaction", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WebBaseSummaryCloseOfDayObject.prototype, "total_collected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WebBaseSummaryCloseOfDayObject.prototype, "net_sales", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [WebBaseSummaryByPaymentMethodObject] }),
    __metadata("design:type", Array)
], WebBaseSummaryCloseOfDayObject.prototype, "summary_by_payment_method", void 0);
class SummaryCloseOfDayResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: SummaryCloseOfDayObject,
        example: {
            total_transaction: 3,
            total_collected: 280.79999999999995,
            net_sales: 240,
            summary_by_payment_method: {
                cash: 2,
                credit_card: 1,
                debit_card: 0,
                others: 0
            }
        }
    }),
    __metadata("design:type", SummaryCloseOfDayObject)
], SummaryCloseOfDayResponse.prototype, "data", void 0);
exports.SummaryCloseOfDayResponse = SummaryCloseOfDayResponse;
class WebBaseSummaryCloseOfDayResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: SummaryCloseOfDayObject,
        example: {
            total_transaction: 3,
            total_collected: 27.353433600000002,
            net_sales: 23.99424,
            summary_by_payment_method: [
                {
                    label: 'Cash',
                    amount: 0
                },
                {
                    label: 'Credit Card',
                    amount: 0
                },
                {
                    label: 'Debit Card',
                    amount: 27.353433600000002
                },
                {
                    label: 'Check',
                    amount: 0
                },
                {
                    label: 'Gift Card',
                    amount: 0
                },
                {
                    label: 'Delivery',
                    amount: 0
                },
                {
                    label: 'Others',
                    amount: 0
                }
            ]
        }
    }),
    __metadata("design:type", WebBaseSummaryCloseOfDayObject)
], WebBaseSummaryCloseOfDayResponse.prototype, "data", void 0);
exports.WebBaseSummaryCloseOfDayResponse = WebBaseSummaryCloseOfDayResponse;
//# sourceMappingURL=summary-close.entity.js.map