import { ItemsDto } from 'src/modules/order/dto/create-order-pos.dto';
declare class CustomerField {
    id: string;
    firstName: string;
    lastName: string;
    tel: string;
}
export declare class CreateTransactionPOSDto {
    txn: string;
    paymentStatus: string;
    transactionStatus: string;
    restaurantId: string;
    orderId: string;
    PorderId: string;
    orderType: string;
    customer: CustomerField;
    summaryItems: Array<ItemsDto>;
    subtotal: number;
    discount: number;
    serviceCharge: number;
    tax: number;
    alcoholTax: number;
    convenienceFee: number;
    total: number;
    splitMethod: string;
    splitBy: number;
    splitItems: Array<ItemsDto>;
    totalSplitItems: number;
    cardType: string;
    paymentMethod: string;
    cardLastFour: string;
    paidBy: string;
    retRef: string;
    batchId: number;
    openDate: string;
    paidAmount: number;
    tips: number;
    totalPaid: number;
    response: any;
    note: string;
}
export {};
