"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionPaginateDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const dayjs = require("dayjs");
const paginate_dto_1 = require("../../../common/dto/paginate.dto");
const status_enum_1 = require("../../../common/enum/status.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const payment_method_enum_1 = require("../common/payment-method.enum");
const payment_status_enum_1 = require("../common/payment-status.enum");
class TransactionPaginateDto extends paginate_dto_1.PaginateDto {
    constructor() {
        super(...arguments);
        this.sortBy = 'createdAt';
        this.sortOrder = 'desc';
        this.search = '';
    }
    buildQuery(id) {
        const result = {
            $or: [
                {
                    orderId: {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    porderId: {
                        $regex: this.search,
                        $options: 'i'
                    }
                },
                {
                    cardLastFour: {
                        $regex: this.search,
                        $options: 'i'
                    }
                }
            ],
            openDate: {
                $gte: dayjs(this.startDate).toDate(),
                $lte: dayjs(this.endDate).toDate()
            },
            restaurantId: id,
            orderType: this.orderType,
            paymentMethod: this.paymentMethod,
            paymentStatus: this.paymentStatus,
            status: status_enum_1.StatusEnum.ACTIVE
        };
        if (!this.paymentStatus)
            delete result.paymentStatus;
        if (!this.paymentMethod)
            delete result.paymentMethod;
        if (!this.orderType)
            delete result.orderType;
        return result;
    }
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'createdAt' }),
    __metadata("design:type", String)
], TransactionPaginateDto.prototype, "sortBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'desc' }),
    __metadata("design:type", String)
], TransactionPaginateDto.prototype, "sortOrder", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: '4321',
        description: 'use for search 4 last card number.'
    }),
    __metadata("design:type", String)
], TransactionPaginateDto.prototype, "search", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: payment_method_enum_1.PaymentMethodEnum.CREDIT,
        enum: Object.values(payment_method_enum_1.PaymentMethodEnum)
    }),
    __metadata("design:type", String)
], TransactionPaginateDto.prototype, "paymentMethod", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: order_type_enum_1.OrderTypeEnum.DINE_IN,
        enum: Object.values(order_type_enum_1.OrderTypeEnum)
    }),
    __metadata("design:type", String)
], TransactionPaginateDto.prototype, "orderType", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: payment_status_enum_1.PaymentStatusEnum.PAID,
        enum: Object.values(payment_status_enum_1.PaymentStatusEnum)
    }),
    __metadata("design:type", String)
], TransactionPaginateDto.prototype, "paymentStatus", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], TransactionPaginateDto.prototype, "startDate", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], TransactionPaginateDto.prototype, "endDate", void 0);
exports.TransactionPaginateDto = TransactionPaginateDto;
//# sourceMappingURL=get-transaction.dto.js.map