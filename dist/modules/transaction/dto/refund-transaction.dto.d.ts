import { ItemsDto } from 'src/modules/order/dto/create-order-pos.dto';
export declare class RefundTransactionDto {
    transactionId: string;
    amount: number;
    summaryItems: Array<ItemsDto>;
    readonly refundedReason: string;
}
