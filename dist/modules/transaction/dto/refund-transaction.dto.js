"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundTransactionDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const create_order_pos_dto_1 = require("../../order/dto/create-order-pos.dto");
class RefundTransactionDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '<transaction-id>' }),
    __metadata("design:type", String)
], RefundTransactionDto.prototype, "transactionId", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 1.01, description: 'use for partial refund' }),
    __metadata("design:type", Number)
], RefundTransactionDto.prototype, "amount", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_transformer_1.Type)(() => create_order_pos_dto_1.ItemsDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiPropertyOptional)({
        type: () => [create_order_pos_dto_1.ItemsDto],
        description: 'use for partial refund'
    }),
    __metadata("design:type", Array)
], RefundTransactionDto.prototype, "summaryItems", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Some Reason' }),
    __metadata("design:type", String)
], RefundTransactionDto.prototype, "refundedReason", void 0);
exports.RefundTransactionDto = RefundTransactionDto;
//# sourceMappingURL=refund-transaction.dto.js.map