export declare class CloseOfDayDto {
    startDate: string;
    endDate: string;
}
