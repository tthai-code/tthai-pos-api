export declare class AddTipsDto {
    readonly transactionId: string;
    readonly tips: number;
    readonly signature: string;
}
