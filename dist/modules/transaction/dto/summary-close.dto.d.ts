import { StatusEnum } from 'src/common/enum/status.enum';
export declare class SummaryClosOfDayDto {
    readonly startDate: string;
    readonly endDate: string;
    buildQuery(id: string): {
        openDate: {
            $gte: Date;
            $lte: Date;
        };
        restaurantId: string;
        status: StatusEnum;
    };
}
