"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateTransactionPOSDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const create_order_pos_dto_1 = require("../../order/dto/create-order-pos.dto");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const card_type_enum_1 = require("../common/card-type.enum");
const payment_method_enum_1 = require("../common/payment-method.enum");
const split_method_enum_1 = require("../common/split-method.enum");
class CustomerField {
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: null }),
    __metadata("design:type", String)
], CustomerField.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Guest' }),
    __metadata("design:type", String)
], CustomerField.prototype, "firstName", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '' }),
    __metadata("design:type", String)
], CustomerField.prototype, "lastName", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '' }),
    __metadata("design:type", String)
], CustomerField.prototype, "tel", void 0);
class CreateTransactionPOSDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '<restaurant-id>' }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "restaurantId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: '<order-id>' }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "orderId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiProperty)({ example: '<payment-order-id>' }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "PorderId", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(order_type_enum_1.OrderTypeEnum),
    (0, swagger_1.ApiProperty)({
        enum: [order_type_enum_1.OrderTypeEnum.DINE_IN, order_type_enum_1.OrderTypeEnum.TO_GO],
        example: order_type_enum_1.OrderTypeEnum.DINE_IN
    }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "orderType", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => CustomerField),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => CustomerField }),
    __metadata("design:type", CustomerField)
], CreateTransactionPOSDto.prototype, "customer", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => create_order_pos_dto_1.ItemsDto),
    (0, class_validator_1.ArrayNotEmpty)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => [create_order_pos_dto_1.ItemsDto] }),
    __metadata("design:type", Array)
], CreateTransactionPOSDto.prototype, "summaryItems", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 100 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "subtotal", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 0 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "discount", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 10 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "serviceCharge", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 7 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "tax", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 0 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "alcoholTax", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 0 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "convenienceFee", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 117 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "total", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(split_method_enum_1.SplitMethodEnum),
    (0, swagger_1.ApiProperty)({
        enum: Object.values(split_method_enum_1.SplitMethodEnum),
        example: split_method_enum_1.SplitMethodEnum.NO_SPLIT
    }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "splitMethod", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 1 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "splitBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_transformer_1.Type)(() => create_order_pos_dto_1.ItemsDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiPropertyOptional)({ type: () => [create_order_pos_dto_1.ItemsDto] }),
    __metadata("design:type", Array)
], CreateTransactionPOSDto.prototype, "splitItems", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 11.7 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "totalSplitItems", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsIn)(Object.values(card_type_enum_1.CardTypeEnum)),
    (0, swagger_1.ApiPropertyOptional)({
        enum: Object.values(card_type_enum_1.CardTypeEnum),
        example: card_type_enum_1.CardTypeEnum.MASTERCARD
    }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "cardType", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(payment_method_enum_1.PaymentMethodEnum),
    (0, swagger_1.ApiProperty)({
        example: payment_method_enum_1.PaymentMethodEnum.CREDIT,
        enum: Object.values(payment_method_enum_1.PaymentMethodEnum)
    }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "paymentMethod", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '4321' }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "cardLastFour", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'John Doe' }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "paidBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: '<retref>',
        description: 'retref from transaction Payment Gateway API'
    }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "retRef", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: 120,
        description: 'batchId from transaction Payment Gateway API'
    }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "batchId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: new Date().toISOString() }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "openDate", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 117 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "paidAmount", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 10 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "tips", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 127 }),
    __metadata("design:type", Number)
], CreateTransactionPOSDto.prototype, "totalPaid", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)({
        example: '',
        description: 'response from Payment Gateway API'
    }),
    __metadata("design:type", Object)
], CreateTransactionPOSDto.prototype, "response", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'note' }),
    __metadata("design:type", String)
], CreateTransactionPOSDto.prototype, "note", void 0);
exports.CreateTransactionPOSDto = CreateTransactionPOSDto;
//# sourceMappingURL=create-transaction.dto.js.map