import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
export declare class TransactionPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly paymentMethod: string;
    readonly orderType: string;
    readonly paymentStatus: string;
    readonly startDate: string;
    readonly endDate: string;
    buildQuery(id: string): {
        $or: ({
            orderId: {
                $regex: string;
                $options: string;
            };
            porderId?: undefined;
            cardLastFour?: undefined;
        } | {
            porderId: {
                $regex: string;
                $options: string;
            };
            orderId?: undefined;
            cardLastFour?: undefined;
        } | {
            cardLastFour: {
                $regex: string;
                $options: string;
            };
            orderId?: undefined;
            porderId?: undefined;
        })[];
        openDate: {
            $gte: Date;
            $lte: Date;
        };
        restaurantId: string;
        orderType: string;
        paymentMethod: string;
        paymentStatus: string;
        status: StatusEnum;
    };
}
