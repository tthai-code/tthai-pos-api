"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionSchema = exports.TransactionFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const order_items_schema_1 = require("../../order/schemas/order-items.schema");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const payment_method_enum_1 = require("../common/payment-method.enum");
const payment_status_enum_1 = require("../common/payment-status.enum");
const split_method_enum_1 = require("../common/split-method.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const order_schema_1 = require("../../order/schemas/order.schema");
const transaction_status_enum_1 = require("../common/transaction-status.enum");
let TransactionFields = class TransactionFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], TransactionFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], TransactionFields.prototype, "txn", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], TransactionFields.prototype, "orderId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], TransactionFields.prototype, "porderId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true, enum: Object.values(order_type_enum_1.OrderTypeEnum) }),
    __metadata("design:type", String)
], TransactionFields.prototype, "orderType", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: new order_schema_1.CustomerFields() }),
    __metadata("design:type", order_schema_1.CustomerFields)
], TransactionFields.prototype, "customer", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Array)
], TransactionFields.prototype, "summaryItems", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "subtotal", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "discount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "serviceCharge", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "tax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "alcoholTax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "convenienceFee", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "total", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(split_method_enum_1.SplitMethodEnum),
        default: split_method_enum_1.SplitMethodEnum.NO_SPLIT
    }),
    __metadata("design:type", String)
], TransactionFields.prototype, "splitMethod", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 1 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "splitBy", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], TransactionFields.prototype, "splitItems", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "totalSplitItems", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TransactionFields.prototype, "cardType", void 0);
__decorate([
    (0, mongoose_1.Prop)({ enum: Object.values(payment_method_enum_1.PaymentMethodEnum), required: true }),
    __metadata("design:type", String)
], TransactionFields.prototype, "paymentMethod", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TransactionFields.prototype, "cardLastFour", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 'Guest' }),
    __metadata("design:type", String)
], TransactionFields.prototype, "paidBy", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TransactionFields.prototype, "retRef", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "batchId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Date)
], TransactionFields.prototype, "openDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Date)
], TransactionFields.prototype, "closeDate", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(payment_status_enum_1.PaymentStatusEnum),
        default: payment_status_enum_1.PaymentStatusEnum.PAID
    }),
    __metadata("design:type", String)
], TransactionFields.prototype, "paymentStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: false }),
    __metadata("design:type", Boolean)
], TransactionFields.prototype, "isVoidBeforeTransaction", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "paidAmount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "tips", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "totalPaid", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        enum: Object.values(transaction_status_enum_1.TransactionStatusEnum),
        default: transaction_status_enum_1.TransactionStatusEnum.OPEN
    }),
    __metadata("design:type", String)
], TransactionFields.prototype, "transactionStatus", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", mongoose_2.Schema.Types.Mixed)
], TransactionFields.prototype, "response", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", mongoose_2.Schema.Types.Mixed)
], TransactionFields.prototype, "refundedResponse", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", mongoose_2.Schema.Types.Mixed)
], TransactionFields.prototype, "voidResponse", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], TransactionFields.prototype, "refundedItems", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "refundedAmount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], TransactionFields.prototype, "refundedReason", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TransactionFields.prototype, "deliverySource", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TransactionFields.prototype, "signature", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], TransactionFields.prototype, "note", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TransactionFields.prototype, "voidReason", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], TransactionFields.prototype, "voidItems", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionFields.prototype, "voidAmount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], TransactionFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], TransactionFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], TransactionFields.prototype, "createdBy", void 0);
TransactionFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'transactions' })
], TransactionFields);
exports.TransactionFields = TransactionFields;
exports.TransactionSchema = mongoose_1.SchemaFactory.createForClass(TransactionFields);
exports.TransactionSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
exports.TransactionSchema.plugin(mongoosePaginate);
//# sourceMappingURL=transaction.schema.js.map