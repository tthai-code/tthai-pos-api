import * as mongoose from 'mongoose';
export declare type TransactionCounterDocument = TransactionCounter & mongoose.Document;
export declare class TransactionCounter {
    prefix: string;
    restaurantId: mongoose.Types.ObjectId;
    counter: number;
}
export declare const TransactionCounterSchema: mongoose.Schema<mongoose.Document<TransactionCounter, any, any>, mongoose.Model<mongoose.Document<TransactionCounter, any, any>, any, any, any>, any, any>;
