"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionCounterSchema = exports.TransactionCounter = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose = require("mongoose");
let TransactionCounter = class TransactionCounter {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], TransactionCounter.prototype, "prefix", void 0);
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose.Types.ObjectId)
], TransactionCounter.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TransactionCounter.prototype, "counter", void 0);
TransactionCounter = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, collection: '_transactionCounter' })
], TransactionCounter);
exports.TransactionCounter = TransactionCounter;
exports.TransactionCounterSchema = mongoose_1.SchemaFactory.createForClass(TransactionCounter);
//# sourceMappingURL=transaction-counter.schema.js.map