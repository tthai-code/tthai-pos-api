import * as mongoose from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type TransactionLogDocument = TransactionLogFields & mongoose.Document;
export declare class TransactionLogFields {
    status: string;
    ip: string;
    baseUrl: string;
    originalUrl: string;
    headers: mongoose.Schema.Types.Mixed;
    body: mongoose.Schema.Types.Mixed;
    params: mongoose.Schema.Types.Mixed;
    query: mongoose.Schema.Types.Mixed;
    method: string;
    note: string;
    req: mongoose.Schema.Types.Mixed;
    res: mongoose.Schema.Types.Mixed;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const TransactionLogSchema: mongoose.Schema<mongoose.Document<TransactionLogFields, any, any>, mongoose.Model<mongoose.Document<TransactionLogFields, any, any>, any, any, any>, any, any>;
