"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const cash_drawer_module_1 = require("../cash-drawer/cash-drawer.module");
const order_module_1 = require("../order/order.module");
const payment_gateway_module_1 = require("../payment-gateway/payment-gateway.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const table_module_1 = require("../table/table.module");
const log_transaction_controller_1 = require("./controllers/log-transaction.controller");
const pos_transaction_controller_1 = require("./controllers/pos-transaction.controller");
const transaction_controller_1 = require("./controllers/transaction.controller");
const log_transaction_logic_1 = require("./logics/log-transaction.logic");
const pos_transaction_logic_1 = require("./logics/pos-transaction.logic");
const transaction_logic_1 = require("./logics/transaction.logic");
const void_transaction_logic_1 = require("./logics/void-transaction.logic");
const log_transaction_schema_1 = require("./schemas/log-transaction.schema");
const transaction_counter_schema_1 = require("./schemas/transaction-counter.schema");
const transaction_schema_1 = require("./schemas/transaction.schema");
const log_transaction_service_1 = require("./services/log.transaction.service");
const transaction_counter_service_1 = require("./services/transaction-counter.service");
const transaction_service_1 = require("./services/transaction.service");
let TransactionModule = class TransactionModule {
};
TransactionModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            (0, common_1.forwardRef)(() => order_module_1.OrderModule),
            (0, common_1.forwardRef)(() => table_module_1.TableModule),
            (0, common_1.forwardRef)(() => payment_gateway_module_1.PaymentGatewayModule),
            (0, common_1.forwardRef)(() => cash_drawer_module_1.CashDrawerModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'transactionLog', schema: log_transaction_schema_1.TransactionLogSchema },
                { name: 'transaction', schema: transaction_schema_1.TransactionSchema },
                { name: 'transaction-counter', schema: transaction_counter_schema_1.TransactionCounterSchema }
            ])
        ],
        providers: [
            log_transaction_service_1.TransactionLogService,
            log_transaction_logic_1.TransactionLogLogic,
            transaction_logic_1.TransactionLogic,
            transaction_service_1.TransactionService,
            transaction_counter_service_1.TransactionCounterService,
            void_transaction_logic_1.VoidTransactionLogic,
            pos_transaction_logic_1.POSTransactionLogic
        ],
        controllers: [
            log_transaction_controller_1.TransactionLogController,
            pos_transaction_controller_1.POSTransactionController,
            transaction_controller_1.TransactionController
        ],
        exports: [
            log_transaction_logic_1.TransactionLogLogic,
            transaction_logic_1.TransactionLogic,
            transaction_service_1.TransactionService,
            void_transaction_logic_1.VoidTransactionLogic
        ]
    })
], TransactionModule);
exports.TransactionModule = TransactionModule;
//# sourceMappingURL=transaction.module.js.map