import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CloseOfDayDto } from '../dto/close-of-day.dto';
import { TransactionPaginateDto } from '../dto/get-transaction.dto';
import { TransactionService } from '../services/transaction.service';
export declare class POSTransactionLogic {
    private readonly restaurantService;
    private readonly transactionService;
    private readonly kitchenQueueService;
    constructor(restaurantService: RestaurantService, transactionService: TransactionService, kitchenQueueService: KitchenQueueService);
    private paymentMethodStringFormat;
    private summaryTransactionItem;
    checkCloseOfDayStatus(restaurantId: string, payload: CloseOfDayDto): Promise<{
        isClosed: boolean;
    }>;
    getPOSTransactionList(query: TransactionPaginateDto, restaurantId: string): Promise<any>;
}
