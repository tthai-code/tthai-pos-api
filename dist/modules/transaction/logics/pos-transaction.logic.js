"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSTransactionLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const kitchen_queue_service_1 = require("../../order/services/kitchen-queue.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const payment_method_enum_1 = require("../common/payment-method.enum");
const payment_status_enum_1 = require("../common/payment-status.enum");
const transaction_status_enum_1 = require("../common/transaction-status.enum");
const transaction_service_1 = require("../services/transaction.service");
let POSTransactionLogic = class POSTransactionLogic {
    constructor(restaurantService, transactionService, kitchenQueueService) {
        this.restaurantService = restaurantService;
        this.transactionService = transactionService;
        this.kitchenQueueService = kitchenQueueService;
    }
    paymentMethodStringFormat(text) {
        switch (text) {
            case 'CASH':
                return 'Cash';
            case 'CREDIT':
                return 'Credit';
            case 'DEBIT':
                return 'Debit';
            case 'THIRD_PARTY':
                return 'Third Party';
            case 'GIFT_CARD':
                return 'Gift Card';
            case 'CHECK':
                return 'Check';
            case 'DELIVERY':
                return 'Delivery';
            case 'DE_MINIMIS':
                return 'Fringe Benefit';
            case 'OTHER':
                return 'Others';
            default:
                return text;
        }
    }
    summaryTransactionItem(items, key) {
        return (items
            .map((item) => item[key])
            .reduce((sum, current) => sum + current, 0) || 0);
    }
    async checkCloseOfDayStatus(restaurantId, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const transactions = await this.transactionService.getAll({
            restaurantId,
            openDate: {
                $gte: payload.startDate,
                $lte: payload.endDate
            },
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const isClosed = transactions.every((item) => item.transactionStatus === transaction_status_enum_1.TransactionStatusEnum.CLOSED);
        return { isClosed };
    }
    async getPOSTransactionList(query, restaurantId) {
        const results = await this.transactionService.paginate(query.buildQuery(restaurantId), query);
        const transactions = await this.transactionService.getAll(query.buildQuery(restaurantId));
        const summaryTotal = this.summaryTransactionItem(transactions, 'totalPaid');
        const paidTransaction = transactions.filter((item) => item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.VOID &&
            item.paymentMethod !== payment_method_enum_1.PaymentMethodEnum.DE_MINIMIS);
        const totalCollected = this.summaryTransactionItem(paidTransaction, 'totalPaid');
        const refundedTransaction = transactions.filter((item) => item.paymentStatus === payment_status_enum_1.PaymentStatusEnum.REFUNDED);
        const paidOrderIds = paidTransaction.map((item) => item.orderId);
        paidOrderIds.forEach((item, index) => {
            if (paidOrderIds.indexOf(item) !== index) {
                paidTransaction.splice(index, 1);
            }
        });
        const summarySubtotal = this.summaryTransactionItem(paidTransaction, 'subtotal');
        const summaryDiscount = this.summaryTransactionItem(paidTransaction, 'discount');
        const summaryRefunded = this.summaryTransactionItem(refundedTransaction, 'refundedAmount');
        const { docs } = results;
        const transformDocs = [];
        for (const doc of docs) {
            const kitchenQueue = await this.kitchenQueueService.findOne({
                orderId: doc.orderId
            }, { ticketNo: 1 }, { sort: { createdAt: -1 } });
            const paymentMethod = this.paymentMethodStringFormat(doc.paymentMethod);
            transformDocs.push(Object.assign(Object.assign({}, doc === null || doc === void 0 ? void 0 : doc.toObject()), { ticketNo: (kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) || null, paymentMethod }));
        }
        return Object.assign(Object.assign({}, results), { docs: transformDocs, totalCollected: totalCollected - summaryRefunded, netSales: summarySubtotal - summaryDiscount - summaryRefunded, summaryTotal });
    }
};
POSTransactionLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        transaction_service_1.TransactionService,
        kitchen_queue_service_1.KitchenQueueService])
], POSTransactionLogic);
exports.POSTransactionLogic = POSTransactionLogic;
//# sourceMappingURL=pos-transaction.logic.js.map