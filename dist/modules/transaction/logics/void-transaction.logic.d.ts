import { ClientSession } from 'mongoose';
import { OrderItemsLogic } from 'src/modules/order/logics/order-items.logic';
import { OrderDocument } from 'src/modules/order/schemas/order.schema';
import { TransactionCounterService } from '../services/transaction-counter.service';
import { TransactionService } from '../services/transaction.service';
export declare class VoidTransactionLogic {
    private readonly transactionService;
    private readonly transactionCounterService;
    private readonly orderItemsLogic;
    constructor(transactionService: TransactionService, transactionCounterService: TransactionCounterService, orderItemsLogic: OrderItemsLogic);
    private genTransactionId;
    createVoidTransaction(order: OrderDocument, session: ClientSession): Promise<import("../schemas/transaction.schema").TransactionDocument>;
}
