"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VoidTransactionLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs = require("dayjs");
const order_items_logic_1 = require("../../order/logics/order-items.logic");
const order_schema_1 = require("../../order/schemas/order.schema");
const payment_method_enum_1 = require("../common/payment-method.enum");
const payment_status_enum_1 = require("../common/payment-status.enum");
const transaction_counter_service_1 = require("../services/transaction-counter.service");
const transaction_service_1 = require("../services/transaction.service");
let VoidTransactionLogic = class VoidTransactionLogic {
    constructor(transactionService, transactionCounterService, orderItemsLogic) {
        this.transactionService = transactionService;
        this.transactionCounterService = transactionCounterService;
        this.orderItemsLogic = orderItemsLogic;
    }
    async genTransactionId(restaurantId) {
        const now = dayjs().format('YYYYMMDD');
        const countQueue = await this.transactionCounterService.getCounter(now, restaurantId);
        return `${countQueue.toString().padStart(3, '0')}`;
    }
    async createVoidTransaction(order, session) {
        const txn = await this.genTransactionId(order.restaurant.id);
        const payload = {
            restaurantId: order.restaurant.id,
            txn,
            orderId: order.id,
            porderId: order.id,
            orderType: order.orderType,
            customer: order.customer,
            summaryItems: order.summaryItems,
            subtotal: order.subtotal,
            discount: order.discount,
            serviceCharge: order.serviceCharge,
            tax: order.tax,
            alcoholTax: order.alcoholTax,
            convenienceFee: order.convenienceFee,
            total: order.total,
            paymentMethod: payment_method_enum_1.PaymentMethodEnum.OTHER,
            paymentStatus: payment_status_enum_1.PaymentStatusEnum.VOID,
            openDate: new Date(),
            paidAmount: order.total,
            totalPaid: order.total,
            note: 'Void order before transaction',
            isVoidBeforeTransaction: true
        };
        return this.transactionService.transactionCreate(payload, session);
    }
};
VoidTransactionLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [transaction_service_1.TransactionService,
        transaction_counter_service_1.TransactionCounterService,
        order_items_logic_1.OrderItemsLogic])
], VoidTransactionLogic);
exports.VoidTransactionLogic = VoidTransactionLogic;
//# sourceMappingURL=void-transaction.logic.js.map