import { TransactionLogService } from '../services/log.transaction.service';
export declare class TransactionLogLogic {
    private readonly transactionLogService;
    constructor(transactionLogService: TransactionLogService);
    createLogic(request: any, res: any, note?: any): Promise<import("../schemas/log-transaction.schema").TransactionLogDocument>;
}
