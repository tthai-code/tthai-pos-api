"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionLogLogic = void 0;
const common_1 = require("@nestjs/common");
const log_transaction_service_1 = require("../services/log.transaction.service");
let TransactionLogLogic = class TransactionLogLogic {
    constructor(transactionLogService) {
        this.transactionLogService = transactionLogService;
    }
    async createLogic(request, res, note) {
        const requestIp = require('request-ip');
        const reqIp = request.clientIp
            ? request.clientIp
            : requestIp.getClientIp(request);
        const payload = {
            ip: reqIp,
            baseUrl: request.baseUrl,
            originalUrl: request.originalUrl,
            headers: request.headers,
            body: request.body,
            params: request.params,
            query: request.query,
            method: request.method,
            res,
            note
        };
        return await this.transactionLogService.create(payload);
    }
};
TransactionLogLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [log_transaction_service_1.TransactionLogService])
], TransactionLogLogic);
exports.TransactionLogLogic = TransactionLogLogic;
//# sourceMappingURL=log-transaction.logic.js.map