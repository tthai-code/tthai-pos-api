"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs_1 = require("../../../plugins/dayjs");
const status_enum_1 = require("../../../common/enum/status.enum");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const order_state_enum_1 = require("../../order/enum/order-state.enum");
const order_service_1 = require("../../order/services/order.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const transaction_counter_service_1 = require("../services/transaction-counter.service");
const transaction_service_1 = require("../services/transaction.service");
const payment_status_enum_1 = require("../common/payment-status.enum");
const transaction_status_enum_1 = require("../common/transaction-status.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const table_service_1 = require("../../table/services/table.service");
const table_status_enum_1 = require("../../table/common/table-status.enum");
const card_pointe_logic_1 = require("../../payment-gateway/logics/card-pointe.logic");
const payment_method_enum_1 = require("../common/payment-method.enum");
const third_party_order_schema_1 = require("../../order/schemas/third-party-order.schema");
const kitchen_queue_service_1 = require("../../order/services/kitchen-queue.service");
const item_status_enum_1 = require("../../order/enum/item-status.enum");
const order_online_schema_1 = require("../../order/schemas/order-online.schema");
const order_online_service_1 = require("../../order/services/order-online.service");
const pos_cash_drawer_logic_1 = require("../../cash-drawer/logic/pos-cash-drawer.logic");
const cash_drawer_enum_1 = require("../../cash-drawer/common/cash-drawer.enum");
const order_schema_1 = require("../../order/schemas/order.schema");
let TransactionLogic = class TransactionLogic {
    constructor(transactionService, transactionCounterService, restaurantService, orderService, onlineOrderService, tableService, cardPointeLogic, kitchenQueueService, posCashDrawerLogic) {
        this.transactionService = transactionService;
        this.transactionCounterService = transactionCounterService;
        this.restaurantService = restaurantService;
        this.orderService = orderService;
        this.onlineOrderService = onlineOrderService;
        this.tableService = tableService;
        this.cardPointeLogic = cardPointeLogic;
        this.kitchenQueueService = kitchenQueueService;
        this.posCashDrawerLogic = posCashDrawerLogic;
    }
    async genTransactionId(restaurantId) {
        const { timeZone } = await this.restaurantService.findOne({
            _id: restaurantId
        });
        const now = (0, dayjs_1.default)().tz(timeZone);
        const nowUnix = now.valueOf();
        const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf();
        const endOfDay = now
            .add(1, 'day')
            .hour(3)
            .minute(59)
            .millisecond(999)
            .valueOf();
        let nowFormat = now.subtract(1, 'day').format('YYYYMMDD');
        if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
            nowFormat = now.format('YYYYMMDD');
        }
        const countQueue = await this.transactionCounterService.getCounter(nowFormat, restaurantId);
        return `${countQueue.toString().padStart(3, '0')}`;
    }
    summaryTransactionItem(items, key) {
        return (items
            .map((item) => item[key])
            .reduce((sum, current) => sum + current, 0) || 0);
    }
    async createTransactionPOS(payload) {
        var _a;
        const logic = async (session) => {
            const restaurant = await this.restaurantService.findOne({
                _id: payload.restaurantId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!restaurant)
                throw new common_1.NotFoundException('Not found restaurant.');
            const order = await this.orderService.findOne({
                _id: payload.orderId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!order) {
                throw new common_1.NotFoundException('Not found order.');
            }
            const txn = await this.genTransactionId(payload.restaurantId);
            payload.txn = txn;
            payload.paymentStatus = payment_status_enum_1.PaymentStatusEnum.PAID;
            payload.transactionStatus = transaction_status_enum_1.TransactionStatusEnum.OPEN;
            const created = await this.transactionService.transactionCreate(payload, session);
            if (payload.orderType === order_type_enum_1.OrderTypeEnum.DINE_IN) {
                await this.tableService.transactionUpdate(order.table.id, {
                    tableStatus: table_status_enum_1.TableStatusEnum.AVAILABLE
                }, session);
            }
            await this.orderService.transactionUpdate(payload.orderId, { orderStatus: order_state_enum_1.OrderStateEnum.COMPLETED, discount: payload.discount }, session);
            await this.kitchenQueueService.transactionUpdateMany({ orderId: payload.orderId }, { queueStatus: item_status_enum_1.ItemStatusEnum.COMPLETED }, session);
            return created;
        };
        const created = await (0, mongoose_transaction_1.runTransaction)(logic);
        if (created.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CASH) {
            await this.posCashDrawerLogic.cashSales(payload.restaurantId, {
                date: created.openDate,
                amount: created.totalPaid,
                action: cash_drawer_enum_1.CashDrawerActionEnum.CASH_SALES,
                user: (_a = created === null || created === void 0 ? void 0 : created.createdBy) === null || _a === void 0 ? void 0 : _a.username,
                comment: `Order# ${created.orderId}`
            });
        }
        return created;
    }
    async handleVoidCreditDebit(transactionId, restaurantId, transaction) {
        if (!transaction.retRef)
            throw new common_1.BadRequestException('This transaction not have a retRef.');
        const { data } = await this.cardPointeLogic.voidTransaction(transaction.retRef, restaurantId);
        if (data.respstat !== 'A')
            throw new common_1.BadRequestException(data.resptext);
        return data;
    }
    async handleVoidOrder(transaction, session) {
        if (transaction.orderType === order_type_enum_1.OrderTypeEnum.DINE_IN ||
            transaction.orderType === order_type_enum_1.OrderTypeEnum.TO_GO) {
            return this.orderService.transactionUpdate(transaction.orderId, { orderStatus: order_state_enum_1.OrderStateEnum.VOID }, session);
        }
        if (transaction.orderType === order_type_enum_1.OrderTypeEnum.ONLINE) {
            return this.onlineOrderService.transactionUpdate(transaction.orderId, { orderStatus: order_state_enum_1.OrderStateEnum.VOID }, session);
        }
    }
    async voidTransaction(transactionId, restaurantId) {
        const logic = async (session) => {
            const transaction = await this.transactionService.findOne({
                _id: transactionId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!transaction)
                throw new common_1.NotFoundException('Not found transaction.');
            if (transaction.paymentStatus === payment_status_enum_1.PaymentStatusEnum.VOID) {
                throw new common_1.BadRequestException('This transaction is already voided.');
            }
            if (transaction.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CREDIT ||
                transaction.paymentMethod === payment_method_enum_1.PaymentMethodEnum.DEBIT) {
                const data = await this.handleVoidCreditDebit(transactionId, restaurantId, transaction);
                await this.transactionService.transactionUpdate(transactionId, { paymentStatus: payment_status_enum_1.PaymentStatusEnum.VOID, voidResponse: data }, session);
                await this.handleVoidOrder(transaction, session);
                return { success: true, response: data ? data : null };
            }
            await this.transactionService.transactionUpdate(transactionId, { paymentStatus: payment_status_enum_1.PaymentStatusEnum.VOID }, session);
            await this.handleVoidOrder(transaction, session);
            return { success: true, response: null };
        };
        return (0, mongoose_transaction_1.runTransaction)(logic);
    }
    async closeBatch(restaurantId, payload) {
        const startDate = (0, dayjs_1.default)(payload.startDate)
            .set('second', 0)
            .set('millisecond', 0)
            .toDate();
        const endDate = (0, dayjs_1.default)(payload.endDate)
            .set('second', 59)
            .set('millisecond', 999)
            .toDate();
        const transactions = await this.transactionService.getAll({
            restaurantId,
            openDate: {
                $gte: startDate,
                $lte: endDate
            },
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const batchIds = [];
        for (const transaction of transactions) {
            const { batchId } = transaction;
            if (!!batchId && !batchIds.includes(batchId))
                batchIds.push(batchId);
        }
        const promises = batchIds.map((item) => this.cardPointeLogic.closeBatch(restaurantId, item));
        await Promise.all(promises);
        const query = {
            restaurantId,
            $or: [
                { batchId: { $in: batchIds } },
                {
                    paymentMethod: {
                        $nin: [payment_method_enum_1.PaymentMethodEnum.CREDIT, payment_method_enum_1.PaymentMethodEnum.DEBIT]
                    }
                }
            ],
            openDate: {
                $gte: startDate,
                $lte: endDate
            },
            status: status_enum_1.StatusEnum.ACTIVE
        };
        await this.transactionService.updateMany(query, {
            transactionStatus: transaction_status_enum_1.TransactionStatusEnum.CLOSED,
            closeDate: new Date()
        });
        return { success: true };
    }
    async refundTransaction(payload, restaurantId) {
        const transaction = await this.transactionService.findOne({
            _id: payload.transactionId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!transaction)
            throw new common_1.NotFoundException('Not found transaction.');
        if (!transaction.retRef) {
            throw new common_1.BadRequestException('This transaction not have a retRef.');
        }
        if (transaction.paymentStatus === payment_status_enum_1.PaymentStatusEnum.REFUNDED) {
            throw new common_1.BadRequestException('This transaction was already refunded.');
        }
        const { data } = await this.cardPointeLogic.refundTransaction(transaction.retRef, restaurantId, payload.amount);
        if (data.respstat !== 'A')
            throw new common_1.BadRequestException(data.resptext);
        await this.transactionService.update(payload.transactionId, {
            paymentStatus: payment_status_enum_1.PaymentStatusEnum.REFUNDED,
            refundedItems: payload.summaryItems,
            refundAmount: payload.amount || transaction.totalPaid,
            refundedResponse: data,
            refundedReason: payload.refundedReason
        });
        return { success: true };
    }
    async getTransactionList(query, restaurantId) {
        const results = await this.transactionService.paginate(query.buildQuery(restaurantId), query);
        const transactions = await this.transactionService.getAll(query.buildQuery(restaurantId));
        const summaryTotal = this.summaryTransactionItem(transactions, 'totalPaid');
        const paidTransaction = transactions.filter((item) => item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.VOID &&
            item.paymentMethod !== payment_method_enum_1.PaymentMethodEnum.DE_MINIMIS);
        const totalCollected = this.summaryTransactionItem(paidTransaction, 'totalPaid');
        const refundedTransaction = transactions.filter((item) => item.paymentStatus === payment_status_enum_1.PaymentStatusEnum.REFUNDED);
        const paidOrderIds = paidTransaction.map((item) => item.orderId);
        paidOrderIds.forEach((item, index) => {
            if (paidOrderIds.indexOf(item) !== index) {
                paidTransaction.splice(index, 1);
            }
        });
        const summarySubtotal = this.summaryTransactionItem(paidTransaction, 'subtotal');
        const summaryDiscount = this.summaryTransactionItem(paidTransaction, 'discount');
        const summaryRefunded = this.summaryTransactionItem(refundedTransaction, 'refundedAmount');
        const { docs } = results;
        const transformDocs = [];
        for (const doc of docs) {
            const kitchenQueue = await this.kitchenQueueService.findOne({
                orderId: doc.orderId
            }, { ticketNo: 1 }, { sort: { createdAt: -1 } });
            transformDocs.push(Object.assign(Object.assign({}, doc === null || doc === void 0 ? void 0 : doc.toObject()), { ticketNo: (kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) || null }));
        }
        return Object.assign(Object.assign({}, results), { docs: transformDocs, totalCollected: totalCollected - summaryRefunded, netSales: summarySubtotal - summaryDiscount - summaryRefunded, summaryTotal });
    }
    async addTipLogic(payload) {
        const transaction = await this.transactionService.findOne({
            _id: payload.transactionId,
            orderType: {
                $in: [order_type_enum_1.OrderTypeEnum.DINE_IN, order_type_enum_1.OrderTypeEnum.TO_GO]
            },
            paymentMethod: {
                $in: [payment_method_enum_1.PaymentMethodEnum.CREDIT, payment_method_enum_1.PaymentMethodEnum.DEBIT]
            },
            transactionStatus: transaction_status_enum_1.TransactionStatusEnum.OPEN
        });
        if (!transaction) {
            throw new common_1.NotFoundException('Not found transaction.');
        }
        const amount = payload.tips + transaction.paidAmount;
        const { data } = await this.cardPointeLogic.captureTransaction(transaction.retRef, `${transaction.restaurantId}`, amount);
        if (!['00', '000'].includes(data.respcode)) {
            throw new common_1.BadRequestException(data.resptext);
        }
        await this.transactionService.update(payload.transactionId, {
            totalPaid: amount,
            tips: payload.tips,
            signature: payload === null || payload === void 0 ? void 0 : payload.signature,
            response: data
        });
        return { success: true };
    }
    async createThirdPartyOrderTransaction(order, session) {
        var _a;
        const payload = {};
        payload.txn = await this.genTransactionId(order.restaurant.id);
        payload.paymentStatus = payment_status_enum_1.PaymentStatusEnum.PAID;
        payload.transactionStatus = transaction_status_enum_1.TransactionStatusEnum.OPEN;
        payload.restaurantId = order.restaurant.id;
        payload.orderType = order_type_enum_1.OrderTypeEnum.THIRD_PARTY;
        payload.customer = order.customer;
        payload.orderId = order.id;
        payload.summaryItems = order.summaryItems;
        payload.subtotal = order.subtotal;
        payload.discount = order.discount;
        payload.serviceCharge = order.serviceCharge;
        payload.tax = order.tax;
        payload.alcoholTax = order.alcoholTax;
        payload.convenienceFee = order.convenienceFee;
        payload.total = order.total;
        payload.paymentMethod = payment_method_enum_1.PaymentMethodEnum.THIRD_PARTY;
        payload.paidBy = (_a = order === null || order === void 0 ? void 0 : order.customer) === null || _a === void 0 ? void 0 : _a.firstName;
        payload.openDate = new Date();
        payload.paidAmount = order.total;
        payload.tips = order.tips;
        payload.totalPaid = order.total + order.tips;
        payload.deliverySource = order.deliverySource;
        return this.transactionService.transactionCreate(payload, session);
    }
    async summaryCloseOfDay(query, restaurantId) {
        const transactions = await this.transactionService.getAll(query.buildQuery(restaurantId));
        const paidTransaction = transactions.filter((item) => item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.VOID &&
            item.paymentMethod !== payment_method_enum_1.PaymentMethodEnum.DE_MINIMIS);
        const totalCollected = this.summaryTransactionItem(paidTransaction, 'totalPaid');
        const refundedTransaction = transactions.filter((item) => item.paymentStatus === payment_status_enum_1.PaymentStatusEnum.REFUNDED);
        const paidOrderIds = paidTransaction.map((item) => item.orderId);
        paidOrderIds.forEach((item, index) => {
            if (paidOrderIds.indexOf(item) !== index) {
                paidTransaction.splice(index, 1);
            }
        });
        const summarySubtotal = this.summaryTransactionItem(paidTransaction, 'subtotal');
        const summaryDiscount = this.summaryTransactionItem(paidTransaction, 'discount');
        const summaryRefunded = this.summaryTransactionItem(refundedTransaction, 'refundedAmount');
        return {
            totalTransaction: transactions.length,
            totalCollected: totalCollected - summaryRefunded,
            netSales: summarySubtotal - summaryDiscount - summaryRefunded,
            summaryByPaymentMethod: [
                {
                    label: 'Cash',
                    amount: this.summaryTransactionItem(paidTransaction.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CASH &&
                        item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.REFUNDED), 'totalPaid')
                },
                {
                    label: 'Credit Card',
                    amount: this.summaryTransactionItem(paidTransaction.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CREDIT &&
                        item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.REFUNDED), 'totalPaid')
                },
                {
                    label: 'Debit Card',
                    amount: this.summaryTransactionItem(paidTransaction.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.DEBIT &&
                        item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.REFUNDED), 'totalPaid')
                },
                {
                    label: 'Check',
                    amount: this.summaryTransactionItem(paidTransaction.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.CHECK &&
                        item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.REFUNDED), 'totalPaid')
                },
                {
                    label: 'Gift Card',
                    amount: this.summaryTransactionItem(paidTransaction.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.GIFT_CARD &&
                        item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.REFUNDED), 'totalPaid')
                },
                {
                    label: 'Delivery',
                    amount: this.summaryTransactionItem(paidTransaction.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.DELIVERY &&
                        item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.REFUNDED), 'totalPaid')
                },
                {
                    label: 'Others',
                    amount: this.summaryTransactionItem(paidTransaction.filter((item) => item.paymentMethod === payment_method_enum_1.PaymentMethodEnum.OTHER &&
                        item.paymentStatus !== payment_status_enum_1.PaymentStatusEnum.REFUNDED), 'totalPaid')
                }
            ]
        };
    }
    async createOnlineTransaction(order, payment, session) {
        const payload = {};
        payload.txn = await this.genTransactionId(order.restaurant.id);
        payload.paymentStatus = payment_status_enum_1.PaymentStatusEnum.PAID;
        payload.transactionStatus = transaction_status_enum_1.TransactionStatusEnum.OPEN;
        payload.restaurantId = order.restaurant.id;
        payload.orderType = order_type_enum_1.OrderTypeEnum.ONLINE;
        payload.customer = order.customer;
        payload.orderId = order.id;
        payload.summaryItems = order.summaryItems;
        payload.subtotal = order.subtotal;
        payload.discount = order.discount;
        payload.serviceCharge = order.serviceCharge;
        payload.tax = order.tax;
        payload.alcoholTax = order.alcoholTax;
        payload.convenienceFee = order.convenienceFee;
        payload.total = order.total;
        payload.retRef = payment.retRef;
        payload.batchId = Number(payment.batchId);
        payload.cardType = payment.cardType;
        payload.paymentMethod = payment.paymentMethod;
        payload.cardLastFour = payment.cardLastFour;
        payload.paidBy = payment.paidBy;
        payload.openDate = new Date();
        payload.paidAmount = order.total;
        payload.tips = order.tips;
        payload.totalPaid = order.total;
        payload.response = payment.response;
        return this.transactionService.transactionCreate(payload, session);
    }
};
TransactionLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [transaction_service_1.TransactionService,
        transaction_counter_service_1.TransactionCounterService,
        restaurant_service_1.RestaurantService,
        order_service_1.OrderService,
        order_online_service_1.OrderOnlineService,
        table_service_1.TableService,
        card_pointe_logic_1.CardPointeLogic,
        kitchen_queue_service_1.KitchenQueueService,
        pos_cash_drawer_logic_1.POSCashDrawerLogic])
], TransactionLogic);
exports.TransactionLogic = TransactionLogic;
//# sourceMappingURL=transaction.logic.js.map