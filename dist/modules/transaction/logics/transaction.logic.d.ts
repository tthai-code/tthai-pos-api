import { ClientSession } from 'mongoose';
import { OrderService } from 'src/modules/order/services/order.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { CreateTransactionPOSDto } from '../dto/create-transaction.dto';
import { TransactionCounterService } from '../services/transaction-counter.service';
import { TransactionService } from '../services/transaction.service';
import { TableService } from 'src/modules/table/services/table.service';
import { CardPointeLogic } from 'src/modules/payment-gateway/logics/card-pointe.logic';
import { RefundTransactionDto } from '../dto/refund-transaction.dto';
import { TransactionPaginateDto } from '../dto/get-transaction.dto';
import { AddTipsDto } from '../dto/add-tips.dto';
import { ThirdPartyOrderDocument } from 'src/modules/order/schemas/third-party-order.schema';
import { CloseOfDayDto } from '../dto/close-of-day.dto';
import { SummaryClosOfDayDto } from '../dto/summary-close.dto';
import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service';
import { IPaymentMethodCardType } from '../interfaces/online-transaction';
import { OrderOnlineDocument } from 'src/modules/order/schemas/order-online.schema';
import { OrderOnlineService } from 'src/modules/order/services/order-online.service';
import { POSCashDrawerLogic } from 'src/modules/cash-drawer/logic/pos-cash-drawer.logic';
import { TransactionDocument } from '../schemas/transaction.schema';
import { OrderDocument } from 'src/modules/order/schemas/order.schema';
export declare class TransactionLogic {
    private readonly transactionService;
    private readonly transactionCounterService;
    private readonly restaurantService;
    private readonly orderService;
    private readonly onlineOrderService;
    private readonly tableService;
    private readonly cardPointeLogic;
    private readonly kitchenQueueService;
    private readonly posCashDrawerLogic;
    constructor(transactionService: TransactionService, transactionCounterService: TransactionCounterService, restaurantService: RestaurantService, orderService: OrderService, onlineOrderService: OrderOnlineService, tableService: TableService, cardPointeLogic: CardPointeLogic, kitchenQueueService: KitchenQueueService, posCashDrawerLogic: POSCashDrawerLogic);
    private genTransactionId;
    private summaryTransactionItem;
    createTransactionPOS(payload: CreateTransactionPOSDto): Promise<TransactionDocument>;
    handleVoidCreditDebit(transactionId: string, restaurantId: string, transaction: TransactionDocument): Promise<any>;
    handleVoidOrder(transaction: TransactionDocument, session: ClientSession): Promise<OrderDocument | OrderOnlineDocument>;
    voidTransaction(transactionId: string, restaurantId: string): Promise<{
        success: boolean;
        response: any;
    }>;
    closeBatch(restaurantId: string, payload: CloseOfDayDto): Promise<{
        success: boolean;
    }>;
    refundTransaction(payload: RefundTransactionDto, restaurantId: string): Promise<{
        success: boolean;
    }>;
    getTransactionList(query: TransactionPaginateDto, restaurantId: string): Promise<any>;
    addTipLogic(payload: AddTipsDto): Promise<{
        success: boolean;
    }>;
    createThirdPartyOrderTransaction(order: ThirdPartyOrderDocument, session: ClientSession): Promise<TransactionDocument>;
    summaryCloseOfDay(query: SummaryClosOfDayDto, restaurantId: string): Promise<{
        totalTransaction: number;
        totalCollected: number;
        netSales: number;
        summaryByPaymentMethod: {
            label: string;
            amount: number;
        }[];
    }>;
    createOnlineTransaction(order: OrderOnlineDocument, payment: IPaymentMethodCardType, session: ClientSession): Promise<TransactionDocument>;
}
