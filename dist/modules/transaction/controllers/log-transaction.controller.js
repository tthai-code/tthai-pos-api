"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionLogController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const log_transaction_dto_1 = require("../dto/log-transaction.dto");
const log_transaction_service_1 = require("../services/log.transaction.service");
let TransactionLogController = class TransactionLogController {
    constructor(transactionLogService) {
        this.transactionLogService = transactionLogService;
    }
    async getAccessLog(query) {
        return await this.transactionLogService.paginate(query.buildQuery(), query);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Get Transaction Log List' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [log_transaction_dto_1.TransactionLogPaginateDto]),
    __metadata("design:returntype", Promise)
], TransactionLogController.prototype, "getAccessLog", null);
TransactionLogController = __decorate([
    (0, swagger_1.ApiTags)('transaction-logs'),
    (0, common_1.Controller)('v1/transaction-logs'),
    __metadata("design:paramtypes", [log_transaction_service_1.TransactionLogService])
], TransactionLogController);
exports.TransactionLogController = TransactionLogController;
//# sourceMappingURL=log-transaction.controller.js.map