import { TransactionLogPaginateDto } from '../dto/log-transaction.dto';
import { TransactionLogService } from '../services/log.transaction.service';
export declare class TransactionLogController {
    private readonly transactionLogService;
    constructor(transactionLogService: TransactionLogService);
    getAccessLog(query: TransactionLogPaginateDto): Promise<PaginateResult<import("../schemas/log-transaction.schema").TransactionLogDocument>>;
}
