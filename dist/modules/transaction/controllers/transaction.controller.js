"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const add_tips_dto_1 = require("../dto/add-tips.dto");
const close_of_day_dto_1 = require("../dto/close-of-day.dto");
const get_transaction_dto_1 = require("../dto/get-transaction.dto");
const refund_transaction_dto_1 = require("../dto/refund-transaction.dto");
const summary_close_dto_1 = require("../dto/summary-close.dto");
const void_transaction_dto_1 = require("../dto/void-transaction.dto");
const paginate_transaction_entity_1 = require("../entity/paginate-transaction.entity");
const pos_create_transaction_entity_1 = require("../entity/pos-create-transaction.entity");
const summary_close_entity_1 = require("../entity/summary-close.entity");
const void_transaction_entity_1 = require("../entity/void-transaction.entity");
const transaction_logic_1 = require("../logics/transaction.logic");
const transaction_service_1 = require("../services/transaction.service");
let TransactionController = class TransactionController {
    constructor(transactionService, transactionLogic) {
        this.transactionService = transactionService;
        this.transactionLogic = transactionLogic;
    }
    async getTransactions(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.transactionLogic.getTransactionList(query, restaurantId);
    }
    async getTransactionById(transactionId) {
        return await this.transactionService.findOne({
            _id: transactionId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
    }
    async voidTransaction(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.transactionLogic.voidTransaction(payload.transactionId, restaurantId);
    }
    async refundTransaction(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.transactionLogic.refundTransaction(payload, restaurantId);
    }
    async summaryCloseOfDay(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.transactionLogic.summaryCloseOfDay(query, restaurantId);
    }
    async closeBatch(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.transactionLogic.closeBatch(restaurantId, payload);
    }
    async tipsTransaction(payload) {
        return await this.transactionLogic.addTipLogic(payload);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => paginate_transaction_entity_1.PaginateTransactionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Transaction List',
        description: 'use *bearer* `restaurant_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_transaction_dto_1.TransactionPaginateDto]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getTransactions", null);
__decorate([
    (0, common_1.Get)(':transactionId'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_create_transaction_entity_1.CreateTransactionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Transaction By Transaction ID',
        description: 'use *bearer* `restaurant_token`'
    }),
    __param(0, (0, common_1.Param)('transactionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "getTransactionById", null);
__decorate([
    (0, common_1.Put)('void'),
    (0, swagger_1.ApiOkResponse)({ type: () => void_transaction_entity_1.VoidTransactionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Void Transaction By Transaction ID',
        description: 'use *bearer* `restaurant_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [void_transaction_dto_1.VoidTransactionDto]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "voidTransaction", null);
__decorate([
    (0, common_1.Put)('refund'),
    (0, swagger_1.ApiOkResponse)({ type: () => void_transaction_entity_1.VoidTransactionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Refund Transaction By Transaction ID',
        description: 'use *bearer* `restaurant_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [refund_transaction_dto_1.RefundTransactionDto]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "refundTransaction", null);
__decorate([
    (0, common_1.Get)('close-batch/summary'),
    (0, swagger_1.ApiOkResponse)({ type: () => summary_close_entity_1.WebBaseSummaryCloseOfDayResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Summary Transaction Before Close of Day',
        description: 'use *bearer* `restaurant_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [summary_close_dto_1.SummaryClosOfDayDto]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "summaryCloseOfDay", null);
__decorate([
    (0, common_1.Put)('close-batch'),
    (0, swagger_1.ApiOkResponse)({ type: () => void_transaction_entity_1.VoidTransactionResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Close Batch Payment/Run close of day',
        description: 'use *bearer* `restaurant_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [close_of_day_dto_1.CloseOfDayDto]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "closeBatch", null);
__decorate([
    (0, common_1.Patch)('tips'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Tips Transaction By Transaction ID',
        description: 'use *bearer* `restaurant_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [add_tips_dto_1.AddTipsDto]),
    __metadata("design:returntype", Promise)
], TransactionController.prototype, "tipsTransaction", null);
TransactionController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('transaction'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/transaction'),
    __metadata("design:paramtypes", [transaction_service_1.TransactionService,
        transaction_logic_1.TransactionLogic])
], TransactionController);
exports.TransactionController = TransactionController;
//# sourceMappingURL=transaction.controller.js.map