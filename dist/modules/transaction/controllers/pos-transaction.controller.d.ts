import { LogLogic } from 'src/modules/log/logic/log.logic';
import { AddTipsDto } from '../dto/add-tips.dto';
import { CloseOfDayDto } from '../dto/close-of-day.dto';
import { CreateTransactionPOSDto } from '../dto/create-transaction.dto';
import { TransactionPaginateDto } from '../dto/get-transaction.dto';
import { RefundTransactionDto } from '../dto/refund-transaction.dto';
import { SummaryClosOfDayDto } from '../dto/summary-close.dto';
import { VoidTransactionDto } from '../dto/void-transaction.dto';
import { POSTransactionLogic } from '../logics/pos-transaction.logic';
import { TransactionLogic } from '../logics/transaction.logic';
import { TransactionService } from '../services/transaction.service';
export declare class POSTransactionController {
    private readonly transactionService;
    private readonly transactionLogic;
    private readonly posTransactionLogic;
    private readonly logLogic;
    constructor(transactionService: TransactionService, transactionLogic: TransactionLogic, posTransactionLogic: POSTransactionLogic, logLogic: LogLogic);
    getTransactions(query: TransactionPaginateDto, request: any): Promise<any>;
    createTransaction(payload: CreateTransactionPOSDto, request: any): Promise<import("../schemas/transaction.schema").TransactionDocument>;
    getTransactionById(transactionId: string): Promise<import("../schemas/transaction.schema").TransactionDocument>;
    voidTransaction(payload: VoidTransactionDto): Promise<{
        success: boolean;
        response: any;
    }>;
    refundTransaction(payload: RefundTransactionDto): Promise<{
        success: boolean;
    }>;
    checkCloseOfDayStatus(payload: CloseOfDayDto): Promise<{
        isClosed: boolean;
    }>;
    summaryCloseOfDay(query: SummaryClosOfDayDto): Promise<{
        totalTransaction: number;
        totalCollected: number;
        netSales: number;
        summaryByPaymentMethod: {
            label: string;
            amount: number;
        }[];
    }>;
    closeBatch(payload: CloseOfDayDto): Promise<{
        success: boolean;
    }>;
    tipsTransaction(payload: AddTipsDto): Promise<{
        success: boolean;
    }>;
}
