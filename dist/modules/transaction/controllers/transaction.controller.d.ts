import { AddTipsDto } from '../dto/add-tips.dto';
import { CloseOfDayDto } from '../dto/close-of-day.dto';
import { TransactionPaginateDto } from '../dto/get-transaction.dto';
import { RefundTransactionDto } from '../dto/refund-transaction.dto';
import { SummaryClosOfDayDto } from '../dto/summary-close.dto';
import { VoidTransactionDto } from '../dto/void-transaction.dto';
import { TransactionLogic } from '../logics/transaction.logic';
import { TransactionService } from '../services/transaction.service';
export declare class TransactionController {
    private readonly transactionService;
    private readonly transactionLogic;
    constructor(transactionService: TransactionService, transactionLogic: TransactionLogic);
    getTransactions(query: TransactionPaginateDto): Promise<any>;
    getTransactionById(transactionId: string): Promise<import("../schemas/transaction.schema").TransactionDocument>;
    voidTransaction(payload: VoidTransactionDto): Promise<{
        success: boolean;
        response: any;
    }>;
    refundTransaction(payload: RefundTransactionDto): Promise<{
        success: boolean;
    }>;
    summaryCloseOfDay(query: SummaryClosOfDayDto): Promise<{
        totalTransaction: number;
        totalCollected: number;
        netSales: number;
        summaryByPaymentMethod: {
            label: string;
            amount: number;
        }[];
    }>;
    closeBatch(payload: CloseOfDayDto): Promise<{
        success: boolean;
    }>;
    tipsTransaction(payload: AddTipsDto): Promise<{
        success: boolean;
    }>;
}
