"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentStatusEnum = void 0;
var PaymentStatusEnum;
(function (PaymentStatusEnum) {
    PaymentStatusEnum["PAID"] = "PAID";
    PaymentStatusEnum["VOID"] = "VOID";
    PaymentStatusEnum["REFUNDED"] = "REFUNDED";
})(PaymentStatusEnum = exports.PaymentStatusEnum || (exports.PaymentStatusEnum = {}));
//# sourceMappingURL=payment-status.enum.js.map