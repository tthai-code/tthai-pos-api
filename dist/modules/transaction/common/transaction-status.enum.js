"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionStatusEnum = void 0;
var TransactionStatusEnum;
(function (TransactionStatusEnum) {
    TransactionStatusEnum["OPEN"] = "OPEN";
    TransactionStatusEnum["CLOSED"] = "CLOSED";
})(TransactionStatusEnum = exports.TransactionStatusEnum || (exports.TransactionStatusEnum = {}));
//# sourceMappingURL=transaction-status.enum.js.map