export declare enum PaymentMethodEnum {
    CASH = "CASH",
    CREDIT = "CREDIT",
    DEBIT = "DEBIT",
    THIRD_PARTY = "THIRD_PARTY",
    GIFT_CARD = "GIFT_CARD",
    CHECK = "CHECK",
    DELIVERY = "DELIVERY",
    DE_MINIMIS = "DE_MINIMIS",
    OTHER = "OTHER"
}
