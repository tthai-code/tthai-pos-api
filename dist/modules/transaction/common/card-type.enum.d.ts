export declare enum CardTypeEnum {
    AMEX = "AMEX",
    DISCOVER = "DISCOVER",
    MASTERCARD = "MASTERCARD",
    NON_CO_BRAND = "NON_CO_BRAND",
    VISA = "VISA",
    APPLE_PAY = "APPLE_PAY"
}
