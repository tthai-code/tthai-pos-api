"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardTypeEnum = void 0;
var CardTypeEnum;
(function (CardTypeEnum) {
    CardTypeEnum["AMEX"] = "AMEX";
    CardTypeEnum["DISCOVER"] = "DISCOVER";
    CardTypeEnum["MASTERCARD"] = "MASTERCARD";
    CardTypeEnum["NON_CO_BRAND"] = "NON_CO_BRAND";
    CardTypeEnum["VISA"] = "VISA";
    CardTypeEnum["APPLE_PAY"] = "APPLE_PAY";
})(CardTypeEnum = exports.CardTypeEnum || (exports.CardTypeEnum = {}));
//# sourceMappingURL=card-type.enum.js.map