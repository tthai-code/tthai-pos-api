export declare enum PaymentStatusEnum {
    PAID = "PAID",
    VOID = "VOID",
    REFUNDED = "REFUNDED"
}
