"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SplitMethodEnum = void 0;
var SplitMethodEnum;
(function (SplitMethodEnum) {
    SplitMethodEnum["NO_SPLIT"] = "NO_SPLIT";
    SplitMethodEnum["BY_ITEMS"] = "BY_ITEMS";
    SplitMethodEnum["BY_AMOUNT"] = "BY_AMOUNT";
    SplitMethodEnum["BY_AMOUNT_CUSTOM"] = "BY_AMOUNT_CUSTOM";
})(SplitMethodEnum = exports.SplitMethodEnum || (exports.SplitMethodEnum = {}));
//# sourceMappingURL=split-method.enum.js.map