"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentMethodEnum = void 0;
var PaymentMethodEnum;
(function (PaymentMethodEnum) {
    PaymentMethodEnum["CASH"] = "CASH";
    PaymentMethodEnum["CREDIT"] = "CREDIT";
    PaymentMethodEnum["DEBIT"] = "DEBIT";
    PaymentMethodEnum["THIRD_PARTY"] = "THIRD_PARTY";
    PaymentMethodEnum["GIFT_CARD"] = "GIFT_CARD";
    PaymentMethodEnum["CHECK"] = "CHECK";
    PaymentMethodEnum["DELIVERY"] = "DELIVERY";
    PaymentMethodEnum["DE_MINIMIS"] = "DE_MINIMIS";
    PaymentMethodEnum["OTHER"] = "OTHER";
})(PaymentMethodEnum = exports.PaymentMethodEnum || (exports.PaymentMethodEnum = {}));
//# sourceMappingURL=payment-method.enum.js.map