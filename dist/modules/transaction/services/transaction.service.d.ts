import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { TransactionDocument } from '../schemas/transaction.schema';
import { TransactionPaginateDto } from '../dto/get-transaction.dto';
export declare class TransactionService {
    private readonly TransactionModel;
    private request;
    constructor(TransactionModel: PaginateModel<TransactionDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<TransactionDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<TransactionDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<TransactionDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<TransactionDocument>;
    update(id: string, product: any): Promise<TransactionDocument>;
    updateOne(condition: any, payload: any): Promise<TransactionDocument>;
    updateMany(condition: any, payload: any): Promise<Array<any>>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<TransactionDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
    delete(id: string): Promise<TransactionDocument>;
    getAll(condition: any, project?: any): Promise<TransactionDocument[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<TransactionDocument>;
    paginate(query: any, queryParam: TransactionPaginateDto, select?: any): Promise<PaginateResult<TransactionDocument>>;
}
