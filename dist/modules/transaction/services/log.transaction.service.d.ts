import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { TransactionLogPaginateDto } from '../dto/log-transaction.dto';
import { TransactionLogDocument } from '../schemas/log-transaction.schema';
export declare class TransactionLogService {
    private transactionLogModel;
    private request;
    constructor(transactionLogModel: PaginateModel<TransactionLogDocument>, request: any);
    create(payload: any): Promise<TransactionLogDocument>;
    paginate(query: any, queryParam: TransactionLogPaginateDto): Promise<PaginateResult<TransactionLogDocument>>;
}
