import { Model } from 'mongoose';
import { TransactionCounterDocument } from '../schemas/transaction-counter.schema';
export declare class TransactionCounterService {
    private readonly TransactionCounterModel;
    constructor(TransactionCounterModel: Model<TransactionCounterDocument>);
    getCounter(prefix: string, restaurantId: string): Promise<number>;
}
