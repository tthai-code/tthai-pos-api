export interface ICustomerTransaction {
    id?: string;
    firstName?: string;
    lastName?: string;
    tel?: string;
}
export interface IModifierTransaction {
    id: string;
    label: string;
    abbreviation: string;
    name: string;
    nativeName: string;
    price: number;
}
export interface IOrderItemsTransaction {
    id: number;
    name: string;
    nativeName: string;
    price: number;
    modifiers?: IModifierTransaction[];
    isContainAlcohol: boolean;
    coverImage?: string;
    unit: number;
    amount: number;
    note?: string;
}
export interface IVoidTransaction {
    restaurantId: string;
    txn: string;
    orderId: string;
    porderId: string;
    orderType: string;
    customer: ICustomerTransaction;
    summaryItems: IOrderItemsTransaction[];
    subtotal?: number;
    discount?: number;
    serviceCharge?: number;
    tax?: number;
    alcoholTax?: number;
    convenienceFee?: number;
    total?: number;
    paymentMethod?: string;
    paymentStatus?: string;
    openDate?: Date | string;
    paidAmount?: number;
    totalPaid?: number;
    note?: string;
    isVoidBeforeTransaction?: boolean;
}
