"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authorStampCreatePlugin = void 0;
function stampAuthor(requestUser) {
    const result = {
        id: (requestUser === null || requestUser === void 0 ? void 0 : requestUser.id) || '0',
        username: (requestUser === null || requestUser === void 0 ? void 0 : requestUser.username) || 'system'
    };
    return result;
}
const authorStampCreatePlugin = (schema) => {
    schema.pre('save', function (next) {
        const doc = this;
        const requestUser = doc._requestUser;
        this.updatedBy = stampAuthor(requestUser);
        if (this.isNew) {
            this.createdBy = stampAuthor(requestUser);
        }
        next();
    });
    schema.pre('findOneAndUpdate', function (next) {
        const doc = this;
        const requestUser = doc._requestUser;
        this.updatedBy = stampAuthor(requestUser);
        next();
    });
    schema.pre('update', function (next) {
        const doc = this;
        const requestUser = doc._requestUser;
        this.updatedBy = stampAuthor(requestUser);
        next();
    });
    schema.pre('updateOne', function (next) {
        const doc = this;
        const requestUser = doc._requestUser;
        this.updatedBy = stampAuthor(requestUser);
        next();
    });
    schema.pre('updateMany', function (next) {
        const doc = this;
        const requestUser = doc._requestUser;
        this.updatedBy = stampAuthor(requestUser);
        next();
    });
    schema.pre('insertMany', function (next) {
        const doc = this;
        const requestUser = doc._requestUser;
        this.updatedBy = stampAuthor(requestUser);
        next();
    });
};
exports.authorStampCreatePlugin = authorStampCreatePlugin;
//# sourceMappingURL=author-stamp.plugin.js.map