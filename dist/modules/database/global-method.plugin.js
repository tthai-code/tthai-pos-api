"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.globalMethodPlugin = void 0;
const globalMethodPlugin = (schema) => {
    schema.method('setAuthor', function (request) {
        this._requestUser = request === null || request === void 0 ? void 0 : request.user;
    });
};
exports.globalMethodPlugin = globalMethodPlugin;
//# sourceMappingURL=global-method.plugin.js.map