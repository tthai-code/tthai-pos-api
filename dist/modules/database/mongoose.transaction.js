"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.runTransaction = void 0;
const common_1 = require("@nestjs/common");
const database_service_1 = require("./database.service");
async function runTransaction(transactionActivity, fallbackActivity, onCommitted) {
    let result;
    let rerun = true;
    while (rerun) {
        const session = await database_service_1.DatabaseService.startSession();
        session.startTransaction();
        try {
            result = await transactionActivity(session);
            await session.commitTransaction();
            rerun = false;
        }
        catch (error) {
            await session.abortTransaction();
            const caseWriteConflict = (error === null || error === void 0 ? void 0 : error.name) === 'MongoError' && (error === null || error === void 0 ? void 0 : error.code) === 112;
            const caseVersionError = (error === null || error === void 0 ? void 0 : error.name) === 'VersionError' && typeof (error === null || error === void 0 ? void 0 : error.version) === 'number';
            const isInExpectedErrorCase = caseWriteConflict || caseVersionError;
            if (!isInExpectedErrorCase) {
                rerun = false;
                if (!fallbackActivity) {
                    if (error.status) {
                        errorHandle(error);
                    }
                    else {
                        throw error;
                    }
                }
                await fallbackActivity(error);
            }
        }
        finally {
            session.endSession();
        }
    }
    await (onCommitted === null || onCommitted === void 0 ? void 0 : onCommitted(result));
    return result;
}
exports.runTransaction = runTransaction;
const errorHandle = (error) => {
    switch (error.status) {
        case 400:
            throw new common_1.BadRequestException(error.message);
        case 401:
            throw new common_1.UnauthorizedException(error.message);
        case 403:
            throw new common_1.ForbiddenException(error.message);
        case 404:
            throw new common_1.NotFoundException(error.message);
        case 502:
            throw new common_1.BadGatewayException(error.message);
        default:
            throw new common_1.InternalServerErrorException(error.message);
    }
};
//# sourceMappingURL=mongoose.transaction.js.map