import { Connection, ClientSession, Model, Document } from 'mongoose';
export declare class DatabaseService {
    readonly connection: Connection;
    private static _this;
    constructor(connection: Connection);
    static getModel<T extends Document = any>(modelName: any): Model<T>;
    static startSession(): Promise<ClientSession>;
}
