import * as mongoose from 'mongoose';
export declare function runTransaction<T>(transactionActivity: (session: mongoose.ClientSession) => Promise<T>, fallbackActivity?: (errors: any) => Promise<T | void>, onCommitted?: (result?: T) => any): Promise<T>;
