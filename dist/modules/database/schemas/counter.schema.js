"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CounterSchema = void 0;
const mongoose_1 = require("mongoose");
exports.CounterSchema = new mongoose_1.Schema({
    _id: {
        type: String,
        required: true
    },
    seq: {
        type: Number,
        default: 0
    }
}, { collection: '_counters' });
//# sourceMappingURL=counter.schema.js.map