"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updatePrinterResponse = void 0;
exports.updatePrinterResponse = {
    schema: {
        type: 'object',
        properties: {
            message: { type: 'string', example: 'done' },
            data: {
                type: 'object',
                properties: {},
                example: {
                    created_by: {
                        username: 'corywong@mail.com',
                        id: '630e53ec9e21d871a49fb4f5'
                    },
                    updated_by: {
                        username: 'corywong@mail.com',
                        id: '630e53ec9e21d871a49fb4f5'
                    },
                    status: 'active',
                    printer_setting: {
                        receipt_ip_printer: '0.0.0.0',
                        kitchen_ip_printer: '0.0.0.0'
                    },
                    pos_processing_fee: {
                        debit_card: 0.25,
                        credit_card: 0.5
                    },
                    receipt_setting: {
                        language: 'ENG',
                        font_size: 'NORMAL',
                        text: '',
                        is_display_text: false,
                        is_display_logo: false
                    },
                    tax_rate: {
                        alcohol_tax: 12,
                        tax: 10
                    },
                    logo_url: null,
                    sales_tax_permit_document: 'https://example.com/####/####',
                    ein_document: 'https://example.com/####/####',
                    referred_by: 'TEST02',
                    email: 'corywong@mail.com',
                    phone: '093243222',
                    title: 'Owner',
                    authorized_person_last_name: 'Wong',
                    authorized_person_first_name: 'Cory',
                    business_phone: '0222222222',
                    address: {
                        note: 'Cory Wong Test',
                        zip_code: '75001',
                        state: 'Dallas',
                        city: 'Addison',
                        address: 'Some Address'
                    },
                    ein: '983542321',
                    dba: 'dba',
                    legal_business_name: 'Holy Beef',
                    account_id: '630e53ec9e21d871a49fb4f5',
                    updated_at: '2022-09-14T08:25:28.611Z',
                    created_at: '2022-08-30T18:23:28.343Z',
                    id: '630e55a0d9c30fd7cdcb424b'
                }
            }
        }
    }
};
//# sourceMappingURL=update-printer-res.js.map