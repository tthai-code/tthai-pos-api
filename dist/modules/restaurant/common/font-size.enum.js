"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FontSizeEnum = void 0;
var FontSizeEnum;
(function (FontSizeEnum) {
    FontSizeEnum["SMALL"] = "SMALL";
    FontSizeEnum["NORMAL"] = "NORMAL";
    FontSizeEnum["BIG"] = "BIG";
})(FontSizeEnum = exports.FontSizeEnum || (exports.FontSizeEnum = {}));
//# sourceMappingURL=font-size.enum.js.map