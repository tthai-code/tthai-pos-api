export declare const updatePrinterResponse: {
    schema: {
        type: string;
        properties: {
            message: {
                type: string;
                example: string;
            };
            data: {
                type: string;
                properties: {};
                example: {
                    created_by: {
                        username: string;
                        id: string;
                    };
                    updated_by: {
                        username: string;
                        id: string;
                    };
                    status: string;
                    printer_setting: {
                        receipt_ip_printer: string;
                        kitchen_ip_printer: string;
                    };
                    pos_processing_fee: {
                        debit_card: number;
                        credit_card: number;
                    };
                    receipt_setting: {
                        language: string;
                        font_size: string;
                        text: string;
                        is_display_text: boolean;
                        is_display_logo: boolean;
                    };
                    tax_rate: {
                        alcohol_tax: number;
                        tax: number;
                    };
                    logo_url: any;
                    sales_tax_permit_document: string;
                    ein_document: string;
                    referred_by: string;
                    email: string;
                    phone: string;
                    title: string;
                    authorized_person_last_name: string;
                    authorized_person_first_name: string;
                    business_phone: string;
                    address: {
                        note: string;
                        zip_code: string;
                        state: string;
                        city: string;
                        address: string;
                    };
                    ein: string;
                    dba: string;
                    legal_business_name: string;
                    account_id: string;
                    updated_at: string;
                    created_at: string;
                    id: string;
                };
            };
        };
    };
};
