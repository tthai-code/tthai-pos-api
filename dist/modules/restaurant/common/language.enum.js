"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LanguageEnum = void 0;
var LanguageEnum;
(function (LanguageEnum) {
    LanguageEnum["ENG"] = "ENG";
    LanguageEnum["THAI"] = "THAI";
    LanguageEnum["ENGTHAI"] = "ENG/THAI";
})(LanguageEnum = exports.LanguageEnum || (exports.LanguageEnum = {}));
//# sourceMappingURL=language.enum.js.map