"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetACHResponse = exports.AdminRestaurantInfoResponse = exports.AdminRestaurantPaginateResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const ach_bank_type_enum_1 = require("../../payment-gateway/common/ach-bank.type.enum");
class AdminRestaurantPaginateObject extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: [
            {
                default_service_charge: 10,
                status: 'active',
                printer_setting: {
                    receipt_ip_printer: '',
                    kitchen_ip_printer: ''
                },
                pos_processing_fee: {
                    debit_card: 0.25,
                    credit_card: 0.5
                },
                receipt_setting: {
                    language: 'ENG',
                    font_size: 'NORMAL',
                    text: '',
                    is_display_text: false,
                    is_display_logo: false
                },
                tax_rate: {
                    is_alcohol_tax_active: false,
                    alcohol_tax: 0,
                    tax: 0
                },
                logo_url: null,
                sales_tax_permit_document: 'https://example.com/####/####',
                ein_document: 'https://example.com/####/####',
                referred_by: '',
                email: 'newjeans@gmail.com',
                phone: '093243221',
                title: 'Owner',
                authorized_person_last_name: 'Jeans',
                authorized_person_first_name: 'New',
                business_phone: '0222222233',
                address: {
                    zip_code: '75001',
                    state: 'Dallas',
                    city: 'Addison',
                    address: 'Some Address'
                },
                ein: '983542323',
                dba: 'New Jeans',
                legal_business_name: 'New Jeans',
                account_id: '633d3606a9c503069fb8566f',
                created_at: '2022-10-09T21:34:57.241Z',
                updated_at: '2022-10-09T21:34:57.241Z',
                updated_by: {
                    username: 'newjeans@gmail.com',
                    id: '633d3606a9c503069fb8566f'
                },
                created_by: {
                    username: 'newjeans@gmail.com',
                    id: '633d3606a9c503069fb8566f'
                },
                id: '63433e8145728de99075847a'
            }
        ]
    }),
    __metadata("design:type", Object)
], AdminRestaurantPaginateObject.prototype, "results", void 0);
class AdminRestaurantPaginateResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: AdminRestaurantPaginateObject }),
    __metadata("design:type", Object)
], AdminRestaurantPaginateResponse.prototype, "data", void 0);
exports.AdminRestaurantPaginateResponse = AdminRestaurantPaginateResponse;
class ProfileObject {
}
class PaymentGatewayObject {
}
class ACHProfileObject {
}
class POSProcessingFeeObject {
}
class AdminRestaurantInfo {
}
class AdminRestaurantInfoResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            profile: {
                id: '630e55a0d9c30fd7cdcb424b',
                legal_business_name: 'Holy Beef',
                dba: 'Test Doing as Business',
                ein: '983542321',
                address: 'Some Address',
                city: 'Addison',
                state: 'Dallas',
                zip_code: '75001',
                phone: '093243222',
                email: 'corywong@mail.com'
            },
            payment_gateway: {
                merchant_id: '820000003069',
                status: 'ACTIVE',
                payment_credit_card_charges: 'Interchange Plus'
            },
            ach_payment_file: [
                {
                    date: '2022-11-24T10:48:15.658Z',
                    id: '6384568aa13f6c6b60e231b5'
                },
                {
                    date: '2022-11-24T10:48:15.658Z',
                    id: '6384560fc8e1b6d47c475c86'
                },
                {
                    date: '2022-11-24T10:48:15.658Z',
                    id: '63844ba6438534c4ecaf0b49'
                },
                {
                    date: '2022-11-24T10:48:15.658Z',
                    id: '63844b25c05b5be2ac6c6657'
                },
                {
                    date: '2022-11-24T10:48:15.658Z',
                    id: '637f4c0cbf5a8f91bfb59f0f'
                }
            ],
            pos_processing_fee: {
                credit_card: 0.5,
                debit_card: 0.25
            }
        }
    }),
    __metadata("design:type", AdminRestaurantInfo)
], AdminRestaurantInfoResponse.prototype, "data", void 0);
exports.AdminRestaurantInfoResponse = AdminRestaurantInfoResponse;
class AdminACHObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(ach_bank_type_enum_1.BankAccountStatusEnum) }),
    __metadata("design:type", String)
], AdminACHObject.prototype, "bank_account_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "token", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "signature", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "routing_number", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "account_number", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(ach_bank_type_enum_1.ACHBankAccountTypeEnum) }),
    __metadata("design:type", String)
], AdminACHObject.prototype, "account_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "title", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "print_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AdminACHObject.prototype, "id", void 0);
class GetACHResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: AdminACHObject,
        example: {
            bank_account_status: 'INACTIVE',
            token: '9031395982141234',
            date: '2022-11-24T10:48:15.658Z',
            signature: 'Cory Wong 1',
            routing_number: 'XXXXXX808',
            account_number: 'XXXXXXXXXXXX1234',
            account_type: 'SAVING',
            title: 'Mr.',
            print_name: 'Cory Wong',
            name: 'Cory Wong 1',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            id: '63844b25c05b5be2ac6c6657'
        }
    }),
    __metadata("design:type", AdminACHObject)
], GetACHResponse.prototype, "data", void 0);
exports.GetACHResponse = GetACHResponse;
//# sourceMappingURL=admin-restaurant.entity.js.map