"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TaxAndServiceChargeResponse = exports.GetPOSRestaurantInfoResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class TaxRateFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], TaxRateFields.prototype, "is_alcohol_tax_active", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TaxRateFields.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TaxRateFields.prototype, "tax", void 0);
class TaxAndServiceChargeFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TaxAndServiceChargeFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: TaxRateFields }),
    __metadata("design:type", TaxRateFields)
], TaxAndServiceChargeFields.prototype, "tax_rate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TaxAndServiceChargeFields.prototype, "defaultServiceCharge", void 0);
class AddressFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "zip_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "address", void 0);
class RestaurantFields extends TaxAndServiceChargeFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "time_zone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "logo_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "authorized_person_last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "authorized_person_first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: AddressFields }),
    __metadata("design:type", AddressFields)
], RestaurantFields.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "dba", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "legal_business_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "authorized_person_name", void 0);
class GetPOSRestaurantInfoResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: RestaurantFields,
        example: {
            time_zone: 'Etc/UTC',
            default_service_charge: 10,
            tax_rate: {
                is_alcohol_tax_active: false,
                alcohol_tax: 0,
                tax: 10
            },
            logo_url: null,
            email: 'test03@gmail.com',
            authorized_person_last_name: 'Norton',
            authorized_person_first_name: 'Edward',
            business_phone: '1234567890',
            address: {
                zip_code: '90263',
                state: 'CA',
                city: 'Malibu',
                address: 'TEestst Address'
            },
            dba: 'Test DBA 03',
            legal_business_name: 'TeST 03',
            authorized_person_name: 'Edward Norton',
            id: '63984d6690416c27a7415915'
        }
    }),
    __metadata("design:type", RestaurantFields)
], GetPOSRestaurantInfoResponse.prototype, "data", void 0);
exports.GetPOSRestaurantInfoResponse = GetPOSRestaurantInfoResponse;
class TaxAndServiceChargeResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: TaxAndServiceChargeFields,
        example: {
            tax_rate: {
                is_alcohol_tax_active: false,
                alcohol_tax: 12.5,
                tax: 10
            },
            default_service_charge: 10,
            id: '630e55a0d9c30fd7cdcb424b'
        }
    }),
    __metadata("design:type", TaxAndServiceChargeFields)
], TaxAndServiceChargeResponse.prototype, "data", void 0);
exports.TaxAndServiceChargeResponse = TaxAndServiceChargeResponse;
//# sourceMappingURL=pos-restaurant.entity.js.map