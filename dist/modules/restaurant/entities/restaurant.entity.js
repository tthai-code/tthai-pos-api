"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantEntity = void 0;
const swagger_1 = require("@nestjs/swagger");
class AddressResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: 'Some Address' }),
    __metadata("design:type", String)
], AddressResponse.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 'Addison' }),
    __metadata("design:type", String)
], AddressResponse.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 'Dallas' }),
    __metadata("design:type", String)
], AddressResponse.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: '75001' }),
    __metadata("design:type", String)
], AddressResponse.prototype, "zip_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 'test' }),
    __metadata("design:type", String)
], AddressResponse.prototype, "note", void 0);
class ResultsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: '6308974b19416019c26ae15d' }),
    __metadata("design:type", String)
], ResultsResponse.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 'Tthai Restaurant' }),
    __metadata("design:type", String)
], ResultsResponse.prototype, "legal_business_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: '0111111111' }),
    __metadata("design:type", String)
], ResultsResponse.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 'test@mail.com' }),
    __metadata("design:type", String)
], ResultsResponse.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => AddressResponse }),
    __metadata("design:type", AddressResponse)
], ResultsResponse.prototype, "address", void 0);
class DataResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: 1 }),
    __metadata("design:type", Number)
], DataResponse.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 1 }),
    __metadata("design:type", Number)
], DataResponse.prototype, "limit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 1 }),
    __metadata("design:type", Number)
], DataResponse.prototype, "page", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: 1 }),
    __metadata("design:type", Number)
], DataResponse.prototype, "pages", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => [ResultsResponse] }),
    __metadata("design:type", Array)
], DataResponse.prototype, "results", void 0);
class RestaurantEntity {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: 'done' }),
    __metadata("design:type", String)
], RestaurantEntity.prototype, "message", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => DataResponse }),
    __metadata("design:type", DataResponse)
], RestaurantEntity.prototype, "data", void 0);
exports.RestaurantEntity = RestaurantEntity;
//# sourceMappingURL=restaurant.entity.js.map