import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class TaxRateFields {
    is_alcohol_tax_active: boolean;
    alcohol_tax: number;
    tax: number;
}
declare class AddressFields {
    zip_code: string;
    state: string;
    city: string;
    address: string;
}
declare class GetRestaurantFields {
    id: string;
    default_service_charge: number;
    tax_rate: TaxRateFields;
    logo_url: string;
    email: string;
    business_phone: string;
    address: AddressFields;
    doing_business_as: string;
    legal_business_name: string;
}
declare class PaginateRestaurantOpenFields extends PaginateResponseDto<Array<GetRestaurantFields>> {
    results: Array<GetRestaurantFields>;
}
export declare class PaginateRestaurantOpenResponse extends ResponseDto<PaginateRestaurantOpenFields> {
    data: PaginateRestaurantOpenFields;
}
export declare class OpenGetRestaurantResponse extends ResponseDto<GetRestaurantFields> {
    data: GetRestaurantFields;
}
export {};
