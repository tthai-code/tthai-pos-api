import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class AdminRestaurantPaginateObject extends PaginateResponseDto<any> {
    results: any;
}
export declare class AdminRestaurantPaginateResponse extends ResponseDto<AdminRestaurantPaginateObject> {
    data: any;
}
declare class ProfileObject {
    id: string;
    legal_business_name: string;
    dba: string;
    ein: string;
    address: string;
    city: string;
    state: string;
    zip_code: string;
    phone: string;
    email: string;
}
declare class PaymentGatewayObject {
    merchant_id: string;
    status: string;
    payment_credit_card_charges: string;
}
declare class ACHProfileObject {
    date: string;
    id: string;
}
declare class POSProcessingFeeObject {
    credit_card: number;
    debit_card: number;
}
declare class AdminRestaurantInfo {
    profile: ProfileObject;
    payment_gateway: PaymentGatewayObject;
    ach_payment_file: ACHProfileObject[];
    pos_processing_fee: POSProcessingFeeObject;
}
export declare class AdminRestaurantInfoResponse extends ResponseDto<AdminRestaurantInfo> {
    data: AdminRestaurantInfo;
}
declare class AdminACHObject {
    bank_account_status: string;
    token: string;
    date: string;
    signature: string;
    routing_number: string;
    account_number: string;
    account_type: string;
    title: string;
    print_name: string;
    name: string;
    restaurant_id: string;
    id: string;
}
export declare class GetACHResponse extends ResponseDto<AdminACHObject> {
    data: AdminACHObject;
}
export {};
