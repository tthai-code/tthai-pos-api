import { ResponseDto } from 'src/common/entity/response.entity';
declare class TaxRateFields {
    is_alcohol_tax_active: boolean;
    alcohol_tax: number;
    tax: number;
}
declare class TaxAndServiceChargeFields {
    id: string;
    tax_rate: TaxRateFields;
    defaultServiceCharge: number;
}
declare class AddressFields {
    zip_code: string;
    state: string;
    city: string;
    address: string;
}
declare class RestaurantFields extends TaxAndServiceChargeFields {
    time_zone: string;
    logo_url: string;
    email: string;
    authorized_person_last_name: string;
    authorized_person_first_name: string;
    business_phone: string;
    address: AddressFields;
    dba: string;
    legal_business_name: string;
    authorized_person_name: string;
}
export declare class GetPOSRestaurantInfoResponse extends ResponseDto<RestaurantFields> {
    data: RestaurantFields;
}
export declare class TaxAndServiceChargeResponse extends ResponseDto<TaxAndServiceChargeFields> {
    data: TaxAndServiceChargeFields;
}
export {};
