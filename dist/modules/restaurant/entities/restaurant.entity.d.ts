declare class AddressResponse {
    readonly address: string;
    readonly city: string;
    readonly state: string;
    readonly zip_code: string;
    readonly note: string;
}
declare class ResultsResponse {
    id: string;
    legal_business_name: string;
    business_phone: string;
    email: string;
    address: AddressResponse;
}
declare class DataResponse {
    total: number;
    limit: number;
    page: number;
    pages: number;
    results: Array<ResultsResponse>;
}
export declare class RestaurantEntity {
    message: string;
    data: DataResponse;
}
export {};
