"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenGetRestaurantResponse = exports.PaginateRestaurantOpenResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
class TaxRateFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], TaxRateFields.prototype, "is_alcohol_tax_active", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TaxRateFields.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TaxRateFields.prototype, "tax", void 0);
class AddressFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "zip_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressFields.prototype, "address", void 0);
class GetRestaurantFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetRestaurantFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], GetRestaurantFields.prototype, "default_service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", TaxRateFields)
], GetRestaurantFields.prototype, "tax_rate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetRestaurantFields.prototype, "logo_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetRestaurantFields.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetRestaurantFields.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", AddressFields)
], GetRestaurantFields.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetRestaurantFields.prototype, "doing_business_as", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetRestaurantFields.prototype, "legal_business_name", void 0);
class PaginateRestaurantOpenFields extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [GetRestaurantFields],
        example: [
            {
                tax_rate: {
                    is_alcohol_tax_active: false,
                    alcohol_tax: 25.2,
                    tax: 14
                },
                logo_url: null,
                email: 'info@carolinaswise.com',
                business_phone: '5102638342',
                address: {
                    zip_code: '94501',
                    state: 'CA',
                    city: 'Alameda',
                    address: '1319 Park St'
                },
                legal_business_name: 'May Thai Kitchen LLC',
                default_service_charge: 10,
                doing_business_as: 'May Thai Kitchen',
                id: '630eff5751c2eac55f52662c'
            }
        ]
    }),
    __metadata("design:type", Array)
], PaginateRestaurantOpenFields.prototype, "results", void 0);
class PaginateRestaurantOpenResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: PaginateRestaurantOpenFields
    }),
    __metadata("design:type", PaginateRestaurantOpenFields)
], PaginateRestaurantOpenResponse.prototype, "data", void 0);
exports.PaginateRestaurantOpenResponse = PaginateRestaurantOpenResponse;
class OpenGetRestaurantResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: GetRestaurantFields,
        example: {
            tax_rate: {
                is_alcohol_tax_active: false,
                alcohol_tax: 25.2,
                tax: 14
            },
            logo_url: null,
            email: 'info@carolinaswise.com',
            business_phone: '5102638342',
            address: {
                zip_code: '94501',
                state: 'CA',
                city: 'Alameda',
                address: '1319 Park St'
            },
            legal_business_name: 'May Thai Kitchen LLC',
            default_service_charge: 10,
            doing_business_as: 'May Thai Kitchen',
            id: '630eff5751c2eac55f52662c'
        }
    }),
    __metadata("design:type", GetRestaurantFields)
], OpenGetRestaurantResponse.prototype, "data", void 0);
exports.OpenGetRestaurantResponse = OpenGetRestaurantResponse;
//# sourceMappingURL=restaurant-open.entity.js.map