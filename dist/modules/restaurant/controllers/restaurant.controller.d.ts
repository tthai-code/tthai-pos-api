/// <reference types="mongoose" />
import { RestaurantPaginateDto } from '../dto/get-restaurant.dto';
import { CreateRestaurantDto } from '../dto/create-restaurant.dto';
import { RestaurantService } from '../services/restaurant.service';
import { RestaurantLogic } from '../logics/restaurant.logic';
import { SetTaxRestaurantDto } from '../dto/set-tax-restaurant.dto';
import { UpdateRestaurantProfile } from '../dto/update-restaurant-profile.dto';
import { SetServiceCharge } from '../dto/set-service-charge.dto';
export declare class RestaurantController {
    private readonly restaurantService;
    private readonly restaurantLogic;
    constructor(restaurantService: RestaurantService, restaurantLogic: RestaurantLogic);
    getRestaurants(query: RestaurantPaginateDto): Promise<PaginateResult<import("../schemas/restaurant.schema").RestaurantDocument>>;
    getRestaurant(id: number): Promise<any>;
    createRestaurant(restaurant: CreateRestaurantDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        ownerName: string;
        restaurant: import("../schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
    updateRestaurant(Restaurant: UpdateRestaurantProfile, id: string): Promise<import("../schemas/restaurant.schema").RestaurantDocument>;
    deleteRestaurant(id: string): Promise<import("../schemas/restaurant.schema").RestaurantDocument>;
    setTaxRestaurant(id: string, payload: SetTaxRestaurantDto): Promise<import("../schemas/restaurant.schema").RestaurantDocument>;
    setServiceCharge(id: string, payload: SetServiceCharge): Promise<import("../schemas/restaurant.schema").RestaurantDocument>;
    getTaxRestaurant(id: string): Promise<import("../schemas/restaurant.schema").RestaurantDocument>;
    getInfoAvailable(): Promise<{
        bankAccount: boolean;
        paymentGateway: boolean;
        subscription: any[];
    }>;
}
