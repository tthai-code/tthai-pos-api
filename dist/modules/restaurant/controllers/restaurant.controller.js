"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const get_restaurant_dto_1 = require("../dto/get-restaurant.dto");
const create_restaurant_dto_1 = require("../dto/create-restaurant.dto");
const restaurant_service_1 = require("../services/restaurant.service");
const restaurant_entity_1 = require("../entities/restaurant.entity");
const restaurant_logic_1 = require("../logics/restaurant.logic");
const set_tax_restaurant_dto_1 = require("../dto/set-tax-restaurant.dto");
const get_restaurant_by_id_response_1 = require("../response/get-restaurant-by-id.response");
const update_restaurant_profile_dto_1 = require("../dto/update-restaurant-profile.dto");
const set_service_charge_dto_1 = require("../dto/set-service-charge.dto");
let RestaurantController = class RestaurantController {
    constructor(restaurantService, restaurantLogic) {
        this.restaurantService = restaurantService;
        this.restaurantLogic = restaurantLogic;
    }
    async getRestaurants(query) {
        return await this.restaurantService.paginate(query.buildQuery(), query);
    }
    async getRestaurant(id) {
        const raw = await this.restaurantService.findById(id);
        const transform = raw.toObject();
        transform.authorizedPersonName = `${transform.authorizedPersonFirstName} ${transform.authorizedPersonLastName}`;
        delete transform.authorizedPersonFirstName;
        delete transform.authorizedPersonLastName;
        return transform;
    }
    async createRestaurant(restaurant) {
        var _a;
        const username = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.username;
        return this.restaurantLogic.CreateLogic(restaurant, username);
    }
    async updateRestaurant(Restaurant, id) {
        return this.restaurantLogic.updateRestaurantProfile(id, Restaurant);
    }
    async deleteRestaurant(id) {
        return this.restaurantService.update(id, { status: status_enum_1.StatusEnum.DELETED });
    }
    async setTaxRestaurant(id, payload) {
        return this.restaurantService.update(id, { taxRate: payload });
    }
    async setServiceCharge(id, payload) {
        return this.restaurantService.update(id, payload);
    }
    async getTaxRestaurant(id) {
        return await this.restaurantService.findOneWithSelect({ _id: id }, { taxRate: 1 });
    }
    async getInfoAvailable() {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.restaurantLogic.getInfoAvailable(restaurantId);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Get Restaurant List' }),
    (0, swagger_1.ApiOkResponse)({ type: [restaurant_entity_1.RestaurantEntity] }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_restaurant_dto_1.RestaurantPaginateDto]),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "getRestaurants", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)(get_restaurant_by_id_response_1.getRestaurantByIdResponse),
    (0, swagger_1.ApiOperation)({ summary: 'Get Restaurant By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "getRestaurant", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create Restaurant' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_restaurant_dto_1.CreateRestaurantDto]),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "createRestaurant", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Restaurant Profile' }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_restaurant_profile_dto_1.UpdateRestaurantProfile, String]),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "updateRestaurant", null);
__decorate([
    (0, common_1.Delete)(),
    (0, swagger_1.ApiOperation)({ summary: 'Delete Restaurant' }),
    __param(0, (0, common_1.Query)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "deleteRestaurant", null);
__decorate([
    (0, common_1.Patch)(':id/tax'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Tax Setting' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, set_tax_restaurant_dto_1.SetTaxRestaurantDto]),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "setTaxRestaurant", null);
__decorate([
    (0, common_1.Patch)(':id/service-charge'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Service Charge Setting' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, set_service_charge_dto_1.SetServiceCharge]),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "setServiceCharge", null);
__decorate([
    (0, common_1.Get)(':id/tax'),
    (0, swagger_1.ApiOperation)({ summary: 'Get Tax Setting' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "getTaxRestaurant", null);
__decorate([
    (0, common_1.Get)('info/available'),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Restaurant Info Available Feature',
        description: 'use bearer `restaurant_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], RestaurantController.prototype, "getInfoAvailable", null);
RestaurantController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('restaurant'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/restaurant'),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        restaurant_logic_1.RestaurantLogic])
], RestaurantController);
exports.RestaurantController = RestaurantController;
//# sourceMappingURL=restaurant.controller.js.map