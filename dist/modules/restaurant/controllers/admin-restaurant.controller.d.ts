import { RestaurantPaginateDto } from '../dto/get-restaurant.dto';
import { EditDebitCreditFeeDto } from '../dto/pos-fee.dto';
import { AdminRestaurantLogic } from '../logics/admin-restaurant.logic';
import { RestaurantService } from '../services/restaurant.service';
export declare class AdminRestaurantController {
    private readonly restaurantService;
    private readonly adminRestaurantLogic;
    constructor(restaurantService: RestaurantService, adminRestaurantLogic: AdminRestaurantLogic);
    getRestaurants(query: RestaurantPaginateDto): Promise<PaginateResult<import("../schemas/restaurant.schema").RestaurantDocument>>;
    getRestaurantInfo(id: string): Promise<import("../interfaces/admin-restaurant.interface").IRestaurantInfo>;
    getACHById(id: string): Promise<import("../../payment-gateway/schemas/bank-account.schema").BankAccountDocument>;
    editCreditFee(restaurantId: string, payload: EditDebitCreditFeeDto): Promise<{
        success: boolean;
    }>;
    editDebitFee(restaurantId: string, payload: EditDebitCreditFeeDto): Promise<{
        success: boolean;
    }>;
    activateRestaurant(restaurantId: string): Promise<{
        success: boolean;
        id: string;
    }>;
    deactivateRestaurant(restaurantId: string): Promise<{
        success: boolean;
        id: string;
    }>;
    deleteRestaurant(restaurantId: string): Promise<{
        success: boolean;
        id: string;
    }>;
}
