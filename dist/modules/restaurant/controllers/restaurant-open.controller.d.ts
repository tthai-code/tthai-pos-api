import { RestaurantPaginateDto } from '../dto/get-restaurant.dto';
import { RestaurantService } from '../services/restaurant.service';
export declare class RestaurantOpenController {
    private readonly restaurantService;
    constructor(restaurantService: RestaurantService);
    getRestaurants(query: RestaurantPaginateDto): Promise<PaginateResult<import("../schemas/restaurant.schema").RestaurantDocument>>;
    getRestaurant(id: string): Promise<import("../schemas/restaurant.schema").RestaurantDocument>;
}
