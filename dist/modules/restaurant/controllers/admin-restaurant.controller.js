"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRestaurantController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const status_enum_1 = require("../../../common/enum/status.enum");
const super_admin_auth_guard_1 = require("../../auth/guards/super-admin-auth.guard");
const get_restaurant_dto_1 = require("../dto/get-restaurant.dto");
const pos_fee_dto_1 = require("../dto/pos-fee.dto");
const admin_restaurant_entity_1 = require("../entities/admin-restaurant.entity");
const admin_restaurant_logic_1 = require("../logics/admin-restaurant.logic");
const restaurant_service_1 = require("../services/restaurant.service");
let AdminRestaurantController = class AdminRestaurantController {
    constructor(restaurantService, adminRestaurantLogic) {
        this.restaurantService = restaurantService;
        this.adminRestaurantLogic = adminRestaurantLogic;
    }
    async getRestaurants(query) {
        return await this.restaurantService.paginate(query.buildQuery(), query);
    }
    async getRestaurantInfo(id) {
        return await this.adminRestaurantLogic.getRestaurantInfo(id);
    }
    async getACHById(id) {
        return await this.adminRestaurantLogic.getBankAccount(id);
    }
    async editCreditFee(restaurantId, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found Restaurant.');
        const { posProcessingFee } = restaurant;
        posProcessingFee.creditCard = +payload.fee;
        await this.restaurantService.update(restaurantId, {
            posProcessingFee
        });
        return { success: true };
    }
    async editDebitFee(restaurantId, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found Restaurant.');
        const { posProcessingFee } = restaurant;
        posProcessingFee.debitCard = +payload.fee;
        await this.restaurantService.update(restaurantId, {
            posProcessingFee
        });
        return { success: true };
    }
    async activateRestaurant(restaurantId) {
        return await this.adminRestaurantLogic.updateRestaurantStatus(restaurantId, status_enum_1.StatusEnum.ACTIVE);
    }
    async deactivateRestaurant(restaurantId) {
        return await this.adminRestaurantLogic.updateRestaurantStatus(restaurantId, status_enum_1.StatusEnum.INACTIVE);
    }
    async deleteRestaurant(restaurantId) {
        return await this.adminRestaurantLogic.updateRestaurantStatus(restaurantId, status_enum_1.StatusEnum.DELETED);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_restaurant_entity_1.AdminRestaurantPaginateResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Restaurant List',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_restaurant_dto_1.RestaurantPaginateDto]),
    __metadata("design:returntype", Promise)
], AdminRestaurantController.prototype, "getRestaurants", null);
__decorate([
    (0, common_1.Get)(':restaurantId/info'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_restaurant_entity_1.AdminRestaurantInfoResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Restaurant Info',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminRestaurantController.prototype, "getRestaurantInfo", null);
__decorate([
    (0, common_1.Get)(':achId/ach'),
    (0, swagger_1.ApiOkResponse)({ type: () => admin_restaurant_entity_1.GetACHResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get ACH by ID',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('achId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminRestaurantController.prototype, "getACHById", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/credit-fee'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Edit POS Processing Credit Fee',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, pos_fee_dto_1.EditDebitCreditFeeDto]),
    __metadata("design:returntype", Promise)
], AdminRestaurantController.prototype, "editCreditFee", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/debit-fee'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Edit POS Processing Debit Fee',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, pos_fee_dto_1.EditDebitCreditFeeDto]),
    __metadata("design:returntype", Promise)
], AdminRestaurantController.prototype, "editDebitFee", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/activate'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessWithIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Activate Restaurant by ID',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminRestaurantController.prototype, "activateRestaurant", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/deactivate'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessWithIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Deactivate Restaurant by ID',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminRestaurantController.prototype, "deactivateRestaurant", null);
__decorate([
    (0, common_1.Delete)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessWithIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Delete Restaurant by ID',
        description: 'use bearer `admin_token`'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminRestaurantController.prototype, "deleteRestaurant", null);
AdminRestaurantController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('admin/restaurant'),
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    (0, common_1.Controller)('v1/admin/restaurant'),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        admin_restaurant_logic_1.AdminRestaurantLogic])
], AdminRestaurantController);
exports.AdminRestaurantController = AdminRestaurantController;
//# sourceMappingURL=admin-restaurant.controller.js.map