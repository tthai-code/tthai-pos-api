"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantOpenController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const get_restaurant_dto_1 = require("../dto/get-restaurant.dto");
const restaurant_open_entity_1 = require("../entities/restaurant-open.entity");
const restaurant_service_1 = require("../services/restaurant.service");
let RestaurantOpenController = class RestaurantOpenController {
    constructor(restaurantService) {
        this.restaurantService = restaurantService;
    }
    async getRestaurants(query) {
        return await this.restaurantService.paginate(query.buildQuery(), query, {
            legalBusinessName: 1,
            doingBusinessAs: '$dba',
            address: 1,
            businessPhone: 1,
            email: 1,
            logoUrl: 1,
            taxRate: 1,
            defaultServiceCharge: 1
        });
    }
    async getRestaurant(id) {
        return await this.restaurantService.findOne({ _id: id, status: status_enum_1.StatusEnum.ACTIVE }, {
            legalBusinessName: 1,
            doingBusinessAs: '$dba',
            address: 1,
            businessPhone: 1,
            email: 1,
            logoUrl: 1,
            taxRate: 1,
            defaultServiceCharge: 1
        });
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => restaurant_open_entity_1.PaginateRestaurantOpenResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get paginate restaurant list',
        description: 'username: `tthai_pos_api`\n\n password: `0#3hWwqS3iDVI1ou%JHrbFlY0v$`'
    }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_restaurant_dto_1.RestaurantPaginateDto]),
    __metadata("design:returntype", Promise)
], RestaurantOpenController.prototype, "getRestaurants", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => restaurant_open_entity_1.OpenGetRestaurantResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get restaurant by ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RestaurantOpenController.prototype, "getRestaurant", null);
RestaurantOpenController = __decorate([
    (0, swagger_1.ApiBasicAuth)(),
    (0, swagger_1.ApiTags)('open/restaurant'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    (0, common_1.Controller)('v1/open/restaurant'),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService])
], RestaurantOpenController);
exports.RestaurantOpenController = RestaurantOpenController;
//# sourceMappingURL=restaurant-open.controller.js.map