"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSRestaurantController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const pos_restaurant_entity_1 = require("../entities/pos-restaurant.entity");
const restaurant_service_1 = require("../services/restaurant.service");
let POSRestaurantController = class POSRestaurantController {
    constructor(restaurantService) {
        this.restaurantService = restaurantService;
    }
    async getRestaurantInfo() {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            legalBusinessName: 1,
            dba: 1,
            address: 1,
            businessPhone: 1,
            email: 1,
            taxRate: 1,
            defaultServiceCharge: 1,
            logoUrl: 1,
            authorizedPersonName: {
                $concat: [
                    '$authorizedPersonFirstName',
                    ' ',
                    '$authorizedPersonLastName'
                ]
            },
            authorizedPersonFirstName: 1,
            authorizedPersonLastName: 1,
            timeZone: 1
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        return restaurant.toObject();
    }
    async getTaxInfo() {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const restaurant = await this.restaurantService.findOne({ _id: restaurantId, status: status_enum_1.StatusEnum.ACTIVE }, { taxRate: 1, defaultServiceCharge: 1 });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        return restaurant.toObject();
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_restaurant_entity_1.GetPOSRestaurantInfoResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Restaurant Info',
        description: 'use *bearer* `staff_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], POSRestaurantController.prototype, "getRestaurantInfo", null);
__decorate([
    (0, common_1.Get)('tax-service-charge'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_restaurant_entity_1.TaxAndServiceChargeResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Restaurant Tax and Service Charge Info',
        description: 'use *bearer* `staff_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], POSRestaurantController.prototype, "getTaxInfo", null);
POSRestaurantController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/restaurant'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/restaurant'),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService])
], POSRestaurantController);
exports.POSRestaurantController = POSRestaurantController;
//# sourceMappingURL=pos-restaurant.controller.js.map