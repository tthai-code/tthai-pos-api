/// <reference types="mongoose" />
import { RestaurantService } from '../services/restaurant.service';
export declare class POSRestaurantController {
    private readonly restaurantService;
    constructor(restaurantService: RestaurantService);
    getRestaurantInfo(): Promise<import("mongoose").LeanDocument<import("../schemas/restaurant.schema").RestaurantDocument>>;
    getTaxInfo(): Promise<import("mongoose").LeanDocument<import("../schemas/restaurant.schema").RestaurantDocument>>;
}
