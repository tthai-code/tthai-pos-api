/// <reference types="mongoose" />
import { AuthLogic } from 'src/modules/auth/logics/auth.logic';
import { BankAccountService } from 'src/modules/payment-gateway/services/bank-account.service';
import { PaymentGatewayService } from 'src/modules/payment-gateway/services/payment-gateway.service';
import { RestaurantHolidaysService } from 'src/modules/restaurant-holiday/services/restaurant-holiday.service';
import { PackageService } from 'src/modules/subscription/services/package.service';
import { SubscriptionService } from 'src/modules/subscription/services/subscription.service';
import { CreateRestaurantDto } from '../dto/create-restaurant.dto';
import { UpdateRestaurantProfile } from '../dto/update-restaurant-profile.dto';
import { RestaurantService } from '../services/restaurant.service';
export declare class RestaurantLogic {
    private readonly restaurantService;
    private readonly restaurantHolidaysService;
    private readonly paymentGatewayService;
    private readonly bankAccountService;
    private readonly subscriptionService;
    private readonly packageService;
    private readonly authLogic;
    constructor(restaurantService: RestaurantService, restaurantHolidaysService: RestaurantHolidaysService, paymentGatewayService: PaymentGatewayService, bankAccountService: BankAccountService, subscriptionService: SubscriptionService, packageService: PackageService, authLogic: AuthLogic);
    private splitName;
    CreateLogic(payload: CreateRestaurantDto, username: string): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        ownerName: string;
        restaurant: import("../schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
    updateRestaurantProfile(id: string, payload: UpdateRestaurantProfile): Promise<import("../schemas/restaurant.schema").RestaurantDocument>;
    getInfoAvailable(restaurantId: string): Promise<{
        bankAccount: boolean;
        paymentGateway: boolean;
        subscription: any[];
    }>;
}
