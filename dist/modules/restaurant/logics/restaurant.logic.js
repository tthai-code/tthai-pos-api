"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const auth_logic_1 = require("../../auth/logics/auth.logic");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const ach_bank_type_enum_1 = require("../../payment-gateway/common/ach-bank.type.enum");
const payment_gateway_enum_1 = require("../../payment-gateway/common/payment-gateway.enum");
const bank_account_service_1 = require("../../payment-gateway/services/bank-account.service");
const payment_gateway_service_1 = require("../../payment-gateway/services/payment-gateway.service");
const restaurant_holiday_service_1 = require("../../restaurant-holiday/services/restaurant-holiday.service");
const subscription_plan_enum_1 = require("../../subscription/common/subscription-plan.enum");
const package_service_1 = require("../../subscription/services/package.service");
const subscription_service_1 = require("../../subscription/services/subscription.service");
const restaurant_service_1 = require("../services/restaurant.service");
let RestaurantLogic = class RestaurantLogic {
    constructor(restaurantService, restaurantHolidaysService, paymentGatewayService, bankAccountService, subscriptionService, packageService, authLogic) {
        this.restaurantService = restaurantService;
        this.restaurantHolidaysService = restaurantHolidaysService;
        this.paymentGatewayService = paymentGatewayService;
        this.bankAccountService = bankAccountService;
        this.subscriptionService = subscriptionService;
        this.packageService = packageService;
        this.authLogic = authLogic;
    }
    splitName(name) {
        const str = name.split(' ');
        const firstName = str[0];
        str.splice(0, 1);
        const lastName = str.join(' ');
        return { firstName, lastName };
    }
    async CreateLogic(payload, username) {
        const logic = async (session) => {
            if (!username)
                throw new common_1.NotFoundException();
            const restaurant = await this.restaurantService.findOne({
                accountId: payload.accountId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (restaurant)
                throw new common_1.BadRequestException('This account has restaurant profile already.');
            const { firstName, lastName } = this.splitName(payload.authorizedPersonName);
            payload.authorizedPersonFirstName = firstName;
            payload.authorizedPersonLastName = lastName;
            delete payload.authorizedPersonName;
            const created = await this.restaurantService.transactionCreate(payload, session);
            if (!created)
                throw new common_1.BadRequestException();
            const holidays = await this.restaurantHolidaysService.transactionCreate({ restaurantId: created._id, holidays: [] }, session);
            if (!holidays)
                throw new common_1.BadRequestException();
            const payment = await this.paymentGatewayService.transactionCreate({ restaurantId: created._id }, session);
            if (!payment)
                throw new common_1.BadRequestException();
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        const result = await this.authLogic.RestaurantLoginAfterCreated(username);
        return result;
    }
    async updateRestaurantProfile(id, payload) {
        const restaurant = await this.restaurantService.findOne({ _id: id });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const { firstName, lastName } = this.splitName(payload.authorizedPersonName);
        payload.authorizedPersonFirstName = firstName;
        payload.authorizedPersonLastName = lastName;
        delete payload.authorizedPersonName;
        const updated = await this.restaurantService.update(id, payload);
        if (!updated)
            throw new common_1.BadRequestException();
        return updated;
    }
    async getInfoAvailable(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const available = {
            bankAccount: false,
            paymentGateway: false,
            subscription: []
        };
        const paymentGateway = await this.paymentGatewayService.findOne({
            restaurantId
        });
        available.paymentGateway =
            (paymentGateway === null || paymentGateway === void 0 ? void 0 : paymentGateway.paymentStatus) === payment_gateway_enum_1.PaymentGatewayStatusEnum.ACTIVE;
        const bankAccount = await this.bankAccountService.findOne({
            restaurantId,
            bankAccountStatus: ach_bank_type_enum_1.BankAccountStatusEnum.ACTIVE
        });
        available.bankAccount = !!bankAccount;
        const subscriptions = await this.subscriptionService.getAll({
            restaurantId,
            subscriptionStatus: subscription_plan_enum_1.SubscriptionStatusEnum.ACTIVE
        });
        const packageIds = [];
        for (const subscription of subscriptions) {
            packageIds.push(subscription === null || subscription === void 0 ? void 0 : subscription.packageId);
        }
        const setPackageIds = [...new Set(packageIds)];
        const allPackage = await this.packageService.getAll({
            _id: { $in: setPackageIds },
            status: status_enum_1.StatusEnum.ACTIVE
        });
        available.subscription = allPackage.map((item) => item.packageType);
        available.subscription = [...new Set(available.subscription.flat())];
        return available;
    }
};
RestaurantLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        restaurant_holiday_service_1.RestaurantHolidaysService,
        payment_gateway_service_1.PaymentGatewayService,
        bank_account_service_1.BankAccountService,
        subscription_service_1.SubscriptionService,
        package_service_1.PackageService,
        auth_logic_1.AuthLogic])
], RestaurantLogic);
exports.RestaurantLogic = RestaurantLogic;
//# sourceMappingURL=restaurant.logic.js.map