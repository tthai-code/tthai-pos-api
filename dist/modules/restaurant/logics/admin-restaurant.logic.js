"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRestaurantLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const payment_gateway_enum_1 = require("../../payment-gateway/common/payment-gateway.enum");
const bank_account_logic_1 = require("../../payment-gateway/logics/bank-account.logic");
const payment_gateway_service_1 = require("../../payment-gateway/services/payment-gateway.service");
const restaurant_service_1 = require("../services/restaurant.service");
let AdminRestaurantLogic = class AdminRestaurantLogic {
    constructor(restaurantService, paymentGatewayService, bankAccountLogic) {
        this.restaurantService = restaurantService;
        this.paymentGatewayService = paymentGatewayService;
        this.bankAccountLogic = bankAccountLogic;
    }
    async getRestaurantInfo(restaurantId) {
        var _a, _b, _c, _d, _e, _f;
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId
        });
        if (!restaurant) {
            throw new common_1.NotFoundException('Not found restaurant.');
        }
        const paymentGateway = await this.paymentGatewayService.findOne({
            restaurantId
        });
        const bankAccountLogic = await this.bankAccountLogic.getBankAccountForAdmin(restaurantId);
        const result = {
            profile: {
                id: restaurantId,
                legalBusinessName: restaurant === null || restaurant === void 0 ? void 0 : restaurant.legalBusinessName,
                dba: restaurant === null || restaurant === void 0 ? void 0 : restaurant.dba,
                ein: restaurant === null || restaurant === void 0 ? void 0 : restaurant.ein,
                address: (_a = restaurant === null || restaurant === void 0 ? void 0 : restaurant.address) === null || _a === void 0 ? void 0 : _a.address,
                city: (_b = restaurant === null || restaurant === void 0 ? void 0 : restaurant.address) === null || _b === void 0 ? void 0 : _b.city,
                state: (_c = restaurant === null || restaurant === void 0 ? void 0 : restaurant.address) === null || _c === void 0 ? void 0 : _c.state,
                zipCode: (_d = restaurant === null || restaurant === void 0 ? void 0 : restaurant.address) === null || _d === void 0 ? void 0 : _d.zipCode,
                phone: restaurant === null || restaurant === void 0 ? void 0 : restaurant.phone,
                email: restaurant === null || restaurant === void 0 ? void 0 : restaurant.email
            },
            paymentGateway: {
                merchantId: paymentGateway === null || paymentGateway === void 0 ? void 0 : paymentGateway.mid,
                status: (paymentGateway === null || paymentGateway === void 0 ? void 0 : paymentGateway.paymentStatus) || payment_gateway_enum_1.PaymentGatewayStatusEnum.INACTIVE,
                paymentCreditCardCharges: 'Interchange Plus'
            },
            achPaymentFile: bankAccountLogic,
            posProcessingFee: {
                creditCard: (_e = restaurant === null || restaurant === void 0 ? void 0 : restaurant.posProcessingFee) === null || _e === void 0 ? void 0 : _e.creditCard,
                debitCard: (_f = restaurant === null || restaurant === void 0 ? void 0 : restaurant.posProcessingFee) === null || _f === void 0 ? void 0 : _f.debitCard
            }
        };
        return result;
    }
    async getBankAccount(id) {
        return this.bankAccountLogic.getBankAccountById(id);
    }
    async updateRestaurantStatus(id, status) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: { $ne: status_enum_1.StatusEnum.DELETED }
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        if (restaurant.status === status) {
            throw new common_1.NotFoundException(`Restaurant status is already ${status}`);
        }
        await this.restaurantService.update(id, { status });
        return { success: true, id };
    }
};
AdminRestaurantLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        payment_gateway_service_1.PaymentGatewayService,
        bank_account_logic_1.BankAccountLogic])
], AdminRestaurantLogic);
exports.AdminRestaurantLogic = AdminRestaurantLogic;
//# sourceMappingURL=admin-restaurant.logic.js.map