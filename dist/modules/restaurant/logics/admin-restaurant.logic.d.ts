import { BankAccountLogic } from 'src/modules/payment-gateway/logics/bank-account.logic';
import { PaymentGatewayService } from 'src/modules/payment-gateway/services/payment-gateway.service';
import { IRestaurantInfo } from '../interfaces/admin-restaurant.interface';
import { RestaurantService } from '../services/restaurant.service';
export declare class AdminRestaurantLogic {
    private readonly restaurantService;
    private readonly paymentGatewayService;
    private readonly bankAccountLogic;
    constructor(restaurantService: RestaurantService, paymentGatewayService: PaymentGatewayService, bankAccountLogic: BankAccountLogic);
    getRestaurantInfo(restaurantId: string): Promise<IRestaurantInfo>;
    getBankAccount(id: string): Promise<import("../../payment-gateway/schemas/bank-account.schema").BankAccountDocument>;
    updateRestaurantStatus(id: string, status: string): Promise<{
        success: boolean;
        id: string;
    }>;
}
