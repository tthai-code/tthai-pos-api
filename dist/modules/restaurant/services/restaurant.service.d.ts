import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { RestaurantDocument } from '../schemas/restaurant.schema';
import { RestaurantPaginateDto } from '../dto/get-restaurant.dto';
import { AdminReportPaginateDto } from 'src/modules/report/dto/admin-report.dto';
export declare class RestaurantService {
    private readonly RestaurantModel;
    private request;
    constructor(RestaurantModel: PaginateModel<RestaurantDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<RestaurantDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<RestaurantDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<RestaurantDocument>;
    create(payload: any): Promise<RestaurantDocument>;
    update(id: string, payload: any): Promise<RestaurantDocument>;
    delete(id: string): Promise<RestaurantDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<RestaurantDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<RestaurantDocument>;
    paginate(query: any, queryParam: RestaurantPaginateDto | AdminReportPaginateDto, select?: any): Promise<PaginateResult<RestaurantDocument>>;
    findOneForOrder(id: string): Promise<RestaurantDocument>;
}
