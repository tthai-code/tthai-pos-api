"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const payment_gateway_module_1 = require("../payment-gateway/payment-gateway.module");
const restaurant_holiday_module_1 = require("../restaurant-holiday/restaurant-holiday.module");
const restaurant_hours_module_1 = require("../restaurant-hours/restaurant-hours.module");
const subscription_module_1 = require("../subscription/subscription.module");
const admin_restaurant_controller_1 = require("./controllers/admin-restaurant.controller");
const pos_restaurant_controller_1 = require("./controllers/pos-restaurant.controller");
const restaurant_open_controller_1 = require("./controllers/restaurant-open.controller");
const restaurant_controller_1 = require("./controllers/restaurant.controller");
const admin_restaurant_logic_1 = require("./logics/admin-restaurant.logic");
const restaurant_logic_1 = require("./logics/restaurant.logic");
const restaurant_schema_1 = require("./schemas/restaurant.schema");
const restaurant_service_1 = require("./services/restaurant.service");
let RestaurantModule = class RestaurantModule {
};
RestaurantModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => payment_gateway_module_1.PaymentGatewayModule),
            (0, common_1.forwardRef)(() => subscription_module_1.SubscriptionModule),
            restaurant_hours_module_1.RestaurantHoursModule,
            restaurant_holiday_module_1.RestaurantHolidaysModule,
            payment_gateway_module_1.PaymentGatewayModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'restaurant', schema: restaurant_schema_1.RestaurantSchema }
            ])
        ],
        providers: [restaurant_service_1.RestaurantService, restaurant_logic_1.RestaurantLogic, admin_restaurant_logic_1.AdminRestaurantLogic],
        controllers: [
            restaurant_controller_1.RestaurantController,
            pos_restaurant_controller_1.POSRestaurantController,
            restaurant_open_controller_1.RestaurantOpenController,
            admin_restaurant_controller_1.AdminRestaurantController
        ],
        exports: [restaurant_service_1.RestaurantService]
    })
], RestaurantModule);
exports.RestaurantModule = RestaurantModule;
//# sourceMappingURL=restaurant.module.js.map