import { AddressFieldDto } from './create-restaurant.dto';
export declare class UpdateRestaurantProfile {
    authorizedPersonFirstName: string;
    authorizedPersonLastName: string;
    readonly legalBusinessName: string;
    readonly dba: string;
    readonly ein: string;
    readonly address: AddressFieldDto;
    readonly businessPhone: string;
    authorizedPersonName: string;
    readonly title: string;
    readonly phone: string;
    readonly email: string;
    readonly referredBy: string;
    readonly einDocument: string;
    readonly salesTaxPermitDocument: string;
    readonly timeZone: string;
}
