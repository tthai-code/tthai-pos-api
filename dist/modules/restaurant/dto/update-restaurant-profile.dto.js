"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateRestaurantProfile = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const create_restaurant_dto_1 = require("./create-restaurant.dto");
class UpdateRestaurantProfile {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ required: true, example: 'Tthai Restaurant' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "legalBusinessName", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'dba' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "dba", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ required: true, example: '983242321' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "ein", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => create_restaurant_dto_1.AddressFieldDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => create_restaurant_dto_1.AddressFieldDto }),
    __metadata("design:type", create_restaurant_dto_1.AddressFieldDto)
], UpdateRestaurantProfile.prototype, "address", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '0111111111' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "businessPhone", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Yoshi Satoshi' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "authorizedPersonName", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Owner' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "title", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: '093243221' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "phone", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'test@mail.com' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "email", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'TEST01' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "referredBy", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'https://example.com/####/####' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "einDocument", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'https://example.com/####/####' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "salesTaxPermitDocument", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'America/Los_Angeles' }),
    __metadata("design:type", String)
], UpdateRestaurantProfile.prototype, "timeZone", void 0);
exports.UpdateRestaurantProfile = UpdateRestaurantProfile;
//# sourceMappingURL=update-restaurant-profile.dto.js.map