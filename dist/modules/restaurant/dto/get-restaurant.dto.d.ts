import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class RestaurantPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    buildQuery(): {
        $or: ({
            legalBusinessName: {
                $regex: string;
                $options: string;
            };
            dba?: undefined;
            email?: undefined;
            businessPhone?: undefined;
        } | {
            dba: {
                $regex: string;
                $options: string;
            };
            legalBusinessName?: undefined;
            email?: undefined;
            businessPhone?: undefined;
        } | {
            email: {
                $regex: string;
                $options: string;
            };
            legalBusinessName?: undefined;
            dba?: undefined;
            businessPhone?: undefined;
        } | {
            businessPhone: {
                $regex: string;
                $options: string;
            };
            legalBusinessName?: undefined;
            dba?: undefined;
            email?: undefined;
        })[];
        status: {
            $ne: StatusEnum;
        };
    };
}
