export declare class AddressFieldDto {
    readonly address: string;
    readonly city: string;
    readonly state: string;
    readonly zipCode: string;
}
export declare class CreateRestaurantDto {
    authorizedPersonFirstName: string;
    authorizedPersonLastName: string;
    readonly accountId: string;
    readonly legalBusinessName: string;
    readonly dba: string;
    readonly ein: string;
    readonly address: AddressFieldDto;
    readonly businessPhone: string;
    authorizedPersonName: string;
    readonly title: string;
    readonly phone: string;
    readonly email: string;
    readonly referredBy: string;
    readonly einDocument: string;
    readonly salesTaxPermitDocument: string;
    readonly timeZone: string;
}
