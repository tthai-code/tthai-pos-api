export declare class SetTaxRestaurantDto {
    readonly tax: number;
    readonly alcoholTax: number;
    readonly isAlcoholTaxActive: boolean;
}
