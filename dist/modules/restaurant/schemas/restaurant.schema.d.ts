import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type RestaurantDocument = RestaurantFields & Document;
export declare class AddressFields {
    address: string;
    city: string;
    state: string;
    zipCode: string;
}
export declare class TaxRateSettingFields {
    tax: number;
    alcoholTax: number;
    isAlcoholTaxActive: boolean;
}
export declare class ReceiptSettingFields {
    isDisplayLogo: boolean;
    isDisplayText: boolean;
    text: string;
    fontSize: string;
    language: string;
}
export declare class PosProcessingFeeFields {
    creditCard: number;
    debitCard: number;
}
export declare class RestaurantFields {
    accountId: Types.ObjectId;
    legalBusinessName: string;
    dba: string;
    ein: string;
    address: AddressFields;
    businessPhone: string;
    authorizedPersonFirstName: string;
    authorizedPersonLastName: string;
    title: string;
    phone: string;
    email: string;
    referredBy: string;
    einDocument: string;
    salesTaxPermitDocument: string;
    logoUrl: string;
    taxRate: TaxRateSettingFields;
    defaultServiceCharge: number;
    receiptSetting: ReceiptSettingFields;
    posProcessingFee: PosProcessingFeeFields;
    timeZone: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const RestaurantSchema: MongooseSchema<Document<RestaurantFields, any, any>, import("mongoose").Model<Document<RestaurantFields, any, any>, any, any, any>, any, any>;
