"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantSchema = exports.RestaurantFields = exports.PosProcessingFeeFields = exports.ReceiptSettingFields = exports.TaxRateSettingFields = exports.AddressFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const timeZone_1 = require("../../../resources/timeZone");
const font_size_enum_1 = require("../common/font-size.enum");
const language_enum_1 = require("../common/language.enum");
let AddressFields = class AddressFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], AddressFields.prototype, "address", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], AddressFields.prototype, "city", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], AddressFields.prototype, "state", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], AddressFields.prototype, "zipCode", void 0);
AddressFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], AddressFields);
exports.AddressFields = AddressFields;
let TaxRateSettingFields = class TaxRateSettingFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TaxRateSettingFields.prototype, "tax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], TaxRateSettingFields.prototype, "alcoholTax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: false }),
    __metadata("design:type", Boolean)
], TaxRateSettingFields.prototype, "isAlcoholTaxActive", void 0);
TaxRateSettingFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], TaxRateSettingFields);
exports.TaxRateSettingFields = TaxRateSettingFields;
let ReceiptSettingFields = class ReceiptSettingFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: false }),
    __metadata("design:type", Boolean)
], ReceiptSettingFields.prototype, "isDisplayLogo", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: false }),
    __metadata("design:type", Boolean)
], ReceiptSettingFields.prototype, "isDisplayText", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], ReceiptSettingFields.prototype, "text", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: font_size_enum_1.FontSizeEnum.NORMAL, enum: Object.values(font_size_enum_1.FontSizeEnum) }),
    __metadata("design:type", String)
], ReceiptSettingFields.prototype, "fontSize", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: language_enum_1.LanguageEnum.ENG, enum: Object.values(language_enum_1.LanguageEnum) }),
    __metadata("design:type", String)
], ReceiptSettingFields.prototype, "language", void 0);
ReceiptSettingFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], ReceiptSettingFields);
exports.ReceiptSettingFields = ReceiptSettingFields;
let PosProcessingFeeFields = class PosProcessingFeeFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: 0.25 }),
    __metadata("design:type", Number)
], PosProcessingFeeFields.prototype, "creditCard", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0.5 }),
    __metadata("design:type", Number)
], PosProcessingFeeFields.prototype, "debitCard", void 0);
PosProcessingFeeFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], PosProcessingFeeFields);
exports.PosProcessingFeeFields = PosProcessingFeeFields;
let RestaurantFields = class RestaurantFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurantAccount' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], RestaurantFields.prototype, "accountId", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "legalBusinessName", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "dba", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "ein", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: new AddressFields()
    }),
    __metadata("design:type", AddressFields)
], RestaurantFields.prototype, "address", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "businessPhone", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], RestaurantFields.prototype, "authorizedPersonFirstName", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], RestaurantFields.prototype, "authorizedPersonLastName", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "title", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "phone", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "email", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], RestaurantFields.prototype, "referredBy", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], RestaurantFields.prototype, "einDocument", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], RestaurantFields.prototype, "salesTaxPermitDocument", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], RestaurantFields.prototype, "logoUrl", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: new TaxRateSettingFields()
    }),
    __metadata("design:type", TaxRateSettingFields)
], RestaurantFields.prototype, "taxRate", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 10 }),
    __metadata("design:type", Number)
], RestaurantFields.prototype, "defaultServiceCharge", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: new ReceiptSettingFields()
    }),
    __metadata("design:type", ReceiptSettingFields)
], RestaurantFields.prototype, "receiptSetting", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: new PosProcessingFeeFields()
    }),
    __metadata("design:type", PosProcessingFeeFields)
], RestaurantFields.prototype, "posProcessingFee", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 'Etc/UTC', enum: timeZone_1.timeZone }),
    __metadata("design:type", String)
], RestaurantFields.prototype, "timeZone", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], RestaurantFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], RestaurantFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], RestaurantFields.prototype, "createdBy", void 0);
RestaurantFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true })
], RestaurantFields);
exports.RestaurantFields = RestaurantFields;
exports.RestaurantSchema = mongoose_1.SchemaFactory.createForClass(RestaurantFields);
exports.RestaurantSchema.plugin(mongoosePaginate);
exports.RestaurantSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=restaurant.schema.js.map