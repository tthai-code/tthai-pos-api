"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSAccountController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const device_auth_guard_1 = require("../../auth/guards/device-auth.guard");
const check_account_dto_1 = require("../dto/check-account.dto");
const pos_account_entity_1 = require("../entity/pos-account.entity");
const pos_restaurant_account_logic_1 = require("../logics/pos-restaurant-account.logic");
let POSAccountController = class POSAccountController {
    constructor(posAccountLogic) {
        this.posAccountLogic = posAccountLogic;
    }
    async checkUsernamePassword(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = device_auth_guard_1.DeviceAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.posAccountLogic.checkUsernamePassword(restaurantId, payload);
    }
};
__decorate([
    (0, common_1.Post)('check'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_account_entity_1.CheckAccountResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Check Username and Password',
        description: 'use bearer `device_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [check_account_dto_1.CheckAccountDto]),
    __metadata("design:returntype", Promise)
], POSAccountController.prototype, "checkUsernamePassword", null);
POSAccountController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/auth'),
    (0, common_1.UseGuards)(device_auth_guard_1.DeviceAuthGuard),
    (0, common_1.Controller)('v1/pos/auth'),
    __metadata("design:paramtypes", [pos_restaurant_account_logic_1.POSAccountLogic])
], POSAccountController);
exports.POSAccountController = POSAccountController;
//# sourceMappingURL=pos-restaurant-account.controller.js.map