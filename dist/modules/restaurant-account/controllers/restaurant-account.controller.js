"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantAccountController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const get_restaurant_account_dto_1 = require("../dto/get-restaurant-account.dto");
const create_account_dto_1 = require("../dto/create-account.dto");
const restaurant_account_service_1 = require("../services/restaurant-account.service");
const restaurant_account_logic_1 = require("../logics/restaurant-account.logic");
const change_username_dto_1 = require("../dto/change-username.dto");
const change_password_dto_1 = require("../dto/change-password.dto");
let RestaurantAccountController = class RestaurantAccountController {
    constructor(restaurantAccountService, accountLogic) {
        this.restaurantAccountService = restaurantAccountService;
        this.accountLogic = accountLogic;
    }
    async getAccounts(query) {
        return await this.restaurantAccountService.paginate(query.buildQuery(), query);
    }
    async getAccount(id) {
        return await this.restaurantAccountService.findById(id);
    }
    async updateUsername(id, username) {
        const account = await this.restaurantAccountService.findById(id);
        if (!account)
            throw new common_1.NotFoundException();
        return await this.restaurantAccountService.update(id, username);
    }
    async updatePassword(id, payload) {
        const account = await this.restaurantAccountService.findById(id);
        if (!account)
            throw new common_1.NotFoundException();
        return await this.accountLogic.changeUserPasswordLogic(id, payload);
    }
    async createAccount(account) {
        return await this.accountLogic.createUserLogic(account);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Get Account List' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_restaurant_account_dto_1.RestaurantAccountPaginateDto]),
    __metadata("design:returntype", Promise)
], RestaurantAccountController.prototype, "getAccounts", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Get Account By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], RestaurantAccountController.prototype, "getAccount", null);
__decorate([
    (0, common_1.Patch)(':id/username'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Username' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, change_username_dto_1.ChangeUsernameRestaurantDto]),
    __metadata("design:returntype", Promise)
], RestaurantAccountController.prototype, "updateUsername", null);
__decorate([
    (0, common_1.Patch)(':id/password'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Password' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, change_password_dto_1.ChangePasswordRestaurantDto]),
    __metadata("design:returntype", Promise)
], RestaurantAccountController.prototype, "updatePassword", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create Account' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_account_dto_1.CreateAccountDto]),
    __metadata("design:returntype", Promise)
], RestaurantAccountController.prototype, "createAccount", null);
RestaurantAccountController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('restaurant-account'),
    (0, common_1.Controller)('v1/account'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __metadata("design:paramtypes", [restaurant_account_service_1.RestaurantAccountService,
        restaurant_account_logic_1.AccountLogic])
], RestaurantAccountController);
exports.RestaurantAccountController = RestaurantAccountController;
//# sourceMappingURL=restaurant-account.controller.js.map