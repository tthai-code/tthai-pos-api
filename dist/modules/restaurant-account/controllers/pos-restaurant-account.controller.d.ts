import { CheckAccountDto } from '../dto/check-account.dto';
import { POSAccountLogic } from '../logics/pos-restaurant-account.logic';
export declare class POSAccountController {
    private readonly posAccountLogic;
    constructor(posAccountLogic: POSAccountLogic);
    checkUsernamePassword(payload: CheckAccountDto): Promise<{
        isCorrect: any;
    }>;
}
