import { RestaurantAccountPaginateDto } from '../dto/get-restaurant-account.dto';
import { CreateAccountDto } from '../dto/create-account.dto';
import { RestaurantAccountService } from '../services/restaurant-account.service';
import { AccountLogic } from '../logics/restaurant-account.logic';
import { ChangeUsernameRestaurantDto } from '../dto/change-username.dto';
import { ChangePasswordRestaurantDto } from '../dto/change-password.dto';
export declare class RestaurantAccountController {
    private readonly restaurantAccountService;
    private readonly accountLogic;
    constructor(restaurantAccountService: RestaurantAccountService, accountLogic: AccountLogic);
    getAccounts(query: RestaurantAccountPaginateDto): Promise<PaginateResult<import("../schemas/restaurant-account.schema").RestaurantAccountDocument>>;
    getAccount(id: string): Promise<any>;
    updateUsername(id: string, username: ChangeUsernameRestaurantDto): Promise<import("../schemas/restaurant-account.schema").RestaurantAccountDocument>;
    updatePassword(id: string, payload: ChangePasswordRestaurantDto): Promise<import("../schemas/restaurant-account.schema").RestaurantAccountDocument>;
    createAccount(account: CreateAccountDto): Promise<import("../schemas/restaurant-account.schema").RestaurantAccountDocument>;
}
