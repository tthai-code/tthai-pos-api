"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountPublicController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_logic_1 = require("../../auth/logics/auth.logic");
const login_dto_1 = require("../../user/dto/login.dto");
const create_account_dto_1 = require("../dto/create-account.dto");
const restaurant_account_logic_1 = require("../logics/restaurant-account.logic");
let AccountPublicController = class AccountPublicController {
    constructor(authLogic, accountLogic) {
        this.authLogic = authLogic;
        this.accountLogic = accountLogic;
    }
    async login(login) {
        const userData = await this.authLogic.RestaurantLoginLogic(login);
        return userData;
    }
    async createAccount(account) {
        return await this.accountLogic.createUserLogic(account);
    }
    async secretLogin(id) {
        return await this.authLogic.DirectAdminLoginLogic(id);
    }
};
__decorate([
    (0, common_1.Post)('login'),
    (0, swagger_1.ApiOperation)({ summary: 'Login' }),
    (0, swagger_1.ApiOkResponse)({
        schema: {
            type: 'object',
            properties: {
                message: { type: 'string', example: 'done' },
                data: {
                    type: 'object',
                    properties: {
                        id: { type: 'string', example: '630e55a0d9c30fd7cdcb424b' },
                        username: { type: 'string', example: 'corywong@mail.com' },
                        restaurant: {
                            type: 'object',
                            properties: {
                                legal_business_name: { type: 'string', example: 'Holy Beef' },
                                logo_url: { type: 'string', example: null },
                                email: { type: 'string', example: 'corywong@mail.com' },
                                business_phone: { type: 'string', example: '0222222222' },
                                address: {
                                    type: 'object',
                                    properties: {
                                        note: { type: 'string', example: 'Cory Wong Test' },
                                        zip_code: { type: 'string', example: '75001' },
                                        state: { type: 'string', example: 'Dallas' },
                                        city: { type: 'string', example: 'Addison' },
                                        address: { type: 'string', example: 'Some Address' }
                                    }
                                },
                                tax_rate: {
                                    type: 'object',
                                    properties: {
                                        alcohol_tax: { type: 'number', example: 12 },
                                        tax: { type: 'number', example: 10 }
                                    }
                                },
                                printer_setting: {
                                    type: 'object',
                                    properties: {
                                        receipt_ip_printer: { type: 'string', example: '0.0.0.0' },
                                        kitchen_ip_printer: { type: 'string', example: '0.0.0.0' }
                                    }
                                }
                            }
                        },
                        token_expire: { type: 'number', example: 1663868598019 }
                    }
                },
                access_token: { type: 'string', example: '<access_token>' }
            }
        }
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [login_dto_1.LoginDto]),
    __metadata("design:returntype", Promise)
], AccountPublicController.prototype, "login", null);
__decorate([
    (0, common_1.Post)('register'),
    (0, swagger_1.ApiOperation)({ summary: 'Restaurant Account Register' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_account_dto_1.CreateAccountDto]),
    __metadata("design:returntype", Promise)
], AccountPublicController.prototype, "createAccount", null);
__decorate([
    (0, common_1.Get)('login/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AccountPublicController.prototype, "secretLogin", null);
AccountPublicController = __decorate([
    (0, swagger_1.ApiTags)('public/account'),
    (0, common_1.Controller)('public/account'),
    __metadata("design:paramtypes", [auth_logic_1.AuthLogic,
        restaurant_account_logic_1.AccountLogic])
], AccountPublicController);
exports.AccountPublicController = AccountPublicController;
//# sourceMappingURL=public-account.controller.js.map