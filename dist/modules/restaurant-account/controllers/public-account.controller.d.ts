/// <reference types="mongoose" />
import { AuthLogic } from 'src/modules/auth/logics/auth.logic';
import { LoginDto } from 'src/modules/user/dto/login.dto';
import { CreateAccountDto } from '../dto/create-account.dto';
import { AccountLogic } from '../logics/restaurant-account.logic';
export declare class AccountPublicController {
    private readonly authLogic;
    private readonly accountLogic;
    constructor(authLogic: AuthLogic, accountLogic: AccountLogic);
    login(login: LoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        ownerName: string;
        restaurant: import("../../restaurant/schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
    createAccount(account: CreateAccountDto): Promise<import("../schemas/restaurant-account.schema").RestaurantAccountDocument>;
    secretLogin(id: string): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        ownerName: string;
        restaurant: import("../../restaurant/schemas/restaurant.schema").RestaurantFields & import("mongoose").Document<any, any, any> & {
            _id: any;
        };
    }>;
}
