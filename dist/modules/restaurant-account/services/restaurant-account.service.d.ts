import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { Model } from 'mongoose';
import { RestaurantAccountDocument } from '../schemas/restaurant-account.schema';
import { RestaurantAccountPaginateDto } from '../dto/get-restaurant-account.dto';
export declare class RestaurantAccountService {
    private readonly AccountModel;
    private request;
    constructor(AccountModel: PaginateModel<RestaurantAccountDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<RestaurantAccountDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<RestaurantAccountDocument>;
    create(payload: any): Promise<RestaurantAccountDocument>;
    update(id: string, User: any): Promise<RestaurantAccountDocument>;
    delete(id: string): Promise<RestaurantAccountDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any): Promise<RestaurantAccountDocument>;
    findOneWithPassword(condition: any): Promise<RestaurantAccountDocument>;
    paginate(query: any, queryParam: RestaurantAccountPaginateDto): Promise<PaginateResult<RestaurantAccountDocument>>;
}
