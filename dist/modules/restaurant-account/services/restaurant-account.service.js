"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestaurantAccountService = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const mongoose_1 = require("@nestjs/mongoose");
const status_enum_1 = require("../../../common/enum/status.enum");
let RestaurantAccountService = class RestaurantAccountService {
    constructor(AccountModel, request) {
        this.AccountModel = AccountModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.AccountModel.findById({ _id: id });
    }
    async isExists(condition) {
        const result = await this.AccountModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.AccountModel;
    }
    async create(payload) {
        const document = new this.AccountModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async update(id, User) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, User)).save();
    }
    async delete(id) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set({ status: status_enum_1.StatusEnum.DELETED }).save();
    }
    getAll(condition, project) {
        return this.AccountModel.find(condition, project);
    }
    findById(id) {
        return this.AccountModel.findById(id);
    }
    findOne(condition) {
        return this.AccountModel.findOne(condition, { password: 0 });
    }
    findOneWithPassword(condition) {
        return this.AccountModel.findOne(condition);
    }
    paginate(query, queryParam) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder }
        };
        return this.AccountModel.paginate(query, options);
    }
};
RestaurantAccountService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('restaurantAccount')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], RestaurantAccountService);
exports.RestaurantAccountService = RestaurantAccountService;
//# sourceMappingURL=restaurant-account.service.js.map