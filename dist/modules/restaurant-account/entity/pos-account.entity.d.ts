import { ResponseDto } from 'src/common/entity/response.entity';
declare class CheckAccountObject {
    is_correct: boolean;
}
export declare class CheckAccountResponse extends ResponseDto<CheckAccountObject> {
    data: CheckAccountObject;
}
export {};
