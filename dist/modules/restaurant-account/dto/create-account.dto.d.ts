export declare class CreateAccountDto {
    readonly username: string;
    password: string;
    readonly email: string;
    readonly locale: string;
}
