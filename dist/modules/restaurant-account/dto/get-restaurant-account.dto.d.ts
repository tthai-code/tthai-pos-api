import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class RestaurantAccountPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly status: string;
    readonly search: string;
    buildQuery(): {
        $or: ({
            username: {
                $regex: string;
                $options: string;
            };
            email?: undefined;
        } | {
            email: {
                $regex: string;
                $options: string;
            };
            username?: undefined;
        })[];
        status: string | {
            $ne: StatusEnum;
        };
    };
}
