import { Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type RestaurantAccountDocument = RestaurantAccountFields & Document;
export declare class RestaurantAccountFields {
    username: string;
    password: string;
    email: string;
    locale: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const RestaurantAccountSchema: import("mongoose").Schema<Document<RestaurantAccountFields, any, any>, import("mongoose").Model<Document<RestaurantAccountFields, any, any>, any, any, any>, any, any>;
