import { RestaurantAccountService } from '../services/restaurant-account.service';
import { CheckAccountDto } from '../dto/check-account.dto';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
export declare class POSAccountLogic {
    private readonly accountService;
    private readonly restaurantService;
    constructor(accountService: RestaurantAccountService, restaurantService: RestaurantService);
    checkUsernamePassword(restaurantId: string, payload: CheckAccountDto): Promise<{
        isCorrect: any;
    }>;
}
