"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSAccountLogic = void 0;
const common_1 = require("@nestjs/common");
const bcrypt = require("bcryptjs");
const restaurant_account_service_1 = require("../services/restaurant-account.service");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
let POSAccountLogic = class POSAccountLogic {
    constructor(accountService, restaurantService) {
        this.accountService = accountService;
        this.restaurantService = restaurantService;
    }
    async checkUsernamePassword(restaurantId, payload) {
        const accountData = await this.accountService.findOneWithPassword({
            status: status_enum_1.StatusEnum.ACTIVE,
            username: payload.username
        });
        if (!accountData)
            throw new common_1.NotFoundException('Not found account.');
        const restaurant = await this.restaurantService.findOne({
            accountId: accountData.id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant) {
            throw new common_1.NotFoundException('Not found restaurant.');
        }
        if (`${restaurant.id}` !== restaurantId) {
            throw new common_1.UnauthorizedException();
        }
        const isCorrect = await bcrypt.compare(payload.password, accountData.password);
        return { isCorrect };
    }
};
POSAccountLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_account_service_1.RestaurantAccountService,
        restaurant_service_1.RestaurantService])
], POSAccountLogic);
exports.POSAccountLogic = POSAccountLogic;
//# sourceMappingURL=pos-restaurant-account.logic.js.map