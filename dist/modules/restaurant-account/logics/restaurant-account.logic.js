"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountLogic = void 0;
const common_1 = require("@nestjs/common");
const bcrypt = require("bcryptjs");
const restaurant_account_service_1 = require("../services/restaurant-account.service");
const status_enum_1 = require("../../../common/enum/status.enum");
let AccountLogic = class AccountLogic {
    constructor(accountService) {
        this.accountService = accountService;
    }
    async createUserLogic(payload) {
        const userData = await this.accountService.findOne({
            status: status_enum_1.StatusEnum.ACTIVE,
            username: payload.username
        });
        if (userData) {
            throw new common_1.BadRequestException(`Username ${payload.username} already used.`);
        }
        const salt = bcrypt.genSaltSync(10);
        payload.password = bcrypt.hashSync(payload.password, salt);
        return await this.accountService.create(payload);
    }
    async changeUserPasswordLogic(id, payload) {
        const salt = bcrypt.genSaltSync(10);
        payload.password = bcrypt.hashSync(payload.password, salt);
        return await this.accountService.update(id, payload);
    }
};
AccountLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_account_service_1.RestaurantAccountService])
], AccountLogic);
exports.AccountLogic = AccountLogic;
//# sourceMappingURL=restaurant-account.logic.js.map