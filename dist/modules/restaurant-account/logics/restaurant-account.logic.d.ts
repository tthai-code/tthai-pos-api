import { CreateAccountDto } from '../dto/create-account.dto';
import { RestaurantAccountService } from '../services/restaurant-account.service';
import { ChangePasswordRestaurantDto } from '../dto/change-password.dto';
export declare class AccountLogic {
    private readonly accountService;
    constructor(accountService: RestaurantAccountService);
    createUserLogic(payload: CreateAccountDto): Promise<import("../schemas/restaurant-account.schema").RestaurantAccountDocument>;
    changeUserPasswordLogic(id: string, payload: ChangePasswordRestaurantDto): Promise<import("../schemas/restaurant-account.schema").RestaurantAccountDocument>;
}
