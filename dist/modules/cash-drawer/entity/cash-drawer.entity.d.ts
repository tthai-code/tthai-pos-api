import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class CashDrawerObject {
    id: string;
    restaurant_id: string;
    start_time: string;
    end_time: string;
    cash_drawer_status: string;
    total_cash_sales: number;
    cash_in: number;
    expected_amount: number;
    actual_amount: number;
    difference: number;
}
declare class PaginateCashDrawerObject extends PaginateResponseDto<CashDrawerObject[]> {
    results: CashDrawerObject[];
}
export declare class PaginateCashDrawerResponse extends ResponseDto<PaginateCashDrawerObject> {
    data: PaginateCashDrawerObject;
}
declare class CashActivityObject {
    action: string;
    comment: string;
    user: string;
    amount: number;
    date: string;
    id: string;
}
declare class CashDrawerWithActivityObject {
    id: string;
    start_time: string;
    end_time: string;
    activity: CashActivityObject[];
    expected_amount: number;
    actual_amount: number;
    difference: number;
    cash_drawer_status: string;
}
export declare class CashDrawerWithActivityResponse extends ResponseDto<CashDrawerWithActivityObject> {
    data: CashDrawerWithActivityObject;
}
export {};
