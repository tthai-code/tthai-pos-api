"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SummaryClosingCashResponse = exports.CashDrawerStatusResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class CashDrawerStatusObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CashDrawerStatusObject.prototype, "is_open_cash_drawer_status", void 0);
class ExpectedCashObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: 128.5 }),
    __metadata("design:type", Number)
], ExpectedCashObject.prototype, "expected_amount", void 0);
class CashDrawerStatusResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CashDrawerStatusObject,
        example: {
            is_open_cash_drawer_status: false
        }
    }),
    __metadata("design:type", CashDrawerStatusObject)
], CashDrawerStatusResponse.prototype, "data", void 0);
exports.CashDrawerStatusResponse = CashDrawerStatusResponse;
class SummaryClosingCashResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: ExpectedCashObject
    }),
    __metadata("design:type", ExpectedCashObject)
], SummaryClosingCashResponse.prototype, "data", void 0);
exports.SummaryClosingCashResponse = SummaryClosingCashResponse;
//# sourceMappingURL=pos-cash-drawer.entity.js.map