import { ResponseDto } from 'src/common/entity/response.entity';
declare class CashDrawerStatusObject {
    is_open_cash_drawer_status: boolean;
}
declare class ExpectedCashObject {
    expected_amount: number;
}
export declare class CashDrawerStatusResponse extends ResponseDto<CashDrawerStatusObject> {
    data: CashDrawerStatusObject;
}
export declare class SummaryClosingCashResponse extends ResponseDto<ExpectedCashObject> {
    data: ExpectedCashObject;
}
export {};
