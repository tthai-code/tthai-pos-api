"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CashDrawerWithActivityResponse = exports.PaginateCashDrawerResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const cash_drawer_enum_1 = require("../common/cash-drawer.enum");
class CashDrawerObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashDrawerObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashDrawerObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashDrawerObject.prototype, "start_time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashDrawerObject.prototype, "end_time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(cash_drawer_enum_1.CashDrawerStatusEnum) }),
    __metadata("design:type", String)
], CashDrawerObject.prototype, "cash_drawer_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashDrawerObject.prototype, "total_cash_sales", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashDrawerObject.prototype, "cash_in", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashDrawerObject.prototype, "expected_amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashDrawerObject.prototype, "actual_amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashDrawerObject.prototype, "difference", void 0);
class PaginateCashDrawerObject extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [CashDrawerObject],
        example: {
            id: '639b30a9a595361d1b13b926',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            start_time: '2022-12-15T14:35:03.772Z',
            end_time: '2022-12-15T15:02:05.148Z',
            cash_drawer_status: 'CLOSED',
            total_cash_sales: 0,
            cash_in: 110.4,
            expected_amount: 110.4,
            actual_amount: 120.55,
            difference: 10.15
        }
    }),
    __metadata("design:type", Array)
], PaginateCashDrawerObject.prototype, "results", void 0);
class PaginateCashDrawerResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: PaginateCashDrawerObject }),
    __metadata("design:type", PaginateCashDrawerObject)
], PaginateCashDrawerResponse.prototype, "data", void 0);
exports.PaginateCashDrawerResponse = PaginateCashDrawerResponse;
class CashActivityObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        enum: Object.values(cash_drawer_enum_1.CashDrawerActionEnum).filter((item) => item !== cash_drawer_enum_1.CashDrawerActionEnum.CASH_SALES)
    }),
    __metadata("design:type", String)
], CashActivityObject.prototype, "action", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashActivityObject.prototype, "comment", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashActivityObject.prototype, "user", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashActivityObject.prototype, "amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashActivityObject.prototype, "date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashActivityObject.prototype, "id", void 0);
class CashDrawerWithActivityObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashDrawerWithActivityObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashDrawerWithActivityObject.prototype, "start_time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashDrawerWithActivityObject.prototype, "end_time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [CashActivityObject] }),
    __metadata("design:type", Array)
], CashDrawerWithActivityObject.prototype, "activity", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashDrawerWithActivityObject.prototype, "expected_amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashDrawerWithActivityObject.prototype, "actual_amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CashDrawerWithActivityObject.prototype, "difference", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CashDrawerWithActivityObject.prototype, "cash_drawer_status", void 0);
class CashDrawerWithActivityResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CashDrawerWithActivityObject,
        example: {
            id: '639b30a9a595361d1b13b926',
            start_time: '2022-12-15T14:35:03.772Z',
            end_time: '2022-12-15T15:02:05.148Z',
            activity: [
                {
                    action: 'STARTING_CASH',
                    comment: 'Starting night shift',
                    user: 'Kevin Start',
                    amount: 10.2,
                    date: '2022-12-15T08:35:03.772Z',
                    id: '639b30a9a595361d1b13b928'
                },
                {
                    action: 'CASH_IN',
                    comment: 'Starting night shift',
                    user: 'Kevin Start',
                    amount: 10.2,
                    date: '2022-12-15T14:35:03.772Z',
                    id: '639b323ed414156964aecf08'
                },
                {
                    action: 'CASH_OUT',
                    comment: 'Starting night shift',
                    user: 'Kevin Start',
                    amount: 10.2,
                    date: '2022-12-15T14:35:03.772Z',
                    id: '639b3242d414156964aecf0d'
                },
                {
                    action: 'CASH_IN',
                    comment: 'Starting night shift',
                    user: 'Kevin Start',
                    amount: 100.2,
                    date: '2022-12-15T14:35:03.772Z',
                    id: '639b3256d414156964aecf13'
                },
                {
                    action: 'CASH_COUNT',
                    comment: 'Starting night shift',
                    user: 'Kevin Start',
                    amount: 0,
                    date: '2022-12-15T14:35:03.772Z',
                    id: '639b32d29eca4b55f4b55756'
                }
            ],
            expected_amount: 110.4,
            actual_amount: 120.55,
            difference: 10.15,
            cash_drawer_status: 'CLOSED'
        }
    }),
    __metadata("design:type", CashDrawerWithActivityObject)
], CashDrawerWithActivityResponse.prototype, "data", void 0);
exports.CashDrawerWithActivityResponse = CashDrawerWithActivityResponse;
//# sourceMappingURL=cash-drawer.entity.js.map