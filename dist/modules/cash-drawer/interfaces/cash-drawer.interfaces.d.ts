export interface ICashDrawer {
    restaurantId: string;
    startTime: string | Date;
    endTime?: string | Date;
    cashDrawerStatus?: string;
    startingCash?: number;
    totalCashSales?: number;
    cashIn?: number;
    cashOut?: number;
    actualAmount?: number;
}
export interface ISummaryCashForClosing {
    expectedAmount: number;
}
export interface ICashDrawerResponse extends ICashDrawer {
    id: string;
    expectedAmount: number;
    difference: number;
}
export interface ICashSales {
    restaurantId?: string;
    cashDrawerId?: string;
    date: string | Date;
    amount: number;
    action: string;
    user: string;
    comment: string;
}
