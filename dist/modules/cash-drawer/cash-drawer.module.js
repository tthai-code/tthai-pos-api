"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CashDrawerModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const cash_drawer_controller_1 = require("./controllers/cash-drawer.controller");
const pos_cash_drawer_controller_1 = require("./controllers/pos-cash-drawer.controller");
const cash_drawer_logic_1 = require("./logic/cash-drawer.logic");
const pos_cash_drawer_logic_1 = require("./logic/pos-cash-drawer.logic");
const cash_activity_schema_1 = require("./schemas/cash-activity.schema");
const cash_drawer_schema_1 = require("./schemas/cash-drawer.schema");
const cash_activity_service_1 = require("./services/cash-activity.service");
const cash_drawer_service_1 = require("./services/cash-drawer.service");
let CashDrawerModule = class CashDrawerModule {
};
CashDrawerModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'cashDrawers', schema: cash_drawer_schema_1.CashDrawerSchema },
                { name: 'cashActivity', schema: cash_activity_schema_1.CashActivitySchema }
            ])
        ],
        providers: [
            cash_drawer_service_1.CashDrawerService,
            cash_activity_service_1.CashActivityService,
            pos_cash_drawer_logic_1.POSCashDrawerLogic,
            cash_drawer_logic_1.CashDrawerLogic
        ],
        controllers: [cash_drawer_controller_1.CashDrawerController, pos_cash_drawer_controller_1.POSCashDrawerController],
        exports: [pos_cash_drawer_logic_1.POSCashDrawerLogic]
    })
], CashDrawerModule);
exports.CashDrawerModule = CashDrawerModule;
//# sourceMappingURL=cash-drawer.module.js.map