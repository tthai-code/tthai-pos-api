"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CashDrawerActionEnum = exports.CashDrawerStatusEnum = void 0;
var CashDrawerStatusEnum;
(function (CashDrawerStatusEnum) {
    CashDrawerStatusEnum["OPEN"] = "OPEN";
    CashDrawerStatusEnum["CLOSED"] = "CLOSED";
})(CashDrawerStatusEnum = exports.CashDrawerStatusEnum || (exports.CashDrawerStatusEnum = {}));
var CashDrawerActionEnum;
(function (CashDrawerActionEnum) {
    CashDrawerActionEnum["STARTING_CASH"] = "STARTING_CASH";
    CashDrawerActionEnum["CASH_IN"] = "CASH_IN";
    CashDrawerActionEnum["CASH_OUT"] = "CASH_OUT";
    CashDrawerActionEnum["CASH_COUNT"] = "CASH_COUNT";
    CashDrawerActionEnum["CASH_SALES"] = "CASH_SALES";
})(CashDrawerActionEnum = exports.CashDrawerActionEnum || (exports.CashDrawerActionEnum = {}));
//# sourceMappingURL=cash-drawer.enum.js.map