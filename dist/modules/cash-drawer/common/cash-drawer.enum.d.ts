export declare enum CashDrawerStatusEnum {
    OPEN = "OPEN",
    CLOSED = "CLOSED"
}
export declare enum CashDrawerActionEnum {
    STARTING_CASH = "STARTING_CASH",
    CASH_IN = "CASH_IN",
    CASH_OUT = "CASH_OUT",
    CASH_COUNT = "CASH_COUNT",
    CASH_SALES = "CASH_SALES"
}
