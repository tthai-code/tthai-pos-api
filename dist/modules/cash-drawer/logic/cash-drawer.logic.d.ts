import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { PaginateCashDrawerDto } from '../dto/get-cash-drawer.dto';
import { CashActivityService } from '../services/cash-activity.service';
import { CashDrawerService } from '../services/cash-drawer.service';
export declare class CashDrawerLogic {
    private readonly restaurantService;
    private readonly cashDrawerService;
    private readonly cashActivityService;
    constructor(restaurantService: RestaurantService, cashDrawerService: CashDrawerService, cashActivityService: CashActivityService);
    getCashDrawer(restaurantId: string, query: PaginateCashDrawerDto): Promise<any>;
    getCashDrawerActivities(id: string): Promise<{
        id: string;
        startTime: Date;
        endTime: Date;
        activity: any[];
        expectedAmount: number;
        actualAmount: number;
        difference: number;
        cashDrawerStatus: string;
    }>;
}
