"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSCashDrawerLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const cash_drawer_enum_1 = require("../common/cash-drawer.enum");
const cash_activity_service_1 = require("../services/cash-activity.service");
const cash_drawer_service_1 = require("../services/cash-drawer.service");
let POSCashDrawerLogic = class POSCashDrawerLogic {
    constructor(cashDrawerService, cashActivityService, restaurantService) {
        this.cashDrawerService = cashDrawerService;
        this.cashActivityService = cashActivityService;
        this.restaurantService = restaurantService;
    }
    async getStatus(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found Restaurant.');
        const openCashDrawer = await this.cashDrawerService.findOne({
            restaurantId,
            cashDrawerStatus: cash_drawer_enum_1.CashDrawerStatusEnum.OPEN
        });
        if (!openCashDrawer) {
            return { isOpenCashDrawerStatus: false };
        }
        return { isOpenCashDrawerStatus: true };
    }
    async startingCash(restaurantId, payload) {
        let openCashDrawer = await this.cashDrawerService.findOne({
            restaurantId,
            cashDrawerStatus: cash_drawer_enum_1.CashDrawerStatusEnum.OPEN
        });
        payload.restaurantId = restaurantId;
        const logic = async (session) => {
            if (!openCashDrawer) {
                const cashDrawerPayload = {
                    restaurantId,
                    startTime: payload.date,
                    startingCash: payload.amount
                };
                openCashDrawer = await this.cashDrawerService.transactionCreate(cashDrawerPayload, session);
            }
            else {
                await this.cashDrawerService.transactionUpdateOne({ _id: openCashDrawer.id }, {
                    $inc: { startingCash: payload.amount }
                }, session);
            }
            payload.cashDrawerId = openCashDrawer.id;
            await this.cashActivityService.transactionCreate(payload, session);
            return;
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return { success: true };
    }
    async cashInOut(restaurantId, payload) {
        const openCashDrawer = await this.cashDrawerService.findOne({
            restaurantId,
            cashDrawerStatus: cash_drawer_enum_1.CashDrawerStatusEnum.OPEN
        });
        if (!openCashDrawer) {
            throw new common_1.BadGatewayException('Please Starting Cash Before do another action.');
        }
        payload.restaurantId = restaurantId;
        payload.cashDrawerId = openCashDrawer.id;
        let inc;
        if (payload.action === cash_drawer_enum_1.CashDrawerActionEnum.CASH_IN) {
            inc = { $inc: { cashIn: payload.amount } };
        }
        else if (payload.action === cash_drawer_enum_1.CashDrawerActionEnum.CASH_OUT) {
            inc = { $inc: { cashOut: payload.amount } };
        }
        const logic = async (session) => {
            await this.cashActivityService.transactionCreate(payload, session);
            await this.cashDrawerService.transactionUpdateOne({ _id: openCashDrawer.id }, inc, session);
            return;
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return { success: true };
    }
    async cashCount(restaurantId, payload) {
        const openCashDrawer = await this.cashDrawerService.findOne({
            restaurantId,
            cashDrawerStatus: cash_drawer_enum_1.CashDrawerStatusEnum.OPEN
        });
        if (!openCashDrawer) {
            throw new common_1.BadGatewayException('Please Starting Cash Before do another action.');
        }
        payload.restaurantId = restaurantId;
        payload.cashDrawerId = openCashDrawer.id;
        await this.cashActivityService.create(payload);
        return { success: true };
    }
    async handleCashActivity(restaurantId, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found Restaurant.');
        const { action } = payload;
        if (action === cash_drawer_enum_1.CashDrawerActionEnum.STARTING_CASH) {
            return this.startingCash(restaurantId, payload);
        }
        else if (action === cash_drawer_enum_1.CashDrawerActionEnum.CASH_IN) {
            return this.cashInOut(restaurantId, payload);
        }
        else if (action === cash_drawer_enum_1.CashDrawerActionEnum.CASH_OUT) {
            return this.cashInOut(restaurantId, payload);
        }
        else if (action === cash_drawer_enum_1.CashDrawerActionEnum.CASH_COUNT) {
            return this.cashCount(restaurantId, payload);
        }
        return { success: false };
    }
    async getSummaryCash(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found Restaurant.');
        const openCashDrawer = await this.cashDrawerService.findOne({
            restaurantId,
            cashDrawerStatus: cash_drawer_enum_1.CashDrawerStatusEnum.OPEN
        });
        if (!openCashDrawer) {
            throw new common_1.BadGatewayException('Please Starting Cash Before do another action.');
        }
        const { startingCash, totalCashSales, cashIn, cashOut } = openCashDrawer;
        const payload = {
            expectedAmount: startingCash + totalCashSales + cashIn - cashOut
        };
        return payload;
    }
    async closingCash(restaurantId, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found Restaurant.');
        const openCashDrawer = await this.cashDrawerService.findOne({
            restaurantId,
            cashDrawerStatus: cash_drawer_enum_1.CashDrawerStatusEnum.OPEN
        });
        if (!openCashDrawer) {
            throw new common_1.BadGatewayException('Please Starting Cash Before do another action.');
        }
        await this.cashDrawerService.update(openCashDrawer.id, {
            endTime: payload.date,
            actualAmount: payload.actualAmount,
            cashDrawerStatus: cash_drawer_enum_1.CashDrawerStatusEnum.CLOSED
        });
        return { success: true };
    }
    async cashSales(restaurantId, payload) {
        const openCashDrawer = await this.cashDrawerService.findOne({
            restaurantId,
            cashDrawerStatus: cash_drawer_enum_1.CashDrawerStatusEnum.OPEN
        });
        if (!openCashDrawer) {
            throw new common_1.BadGatewayException('Please Starting Cash Before do another action.');
        }
        payload.restaurantId = restaurantId;
        payload.cashDrawerId = openCashDrawer.id;
        const logic = async (session) => {
            await this.cashActivityService.transactionCreate(payload, session);
            await this.cashDrawerService.transactionUpdateOne({ _id: openCashDrawer.id }, { $inc: { totalCashSales: payload.amount } }, session);
            return;
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return { success: true };
    }
};
POSCashDrawerLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [cash_drawer_service_1.CashDrawerService,
        cash_activity_service_1.CashActivityService,
        restaurant_service_1.RestaurantService])
], POSCashDrawerLogic);
exports.POSCashDrawerLogic = POSCashDrawerLogic;
//# sourceMappingURL=pos-cash-drawer.logic.js.map