import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { ClosingCashDto } from '../dto/closing-cash.dto';
import { CashActivityDto } from '../dto/handle-cash-action.dto';
import { ICashSales, ISummaryCashForClosing } from '../interfaces/cash-drawer.interfaces';
import { CashActivityService } from '../services/cash-activity.service';
import { CashDrawerService } from '../services/cash-drawer.service';
export declare class POSCashDrawerLogic {
    private readonly cashDrawerService;
    private readonly cashActivityService;
    private readonly restaurantService;
    constructor(cashDrawerService: CashDrawerService, cashActivityService: CashActivityService, restaurantService: RestaurantService);
    getStatus(restaurantId: string): Promise<{
        isOpenCashDrawerStatus: boolean;
    }>;
    private startingCash;
    private cashInOut;
    private cashCount;
    handleCashActivity(restaurantId: string, payload: CashActivityDto): Promise<{
        success: boolean;
    }>;
    getSummaryCash(restaurantId: string): Promise<ISummaryCashForClosing>;
    closingCash(restaurantId: string, payload: ClosingCashDto): Promise<{
        success: boolean;
    }>;
    cashSales(restaurantId: string, payload: ICashSales): Promise<{
        success: boolean;
    }>;
}
