"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CashDrawerLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const cash_activity_service_1 = require("../services/cash-activity.service");
const cash_drawer_service_1 = require("../services/cash-drawer.service");
let CashDrawerLogic = class CashDrawerLogic {
    constructor(restaurantService, cashDrawerService, cashActivityService) {
        this.restaurantService = restaurantService;
        this.cashDrawerService = cashDrawerService;
        this.cashActivityService = cashActivityService;
    }
    async getCashDrawer(restaurantId, query) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found Restaurant.');
        const cashDrawers = await this.cashDrawerService.paginate(query.buildQuery(restaurantId), query);
        const { docs } = cashDrawers;
        const transform = docs.map((item) => {
            const expectedAmount = Number((item.startingCash +
                item.totalCashSales +
                item.cashIn -
                item.cashOut).toFixed(2));
            const difference = Number((item.actualAmount - expectedAmount).toFixed(2));
            const result = {
                id: item.id,
                restaurantId,
                startTime: item.startTime,
                endTime: item.endTime,
                cashDrawerStatus: item.cashDrawerStatus,
                totalCashSales: item.totalCashSales,
                cashIn: item.cashIn,
                expectedAmount,
                actualAmount: item.actualAmount,
                difference
            };
            return result;
        });
        return Object.assign(Object.assign({}, cashDrawers), { docs: transform });
    }
    async getCashDrawerActivities(id) {
        const cashDrawer = await this.cashDrawerService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!cashDrawer) {
            throw new common_1.NotFoundException('Not found cash drawer activity.');
        }
        const cashActivity = await this.cashActivityService.getAll({
            cashDrawerId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            status: 0,
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0,
            cashDrawerId: 0
        }, {
            sort: {
                date: 1
            }
        });
        const expectedAmount = Number((cashDrawer.startingCash +
            cashDrawer.totalCashSales +
            cashDrawer.cashIn -
            cashDrawer.cashOut).toFixed(2));
        const difference = Number((cashDrawer.actualAmount - expectedAmount).toFixed(2));
        const result = {
            id,
            startTime: cashDrawer.startTime,
            endTime: cashDrawer.endTime,
            activity: cashActivity,
            expectedAmount,
            actualAmount: cashDrawer.actualAmount,
            difference,
            cashDrawerStatus: cashDrawer.cashDrawerStatus
        };
        return result;
    }
};
CashDrawerLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [restaurant_service_1.RestaurantService,
        cash_drawer_service_1.CashDrawerService,
        cash_activity_service_1.CashActivityService])
], CashDrawerLogic);
exports.CashDrawerLogic = CashDrawerLogic;
//# sourceMappingURL=cash-drawer.logic.js.map