export declare class ClosingCashDto {
    date: string | Date;
    readonly actualAmount: number;
}
