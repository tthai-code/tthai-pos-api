"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CashActivityDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const cash_drawer_enum_1 = require("../common/cash-drawer.enum");
class CashActivityDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ type: 'string', example: new Date().toISOString() }),
    __metadata("design:type", Object)
], CashActivityDto.prototype, "date", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 10.2 }),
    __metadata("design:type", Number)
], CashActivityDto.prototype, "amount", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsIn)(Object.values(cash_drawer_enum_1.CashDrawerActionEnum)),
    (0, swagger_1.ApiProperty)({
        enum: Object.values(cash_drawer_enum_1.CashDrawerActionEnum).filter((item) => item !== cash_drawer_enum_1.CashDrawerActionEnum.CASH_SALES),
        example: cash_drawer_enum_1.CashDrawerActionEnum.STARTING_CASH
    }),
    __metadata("design:type", String)
], CashActivityDto.prototype, "action", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: 'Kevin Start' }),
    __metadata("design:type", String)
], CashActivityDto.prototype, "user", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)({ example: 'Starting evening shift' }),
    __metadata("design:type", String)
], CashActivityDto.prototype, "comment", void 0);
exports.CashActivityDto = CashActivityDto;
//# sourceMappingURL=handle-cash-action.dto.js.map