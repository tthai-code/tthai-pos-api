export declare class CashActivityDto {
    restaurantId: string;
    cashDrawerId: string;
    date: string | Date;
    readonly amount: number;
    readonly action: string;
    readonly user: string;
    readonly comment: string;
}
