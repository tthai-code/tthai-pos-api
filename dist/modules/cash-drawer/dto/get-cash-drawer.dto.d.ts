import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
export declare class PaginateCashDrawerDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly startDate: string;
    readonly endDate: string;
    buildQuery(restaurantId: string): {
        startTime: {
            $gte: Date;
            $lte: Date;
        };
        restaurantId: string;
        status: StatusEnum;
    };
}
