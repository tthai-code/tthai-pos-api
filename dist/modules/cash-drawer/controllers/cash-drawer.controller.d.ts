import { PaginateCashDrawerDto } from '../dto/get-cash-drawer.dto';
import { CashDrawerLogic } from '../logic/cash-drawer.logic';
import { CashDrawerService } from '../services/cash-drawer.service';
export declare class CashDrawerController {
    private readonly cashDrawerService;
    private readonly cashDrawerLogic;
    constructor(cashDrawerService: CashDrawerService, cashDrawerLogic: CashDrawerLogic);
    getCashDrawers(query: PaginateCashDrawerDto): Promise<any>;
    getCashActivity(id: string): Promise<{
        id: string;
        startTime: Date;
        endTime: Date;
        activity: any[];
        expectedAmount: number;
        actualAmount: number;
        difference: number;
        cashDrawerStatus: string;
    }>;
}
