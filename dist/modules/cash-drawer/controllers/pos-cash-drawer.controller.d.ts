import { LogLogic } from 'src/modules/log/logic/log.logic';
import { ClosingCashDto } from '../dto/closing-cash.dto';
import { CashActivityDto } from '../dto/handle-cash-action.dto';
import { POSCashDrawerLogic } from '../logic/pos-cash-drawer.logic';
import { CashDrawerService } from '../services/cash-drawer.service';
export declare class POSCashDrawerController {
    private readonly cashDrawerService;
    private readonly posCashDrawerLogic;
    private readonly logLogic;
    constructor(cashDrawerService: CashDrawerService, posCashDrawerLogic: POSCashDrawerLogic, logLogic: LogLogic);
    getStatus(): Promise<{
        isOpenCashDrawerStatus: boolean;
    }>;
    handleCashAction(payload: CashActivityDto, request: any): Promise<{
        success: boolean;
    }>;
    getSummaryCash(request: any): Promise<import("../interfaces/cash-drawer.interfaces").ISummaryCashForClosing>;
    closingCash(payload: ClosingCashDto): Promise<{
        success: boolean;
    }>;
}
