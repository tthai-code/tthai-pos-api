"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSCashDrawerController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const log_logic_1 = require("../../log/logic/log.logic");
const closing_cash_dto_1 = require("../dto/closing-cash.dto");
const handle_cash_action_dto_1 = require("../dto/handle-cash-action.dto");
const pos_cash_drawer_entity_1 = require("../entity/pos-cash-drawer.entity");
const pos_cash_drawer_logic_1 = require("../logic/pos-cash-drawer.logic");
const cash_drawer_service_1 = require("../services/cash-drawer.service");
let POSCashDrawerController = class POSCashDrawerController {
    constructor(cashDrawerService, posCashDrawerLogic, logLogic) {
        this.cashDrawerService = cashDrawerService;
        this.posCashDrawerLogic = posCashDrawerLogic;
        this.logLogic = logLogic;
    }
    async getStatus() {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.posCashDrawerLogic.getStatus(restaurantId);
    }
    async handleCashAction(payload, request) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const cashDrawer = await this.posCashDrawerLogic.handleCashActivity(restaurantId, payload);
        await this.logLogic.createLogic(request, 'Create Cash Drawer Action', cashDrawer);
        return cashDrawer;
    }
    async getSummaryCash(request) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const summaryCash = await this.posCashDrawerLogic.getSummaryCash(restaurantId);
        await this.logLogic.createLogic(request, 'Get Expected Amount Before Closing Cash', summaryCash);
        return summaryCash;
    }
    async closingCash(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.posCashDrawerLogic.closingCash(restaurantId, payload);
    }
};
__decorate([
    (0, common_1.Get)('status'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_cash_drawer_entity_1.CashDrawerStatusResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Action Before Create Action',
        description: 'use bearer `staff_token`'
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], POSCashDrawerController.prototype, "getStatus", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create Cash Drawer Action',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [handle_cash_action_dto_1.CashActivityDto, Object]),
    __metadata("design:returntype", Promise)
], POSCashDrawerController.prototype, "handleCashAction", null);
__decorate([
    (0, common_1.Get)('summary'),
    (0, swagger_1.ApiOkResponse)({ type: () => pos_cash_drawer_entity_1.SummaryClosingCashResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Expected Amount Before Closing Cash',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], POSCashDrawerController.prototype, "getSummaryCash", null);
__decorate([
    (0, common_1.Post)('closing'),
    (0, swagger_1.ApiCreatedResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Closing Cash Drawer Action',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [closing_cash_dto_1.ClosingCashDto]),
    __metadata("design:returntype", Promise)
], POSCashDrawerController.prototype, "closingCash", null);
POSCashDrawerController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/cash-drawer'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/cash-drawer'),
    __metadata("design:paramtypes", [cash_drawer_service_1.CashDrawerService,
        pos_cash_drawer_logic_1.POSCashDrawerLogic,
        log_logic_1.LogLogic])
], POSCashDrawerController);
exports.POSCashDrawerController = POSCashDrawerController;
//# sourceMappingURL=pos-cash-drawer.controller.js.map