"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CashDrawerController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const get_cash_drawer_dto_1 = require("../dto/get-cash-drawer.dto");
const cash_drawer_entity_1 = require("../entity/cash-drawer.entity");
const cash_drawer_logic_1 = require("../logic/cash-drawer.logic");
const cash_drawer_service_1 = require("../services/cash-drawer.service");
let CashDrawerController = class CashDrawerController {
    constructor(cashDrawerService, cashDrawerLogic) {
        this.cashDrawerService = cashDrawerService;
        this.cashDrawerLogic = cashDrawerLogic;
    }
    async getCashDrawers(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.cashDrawerLogic.getCashDrawer(restaurantId, query);
    }
    async getCashActivity(id) {
        return await this.cashDrawerLogic.getCashDrawerActivities(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => cash_drawer_entity_1.PaginateCashDrawerResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'get cash drawer' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_cash_drawer_dto_1.PaginateCashDrawerDto]),
    __metadata("design:returntype", Promise)
], CashDrawerController.prototype, "getCashDrawers", null);
__decorate([
    (0, common_1.Get)(':cashDrawerId'),
    (0, swagger_1.ApiOkResponse)({ type: () => cash_drawer_entity_1.CashDrawerWithActivityResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get Cash Activity by Cash Drawer ID' }),
    __param(0, (0, common_1.Param)('cashDrawerId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CashDrawerController.prototype, "getCashActivity", null);
CashDrawerController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('cash-drawer'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/cash-drawer'),
    __metadata("design:paramtypes", [cash_drawer_service_1.CashDrawerService,
        cash_drawer_logic_1.CashDrawerLogic])
], CashDrawerController);
exports.CashDrawerController = CashDrawerController;
//# sourceMappingURL=cash-drawer.controller.js.map