"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CashActivitySchema = exports.CashActivityFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const cash_drawer_enum_1 = require("../common/cash-drawer.enum");
let CashActivityFields = class CashActivityFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'cashDrawers' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], CashActivityFields.prototype, "cashDrawerId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", Date)
], CashActivityFields.prototype, "date", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], CashActivityFields.prototype, "amount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ required: true }),
    __metadata("design:type", String)
], CashActivityFields.prototype, "user", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: '' }),
    __metadata("design:type", String)
], CashActivityFields.prototype, "comment", void 0);
__decorate([
    (0, mongoose_1.Prop)({ enum: Object.values(cash_drawer_enum_1.CashDrawerActionEnum) }),
    __metadata("design:type", String)
], CashActivityFields.prototype, "action", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], CashActivityFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], CashActivityFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], CashActivityFields.prototype, "createdBy", void 0);
CashActivityFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'cashActivities' })
], CashActivityFields);
exports.CashActivityFields = CashActivityFields;
exports.CashActivitySchema = mongoose_1.SchemaFactory.createForClass(CashActivityFields);
exports.CashActivitySchema.plugin(mongoosePaginate);
exports.CashActivitySchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=cash-activity.schema.js.map