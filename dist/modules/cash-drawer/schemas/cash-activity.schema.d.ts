import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type CashActivityDocument = CashActivityFields & Document;
export declare class CashActivityFields {
    cashDrawerId: Types.ObjectId;
    date: Date;
    amount: number;
    user: string;
    comment: string;
    action: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const CashActivitySchema: MongooseSchema<Document<CashActivityFields, any, any>, import("mongoose").Model<Document<CashActivityFields, any, any>, any, any, any>, any, any>;
