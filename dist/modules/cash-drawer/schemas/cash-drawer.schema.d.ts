import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type CashDrawerDocument = CashDrawerFields & Document;
export declare class CashDrawerFields {
    restaurantId: Types.ObjectId;
    startTime: Date;
    endTime: Date;
    cashDrawerStatus: string;
    startingCash: number;
    totalCashSales: number;
    cashIn: number;
    cashOut: number;
    actualAmount: number;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const CashDrawerSchema: MongooseSchema<Document<CashDrawerFields, any, any>, import("mongoose").Model<Document<CashDrawerFields, any, any>, any, any, any>, any, any>;
