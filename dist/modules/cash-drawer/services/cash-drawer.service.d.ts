import { ClientSession, Model } from 'mongoose';
import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { CashDrawerDocument } from '../schemas/cash-drawer.schema';
export declare class CashDrawerService {
    private readonly CashDrawerModel;
    private request;
    constructor(CashDrawerModel: PaginateModel<CashDrawerDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<CashDrawerDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<CashDrawerDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<CashDrawerDocument>;
    createMany(payload: any): Promise<any>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<CashDrawerDocument>;
    update(id: string, product: any): Promise<CashDrawerDocument>;
    updateOne(condition: any, payload: any): Promise<CashDrawerDocument>;
    updateMany(condition: any, payload: any): Promise<Array<any>>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<CashDrawerDocument>;
    transactionUpdateOne(condition: any, payload: any, session: ClientSession): Promise<any>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
    delete(id: string): Promise<CashDrawerDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<CashDrawerDocument>;
    paginate(query: any, queryParam: any, select?: any): PaginateResult<CashDrawerDocument>;
}
