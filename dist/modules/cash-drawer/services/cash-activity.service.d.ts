import { ClientSession, Model } from 'mongoose';
import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { CashActivityDocument } from '../schemas/cash-activity.schema';
export declare class CashActivityService {
    private readonly CashActivityModel;
    private request;
    constructor(CashActivityModel: PaginateModel<CashActivityDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<CashActivityDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<CashActivityDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<CashActivityDocument>;
    createMany(payload: any): Promise<any>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<CashActivityDocument>;
    update(id: string, product: any): Promise<CashActivityDocument>;
    updateOne(condition: any, payload: any): Promise<CashActivityDocument>;
    updateMany(condition: any, payload: any): Promise<Array<any>>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<CashActivityDocument>;
    transactionUpdateMany(condition: any, payload: any, session: ClientSession): Promise<any>;
    delete(id: string): Promise<CashActivityDocument>;
    getAll(condition: any, project?: any, options?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<CashActivityDocument>;
    paginate(query: any, queryParam: any, select?: any): PaginateResult<CashActivityDocument>;
}
