"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllTableZoneResponse = exports.TableZoneResponseFields = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class TableZoneResponseFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableZoneResponseFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TableZoneResponseFields.prototype, "name", void 0);
exports.TableZoneResponseFields = TableZoneResponseFields;
class GetAllTableZoneResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: TableZoneResponseFields,
        example: [
            {
                name: 'Table',
                id: '6312697243656a16831e950c'
            },
            {
                name: 'Bar',
                id: '63128eda000e63764c3b59fc'
            }
        ]
    }),
    __metadata("design:type", Array)
], GetAllTableZoneResponse.prototype, "data", void 0);
exports.GetAllTableZoneResponse = GetAllTableZoneResponse;
//# sourceMappingURL=table-zone.entity.js.map