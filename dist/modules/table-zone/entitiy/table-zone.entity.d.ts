import { ResponseDto } from 'src/common/entity/response.entity';
export declare class TableZoneResponseFields {
    id: string;
    name: string;
}
export declare class GetAllTableZoneResponse extends ResponseDto<Array<TableZoneResponseFields>> {
    data: Array<TableZoneResponseFields>;
}
