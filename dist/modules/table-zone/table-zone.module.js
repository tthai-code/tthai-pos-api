"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableZoneModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const table_zone_controller_1 = require("./controllers/table-zone.controller");
const table_zone_schema_1 = require("./models/table-zone.schema");
const table_zone_service_1 = require("./services/table-zone.service");
let TableZoneModule = class TableZoneModule {
};
TableZoneModule = __decorate([
    (0, common_1.Module)({
        imports: [
            mongoose_1.MongooseModule.forFeature([{ name: 'tableZone', schema: table_zone_schema_1.TableZoneSchema }])
        ],
        providers: [table_zone_service_1.TableZoneService],
        exports: [table_zone_service_1.TableZoneService],
        controllers: [table_zone_controller_1.TableZoneController]
    })
], TableZoneModule);
exports.TableZoneModule = TableZoneModule;
//# sourceMappingURL=table-zone.module.js.map