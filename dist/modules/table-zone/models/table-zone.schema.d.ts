import { Types, Document, Schema as MongooseSchema } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type TableZoneDocument = TableZoneFields & Document;
export declare class TableZoneFields {
    restaurantId: Types.ObjectId;
    name: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const TableZoneSchema: MongooseSchema<Document<TableZoneFields, any, any>, import("mongoose").Model<Document<TableZoneFields, any, any>, any, any, any>, any, any>;
