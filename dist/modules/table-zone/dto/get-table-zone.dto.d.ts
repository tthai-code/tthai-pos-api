import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class TableZonePaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly restaurant: string;
    readonly status: string;
    buildQuery(id: string): {
        $or: {
            name: {
                $regex: string;
                $options: string;
            };
        }[];
        restaurantId: {
            $eq: string;
        };
        status: string | {
            $ne: StatusEnum;
        };
    };
}
