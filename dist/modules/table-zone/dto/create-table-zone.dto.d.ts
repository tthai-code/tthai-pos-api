export declare class CreateTableZoneDto {
    readonly restaurantId: string;
    readonly name: string;
    readonly status: string;
}
