import { StatusEnum } from 'src/common/enum/status.enum';
export declare class AllTableZonePaginateDto {
    buildQuery(id: string): {
        restaurantId: {
            $eq: string;
        };
        status: StatusEnum;
    };
}
