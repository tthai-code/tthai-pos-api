"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllTableZonePaginateDto = void 0;
const status_enum_1 = require("../../../common/enum/status.enum");
class AllTableZonePaginateDto {
    buildQuery(id) {
        const result = {
            restaurantId: { $eq: id },
            status: status_enum_1.StatusEnum.ACTIVE
        };
        return result;
    }
}
exports.AllTableZonePaginateDto = AllTableZonePaginateDto;
//# sourceMappingURL=get-all-table-zone.dto.js.map