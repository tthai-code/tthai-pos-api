import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { TableZoneDocument } from '../models/table-zone.schema';
import { TableZonePaginateDto } from '../dto/get-table-zone.dto';
export declare class TableZoneService {
    private readonly TableZoneModel;
    private request;
    constructor(TableZoneModel: PaginateModel<TableZoneDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<TableZoneDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<TableZoneDocument>;
    getSession(): Promise<ClientSession>;
    create(payload: any): Promise<TableZoneDocument>;
    update(id: string, SeatType: any): Promise<TableZoneDocument>;
    delete(id: string): Promise<TableZoneDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any, options?: any): Promise<TableZoneDocument>;
    paginate(query: any, queryParam: TableZonePaginateDto, populate?: any): Promise<PaginateResult<TableZoneDocument>>;
}
