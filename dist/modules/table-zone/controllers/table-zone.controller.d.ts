import { CreateTableZoneDto } from '../dto/create-table-zone.dto';
import { AllTableZonePaginateDto } from '../dto/get-all-table-zone.dto';
import { TableZonePaginateDto } from '../dto/get-table-zone.dto';
import { UpdateTableZoneDto } from '../dto/update-table-zone.dto';
import { TableZoneService } from '../services/table-zone.service';
export declare class TableZoneController {
    private readonly tableZoneService;
    constructor(tableZoneService: TableZoneService);
    getTableZones(query: TableZonePaginateDto): Promise<PaginateResult<import("../models/table-zone.schema").TableZoneDocument>>;
    getAllTableZone(query: AllTableZonePaginateDto): Promise<any[]>;
    createTableZone(payload: CreateTableZoneDto): Promise<import("../models/table-zone.schema").TableZoneDocument>;
    getTableZone(id: string): Promise<import("../models/table-zone.schema").TableZoneDocument>;
    updateTableZone(id: string, payload: UpdateTableZoneDto): Promise<import("../models/table-zone.schema").TableZoneDocument>;
    deleteTableZone(id: string): Promise<import("../models/table-zone.schema").TableZoneDocument>;
}
