"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableZoneController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const create_table_zone_dto_1 = require("../dto/create-table-zone.dto");
const get_all_table_zone_dto_1 = require("../dto/get-all-table-zone.dto");
const get_table_zone_dto_1 = require("../dto/get-table-zone.dto");
const update_table_zone_dto_1 = require("../dto/update-table-zone.dto");
const table_zone_entity_1 = require("../entitiy/table-zone.entity");
const table_zone_service_1 = require("../services/table-zone.service");
let TableZoneController = class TableZoneController {
    constructor(tableZoneService) {
        this.tableZoneService = tableZoneService;
    }
    async getTableZones(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.tableZoneService.paginate(query.buildQuery(restaurantId), query, { path: 'amount', match: { status: status_enum_1.StatusEnum.ACTIVE } });
    }
    async getAllTableZone(query) {
        var _a, _b;
        const restaurantId = (_b = (_a = admin_auth_guard_1.AdminAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        return await this.tableZoneService.getAll(query.buildQuery(restaurantId), {
            id: 1,
            name: 1
        });
    }
    async createTableZone(payload) {
        return await this.tableZoneService.create(payload);
    }
    async getTableZone(id) {
        return await this.tableZoneService.findOne({ _id: id });
    }
    async updateTableZone(id, payload) {
        return await this.tableZoneService.update(id, payload);
    }
    async deleteTableZone(id) {
        return await this.tableZoneService.delete(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Get List Paginate Table Zone for Management' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_table_zone_dto_1.TableZonePaginateDto]),
    __metadata("design:returntype", Promise)
], TableZoneController.prototype, "getTableZones", null);
__decorate([
    (0, common_1.Get)('all'),
    (0, swagger_1.ApiOperation)({ summary: 'Get all table zone for POS' }),
    (0, swagger_1.ApiOkResponse)({ type: () => table_zone_entity_1.GetAllTableZoneResponse }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_all_table_zone_dto_1.AllTableZonePaginateDto]),
    __metadata("design:returntype", Promise)
], TableZoneController.prototype, "getAllTableZone", null);
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Create a Table Zone' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_table_zone_dto_1.CreateTableZoneDto]),
    __metadata("design:returntype", Promise)
], TableZoneController.prototype, "createTableZone", null);
__decorate([
    (0, common_1.Get)(':id/table-zone'),
    (0, swagger_1.ApiOperation)({ summary: 'Get Table Zone By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TableZoneController.prototype, "getTableZone", null);
__decorate([
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Update Table Zone' }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_table_zone_dto_1.UpdateTableZoneDto]),
    __metadata("design:returntype", Promise)
], TableZoneController.prototype, "updateTableZone", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiOperation)({ summary: 'Delete Table Zone (Soft)' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TableZoneController.prototype, "deleteTableZone", null);
TableZoneController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('table-zone'),
    (0, common_1.Controller)('v1/table-zone'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    __metadata("design:paramtypes", [table_zone_service_1.TableZoneService])
], TableZoneController);
exports.TableZoneController = TableZoneController;
//# sourceMappingURL=table-zone.controller.js.map