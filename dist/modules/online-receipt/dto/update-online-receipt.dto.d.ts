export declare class UpdateOnlineReceiptDto {
    readonly restaurantLogoUrl: string;
    readonly facebookUrl: string;
    readonly instagramUrl: string;
    readonly twitterUrl: string;
}
