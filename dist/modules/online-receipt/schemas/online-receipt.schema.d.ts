import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type OnlineReceiptDocument = OnlineReceiptFields & Document;
export declare class OnlineReceiptFields {
    restaurantId: Types.ObjectId;
    restaurantLogoUrl: string;
    facebookUrl: string;
    instagramUrl: string;
    twitterUrl: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const OnlineReceiptSchema: MongooseSchema<Document<OnlineReceiptFields, any, any>, import("mongoose").Model<Document<OnlineReceiptFields, any, any>, any, any, any>, any, any>;
