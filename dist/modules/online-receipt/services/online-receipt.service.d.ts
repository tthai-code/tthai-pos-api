import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { OnlineReceiptDocument } from '../schemas/online-receipt.schema';
export declare class OnlineReceiptService {
    private readonly OnlineReceiptModel;
    private request;
    constructor(OnlineReceiptModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<OnlineReceiptDocument | any>;
    resolveByCondition(condition: any): Promise<OnlineReceiptDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<OnlineReceiptDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<OnlineReceiptDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<OnlineReceiptDocument>;
    create(payload: any): Promise<OnlineReceiptDocument>;
    update(id: string, payload: any): Promise<OnlineReceiptDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<OnlineReceiptDocument>;
    delete(id: string): Promise<OnlineReceiptDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<OnlineReceiptDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<OnlineReceiptDocument>;
}
