"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOnlineReceiptResponse = exports.OnlineReceiptResponseField = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
class OnlineReceiptResponseField extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineReceiptResponseField.prototype, "twitterUrl", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineReceiptResponseField.prototype, "instagramUrl", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineReceiptResponseField.prototype, "facebookUrl", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineReceiptResponseField.prototype, "restaurantLogoUrl", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OnlineReceiptResponseField.prototype, "restaurantId", void 0);
exports.OnlineReceiptResponseField = OnlineReceiptResponseField;
class GetOnlineReceiptResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: OnlineReceiptResponseField,
        example: {
            status: 'active',
            twitter_url: null,
            instagram_url: null,
            facebook_url: null,
            restaurant_logo_url: 'https://storage.googleapis.com/tthai-pos-staging/upload/166538232509038624.png',
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-10-10T12:21:36.160Z',
            updated_at: '2022-10-10T12:23:29.612Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '63440e5016effaa8e241f2e1'
        }
    }),
    __metadata("design:type", OnlineReceiptResponseField)
], GetOnlineReceiptResponse.prototype, "data", void 0);
exports.GetOnlineReceiptResponse = GetOnlineReceiptResponse;
//# sourceMappingURL=online-receipt.entity.js.map