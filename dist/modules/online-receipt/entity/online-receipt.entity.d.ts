import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
export declare class OnlineReceiptResponseField extends TimestampResponseDto {
    twitterUrl: string;
    instagramUrl: string;
    facebookUrl: string;
    restaurantLogoUrl: string;
    restaurantId: string;
}
export declare class GetOnlineReceiptResponse extends ResponseDto<OnlineReceiptResponseField> {
    data: OnlineReceiptResponseField;
}
