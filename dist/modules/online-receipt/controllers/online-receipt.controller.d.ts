import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdateOnlineReceiptDto } from '../dto/update-online-receipt.dto';
import { OnlineReceiptService } from '../services/online-receipt.service';
export declare class OnlineReceiptController {
    private readonly onlineReceiptService;
    private readonly restaurantService;
    constructor(onlineReceiptService: OnlineReceiptService, restaurantService: RestaurantService);
    getOnlineReceipt(id: string): Promise<import("../schemas/online-receipt.schema").OnlineReceiptDocument>;
    updateOnlineReceipt(id: string, payload: UpdateOnlineReceiptDto): Promise<import("../schemas/online-receipt.schema").OnlineReceiptDocument>;
}
