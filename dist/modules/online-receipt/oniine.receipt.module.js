"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OnlineReceiptModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const online_receipt_controller_1 = require("./controllers/online-receipt.controller");
const online_receipt_schema_1 = require("./schemas/online-receipt.schema");
const online_receipt_service_1 = require("./services/online-receipt.service");
let OnlineReceiptModule = class OnlineReceiptModule {
};
OnlineReceiptModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'onlineReceipt', schema: online_receipt_schema_1.OnlineReceiptSchema }
            ])
        ],
        providers: [online_receipt_service_1.OnlineReceiptService],
        controllers: [online_receipt_controller_1.OnlineReceiptController],
        exports: [online_receipt_service_1.OnlineReceiptService]
    })
], OnlineReceiptModule);
exports.OnlineReceiptModule = OnlineReceiptModule;
//# sourceMappingURL=oniine.receipt.module.js.map