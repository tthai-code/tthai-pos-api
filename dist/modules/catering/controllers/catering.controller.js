"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CateringController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const update_catering_dto_1 = require("../dto/update-catering.dto");
const catering_entity_1 = require("../enitity/catering.entity");
const catering_service_1 = require("../services/catering.service");
let CateringController = class CateringController {
    constructor(cateringService, restaurantService) {
        this.cateringService = cateringService;
        this.restaurantService = restaurantService;
    }
    async getCatering(id) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const catering = await this.cateringService.findOne({
            restaurantId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!catering) {
            await this.cateringService.create({
                restaurantId: id
            });
            return await this.cateringService.findOne({
                restaurantId: id,
                status: status_enum_1.StatusEnum.ACTIVE
            });
        }
        return catering;
    }
    async updateOnlineReceipt(id, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        return await this.cateringService.findOneAndUpdate({ restaurantId: id }, payload);
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => catering_entity_1.GetCateringResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'get catering setting by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CateringController.prototype, "getCatering", null);
__decorate([
    (0, common_1.Put)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => catering_entity_1.GetCateringResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'update catering setting by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_catering_dto_1.UpdateCateringDto]),
    __metadata("design:returntype", Promise)
], CateringController.prototype, "updateOnlineReceipt", null);
CateringController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('catering-setting'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/catering'),
    __metadata("design:paramtypes", [catering_service_1.CateringService,
        restaurant_service_1.RestaurantService])
], CateringController);
exports.CateringController = CateringController;
//# sourceMappingURL=catering.controller.js.map