import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdateCateringDto } from '../dto/update-catering.dto';
import { CateringService } from '../services/catering.service';
export declare class CateringController {
    private readonly cateringService;
    private readonly restaurantService;
    constructor(cateringService: CateringService, restaurantService: RestaurantService);
    getCatering(id: string): Promise<import("../schemas/catering.schema").CateringDocument>;
    updateOnlineReceipt(id: string, payload: UpdateCateringDto): Promise<import("../schemas/catering.schema").CateringDocument>;
}
