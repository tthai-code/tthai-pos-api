import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { CateringDocument } from '../schemas/catering.schema';
export declare class CateringService {
    private readonly CateringModel;
    private request;
    constructor(CateringModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<CateringDocument | any>;
    resolveByCondition(condition: any): Promise<CateringDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<CateringDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<CateringDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<CateringDocument>;
    create(payload: any): Promise<CateringDocument>;
    update(id: string, payload: any): Promise<CateringDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<CateringDocument>;
    delete(id: string): Promise<CateringDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<CateringDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<CateringDocument>;
}
