export declare class UpdateCateringDto {
    readonly isActive: boolean;
    readonly deposit: number;
    readonly minimumDeliveryTime: number;
}
