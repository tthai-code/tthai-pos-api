"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetCateringResponse = exports.CateringResponseField = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
class CateringResponseField extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CateringResponseField.prototype, "minimum_delivery_time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CateringResponseField.prototype, "deposit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], CateringResponseField.prototype, "is_active", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CateringResponseField.prototype, "restaurant_id", void 0);
exports.CateringResponseField = CateringResponseField;
class GetCateringResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: CateringResponseField,
        example: {
            status: 'active',
            minimum_delivery_time: 15,
            deposit: 1,
            is_active: true,
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-10-10T12:35:20.193Z',
            updated_at: '2022-10-10T12:35:58.048Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '634411882921cfce8d7d54dc'
        }
    }),
    __metadata("design:type", CateringResponseField)
], GetCateringResponse.prototype, "data", void 0);
exports.GetCateringResponse = GetCateringResponse;
//# sourceMappingURL=catering.entity.js.map