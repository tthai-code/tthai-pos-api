import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
export declare class CateringResponseField extends TimestampResponseDto {
    minimum_delivery_time: number;
    deposit: number;
    is_active: boolean;
    restaurant_id: string;
}
export declare class GetCateringResponse extends ResponseDto<CateringResponseField> {
    data: CateringResponseField;
}
