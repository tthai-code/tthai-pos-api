import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type CateringDocument = CateringFields & Document;
export declare class CateringFields {
    restaurantId: Types.ObjectId;
    isActive: boolean;
    deposit: number;
    minimumDeliveryTime: number;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const CateringSchema: MongooseSchema<Document<CateringFields, any, any>, import("mongoose").Model<Document<CateringFields, any, any>, any, any, any>, any, any>;
