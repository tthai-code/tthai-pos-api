import { CreateUserDto } from '../dto/create-user.dto';
import { UserService } from '../services/user.service';
import { UpdateUserDto } from '../dto/update-user.dto';
import { ChangeUserPwdDto } from '../dto/change-user-pwd.dto';
export declare class UserLogic {
    private readonly userService;
    constructor(userService: UserService);
    createUserLogic(payload: CreateUserDto): Promise<import("../schemas/user.schema").UserDocument>;
    updateUserLogic(id: string, payload: UpdateUserDto): Promise<import("../schemas/user.schema").UserDocument>;
    changeUserPasswordLogic(id: string, payload: ChangeUserPwdDto): Promise<import("../schemas/user.schema").UserDocument>;
}
