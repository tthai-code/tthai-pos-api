"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserLogic = void 0;
const common_1 = require("@nestjs/common");
const user_service_1 = require("../services/user.service");
const bcrypt = require("bcryptjs");
const status_enum_1 = require("../../../common/enum/status.enum");
let UserLogic = class UserLogic {
    constructor(userService) {
        this.userService = userService;
    }
    async createUserLogic(payload) {
        const userData = await this.userService.findOne({
            status: status_enum_1.StatusEnum.ACTIVE,
            username: payload.username
        });
        if (userData) {
            throw new common_1.BadRequestException(`Username ${payload.username} already used.`);
        }
        const salt = bcrypt.genSaltSync(10);
        payload.password = bcrypt.hashSync(payload.password, salt);
        return this.userService.create(payload);
    }
    async updateUserLogic(id, payload) {
        const userData = await this.userService.findOne({
            status: status_enum_1.StatusEnum.ACTIVE,
            username: payload.username,
            _id: { $ne: id }
        });
        if (userData) {
            throw new common_1.BadRequestException(`Username ${payload.username} already used.`);
        }
        return await this.userService.update(id, payload);
    }
    async changeUserPasswordLogic(id, payload) {
        const salt = bcrypt.genSaltSync(10);
        payload.password = bcrypt.hashSync(payload.password, salt);
        return await this.userService.update(id, payload);
    }
};
UserLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserLogic);
exports.UserLogic = UserLogic;
//# sourceMappingURL=user.logic.js.map