"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserWithOutPasswordResponse = exports.UserResponse = exports.GetAllUserResponse = exports.LoginUserResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
class LoginUserFields {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], LoginUserFields.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], LoginUserFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], LoginUserFields.prototype, "role", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], LoginUserFields.prototype, "token_expire", void 0);
class LoginUserResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: LoginUserFields,
        example: {
            username: 'superadmin',
            id: '6318d06a1fa78236d380db83',
            role: 'SUPER_ADMIN',
            token_expire: 1668165517453
        }
    }),
    __metadata("design:type", LoginUserFields)
], LoginUserResponse.prototype, "data", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: '<access-token>' }),
    __metadata("design:type", String)
], LoginUserResponse.prototype, "access_token", void 0);
exports.LoginUserResponse = LoginUserResponse;
class GetAllUserObject extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: [
            {
                status: 'active',
                role: 'SUPER_ADMIN',
                password: '$2b$10$Rwr1n7umjOT9LX9NwdrbqeucI1UP3/Zp1oQP.9AHmv45cJs4XjUKu',
                username: 'superadmin',
                created_at: '2022-09-07T17:10:02.187Z',
                updated_at: '2022-09-07T17:10:02.187Z',
                updated_by: {
                    username: 'system',
                    id: '0'
                },
                created_by: {
                    username: 'system',
                    id: '0'
                },
                id: '6318d06a1fa78236d380db83'
            }
        ]
    }),
    __metadata("design:type", Array)
], GetAllUserObject.prototype, "results", void 0);
class GetAllUserResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", GetAllUserObject)
], GetAllUserResponse.prototype, "data", void 0);
exports.GetAllUserResponse = GetAllUserResponse;
class UserResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            status: 'active',
            role: 'SUPER_ADMIN',
            password: '$2b$10$Rwr1n7umjOT9LX9NwdrbqeucI1UP3/Zp1oQP.9AHmv45cJs4XjUKu',
            username: 'superadmin',
            created_at: '2022-09-07T17:10:02.187Z',
            updated_at: '2022-09-07T17:10:02.187Z',
            updated_by: {
                username: 'system',
                id: '0'
            },
            created_by: {
                username: 'system',
                id: '0'
            },
            id: '6318d06a1fa78236d380db83'
        }
    }),
    __metadata("design:type", Object)
], UserResponse.prototype, "data", void 0);
exports.UserResponse = UserResponse;
class UserWithOutPasswordResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            status: 'active',
            role: 'SUPER_ADMIN',
            username: 'superadmin',
            created_at: '2022-09-07T17:10:02.187Z',
            updated_at: '2022-09-07T17:10:02.187Z',
            updated_by: {
                username: 'system',
                id: '0'
            },
            created_by: {
                username: 'system',
                id: '0'
            },
            id: '6318d06a1fa78236d380db83'
        }
    }),
    __metadata("design:type", Object)
], UserWithOutPasswordResponse.prototype, "data", void 0);
exports.UserWithOutPasswordResponse = UserWithOutPasswordResponse;
//# sourceMappingURL=user.entity.js.map