import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class LoginUserFields {
    username: string;
    id: string;
    role: string;
    token_expire: string;
}
export declare class LoginUserResponse extends ResponseDto<LoginUserFields> {
    data: LoginUserFields;
    access_token: string;
}
declare class GetAllUserObject extends PaginateResponseDto<any[]> {
    results: any[];
}
export declare class GetAllUserResponse extends ResponseDto<GetAllUserObject> {
    data: GetAllUserObject;
}
export declare class UserResponse extends ResponseDto<any> {
    data: any;
}
export declare class UserWithOutPasswordResponse extends ResponseDto<any> {
    data: any;
}
export {};
