"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = exports.UserFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const role_enum_1 = require("../common/role.enum");
let UserFields = class UserFields {
};
__decorate([
    (0, mongoose_1.Prop)({ required: true, index: true }),
    __metadata("design:type", String)
], UserFields.prototype, "username", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], UserFields.prototype, "password", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: role_enum_1.UserRoleEnum.SUPER_ADMIN,
        enum: Object.values(role_enum_1.UserRoleEnum)
    }),
    __metadata("design:type", String)
], UserFields.prototype, "role", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], UserFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], UserFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], UserFields.prototype, "createdBy", void 0);
UserFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'users' })
], UserFields);
exports.UserFields = UserFields;
exports.UserSchema = mongoose_1.SchemaFactory.createForClass(UserFields);
exports.UserSchema.plugin(mongoosePaginate);
exports.UserSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=user.schema.js.map