import * as mongoose from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type UserDocument = UserFields & mongoose.Document;
export declare class UserFields {
    username: string;
    password: string;
    role: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const UserSchema: mongoose.Schema<mongoose.Document<UserFields, any, any>, mongoose.Model<mongoose.Document<UserFields, any, any>, any, any, any>, any, any>;
