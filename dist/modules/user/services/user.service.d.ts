import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { UserDocument } from '../schemas/user.schema';
import { UserPaginateDto } from '../dto/get-user.dto';
export declare class UserService {
    private readonly UserModel;
    private request;
    constructor(UserModel: PaginateModel<UserDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<UserDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<UserDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<UserDocument>;
    create(payload: any): Promise<UserDocument>;
    update(id: string, User: any): Promise<UserDocument>;
    delete(id: string): Promise<UserDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any): Promise<UserDocument>;
    findOneWithPassword(condition: any): Promise<UserDocument>;
    getOne(condition: any, project?: any): Promise<UserDocument>;
    paginate(query: any, queryParam: UserPaginateDto, select?: any): Promise<PaginateResult<UserDocument>>;
}
