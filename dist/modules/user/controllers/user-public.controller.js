"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPublicController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const auth_logic_1 = require("../../auth/logics/auth.logic");
const login_dto_1 = require("../dto/login.dto");
const user_entity_1 = require("../entity/user.entity");
let UserPublicController = class UserPublicController {
    constructor(authLogic) {
        this.authLogic = authLogic;
    }
    async login(login) {
        const userData = await this.authLogic.loginLogic(login);
        return userData;
    }
};
__decorate([
    (0, common_1.Post)('login'),
    (0, swagger_1.ApiOkResponse)({ type: () => user_entity_1.LoginUserResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Login' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [login_dto_1.LoginDto]),
    __metadata("design:returntype", Promise)
], UserPublicController.prototype, "login", null);
UserPublicController = __decorate([
    (0, swagger_1.ApiTags)('public/user'),
    (0, common_1.Controller)('public/user'),
    __metadata("design:paramtypes", [auth_logic_1.AuthLogic])
], UserPublicController);
exports.UserPublicController = UserPublicController;
//# sourceMappingURL=user-public.controller.js.map