import { AuthLogic } from 'src/modules/auth/logics/auth.logic';
import { LoginDto } from '../dto/login.dto';
export declare class UserPublicController {
    private readonly authLogic;
    constructor(authLogic: AuthLogic);
    login(login: LoginDto): Promise<{
        accessToken: string;
        tokenExpire: number;
        username: string;
        id: any;
        role: string;
    }>;
}
