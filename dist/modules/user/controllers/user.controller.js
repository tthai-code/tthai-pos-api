"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const role_decorator_1 = require("../../../decorators/role.decorator");
const super_admin_auth_guard_1 = require("../../auth/guards/super-admin-auth.guard");
const role_enum_1 = require("../common/role.enum");
const change_user_pwd_dto_1 = require("../dto/change-user-pwd.dto");
const create_user_dto_1 = require("../dto/create-user.dto");
const get_user_dto_1 = require("../dto/get-user.dto");
const update_user_dto_1 = require("../dto/update-user.dto");
const user_entity_1 = require("../entity/user.entity");
const user_logic_1 = require("../logics/user.logic");
const user_service_1 = require("../services/user.service");
let UserController = class UserController {
    constructor(userService, userLogic) {
        this.userService = userService;
        this.userLogic = userLogic;
    }
    async getUsers(query) {
        return await this.userService.paginate(query.buildQuery(), query);
    }
    async getUser(id) {
        return await this.userService.findOne({ _id: id });
    }
    async createUser(user) {
        return this.userLogic.createUserLogic(user);
    }
    async userUpdate(user, id) {
        return this.userLogic.updateUserLogic(id, user);
    }
    async changePasswordUpdate(user, id) {
        return this.userLogic.changeUserPasswordLogic(id, user);
    }
    async deleteUser(id) {
        return this.userService.update(id, { status: status_enum_1.StatusEnum.DELETED });
    }
};
__decorate([
    (0, role_decorator_1.Roles)(role_enum_1.UserRoleEnum.SUPER_ADMIN),
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => user_entity_1.GetAllUserResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get User List' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_user_dto_1.UserPaginateDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUsers", null);
__decorate([
    (0, role_decorator_1.Roles)(role_enum_1.UserRoleEnum.SUPER_ADMIN),
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => user_entity_1.UserWithOutPasswordResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Get User By ID' }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUser", null);
__decorate([
    (0, role_decorator_1.Roles)(role_enum_1.UserRoleEnum.SUPER_ADMIN),
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => user_entity_1.UserResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Create User' }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_user_dto_1.CreateUserDto]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "createUser", null);
__decorate([
    (0, role_decorator_1.Roles)(role_enum_1.UserRoleEnum.SUPER_ADMIN),
    (0, common_1.Put)(':id'),
    (0, swagger_1.ApiOkResponse)({ type: () => user_entity_1.UserResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Update User' }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_user_dto_1.UpdateUserDto, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "userUpdate", null);
__decorate([
    (0, role_decorator_1.Roles)(role_enum_1.UserRoleEnum.SUPER_ADMIN),
    (0, common_1.Put)(':id/password'),
    (0, swagger_1.ApiOkResponse)({ type: () => user_entity_1.UserResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Change User Password' }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [change_user_pwd_dto_1.ChangeUserPwdDto, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "changePasswordUpdate", null);
__decorate([
    (0, role_decorator_1.Roles)(role_enum_1.UserRoleEnum.SUPER_ADMIN),
    (0, common_1.Delete)(),
    (0, swagger_1.ApiOkResponse)({ type: () => user_entity_1.UserResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'Delete User' }),
    __param(0, (0, common_1.Query)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "deleteUser", null);
UserController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('users-admin'),
    (0, common_1.Controller)('v1/users'),
    (0, common_1.UseGuards)(super_admin_auth_guard_1.SuperAdminAuthGuard),
    __metadata("design:paramtypes", [user_service_1.UserService,
        user_logic_1.UserLogic])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map