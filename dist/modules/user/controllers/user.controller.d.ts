import { ChangeUserPwdDto } from '../dto/change-user-pwd.dto';
import { CreateUserDto } from '../dto/create-user.dto';
import { UserPaginateDto } from '../dto/get-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { UserLogic } from '../logics/user.logic';
import { UserService } from '../services/user.service';
export declare class UserController {
    private readonly userService;
    private readonly userLogic;
    constructor(userService: UserService, userLogic: UserLogic);
    getUsers(query: UserPaginateDto): Promise<PaginateResult<import("../schemas/user.schema").UserDocument>>;
    getUser(id: string): Promise<import("../schemas/user.schema").UserDocument>;
    createUser(user: CreateUserDto): Promise<import("../schemas/user.schema").UserDocument>;
    userUpdate(user: UpdateUserDto, id: string): Promise<import("../schemas/user.schema").UserDocument>;
    changePasswordUpdate(user: ChangeUserPwdDto, id: string): Promise<import("../schemas/user.schema").UserDocument>;
    deleteUser(id: string): Promise<import("../schemas/user.schema").UserDocument>;
}
