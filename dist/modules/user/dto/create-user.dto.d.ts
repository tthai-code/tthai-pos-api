export declare class CreateUserDto {
    readonly username: string;
    password: string;
    readonly role: string;
}
