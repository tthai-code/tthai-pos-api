import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class UserPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly status: string;
    readonly search: string;
    readonly role: string;
    buildQuery(): {
        $or: {
            username: {
                $regex: string;
                $options: string;
            };
        }[];
        role: string | {
            $ne: any;
        };
        status: string | {
            $ne: StatusEnum;
        };
    };
}
