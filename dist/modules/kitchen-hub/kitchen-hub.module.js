"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenHubModule = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const mongoose_1 = require("@nestjs/mongoose");
const order_module_1 = require("../order/order.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const socket_module_1 = require("../socket/socket.module");
const transaction_module_1 = require("../transaction/transaction.module");
const tthai_app_module_1 = require("../tthai-app/tthai-app.module");
const open_kitchen_hub_controller_1 = require("./controllers/open-kitchen-hub.controller");
const pos_delivery_setting_controller_1 = require("./controllers/pos-delivery-setting.controller");
const pos_kitchen_hub_controller_1 = require("./controllers/pos-kitchen-hub.controller");
const public_queue_controller_1 = require("./controllers/public-queue.controller");
const delivery_setting_logic_1 = require("./logic/delivery-setting.logic");
const kitchen_hub_logic_1 = require("./logic/kitchen-hub.logic");
const pos_kitchen_hub_logic_1 = require("./logic/pos-kitchen-hub.logic");
const delivery_setting_schema_1 = require("./schemas/delivery-setting.schema");
const kitchen_hub_schema_1 = require("./schemas/kitchen-hub.schema");
const delivery_setting_service_1 = require("./services/delivery-setting.service");
const kitchen_hub_http_service_1 = require("./services/kitchen-hub-http.service");
const kitchen_hub_service_1 = require("./services/kitchen-hub.service");
let KitchenHubModule = class KitchenHubModule {
};
KitchenHubModule = __decorate([
    (0, common_1.Module)({
        imports: [
            axios_1.HttpModule,
            config_1.ConfigModule,
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            (0, common_1.forwardRef)(() => transaction_module_1.TransactionModule),
            (0, common_1.forwardRef)(() => socket_module_1.SocketModule),
            (0, common_1.forwardRef)(() => order_module_1.OrderModule),
            (0, common_1.forwardRef)(() => tthai_app_module_1.TThaiAppModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'kitchenHub', schema: kitchen_hub_schema_1.KitchenHubSchema },
                { name: 'deliverySetting', schema: delivery_setting_schema_1.DeliverySettingSchema }
            ])
        ],
        providers: [
            kitchen_hub_service_1.KitchenHubService,
            delivery_setting_service_1.DeliverySettingService,
            delivery_setting_logic_1.DeliverySettingLogic,
            kitchen_hub_http_service_1.KitchenHubHttpService,
            kitchen_hub_logic_1.KitchenHubLogic,
            pos_kitchen_hub_logic_1.POSKitchenHubLogic
        ],
        controllers: [
            open_kitchen_hub_controller_1.OpenKitchenHubController,
            pos_delivery_setting_controller_1.POSDeliverySettingController,
            pos_kitchen_hub_controller_1.POSKitchenHubController,
            public_queue_controller_1.PublicQueueController
        ],
        exports: [kitchen_hub_http_service_1.KitchenHubHttpService, delivery_setting_service_1.DeliverySettingService]
    })
], KitchenHubModule);
exports.KitchenHubModule = KitchenHubModule;
//# sourceMappingURL=kitchen-hub.module.js.map