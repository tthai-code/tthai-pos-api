"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateKitchenHubOrderDto = exports.TimestampsObjectDto = exports.ChargesObjectDto = exports.PaymentObjectDto = exports.DeliveryObjectDto = exports.CourierObjectDto = exports.AddressObjectDto = exports.ItemObjectDto = exports.OptionObjectDto = exports.CustomerObjectDto = exports.StoreObjectDto = exports.LocationObjectDto = exports.ProviderObjectDto = exports.OrderObjectDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const kitchen_order_status_enum_1 = require("../common/kitchen-order-status.enum");
const kitchen_order_type_enum_1 = require("../common/kitchen-order-type.enum");
class OrderObjectDto {
}
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderObjectDto.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderObjectDto.prototype, "externalId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OrderObjectDto.prototype, "number", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OrderObjectDto.prototype, "dailyNumber", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(kitchen_order_type_enum_1.KitchenHubOrderTypeEnum),
    (0, swagger_1.ApiProperty)({ enum: Object.values(kitchen_order_type_enum_1.KitchenHubOrderTypeEnum) }),
    __metadata("design:type", String)
], OrderObjectDto.prototype, "type", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(kitchen_order_status_enum_1.KitchenHubOrderStatusEnum),
    (0, swagger_1.ApiProperty)({ enum: Object.values(kitchen_order_status_enum_1.KitchenHubOrderStatusEnum) }),
    __metadata("design:type", String)
], OrderObjectDto.prototype, "status", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], OrderObjectDto.prototype, "notes", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], OrderObjectDto.prototype, "paid", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], OrderObjectDto.prototype, "asap", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Boolean)
], OrderObjectDto.prototype, "addDisposableItems", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], OrderObjectDto.prototype, "prepTimeMinutes", void 0);
exports.OrderObjectDto = OrderObjectDto;
class ProviderObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ProviderObjectDto.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ProviderObjectDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ProviderObjectDto.prototype, "merchantId", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ProviderObjectDto.prototype, "storeId", void 0);
exports.ProviderObjectDto = ProviderObjectDto;
class LocationObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], LocationObjectDto.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], LocationObjectDto.prototype, "partnerLocationId", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], LocationObjectDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], LocationObjectDto.prototype, "street", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], LocationObjectDto.prototype, "city", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], LocationObjectDto.prototype, "state", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], LocationObjectDto.prototype, "zipCode", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], LocationObjectDto.prototype, "lat", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], LocationObjectDto.prototype, "lon", void 0);
exports.LocationObjectDto = LocationObjectDto;
class StoreObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StoreObjectDto.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], StoreObjectDto.prototype, "partnerStoreId", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StoreObjectDto.prototype, "name", void 0);
exports.StoreObjectDto = StoreObjectDto;
class CustomerObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerObjectDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CustomerObjectDto.prototype, "email", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CustomerObjectDto.prototype, "phoneNumber", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CustomerObjectDto.prototype, "phoneCode", void 0);
exports.CustomerObjectDto = CustomerObjectDto;
class OptionObjectDto {
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], OptionObjectDto.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OptionObjectDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], OptionObjectDto.prototype, "quantity", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], OptionObjectDto.prototype, "basePrice", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], OptionObjectDto.prototype, "price", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], OptionObjectDto.prototype, "modifierName", void 0);
exports.OptionObjectDto = OptionObjectDto;
class ItemObjectDto {
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ItemObjectDto.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemObjectDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ItemObjectDto.prototype, "quantity", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ItemObjectDto.prototype, "basePrice", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ItemObjectDto.prototype, "price", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ItemObjectDto.prototype, "instructions", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => OptionObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: [OptionObjectDto] }),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Array)
], ItemObjectDto.prototype, "options", void 0);
exports.ItemObjectDto = ItemObjectDto;
class AddressObjectDto {
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], AddressObjectDto.prototype, "country", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], AddressObjectDto.prototype, "street", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], AddressObjectDto.prototype, "city", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], AddressObjectDto.prototype, "state", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], AddressObjectDto.prototype, "zipCode", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], AddressObjectDto.prototype, "unitNumber", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], AddressObjectDto.prototype, "latitude", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", Number)
], AddressObjectDto.prototype, "longitude", void 0);
exports.AddressObjectDto = AddressObjectDto;
class CourierObjectDto {
}
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CourierObjectDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CourierObjectDto.prototype, "photoUrl", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CourierObjectDto.prototype, "phoneNumber", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], CourierObjectDto.prototype, "status", void 0);
exports.CourierObjectDto = CourierObjectDto;
class DeliveryObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], DeliveryObjectDto.prototype, "type", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], DeliveryObjectDto.prototype, "status", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], DeliveryObjectDto.prototype, "notes", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => AddressObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: AddressObjectDto }),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", AddressObjectDto)
], DeliveryObjectDto.prototype, "address", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => CourierObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: CourierObjectDto }),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", CourierObjectDto)
], DeliveryObjectDto.prototype, "courier", void 0);
exports.DeliveryObjectDto = DeliveryObjectDto;
class PaymentObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PaymentObjectDto.prototype, "method", void 0);
exports.PaymentObjectDto = PaymentObjectDto;
class ChargesObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ChargesObjectDto.prototype, "subtotal", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ChargesObjectDto.prototype, "tax", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ChargesObjectDto.prototype, "tips", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], ChargesObjectDto.prototype, "discount", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ChargesObjectDto.prototype, "total", void 0);
exports.ChargesObjectDto = ChargesObjectDto;
class TimestampsObjectDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TimestampsObjectDto.prototype, "placedAt", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TimestampsObjectDto.prototype, "createdAt", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], TimestampsObjectDto.prototype, "acceptedAt", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], TimestampsObjectDto.prototype, "cancelledAt", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], TimestampsObjectDto.prototype, "completedAt", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsOptional)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], TimestampsObjectDto.prototype, "pickupAt", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], TimestampsObjectDto.prototype, "deliveryAt", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiPropertyOptional)(),
    __metadata("design:type", String)
], TimestampsObjectDto.prototype, "scheduledFor", void 0);
exports.TimestampsObjectDto = TimestampsObjectDto;
class CreateKitchenHubOrderDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => OrderObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: OrderObjectDto }),
    __metadata("design:type", OrderObjectDto)
], CreateKitchenHubOrderDto.prototype, "order", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => ProviderObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: ProviderObjectDto }),
    __metadata("design:type", ProviderObjectDto)
], CreateKitchenHubOrderDto.prototype, "provider", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => LocationObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: LocationObjectDto }),
    __metadata("design:type", LocationObjectDto)
], CreateKitchenHubOrderDto.prototype, "location", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => StoreObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: StoreObjectDto }),
    __metadata("design:type", Object)
], CreateKitchenHubOrderDto.prototype, "store", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => CustomerObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: CustomerObjectDto }),
    __metadata("design:type", CustomerObjectDto)
], CreateKitchenHubOrderDto.prototype, "customer", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => ItemObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: [ItemObjectDto] }),
    __metadata("design:type", Array)
], CreateKitchenHubOrderDto.prototype, "items", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => DeliveryObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: DeliveryObjectDto }),
    __metadata("design:type", DeliveryObjectDto)
], CreateKitchenHubOrderDto.prototype, "delivery", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => PaymentObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: PaymentObjectDto }),
    __metadata("design:type", PaymentObjectDto)
], CreateKitchenHubOrderDto.prototype, "payment", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => ChargesObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: ChargesObjectDto }),
    __metadata("design:type", ChargesObjectDto)
], CreateKitchenHubOrderDto.prototype, "charges", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => TimestampsObjectDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: TimestampsObjectDto }),
    __metadata("design:type", TimestampsObjectDto)
], CreateKitchenHubOrderDto.prototype, "timestamps", void 0);
exports.CreateKitchenHubOrderDto = CreateKitchenHubOrderDto;
//# sourceMappingURL=create-kitchen-hub.dto.js.map