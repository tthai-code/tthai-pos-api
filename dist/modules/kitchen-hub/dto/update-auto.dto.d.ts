export declare class UpdateAutoAcceptDto {
    readonly isAutoAccept: boolean;
}
export declare class PauseProviderDto {
    provider: string;
    online: boolean;
}
