import { PaginateDto } from 'src/common/dto/paginate.dto';
import { StatusEnum } from 'src/common/enum/status.enum';
export declare class KitchenHubPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    buildQuery(): {
        $or: {
            'order.id': {
                $eq: string;
            };
        }[];
        status: StatusEnum;
    };
}
