export declare class OrderObjectDto {
    id: number;
    externalId: string;
    number: string;
    dailyNumber: number;
    type: string;
    status: string;
    notes: string;
    paid: boolean;
    asap: boolean;
    addDisposableItems: boolean;
    prepTimeMinutes: number;
}
export declare class ProviderObjectDto {
    id: string;
    name: string;
    merchantId: string;
    storeId: string;
}
export declare class LocationObjectDto {
    id: string;
    partnerLocationId: string;
    name: string;
    street: string;
    city: string;
    state: string;
    zipCode: string;
    lat: number;
    lon: number;
}
export declare class StoreObjectDto {
    id: string;
    partnerStoreId: string;
    name: string;
}
export declare class CustomerObjectDto {
    name: string;
    email: string;
    phoneNumber: string;
    phoneCode: string;
}
export declare class OptionObjectDto {
    id: string;
    name: string;
    quantity: number;
    basePrice: string;
    price: string;
    modifierName: string;
}
export declare class ItemObjectDto {
    id: string;
    name: string;
    quantity: number;
    basePrice: string;
    price: string;
    instructions: string;
    options: OptionObjectDto[];
}
export declare class AddressObjectDto {
    country: string;
    street: string;
    city: string;
    state: string;
    zipCode: string;
    unitNumber: string;
    latitude: number;
    longitude: number;
}
export declare class CourierObjectDto {
    name: string;
    photoUrl: string;
    phoneNumber: string;
    status: string;
}
export declare class DeliveryObjectDto {
    type: string;
    status: string;
    notes: string;
    address: AddressObjectDto;
    courier: CourierObjectDto;
}
export declare class PaymentObjectDto {
    method: string;
}
export declare class ChargesObjectDto {
    subtotal: string;
    tax: string;
    tips: string;
    discount: string;
    total: string;
}
export declare class TimestampsObjectDto {
    placedAt: string;
    createdAt: string;
    acceptedAt: string;
    cancelledAt: string;
    completedAt: string;
    pickupAt: string;
    deliveryAt: string;
    scheduledFor: string;
}
export declare class CreateKitchenHubOrderDto {
    order: OrderObjectDto;
    provider: ProviderObjectDto;
    location: LocationObjectDto;
    store: any;
    customer: CustomerObjectDto;
    items: ItemObjectDto[];
    delivery: DeliveryObjectDto;
    payment: PaymentObjectDto;
    charges: ChargesObjectDto;
    timestamps: TimestampsObjectDto;
}
