export declare class AcceptOrderDto {
    readonly orderId: string;
    readonly preparationTime: number;
}
