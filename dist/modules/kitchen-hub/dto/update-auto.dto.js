"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PauseProviderDto = exports.UpdateAutoAcceptDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const provider_enum_1 = require("../common/provider.enum");
class UpdateAutoAcceptDto {
}
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: true }),
    __metadata("design:type", Boolean)
], UpdateAutoAcceptDto.prototype, "isAutoAccept", void 0);
exports.UpdateAutoAcceptDto = UpdateAutoAcceptDto;
const providerEnum = Object.values(provider_enum_1.ProviderEnum).filter((item) => item !== provider_enum_1.ProviderEnum.GLORIAFOOD);
class PauseProviderDto {
}
__decorate([
    (0, class_validator_1.IsIn)(providerEnum),
    (0, swagger_1.ApiProperty)({
        enum: providerEnum,
        example: provider_enum_1.ProviderEnum.DOORDASH
    }),
    __metadata("design:type", String)
], PauseProviderDto.prototype, "provider", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: false }),
    __metadata("design:type", Boolean)
], PauseProviderDto.prototype, "online", void 0);
exports.PauseProviderDto = PauseProviderDto;
//# sourceMappingURL=update-auto.dto.js.map