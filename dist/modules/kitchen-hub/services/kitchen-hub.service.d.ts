import { PaginateModel, PaginateResult } from 'mongoose-paginate';
import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { KitchenHubDocument } from '../schemas/kitchen-hub.schema';
export declare class KitchenHubService {
    private readonly KitchenHubModel;
    private request;
    constructor(KitchenHubModel: PaginateModel<KitchenHubDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<KitchenHubDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<KitchenHubDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<KitchenHubDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<KitchenHubDocument>;
    update(id: string, product: any): Promise<KitchenHubDocument>;
    updateOne(condition: any, payload: any): Promise<KitchenHubDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<KitchenHubDocument>;
    delete(id: string): Promise<KitchenHubDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<KitchenHubDocument>;
    paginate(query: any, queryParam: any, select?: any): Promise<PaginateResult<KitchenHubDocument>>;
}
