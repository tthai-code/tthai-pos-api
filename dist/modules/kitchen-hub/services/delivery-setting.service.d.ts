import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { DeliverySettingDocument } from '../schemas/delivery-setting.schema';
export declare class DeliverySettingService {
    private readonly DeliverySettingModel;
    private request;
    constructor(DeliverySettingModel: Model<DeliverySettingDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<DeliverySettingDocument | any>;
    getRequest(): any;
    count(query: any): any;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<DeliverySettingDocument>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<DeliverySettingDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    create(payload: any): Promise<DeliverySettingDocument>;
    update(id: string, product: any): Promise<DeliverySettingDocument>;
    updateOne(condition: any, payload: any): Promise<DeliverySettingDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<DeliverySettingDocument>;
    delete(id: string): Promise<DeliverySettingDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: any): Promise<any>;
    findOne(condition: any, project?: any): Promise<DeliverySettingDocument>;
}
