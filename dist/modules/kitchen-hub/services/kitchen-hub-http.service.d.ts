import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { IAuthProvider, IConnectProvider, IDisconnectProvider, ISetProviderStatus } from '../interfaces/kitchen-hub.interface';
export declare class KitchenHubHttpService {
    private readonly httpService;
    private readonly configService;
    private request;
    constructor(httpService: HttpService, configService: ConfigService, request: any);
    private readonly url;
    private readonly key;
    private readonly locationId;
    private readonly options;
    ping(): Promise<any>;
    getLocation(locationId?: string): Promise<any>;
    createStore(storeName: string, locationId?: string, partnerStoreId?: string): Promise<any>;
    authProvider(payload: IAuthProvider): Promise<any>;
    connectProvider(payload: IConnectProvider): Promise<any>;
    disconnectProvider(payload: IDisconnectProvider): Promise<any>;
    getStoreProviders(storeId: string): Promise<any>;
    getProviderStatus(providerId: string, storeId: string): Promise<any>;
    setProviderStatus(payload: ISetProviderStatus): Promise<any>;
    acceptOrder(orderId: string, prepTime: number): Promise<any>;
    cancelOrder(orderId: string): Promise<any>;
    completeOrder(orderId: string): Promise<any>;
}
