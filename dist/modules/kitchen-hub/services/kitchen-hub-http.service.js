"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenHubHttpService = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const core_1 = require("@nestjs/core");
let KitchenHubHttpService = class KitchenHubHttpService {
    constructor(httpService, configService, request) {
        this.httpService = httpService;
        this.configService = configService;
        this.request = request;
        this.url = this.configService.get('KITCHEN_HUB_API_URL');
        this.key = this.configService.get('KITCHEN_HUB_API_KEY');
        this.locationId = this.configService.get('KITCHEN_HUB_LOCATION_ID');
        this.options = {
            headers: {
                'x-api-key': this.key
            }
        };
    }
    async ping() {
        return this.httpService
            .post(`${this.url}/ping`, null, this.options)
            .toPromise();
    }
    async getLocation(locationId = this.locationId) {
        return this.httpService
            .post(`${this.url}/getLocation`, {
            location_id: locationId
        }, this.options)
            .toPromise();
    }
    async createStore(storeName, locationId = this.locationId, partnerStoreId) {
        return this.httpService
            .post(`${this.url}/createStore`, {
            store_name: storeName,
            location_id: locationId,
            partner_store_id: partnerStoreId || null
        }, this.options)
            .toPromise();
    }
    async authProvider(payload) {
        return this.httpService
            .post(`${this.url}/authProvider`, payload, this.options)
            .toPromise();
    }
    async connectProvider(payload) {
        return this.httpService
            .post(`${this.url}/connectProvider`, payload, this.options)
            .toPromise();
    }
    async disconnectProvider(payload) {
        return this.httpService
            .post(`${this.url}/disconnectProvider`, payload, this.options)
            .toPromise();
    }
    async getStoreProviders(storeId) {
        return this.httpService
            .post(`${this.url}/getStoreProviders`, { store_id: storeId }, this.options)
            .toPromise();
    }
    async getProviderStatus(providerId, storeId) {
        return this.httpService
            .post(`${this.url}/getProviderStatus`, { provider_id: providerId, store_id: storeId }, this.options)
            .toPromise();
    }
    async setProviderStatus(payload) {
        return this.httpService
            .post(`${this.url}/setProviderStatus`, Object.assign({ online: true }, payload), this.options)
            .toPromise();
    }
    async acceptOrder(orderId, prepTime) {
        const payload = {
            order_id: orderId,
            prep_time_minutes: prepTime
        };
        return this.httpService
            .post(`${this.url}/acceptOrder`, payload, this.options)
            .toPromise();
    }
    async cancelOrder(orderId) {
        return this.httpService
            .post(`${this.url}/cancelOrder`, { order_id: orderId }, this.options)
            .toPromise();
    }
    async completeOrder(orderId) {
        return this.httpService
            .post(`${this.url}/completeOrder`, { order_id: orderId }, this.options)
            .toPromise();
    }
};
KitchenHubHttpService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(2, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [axios_1.HttpService,
        config_1.ConfigService, Object])
], KitchenHubHttpService);
exports.KitchenHubHttpService = KitchenHubHttpService;
//# sourceMappingURL=kitchen-hub-http.service.js.map