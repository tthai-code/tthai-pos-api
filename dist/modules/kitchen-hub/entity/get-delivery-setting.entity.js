"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetDeliverySettingResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const provider_enum_1 = require("../common/provider.enum");
class ProviderObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(provider_enum_1.ProviderEnum) }),
    __metadata("design:type", String)
], ProviderObject.prototype, "provider", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], ProviderObject.prototype, "is_connected", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], ProviderObject.prototype, "online", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], ProviderObject.prototype, "can_pause", void 0);
class DeliverySettingObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], DeliverySettingObject.prototype, "is_auto_accept", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], DeliverySettingObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], DeliverySettingObject.prototype, "online_count", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ProviderObject] }),
    __metadata("design:type", Array)
], DeliverySettingObject.prototype, "providers", void 0);
class GetDeliverySettingResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: DeliverySettingObject,
        example: {
            is_auto_accept: true,
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            online_count: 1,
            providers: [
                {
                    provider: 'doordash',
                    is_connected: false,
                    online: false,
                    can_pause: true
                },
                {
                    provider: 'grubhub',
                    is_connected: false,
                    online: false,
                    can_pause: true
                },
                {
                    provider: 'ubereats',
                    is_connected: false,
                    online: false,
                    can_pause: true
                },
                {
                    provider: 'gloriafood',
                    is_connected: true,
                    online: true,
                    can_pause: false
                }
            ]
        }
    }),
    __metadata("design:type", DeliverySettingObject)
], GetDeliverySettingResponse.prototype, "data", void 0);
exports.GetDeliverySettingResponse = GetDeliverySettingResponse;
//# sourceMappingURL=get-delivery-setting.entity.js.map