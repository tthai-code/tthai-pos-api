import { ResponseDto } from 'src/common/entity/response.entity';
declare class ProviderObject {
    provider: string;
    is_connected: boolean;
    online: boolean;
    can_pause: boolean;
}
declare class DeliverySettingObject {
    is_auto_accept: boolean;
    restaurant_id: string;
    online_count: number;
    providers: ProviderObject[];
}
export declare class GetDeliverySettingResponse extends ResponseDto<DeliverySettingObject> {
    data: DeliverySettingObject;
}
export {};
