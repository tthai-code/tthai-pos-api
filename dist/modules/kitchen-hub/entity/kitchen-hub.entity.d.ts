import { PaginateResponseDto } from 'src/common/entity/paginate-response.entitiy';
import { ResponseDto } from 'src/common/entity/response.entity';
declare class SummaryStatusObject {
    active: number;
    now: number;
    inProgress: number;
    ready: number;
    upcoming: number;
    completed: number;
}
declare class SummaryTypeObject {
    delivery: number;
    pickup: number;
}
declare class SummarySourceObject {
    doordash: number;
    gloriafood: number;
    grubhub: number;
    ubereats: number;
}
declare class ModifiersObject {
    id: string;
    name: string;
    label: string;
    abbreviation: string;
    native_name: string;
    price: number;
}
declare class ThirdPartyOrderItemObject {
    id: string;
    name: string;
    unit: number;
    price: number;
    amount: number;
    modifiers: ModifiersObject[];
    note: string;
}
declare class StaffObject {
    id: string;
    first_name: string;
    last_name: string;
}
declare class CustomerObject {
    tel: string;
    last_name: string;
    first_name: string;
}
declare class AddressObject {
    zip_code: string;
    state: string;
    city: string;
    address: string;
}
declare class RestaurantObject {
    email: string;
    business_phone: string;
    address: AddressObject;
    legal_business_name: string;
    id: string;
}
declare class KitchenHubObject {
    completed_date: string;
    note: string;
    order_k_h_id: number;
    delivery_source: string;
    order_status: string;
    total: number;
    tips: number;
    convenience_fee: number;
    alcohol_tax: number;
    tax: number;
    service_charge: number;
    discount: number;
    subtotal: number;
    summary_items: ThirdPartyOrderItemObject[];
    scheduled_for_date: string;
    asap: boolean;
    pick_up_date: string;
    order_date: string;
    number_of_guest: number;
    order_method: string;
    order_type: string;
    staff: StaffObject;
    customer: CustomerObject;
    restaurant: RestaurantObject;
    id: string;
    ticket_no: string;
    prepare_time: number;
}
declare class PaginateKitchenHubOrder extends PaginateResponseDto<KitchenHubObject[]> {
    status: SummaryStatusObject;
    type: SummaryTypeObject;
    source: SummarySourceObject;
    results: KitchenHubObject[];
}
export declare class PaginateKitchenHubResponse extends ResponseDto<PaginateKitchenHubOrder> {
    data: PaginateKitchenHubOrder;
}
declare class GetOrderByIdObject extends KitchenHubObject {
    ticket_no: string;
}
export declare class GetThirdPartyOrderByIdResponse extends ResponseDto<GetOrderByIdObject> {
    data: GetOrderByIdObject;
}
export {};
