"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetThirdPartyOrderByIdResponse = exports.PaginateKitchenHubResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const paginate_response_entitiy_1 = require("../../../common/entity/paginate-response.entitiy");
const response_entity_1 = require("../../../common/entity/response.entity");
const order_state_enum_1 = require("../../order/enum/order-state.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const provider_enum_1 = require("../common/provider.enum");
class SummaryStatusObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryStatusObject.prototype, "active", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryStatusObject.prototype, "now", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryStatusObject.prototype, "inProgress", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryStatusObject.prototype, "ready", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryStatusObject.prototype, "upcoming", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryStatusObject.prototype, "completed", void 0);
class SummaryTypeObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryTypeObject.prototype, "delivery", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummaryTypeObject.prototype, "pickup", void 0);
class SummarySourceObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummarySourceObject.prototype, "doordash", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummarySourceObject.prototype, "gloriafood", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummarySourceObject.prototype, "grubhub", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], SummarySourceObject.prototype, "ubereats", void 0);
class ModifiersObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersObject.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersObject.prototype, "abbreviation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ModifiersObject.prototype, "native_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ModifiersObject.prototype, "price", void 0);
class ThirdPartyOrderItemObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ThirdPartyOrderItemObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ThirdPartyOrderItemObject.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ThirdPartyOrderItemObject.prototype, "unit", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ThirdPartyOrderItemObject.prototype, "price", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], ThirdPartyOrderItemObject.prototype, "amount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ModifiersObject] }),
    __metadata("design:type", Array)
], ThirdPartyOrderItemObject.prototype, "modifiers", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], ThirdPartyOrderItemObject.prototype, "note", void 0);
class StaffObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffObject.prototype, "first_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], StaffObject.prototype, "last_name", void 0);
class CustomerObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerObject.prototype, "tel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerObject.prototype, "last_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomerObject.prototype, "first_name", void 0);
class AddressObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "zip_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "address", void 0);
class RestaurantObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", AddressObject)
], RestaurantObject.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "legal_business_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "id", void 0);
class KitchenHubObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "completed_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "order_k_h_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(provider_enum_1.ProviderEnum) }),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "delivery_source", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        enum: [
            order_state_enum_1.OrderStateEnum.NEW,
            order_state_enum_1.OrderStateEnum.IN_PROGRESS,
            order_state_enum_1.OrderStateEnum.READY,
            order_state_enum_1.OrderStateEnum.UPCOMING,
            order_state_enum_1.OrderStateEnum.COMPLETED,
            order_state_enum_1.OrderStateEnum.CANCELED
        ]
    }),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "order_status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "total", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "tips", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "convenience_fee", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "discount", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "subtotal", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [ThirdPartyOrderItemObject] }),
    __metadata("design:type", Array)
], KitchenHubObject.prototype, "summary_items", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "scheduled_for_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], KitchenHubObject.prototype, "asap", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "pick_up_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "order_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "number_of_guest", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(order_type_enum_1.OrderMethodEnum) }),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "order_method", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(order_type_enum_1.OrderTypeEnum) }),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "order_type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: StaffObject }),
    __metadata("design:type", StaffObject)
], KitchenHubObject.prototype, "staff", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: CustomerObject }),
    __metadata("design:type", CustomerObject)
], KitchenHubObject.prototype, "customer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: RestaurantObject }),
    __metadata("design:type", RestaurantObject)
], KitchenHubObject.prototype, "restaurant", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], KitchenHubObject.prototype, "ticket_no", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], KitchenHubObject.prototype, "prepare_time", void 0);
class PaginateKitchenHubOrder extends paginate_response_entitiy_1.PaginateResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            active: 6,
            new: 0,
            inProgress: 0,
            ready: 0,
            upcoming: 0,
            completed: 0
        }
    }),
    __metadata("design:type", SummaryStatusObject)
], PaginateKitchenHubOrder.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            delivery: 0,
            pickup: 6
        }
    }),
    __metadata("design:type", SummaryTypeObject)
], PaginateKitchenHubOrder.prototype, "type", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: {
            doordash: 0,
            gloriafood: 6,
            grubhub: 0,
            ubereats: 0
        }
    }),
    __metadata("design:type", SummarySourceObject)
], PaginateKitchenHubOrder.prototype, "source", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        type: [KitchenHubObject],
        example: {
            completed_date: null,
            prepare_time: 44,
            note: '',
            order_k_h_id: 5191520877805568,
            delivery_source: 'gloriafood',
            order_status: 'UPCOMING',
            total: 3.11,
            tips: 0,
            convenience_fee: 0,
            alcohol_tax: 0,
            tax: 0.12,
            service_charge: 0,
            discount: 0,
            subtotal: 2.99,
            summary_items: [
                {
                    id: '2487615',
                    name: '20 oz. Bottled Soda',
                    unit: 1,
                    price: 2.99,
                    amount: 2.99,
                    modifiers: [
                        {
                            id: '7241500',
                            name: 'Very very long long long option name to test multiline',
                            label: 'Choose a drink',
                            abbreviation: 'C',
                            native_name: 'Choose a drink',
                            price: 0
                        }
                    ],
                    note: null
                }
            ],
            scheduled_for_date: null,
            asap: true,
            pick_up_date: '2022-12-01T14:53:34.000Z',
            order_date: '2022-12-01T14:09:34.000Z',
            number_of_guest: 1,
            order_method: 'PICKUP',
            order_type: 'THIRD_PARTY',
            staff: null,
            customer: {
                tel: '6262145123',
                last_name: 'Test',
                first_name: 'Tthai'
            },
            restaurant: {
                email: 'corywong@mail.com',
                business_phone: '0222222222',
                address: {
                    zip_code: '75001',
                    state: 'Dallas',
                    city: 'Addison',
                    address: 'Some Address'
                },
                legal_business_name: 'Holy Beef',
                id: '630e55a0d9c30fd7cdcb424b'
            },
            id: 'jQxRHeHt',
            ticket_no: '8'
        }
    }),
    __metadata("design:type", Array)
], PaginateKitchenHubOrder.prototype, "results", void 0);
class PaginateKitchenHubResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", PaginateKitchenHubOrder)
], PaginateKitchenHubResponse.prototype, "data", void 0);
exports.PaginateKitchenHubResponse = PaginateKitchenHubResponse;
class GetOrderByIdObject extends KitchenHubObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], GetOrderByIdObject.prototype, "ticket_no", void 0);
class GetThirdPartyOrderByIdResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: GetOrderByIdObject,
        example: {
            completed_date: null,
            note: '',
            prepare_time: 44,
            order_k_h_id: 5191520877805568,
            delivery_source: 'gloriafood',
            order_status: 'UPCOMING',
            total: 3.11,
            tips: 0,
            convenience_fee: 0,
            alcohol_tax: 0,
            tax: 0.12,
            service_charge: 0,
            discount: 0,
            subtotal: 2.99,
            scheduled_for_date: null,
            asap: true,
            pick_up_date: '2022-12-01T14:53:34.000Z',
            order_date: '2022-12-01T14:09:34.000Z',
            number_of_guest: 1,
            order_method: 'PICKUP',
            order_type: 'THIRD_PARTY',
            staff: null,
            customer: {
                id: null,
                first_name: 'Tthai',
                last_name: 'Test',
                tel: '6262145123'
            },
            restaurant: {
                email: 'corywong@mail.com',
                business_phone: '0222222222',
                address: {
                    zip_code: '75001',
                    state: 'Dallas',
                    city: 'Addison',
                    address: 'Some Address'
                },
                legal_business_name: 'Holy Beef',
                id: '630e55a0d9c30fd7cdcb424b'
            },
            id: 'jQxRHeHt',
            items: [
                {
                    item_status: 'COMPLETED',
                    note: null,
                    amount: 2.99,
                    unit: 1,
                    modifiers: [
                        {
                            id: '7241500',
                            name: 'Very very long long long option name to test multiline',
                            label: 'Choose a drink',
                            abbreviation: 'C',
                            native_name: 'Choose a drink',
                            price: 0
                        }
                    ],
                    price: 2.99,
                    name: '20 oz. Bottled Soda',
                    order_id: 'jQxRHeHt',
                    id: '6388b5d215af8c5c0cf0721b'
                }
            ],
            ticket_no: '8'
        }
    }),
    __metadata("design:type", GetOrderByIdObject)
], GetThirdPartyOrderByIdResponse.prototype, "data", void 0);
exports.GetThirdPartyOrderByIdResponse = GetThirdPartyOrderByIdResponse;
//# sourceMappingURL=kitchen-hub.entity.js.map