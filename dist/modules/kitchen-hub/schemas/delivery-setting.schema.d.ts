import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type DeliverySettingDocument = DeliverySettingField & Document;
export declare class DeliverySettingField {
    restaurantId: Types.ObjectId;
    storeId: string;
    isAutoAccept: boolean;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const DeliverySettingSchema: MongooseSchema<Document<DeliverySettingField, any, any>, import("mongoose").Model<Document<DeliverySettingField, any, any>, any, any, any>, any, any>;
