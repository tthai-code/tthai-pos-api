/// <reference types="mongoose" />
export declare type KitchenHubDocument = KitchenHubField & Document;
declare class OrderObjectField {
    id: number;
    externalId: string;
    number: string;
    dailyNumber: number;
    type: string;
    status: string;
    notes: string;
    paid: boolean;
    asap: boolean;
    addDisposableItems: boolean;
    prepTimeMinutes: number;
}
declare class ProviderField {
    id: string;
    name: string;
    merchantId: string;
    storeId: string;
}
declare class LocationField {
    id: string;
    partnerLocationId: string;
    name: string;
    street: string;
    city: string;
    state: string;
    zipCode: string;
    lat: number;
    lon: number;
}
declare class StoreField {
    id: string;
    partnerStoreId: string;
    name: string;
}
declare class CustomerField {
    name: string;
    email: string;
    phoneNumber: string;
    phoneCode: string;
}
declare class OptionObjectField {
    id: string;
    name: string;
    basePrice: string;
    quantity: number;
    price: string;
    modifierName: string;
}
declare class ItemObjectField {
    id: string;
    name: string;
    quantity: number;
    basePrice: string;
    price: string;
    instructions: string;
    options: OptionObjectField[];
}
declare class AddressObjectField {
    country: string;
    street: string;
    city: string;
    state: string;
    zipCode: string;
    unitNumber: string;
    latitude: number;
    longitude: number;
}
declare class CourierObjectField {
    name: string;
    photoUrl: string;
    phoneNumber: string;
    status: string;
}
declare class DeliveryObjectField {
    type: string;
    status: string;
    notes: string;
    address: AddressObjectField;
    courier: CourierObjectField;
}
declare class PaymentObjectField {
    method: string;
}
declare class ChargesObjectField {
    subtotal: string;
    tax: string;
    tips: string;
    discount: string;
    total: string;
}
declare class TimestampsObjectField {
    placedAt: string;
    createdAt: string;
    acceptedAt: string;
    cancelledAt: string;
    completedAt: string;
    pickupAt: string;
    deliveryAt: string;
    scheduledFor: string;
}
export declare class KitchenHubField {
    order: OrderObjectField;
    provider: ProviderField;
    location: LocationField;
    store: StoreField;
    customer: CustomerField;
    items: ItemObjectField[];
    delivery: DeliveryObjectField;
    payment: PaymentObjectField;
    charges: ChargesObjectField;
    timestamps: TimestampsObjectField;
    status: string;
}
export declare const KitchenHubSchema: import("mongoose").Schema<import("mongoose").Document<KitchenHubField, any, any>, import("mongoose").Model<import("mongoose").Document<KitchenHubField, any, any>, any, any, any>, any, any>;
export {};
