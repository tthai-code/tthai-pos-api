"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenHubSchema = exports.KitchenHubField = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoosePaginate = require("mongoose-paginate");
const status_enum_1 = require("../../../common/enum/status.enum");
const kitchen_order_status_enum_1 = require("../common/kitchen-order-status.enum");
const kitchen_order_type_enum_1 = require("../common/kitchen-order-type.enum");
let OrderObjectField = class OrderObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], OrderObjectField.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], OrderObjectField.prototype, "externalId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], OrderObjectField.prototype, "number", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], OrderObjectField.prototype, "dailyNumber", void 0);
__decorate([
    (0, mongoose_1.Prop)({ enum: Object.values(kitchen_order_type_enum_1.KitchenHubOrderTypeEnum) }),
    __metadata("design:type", String)
], OrderObjectField.prototype, "type", void 0);
__decorate([
    (0, mongoose_1.Prop)({ enum: Object.values(kitchen_order_status_enum_1.KitchenHubOrderStatusEnum) }),
    __metadata("design:type", String)
], OrderObjectField.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], OrderObjectField.prototype, "notes", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Boolean)
], OrderObjectField.prototype, "paid", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Boolean)
], OrderObjectField.prototype, "asap", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Boolean)
], OrderObjectField.prototype, "addDisposableItems", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], OrderObjectField.prototype, "prepTimeMinutes", void 0);
OrderObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], OrderObjectField);
let ProviderField = class ProviderField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ProviderField.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ProviderField.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ProviderField.prototype, "merchantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ProviderField.prototype, "storeId", void 0);
ProviderField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], ProviderField);
let LocationField = class LocationField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], LocationField.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], LocationField.prototype, "partnerLocationId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], LocationField.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], LocationField.prototype, "street", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], LocationField.prototype, "city", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], LocationField.prototype, "state", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], LocationField.prototype, "zipCode", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], LocationField.prototype, "lat", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], LocationField.prototype, "lon", void 0);
LocationField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], LocationField);
let StoreField = class StoreField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], StoreField.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], StoreField.prototype, "partnerStoreId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], StoreField.prototype, "name", void 0);
StoreField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], StoreField);
let CustomerField = class CustomerField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CustomerField.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CustomerField.prototype, "email", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CustomerField.prototype, "phoneNumber", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CustomerField.prototype, "phoneCode", void 0);
CustomerField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], CustomerField);
let OptionObjectField = class OptionObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], OptionObjectField.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], OptionObjectField.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], OptionObjectField.prototype, "basePrice", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], OptionObjectField.prototype, "quantity", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], OptionObjectField.prototype, "price", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], OptionObjectField.prototype, "modifierName", void 0);
OptionObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], OptionObjectField);
let ItemObjectField = class ItemObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ItemObjectField.prototype, "id", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ItemObjectField.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], ItemObjectField.prototype, "quantity", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ItemObjectField.prototype, "basePrice", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ItemObjectField.prototype, "price", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ItemObjectField.prototype, "instructions", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Array)
], ItemObjectField.prototype, "options", void 0);
ItemObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], ItemObjectField);
let AddressObjectField = class AddressObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], AddressObjectField.prototype, "country", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], AddressObjectField.prototype, "street", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], AddressObjectField.prototype, "city", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], AddressObjectField.prototype, "state", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], AddressObjectField.prototype, "zipCode", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], AddressObjectField.prototype, "unitNumber", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], AddressObjectField.prototype, "latitude", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Number)
], AddressObjectField.prototype, "longitude", void 0);
AddressObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], AddressObjectField);
let CourierObjectField = class CourierObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CourierObjectField.prototype, "name", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CourierObjectField.prototype, "photoUrl", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CourierObjectField.prototype, "phoneNumber", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CourierObjectField.prototype, "status", void 0);
CourierObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], CourierObjectField);
let DeliveryObjectField = class DeliveryObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], DeliveryObjectField.prototype, "type", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], DeliveryObjectField.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], DeliveryObjectField.prototype, "notes", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", AddressObjectField)
], DeliveryObjectField.prototype, "address", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", CourierObjectField)
], DeliveryObjectField.prototype, "courier", void 0);
DeliveryObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], DeliveryObjectField);
let PaymentObjectField = class PaymentObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], PaymentObjectField.prototype, "method", void 0);
PaymentObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], PaymentObjectField);
let ChargesObjectField = class ChargesObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ChargesObjectField.prototype, "subtotal", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ChargesObjectField.prototype, "tax", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ChargesObjectField.prototype, "tips", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ChargesObjectField.prototype, "discount", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], ChargesObjectField.prototype, "total", void 0);
ChargesObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], ChargesObjectField);
let TimestampsObjectField = class TimestampsObjectField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TimestampsObjectField.prototype, "placedAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TimestampsObjectField.prototype, "createdAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TimestampsObjectField.prototype, "acceptedAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TimestampsObjectField.prototype, "cancelledAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TimestampsObjectField.prototype, "completedAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TimestampsObjectField.prototype, "pickupAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TimestampsObjectField.prototype, "deliveryAt", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], TimestampsObjectField.prototype, "scheduledFor", void 0);
TimestampsObjectField = __decorate([
    (0, mongoose_1.Schema)({ _id: false, strict: true, timestamps: false })
], TimestampsObjectField);
let KitchenHubField = class KitchenHubField {
};
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", OrderObjectField)
], KitchenHubField.prototype, "order", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", ProviderField)
], KitchenHubField.prototype, "provider", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", LocationField)
], KitchenHubField.prototype, "location", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", StoreField)
], KitchenHubField.prototype, "store", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", CustomerField)
], KitchenHubField.prototype, "customer", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", Array)
], KitchenHubField.prototype, "items", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", DeliveryObjectField)
], KitchenHubField.prototype, "delivery", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", PaymentObjectField)
], KitchenHubField.prototype, "payment", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", ChargesObjectField)
], KitchenHubField.prototype, "charges", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", TimestampsObjectField)
], KitchenHubField.prototype, "timestamps", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], KitchenHubField.prototype, "status", void 0);
KitchenHubField = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'kitchenHub' })
], KitchenHubField);
exports.KitchenHubField = KitchenHubField;
exports.KitchenHubSchema = mongoose_1.SchemaFactory.createForClass(KitchenHubField);
exports.KitchenHubSchema.plugin(mongoosePaginate);
//# sourceMappingURL=kitchen-hub.schema.js.map