"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliverySettingSchema = exports.DeliverySettingField = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
let DeliverySettingField = class DeliverySettingField {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], DeliverySettingField.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], DeliverySettingField.prototype, "storeId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: true }),
    __metadata("design:type", Boolean)
], DeliverySettingField.prototype, "isAutoAccept", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], DeliverySettingField.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], DeliverySettingField.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], DeliverySettingField.prototype, "createdBy", void 0);
DeliverySettingField = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'deliverySettings' })
], DeliverySettingField);
exports.DeliverySettingField = DeliverySettingField;
exports.DeliverySettingSchema = mongoose_1.SchemaFactory.createForClass(DeliverySettingField);
exports.DeliverySettingSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=delivery-setting.schema.js.map