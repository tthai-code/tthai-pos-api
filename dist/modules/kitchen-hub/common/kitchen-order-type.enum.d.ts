export declare enum KitchenHubOrderTypeEnum {
    PICKUP = "pickup",
    DELIVERY = "delivery",
    DINE_IN = "dinein"
}
