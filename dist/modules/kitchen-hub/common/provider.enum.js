"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProviderEnum = void 0;
var ProviderEnum;
(function (ProviderEnum) {
    ProviderEnum["DOORDASH"] = "doordash";
    ProviderEnum["GRUBHUB"] = "grubhub";
    ProviderEnum["UBEREATS"] = "ubereats";
    ProviderEnum["GLORIAFOOD"] = "gloriafood";
})(ProviderEnum = exports.ProviderEnum || (exports.ProviderEnum = {}));
//# sourceMappingURL=provider.enum.js.map