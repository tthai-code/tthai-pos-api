"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenHubOrderTypeEnum = void 0;
var KitchenHubOrderTypeEnum;
(function (KitchenHubOrderTypeEnum) {
    KitchenHubOrderTypeEnum["PICKUP"] = "pickup";
    KitchenHubOrderTypeEnum["DELIVERY"] = "delivery";
    KitchenHubOrderTypeEnum["DINE_IN"] = "dinein";
})(KitchenHubOrderTypeEnum = exports.KitchenHubOrderTypeEnum || (exports.KitchenHubOrderTypeEnum = {}));
//# sourceMappingURL=kitchen-order-type.enum.js.map