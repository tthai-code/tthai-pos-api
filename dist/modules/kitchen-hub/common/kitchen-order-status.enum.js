"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenHubOrderStatusEnum = void 0;
var KitchenHubOrderStatusEnum;
(function (KitchenHubOrderStatusEnum) {
    KitchenHubOrderStatusEnum["NEW"] = "new";
    KitchenHubOrderStatusEnum["ACCEPTED"] = "accepted";
    KitchenHubOrderStatusEnum["COMPLETED"] = "completed";
    KitchenHubOrderStatusEnum["CANCELLED"] = "cancelled";
})(KitchenHubOrderStatusEnum = exports.KitchenHubOrderStatusEnum || (exports.KitchenHubOrderStatusEnum = {}));
//# sourceMappingURL=kitchen-order-status.enum.js.map