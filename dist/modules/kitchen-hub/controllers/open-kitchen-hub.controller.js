"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OpenKitchenHubController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const log_logic_1 = require("../../log/logic/log.logic");
const create_kitchen_hub_dto_1 = require("../dto/create-kitchen-hub.dto");
const kitchen_hub_logic_1 = require("../logic/kitchen-hub.logic");
const kitchen_hub_service_1 = require("../services/kitchen-hub.service");
let OpenKitchenHubController = class OpenKitchenHubController {
    constructor(kitchenHubService, kitchenHubLogic, logLogic) {
        this.kitchenHubService = kitchenHubService;
        this.kitchenHubLogic = kitchenHubLogic;
        this.logLogic = logLogic;
    }
    async receiveOrder(payload, req) {
        const created = await this.kitchenHubLogic.receiveOrderFromKH(payload);
        await this.logLogic.createLogic(req, 'Receive Kitchen Hub Order', {
            created
        });
        return { success: true };
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiOperation)({ summary: 'Receive Kitchen Hub Notify Order' }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_kitchen_hub_dto_1.CreateKitchenHubOrderDto, Object]),
    __metadata("design:returntype", Promise)
], OpenKitchenHubController.prototype, "receiveOrder", null);
OpenKitchenHubController = __decorate([
    (0, swagger_1.ApiHeaders)([{ name: 'x-api-key', required: true }]),
    (0, swagger_1.ApiTags)('open/kitchen-hub'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('api-key')),
    (0, common_1.Controller)('v1/open/kitchen-hub'),
    __metadata("design:paramtypes", [kitchen_hub_service_1.KitchenHubService,
        kitchen_hub_logic_1.KitchenHubLogic,
        log_logic_1.LogLogic])
], OpenKitchenHubController);
exports.OpenKitchenHubController = OpenKitchenHubController;
//# sourceMappingURL=open-kitchen-hub.controller.js.map