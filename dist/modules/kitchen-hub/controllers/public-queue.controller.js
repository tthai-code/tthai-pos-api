"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicQueueController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const item_status_enum_1 = require("../../order/enum/item-status.enum");
const kitchen_queue_service_1 = require("../../order/services/kitchen-queue.service");
const tthai_app_logic_1 = require("../../tthai-app/logics/tthai-app.logic");
class CheckQueueDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CheckQueueDto.prototype, "restaurantId", void 0);
__decorate([
    (0, class_validator_1.IsNumberString)(),
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], CheckQueueDto.prototype, "item", void 0);
let PublicQueueController = class PublicQueueController {
    constructor(kitchenQueueService, tthaiAppLogic) {
        this.kitchenQueueService = kitchenQueueService;
        this.tthaiAppLogic = tthaiAppLogic;
    }
    async checkPrepareTime(query) {
        const tthaiAppSetting = await this.tthaiAppLogic.getTThaiApp(query.restaurantId);
        const { waitTime } = tthaiAppSetting;
        const queue = await this.kitchenQueueService.getAll({
            restaurantId: query.restaurantId,
            queueStatus: item_status_enum_1.ItemStatusEnum.IN_PROGRESS
        });
        const length = queue.length || 0;
        let prepareTime = waitTime.baseTime +
            length * waitTime.perOrderTime +
            +query.item * waitTime.perItemTime;
        if (prepareTime > waitTime.maxTime) {
            prepareTime = waitTime.maxTime;
        }
        return { prepareTime, length, waitTime };
    }
};
__decorate([
    (0, common_1.Get)('queue'),
    (0, swagger_1.ApiOperation)({ summary: '', description: 'check queue prepare time' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [CheckQueueDto]),
    __metadata("design:returntype", Promise)
], PublicQueueController.prototype, "checkPrepareTime", null);
PublicQueueController = __decorate([
    (0, common_1.Controller)('v1/public'),
    __metadata("design:paramtypes", [kitchen_queue_service_1.KitchenQueueService,
        tthai_app_logic_1.TThaiAppLogic])
], PublicQueueController);
exports.PublicQueueController = PublicQueueController;
//# sourceMappingURL=public-queue.controller.js.map