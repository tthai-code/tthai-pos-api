"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSKitchenHubController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const log_logic_1 = require("../../log/logic/log.logic");
const get_third_party_order_dto_1 = require("../../order/dto/get-third-party-order.dto");
const order_state_enum_1 = require("../../order/enum/order-state.enum");
const third_party_order_service_1 = require("../../order/services/third-party-order.service");
const accept_order_dto_1 = require("../dto/accept-order.dto");
const kitchen_hub_entity_1 = require("../entity/kitchen-hub.entity");
const kitchen_hub_logic_1 = require("../logic/kitchen-hub.logic");
const pos_kitchen_hub_logic_1 = require("../logic/pos-kitchen-hub.logic");
let POSKitchenHubController = class POSKitchenHubController {
    constructor(kitchenHubLogic, thirdPartyOrderService, posKitchenHubLogic, logLogic) {
        this.kitchenHubLogic = kitchenHubLogic;
        this.thirdPartyOrderService = thirdPartyOrderService;
        this.posKitchenHubLogic = posKitchenHubLogic;
        this.logLogic = logLogic;
    }
    async getAllOrder(query, request) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const results = await this.posKitchenHubLogic.getAllOrder(restaurantId, query);
        await this.logLogic.createLogic(request, 'Get 3rd party order list', results);
        return results;
    }
    async getOrderById(orderId) {
        return await this.kitchenHubLogic.getOrderByIdForPOS(orderId);
    }
    async acceptOrder(payload) {
        return await this.kitchenHubLogic.acceptOrder(payload);
    }
    async cancelOrder(payload) {
        return await this.kitchenHubLogic.rejectOrder(payload.orderId);
    }
    async readyOrder(payload) {
        await this.thirdPartyOrderService.update(payload.orderId, {
            orderStatus: order_state_enum_1.OrderStateEnum.READY
        });
        return { success: true };
    }
    async upcomingOrder(payload) {
        await this.thirdPartyOrderService.update(payload.orderId, {
            orderStatus: order_state_enum_1.OrderStateEnum.UPCOMING
        });
        return { success: true };
    }
    async completeOrder(payload) {
        return await this.kitchenHubLogic.completeOrder(payload.orderId);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => kitchen_hub_entity_1.PaginateKitchenHubResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get 3rd party order list',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Query)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_third_party_order_dto_1.ThirdPartyOrderPaginateDto, Object]),
    __metadata("design:returntype", Promise)
], POSKitchenHubController.prototype, "getAllOrder", null);
__decorate([
    (0, common_1.Get)(':orderId'),
    (0, swagger_1.ApiOkResponse)({ type: () => kitchen_hub_entity_1.GetThirdPartyOrderByIdResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get 3rd party Order Detail by ID',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Param)('orderId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSKitchenHubController.prototype, "getOrderById", null);
__decorate([
    (0, common_1.Patch)('accept-order'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Accept 3rd party order/Mark in progress',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_dto_1.AcceptOrderDto]),
    __metadata("design:returntype", Promise)
], POSKitchenHubController.prototype, "acceptOrder", null);
__decorate([
    (0, common_1.Patch)('cancel-order'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Cancel 3rd party order',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_dto_1.AcceptOrderDto]),
    __metadata("design:returntype", Promise)
], POSKitchenHubController.prototype, "cancelOrder", null);
__decorate([
    (0, common_1.Patch)('ready-order'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Ready 3rd party order',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_dto_1.AcceptOrderDto]),
    __metadata("design:returntype", Promise)
], POSKitchenHubController.prototype, "readyOrder", null);
__decorate([
    (0, common_1.Patch)('upcoming-order'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Upcoming 3rd party order',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_dto_1.AcceptOrderDto]),
    __metadata("design:returntype", Promise)
], POSKitchenHubController.prototype, "upcomingOrder", null);
__decorate([
    (0, common_1.Patch)('complete-order'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Complete 3rd party order',
        description: 'use bearer `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [accept_order_dto_1.AcceptOrderDto]),
    __metadata("design:returntype", Promise)
], POSKitchenHubController.prototype, "completeOrder", null);
POSKitchenHubController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/kitchen-hub'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('/v1/pos/kitchen-hub'),
    __metadata("design:paramtypes", [kitchen_hub_logic_1.KitchenHubLogic,
        third_party_order_service_1.ThirdPartyOrderService,
        pos_kitchen_hub_logic_1.POSKitchenHubLogic,
        log_logic_1.LogLogic])
], POSKitchenHubController);
exports.POSKitchenHubController = POSKitchenHubController;
//# sourceMappingURL=pos-kitchen-hub.controller.js.map