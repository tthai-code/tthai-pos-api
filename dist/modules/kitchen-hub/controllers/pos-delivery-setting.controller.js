"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSDeliverySettingController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const success_entity_1 = require("../../../common/entity/success.entity");
const staff_auth_guard_1 = require("../../auth/guards/staff-auth.guard");
const log_logic_1 = require("../../log/logic/log.logic");
const errorResponse_1 = require("../../../utilities/errorResponse");
const update_auto_dto_1 = require("../dto/update-auto.dto");
const get_delivery_setting_entity_1 = require("../entity/get-delivery-setting.entity");
const delivery_setting_logic_1 = require("../logic/delivery-setting.logic");
const delivery_setting_service_1 = require("../services/delivery-setting.service");
let POSDeliverySettingController = class POSDeliverySettingController {
    constructor(deliverySettingService, deliverySettingLogic, logLogic) {
        this.deliverySettingService = deliverySettingService;
        this.deliverySettingLogic = deliverySettingLogic;
        this.logLogic = logLogic;
    }
    async getDeliverySetting(request) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const deliverySetting = await this.deliverySettingLogic.getDeliverySetting(restaurantId);
        await this.logLogic.createLogic(request, 'Get Delivery Setting (Auto Accept)', deliverySetting);
        return deliverySetting;
    }
    async updateAutoAccept(payload) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const updated = await this.deliverySettingService.updateOne({ restaurantId }, payload);
        if (!updated)
            throw new common_1.BadRequestException();
        return { success: true };
    }
    async pauseProvider(payload, request) {
        var _a, _b;
        const restaurantId = (_b = (_a = staff_auth_guard_1.StaffAuthGuard.getAuthorizedUser()) === null || _a === void 0 ? void 0 : _a.restaurant) === null || _b === void 0 ? void 0 : _b.id;
        const updated = await this.deliverySettingLogic.pauseProvider(restaurantId, payload);
        await this.logLogic.createLogic(request, 'Pause Provider', updated);
        return updated;
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ type: () => get_delivery_setting_entity_1.GetDeliverySettingResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Delivery Setting (Auto Accept)',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], POSDeliverySettingController.prototype, "getDeliverySetting", null);
__decorate([
    (0, common_1.Patch)('/auto'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Update Delivery Setting (Auto Accept)',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_auto_dto_1.UpdateAutoAcceptDto]),
    __metadata("design:returntype", Promise)
], POSDeliverySettingController.prototype, "updateAutoAccept", null);
__decorate([
    (0, common_1.Patch)('/pause'),
    (0, swagger_1.ApiOkResponse)({ type: () => success_entity_1.SuccessObjectResponse }),
    (0, swagger_1.ApiBadRequestResponse)((0, errorResponse_1.errorResponse)(400, 'provider_not_found', '2022-11-17T11:03:25.372Z')),
    (0, swagger_1.ApiOperation)({
        summary: 'Pause Provider',
        description: 'use *bearer* `staff_token`'
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_auto_dto_1.PauseProviderDto, Object]),
    __metadata("design:returntype", Promise)
], POSDeliverySettingController.prototype, "pauseProvider", null);
POSDeliverySettingController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('pos/delivery-setting'),
    (0, common_1.UseGuards)(staff_auth_guard_1.StaffAuthGuard),
    (0, common_1.Controller)('v1/pos/delivery-setting'),
    __metadata("design:paramtypes", [delivery_setting_service_1.DeliverySettingService,
        delivery_setting_logic_1.DeliverySettingLogic,
        log_logic_1.LogLogic])
], POSDeliverySettingController);
exports.POSDeliverySettingController = POSDeliverySettingController;
//# sourceMappingURL=pos-delivery-setting.controller.js.map