import { LogLogic } from 'src/modules/log/logic/log.logic';
import { CreateKitchenHubOrderDto } from '../dto/create-kitchen-hub.dto';
import { KitchenHubLogic } from '../logic/kitchen-hub.logic';
import { KitchenHubService } from '../services/kitchen-hub.service';
export declare class OpenKitchenHubController {
    private readonly kitchenHubService;
    private readonly kitchenHubLogic;
    private readonly logLogic;
    constructor(kitchenHubService: KitchenHubService, kitchenHubLogic: KitchenHubLogic, logLogic: LogLogic);
    receiveOrder(payload: CreateKitchenHubOrderDto, req: any): Promise<{
        success: boolean;
    }>;
}
