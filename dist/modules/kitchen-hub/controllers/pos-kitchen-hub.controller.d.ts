/// <reference types="mongoose" />
import { LogLogic } from 'src/modules/log/logic/log.logic';
import { ThirdPartyOrderPaginateDto } from 'src/modules/order/dto/get-third-party-order.dto';
import { ThirdPartyOrderService } from 'src/modules/order/services/third-party-order.service';
import { AcceptOrderDto } from '../dto/accept-order.dto';
import { KitchenHubLogic } from '../logic/kitchen-hub.logic';
import { POSKitchenHubLogic } from '../logic/pos-kitchen-hub.logic';
export declare class POSKitchenHubController {
    private readonly kitchenHubLogic;
    private readonly thirdPartyOrderService;
    private readonly posKitchenHubLogic;
    private readonly logLogic;
    constructor(kitchenHubLogic: KitchenHubLogic, thirdPartyOrderService: ThirdPartyOrderService, posKitchenHubLogic: POSKitchenHubLogic, logLogic: LogLogic);
    getAllOrder(query: ThirdPartyOrderPaginateDto, request: any): Promise<any>;
    getOrderById(orderId: string): Promise<{
        items: any[];
        customer: {
            id: string;
            firstName: string;
            lastName: string;
            tel: string;
        };
        ticketNo: string;
        status: string;
        _id: any;
        id?: any;
        __v?: any;
        note: string;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        tax: number;
        alcoholTax: number;
        restaurant: import("../../order/schemas/order.schema").RestaurantFields;
        staff: import("../../order/schemas/third-party-order.schema").StaffThirdPartyFields;
        total: number;
        convenienceFee: number;
        serviceCharge: number;
        subtotal: number;
        discount: number;
        numberOfGuest: number;
        pickUpDate: Date;
        orderMethod: string;
        orderType: string;
        orderDate: Date;
        summaryItems: import("mongoose").LeanDocument<import("../../order/schemas/third-party-order.schema").ItemThirdPartyFields>[];
        orderStatus: string;
        tips: number;
        deliverySource: string;
        completedDate: Date;
        asap: boolean;
        scheduledForDate: Date;
        orderKHId: number;
        prepareTime: number;
    }>;
    acceptOrder(payload: AcceptOrderDto): Promise<{
        success: boolean;
    }>;
    cancelOrder(payload: AcceptOrderDto): Promise<{
        success: boolean;
    }>;
    readyOrder(payload: AcceptOrderDto): Promise<{
        success: boolean;
    }>;
    upcomingOrder(payload: AcceptOrderDto): Promise<{
        success: boolean;
    }>;
    completeOrder(payload: AcceptOrderDto): Promise<{
        success: boolean;
    }>;
}
