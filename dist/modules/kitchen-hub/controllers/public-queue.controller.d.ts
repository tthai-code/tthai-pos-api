import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service';
import { TThaiAppLogic } from 'src/modules/tthai-app/logics/tthai-app.logic';
declare class CheckQueueDto {
    restaurantId: string;
    item: number;
}
export declare class PublicQueueController {
    private readonly kitchenQueueService;
    private readonly tthaiAppLogic;
    constructor(kitchenQueueService: KitchenQueueService, tthaiAppLogic: TThaiAppLogic);
    checkPrepareTime(query: CheckQueueDto): Promise<{
        prepareTime: number;
        length: number;
        waitTime: import("../../tthai-app/schemas/tthai-app.schema").WaitTimeFields;
    }>;
}
export {};
