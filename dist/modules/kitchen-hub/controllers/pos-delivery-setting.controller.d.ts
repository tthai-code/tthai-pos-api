/// <reference types="mongoose" />
import { LogLogic } from 'src/modules/log/logic/log.logic';
import { PauseProviderDto, UpdateAutoAcceptDto } from '../dto/update-auto.dto';
import { DeliverySettingLogic } from '../logic/delivery-setting.logic';
import { DeliverySettingService } from '../services/delivery-setting.service';
export declare class POSDeliverySettingController {
    private readonly deliverySettingService;
    private readonly deliverySettingLogic;
    private readonly logLogic;
    constructor(deliverySettingService: DeliverySettingService, deliverySettingLogic: DeliverySettingLogic, logLogic: LogLogic);
    getDeliverySetting(request: any): Promise<{
        onlineCount: any;
        providers: any[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        restaurantId: import("mongoose").Types.ObjectId;
        storeId: string;
        isAutoAccept: boolean;
    }>;
    updateAutoAccept(payload: UpdateAutoAcceptDto): Promise<{
        success: boolean;
    }>;
    pauseProvider(payload: PauseProviderDto, request: any): Promise<{
        success: boolean;
    }>;
}
