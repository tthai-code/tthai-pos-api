/// <reference types="mongoose" />
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { PauseProviderDto } from '../dto/update-auto.dto';
import { DeliverySettingService } from '../services/delivery-setting.service';
import { KitchenHubHttpService } from '../services/kitchen-hub-http.service';
export declare class DeliverySettingLogic {
    private readonly deliverySettingService;
    private readonly httpService;
    private readonly restaurantService;
    constructor(deliverySettingService: DeliverySettingService, httpService: KitchenHubHttpService, restaurantService: RestaurantService);
    getDeliverySetting(restaurantId: string): Promise<{
        onlineCount: any;
        providers: any[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        restaurantId: import("mongoose").Types.ObjectId;
        storeId: string;
        isAutoAccept: boolean;
    }>;
    pauseProvider(restaurantId: string, pauseProvider: PauseProviderDto): Promise<{
        success: boolean;
    }>;
}
