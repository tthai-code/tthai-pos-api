"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSKitchenHubLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const get_third_party_order_dto_1 = require("../../order/dto/get-third-party-order.dto");
const order_state_enum_1 = require("../../order/enum/order-state.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const kitchen_queue_service_1 = require("../../order/services/kitchen-queue.service");
const third_party_order_service_1 = require("../../order/services/third-party-order.service");
const provider_enum_1 = require("../common/provider.enum");
let POSKitchenHubLogic = class POSKitchenHubLogic {
    constructor(thirdPartyOrderService, kitchenQueueService) {
        this.thirdPartyOrderService = thirdPartyOrderService;
        this.kitchenQueueService = kitchenQueueService;
    }
    async getAllOrder(restaurantId, query) {
        const allOrder = await this.thirdPartyOrderService.getAll(query.buildQueryGetAll(restaurantId));
        const orders = await this.thirdPartyOrderService.paginate(query.buildQuery(restaurantId), query, { createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0, status: 0 });
        const { docs } = orders;
        const orderIds = docs.map((item) => item.id);
        const kitchenQueues = await this.kitchenQueueService.getAll({
            orderId: { $in: orderIds },
            status: status_enum_1.StatusEnum.ACTIVE
        });
        const transformDocs = docs.map((item) => {
            var _a, _b;
            const index = kitchenQueues.findIndex((queue) => queue.orderId === item.id);
            if (index > -1) {
                return Object.assign(Object.assign({}, item.toObject()), { ticketNo: ((_a = kitchenQueues[index]) === null || _a === void 0 ? void 0 : _a.ticketNo) || null });
            }
            return Object.assign(Object.assign({}, item.toObject()), { ticketNo: ((_b = kitchenQueues[index]) === null || _b === void 0 ? void 0 : _b.ticketNo) || null });
        });
        const results = Object.assign(Object.assign({}, orders), { docs: transformDocs, status: {
                active: allOrder.filter((item) => item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                    item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED).length,
                new: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.NEW)
                    .length,
                inProgress: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS).length,
                ready: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.READY).length,
                upcoming: allOrder.filter((item) => !item.asap).length,
                completed: allOrder.filter((item) => item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED).length
            }, type: {
                delivery: allOrder.filter((item) => {
                    const deliveryCondition = item.orderMethod === order_type_enum_1.OrderMethodEnum.DELIVERY;
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                        return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                            item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED &&
                            deliveryCondition);
                    }
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                        return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW && deliveryCondition;
                    }
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                        return (item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS &&
                            deliveryCondition);
                    }
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                        return (item.orderStatus === order_state_enum_1.OrderStateEnum.READY && deliveryCondition);
                    }
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                        return (item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED && deliveryCondition);
                    }
                }).length,
                pickup: allOrder.filter((item) => {
                    const pickUpCondition = item.orderMethod === order_type_enum_1.OrderMethodEnum.PICKUP;
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                        return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                            item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED &&
                            pickUpCondition);
                    }
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                        return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW && pickUpCondition;
                    }
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                        return (item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS && pickUpCondition);
                    }
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                        return item.orderStatus === order_state_enum_1.OrderStateEnum.READY && pickUpCondition;
                    }
                    if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                        return (item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED && pickUpCondition);
                    }
                }).length
            }, source: {
                doordash: allOrder.filter((item) => {
                    if (item.deliverySource !== provider_enum_1.ProviderEnum.DOORDASH) {
                        return false;
                    }
                    if (item.orderMethod === order_type_enum_1.OrderMethodEnum.DELIVERY &&
                        query.type === order_type_enum_1.OrderMethodEnum.DELIVERY) {
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                            return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                                item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED);
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.READY;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED;
                        }
                    }
                    if (item.orderMethod === order_type_enum_1.OrderMethodEnum.PICKUP &&
                        query.type === order_type_enum_1.OrderMethodEnum.PICKUP) {
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                            return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                                item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED);
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.READY;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED;
                        }
                    }
                }).length,
                gloriafood: allOrder.filter((item) => {
                    if (item.deliverySource !== provider_enum_1.ProviderEnum.GLORIAFOOD) {
                        return false;
                    }
                    if (item.orderMethod === order_type_enum_1.OrderMethodEnum.DELIVERY &&
                        query.type === order_type_enum_1.OrderMethodEnum.DELIVERY) {
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                            return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                                item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED);
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.READY;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED;
                        }
                    }
                    if (item.orderMethod === order_type_enum_1.OrderMethodEnum.PICKUP &&
                        query.type === order_type_enum_1.OrderMethodEnum.PICKUP) {
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                            return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                                item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED);
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.READY;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED;
                        }
                    }
                }).length,
                grubhub: allOrder.filter((item) => {
                    if (item.deliverySource !== provider_enum_1.ProviderEnum.GRUBHUB) {
                        return false;
                    }
                    if (item.orderMethod === order_type_enum_1.OrderMethodEnum.DELIVERY &&
                        query.type === order_type_enum_1.OrderMethodEnum.DELIVERY) {
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                            return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                                item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED);
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.READY;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED;
                        }
                    }
                    if (item.orderMethod === order_type_enum_1.OrderMethodEnum.PICKUP &&
                        query.type === order_type_enum_1.OrderMethodEnum.PICKUP) {
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                            return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                                item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED);
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.READY;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED;
                        }
                    }
                }).length,
                ubereats: allOrder.filter((item) => {
                    if (item.deliverySource !== provider_enum_1.ProviderEnum.UBEREATS) {
                        return false;
                    }
                    if (item.orderMethod === order_type_enum_1.OrderMethodEnum.DELIVERY &&
                        query.type === order_type_enum_1.OrderMethodEnum.DELIVERY) {
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                            return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                                item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED);
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.READY;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED;
                        }
                    }
                    if (item.orderMethod === order_type_enum_1.OrderMethodEnum.PICKUP &&
                        query.type === order_type_enum_1.OrderMethodEnum.PICKUP) {
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.ACTIVE) {
                            return (item.orderStatus !== order_state_enum_1.OrderStateEnum.COMPLETED &&
                                item.orderStatus !== order_state_enum_1.OrderStateEnum.CANCELED);
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.NEW) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.NEW;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.IN_PROGRESS) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.IN_PROGRESS;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.READY) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.READY;
                        }
                        if (query.status === order_state_enum_1.ThirdPartyOrderStatusEnum.COMPLETED) {
                            return item.orderStatus === order_state_enum_1.OrderStateEnum.COMPLETED;
                        }
                    }
                }).length
            } });
        return results;
    }
};
POSKitchenHubLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [third_party_order_service_1.ThirdPartyOrderService,
        kitchen_queue_service_1.KitchenQueueService])
], POSKitchenHubLogic);
exports.POSKitchenHubLogic = POSKitchenHubLogic;
//# sourceMappingURL=pos-kitchen-hub.logic.js.map