import { ThirdPartyOrderPaginateDto } from 'src/modules/order/dto/get-third-party-order.dto';
import { KitchenQueueService } from 'src/modules/order/services/kitchen-queue.service';
import { ThirdPartyOrderService } from 'src/modules/order/services/third-party-order.service';
export declare class POSKitchenHubLogic {
    private readonly thirdPartyOrderService;
    private readonly kitchenQueueService;
    constructor(thirdPartyOrderService: ThirdPartyOrderService, kitchenQueueService: KitchenQueueService);
    getAllOrder(restaurantId: string, query: ThirdPartyOrderPaginateDto): Promise<any>;
}
