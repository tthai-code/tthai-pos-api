"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KitchenHubLogic = void 0;
const common_1 = require("@nestjs/common");
const dayjs_1 = require("../../../plugins/dayjs");
const status_enum_1 = require("../../../common/enum/status.enum");
const mongoose_transaction_1 = require("../../database/mongoose.transaction");
const item_status_enum_1 = require("../../order/enum/item-status.enum");
const order_state_enum_1 = require("../../order/enum/order-state.enum");
const order_type_enum_1 = require("../../order/enum/order-type.enum");
const third_party_order_interfaces_1 = require("../../order/interfaces/third-party-order.interfaces");
const kitchen_counter_service_1 = require("../../order/services/kitchen-counter.service");
const kitchen_queue_service_1 = require("../../order/services/kitchen-queue.service");
const order_items_service_1 = require("../../order/services/order-items.service");
const order_service_1 = require("../../order/services/order.service");
const third_party_order_service_1 = require("../../order/services/third-party-order.service");
const restaurant_schema_1 = require("../../restaurant/schemas/restaurant.schema");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const socket_gateway_1 = require("../../socket/socket.gateway");
const transaction_logic_1 = require("../../transaction/logics/transaction.logic");
const tthai_app_logic_1 = require("../../tthai-app/logics/tthai-app.logic");
const kitchen_order_status_enum_1 = require("../common/kitchen-order-status.enum");
const delivery_setting_service_1 = require("../services/delivery-setting.service");
const kitchen_hub_http_service_1 = require("../services/kitchen-hub-http.service");
const kitchen_hub_service_1 = require("../services/kitchen-hub.service");
let KitchenHubLogic = class KitchenHubLogic {
    constructor(kitchenHubService, kitchenHubHttpService, restaurantService, transactionLogic, socketGateway, deliverySettingService, orderService, thirdPartyOrderService, kitchenQueueService, kitchenCounterService, orderItemsService, tthaiAppLogic) {
        this.kitchenHubService = kitchenHubService;
        this.kitchenHubHttpService = kitchenHubHttpService;
        this.restaurantService = restaurantService;
        this.transactionLogic = transactionLogic;
        this.socketGateway = socketGateway;
        this.deliverySettingService = deliverySettingService;
        this.orderService = orderService;
        this.thirdPartyOrderService = thirdPartyOrderService;
        this.kitchenQueueService = kitchenQueueService;
        this.kitchenCounterService = kitchenCounterService;
        this.orderItemsService = orderItemsService;
        this.tthaiAppLogic = tthaiAppLogic;
    }
    randomOrderId() {
        const message = 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789';
        let id = '';
        for (let i = 0; i < 8; i++) {
            id += message.charAt(Math.floor(Math.random() * message.length));
        }
        return id;
    }
    async genOrderId() {
        let id = '';
        while (true) {
            id = this.randomOrderId();
            const isDuplicated = await this.orderService.findOne({
                _id: id
            });
            if (!isDuplicated) {
                break;
            }
        }
        return id;
    }
    async queueKitchen(restaurantId) {
        const { timeZone } = await this.restaurantService.findOne({
            _id: restaurantId
        });
        const now = (0, dayjs_1.default)().tz(timeZone);
        const nowUnix = now.valueOf();
        const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf();
        const endOfDay = now
            .add(1, 'day')
            .hour(3)
            .minute(59)
            .millisecond(999)
            .valueOf();
        let nowFormat = now.subtract(1, 'day').format('YYYYMMDD');
        if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
            nowFormat = now.format('YYYYMMDD');
        }
        const countQueue = await this.kitchenCounterService.getCounter(nowFormat, restaurantId);
        return `${countQueue}`;
    }
    mappingOrderMethod(method) {
        switch (method) {
            case 'pickup':
                return order_type_enum_1.OrderMethodEnum.PICKUP;
            case 'delivery':
                return order_type_enum_1.OrderMethodEnum.DELIVERY;
            default:
                return order_type_enum_1.OrderMethodEnum.DINE_IN;
        }
    }
    async mappingKHOrderToOrder(restaurant, payload) {
        const { order, customer, items, charges, timestamps, provider } = payload;
        const orderPayload = {};
        orderPayload.orderKHId = order.id;
        orderPayload.restaurant = {
            id: restaurant.id,
            legalBusinessName: restaurant.legalBusinessName,
            address: restaurant.address,
            businessPhone: restaurant.businessPhone,
            email: restaurant.email
        };
        orderPayload._id = await this.genOrderId();
        const customerName = customer.name.split(' ');
        orderPayload.customer = {
            firstName: customerName[0],
            lastName: customerName[customerName.length - 1],
            tel: customer.phoneNumber
        };
        orderPayload.orderType = order_type_enum_1.OrderTypeEnum.THIRD_PARTY;
        orderPayload.orderMethod = this.mappingOrderMethod(order.type);
        orderPayload.summaryItems = items.map((item) => {
            var _a;
            const result = {};
            result.id = item === null || item === void 0 ? void 0 : item.id;
            result.name = item === null || item === void 0 ? void 0 : item.name;
            result.unit = item === null || item === void 0 ? void 0 : item.quantity;
            result.price = Number(item === null || item === void 0 ? void 0 : item.basePrice);
            result.amount = Number(item === null || item === void 0 ? void 0 : item.price);
            result.modifiers = (_a = item === null || item === void 0 ? void 0 : item.options) === null || _a === void 0 ? void 0 : _a.map((option) => ({
                id: option === null || option === void 0 ? void 0 : option.id,
                name: option === null || option === void 0 ? void 0 : option.name,
                label: option === null || option === void 0 ? void 0 : option.modifierName,
                abbreviation: (option === null || option === void 0 ? void 0 : option.modifierName) ? option === null || option === void 0 ? void 0 : option.modifierName[0] : null,
                nativeName: option === null || option === void 0 ? void 0 : option.name,
                price: Number(option === null || option === void 0 ? void 0 : option.price)
            }));
            result.note = item === null || item === void 0 ? void 0 : item.instructions;
            return result;
        });
        orderPayload.subtotal = (charges === null || charges === void 0 ? void 0 : charges.subtotal) || 0;
        orderPayload.discount = (charges === null || charges === void 0 ? void 0 : charges.discount) || 0;
        orderPayload.tax = (charges === null || charges === void 0 ? void 0 : charges.tax) || 0;
        orderPayload.tips = (charges === null || charges === void 0 ? void 0 : charges.tips) || 0;
        orderPayload.total = (charges === null || charges === void 0 ? void 0 : charges.total) - ((charges === null || charges === void 0 ? void 0 : charges.tips) || 0);
        orderPayload.asap = order.asap;
        orderPayload.orderDate = timestamps.placedAt;
        orderPayload.pickUpDate = timestamps === null || timestamps === void 0 ? void 0 : timestamps.pickupAt;
        orderPayload.scheduledForDate = timestamps === null || timestamps === void 0 ? void 0 : timestamps.scheduledFor;
        orderPayload.deliverySource = provider.id;
        return orderPayload;
    }
    async calculatePrepareTime(restaurantId, itemLength) {
        const tthaiAppSetting = await this.tthaiAppLogic.getTThaiApp(restaurantId);
        const { waitTime } = tthaiAppSetting;
        const queue = await this.kitchenQueueService.getAll({
            restaurantId,
            queueStatus: item_status_enum_1.ItemStatusEnum.IN_PROGRESS
        });
        const length = queue.length || 0;
        let prepareTime = waitTime.baseTime +
            length * waitTime.perOrderTime +
            itemLength * waitTime.perItemTime;
        if (prepareTime > waitTime.maxTime) {
            prepareTime = waitTime.maxTime;
        }
        return prepareTime;
    }
    async handleNewOrderFromKH(payload, isAutoAccept, restaurant, session) {
        const { order, items } = payload;
        const orderPayload = await this.mappingKHOrderToOrder(restaurant, payload);
        if (isAutoAccept) {
            const prepareTime = await this.calculatePrepareTime(restaurant.id, items.length);
            orderPayload.orderStatus = order_state_enum_1.OrderStateEnum.IN_PROGRESS;
            orderPayload.prepareTime = prepareTime;
            const created = await this.thirdPartyOrderService.transactionCreate(orderPayload, session);
            const { data } = await this.kitchenHubHttpService.acceptOrder(order.id.toString(), prepareTime);
            if (!data.ok)
                throw new common_1.BadRequestException(data.error_code);
            const itemPayload = created.summaryItems.map((item) => {
                const transform = Object.assign(Object.assign({}, item), { orderId: created.id });
                return transform;
            });
            const itemsData = await this.orderItemsService.transactionCreateMany(itemPayload, session);
            const ticketNo = await this.queueKitchen(created.restaurant.id);
            await this.kitchenQueueService.transactionCreate({
                restaurantId: created.restaurant.id,
                orderId: created.id,
                ticketNo,
                items: itemsData.map((item) => item.id)
            }, session);
            await this.transactionLogic.createThirdPartyOrderTransaction(created, session);
            const notifyObject = {
                id: created.id,
                kitchenHubId: created.orderKHId,
                customer: created.customer,
                type: created.orderMethod,
                placedDate: created.orderDate,
                source: created.deliverySource,
                orderStatus: created.orderStatus
            };
            this.socketGateway.server
                .to(`pos-${restaurant.id}`)
                .emit('receiveThirdPartyOrder', notifyObject);
            return created;
        }
        const created = await this.thirdPartyOrderService.transactionCreate(orderPayload, session);
        const notifyObject = {
            id: created.id,
            kitchenHubId: created.orderKHId,
            customer: created.customer,
            type: created.orderMethod,
            placedDate: created.orderDate,
            source: created.deliverySource,
            orderStatus: created.orderStatus
        };
        this.socketGateway.server
            .to(`pos-${restaurant.id}`)
            .emit('receiveThirdPartyOrder', notifyObject);
        return created;
    }
    async receiveOrderFromKH(payload) {
        const logic = async (session) => {
            const created = await this.kitchenHubService.transactionCreate(payload, session);
            const { order, store } = created;
            const deliveryConfig = await this.deliverySettingService.findOne({
                storeId: store.id
            });
            if (!(deliveryConfig === null || deliveryConfig === void 0 ? void 0 : deliveryConfig.storeId)) {
                throw new common_1.BadRequestException("This store ID wasn't in database.");
            }
            const restaurant = await this.restaurantService.findOne({
                _id: deliveryConfig.restaurantId,
                status: status_enum_1.StatusEnum.ACTIVE
            });
            if (!restaurant) {
                throw new common_1.NotFoundException('Not found restaurant.');
            }
            if (order.status === kitchen_order_status_enum_1.KitchenHubOrderStatusEnum.NEW) {
                await this.handleNewOrderFromKH(payload, deliveryConfig.isAutoAccept, restaurant, session);
            }
            return created;
        };
        return (0, mongoose_transaction_1.runTransaction)(logic);
    }
    async acceptOrder(payload) {
        const { orderId, preparationTime } = payload;
        const order = await this.thirdPartyOrderService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        if (order.orderStatus !== order_state_enum_1.OrderStateEnum.NEW) {
            throw new common_1.BadRequestException(`Order status isn't new order, now order status is ${order.orderStatus}`);
        }
        const prepareTime = preparationTime;
        const { data } = await this.kitchenHubHttpService.acceptOrder(order.orderKHId.toString(), prepareTime);
        if (!data.ok) {
            if (data.error_code === 'order_expired') {
                await this.thirdPartyOrderService.update(orderId, {
                    orderStatus: order_state_enum_1.OrderStateEnum.CANCELED
                });
            }
            throw new common_1.BadRequestException(data.error_code);
        }
        const logic = async (session) => {
            const updated = await this.thirdPartyOrderService.transactionUpdate(orderId, {
                orderStatus: order_state_enum_1.OrderStateEnum.IN_PROGRESS,
                prepareTime
            }, session);
            if (!updated)
                throw new common_1.BadRequestException();
            const itemPayload = updated.summaryItems.map((item) => {
                const transform = Object.assign(Object.assign({}, item), { orderId });
                return transform;
            });
            const itemsData = await this.orderItemsService.transactionCreateMany(itemPayload, session);
            const ticketNo = await this.queueKitchen(updated.restaurant.id);
            await this.kitchenQueueService.transactionCreate({
                restaurantId: updated.restaurant.id,
                orderId: orderId,
                ticketNo,
                items: itemsData.map((item) => item.id)
            }, session);
            await this.transactionLogic.createThirdPartyOrderTransaction(updated, session);
        };
        await (0, mongoose_transaction_1.runTransaction)(logic);
        return { success: true };
    }
    async rejectOrder(orderId) {
        const order = await this.thirdPartyOrderService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        const { data } = await this.kitchenHubHttpService.cancelOrder(order.orderKHId.toString());
        if (!data.ok)
            throw new common_1.BadRequestException(data.error_code);
        await this.thirdPartyOrderService.update(orderId, {
            orderStatus: order_state_enum_1.OrderStateEnum.CANCELED
        });
        await this.kitchenQueueService.updateOne({ orderId }, {
            queueStatus: item_status_enum_1.ItemStatusEnum.CANCELED
        });
        return { success: true };
    }
    async completeOrder(orderId) {
        const order = await this.thirdPartyOrderService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        const { data } = await this.kitchenHubHttpService.completeOrder(order.orderKHId.toString());
        if (!data.ok)
            throw new common_1.BadRequestException(data.error_code);
        await this.thirdPartyOrderService.update(orderId, {
            orderStatus: order_state_enum_1.OrderStateEnum.COMPLETED
        });
        await this.kitchenQueueService.updateOne({ orderId }, {
            queueStatus: item_status_enum_1.ItemStatusEnum.COMPLETED
        });
        await this.orderItemsService.updateMany({ orderId }, { itemStatus: item_status_enum_1.ItemStatusEnum.COMPLETED });
        return { success: true };
    }
    async getOrderByIdForPOS(orderId) {
        var _a, _b, _c, _d, _e;
        const order = await this.thirdPartyOrderService.findOne({
            _id: orderId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            createdAt: 0,
            updatedAt: 0,
            status: 0,
            createdBy: 0,
            updatedBy: 0
        });
        if (!order)
            throw new common_1.NotFoundException('Not found order.');
        const items = await this.orderItemsService.getAll({ orderId: order.id });
        const kitchenQueue = await this.kitchenQueueService.findOne({ orderId });
        const result = Object.assign(Object.assign({}, order.toObject()), { items: items.length > 0 ? items : order.summaryItems, customer: {
                id: ((_a = order.customer) === null || _a === void 0 ? void 0 : _a.id) || null,
                firstName: (_b = order.customer) === null || _b === void 0 ? void 0 : _b.firstName,
                lastName: (_c = order.customer) === null || _c === void 0 ? void 0 : _c.lastName,
                tel: (_d = order.customer) === null || _d === void 0 ? void 0 : _d.tel
            }, ticketNo: (_e = kitchenQueue === null || kitchenQueue === void 0 ? void 0 : kitchenQueue.ticketNo) !== null && _e !== void 0 ? _e : null });
        delete result.summaryItems;
        return result;
    }
};
KitchenHubLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [kitchen_hub_service_1.KitchenHubService,
        kitchen_hub_http_service_1.KitchenHubHttpService,
        restaurant_service_1.RestaurantService,
        transaction_logic_1.TransactionLogic,
        socket_gateway_1.SocketGateway,
        delivery_setting_service_1.DeliverySettingService,
        order_service_1.OrderService,
        third_party_order_service_1.ThirdPartyOrderService,
        kitchen_queue_service_1.KitchenQueueService,
        kitchen_counter_service_1.KitchenCounterService,
        order_items_service_1.OrderItemsService,
        tthai_app_logic_1.TThaiAppLogic])
], KitchenHubLogic);
exports.KitchenHubLogic = KitchenHubLogic;
//# sourceMappingURL=kitchen-hub.logic.js.map