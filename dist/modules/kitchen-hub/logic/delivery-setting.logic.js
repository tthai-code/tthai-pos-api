"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliverySettingLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const provider_enum_1 = require("../common/provider.enum");
const delivery_setting_service_1 = require("../services/delivery-setting.service");
const kitchen_hub_http_service_1 = require("../services/kitchen-hub-http.service");
let DeliverySettingLogic = class DeliverySettingLogic {
    constructor(deliverySettingService, httpService, restaurantService) {
        this.deliverySettingService = deliverySettingService;
        this.httpService = httpService;
        this.restaurantService = restaurantService;
    }
    async getDeliverySetting(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        let deliverySetting = await this.deliverySettingService.findOne({
            restaurantId
        }, { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 });
        if (!deliverySetting) {
            const { data } = await this.httpService.createStore(restaurant.dba);
            if (!(data === null || data === void 0 ? void 0 : data.ok))
                throw new common_1.BadRequestException(data === null || data === void 0 ? void 0 : data.error_code);
            await this.deliverySettingService.create({
                restaurantId,
                storeId: data.result.id
            });
            deliverySetting = await this.deliverySettingService.findOne({
                restaurantId
            }, { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 });
        }
        const { data: providers } = await this.httpService.getStoreProviders(deliverySetting.storeId);
        if (!providers.ok)
            throw new common_1.BadRequestException(providers === null || providers === void 0 ? void 0 : providers.error_code);
        const activeProviders = providers.result
            .filter((item) => item.active)
            .map((item) => item.provider_id);
        const providerStatus = await Promise.all(activeProviders.map((item) => this.httpService.getProviderStatus(item, deliverySetting.storeId)));
        const providerResult = providerStatus.map((item) => {
            const { data } = item;
            return data.result;
        });
        const mapProviders = activeProviders.map((item, index) => ({
            provider: item,
            isConnected: true,
            online: providerResult[index].online,
            canPause: item !== provider_enum_1.ProviderEnum.GLORIAFOOD
        }));
        const allProviderStatus = Object.values(provider_enum_1.ProviderEnum).map((provider) => {
            const filter = mapProviders.filter((e) => e.provider === provider);
            if (filter.length > 0)
                return filter[0];
            return {
                provider,
                isConnected: false,
                online: false,
                canPause: provider !== provider_enum_1.ProviderEnum.GLORIAFOOD
            };
        });
        const transform = deliverySetting.toObject();
        const result = Object.assign(Object.assign({}, transform), { onlineCount: mapProviders.filter((item) => item.online).length, providers: allProviderStatus });
        delete result.storeId;
        return result;
    }
    async pauseProvider(restaurantId, pauseProvider) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const deliverySetting = await this.deliverySettingService.findOne({
            restaurantId
        }, { isAutoAccept: 1, restaurantId: 1, storeId: 1, _id: 0 });
        const { storeId } = deliverySetting;
        const payload = {
            online: pauseProvider.online,
            pause_duration_seconds: null,
            provider_id: pauseProvider.provider,
            store_id: storeId
        };
        const { data } = await this.httpService.setProviderStatus(payload);
        if (!data.ok)
            throw new common_1.BadRequestException(data.error_code);
        return { success: true };
    }
};
DeliverySettingLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [delivery_setting_service_1.DeliverySettingService,
        kitchen_hub_http_service_1.KitchenHubHttpService,
        restaurant_service_1.RestaurantService])
], DeliverySettingLogic);
exports.DeliverySettingLogic = DeliverySettingLogic;
//# sourceMappingURL=delivery-setting.logic.js.map