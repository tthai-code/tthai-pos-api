"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSMailerService = void 0;
const common_1 = require("@nestjs/common");
const mailer_1 = require("@nestjs-modules/mailer");
let POSMailerService = class POSMailerService {
    constructor(mailerService) {
        this.mailerService = mailerService;
    }
    transformToHTML(payload) {
        const logoUrl = `<img style="width: 72px; height: auto;" src="${payload.logoUrl}" alt="logo"/>`;
        const sectionHead = `<tr><td align="center">${logoUrl}</td></tr><tr><td align="center" style="font-size: 1.225rem; font-weight: 600;">${payload.dba}</td></tr>`;
        const sectionTotal = `<tr><td style="font-size: 3rem; font-weight: 700;">${payload.total}</td></tr>`;
        const sectionOrderInfo = `
      <tr>
        <td align="left">${payload.orderDate}</td>
        <td align="right">${payload.orderTime}</td>
      </tr>
      <tr>
        <td align="left">${payload.orderType}</td>
      </tr>
      <tr>
        <td align="left">${payload.orderId}</td>
        <td align="right">${payload.transactionId}</td>
      </tr>
      <tr>
        <td align="left">${payload.ticketNo}</td>
      </tr>
    `;
        const sectionItemsQuantity = `
    <tr>
      <td align="left">${payload.itemsQuantity}</td>
    </tr>
    `;
        const sectionItemList = payload.itemList
            .map((item) => `
    <tr>
      <td align="left">${item.label}</td>
      <td align="right">${item.amount}</td>
    </tr>
    `)
            .join('');
        const sectionSummary = payload.summaryList
            .map((item) => `
    <tr>
      <td align="left">${item.label}</td>
      <td align="right">${item.amount}</td>
    </tr>
  `)
            .join('');
        const sectionTips = `
      <tr>
        <td align="left">${payload.tips.label}</td>
        <td align="right">${payload.tips.amount}</td>
      </tr>
      `;
        const sectionAddress = payload.addressDetail
            .map((item) => `
      <tr>
        <td align="center">${item}</td>
      </tr>
    `)
            .join('');
        const sectionPayment = `
    <tr style="font-size: 0.725rem;">
      <td align="left">${payload.paymentDetail.paidBy} ${payload.paymentDetail.cardType} ${payload.paymentDetail.cardLastFour}</td>
      <td align="right">${payload.paymentDetail.date}</td>
    </tr>
    <tr style="font-size: 0.725rem;">
      <td align="left">${payload.paymentDetail.cardType}</td>
      <td align="right">${payload.paymentDetail.authCode}</td>
    </tr>
    `;
        const sectionPolicy = `
    <tr style="font-size: 0.725rem;">
      <td style="display: flex; flex-wrap: wrap;">
      Return Policy: We stand behind everything we sell. If\n you aren't satisfied with your purchase, you can\n return it for a replacement or full refund
      </td>
    </tr>
    `;
        const footer = `
    <tr style="font-size: 1.225rem; font-weight: 500">
      <td style="display: flex; flex-wrap: wrap;">
      <a href="https://www.tthai.co/th/" style="text-decoration: none;">Powered by TThai.co</a>
      </td>
    </tr>
    `;
        return `
    <head>
    <style>
    </style>
    </head>
    <body>
      <table style="margin-left: auto; margin-right: auto;" align="center">
      ${sectionHead}
      ${sectionTotal}
      </table>
      <table style="margin-left: auto; margin-right: auto;" align="center">
      ${sectionOrderInfo}
      <br>
      ${sectionItemsQuantity}
      ${sectionItemList}
      <br>
      ${sectionSummary}
      <br>
      ${sectionTips}
      </table>
      <br>
      <table style="margin-left: auto; margin-right: auto;" align="center">
      ${sectionAddress}
      </table>
      <br>
      <table style="margin-left: auto; margin-right: auto;" align="center">
      ${sectionPayment}
      </table>
      <br>
      <br>
      ${sectionPolicy}
      ${footer}
    </body>
    `;
    }
    async postMail(mailDto) {
        const { to, from, subject, text, html } = mailDto;
        const send = await this.mailerService.sendMail({
            to,
            from,
            subject,
            text,
            html
        });
        return send;
    }
};
POSMailerService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [mailer_1.MailerService])
], POSMailerService);
exports.POSMailerService = POSMailerService;
//# sourceMappingURL=mailer.service.js.map