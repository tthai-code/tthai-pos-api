import { MailerService } from '@nestjs-modules/mailer';
import { MailerDto } from '../dto/mailer.dto';
export declare class POSMailerService {
    private readonly mailerService;
    constructor(mailerService: MailerService);
    transformToHTML(payload: any): string;
    postMail(mailDto: MailerDto): Promise<SentMessageInfo>;
}
