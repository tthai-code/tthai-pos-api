"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSMailerModule = void 0;
const common_1 = require("@nestjs/common");
const mailer_1 = require("@nestjs-modules/mailer");
const mailer_service_1 = require("./services/mailer.service");
const config_1 = require("@nestjs/config");
const mailer_controller_1 = require("./controllers/mailer.controller");
const axios_1 = require("@nestjs/axios");
let POSMailerModule = class POSMailerModule {
};
POSMailerModule = __decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            axios_1.HttpModule,
            config_1.ConfigModule.forRoot(),
            mailer_1.MailerModule.forRoot({
                transport: {
                    service: 'gmail',
                    host: 'smtp.gmail.com',
                    auth: {
                        user: process.env.EMAIL_USER,
                        pass: process.env.EMAIL_PASSWORD
                    }
                }
            })
        ],
        providers: [mailer_service_1.POSMailerService],
        controllers: [mailer_controller_1.MailerController],
        exports: [mailer_service_1.POSMailerService]
    })
], POSMailerModule);
exports.POSMailerModule = POSMailerModule;
//# sourceMappingURL=mailer.module.js.map