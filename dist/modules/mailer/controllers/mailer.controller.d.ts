import { MailerDto } from '../dto/mailer.dto';
import { POSMailerService } from '../services/mailer.service';
export declare class MailerController {
    private readonly mailerService;
    constructor(mailerService: POSMailerService);
    postEmail(payload: MailerDto): Promise<SentMessageInfo>;
}
