import { LogService } from '../services/log.service';
export declare class LogLogic {
    private readonly accessLogService;
    constructor(accessLogService: LogService);
    createLogic(request: any, note: any, remark?: any): Promise<import("../schemas/log.schema").LogDocument>;
}
