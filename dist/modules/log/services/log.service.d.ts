import { LogPaginateDto } from '../dto/get-log.dto';
import { LogDocument } from '../schemas/log.schema';
import { PaginateModel, PaginateResult } from 'mongoose-paginate';
export declare class LogService {
    private accessLogModel;
    private request;
    constructor(accessLogModel: PaginateModel<LogDocument>, request: any);
    create(createAccessLogDto: any): Promise<LogDocument>;
    paginate(query: any, queryParam: LogPaginateDto): Promise<PaginateResult<LogDocument>>;
}
