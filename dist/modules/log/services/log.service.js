"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_paginate_1 = require("mongoose-paginate");
const core_1 = require("@nestjs/core");
let LogService = class LogService {
    constructor(accessLogModel, request) {
        this.accessLogModel = accessLogModel;
        this.request = request;
    }
    async create(createAccessLogDto) {
        const created = new this.accessLogModel(createAccessLogDto);
        created === null || created === void 0 ? void 0 : created.setAuthor(this.request);
        return created.save();
    }
    paginate(query, queryParam) {
        const options = {
            page: Number(queryParam.page),
            limit: Number(queryParam.limit),
            sort: { [queryParam.sortBy]: queryParam.sortOrder }
        };
        return this.accessLogModel.paginate(query, options);
    }
};
LogService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('log')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_paginate_1.PaginateModel !== "undefined" && mongoose_paginate_1.PaginateModel) === "function" ? _a : Object, Object])
], LogService);
exports.LogService = LogService;
//# sourceMappingURL=log.service.js.map