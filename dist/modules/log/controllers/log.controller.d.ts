import { LogPaginateDto } from '../dto/get-log.dto';
import { LogService } from '../services/log.service';
export declare class LogController {
    private readonly logService;
    constructor(logService: LogService);
    getAccessLog(query: LogPaginateDto): Promise<PaginateResult<import("../schemas/log.schema").LogDocument>>;
}
