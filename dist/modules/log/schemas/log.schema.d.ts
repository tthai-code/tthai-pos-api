import * as mongoose from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type LogDocument = LogFields & mongoose.Document;
export declare class LogFields {
    status: string;
    ip: string;
    baseUrl: string;
    originalUrl: string;
    headers: mongoose.Schema.Types.Mixed;
    body: mongoose.Schema.Types.Mixed;
    params: mongoose.Schema.Types.Mixed;
    query: mongoose.Schema.Types.Mixed;
    method: string;
    note: string;
    req: mongoose.Schema.Types.Mixed;
    remark: mongoose.Schema.Types.Mixed;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const LogSchema: mongoose.Schema<mongoose.Document<LogFields, any, any>, mongoose.Model<mongoose.Document<LogFields, any, any>, any, any, any>, any, any>;
