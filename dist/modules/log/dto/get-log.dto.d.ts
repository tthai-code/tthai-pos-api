import { StatusEnum } from 'src/common/enum/status.enum';
import { PaginateDto } from 'src/common/dto/paginate.dto';
export declare class LogPaginateDto extends PaginateDto {
    readonly sortBy: string;
    readonly sortOrder: string;
    readonly search: string;
    readonly status: string;
    buildQuery(): {
        $or: ({
            'createdBy.username': {
                $regex: string;
                $options: string;
            };
            'body.order.id'?: undefined;
        } | {
            'body.order.id': number;
            'createdBy.username'?: undefined;
        })[];
        status: string | {
            $ne: StatusEnum;
        };
    };
}
