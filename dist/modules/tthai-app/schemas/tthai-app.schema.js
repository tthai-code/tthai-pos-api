"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TThaiAppSchema = exports.TThaiAppFields = exports.CustomizeInAppTextFields = exports.WaitTimeFields = exports.PickHoursFields = exports.PeriodFields = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const status_enum_1 = require("../../../common/enum/status.enum");
const user_stamp_schema_1 = require("../../../common/schemas/user-stamp.schema");
const author_stamp_plugin_1 = require("../../database/author-stamp.plugin");
const day_enum_1 = require("../../restaurant-hours/common/day.enum");
let PeriodFields = class PeriodFields {
};
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], PeriodFields.prototype, "opensAt", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", String)
], PeriodFields.prototype, "closesAt", void 0);
PeriodFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: false, strict: true })
], PeriodFields);
exports.PeriodFields = PeriodFields;
let PickHoursFields = class PickHoursFields {
};
__decorate([
    (0, mongoose_1.Prop)({ enum: Object.values(day_enum_1.DayEnum) }),
    __metadata("design:type", String)
], PickHoursFields.prototype, "label", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: true }),
    __metadata("design:type", Boolean)
], PickHoursFields.prototype, "isClosed", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: [] }),
    __metadata("design:type", Array)
], PickHoursFields.prototype, "period", void 0);
PickHoursFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: false, strict: true })
], PickHoursFields);
exports.PickHoursFields = PickHoursFields;
let WaitTimeFields = class WaitTimeFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], WaitTimeFields.prototype, "baseTime", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], WaitTimeFields.prototype, "perOrderTime", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 0 }),
    __metadata("design:type", Number)
], WaitTimeFields.prototype, "perItemTime", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 60 }),
    __metadata("design:type", Number)
], WaitTimeFields.prototype, "maxTime", void 0);
WaitTimeFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: false, strict: true })
], WaitTimeFields);
exports.WaitTimeFields = WaitTimeFields;
let CustomizeInAppTextFields = class CustomizeInAppTextFields {
};
__decorate([
    (0, mongoose_1.Prop)({ default: 'Please come pick up order inside the restuarant.' }),
    __metadata("design:type", String)
], CustomizeInAppTextFields.prototype, "pickUpOrderInstructionsLabel", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 'Please inform us if you have any dietary restrictions.' }),
    __metadata("design:type", String)
], CustomizeInAppTextFields.prototype, "orderInstructionsDesc", void 0);
__decorate([
    (0, mongoose_1.Prop)({
        default: 'All deliveries provided by in-house and third-party drivers.'
    }),
    __metadata("design:type", String)
], CustomizeInAppTextFields.prototype, "deliveryOrderInstructionsLabel", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: 'Special Instructions' }),
    __metadata("design:type", String)
], CustomizeInAppTextFields.prototype, "itemInstructionsLabel", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: null }),
    __metadata("design:type", String)
], CustomizeInAppTextFields.prototype, "itemInstructionsDesc", void 0);
CustomizeInAppTextFields = __decorate([
    (0, mongoose_1.Schema)({ _id: false, timestamps: false, strict: true })
], CustomizeInAppTextFields);
exports.CustomizeInAppTextFields = CustomizeInAppTextFields;
let TThaiAppFields = class TThaiAppFields {
};
__decorate([
    (0, mongoose_1.Prop)({ type: mongoose_2.Schema.Types.ObjectId, ref: 'restaurant' }),
    __metadata("design:type", mongoose_2.Types.ObjectId)
], TThaiAppFields.prototype, "restaurantId", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: false }),
    __metadata("design:type", Boolean)
], TThaiAppFields.prototype, "isMobileOrdering", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", Array)
], TThaiAppFields.prototype, "pickupHours", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: new WaitTimeFields() }),
    __metadata("design:type", WaitTimeFields)
], TThaiAppFields.prototype, "waitTime", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: false }),
    __metadata("design:type", Boolean)
], TThaiAppFields.prototype, "isItemSpecialInstructions", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: new CustomizeInAppTextFields() }),
    __metadata("design:type", CustomizeInAppTextFields)
], TThaiAppFields.prototype, "customizeInAppText", void 0);
__decorate([
    (0, mongoose_1.Prop)({ default: status_enum_1.StatusEnum.ACTIVE, enum: Object.values(status_enum_1.StatusEnum) }),
    __metadata("design:type", String)
], TThaiAppFields.prototype, "status", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], TThaiAppFields.prototype, "updatedBy", void 0);
__decorate([
    (0, mongoose_1.Prop)(),
    __metadata("design:type", user_stamp_schema_1.UserStampSchema)
], TThaiAppFields.prototype, "createdBy", void 0);
TThaiAppFields = __decorate([
    (0, mongoose_1.Schema)({ timestamps: true, strict: true, collection: 'tthaiApps' })
], TThaiAppFields);
exports.TThaiAppFields = TThaiAppFields;
exports.TThaiAppSchema = mongoose_1.SchemaFactory.createForClass(TThaiAppFields);
exports.TThaiAppSchema.plugin(author_stamp_plugin_1.authorStampCreatePlugin);
//# sourceMappingURL=tthai-app.schema.js.map