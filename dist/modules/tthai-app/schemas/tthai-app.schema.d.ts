import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type TThaiAppDocument = TThaiAppFields & Document;
export declare class PeriodFields {
    opensAt: string;
    closesAt: string;
}
export declare class PickHoursFields {
    label: string;
    isClosed: boolean;
    period: Array<PeriodFields>;
}
export declare class WaitTimeFields {
    baseTime: number;
    perOrderTime: number;
    perItemTime: number;
    maxTime: number;
}
export declare class CustomizeInAppTextFields {
    pickUpOrderInstructionsLabel: string;
    orderInstructionsDesc: string;
    deliveryOrderInstructionsLabel: string;
    itemInstructionsLabel: string;
    itemInstructionsDesc: string;
}
export declare class TThaiAppFields {
    restaurantId: Types.ObjectId;
    isMobileOrdering: boolean;
    pickupHours: Array<PickHoursFields>;
    waitTime: WaitTimeFields;
    isItemSpecialInstructions: boolean;
    customizeInAppText: CustomizeInAppTextFields;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const TThaiAppSchema: MongooseSchema<Document<TThaiAppFields, any, any>, import("mongoose").Model<Document<TThaiAppFields, any, any>, any, any, any>, any, any>;
