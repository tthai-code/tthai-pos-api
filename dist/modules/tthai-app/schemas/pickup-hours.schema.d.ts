import { Types, Schema as MongooseSchema, Document } from 'mongoose';
export declare type PickupHoursDocument = PickupHoursFields & Document;
export declare class PickupHoursFields {
    restaurantId: Types.ObjectId;
    restaurantPeriodId: Types.ObjectId;
}
export declare const PickupHoursSchema: MongooseSchema<Document<PickupHoursFields, any, any>, import("mongoose").Model<Document<PickupHoursFields, any, any>, any, any, any>, any, any>;
