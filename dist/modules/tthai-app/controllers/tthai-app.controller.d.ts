/// <reference types="mongoose" />
import { UpdatePickupHoursDto } from '../dto/update-pickup-hours.dto';
import { UpdateCustomizeInAppTextDto, UpdateItemSpecialInstructionsDto, UpdateMobileOrderingDto, UpdateWaitTimeDto } from '../dto/update-tthai-app.dto';
import { PickupHoursLogic } from '../logics/pickup-hours.logic';
import { TThaiAppLogic } from '../logics/tthai-app.logic';
export declare class TThaiAppController {
    private readonly tthaiAppLogic;
    private readonly pickupHoursLogic;
    constructor(tthaiAppLogic: TThaiAppLogic, pickupHoursLogic: PickupHoursLogic);
    getTThaiApp(id: string): Promise<{
        pickupHours: any[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        restaurantId: import("mongoose").Types.ObjectId;
        isMobileOrdering: boolean;
        waitTime: import("../schemas/tthai-app.schema").WaitTimeFields;
        isItemSpecialInstructions: boolean;
        customizeInAppText: import("../schemas/tthai-app.schema").CustomizeInAppTextFields;
    }>;
    updateMobileOrdering(id: string, payload: UpdateMobileOrderingDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
    updateIsItemSpecialInstructions(id: string, payload: UpdateItemSpecialInstructionsDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
    updatePickUpHoursDto(id: string, payload: UpdatePickupHoursDto): Promise<{
        success: boolean;
    }>;
    updateWaitTimeDto(id: string, payload: UpdateWaitTimeDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
    updateCustomizeInAppTextDto(id: string, payload: UpdateCustomizeInAppTextDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
}
