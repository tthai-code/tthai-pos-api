"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSEatsZaabController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const open_eats_zaap_entity_1 = require("../entity/open-eats-zaap.entity");
const pos_eats_zaap_logic_1 = require("../logics/pos-eats-zaap.logic");
let POSEatsZaabController = class POSEatsZaabController {
    constructor(posEatsZaabLogic) {
        this.posEatsZaabLogic = posEatsZaabLogic;
    }
    async getEatsZaab(id) {
        return await this.posEatsZaabLogic.getTThaiApp(id);
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => open_eats_zaap_entity_1.GetEatsZaabResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get EatsZaab Setting by Restaurant ID',
        description: 'use basic auth'
    }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], POSEatsZaabController.prototype, "getEatsZaab", null);
POSEatsZaabController = __decorate([
    (0, swagger_1.ApiBasicAuth)(),
    (0, swagger_1.ApiTags)('open/eats-zaap'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    (0, common_1.Controller)('v1/open/eats-zaap'),
    __metadata("design:paramtypes", [pos_eats_zaap_logic_1.POSEatsZaabLogic])
], POSEatsZaabController);
exports.POSEatsZaabController = POSEatsZaabController;
//# sourceMappingURL=pos-eats-zaap.controller.js.map