"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TThaiAppController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const update_pickup_hours_dto_1 = require("../dto/update-pickup-hours.dto");
const update_tthai_app_dto_1 = require("../dto/update-tthai-app.dto");
const tthai_app_entity_1 = require("../entity/tthai-app.entity");
const pickup_hours_logic_1 = require("../logics/pickup-hours.logic");
const tthai_app_logic_1 = require("../logics/tthai-app.logic");
let TThaiAppController = class TThaiAppController {
    constructor(tthaiAppLogic, pickupHoursLogic) {
        this.tthaiAppLogic = tthaiAppLogic;
        this.pickupHoursLogic = pickupHoursLogic;
    }
    async getTThaiApp(id) {
        return await this.tthaiAppLogic.getTThaiApp(id);
    }
    async updateMobileOrdering(id, payload) {
        return await this.tthaiAppLogic.updateMobileOrdering(id, payload);
    }
    async updateIsItemSpecialInstructions(id, payload) {
        return await this.tthaiAppLogic.updateItemSpecialInstructions(id, payload);
    }
    async updatePickUpHoursDto(id, payload) {
        return await this.pickupHoursLogic.updatePickupHours(id, payload);
    }
    async updateWaitTimeDto(id, payload) {
        return await this.tthaiAppLogic.updateWaitTimeDto(id, payload);
    }
    async updateCustomizeInAppTextDto(id, payload) {
        return await this.tthaiAppLogic.updateCustomizeInAppTextDto(id, payload);
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => tthai_app_entity_1.TThaiAppResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'get TThai app setting by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TThaiAppController.prototype, "getTThaiApp", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/mobile-ordering'),
    (0, swagger_1.ApiOkResponse)({ type: () => tthai_app_entity_1.TThaiAppResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'update mobile ordering by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_tthai_app_dto_1.UpdateMobileOrderingDto]),
    __metadata("design:returntype", Promise)
], TThaiAppController.prototype, "updateMobileOrdering", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/item-special'),
    (0, swagger_1.ApiOkResponse)({ type: () => tthai_app_entity_1.TThaiAppResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'update item special instructions by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_tthai_app_dto_1.UpdateItemSpecialInstructionsDto]),
    __metadata("design:returntype", Promise)
], TThaiAppController.prototype, "updateIsItemSpecialInstructions", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/pickup-hours'),
    (0, swagger_1.ApiOkResponse)({ type: () => tthai_app_entity_1.TThaiAppResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'update pick up hours by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_pickup_hours_dto_1.UpdatePickupHoursDto]),
    __metadata("design:returntype", Promise)
], TThaiAppController.prototype, "updatePickUpHoursDto", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/wait-time'),
    (0, swagger_1.ApiOkResponse)({ type: () => tthai_app_entity_1.TThaiAppResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'update wait time by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_tthai_app_dto_1.UpdateWaitTimeDto]),
    __metadata("design:returntype", Promise)
], TThaiAppController.prototype, "updateWaitTimeDto", null);
__decorate([
    (0, common_1.Patch)(':restaurantId/in-app-text'),
    (0, swagger_1.ApiOkResponse)({ type: () => tthai_app_entity_1.TThaiAppResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'update customize in app text by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_tthai_app_dto_1.UpdateCustomizeInAppTextDto]),
    __metadata("design:returntype", Promise)
], TThaiAppController.prototype, "updateCustomizeInAppTextDto", null);
TThaiAppController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('tthai-app-setting'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/tthai-app'),
    __metadata("design:paramtypes", [tthai_app_logic_1.TThaiAppLogic,
        pickup_hours_logic_1.PickupHoursLogic])
], TThaiAppController);
exports.TThaiAppController = TThaiAppController;
//# sourceMappingURL=tthai-app.controller.js.map