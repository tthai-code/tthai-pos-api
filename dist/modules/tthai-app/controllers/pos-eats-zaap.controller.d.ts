/// <reference types="mongoose" />
import { POSEatsZaabLogic } from '../logics/pos-eats-zaap.logic';
export declare class POSEatsZaabController {
    private readonly posEatsZaabLogic;
    constructor(posEatsZaabLogic: POSEatsZaabLogic);
    getEatsZaab(id: string): Promise<{
        pickupHours: any[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        restaurantId: import("mongoose").Types.ObjectId;
        isMobileOrdering: boolean;
        waitTime: import("../schemas/tthai-app.schema").WaitTimeFields;
        isItemSpecialInstructions: boolean;
        customizeInAppText: import("../schemas/tthai-app.schema").CustomizeInAppTextFields;
    }>;
}
