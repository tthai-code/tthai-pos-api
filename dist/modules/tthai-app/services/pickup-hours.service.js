"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PickupHoursService = void 0;
const mongoose_1 = require("@nestjs/mongoose");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const status_enum_1 = require("../../../common/enum/status.enum");
let PickupHoursService = class PickupHoursService {
    constructor(PickupHoursModel, request) {
        this.PickupHoursModel = PickupHoursModel;
        this.request = request;
    }
    async resolveByUrl({ id }) {
        return this.PickupHoursModel.findById({ _id: id });
    }
    async resolveByCondition(condition, project) {
        return this.PickupHoursModel.find(condition, project);
    }
    async isExists(condition) {
        const result = await this.PickupHoursModel.findOne(condition);
        return result ? true : false;
    }
    getModel() {
        return this.PickupHoursModel;
    }
    async getSession() {
        await this.PickupHoursModel.createCollection();
        return this.PickupHoursModel.startSession();
    }
    async create(payload) {
        const document = new this.PickupHoursModel(payload);
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.save();
    }
    async update(id, payload) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set(Object.assign({}, payload)).save();
    }
    async delete(id) {
        const document = await this.resolveByUrl({ id });
        document === null || document === void 0 ? void 0 : document.setAuthor(this.request);
        return document.set({ status: status_enum_1.StatusEnum.DELETED }).save();
    }
    async createMany(payload) {
        return this.PickupHoursModel.insertMany(payload);
    }
    async deleteMany(condition) {
        return this.PickupHoursModel.deleteMany(condition);
    }
    async transactionDeleteAll(condition, session) {
        return this.PickupHoursModel.deleteMany(condition, { session });
    }
    getAll(condition, project) {
        return this.PickupHoursModel.find(condition, project);
    }
    findById(id) {
        return this.PickupHoursModel.findById(id);
    }
    findOne(condition) {
        return this.PickupHoursModel.findOne(condition);
    }
};
PickupHoursService = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __param(0, (0, mongoose_1.InjectModel)('pickupHours')),
    __param(1, (0, common_1.Inject)(core_1.REQUEST)),
    __metadata("design:paramtypes", [Object, Object])
], PickupHoursService);
exports.PickupHoursService = PickupHoursService;
//# sourceMappingURL=pickup-hours.service.js.map