import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { TThaiAppDocument } from '../schemas/tthai-app.schema';
export declare class TThaiAppService {
    private readonly TThaiAppModel;
    private request;
    constructor(TThaiAppModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<TThaiAppDocument | any>;
    resolveByCondition(condition: any): Promise<TThaiAppDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<TThaiAppDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<TThaiAppDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<TThaiAppDocument>;
    create(payload: any): Promise<TThaiAppDocument>;
    update(id: string, payload: any): Promise<TThaiAppDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<TThaiAppDocument>;
    delete(id: string): Promise<TThaiAppDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<TThaiAppDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<TThaiAppDocument>;
}
