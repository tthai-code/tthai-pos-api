import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { PickupHoursDocument } from '../schemas/pickup-hours.schema';
export declare class PickupHoursService {
    private readonly PickupHoursModel;
    private request;
    constructor(PickupHoursModel: Model<PickupHoursDocument> | any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<PickupHoursDocument | any>;
    resolveByCondition(condition: any, project?: any): Promise<PickupHoursDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<PickupHoursDocument>;
    getSession(): Promise<ClientSession>;
    create(payload: any): Promise<PickupHoursDocument>;
    update(id: string, payload: any): Promise<PickupHoursDocument>;
    delete(id: string): Promise<PickupHoursDocument>;
    createMany(payload: any): Promise<PickupHoursDocument>;
    deleteMany(condition: any): Promise<void>;
    transactionDeleteAll(condition: any, session: any): Promise<void>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any): Promise<PickupHoursDocument>;
}
