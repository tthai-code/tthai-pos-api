"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TThaiAppModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_hours_module_1 = require("../restaurant-hours/restaurant-hours.module");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const pos_eats_zaap_controller_1 = require("./controllers/pos-eats-zaap.controller");
const tthai_app_controller_1 = require("./controllers/tthai-app.controller");
const pickup_hours_logic_1 = require("./logics/pickup-hours.logic");
const pos_eats_zaap_logic_1 = require("./logics/pos-eats-zaap.logic");
const tthai_app_logic_1 = require("./logics/tthai-app.logic");
const pickup_hours_schema_1 = require("./schemas/pickup-hours.schema");
const tthai_app_schema_1 = require("./schemas/tthai-app.schema");
const pickup_hours_service_1 = require("./services/pickup-hours.service");
const tthai_app_service_1 = require("./services/tthai-app.service");
let TThaiAppModule = class TThaiAppModule {
};
TThaiAppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_hours_module_1.RestaurantHoursModule),
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'tthaiApp', schema: tthai_app_schema_1.TThaiAppSchema },
                { name: 'pickupHours', schema: pickup_hours_schema_1.PickupHoursSchema }
            ])
        ],
        providers: [
            tthai_app_service_1.TThaiAppService,
            tthai_app_logic_1.TThaiAppLogic,
            pickup_hours_service_1.PickupHoursService,
            pickup_hours_logic_1.PickupHoursLogic,
            pos_eats_zaap_logic_1.POSEatsZaabLogic
        ],
        controllers: [tthai_app_controller_1.TThaiAppController, pos_eats_zaap_controller_1.POSEatsZaabController],
        exports: [tthai_app_service_1.TThaiAppService, tthai_app_logic_1.TThaiAppLogic]
    })
], TThaiAppModule);
exports.TThaiAppModule = TThaiAppModule;
//# sourceMappingURL=tthai-app.module.js.map