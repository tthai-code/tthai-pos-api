export declare class UpdateMobileOrderingDto {
    readonly isMobileOrdering: boolean;
}
export declare class UpdateItemSpecialInstructionsDto {
    readonly isItemSpecialInstructions: boolean;
}
declare class PeriodFieldsDto {
    opensAt: string;
    closesAt: string;
}
declare class PickUpHoursFieldsDto {
    readonly label: string;
    readonly isClosed: boolean;
    readonly period: Array<PeriodFieldsDto>;
}
export declare class UpdatePickUpHoursDto {
    readonly pickupHours: Array<PickUpHoursFieldsDto>;
}
declare class UpdateWaitTimeFieldsDto {
    readonly baseTime: number;
    readonly perOrderTime: number;
    readonly perItemTime: number;
    readonly maxTime: number;
}
export declare class UpdateWaitTimeDto {
    readonly waitTime: UpdateWaitTimeFieldsDto;
}
declare class UpdateCustomizeInAppTextFieldsDto {
    readonly pickUpOrderInstructionsLabel: string;
    readonly orderInstructionsDesc: string;
    readonly deliveryOrderInstructionsLabel: string;
    readonly itemInstructionsLabel: string;
    readonly itemInstructionsDesc: string;
}
export declare class UpdateCustomizeInAppTextDto {
    readonly customizeInAppText: UpdateCustomizeInAppTextFieldsDto;
}
export {};
