"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateCustomizeInAppTextDto = exports.UpdateWaitTimeDto = exports.UpdatePickUpHoursDto = exports.UpdateItemSpecialInstructionsDto = exports.UpdateMobileOrderingDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const class_validator_1 = require("class-validator");
const day_enum_1 = require("../../restaurant-hours/common/day.enum");
class UpdateMobileOrderingDto {
}
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)({ example: true }),
    __metadata("design:type", Boolean)
], UpdateMobileOrderingDto.prototype, "isMobileOrdering", void 0);
exports.UpdateMobileOrderingDto = UpdateMobileOrderingDto;
class UpdateItemSpecialInstructionsDto {
}
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, swagger_1.ApiProperty)({ example: true }),
    __metadata("design:type", Boolean)
], UpdateItemSpecialInstructionsDto.prototype, "isItemSpecialInstructions", void 0);
exports.UpdateItemSpecialInstructionsDto = UpdateItemSpecialInstructionsDto;
class PeriodFieldsDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ type: String, example: '09:00AM' }),
    __metadata("design:type", String)
], PeriodFieldsDto.prototype, "opensAt", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ type: String, example: '12:00AM' }),
    __metadata("design:type", String)
], PeriodFieldsDto.prototype, "closesAt", void 0);
class PickUpHoursFieldsDto {
}
__decorate([
    (0, class_validator_1.IsEnum)(day_enum_1.DayEnum),
    (0, swagger_1.ApiProperty)({ enum: Object.values(day_enum_1.DayEnum), example: day_enum_1.DayEnum.MONDAY }),
    __metadata("design:type", String)
], PickUpHoursFieldsDto.prototype, "label", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, swagger_1.ApiProperty)({ example: false }),
    __metadata("design:type", Boolean)
], PickUpHoursFieldsDto.prototype, "isClosed", void 0);
__decorate([
    (0, class_transformer_1.Type)(() => PeriodFieldsDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => [PeriodFieldsDto] }),
    __metadata("design:type", Array)
], PickUpHoursFieldsDto.prototype, "period", void 0);
class UpdatePickUpHoursDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => PickUpHoursFieldsDto),
    (0, class_validator_1.ArrayNotEmpty)(),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => [PickUpHoursFieldsDto] }),
    __metadata("design:type", Array)
], UpdatePickUpHoursDto.prototype, "pickupHours", void 0);
exports.UpdatePickUpHoursDto = UpdatePickUpHoursDto;
class UpdateWaitTimeFieldsDto {
}
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 15 }),
    __metadata("design:type", Number)
], UpdateWaitTimeFieldsDto.prototype, "baseTime", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 15 }),
    __metadata("design:type", Number)
], UpdateWaitTimeFieldsDto.prototype, "perOrderTime", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 15 }),
    __metadata("design:type", Number)
], UpdateWaitTimeFieldsDto.prototype, "perItemTime", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, swagger_1.ApiProperty)({ example: 60 }),
    __metadata("design:type", Number)
], UpdateWaitTimeFieldsDto.prototype, "maxTime", void 0);
class UpdateWaitTimeDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => UpdateWaitTimeFieldsDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => UpdateWaitTimeFieldsDto }),
    __metadata("design:type", UpdateWaitTimeFieldsDto)
], UpdateWaitTimeDto.prototype, "waitTime", void 0);
exports.UpdateWaitTimeDto = UpdateWaitTimeDto;
class UpdateCustomizeInAppTextFieldsDto {
}
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'pickUpOrderInstructionsLabel Test' }),
    __metadata("design:type", String)
], UpdateCustomizeInAppTextFieldsDto.prototype, "pickUpOrderInstructionsLabel", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Test Desc' }),
    __metadata("design:type", String)
], UpdateCustomizeInAppTextFieldsDto.prototype, "orderInstructionsDesc", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Test Label' }),
    __metadata("design:type", String)
], UpdateCustomizeInAppTextFieldsDto.prototype, "deliveryOrderInstructionsLabel", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Item Label Test' }),
    __metadata("design:type", String)
], UpdateCustomizeInAppTextFieldsDto.prototype, "itemInstructionsLabel", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, swagger_1.ApiProperty)({ example: 'Item Desc Test' }),
    __metadata("design:type", String)
], UpdateCustomizeInAppTextFieldsDto.prototype, "itemInstructionsDesc", void 0);
class UpdateCustomizeInAppTextDto {
}
__decorate([
    (0, class_transformer_1.Type)(() => UpdateCustomizeInAppTextFieldsDto),
    (0, class_validator_1.ValidateNested)({ each: true }),
    (0, swagger_1.ApiProperty)({ type: () => UpdateCustomizeInAppTextFieldsDto }),
    __metadata("design:type", UpdateCustomizeInAppTextFieldsDto)
], UpdateCustomizeInAppTextDto.prototype, "customizeInAppText", void 0);
exports.UpdateCustomizeInAppTextDto = UpdateCustomizeInAppTextDto;
//# sourceMappingURL=update-tthai-app.dto.js.map