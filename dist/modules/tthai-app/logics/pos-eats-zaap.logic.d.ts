/// <reference types="mongoose" />
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { TThaiAppService } from '../services/tthai-app.service';
import { PickupHoursLogic } from './pickup-hours.logic';
export declare class POSEatsZaabLogic {
    private readonly tthaiAppService;
    private readonly restaurantService;
    private readonly pickupHoursLogic;
    constructor(tthaiAppService: TThaiAppService, restaurantService: RestaurantService, pickupHoursLogic: PickupHoursLogic);
    private fetchRestaurant;
    getTThaiApp(restaurantId: string): Promise<{
        pickupHours: any[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        restaurantId: import("mongoose").Types.ObjectId;
        isMobileOrdering: boolean;
        waitTime: import("../schemas/tthai-app.schema").WaitTimeFields;
        isItemSpecialInstructions: boolean;
        customizeInAppText: import("../schemas/tthai-app.schema").CustomizeInAppTextFields;
    }>;
}
