"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PickupHoursLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_day_service_1 = require("../../restaurant-hours/services/restaurant-day.service");
const restaurant_period_service_1 = require("../../restaurant-hours/services/restaurant-period.service");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const pickup_hours_service_1 = require("../services/pickup-hours.service");
let PickupHoursLogic = class PickupHoursLogic {
    constructor(pickupHoursService, restaurantService, restaurantPeriodService, restaurantDayService) {
        this.pickupHoursService = pickupHoursService;
        this.restaurantService = restaurantService;
        this.restaurantPeriodService = restaurantPeriodService;
        this.restaurantDayService = restaurantDayService;
    }
    async getPickupHours(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const restaurantDays = await this.restaurantDayService.getAll({ restaurantId }, { _id: 0, label: 1, isClosed: 1 }, { sort: { position: 1 } });
        const openDays = restaurantDays
            .filter((day) => !day.isClosed)
            .map((item) => item.label);
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId,
            label: { $in: openDays },
            status: status_enum_1.StatusEnum.ACTIVE
        }, { label: 1, opensAt: 1, closesAt: 1 });
        const pickupHours = await this.pickupHoursService.getAll({ restaurantId });
        const selectedPeriod = pickupHours.map((selected) => `${selected.restaurantPeriodId}`);
        const result = [];
        for (const day of restaurantDays) {
            const item = {
                label: day.label,
                isClosed: day.isClosed,
                period: []
            };
            if (day.isClosed) {
                result.push(item);
            }
            else {
                const dayPeriod = restaurantPeriods.filter((period) => period.label === day.label);
                item.period = dayPeriod.map((e) => ({
                    id: e._id,
                    label: e.label,
                    opensAt: e.opensAt,
                    closesAt: e.closesAt,
                    isSelected: selectedPeriod.includes(`${e._id}`)
                }));
                result.push(item);
            }
        }
        return result;
    }
    async updatePickupHours(restaurantId, payload) {
        const { period } = payload;
        const pickupHoursPayload = period.map((item) => ({
            restaurantPeriodId: item,
            restaurantId
        }));
        await this.pickupHoursService.deleteMany({ restaurantId });
        await this.pickupHoursService.createMany(pickupHoursPayload);
        return { success: true };
    }
    async getPickupHoursForEatsZaab(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const restaurantDays = await this.restaurantDayService.getAll({ restaurantId }, { _id: 0, label: 1, isClosed: 1 }, { sort: { position: 1 } });
        const openDays = restaurantDays
            .filter((day) => !day.isClosed)
            .map((item) => item.label);
        const restaurantPeriods = await this.restaurantPeriodService.getAll({
            restaurantId,
            label: { $in: openDays },
            status: status_enum_1.StatusEnum.ACTIVE
        }, { label: 1, opensAt: 1, closesAt: 1 });
        const pickupHours = await this.pickupHoursService.getAll({ restaurantId });
        const selectedPeriod = pickupHours.map((selected) => `${selected.restaurantPeriodId}`);
        const result = [];
        for (const day of restaurantDays) {
            const item = {
                label: day.label,
                isClosed: day.isClosed,
                period: []
            };
            if (day.isClosed) {
                result.push(item);
            }
            else {
                const dayPeriod = restaurantPeriods.filter((period) => period.label === day.label &&
                    selectedPeriod.includes(`${period._id}`));
                item.period = dayPeriod.map((e) => ({
                    opensAt: e.opensAt,
                    closesAt: e.closesAt
                }));
                result.push(item);
            }
        }
        return result;
    }
};
PickupHoursLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [pickup_hours_service_1.PickupHoursService,
        restaurant_service_1.RestaurantService,
        restaurant_period_service_1.RestaurantPeriodService,
        restaurant_day_service_1.RestaurantDayService])
], PickupHoursLogic);
exports.PickupHoursLogic = PickupHoursLogic;
//# sourceMappingURL=pickup-hours.logic.js.map