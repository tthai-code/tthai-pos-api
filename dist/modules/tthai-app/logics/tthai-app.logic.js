"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TThaiAppLogic = void 0;
const common_1 = require("@nestjs/common");
const status_enum_1 = require("../../../common/enum/status.enum");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const tthai_app_service_1 = require("../services/tthai-app.service");
const pickup_hours_logic_1 = require("./pickup-hours.logic");
let TThaiAppLogic = class TThaiAppLogic {
    constructor(tthaiAppService, restaurantService, pickupHoursLogic) {
        this.tthaiAppService = tthaiAppService;
        this.restaurantService = restaurantService;
        this.pickupHoursLogic = pickupHoursLogic;
        this.initialHours = [
            {
                label: 'MONDAY',
                isClosed: true,
                period: []
            },
            {
                label: 'TUESDAY',
                isClosed: true,
                period: []
            },
            {
                label: 'WEDNESDAY',
                isClosed: true,
                period: []
            },
            {
                label: 'THURSDAY',
                isClosed: true,
                period: []
            },
            {
                label: 'FRIDAY',
                isClosed: true,
                period: []
            },
            {
                label: 'SATURDAY',
                isClosed: true,
                period: []
            },
            {
                label: 'SUNDAY',
                isClosed: true,
                period: []
            }
        ];
    }
    initTThaiAppSetting(restaurantId) {
        return {
            restaurantId,
            pickupHours: this.initialHours
        };
    }
    async fetchRestaurant(restaurantId) {
        const restaurant = await this.restaurantService.findOne({
            _id: restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        return restaurant;
    }
    async getTThaiApp(restaurantId) {
        await this.fetchRestaurant(restaurantId);
        const tthaiApp = await this.tthaiAppService.findOne({
            restaurantId,
            status: status_enum_1.StatusEnum.ACTIVE
        }, { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 });
        if (!tthaiApp) {
            const payload = this.initTThaiAppSetting(restaurantId);
            const created = await this.tthaiAppService.create(payload);
            if (!created)
                throw new common_1.BadRequestException();
            const docs = await this.tthaiAppService.findOne({
                restaurantId,
                status: status_enum_1.StatusEnum.ACTIVE
            }, { status: 0, createdAt: 0, updatedAt: 0, createdBy: 0, updatedBy: 0 });
            const pickupHours = await this.pickupHoursLogic.getPickupHours(restaurantId);
            const result = Object.assign(Object.assign({}, docs.toObject()), { pickupHours });
            return result;
        }
        const pickupHours = await this.pickupHoursLogic.getPickupHours(restaurantId);
        const result = Object.assign(Object.assign({}, tthaiApp.toObject()), { pickupHours });
        return result;
    }
    async updateMobileOrdering(restaurantId, payload) {
        await this.fetchRestaurant(restaurantId);
        return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload);
    }
    async updateItemSpecialInstructions(restaurantId, payload) {
        await this.fetchRestaurant(restaurantId);
        return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload);
    }
    async updatePickUpHoursDto(restaurantId, payload) {
        await this.fetchRestaurant(restaurantId);
        return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload);
    }
    async updateWaitTimeDto(restaurantId, payload) {
        await this.fetchRestaurant(restaurantId);
        return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload);
    }
    async updateCustomizeInAppTextDto(restaurantId, payload) {
        await this.fetchRestaurant(restaurantId);
        return this.tthaiAppService.findOneAndUpdate({ restaurantId }, payload);
    }
};
TThaiAppLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [tthai_app_service_1.TThaiAppService,
        restaurant_service_1.RestaurantService,
        pickup_hours_logic_1.PickupHoursLogic])
], TThaiAppLogic);
exports.TThaiAppLogic = TThaiAppLogic;
//# sourceMappingURL=tthai-app.logic.js.map