import { RestaurantDayService } from 'src/modules/restaurant-hours/services/restaurant-day.service';
import { RestaurantPeriodService } from 'src/modules/restaurant-hours/services/restaurant-period.service';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdatePickupHoursDto } from '../dto/update-pickup-hours.dto';
import { PickupHoursService } from '../services/pickup-hours.service';
export declare class PickupHoursLogic {
    private readonly pickupHoursService;
    private readonly restaurantService;
    private readonly restaurantPeriodService;
    private readonly restaurantDayService;
    constructor(pickupHoursService: PickupHoursService, restaurantService: RestaurantService, restaurantPeriodService: RestaurantPeriodService, restaurantDayService: RestaurantDayService);
    getPickupHours(restaurantId: string): Promise<any[]>;
    updatePickupHours(restaurantId: string, payload: UpdatePickupHoursDto): Promise<{
        success: boolean;
    }>;
    getPickupHoursForEatsZaab(restaurantId: string): Promise<any[]>;
}
