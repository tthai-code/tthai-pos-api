/// <reference types="mongoose" />
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdateCustomizeInAppTextDto, UpdateItemSpecialInstructionsDto, UpdateMobileOrderingDto, UpdatePickUpHoursDto, UpdateWaitTimeDto } from '../dto/update-tthai-app.dto';
import { TThaiAppService } from '../services/tthai-app.service';
import { PickupHoursLogic } from './pickup-hours.logic';
export declare class TThaiAppLogic {
    private readonly tthaiAppService;
    private readonly restaurantService;
    private readonly pickupHoursLogic;
    constructor(tthaiAppService: TThaiAppService, restaurantService: RestaurantService, pickupHoursLogic: PickupHoursLogic);
    private initialHours;
    private initTThaiAppSetting;
    private fetchRestaurant;
    getTThaiApp(restaurantId: string): Promise<{
        pickupHours: any[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        restaurantId: import("mongoose").Types.ObjectId;
        isMobileOrdering: boolean;
        waitTime: import("../schemas/tthai-app.schema").WaitTimeFields;
        isItemSpecialInstructions: boolean;
        customizeInAppText: import("../schemas/tthai-app.schema").CustomizeInAppTextFields;
    }>;
    updateMobileOrdering(restaurantId: string, payload: UpdateMobileOrderingDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
    updateItemSpecialInstructions(restaurantId: string, payload: UpdateItemSpecialInstructionsDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
    updatePickUpHoursDto(restaurantId: string, payload: UpdatePickUpHoursDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
    updateWaitTimeDto(restaurantId: string, payload: UpdateWaitTimeDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
    updateCustomizeInAppTextDto(restaurantId: string, payload: UpdateCustomizeInAppTextDto): Promise<import("../schemas/tthai-app.schema").TThaiAppDocument>;
}
