import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
declare class PeriodFieldsResponse {
    opensAt: string;
    closesAt: string;
}
declare class PickUpHoursFieldsResponse {
    readonly label: string;
    readonly isClosed: boolean;
    readonly period: Array<PeriodFieldsResponse>;
}
declare class WaitTimeResponse {
    baseTime: number;
    perOrderTime: number;
    perItemTime: number;
}
declare class CustomizeInAppTextResponse {
    pickUpOrderInstructionsLabel: string;
    orderInstructionsDesc: string;
    deliveryOrderInstructionsLabel: string;
    itemInstructionsLabel: string;
    itemInstructionsDesc: string;
}
export declare class TThaiAppResponseFields extends TimestampResponseDto {
    id: string;
    restaurantId: string;
    isMobileOrdering: boolean;
    isItemSpecialInstructions: boolean;
    pickupHours: PickUpHoursFieldsResponse;
    waitTime: WaitTimeResponse;
    customizeInAppText: CustomizeInAppTextResponse;
}
export declare class TThaiAppResponse extends ResponseDto<TThaiAppResponseFields> {
    data: TThaiAppResponseFields;
}
export {};
