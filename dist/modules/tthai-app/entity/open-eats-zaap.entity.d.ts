import { ResponseDto } from 'src/common/entity/response.entity';
declare class PeriodItemsObject {
    opens_at: string;
    closes_at: string;
}
declare class PickupHoursObject {
    label: string;
    is_closed: boolean;
    period: PeriodItemsObject[];
}
declare class CustomizeInAppTextObject {
    item_instructions_desc: string;
    item_instructions_label: string;
    delivery_order_instructions_label: string;
    order_instructions_desc: string;
    pick_up_order_instructions_label: string;
}
declare class WaitTimeObject {
    per_item_time: number;
    per_order_time: number;
    base_time: number;
}
declare class EatsZaabObject {
    customize_in_app_text: CustomizeInAppTextObject;
    is_item_special_instructions: boolean;
    wait_time: WaitTimeObject;
    pickup_hours: PickupHoursObject[];
    is_mobile_ordering: boolean;
    restaurant_id: string;
    id: string;
}
export declare class GetEatsZaabResponse extends ResponseDto<EatsZaabObject> {
    data: EatsZaabObject;
}
export {};
