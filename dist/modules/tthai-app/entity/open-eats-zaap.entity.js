"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetEatsZaabResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
class PeriodItemsObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PeriodItemsObject.prototype, "opens_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PeriodItemsObject.prototype, "closes_at", void 0);
class PickupHoursObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PickupHoursObject.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], PickupHoursObject.prototype, "is_closed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [PeriodItemsObject] }),
    __metadata("design:type", Array)
], PickupHoursObject.prototype, "period", void 0);
class CustomizeInAppTextObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextObject.prototype, "item_instructions_desc", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextObject.prototype, "item_instructions_label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextObject.prototype, "delivery_order_instructions_label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextObject.prototype, "order_instructions_desc", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextObject.prototype, "pick_up_order_instructions_label", void 0);
class WaitTimeObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WaitTimeObject.prototype, "per_item_time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WaitTimeObject.prototype, "per_order_time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WaitTimeObject.prototype, "base_time", void 0);
class EatsZaabObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: CustomizeInAppTextObject }),
    __metadata("design:type", CustomizeInAppTextObject)
], EatsZaabObject.prototype, "customize_in_app_text", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], EatsZaabObject.prototype, "is_item_special_instructions", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: WaitTimeObject }),
    __metadata("design:type", WaitTimeObject)
], EatsZaabObject.prototype, "wait_time", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [PickupHoursObject] }),
    __metadata("design:type", Array)
], EatsZaabObject.prototype, "pickup_hours", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], EatsZaabObject.prototype, "is_mobile_ordering", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], EatsZaabObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], EatsZaabObject.prototype, "id", void 0);
class GetEatsZaabResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: EatsZaabObject,
        example: {
            customize_in_app_text: {
                item_instructions_desc: 'TEST',
                item_instructions_label: 'Special Instructions',
                delivery_order_instructions_label: 'All deliveries provided by in-house and third-party drivers.',
                order_instructions_desc: 'Please inform us if you have any dietary restrictions.',
                pick_up_order_instructions_label: 'Please come pick up order inside the restuarant.'
            },
            is_item_special_instructions: false,
            wait_time: {
                per_item_time: 0,
                per_order_time: 10,
                base_time: 10
            },
            pickup_hours: [
                {
                    label: 'MONDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'TUESDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '11:00',
                            closes_at: '23:30'
                        }
                    ]
                },
                {
                    label: 'WEDNESDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '15:00',
                            closes_at: '12:03'
                        }
                    ]
                },
                {
                    label: 'THURSDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '12:00',
                            closes_at: '23:00'
                        }
                    ]
                },
                {
                    label: 'FRIDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '00:00',
                            closes_at: '23:59'
                        }
                    ]
                },
                {
                    label: 'SATURDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '05:23',
                            closes_at: '12:03'
                        },
                        {
                            opens_at: '22:23',
                            closes_at: '23:00'
                        }
                    ]
                },
                {
                    label: 'SUNDAY',
                    is_closed: true,
                    period: []
                }
            ],
            is_mobile_ordering: false,
            restaurant_id: '630eff5751c2eac55f52662c',
            id: '634cbcc025606c127758c48e'
        }
    }),
    __metadata("design:type", EatsZaabObject)
], GetEatsZaabResponse.prototype, "data", void 0);
exports.GetEatsZaabResponse = GetEatsZaabResponse;
//# sourceMappingURL=open-eats-zaap.entity.js.map