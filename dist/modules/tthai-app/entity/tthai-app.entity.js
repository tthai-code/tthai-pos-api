"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TThaiAppResponse = exports.TThaiAppResponseFields = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
const day_enum_1 = require("../../restaurant-hours/common/day.enum");
class PeriodFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: String }),
    __metadata("design:type", String)
], PeriodFieldsResponse.prototype, "opensAt", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: String }),
    __metadata("design:type", String)
], PeriodFieldsResponse.prototype, "closesAt", void 0);
class PickUpHoursFieldsResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(day_enum_1.DayEnum) }),
    __metadata("design:type", String)
], PickUpHoursFieldsResponse.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: false }),
    __metadata("design:type", Boolean)
], PickUpHoursFieldsResponse.prototype, "isClosed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => [PeriodFieldsResponse] }),
    __metadata("design:type", Array)
], PickUpHoursFieldsResponse.prototype, "period", void 0);
class WaitTimeResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WaitTimeResponse.prototype, "baseTime", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WaitTimeResponse.prototype, "perOrderTime", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], WaitTimeResponse.prototype, "perItemTime", void 0);
class CustomizeInAppTextResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextResponse.prototype, "pickUpOrderInstructionsLabel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextResponse.prototype, "orderInstructionsDesc", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextResponse.prototype, "deliveryOrderInstructionsLabel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextResponse.prototype, "itemInstructionsLabel", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], CustomizeInAppTextResponse.prototype, "itemInstructionsDesc", void 0);
class TThaiAppResponseFields extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TThaiAppResponseFields.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], TThaiAppResponseFields.prototype, "restaurantId", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], TThaiAppResponseFields.prototype, "isMobileOrdering", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], TThaiAppResponseFields.prototype, "isItemSpecialInstructions", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: PickUpHoursFieldsResponse }),
    __metadata("design:type", PickUpHoursFieldsResponse)
], TThaiAppResponseFields.prototype, "pickupHours", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: WaitTimeResponse }),
    __metadata("design:type", WaitTimeResponse)
], TThaiAppResponseFields.prototype, "waitTime", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: CustomizeInAppTextResponse }),
    __metadata("design:type", CustomizeInAppTextResponse)
], TThaiAppResponseFields.prototype, "customizeInAppText", void 0);
exports.TThaiAppResponseFields = TThaiAppResponseFields;
class TThaiAppResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: TThaiAppResponseFields,
        example: {
            status: 'active',
            customize_in_app_text: {
                item_instructions_desc: 'Item Desc Test',
                item_instructions_label: 'Item Label Test',
                delivery_order_instructions_label: 'Test Label',
                order_instructions_desc: 'Test Desc',
                pick_up_order_instructions_label: 'pickUpOrderInstructionsLabel Test'
            },
            is_item_special_instructions: false,
            wait_time: {
                per_item_time: 15,
                per_order_time: 15,
                base_time: 15
            },
            pickup_hours: [
                {
                    label: 'MONDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '09:00AM',
                            closes_at: '12:00AM'
                        }
                    ]
                },
                {
                    label: 'TUESDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'WEDNESDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'THURSDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'FRIDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'SATURDAY',
                    is_closed: true,
                    period: []
                },
                {
                    label: 'SUNDAY',
                    is_closed: true,
                    period: []
                }
            ],
            is_mobile_ordering: true,
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-10-11T09:56:47.331Z',
            updated_at: '2022-10-11T10:42:14.119Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '63453ddf7fd721e55848b038'
        }
    }),
    __metadata("design:type", TThaiAppResponseFields)
], TThaiAppResponse.prototype, "data", void 0);
exports.TThaiAppResponse = TThaiAppResponse;
//# sourceMappingURL=tthai-app.entity.js.map