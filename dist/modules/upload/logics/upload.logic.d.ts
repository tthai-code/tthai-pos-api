interface UploadResultInterface {
    publicUrl: string;
    prefix: string;
    name: string;
}
export declare class UploadLogic {
    private AWS_S3_BUCKET_NAME;
    private s3;
    private accessKeyId;
    private secretAccessKey;
    private AWS_S3_REGION;
    storage: any;
    constructor();
    private randomFileName;
    uploadLogicS3(file: any, path?: string): Promise<UploadResultInterface | string>;
}
export {};
