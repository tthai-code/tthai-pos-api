"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadLogic = void 0;
const common_1 = require("@nestjs/common");
const aws_sdk_1 = require("aws-sdk");
const dayjs = require("dayjs");
const advancedFormat = require("dayjs/plugin/advancedFormat");
dayjs.extend(advancedFormat);
let UploadLogic = class UploadLogic {
    constructor() {
        this.AWS_S3_BUCKET_NAME = process.env.AWS_S3_BUCKET_NAME;
        this.accessKeyId = process.env.AWS_S3_ACCESS_KEY_ID;
        this.secretAccessKey = process.env.AWS_S3_SECRET_ACCESS_KEY;
        this.AWS_S3_REGION = process.env.AWS_S3_REGION;
        this.s3 = new aws_sdk_1.S3({
            accessKeyId: this.accessKeyId,
            secretAccessKey: this.secretAccessKey
        });
    }
    randomFileName() {
        const randomNumber = Math.floor(Math.random() * 100000) + 1;
        return `${dayjs().format('x')}${randomNumber}`;
    }
    uploadLogicS3(file, path) {
        return new Promise(async (resolve, reject) => {
            const { originalname, buffer } = file;
            const temps = buffer;
            const fileName = originalname.split('.');
            const extention = fileName[fileName.length - 1];
            let contentType = 'application/octet-stream';
            if (extention === 'png' ||
                extention === 'jpg' ||
                extention === 'jpeg' ||
                extention === 'gif') {
                contentType = 'image/' + extention;
            }
            else if (extention === 'pdf') {
                contentType = 'application/' + extention;
            }
            const name = `${this.randomFileName()}.${extention}`;
            const folder = process.env.NODE_ENV === 'production' ? 'production' : 'staging';
            const pathFile = path ? `${path.replace(/^\/|\/$/g, '')}/` : '';
            try {
                const uploadedImage = await this.s3
                    .upload({
                    Bucket: this.AWS_S3_BUCKET_NAME,
                    Key: `upload/${folder}/${pathFile}${name}`,
                    Body: Buffer.from(Object.values(temps)),
                    ACL: 'public-read',
                    ContentType: contentType
                })
                    .promise();
                resolve({
                    publicUrl: `${uploadedImage.Location}`,
                    prefix: `https://${this.AWS_S3_BUCKET_NAME}.s3.${this.AWS_S3_REGION}.amazonaws.com/upload/${folder}/${pathFile.replace(/\/$/, '')}`,
                    name
                });
            }
            catch (err) {
                reject(err);
            }
        });
    }
};
UploadLogic = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], UploadLogic);
exports.UploadLogic = UploadLogic;
//# sourceMappingURL=upload.logic.js.map