/// <reference types="multer" />
import { UploadFileDto } from '../dto/upload.dto';
import { UploadLogic } from '../logics/upload.logic';
export declare class UploadController {
    private readonly uploadLogic;
    constructor(uploadLogic: UploadLogic);
    uploadFile(file: Express.Multer.File, query: UploadFileDto): Promise<any>;
}
