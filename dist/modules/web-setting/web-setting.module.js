"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSettingModule = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const restaurant_module_1 = require("../restaurant/restaurant.module");
const web_setting_public_controller_1 = require("./controllers/web-setting-public.controller");
const web_setting_controller_1 = require("./controllers/web-setting.controller");
const web_setting_schema_1 = require("./schemas/web-setting.schema");
const web_setting_service_1 = require("./services/web-setting.service");
let WebSettingModule = class WebSettingModule {
};
WebSettingModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => restaurant_module_1.RestaurantModule),
            mongoose_1.MongooseModule.forFeature([
                { name: 'webSetting', schema: web_setting_schema_1.WebSettingSchema }
            ])
        ],
        providers: [web_setting_service_1.WebSettingService],
        controllers: [web_setting_controller_1.WebSettingController, web_setting_public_controller_1.OnlineWebSettingController],
        exports: [web_setting_service_1.WebSettingService]
    })
], WebSettingModule);
exports.WebSettingModule = WebSettingModule;
//# sourceMappingURL=web-setting.module.js.map