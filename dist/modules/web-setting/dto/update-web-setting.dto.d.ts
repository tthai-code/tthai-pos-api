export declare class UpdateWebSettingDto {
    readonly isOnlineOrdering: boolean;
    readonly isReservation: boolean;
    readonly bannerImageUrl: string;
    readonly domain: string;
    readonly subDomain: string;
    readonly favIconUrl: string;
    readonly logoUrl: string;
    readonly whatAppsUrl: string;
    readonly facebookUrl: string;
    readonly mainColor: string;
    readonly hoverColor: string;
    readonly contrastColor: string;
}
