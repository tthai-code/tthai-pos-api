import { Types, Schema as MongooseSchema, Document } from 'mongoose';
import { UserStampSchema } from 'src/common/schemas/user-stamp.schema';
export declare type WebSettingDocument = WebSettingFields & Document;
export declare class WebSettingFields {
    restaurantId: Types.ObjectId;
    isOnlineOrdering: boolean;
    isReservation: boolean;
    bannerImageUrl: string;
    domain: string;
    subDomain: string;
    favIconUrl: string;
    logoUrl: string;
    whatAppsUrl: string;
    facebookUrl: string;
    mainColor: string;
    hoverColor: string;
    contrastColor: string;
    status: string;
    updatedBy: UserStampSchema;
    createdBy: UserStampSchema;
}
export declare const WebSettingSchema: MongooseSchema<Document<WebSettingFields, any, any>, import("mongoose").Model<Document<WebSettingFields, any, any>, any, any, any>, any, any>;
