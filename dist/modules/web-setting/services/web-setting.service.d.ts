import { ClientSession } from 'mongodb';
import { Model } from 'mongoose';
import { WebSettingDocument } from '../schemas/web-setting.schema';
export declare class WebSettingService {
    private readonly WebSettingModel;
    private request;
    constructor(WebSettingModel: any, request: any);
    resolveByUrl({ id }: {
        id: any;
    }): Promise<WebSettingDocument | any>;
    resolveByCondition(condition: any): Promise<WebSettingDocument | any>;
    isExists(condition: any): Promise<boolean>;
    getModel(): Model<WebSettingDocument>;
    aggregate(pipeline: any[]): Promise<any[]>;
    getSession(): Promise<ClientSession>;
    transactionCreate(payload: any, session: ClientSession): Promise<WebSettingDocument>;
    transactionUpdate(id: string, product: any, session: ClientSession): Promise<WebSettingDocument>;
    create(payload: any): Promise<WebSettingDocument>;
    update(id: string, payload: any): Promise<WebSettingDocument>;
    findOneAndUpdate(condition: any, payload: any): Promise<WebSettingDocument>;
    delete(id: string): Promise<WebSettingDocument>;
    getAll(condition: any, project?: any): Promise<any[]>;
    findById(id: number): Promise<any>;
    findOne(condition: any, project?: any): Promise<WebSettingDocument>;
    findOneWithSelect(condition: any, options?: any): Promise<WebSettingDocument>;
}
