"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicWebSettingResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const day_enum_1 = require("../../restaurant-hours/common/day.enum");
class TaxRateObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], TaxRateObject.prototype, "is_alcohol_tax_active", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TaxRateObject.prototype, "alcohol_tax", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], TaxRateObject.prototype, "tax", void 0);
class AddressObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "zip_code", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "state", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "city", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], AddressObject.prototype, "address", void 0);
class RestaurantObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: TaxRateObject }),
    __metadata("design:type", TaxRateObject)
], RestaurantObject.prototype, "tax_rate", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "business_phone", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: AddressObject }),
    __metadata("design:type", AddressObject)
], RestaurantObject.prototype, "address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "dba", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "legal_business_name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Number)
], RestaurantObject.prototype, "default_service_charge", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], RestaurantObject.prototype, "id", void 0);
class PeriodObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PeriodObject.prototype, "opens_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], PeriodObject.prototype, "closes_at", void 0);
class HoursObject {
}
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(day_enum_1.DayEnum) }),
    __metadata("design:type", String)
], HoursObject.prototype, "label", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], HoursObject.prototype, "is_closed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: [PeriodObject] }),
    __metadata("design:type", Array)
], HoursObject.prototype, "period", void 0);
class WebSettingObject {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingObject.prototype, "sub_domain", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingObject.prototype, "banner_image_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingObject.prototype, "contrast_color", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingObject.prototype, "hover_color", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingObject.prototype, "main_color", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingObject.prototype, "domain", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], WebSettingObject.prototype, "is_reservation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], WebSettingObject.prototype, "is_online_ordering", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingObject.prototype, "restaurant_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingObject.prototype, "id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: RestaurantObject }),
    __metadata("design:type", RestaurantObject)
], WebSettingObject.prototype, "restaurant", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: HoursObject }),
    __metadata("design:type", Array)
], WebSettingObject.prototype, "hours", void 0);
class PublicWebSettingResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: WebSettingObject,
        example: {
            facebook_url: null,
            what_apps_url: null,
            fav_icon_url: null,
            contrast_color: '#FF0000FF',
            hover_color: '#FF0000FF',
            main_color: '#FF0000FF',
            sub_domain: null,
            domain: 'tthai-store-staging.us-west-1.elasticbeanstalk.com',
            banner_image_url: 'https://tthai-pos-storage.s3.us-west-1.amazonaws.com/upload/production/167336677425525414.png',
            is_reservation: false,
            is_online_ordering: true,
            restaurant_id: '63984d6690416c27a7415915',
            logo_url: '',
            id: '639b74eb5d5e20e31ebc7c61',
            restaurant: {
                default_service_charge: 10,
                tax_rate: {
                    is_alcohol_tax_active: false,
                    alcohol_tax: 0,
                    tax: 10
                },
                business_phone: '1234567890',
                address: {
                    zip_code: '90263',
                    state: 'CA',
                    city: 'Malibu',
                    address: 'TEestst Address'
                },
                dba: 'Test DBA 03',
                legal_business_name: 'TeST 03',
                id: '63984d6690416c27a7415915'
            },
            hours: [
                {
                    label: 'MONDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '01:00',
                            closes_at: '05:00'
                        },
                        {
                            opens_at: '12:00',
                            closes_at: '15:00'
                        }
                    ]
                },
                {
                    label: 'TUESDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '00:00',
                            closes_at: '23:59'
                        }
                    ]
                },
                {
                    label: 'WEDNESDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '00:01',
                            closes_at: '23:59'
                        }
                    ]
                },
                {
                    label: 'THURSDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '00:00',
                            closes_at: '17:00'
                        }
                    ]
                },
                {
                    label: 'FRIDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '10:00',
                            closes_at: '13:00'
                        },
                        {
                            opens_at: '15:00',
                            closes_at: '17:00'
                        }
                    ]
                },
                {
                    label: 'SATURDAY',
                    is_closed: false,
                    period: [
                        {
                            opens_at: '11:00',
                            closes_at: '11:30'
                        },
                        {
                            opens_at: '13:00',
                            closes_at: '14:00'
                        },
                        {
                            opens_at: '23:00',
                            closes_at: '23:30'
                        }
                    ]
                },
                {
                    label: 'SUNDAY',
                    is_closed: true,
                    period: []
                }
            ]
        }
    }),
    __metadata("design:type", WebSettingObject)
], PublicWebSettingResponse.prototype, "data", void 0);
exports.PublicWebSettingResponse = PublicWebSettingResponse;
//# sourceMappingURL=public-web-setting.entity.js.map