import { ResponseDto } from 'src/common/entity/response.entity';
import { TimestampResponseDto } from 'src/common/entity/timestamp.entity';
export declare class WebSettingResponseField extends TimestampResponseDto {
    isOnlineOrdering: boolean;
    isReservation: boolean;
    domain: string;
    mainColor: string;
    hoverColor: string;
    contrastColor: string;
    facebook_url: string;
    what_apps_url: string;
    logo_url: string;
    fav_icon_url: string;
    sub_domain: string;
    banner_image_url: string;
}
export declare class GetWebSettingResponse extends ResponseDto<WebSettingResponseField> {
    data: WebSettingResponseField;
}
