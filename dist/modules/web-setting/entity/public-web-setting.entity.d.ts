import { ResponseDto } from 'src/common/entity/response.entity';
declare class TaxRateObject {
    is_alcohol_tax_active: boolean;
    alcohol_tax: number;
    tax: number;
}
declare class AddressObject {
    zip_code: string;
    state: string;
    city: string;
    address: string;
}
declare class RestaurantObject {
    tax_rate: TaxRateObject;
    business_phone: string;
    address: AddressObject;
    dba: string;
    legal_business_name: string;
    default_service_charge: number;
    id: string;
}
declare class PeriodObject {
    opens_at: string;
    closes_at: string;
}
declare class HoursObject {
    label: string;
    is_closed: boolean;
    period: PeriodObject[];
}
declare class WebSettingObject {
    sub_domain: string;
    banner_image_url: string;
    contrast_color: string;
    hover_color: string;
    main_color: string;
    domain: string;
    is_reservation: boolean;
    is_online_ordering: boolean;
    restaurant_id: string;
    id: string;
    restaurant: RestaurantObject;
    hours: HoursObject[];
}
export declare class PublicWebSettingResponse extends ResponseDto<WebSettingObject> {
    data: WebSettingObject;
}
export {};
