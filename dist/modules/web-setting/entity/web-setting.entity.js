"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetWebSettingResponse = exports.WebSettingResponseField = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("../../../common/entity/response.entity");
const timestamp_entity_1 = require("../../../common/entity/timestamp.entity");
class WebSettingResponseField extends timestamp_entity_1.TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], WebSettingResponseField.prototype, "isOnlineOrdering", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], WebSettingResponseField.prototype, "isReservation", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "domain", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "mainColor", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "hoverColor", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "contrastColor", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "facebook_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "what_apps_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "logo_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "fav_icon_url", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "sub_domain", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], WebSettingResponseField.prototype, "banner_image_url", void 0);
exports.WebSettingResponseField = WebSettingResponseField;
class GetWebSettingResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: WebSettingResponseField,
        example: {
            facebook_url: null,
            what_apps_url: null,
            logo_url: null,
            fav_icon_url: null,
            sub_domain: null,
            banner_image_url: null,
            status: 'active',
            contrast_color: null,
            hover_color: null,
            main_color: null,
            domain: 'localhost:3001',
            is_reservation: false,
            is_online_ordering: false,
            restaurant_id: '630e55a0d9c30fd7cdcb424b',
            created_at: '2022-10-10T12:46:07.855Z',
            updated_at: '2022-12-01T05:05:24.359Z',
            updated_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            created_by: {
                username: 'corywong@mail.com',
                id: '630e53ec9e21d871a49fb4f5'
            },
            id: '6344140fce2588c0dfe2428b'
        }
    }),
    __metadata("design:type", WebSettingResponseField)
], GetWebSettingResponse.prototype, "data", void 0);
exports.GetWebSettingResponse = GetWebSettingResponse;
//# sourceMappingURL=web-setting.entity.js.map