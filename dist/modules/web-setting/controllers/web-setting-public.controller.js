"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OnlineWebSettingController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const log_logic_1 = require("../../log/logic/log.logic");
const restaurant_hours_logic_1 = require("../../restaurant-hours/logics/restaurant-hours.logic");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const get_online_web_setting_dto_1 = require("../dto/get-online-web-setting.dto");
const public_web_setting_entity_1 = require("../entity/public-web-setting.entity");
const web_setting_service_1 = require("../services/web-setting.service");
let OnlineWebSettingController = class OnlineWebSettingController {
    constructor(webSettingService, restaurantService, restaurantHourLogic, logLogic) {
        this.webSettingService = webSettingService;
        this.restaurantService = restaurantService;
        this.restaurantHourLogic = restaurantHourLogic;
        this.logLogic = logLogic;
    }
    async getWebSetting(payload) {
        const { domain } = payload;
        const transformDomain = domain.replace(/(?:www.)?/, '');
        const webSetting = await this.webSettingService.findOne({
            $or: [
                {
                    domain: transformDomain
                },
                {
                    subDomain: transformDomain
                }
            ],
            status: status_enum_1.StatusEnum.ACTIVE
        }, {
            status: 0,
            createdAt: 0,
            updatedAt: 0,
            createdBy: 0,
            updatedBy: 0
        });
        if (!webSetting)
            throw new common_1.NotFoundException();
        const restaurant = await this.restaurantService.findOne({ _id: webSetting.restaurantId }, {
            taxRate: 1,
            logo_url: 1,
            address: 1,
            defaultServiceCharge: 1,
            legalBusinessName: 1,
            dba: 1,
            businessPhone: 1
        });
        const { hours } = await this.restaurantHourLogic.getRestaurantHours(restaurant.id);
        return Object.assign(Object.assign({}, webSetting.toObject()), { restaurant, hours });
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: () => public_web_setting_entity_1.PublicWebSettingResponse }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get Web Setting For Web E-commerce',
        description: 'use `basic auth`'
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_online_web_setting_dto_1.GetOnlineWebSettingDto]),
    __metadata("design:returntype", Promise)
], OnlineWebSettingController.prototype, "getWebSetting", null);
OnlineWebSettingController = __decorate([
    (0, swagger_1.ApiBasicAuth)(),
    (0, swagger_1.ApiTags)('online/web-setting'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    (0, common_1.Controller)('v1/online/web-setting'),
    __metadata("design:paramtypes", [web_setting_service_1.WebSettingService,
        restaurant_service_1.RestaurantService,
        restaurant_hours_logic_1.RestaurantHoursLogic,
        log_logic_1.LogLogic])
], OnlineWebSettingController);
exports.OnlineWebSettingController = OnlineWebSettingController;
//# sourceMappingURL=web-setting-public.controller.js.map