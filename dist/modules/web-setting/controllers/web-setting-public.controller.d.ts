/// <reference types="mongoose" />
import { LogLogic } from 'src/modules/log/logic/log.logic';
import { RestaurantHoursLogic } from 'src/modules/restaurant-hours/logics/restaurant-hours.logic';
import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { GetOnlineWebSettingDto } from '../dto/get-online-web-setting.dto';
import { WebSettingService } from '../services/web-setting.service';
export declare class OnlineWebSettingController {
    private readonly webSettingService;
    private readonly restaurantService;
    private readonly restaurantHourLogic;
    private readonly logLogic;
    constructor(webSettingService: WebSettingService, restaurantService: RestaurantService, restaurantHourLogic: RestaurantHoursLogic, logLogic: LogLogic);
    getWebSetting(payload: GetOnlineWebSettingDto): Promise<{
        restaurant: import("../../restaurant/schemas/restaurant.schema").RestaurantDocument;
        hours: any[];
        status: string;
        _id?: any;
        id?: any;
        __v?: any;
        updatedBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        createdBy: import("../../../common/schemas/user-stamp.schema").UserStampSchema;
        logoUrl: string;
        restaurantId: import("mongoose").Types.ObjectId;
        isOnlineOrdering: boolean;
        isReservation: boolean;
        bannerImageUrl: string;
        domain: string;
        subDomain: string;
        favIconUrl: string;
        whatAppsUrl: string;
        facebookUrl: string;
        mainColor: string;
        hoverColor: string;
        contrastColor: string;
    }>;
}
