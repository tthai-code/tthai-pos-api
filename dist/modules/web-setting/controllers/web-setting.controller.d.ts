import { RestaurantService } from 'src/modules/restaurant/services/restaurant.service';
import { UpdateWebSettingDto } from '../dto/update-web-setting.dto';
import { WebSettingService } from '../services/web-setting.service';
export declare class WebSettingController {
    private readonly webSettingService;
    private readonly restaurantService;
    constructor(webSettingService: WebSettingService, restaurantService: RestaurantService);
    getCatering(id: string): Promise<import("../schemas/web-setting.schema").WebSettingDocument>;
    updateOnlineReceipt(id: string, payload: UpdateWebSettingDto): Promise<import("../schemas/web-setting.schema").WebSettingDocument>;
}
