"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSettingController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../../../common/enum/status.enum");
const admin_auth_guard_1 = require("../../auth/guards/admin-auth.guard");
const restaurant_service_1 = require("../../restaurant/services/restaurant.service");
const update_web_setting_dto_1 = require("../dto/update-web-setting.dto");
const web_setting_entity_1 = require("../entity/web-setting.entity");
const web_setting_service_1 = require("../services/web-setting.service");
let WebSettingController = class WebSettingController {
    constructor(webSettingService, restaurantService) {
        this.webSettingService = webSettingService;
        this.restaurantService = restaurantService;
    }
    async getCatering(id) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        const catering = await this.webSettingService.findOne({
            restaurantId: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!catering) {
            await this.webSettingService.create({
                restaurantId: id
            });
            return await this.webSettingService.findOne({
                restaurantId: id,
                status: status_enum_1.StatusEnum.ACTIVE
            });
        }
        return catering;
    }
    async updateOnlineReceipt(id, payload) {
        const restaurant = await this.restaurantService.findOne({
            _id: id,
            status: status_enum_1.StatusEnum.ACTIVE
        });
        if (!restaurant)
            throw new common_1.NotFoundException('Not found restaurant.');
        return await this.webSettingService.findOneAndUpdate({ restaurantId: id }, payload);
    }
};
__decorate([
    (0, common_1.Get)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => web_setting_entity_1.GetWebSettingResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'get web setting by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], WebSettingController.prototype, "getCatering", null);
__decorate([
    (0, common_1.Put)(':restaurantId'),
    (0, swagger_1.ApiOkResponse)({ type: () => web_setting_entity_1.GetWebSettingResponse }),
    (0, swagger_1.ApiOperation)({ summary: 'update web setting by restaurant' }),
    __param(0, (0, common_1.Param)('restaurantId')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_web_setting_dto_1.UpdateWebSettingDto]),
    __metadata("design:returntype", Promise)
], WebSettingController.prototype, "updateOnlineReceipt", null);
WebSettingController = __decorate([
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiTags)('web-setting'),
    (0, common_1.UseGuards)(admin_auth_guard_1.AdminAuthGuard),
    (0, common_1.Controller)('v1/web-setting'),
    __metadata("design:paramtypes", [web_setting_service_1.WebSettingService,
        restaurant_service_1.RestaurantService])
], WebSettingController);
exports.WebSettingController = WebSettingController;
//# sourceMappingURL=web-setting.controller.js.map