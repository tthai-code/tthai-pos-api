"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const dayjs_1 = require("./plugins/dayjs");
const dateTime_1 = require("./utilities/dateTime");
let AppController = class AppController {
    getHello() {
        return {
            date: new Date(),
            status: 'ok'
        };
    }
    async getTimeZone(query) {
        const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        const date = query.date || new Date().toISOString();
        const { tz } = query;
        const dayOfWeekString = await (0, dateTime_1.GetDayStringByDay)((0, dayjs_1.default)().tz(tz).day());
        const now = (0, dayjs_1.default)(date).tz(tz);
        const nowUnix = now.valueOf();
        const startOfDay = now.hour(4).minute(0).millisecond(0).valueOf();
        const endOfDay = now
            .add(1, 'day')
            .hour(3)
            .minute(59)
            .millisecond(999)
            .valueOf();
        let nowFormat = now.subtract(1, 'day').format('YYYYMMDD');
        if (nowUnix >= startOfDay && nowUnix <= endOfDay) {
            nowFormat = now.format('YYYYMMDD');
        }
        return {
            nowFormat,
            timeZone,
            nowLocale: new Date().toLocaleString('en-US', {
                timeZone
            }),
            now: new Date().toString(),
            ISODate: new Date().toISOString(),
            dayjs: (0, dayjs_1.default)(date),
            UTCDayjs: (0, dayjs_1.default)(date).tz('UTC'),
            timeZoneDayjs: (0, dayjs_1.default)(date).tz(tz),
            dayjsFormat: (0, dayjs_1.default)(date).format('YYYYMMDD'),
            UTCDayjsFormat: (0, dayjs_1.default)(date).tz('UTC').format('YYYYMMDD'),
            timeZoneDayjsFormat: (0, dayjs_1.default)(date).tz(tz).format('YYYYMMDD'),
            timeZoneString: (0, dayjs_1.default)(date).tz(tz).toString(),
            dayOfWeekString
        };
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOperation)({ summary: 'Healthz' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], AppController.prototype, "getHello", null);
__decorate([
    (0, common_1.Get)('time-zone'),
    (0, swagger_1.ApiOperation)({ summary: 'Check Time Zone' }),
    __param(0, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "getTimeZone", null);
AppController = __decorate([
    (0, common_1.Controller)()
], AppController);
exports.AppController = AppController;
//# sourceMappingURL=app.controller.js.map