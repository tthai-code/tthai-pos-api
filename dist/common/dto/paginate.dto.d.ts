export declare class PaginateDto {
    readonly page: string;
    readonly limit: string;
}
