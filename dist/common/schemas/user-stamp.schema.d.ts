import * as mongoose from 'mongoose';
export declare class UserStampSchema {
    id: mongoose.Schema.Types.Mixed;
    username: string;
}
