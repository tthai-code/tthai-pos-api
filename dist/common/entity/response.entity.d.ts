export declare abstract class ResponseDto<T> {
    abstract get data(): T;
    message: string;
}
