declare class AuthorStampFields {
    username: string;
    id: string;
}
export declare class TimestampResponseDto {
    created_at: string;
    updated_at: string;
    created_by: AuthorStampFields;
    updated_by: AuthorStampFields;
    status: string;
}
export {};
