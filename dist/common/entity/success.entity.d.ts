import { ResponseDto } from './response.entity';
export declare class SuccessField {
    success: boolean;
}
declare class SuccessWithIdField extends SuccessField {
    id: string;
}
export declare class SuccessObjectResponse extends ResponseDto<SuccessField> {
    data: SuccessField;
}
export declare class SuccessWithIdResponse extends ResponseDto<SuccessWithIdField> {
    data: SuccessWithIdField;
}
export {};
