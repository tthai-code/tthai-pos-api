export declare abstract class PaginateResponseDto<T> {
    abstract get results(): T;
    total: number;
    limit: number;
    page: number;
    pages: number;
}
