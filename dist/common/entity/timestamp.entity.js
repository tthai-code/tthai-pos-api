"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimestampResponseDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const status_enum_1 = require("../enum/status.enum");
class AuthorStampFields {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: 'superadmin' }),
    __metadata("design:type", String)
], AuthorStampFields.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: '6318d06a1fa78236d380db83' }),
    __metadata("design:type", String)
], AuthorStampFields.prototype, "id", void 0);
class TimestampResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ example: '2022-10-11T03:56:46.826Z' }),
    __metadata("design:type", String)
], TimestampResponseDto.prototype, "created_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ example: '2022-10-11T03:56:46.826Z' }),
    __metadata("design:type", String)
], TimestampResponseDto.prototype, "updated_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => AuthorStampFields }),
    __metadata("design:type", AuthorStampFields)
], TimestampResponseDto.prototype, "created_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ type: () => AuthorStampFields }),
    __metadata("design:type", AuthorStampFields)
], TimestampResponseDto.prototype, "updated_by", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ enum: Object.values(status_enum_1.StatusEnum), example: status_enum_1.StatusEnum.ACTIVE }),
    __metadata("design:type", String)
], TimestampResponseDto.prototype, "status", void 0);
exports.TimestampResponseDto = TimestampResponseDto;
//# sourceMappingURL=timestamp.entity.js.map