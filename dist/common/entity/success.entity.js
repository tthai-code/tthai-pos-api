"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuccessWithIdResponse = exports.SuccessObjectResponse = exports.SuccessField = void 0;
const swagger_1 = require("@nestjs/swagger");
const response_entity_1 = require("./response.entity");
class SuccessField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", Boolean)
], SuccessField.prototype, "success", void 0);
exports.SuccessField = SuccessField;
class SuccessWithIdField extends SuccessField {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    __metadata("design:type", String)
], SuccessWithIdField.prototype, "id", void 0);
class SuccessObjectResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({ type: SuccessField, example: { success: true } }),
    __metadata("design:type", SuccessField)
], SuccessObjectResponse.prototype, "data", void 0);
exports.SuccessObjectResponse = SuccessObjectResponse;
class SuccessWithIdResponse extends response_entity_1.ResponseDto {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        type: SuccessWithIdField,
        example: { success: true, id: '63a0dabb4d8b0a61654c6d10' }
    }),
    __metadata("design:type", SuccessWithIdField)
], SuccessWithIdResponse.prototype, "data", void 0);
exports.SuccessWithIdResponse = SuccessWithIdResponse;
//# sourceMappingURL=success.entity.js.map