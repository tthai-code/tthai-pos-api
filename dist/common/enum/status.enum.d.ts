export declare enum StatusEnum {
    ACTIVE = "active",
    DELETED = "deleted",
    INACTIVE = "inactive"
}
