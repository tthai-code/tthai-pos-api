"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusEnum = void 0;
var StatusEnum;
(function (StatusEnum) {
    StatusEnum["ACTIVE"] = "active";
    StatusEnum["DELETED"] = "deleted";
    StatusEnum["INACTIVE"] = "inactive";
})(StatusEnum = exports.StatusEnum || (exports.StatusEnum = {}));
//# sourceMappingURL=status.enum.js.map